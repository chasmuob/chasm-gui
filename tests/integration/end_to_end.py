""" end_to_end.py

End-to-end system tests.

We pre-populate the database with sample input data and real releases for CHASM and QUESTA.

Each test then uploads a task document and checks that:
 - The results match those calculated by standalone CHASM/QUESTA binary
 - The DB is in the appropriate state

Note: These tests assume an already running BigCouch cluster + sentinels running on SL5. Anything else will almost definitely fail.

"""

import json
import os
import unittest

from couch.Couch import CouchServer
from couch.CouchRequest import CouchConflictError

from couchtest_helpers import upload_couchapp, upload_release, results_on_precondition

__COUCHAPP_DIR__ = os.path.join('..', '..', 'src', 'couchapps')
__EXECUTABLES__ = os.path.join('..', 'data', 'executables')
__RELEASES__ = os.path.join('..', 'data', 'releases')
__TASK_DEFINITIONS__ = 'task_definitions'

class SingleTask(unittest.TestCase):
    def setUp(self):
        """ Create and populate DBs """
        self.url = os.getenv("COUCHURL", 'http://admin:pass@localhost:5984')
        self.server = CouchServer(self.url)
        base_dbname = 'integration_test'
        self.dbs = {
            'task': {'name': '_'.join((base_dbname, 'task'))},
            'state': {'name': '_'.join((base_dbname, 'state'))},
            'data': {'name': base_dbname},
            'release': {'name': '_'.join((base_dbname, 'release'))}
        }
        for db_ref, db in self.dbs.items():
            if db['name'] in self.server.listDatabases():
                 self.server.deleteDatabase(db['name'])
            db['db'] = self.server.connectDatabase(db['name'])

        self.__populate_dbs()

    def __populate_dbs(self):
        try:
            upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'chasm_1.2.3.json'), os.path.join(__EXECUTABLES__, 'chasm'))
        except Exception:
            pass    # Assume any errors here are because the release already exists and proceed as normal
        try:
            upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'questa_4.5.6.json'), os.path.join(__EXECUTABLES__, 'questa'))
        except Exception:
            pass    # Assume any errors here are because the release already exists and proceed as normal

        upload_couchapp(os.path.join('couchapps', 'end_to_end'), self.dbs['data']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'mossaic'), self.dbs['data']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'gui'), self.dbs['data']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'mining'), self.dbs['data']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'state-machine'), self.dbs['state']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'workflow'), self.dbs['task']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'release'), self.dbs['release']['name'], self.url)

    def tearDown(self):
        pass

    def test_end_to_end_simple(self):
        """ No vegetation, no reinforcements, no stochastic params"""
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__TASK_DEFINITIONS__, 'chasm.json')).read()))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__TASK_DEFINITIONS__, 'questa.json')).read()))
        # Wait until we have two results
        results = results_on_precondition(function=self.dbs['data']['db'].loadView,
                args=['mossaic', 'results', {'reduce': False, 'include_docs': True}],
                test=lambda x:len(x['rows']) >= 2, wait=10, retry=60)
        for row in results['rows']:
            self.assertTrue('error' not in row['doc'])


if __name__ == '__main__':
    unittest.main(verbosity=2)