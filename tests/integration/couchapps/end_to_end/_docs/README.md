# Documents for end-to-end integration test

These are (mostly) symlinks to files in tests/data/input\_files. This is the sensible thing to do as we avoid duplicating existing files however it does mean that should the files in tests/data/input\_files get removed these links will be dangling.