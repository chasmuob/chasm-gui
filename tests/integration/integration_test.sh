#!/bin/bash

# Prepare remote host for integration test
#
# For best results you will want the private key for the mossaicinstall user in ~/.ssh

REMOTEUSER=mossaicinstall

function print_usage {
    echo "Usage: integration_test.sh -h HOSTNAME -r REMOTE_MOSSAIC_DIR"
}

if [ $# -eq 0 ]; then
    print_usage
    exit 1
fi

while getopts ":h:r:d" opt; do
    case $opt in
        d)
            dummy_run=true
            ;;
        h)
            target_host=$OPTARG
            ;;
        r)
            remote_mossaic_dir=$OPTARG
            ;;
        \?)
            echo "Invalid option: -$OPTARG"
            print_usage
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument."
    esac
done

export COUCHURL=http://admin:pass@$target_host:5984

ssh $REMOTEUSER@$target_host "source $remote_mossaic_dir/mossaicenv;$remote_mossaic_dir/etc/init.d/jobmanager stop"
ssh $REMOTEUSER@$target_host "source $remote_mossaic_dir/mossaicenv;$remote_mossaic_dir/etc/init.d/bigcouch stop"
ssh $REMOTEUSER@$target_host "cp $remote_mossaic_dir/etc/local.ini $remote_mossaic_dir/etc/local.ini.bk; cp $remote_mossaic_dir/etc/jobmanager.ini $remote_mossaic_dir/etc/jobmanager.ini.bk; cp $remote_mossaic_dir/etc/submitter.ini $remote_mossaic_dir/etc/submitter.ini.bk"
scp ../../config/local.test.ini  $REMOTEUSER@$target_host:$remote_mossaic_dir/etc/local.ini
scp jobmanager.ini $REMOTEUSER@$target_host:$remote_mossaic_dir/etc/jobmanager.ini
scp submitter.ini $REMOTEUSER@$target_host:$remote_mossaic_dir/etc/submitter.ini
ssh $REMOTEUSER@$target_host "source $remote_mossaic_dir/mossaicenv;$remote_mossaic_dir/etc/init.d/bigcouch start"
if [ ! $dummy_run ]
then
    sleep 5
    PYTHONPATH=../python:../../src/python python end_to_end.py &
    pid=$!
    sleep 5
    ssh $REMOTEUSER@$target_host "source $remote_mossaic_dir/mossaicenv;$remote_mossaic_dir/etc/init.d/jobmanager start"
    wait $pid
fi
ssh $REMOTEUSER@$target_host "source $remote_mossaic_dir/mossaicenv;$remote_mossaic_dir/etc/init.d/jobmanager stop"
ssh $REMOTEUSER@$target_host "source $remote_mossaic_dir/mossaicenv;$remote_mossaic_dir/etc/init.d/bigcouch stop"
ssh $REMOTEUSER@$target_host "mv $remote_mossaic_dir/etc/local.ini.bk $remote_mossaic_dir/etc/local.ini; mv $remote_mossaic_dir/etc/jobmanager.ini.bk $remote_mossaic_dir/etc/jobmanager.ini; mv $remote_mossaic_dir/etc/submitter.ini.bk $remote_mossaic_dir/etc/submitter.ini"