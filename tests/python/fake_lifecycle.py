#!/usr/bin/env python
# encoding: utf-8
"""
Propagate tasks and jobs through various states at ~random to give
a semi realistic database state.
"""

import random
import logging
import uuid
import time
from couch.Couch import CouchServer
from optparse import OptionParser

def parse_options():
    parser = OptionParser()
    parser.add_option("-v", "--verbose", dest="verbose",
                    action="store_true", default=False, help="Be more verbose")
    parser.add_option("-d", "--debug", dest="debug",
                    action="store_true", default=False, help="print debugging statements")
    parser.add_option("--task-db", dest="taskdb", default="mossaic",
                    help="Read/write data to a different database (default: mossaic)")
    parser.add_option("--state-db", dest="statedb", default="state-machine",
                    help="Read/write data to a different database (default: mossaic)")
    parser.add_option("-u", "--url", dest="couch_url", default="http://localhost:5984",
                    help="the URL where the CouchDB is hosted", metavar="URL")
    parser.add_option("-n", "--iterations", dest="iterations", default=5, type="int",
                    help="number of iterations to run, (default: 5)", metavar="N")
    parser.add_option("-r", "--reset", dest="reset",
                    action="store_true", default=False, help="reset the task state to new")
    options, args = parser.parse_args()
    #set up the logger
    log_level = logging.WARNING
    if options.verbose:
        log_level = logging.INFO
    if options.debug:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level)
    options.logger = logging.getLogger('mossaic job wrapper')
    return options, args

def acquire(options, task_db, state_db):
    options.logger.info('Get all new tasks')
    view_options = {'key': 'new', 'reduce':False, 'include_docs':True}
    tasks = task_db.loadView('workflow', 'state', view_options)['rows']
    options.logger.debug('Got %s tasks' % len(tasks))
    if len(tasks) > 1:
        acquired_tasks = random.sample(tasks, random.randint(1, len(tasks)))
    else:
        acquired_tasks = tasks
    options.logger.info('Acquiring %s tasks' % len(acquired_tasks))
    for task in acquired_tasks:
        # This is a bit dodgy, but ok for testing code. We'll need to have a way of
        # making sure that the task doc isn't updated unless the state docs are done
        # which the queue method obfusicates a bit. Probably need to be two separate
        # loops.
        task_doc = task['doc']
        n_jobs = range(random.randint(1,100))
        options.logger.debug('Task %s has %s jobs' % (task_doc['_id'], max(n_jobs)))
        for i in n_jobs:
            state_doc = {
                            'task_id': task_doc['_id'],
                            'job_id': str(uuid.uuid1()),
                            'state': 'splitting',
                            'state_history':{'splitting': {'modified_by': 'foo', 'timestamp': int(time.time())}}
                        }
            state_db.queue(state_doc, timestamp=True, viewlist=['state-machine/jobs_by_state'])
        task_doc['state'] = 'in_progress'
        task_db.queue(task_doc, viewlist=['workflow/state'])
    state_db.commit(viewlist=['state-machine/jobs_by_state'])
    task_db.commit(viewlist=['workflow/state'])

def split(options, state_db):
    options.logger.info('Split jobs')
    view_options = {'key': 'splitting', 'reduce':False, 'include_docs':True}
    jobs = state_db.loadView('state-machine', 'jobs_by_state', view_options)['rows']
    if len(jobs) > 1:
        jobs_to_split = random.sample(jobs, random.randint(1, len(jobs)))
    else:
        jobs_to_split = jobs
    options.logger.info('Splitting %s jobs' % len(jobs_to_split))
    # make fake job docs, update state documents, bump task state
    for job in jobs_to_split:
        state_doc = job['doc']
        state_doc['state'] = 'ready_to_submit'
        state_doc['state_history']['ready_to_submit'] = {'modified_by': 'foo', 'timestamp': int(time.time())}
        state_db.queue(state_doc, timestamp=True)
    state_db.commit(viewlist=['state-machine/jobs_by_state'])

def submit(options, state_db):
    options.logger.info('Propagate some of the ready_to_submit tasks')
    view_options = {'key': 'ready_to_submit', 'reduce':False, 'include_docs':True}
    jobs = state_db.loadView('state-machine', 'jobs_by_state', view_options)['rows']
    if len(jobs) > 1:
        jobs_to_submit = random.sample(jobs, random.randint(1, len(jobs)))
    else:
        jobs_to_submit = jobs
    options.logger.info('Submitting %s jobs' % len(jobs_to_submit))
    for job in jobs_to_submit:
        state_doc = job['doc']
        state_doc['state'] = 'submitted'
        state_doc['state_history']['submitted'] = {'modified_by': 'foo', 'timestamp': int(time.time())}
        state_db.queue(state_doc, timestamp=True, viewlist=['state-machine/jobs_by_state'])
    state_db.commit(viewlist=['state-machine/jobs_by_state'])

def complete(options, state_db):
    options.logger.info('Propagate some of the submitted tasks')
    view_options = {'key': 'submitted', 'reduce':False, 'include_docs':True}
    jobs = state_db.loadView('state-machine', 'jobs_by_state', view_options)['rows']
    if len(jobs) > 1:
        completed_jobs = random.sample(jobs, random.randint(1, len(jobs)))
    else:
        completed_jobs = jobs
    options.logger.info('Completing %s jobs' % len(completed_jobs))
    for job in completed_jobs:
        state_doc = job['doc']
        state_doc['state'] = 'done'
        state_doc['state_history']['done'] = {'modified_by': 'foo', 'timestamp': int(time.time())}
        state_db.queue(state_doc, timestamp=True, viewlist=['state-machine/jobs_by_state'])
    state_db.commit(viewlist=['state-machine/jobs_by_state'])

def job_close_out(options, task_db, state_db):
    """
    Mark a job as done in the task database
    """
    options.logger.info('Update task database with completed jobs')
    view_options = {'key': 'done', 'reduce':False, 'include_docs':True}
    completed_jobs = state_db.loadView('state-machine', 'jobs_by_state', view_options)['rows']
    # TODO: copy state_history into the job documents
    #for task in task_db.loadDocuments(completed_task_ids)['rows']:
    for job in completed_jobs:
        # This should be loading job documents but since we've not written
        # them yet I'll make them up here
        job_doc = {}
        job_doc['_id'] = job['doc']['job_id']
        job_doc['type'] = 'job'
        job_doc['task_id'] = job['doc']['task_id']
        job_doc['state_history'] = job['doc']['state_history']
        task_db.queue(job_doc, viewlist=['workflow/state'])
    task_db.commit(viewlist=['workflow/state'])


def task_close_out(options, task_db, state_db):
    """
    Find all completed tasks, mark them as done in the task database and remove
    the state document for associated jobs from the state database.
    """
    # TODO: production code should do the close out work against the main
    # database, not the state machine, or run a cross check between the two
    options.logger.info('Update task database with final state')
    view_options = {'reduce':True, 'group':True}
    completed_tasks = state_db.loadView('state-machine', 'done_tasks', view_options)['rows']
    completed_task_ids = [task['key'] for task in completed_tasks if task['value']]

    options.logger.info('Found %s completed tasks' % len(completed_task_ids))
    for task in task_db.loadDocuments(completed_task_ids)['rows']:
         task_doc = task['doc']
         task_doc['state'] = 'done'
         task_db.queue(task_doc, viewlist=['workflow/state'])
    task_db.commit(viewlist=['workflow/state'])

    view_options = {'key': 'done', 'reduce':False, 'include_docs':True}
    completed_jobs = state_db.loadView('state-machine', 'jobs_by_state', view_options)['rows']
    completed_jobs_for_task = [state['doc'] for state in completed_jobs if state['doc']['task_id'] in completed_task_ids]
    options.logger.info('Found %s completed jobs' % len(completed_jobs_for_task))
    for state_doc in completed_jobs_for_task:
        state_db.queueDelete(state_doc, viewlist=['state-machine/jobs_by_state'])
    state_db.commit(viewlist=['state-machine/jobs_by_state'])

def reset(options, task_db):
    options.logger.info('Reset all tasks to new')
    view_options = {'key': 'done', 'reduce':False, 'include_docs':True}
    for task in task_db.loadView('workflow', 'state', view_options)['rows']:
        task_doc = task['doc']
        task_doc['state'] = 'new'
        task_db.queue(task_doc, viewlist=['workflow/state'])
    view_options = {'key': 'in_progress', 'reduce':False, 'include_docs':True}
    for task in task_db.loadView('workflow', 'state', view_options)['rows']:
        task_doc = task['doc']
        task_doc['state'] = 'new'
        task_db.queue(task_doc, viewlist=['workflow/state'])
    task_db.commit(viewlist=['workflow/state'])

def main(options, args):
    task_db = CouchServer(options.couch_url).connectDatabase(options.taskdb, size = 250)
    state_db = CouchServer(options.couch_url).connectDatabase(options.statedb, size = 250)
    i = 0
    if options.reset:
        reset(options, task_db)
    while i <= options.iterations:
        options.logger.warning('Iteration %s' % i)
        acquire(options, task_db, state_db)
        split(options, state_db)
        submit(options, state_db)
        complete(options, state_db)
        job_close_out(options, task_db, state_db)
        #task_close_out(options, task_db, state_db)
        #task_db.compact(views=['mossaic'], blocking=True)
        #state_db.compact(views=['state-machine'], blocking=True)
        i = i + 1

if __name__ == '__main__':
    options, args = parse_options()
    main(options, args)

