""" couchtest_helpers.py

A small library of unrelated functions which are shared by tests involving CouchDB

"""

import base64
import os
import json
import sha
import subprocess
import time
import urlparse

from couch.CouchRequest import CouchConflictError

def delete_dbs(dbs, server):
    """ Drop all dbs on specified server """
    for db in dbs.values():
        server.deleteDatabase(db['name'])

def init_dbs(testname, dbname_prefix, server):
    """ Create suite of dbs needed for tests from testname and dbname_prefix """
    dbname_base = '_'.join((dbname_prefix, testname.lower()))
    dbs = {}
    all_dbs = server.listDatabases()
    for dbtype in ('task', 'data', 'state', 'release'):
        dbname = '_'.join((dbname_base, dbtype))
        dbs[dbtype] = {'name': dbname}
        if dbname in all_dbs:
            server.deleteDatabase(dbname)
        dbs[dbtype]['db'] = server.connectDatabase(dbname)
    return dbs

def upload_couchapp(couchapp_target, target_db, url):
    """ Helper method which uploads the specified couchapp

    We retry if the upload fails - this is because there is a bug with couchapp on Mac OS X which causes uploads to be truncated intermittently

    """
    max_retries = 10
    retries = 0
    for i in range(max_retries):
        try:
            if subprocess.check_call(['couchapp', 'push', '%s/%s' % (url, target_db)], cwd=couchapp_target) == 0:
                break
        except subprocess.CalledProcessError, e:
            if i + 1 == max_retries:
                raise e

def upload_release(db, release_doc_path, executable_path):
    """ Helper method which creates a release doc, uploads it and attaches the specified executable """
    dummy_executable = file(executable_path).read()
    doc = db.commitOne(json.loads(file(release_doc_path).read()), returndocs=True)[0]
    try:
        db.addAttachment(doc['id'], doc['rev'], dummy_executable, name='executable', contentType='application/octet-stream')
    except CouchConflictError:
        pass    # Ignore CouchConflict errors - see https://wikis.bris.ac.uk/display/mossaicgrid/BigCouch+cluster+tests:w

def results_on_precondition(function, args, test, wait, retry):
    """ Retry given function until the results pass the function specified in test, then return it """
    for attempt in range(retry):
        results = function(*args)
        if test(results):
            return results
        else:
            time.sleep(wait)
    raise StandardError

def get_url_and_auth(url, db_name):
    """ Get the base url for the db and any authorization parameters

    Arguments:
    url -- the server url, may contain authorization data
    db_name -- the database name

    Note: Ripped from CMSCouch library

    """
    if url.find("@") != -1:
        endpoint_components = urlparse.urlparse(url)
        scheme = endpoint_components.scheme
        netloc = endpoint_components.netloc
        (auth, hostname) = netloc.split('@')
        url = '%s://%s' % (scheme, hostname)
        full_url = '%s/%s' % (url, db_name)
        return full_url, base64.encodestring(auth).strip()
    else:
        return '%s/%s' % (url, db_name), None

def create_userdoc(username, roles, password, salt):
    """ Create userdoc with hashed password using salt """
    return {
        "_id": "org.couchdb.user:" + username,
        "name": username,
        "type": "user",
        "roles": roles,
        "salt": salt,
        "password_sha": sha.sha(password + salt).hexdigest()
    }