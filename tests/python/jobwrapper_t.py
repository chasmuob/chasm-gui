""" jobwrapper_t.py

Unit tests for the job wrapper module.

"""

import json
import logging
import os
import shutil
import StringIO
import subprocess
import sys
import tarfile
import tempfile
import time
import unittest

from mock import MagicMock, Mock

from couch.Couch import CouchServer, Document, Database
from couch.CouchRequest import CouchError

from mossaic import jobwrapper
from mossaic.jobmanager import proxy_factory

from couchtest_helpers import upload_couchapp, upload_release, init_dbs, delete_dbs, results_on_precondition
from mossaictest_helpers import separate_auth

__COUCHAPP_DIR__ = os.path.join('..', '..', 'src', 'couchapps')
__EXPECTED_DIR__ = os.path.join('..', 'couchapps', 'mossaic', 'shows', 'expected')
__TEST_DATA__ = os.path.join('..', 'data')
__INPUT_FILES__ = os.path.join(__TEST_DATA__, 'input_files')
__OUTPUT_FILES__ = os.path.join(__TEST_DATA__, 'output_files')
__DUMMY_EXECUTABLES__ = os.path.join(__TEST_DATA__, 'dummy_executables')
__RELEASES__ = os.path.join(__TEST_DATA__, 'releases')
__QUORUM__ = {
    'q': os.getenv('QUORUM_Q'),
    'n': os.getenv('QUORUM_N'),
    'r': os.getenv('QUORUM_R'),
    'w': os.getenv('QUORUM_W')
}

def _get_jobwrapper(id, url, auth_file, dbs, https=False, keep=False,
        debug=False, http_retries=3):
    """ Helper method for test methods - creates and returns a job wrapper with options required for this test case

    Arguments:
    id -- The id of the job the JobWrapper will wrap

    """
    class Opts:
        pass
    opts = Opts()
    opts.job_id = id
    opts.couch_url = url
    if https:
        proxy = proxy_factory.get_proxy()
        opts.ssl_key = proxy['key']
        opts.ssl_cert = proxy['cert']
    opts.auth_file = auth_file
    opts.verbose = True
    opts.taskdb = dbs['task']['name']
    opts.datadb = dbs['data']['name']
    opts.releasedb = dbs['release']['name']
    opts.statedb = dbs['state']['name']
    logging.basicConfig(level=logging.WARNING)
    opts.logger = logging.getLogger('mossaic job wrapper')
    opts.keep = keep
    opts.noop = False
    opts.debug = debug
    opts.http_retries = http_retries
    opts.initial_backoff = "0"
    job = jobwrapper.JobWrapper(opts)
    return job

def _write_creds(auth, filename='auth'):
    creds_file = file(filename, 'w')
    creds_file.write('%s\n' % auth['username'])
    creds_file.write('%s\n' % auth['password'])
    creds_file.close()

class TestJobWrapper(unittest.TestCase):
    def setUp(self):
        """ Create couch instance and prepare """
        # Make an instance of the server
        self.url = os.getenv("COUCHURL", 'http://admin:pass@localhost:5984')
        url_https = os.getenv("COUCHURL_HTTPS", None)
        if (url_https):
            self.jobwrapper_url, self.auth = separate_auth(url_https)
            self.jobwrapper_https = True
        else:
            self.jobwrapper_url, self.auth = separate_auth(self.url)
            self.jobwrapper_https = False
        self.auth_file = 'creds'
        _write_creds(self.auth, filename=self.auth_file)
        self.server = CouchServer(self.url)
        testname = self.id().split('.')[-1]
        self.dbs = init_dbs(testname=testname, dbname_prefix='jobwrapper_unittest', server=self.server)

        self.input_filenames = {
            'geometry':'geometry', 'soils':'soils_deterministic', 'stability':'stability_bishop', 'boundary_conditions':'boundary_conditions_deterministic',
            'reinforcements':'reinforcements_both', 'vegetation':'vegetation_deterministic_evapotranspiration_canopy', 'stochastic_parameters':'stochastic_parameters',
            'initial_slope':'geometry_questa_soil_approximated', 'engineering_cost':'engineering_cost', 'road_network':'road_network'
        }

        self.__populate_db()
        self.maxDiff=None

    def __populate_db(self):
        """ Helper method for setUp - populates DB with data needed for these tests """
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'mossaic'), self.dbs['data']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'workflow'), self.dbs['task']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'state-machine'), self.dbs['state']['name'], self.url)
        input_files = [os.path.join(__INPUT_FILES__, '%s.json' % input_filename) for input_filename in self.input_filenames.values()]
        input_docs = dict((input_file, json.loads(file(input_file).read())) for input_file in input_files)
        for input_doc in input_docs.values():
            self.dbs['data']['db'].queue(input_doc)
        self.dbs['data']['db'].commit()

    def tearDown(self):
        os.unlink(self.auth_file)
        if sys.exc_info()[0] == None:
            delete_dbs(self.dbs, self.server)

    def __get_success_filename(self, file_type, filename):
        """ Convert the supplied file_type and filename to a path to the success data for this file

        Normally this is a straightforward mapping but for initial_slope we need to map to geometry, due to the fact that initial_slope is a geometry document
        too (albeit a Questa geometry document).

        Arguments:
        file_type -- The type of file we want to find the success data for
        filename -- The name of the file to be converted into the name of the success data file

        """
        file_type = {'initial_slope':'geometry'}.get(file_type, file_type)
        return os.path.join(__EXPECTED_DIR__, file_type, filename.replace(file_type, 'success'))

    def test_input_files_downloaded(self):
        """ Test that input files are downloaded by the job wrapper into the working directory """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'questa_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'questa_dummy.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read()))
        job = _get_jobwrapper('questa_dummy_job', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https)
        job.initialise_job()
        for file_type, filename in self.input_filenames.items():
            downloaded_doc = file(os.path.join(job.working_dir, filename)).read()
            expected_doc = file(self.__get_success_filename(file_type, filename)).read()
            self.assertEqual(downloaded_doc, expected_doc)
        job.clean_up()

    def test_input_file_not_found(self):
        """ Test that job wrapper fails if one input file cannot be found """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'questa_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'questa_dummy.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy_input_file_missing.json')).read()))
        job = _get_jobwrapper('questa_dummy_job_input_file_missing', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https)
        self.assertRaises(jobwrapper.JobWrapperError, job.initialise_job)
        job.clean_up()

    def test_input_file_wrong_file_type(self):
        """ Test that job wrapper fails if one input file is not the correct type """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'questa_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'questa_dummy.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy_wrong_file_type.json')).read()))
        job = _get_jobwrapper('questa_dummy_job_wrong_file_type', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https)
        self.assertRaises(jobwrapper.JobWrapperError, job.initialise_job)
        job.clean_up()

    def __test_results_generated(self, release_doc, executable, job_definition_doc, job_id, results_doc):
        """ Helper method for test_*_results_generated and test_*_results_generated_if_errors """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, release_doc), os.path.join(__DUMMY_EXECUTABLES__, executable))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, job_definition_doc)).read()))
        job = _get_jobwrapper(job_id, self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https)
        job()
        uploaded_results_id = self.dbs['data']['db'].loadView('mossaic', 'results', {'include_docs': True, 'key': job_id})['rows'][0]['doc']['_id']
        # Now get the doc by ID with r=__QUORUM__['n'] so we have the consistent (with attachments!) doc
        uploaded_results = self.dbs['data']['db'].document(uploaded_results_id, r=__QUORUM__['n'])
        uploaded_results.pop('_id')         # This value is autogenerated so not included in equality test so pop it
        uploaded_results.pop('_rev')        # Ditto
        uploaded_results.pop('_attachments') # And we don't care about attachments for this test so pop that too
        expected_results = json.loads(file(os.path.join(__OUTPUT_FILES__, results_doc)).read())
        expected_results.pop('_id')     # Won't match autogenerated _id so pop it
        self.assertEqual(uploaded_results, expected_results)

    def test_questa_results_generated(self):
        """ Test that, for questa jobs, a results document is generated and stored in CouchDB """
        self.__test_results_generated('questa_dummy.json', 'questa_dummy.sh', 'job_definition_questa_dummy.json', 'questa_dummy_job', 'results_questa.json')

    def test_questa_results_generated_multiple_files(self):
        """ Test that, for questa jobs which generate multiple .fos and .results files, a results document is generated and stored in CouchDB """
        self.__test_results_generated('questa_dummy.json', 'questa_dummy_multiple_files.sh', 'job_definition_questa_dummy.json', 'questa_dummy_job', 'results_questa_multiple_files.json')

    def test_chasm_results_generated(self):
        """ Test that, for chasm jobs, a results document is generated and stored in CouchDB """
        self.__test_results_generated('chasm_dummy.json', 'chasm_dummy.sh', 'job_definition_dummy.json', 'dummy_job', 'results_chasm.json')

    def test_questa_results_generated_if_bad_fos(self):
        """ Tests that questa output summary is still created even if chasm FOS file cannot be parsed """
        self.__test_results_generated('questa_dummy.json', 'questa_dummy_bad_fos.sh', 'job_definition_questa_dummy.json', 'questa_dummy_job', 'results_questa_bad_fos.json')

    def test_questa_results_generated_if_errors(self):
        """ Test that, for questa jobs, a results document is generated and stored in CouchDB for failed jobs """
        self.__test_results_generated('questa_dummy.json', 'questa_dummy_error.sh', 'job_definition_questa_dummy.json', 'questa_dummy_job',
                'results_questa_error.json')

    def test_chasm_results_generated_if_errors(self):
        """ Test that, for chasm jobs, a results document is generated and stored in CouchDB for failed jobs """
        self.__test_results_generated('chasm_dummy.json', 'chasm_dummy_error.sh', 'job_definition_dummy.json', 'dummy_job', 'results_chasm_error.json')

    def __get_expected_tar_data(self, job):
        """ Helper method - gets the expected tar data for a given job """
        os.unlink(os.path.join(job.working_dir, job.executable))    # The executable shouldn't be included

        tar_buffer = StringIO.StringIO()
        tar_file = tarfile.open('output_%s' % job.job_id, fileobj=tar_buffer, mode='w:gz')
        tar_file.add(job.working_dir, arcname=job.job_id)
        tar_index = tar_file.getnames()
        tar_file.close()
        return tar_index

    def __test_tarball(self, job, results_key='questa_dummy_job', exception=None):
        """ Helper method for the tarball tests """
        if exception:
         self.assertRaises(exception, job)
        else:
         job()
        expected_tar_data = self.__get_expected_tar_data(job)
        retry = 10
        for i in range(retry):
            try:
                output_id = results_on_precondition(function=self.dbs['data']['db'].loadView,
                        args=['mossaic', 'results', {'include_docs': True, 'key': results_key}],
                        test=lambda x:len(x['rows']) > 0, wait=2, retry=60)['rows'][0]['id']
            except Exception, e:
                if i == retry - 1:
                    raise e
                else:
                    time.sleep(30)
            
        uploaded_tar = self.dbs['data']['db'].getAttachment(output_id, name='output_%s.tar.gz' % job.job_id)
        uploaded_tar_data = tarfile.open('output', fileobj=StringIO.StringIO(uploaded_tar), mode='r:gz').getnames()
        self.assertEqual(uploaded_tar_data, expected_tar_data)
        job.keep_workdir = False
        job.clean_up()

    # Note: we use unicode strings for the job ids as they can cause problems with tar operations (string buffers in StringIO library...)
    def test_questa_output_tarball_generated(self):
        """ Test that the working directory is compressed and attached to the results document """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'questa_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'questa_dummy.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read()))
        job = _get_jobwrapper(u'questa_dummy_job', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https, keep=True)
        self.__test_tarball(job)

    def test_chasm_output_tarball_generated(self):
        """ Test that the working directory is compressed and attached to the results document """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'chasm_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'chasm_dummy.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_dummy.json')).read()))
        job = _get_jobwrapper(u'dummy_job', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https, keep=True)
        self.__test_tarball(job, results_key='dummy_job')

    def test_questa_output_tarball_generated_if_errors(self):
        """ Tests that the working directory is compressed and attached to the results document even if the executable messed up """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'questa_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'questa_dummy_error.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read()))
        job = _get_jobwrapper(u'questa_dummy_job', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https, keep=True)
        self.__test_tarball(job)

    def test_chasm_output_tarball_generated_if_errors(self):
        """ Test that the working directory is compressed and attached to the results document even if the executable messed up """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'chasm_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'chasm_dummy_error.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_dummy.json')).read()))
        job = _get_jobwrapper(u'dummy_job', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https, keep=True)
        self.__test_tarball(job, results_key='dummy_job')

    def test_questa_output_tarball_generated_if_too_many_outputs(self):
        """ Tests that the working directory is compressed and attached to the results document even if the executable produced too many outputs """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'questa_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__,'questa_dummy_too_many_outputs.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read()))
        job = _get_jobwrapper(u'questa_dummy_job', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https, keep=True)
        self.__test_tarball(job)

    def test_chasm_output_tarball_generated_if_too_many_outputs(self):
        """ Test that the working directory is compressed and attached to the results document even if the executable produced too many outputs """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'chasm_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'chasm_dummy_too_many_outputs.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_dummy.json')).read()))
        job = _get_jobwrapper(u'dummy_job', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https, keep=True)
        self.__test_tarball(job, results_key='dummy_job')

    def test_job_definition_file_not_found(self):
        """ Test that job wrapper fails if job definition file cannot be found """
        job = _get_jobwrapper('non-existent-job-definition', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https)
        self.assertRaises(jobwrapper.JobWrapperError, job.initialise_job)

    def test_executable_downloaded(self):
        """ Test that executable files are downloaded by the job wrapper into the working directory """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'questa_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'questa_dummy.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read()))
        job = _get_jobwrapper('questa_dummy_job', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https)
        job.initialise_job()
        downloaded_executable = file(os.path.join(job.working_dir, job.executable)).read()
        expected_executable = file(os.path.join(__DUMMY_EXECUTABLES__, 'questa_dummy.sh')).read()
        self.assertEqual(downloaded_executable, expected_executable)
        job.clean_up()

    def test_executable_runs(self):
        """ Tests that the executable file has been run by looking for expected output """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'questa_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'questa_dummy.sh'))
        output_files = ['output_summary.out', 'stdout', 'dummy.slip', 'dummy.results']
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read()))
        self.assertTrue(self.__test_executable_output(_get_jobwrapper('questa_dummy_job', self.jobwrapper_url, self.auth_file, self.dbs, https=self.jobwrapper_https), output_files))

    def test_executable_error(self):
        """ Tests that an executable that fails generates stderr and stdout """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'questa_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'questa_dummy_error.sh'))
        output_files = ['stdout', 'stderr']
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read()))
        self.assertTrue(self.__test_executable_output(_get_jobwrapper('questa_dummy_job', self.jobwrapper_url, self.auth_file, self.dbs, https=self.jobwrapper_https), output_files, jobwrapper.JobWrapperError, strip_err=True))

    def __test_executable_output(self, job, output_files, exception=None, strip_err=False):
        """ Helper method for test_executable_runs and test_executable_error - checks specified output files are equal after running the job

        Arguments:
        job -- JobWrapper object that runs the executable
        output_files -- List of output files to be tested for equality
        exception -- If specified, the type of exception that should be raised by the JobWrapper

        """
        job.initialise_job()
        if exception:
         self.assertRaises(exception, job.run_executable)
        else:
         job.run_executable()
        generated_output = [file(os.path.join(job.working_dir, output_file)).read().replace('\r', '') for output_file in output_files]
        expected_output = [file(os.path.join(__OUTPUT_FILES__, output_file)).read().replace('\r', '') for output_file in output_files]
        # Possibly strip last four lines of output (will be the output of time, sent to stderr)
        if strip_err:
            err_index = output_files.index('stderr')
            generated_output[err_index] = '\n'.join(generated_output[err_index].split('\n')[:-4]) + '\n'
        job.clean_up()
        return set(generated_output) == set(expected_output)

    def __test_steering_file_created(self, release_doc, executable, job_definition_doc, job_id):
        """ Helper for test_*_steering_file_created methods """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, release_doc), os.path.join(__DUMMY_EXECUTABLES__, executable))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, job_definition_doc)).read()))
        job = _get_jobwrapper(job_id, self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https, keep=True)
        job()
        created_steering_doc = file(os.path.join(job.working_dir, '0.steering')).read()
        expected_steering_doc = self.dbs['task']['db'].loadShow('workflow', 'steering', job_id)
        self.assertEqual(created_steering_doc, expected_steering_doc)
        job.keep_workdir = False
        job.clean_up()

    def test_questa_steering_file_created(self):
        """ Tests that the steering file was created by the job wrapper """
        self.__test_steering_file_created('questa_dummy.json', 'questa_dummy.sh', 'job_definition_questa_dummy.json', 'questa_dummy_job')

    def test_chasm_steering_file_created(self): #TODO dup
        """ Tests that the steering file was created by the job wrapper """
        self.__test_steering_file_created('chasm_dummy.json', 'chasm_dummy.sh', 'job_definition_dummy.json', 'dummy_job')

    def test_steering_file_passed_to_executable(self):
        """ Tests that the steering file id is passed to the executable by the job wrapper """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'questa_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'questa_dummy_argcheck.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read()))
        self.dbs['state']['db'].commitOne(self.__get_fake_state_doc('questa_dummy_job'), timestamp=True)
        job = _get_jobwrapper('questa_dummy_job', self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https)
        self.assertEqual(job(), jobwrapper.ExitStatus.OK)

    def __get_fake_state_doc(self, job_id):
        """ Generate a fake state doc with specified job id, in state required for job wrapper test ('submitted') """
        return {
            'job_id': job_id,
            'state': 'submitted',
            'state_history': {
               "ready_to_submit": {
                   "timestamp": 1300188082,
                   "modified_by": "Task"
               },
               "splitting": {
                   "timestamp": 1300187895,
                   "modified_by": "Task"
               },
               "submitted": {
                   "timestamp": 1300188202,
                   "modified_by": "Job"
               }
            },
            'task_id': 'whatever'
        }

    def __test_job_state_helper(self, release_doc, executable, job_definition, expected_state, job_id="dummy_job"):
        """ Helper for job state tests """
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, release_doc), os.path.join(__DUMMY_EXECUTABLES__, executable))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, job_definition)).read()), w=__QUORUM__['n'])
        self.dbs['state']['db'].commitOne(self.__get_fake_state_doc(job_id), w=__QUORUM__['n'], timestamp=True)
        job = _get_jobwrapper(job_id, self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https)
        job()
        job_doc = self.dbs['task']['db'].document(job_id, r=__QUORUM__['n'])
        self.assertEqual(job_doc['state'], expected_state)
        self.assertTrue('state_history' in job_doc and expected_state in job_doc['state_history'])
        state_doc = self.dbs['state']['db'].loadView('state-machine', 'states_by_job', {'include_docs': True, 'key': job_id})['rows'][0]['doc']
        self.assertEqual(state_doc['state'], expected_state)
        self.assertTrue(expected_state in state_doc['state_history'])
        self.assertEqual(state_doc['state_history'][expected_state]['modified_by'], 'JobWrapper')

    def test_job_state_updated_on_success(self):
        """ Tests that the job definition document and job state document are updated with state 'done' """
        self.__test_job_state_helper(release_doc='chasm_dummy.json', executable='chasm_dummy.sh',
                job_definition='job_definition_dummy.json', expected_state='done')

    def test_job_state_updated_on_fail(self):
        """ Tests that job definition document and job state document are updated with state 'failed' """
        self.__test_job_state_helper(release_doc='chasm_dummy.json', executable='chasm_dummy_error.sh',
                    job_definition='job_definition_dummy.json', expected_state='failed')

    def test_job_state_updated_on_init_fail(self):
        """ Tests that job definition document and job state document are updated with state 'failed' if input can't be downloaded """
        self.__test_job_state_helper(release_doc='questa_dummy.json', executable='questa_dummy.sh',
                    job_definition='job_definition_questa_dummy_input_file_missing.json', expected_state='failed',
                    job_id="questa_dummy_job_input_file_missing")

    def test_job_definition_updated_with_runtime(self):
        """ Test that the job definition document is updated with the job run time when the job completes """
        job_id = 'dummy_job'
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'chasm_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'chasm_dummy.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_dummy.json')).read()), w=__QUORUM__['n'])
        self.dbs['state']['db'].commitOne(self.__get_fake_state_doc(job_id), w=__QUORUM__['n'], timestamp=True)
        job = _get_jobwrapper(job_id, self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https)
        job()
        job_doc = self.dbs['task']['db'].document(job_id, r=__QUORUM__['n'])
        self.assertTrue('stats' in job_doc)
        self.assertTrue('runtime' in job_doc['stats'])
        self.assertTrue(type(job_doc['stats']['runtime']) == float)

    def test_job_definition_updated_on_init(self):
        """ Test that the job definition document is updated with the job start time when the job is initialised """
        job_id = 'dummy_job'
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'chasm_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'chasm_dummy.sh'))
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_dummy.json')).read()), w=__QUORUM__['n'])
        self.dbs['state']['db'].commitOne(self.__get_fake_state_doc(job_id), w=__QUORUM__['n'], timestamp=True)
        job = _get_jobwrapper(job_id, self.jobwrapper_url, self.auth_file, dbs=self.dbs, https=self.jobwrapper_https)
        job.initialise_job()
        job_doc = self.dbs['task']['db'].document(job_id, r=__QUORUM__['n'])
        self.assertTrue('stats' in job_doc)
        self.assertTrue('start_time' in job_doc['stats'])
        self.assertTrue(type(job_doc['stats']['start_time']) == float)

class TestJobWrapperWithMockServer(unittest.TestCase):
    """ Job wrapper tests that require a mock CouchServer and CouchDatabase """
    def setUp(self):
        url = os.getenv("COUCHURL", 'http://admin:pass@localhost:5984')
        self.url_with_auth = url
        self.url, self.auth = separate_auth(url)
        self.auth_file = 'creds'
        _write_creds(self.auth, filename=self.auth_file)
        testname = self.id().split('.')[-1]
        self.dbname = 'jobwrapper_unittest_%s' % testname.lower()
        self.mock_db = MagicMock()
        self.mock_db.document = MagicMock()
        dbs = {}
        for dbtype in ('task', 'data', 'release', 'state'):
            dbs[dbtype] = {'name': self.dbname, 'db': self.mock_db}
        self.job = _get_jobwrapper('job_definition', self.url, self.auth_file, dbs, http_retries=3)
        self.job.databases['task'] = self.mock_db
        self.job.databases['data'] = self.mock_db
        self.job.databases['state'] = self.mock_db
        release_db = MagicMock()
        release_db.getAttachment = MagicMock()
        release_db.getAttachment.return_value = "dummy executable"
        release_db.document = MagicMock()
        release_db.document.return_value = {
            "_id":"questa_4.21",
            "_attachments":{
                "executable":{
                    "digest":"md5-UXqoUhrpvz8Wuq/Y01TWSQ==",
                }
            }
        }
        self.job.databases['releases'] = release_db

    def tearDown(self):
        """ Although we are using mocks, the job wrapper still creates the database when it is initiated, so we need to delete it """
        os.unlink('creds')
        if sys.exc_info()[0] == None:
            # This test has passed, clean up after it
            testname = self.id().split('.')[-1]
            dbname = 'jobwrapper_unittest_%s' % testname.lower()
            CouchServer(os.getenv("COUCHURL", self.url_with_auth)).deleteDatabase(dbname)
            try:
                shutil.rmtree(self.job.working_dir)
            except (OSError, AttributeError):
                pass    # Meh

    def __jobwrapper_fake_init(self, job_definition, geo_id="geometry_questa_soil_approximated"):
        """ Helper for parameter tests - do a fake initialisation (missing out bits that won't work with mock DB) """
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, geo_id)
        self.mock_db.loadShow.side_effect = self.__makeLoadShowReturn({"count": 0}, "show data", "b4ba0d0cadfd0c83b9dd2c49d1795a8b1bf591c7");
        self.job.get_job_definition()
        self.job.__setattr__('plugin', jobwrapper._get_plugin(None, self.job.definition['executable']['application']))
        self.job.__setattr__('working_dir', tempfile.mkdtemp(dir=os.getcwd()))
        self.job.download_input()

    def test_with_deterministic_parameters_job(self):
        """ Tests that jobwrapper sends an appropriate HTTP request if the job definition doc specifies deterministic parameters """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_parameters_deterministic.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry")
        self.job.job_id = job_definition['_id']
        job_definition['input_files'] = {"geometry": "geometry"} # Just include a geometry file, so we know that will be the last called
        self.__jobwrapper_fake_init(job_definition, "geometry")
        self.mock_db.loadShow.assert_called_with('mossaic', 'geometry', 'geometry', {
                'substitutions': {
                    '/geometry/slope_sections/upslope/1/angle/value': 55,
                    '/geometry/slope_sections/upslope/0/angle/value': 35
                },
                'sha1': True
            })

    def test_with_stochastic_parameters_job(self):
        """ Tests that jobwrapper sends an appropriate HTTP request if the job definition doc specifies stochastic parameters """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_parameters_stochastic.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry")
        self.job.job_id = job_definition['_id']
        job_definition['input_files'] = {"geometry": "geometry", "soils": "soils_deterministic"} # Soils file should be the last called by download_inputs
        self.__jobwrapper_fake_init(job_definition, "geometry")
        self.mock_db.loadShow.assert_called_with('mossaic', 'soils', 'soils_deterministic', {
                'substitutions': {
                    '/soils/0/Ksat/value': 1.05e-06,
                    '/soils/0/saturated_moisture_content/value': 0.431
                },
                'sha1': True
            })

    def test_with_stochastic_parameters_job_geometry(self):
        """ Tests that jobwrapper sends an appropriate HTTP request if the job definition doc specifies stochastic parameters for geometry """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_parameters_stochastic.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry")
        self.job.job_id = job_definition['_id']
        # QUESTA geometry files are stored under the initial_slope key
        # JobWrapper should know that this translates to geometry
        job_definition['input_files'] = {"initial_slope": "geometry"} # Just include a geometry file, so we know that will be the last called
        self.__jobwrapper_fake_init(job_definition, "geometry")
        self.mock_db.loadShow.assert_called_with('mossaic', 'geometry', 'geometry', {
                'substitutions': {
                    "/geometry/soil_strata/interfaces/depth/value": [1.495, 10.01],
                    "/geometry/soil_strata/interfaces/angle/value": [47.985, 45.01]
                },
                'sha1': True
            })

    def test_job_definition_bad_id(self):
        """ Tests that jobwrapper attempts to retry three times if the _id of the job definition doc does not match the job id """
        self.mock_db.document.return_value = {'_id': 'not the expected job id'}
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.mock_db.document.call_count, 4)

    def test_job_definition_service_unavailable(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 503 Service Unavailable response on job definition download """
        self.mock_db.document.side_effect = CouchError(None, None, None, status=503)
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.mock_db.document.call_count, 4)

    def test_job_definition_doc_not_found(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 404 Not Found response on job definition download """
        self.mock_db.document.side_effect = CouchError(None, None, None, status=404)
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.mock_db.document.call_count, 4)

    def test_job_definition_internal_server_error(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 500 error response on job definition download """
        self.mock_db.document.side_effect = CouchError(None, None, None, status=500)
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.mock_db.document.call_count, 4)

    def __makeLoadShowReturn(self, counter, data, checksum):
        def loadShowReturn(*args):
            if counter["count"] % 2 == 0:
                counter["count"] += 1;
                return data
            else:
                counter["count"] += 1;
                return checksum
        return loadShowReturn;

    def __makeDocumentReturn(self, counter, job_definition, geo_id="geometry_questa_soil_approximated"):
        def documentReturn(*args):
            if counter["count"] == 0:
                counter["count"] += 1
                return job_definition
            else:
                return {
                    "_id": geo_id
                }
        return documentReturn

    def test_input_files_sha1(self):
        """ Tests that the jobwrapper validates a downloaded input file against sha1 of _show function """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = 'fake executable'
        self.mock_db.ecCommitOne.return_value = [{'rev': 'dummy_rev_value'}]
        self.mock_db.loadShow.side_effect = self.__makeLoadShowReturn({"count": 0}, "show data", "b4ba0d0cadfd0c83b9dd2c49d1795a8b1bf591c7");
        self.job.initialise_job()
        self.assertEqual(self.mock_db.loadShow.call_count, 22)

    def test_input_files_sha1_no_match(self):
        """ Tests that the jobwrapper retries three times if the sha1 of a downloaded input file does not match the sha1 of _show function """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = 'fake executable'
        self.mock_db.ecCommitOne.return_value = [{'rev': 'dummy_rev_value'}]
        self.mock_db.loadShow.side_effect = self.__makeLoadShowReturn({"count": 0}, "show data", "not_actually_a_valid_sha_of_the_mock_show_data");
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.mock_db.loadShow.call_count, 8)

    def test_input_files_service_unavailable(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 503 Service Unavailable response on input file download """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = 'fake executable'
        self.mock_db.loadShow.side_effect = CouchError(None, None, None, status=503)
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.mock_db.loadShow.call_count, 4)
        self.job.clean_up()

    def test_input_files_doc_not_found(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 404 Not Found response on input file download """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = 'fake executable'
        self.mock_db.loadShow.side_effect = CouchError(None, None, None, status=404)
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.mock_db.loadShow.call_count, 4)
        self.job.clean_up()

    def test_input_files_internal_server_error(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 500 error response on input file download """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = 'fake executable'
        self.mock_db.loadShow.side_effect = CouchError(None, None, None, status=500)
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.mock_db.loadShow.call_count, 4)
        self.job.clean_up()

    def test_download_executable_bad_md5(self):
        """ Tests that jobwrapper attempts to retry three times if the md5 of the executable does not match that held in the release doc """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.job.databases['releases'].document.return_value = {
            "_id":"questa_4.21",
            "_attachments":{
                "executable":{
                    "digest":"md5-bogus-md5-bogus==",
                }
            }
        }
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.job.databases['releases'].getAttachment.call_count, 4)
        self.assertEqual(self.job.databases['releases'].document.call_count, 4)
        self.job.clean_up()

    def test_download_executable_service_unavailable(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 503 Service Unavailable response on executable download """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.job.databases['releases'].getAttachment.side_effect = CouchError(None, None, None, status=503)
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.job.databases['releases'].getAttachment.call_count, 4)
        self.job.clean_up()

    def test_download_executable_doc_not_found(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 404 not found response on executable download """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.job.databases['releases'].getAttachment.side_effect = CouchError(None, None, None, status=404)
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.job.databases['releases'].getAttachment.call_count, 4)
        self.job.clean_up()

    def test_download_executable_internal_server_error(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 500 error response on executable download """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.job.databases['releases'].getAttachment.side_effect = CouchError(None, None, None, status=500)
        self.assertRaises(jobwrapper.JobWrapperError, self.job.initialise_job)
        self.assertEqual(self.job.databases['releases'].getAttachment.call_count, 4)
        self.job.clean_up()

    def test_upload_output_files_service_unavailable(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 503 Service Unavailable response on output file upload """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = '#!/bin/bash\necho foo > output_summary.out'
        self.mock_db.loadShow.side_effect = self.__makeLoadShowReturn({"count": 0}, "show data", "b4ba0d0cadfd0c83b9dd2c49d1795a8b1bf591c7");
        self.mock_db.ecCommitOne.side_effect = CouchError(None, None, None, status=503)
        self.assertEquals(self.job(), jobwrapper.ExitStatus.CRITICAL)
        self.assertEqual(self.mock_db.ecCommitOne.call_count, 4)

    def test_upload_output_files_service_internal_server_error(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 500 error response on output file upload """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = '#!/bin/bash\necho foo > output_summary.out'
        self.mock_db.loadShow.side_effect = self.__makeLoadShowReturn({"count": 0}, "show data", "b4ba0d0cadfd0c83b9dd2c49d1795a8b1bf591c7");
        self.mock_db.ecCommitOne.side_effect = CouchError(None, None, None, status=500)
        self.assertEquals(self.job(), jobwrapper.ExitStatus.CRITICAL)
        self.assertEqual(self.mock_db.ecCommitOne.call_count, 4)

    def test_upload_output_tar_service_unavailable(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 503 Service Unavailable reponse on output tar attachment """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = '#!/bin/bash\necho foo > output_summary.out'
        self.mock_db.loadShow.side_effect = self.__makeLoadShowReturn({"count": 0}, "show data", "b4ba0d0cadfd0c83b9dd2c49d1795a8b1bf591c7");
        self.mock_db.ecCommitOne.return_value = [{"id": "some_id", "rev": "some_rev"}]
        self.mock_db.addAttachment.side_effect = CouchError(None, None, None, status=503)
        self.assertEquals(self.job(), jobwrapper.ExitStatus.CRITICAL)
        self.assertEqual(self.mock_db.addAttachment.call_count, 4)

    def test_upload_output_tar_doc_not_found(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 404 not found reponse on output tar attachment """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = '#!/bin/bash\necho foo > output_summary.out'
        self.mock_db.loadShow.side_effect = self.__makeLoadShowReturn({"count": 0}, "show data", "b4ba0d0cadfd0c83b9dd2c49d1795a8b1bf591c7");
        self.mock_db.ecCommitOne.return_value = [{"id": "some_id", "rev": "some_rev"}]
        self.mock_db.addAttachment.side_effect = CouchError(None, None, None, status=404)
        self.assertEquals(self.job(), jobwrapper.ExitStatus.CRITICAL)
        self.assertEqual(self.mock_db.addAttachment.call_count, 4)

    def test_upload_output_tar_doc_internal_server_error(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 500 error reponse on output tar attachment """
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.side_effect = self.__makeDocumentReturn({"count": 0}, job_definition, "geometry_questa_soil_approximated")
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = '#!/bin/bash\necho foo > output_summary.out'
        self.mock_db.loadShow.side_effect = self.__makeLoadShowReturn({"count": 0}, "show data", "b4ba0d0cadfd0c83b9dd2c49d1795a8b1bf591c7");
        self.mock_db.ecCommitOne.return_value = [{"id": "some_id", "rev": "some_rev"}]
        self.mock_db.addAttachment.side_effect = CouchError(None, None, None, status=500)
        self.assertEquals(self.job(), jobwrapper.ExitStatus.CRITICAL)
        self.assertEqual(self.mock_db.addAttachment.call_count, 4)

    def test_create_steering_file_service_unavailable(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 503 Service Unavailable reponse on steering file download """
        self.mock_db.loadShow.side_effect = CouchError(None, None, None, status=503)
        self.job.definition = {'executable': {'application': 'chasm'}}  # Bare minimum dummy definition data needed
        self.job.__setattr__('plugin', Mock())
        self.assertRaises(CouchError, self.job.create_steering_file)
        self.assertEqual(self.mock_db.loadShow.call_count, 4)

    def _get_mock_db_state(self):
        """ Helper function - returns a mock state DB with required side-effects to drive the jobwrapper """
        mock_db_state = MagicMock()
        state_results = MagicMock()
        state_history = MagicMock()
        state_history.__setitem__.side_effect = None
        doc = MagicMock()
        doc.__setitem__.side_effect = None
        doc.__getitem__.side_effect = state_history
        rows = MagicMock()
        rows.__len__.side_effect = lambda :1
        rows.__getitem__.side_effect = lambda x:{'doc': doc}
        state_results.__getitem__.side_effect = lambda x:rows
        mock_db_state.loadView.side_effect = lambda x, y, z:state_results
        return mock_db_state

    def test_close_job_state_service_unavailable(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 503 Service Unavailable reponse when closing job state """
        mock_db_state = self._get_mock_db_state()
        self.job.databases['state'] = mock_db_state
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        self.mock_db.document.return_value = job_definition
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = '#!/bin/bash\necho foo > output_summary.out'
        self.mock_db.loadShow.return_value = 'fake_document'
        self.mock_db.ecCommitOne.return_value = [{"id": "some_id", "rev": "some_rev"}]
        mock_db_state.ecCommitOne.side_effect = CouchError(None, None, None, status=503)
        self.assertEquals(self.job(), jobwrapper.ExitStatus.CRITICAL)
        self.assertEqual(mock_db_state.ecCommitOne.call_count, 4)

    def test_close_job_service_unavailable(self):
        """ Tests that jobwrapper attempts to retry three times if we get a 503 Service Unavailable reponse when closing job """
        mock_db_state = self._get_mock_db_state()
        mock_db_task = MagicMock()
        mock_db_task.document = MagicMock()
        self.job.databases['state'] = mock_db_state
        self.job.databases['task'] = mock_db_task
        job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_questa_dummy.json')).read())
        mock_db_task.document.return_value = job_definition
        self.job.job_id = job_definition['_id']
        self.mock_db.getAttachment.return_value = '#!/bin/bash\necho foo > output_summary.out'
        self.mock_db.loadShow.return_value = 'fake_document'
        self.mock_db.ecCommitOne.return_value = [{"id": "some_id", "rev": "some_rev"}]
        mock_db_task.ecCommitOne.side_effect = CouchError(None, None, None, status=503)
        self.assertEquals(self.job(), jobwrapper.ExitStatus.CRITICAL)
        self.assertEqual(mock_db_task.ecCommitOne.call_count, 4)


if __name__ == '__main__':
    unittest.main(verbosity=2)