""" mossaictest_helpers.py

Helpers for mossaic tests

"""

import urlparse

def separate_auth(url):
    if (url is None):
        return None, None
    parsed = urlparse.urlparse(url)
    stripped_url = '%s://%s:%s' % (parsed.scheme, parsed.hostname, parsed.port)
    auth = {
        'username': parsed.username,
        'password': parsed.password
    }
    return stripped_url, auth