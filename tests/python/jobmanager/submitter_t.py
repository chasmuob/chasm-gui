""" submitter_t.py

Unit tests for job submitters.

These tests use mock objects. There are additional tests in jobmanager_t.py which use real backends to test the submit/results cycle.

Each job submitter is given a mock object representing the backend.

 SubmitterLocal -> Process backend
 SubmitterDirac -> Dirac backend

"""

import os
import unittest

from collections import namedtuple

from mock import MagicMock, Mock

import mossaic.jobmanager.submitter as submitter

from jobmanagertest_helpers import write_submitter_config
from mossaictest_helpers import separate_auth

def _reload_job_submitter():
    """ This is only called from within tests once the .ini file has been written """
    reload(submitter)
    submitter.TEST = True


class TestSubmitter(unittest.TestCase):
    """ Superclass for job submitter tests """
    def setUp(self):
        url = os.getenv("COUCHURL", 'http://admin:pass@localhost:5984')
        self.url = url
        self.auth_filename = 'jobwrapper_auth'
        testname = self.id().split('.')[-1]
        # We don't need to create a database for these tests, but we need names
        self.dbname_base = 'submitter_unittest_%s' % testname.lower()
        self.dbs = {}
        for dbtype in ('task', 'data', 'state', 'release'):
            dbname = '_'.join((self.dbname_base, dbtype))
            self.dbs[dbtype] = {'name': dbname}

    def tearDown(self):
        os.unlink('submitter.ini')
        os.unlink('jobwrapper_auth')


class TestSubmitterLocal(TestSubmitter):
    """ Test the local job submitter using a mock object to represent the Process backend """
    def setUp(self):
        super(TestSubmitterLocal, self).setUp()
        write_submitter_config(self.url, auth_file=self.auth_filename, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'],
                    data_db=self.dbs['data']['name'], release_db=self.dbs['release']['name'], backend='local', http_retries=3)
        # Import job submitter once we have written the config file
        _reload_job_submitter()

    def test_submit_success(self):
        """ Test successful job submission to local backend """
        mock_process_instance = Mock()
        MockProcess = Mock(return_value=mock_process_instance)
        job_submitter = submitter.get_submitter({'backend': MockProcess})
        result = job_submitter.submit(job_id='foo')
        self.assertEquals(result, submitter.SubmitterResponse.OK)
        MockProcess.assert_called_once_with(target=job_submitter._SubmitterLocal__submit_and_track, args=[job_submitter.job])
        mock_process_instance.start.assert_called_once_with()

    def test_submit_fail_bad_job(self):
        """ Test failed job submission to local backend due to bad job """
        write_submitter_config(self.url, auth_file=self.auth_filename, task_db='_!)()!@(JFOImo)', state_db=self.dbs['state']['name'],
                    data_db=self.dbs['data']['name'], release_db=self.dbs['release']['name'], backend='local', http_retries=3)
        # Import job submitter once we have written the config file
        _reload_job_submitter()
        mock_process_instance = Mock()
        MockProcess = Mock(return_value=mock_process_instance)
        job_submitter = submitter.get_submitter({'backend': MockProcess})
        result = job_submitter.submit(job_id=None)
        self.assertEqual(result, submitter.SubmitterResponse.ERROR_JOB)

    def test_submit_fail_backend_error(self):
        """ Test failed job submission to local backend due to backend error """
        mock_process_instance = Mock()
        def exception():
            raise Exception
        mock_process_instance.start.side_effect = lambda: exception()
        MockProcess = Mock(return_value=mock_process_instance)
        job_submitter = submitter.get_submitter({'backend': MockProcess})
        result = job_submitter.submit(job_id='foo')
        self.assertEqual(result, submitter.SubmitterResponse.ERROR_BACKEND)


class TestSubmitterDirac(TestSubmitter):
    """ Test the dirac job submitter using a mock object to represent the DIRAC API """
    def setUp(self):
        super(TestSubmitterDirac, self).setUp()
        write_submitter_config(self.url, auth_file=self.auth_filename, data_db=self.dbs['data']['name'], task_db=self.dbs['task']['name'], release_db=self.dbs['release']['name'],
                state_db=self.dbs['state']['name'], backend='dirac', http_retries=3)
        _reload_job_submitter()

    def _get_mock_dirac_job(self, fail=False):
        """ Helper """
        mock_dirac_job = Mock()
        mock_dirac_job.setName.return_value = {'OK': True}
        mock_dirac_job.setOwner.return_value = {'OK': True}
        if fail:
            mock_dirac_job.setInputSandbox.return_value = {'OK': False, 'Message': 'Bad things'}
        else:
            mock_dirac_job.setInputSandbox.return_value = {'OK': True}
        mock_dirac_job.setExecutable.return_value = {'OK': True}
        return mock_dirac_job

    def test_submit_success(self):
        """ Test successful job submission via DIRAC """
        mock_dirac_job = self._get_mock_dirac_job(fail=False)
        mock_dirac = Mock()
        mock_dirac.submit.return_value = {'OK': True}
        mock_backend = namedtuple('Backend', 'Dirac, Job')(Mock(return_value=mock_dirac), Mock(return_value=mock_dirac_job))
        job_submitter = submitter.get_submitter({'backend': mock_backend})
        result = job_submitter.submit(job_id='foo')
        self.assertEquals(result, submitter.SubmitterResponse.OK)
        mock_backend.Job.assert_called_once_with()
        mock_dirac_job.setName.assert_called_once_with('foo')
        mock_dirac_job.setOwner.assert_called_once_with('test')
        mock_dirac_job.setInputSandbox.assert_called_once_with(['../../../src/python/mossaic/jobmanager/scripts/start_job.sh',
                '../../../src/python/mossaic/jobwrapper.py', '../../../src/python/mossaic/mossaicparser.py', '../../../src/python/couch/Couch.py',
                '../../../src/python/couch/CouchRequest.py', '../../grid_test_helpers/httplib2/__init__.py',
                '../../grid_test_helpers/httplib2/iri2uri.py', '../../../src/python/mossaic/version.py', '../../../config/jobwrapper_auth',
                '../../../src/python/simplejson/__init__.simplejson.py', '../../../src/python/simplejson/decoder.py',
                '../../../src/python/simplejson/encoder.py', '../../../src/python/simplejson/ordered_dict.py',
                '../../../src/python/simplejson/scanner.py', '../../../src/python/simplejson/tool.py'])
        expected_url = separate_auth(self.url)[0]
        mock_dirac_job.setExecutable.assert_called_once_with('start_job.sh',
                str('--job=%s --url=%s --data-db=%s --task-db=%s --release-db=%s --state-db=%s --http-retries=%s --auth-file=%s --initial-backoff-window=%s' %
                        ('foo', expected_url, self.dbs['data']['name'], self.dbs['task']['name'], self.dbs['release']['name'], self.dbs['state']['name'], 3,
                        'jobwrapper_auth', '0')))
        mock_backend.Dirac.assert_called_once_with()
        mock_dirac.submit.assert_called_once_with(job_submitter.job, mode='local')

    def test_submit_fail_bad_job(self):
        """ Test failed job submission to dirac backend due to bad job """
        mock_dirac_job = self._get_mock_dirac_job(fail=True)
        mock_dirac = Mock()
        mock_dirac.submit.return_value = {'OK': True}
        mock_backend = namedtuple('Backend', 'Dirac, Job')(Mock(return_value=mock_dirac), Mock(return_value=mock_dirac_job))
        job_submitter = submitter.get_submitter({'backend': mock_backend})
        result = job_submitter.submit(job_id='foo')
        self.assertEqual(result, submitter.SubmitterResponse.ERROR_JOB)

    def test_submit_fail_backend_error(self):
        """ Test failed job submission to dirac backend due to backend error """
        mock_dirac_job = self._get_mock_dirac_job(fail=False)
        mock_dirac = Mock()
        mock_dirac.submit.return_value = {'OK': False, 'Message': 'Bad things'}
        mock_backend = namedtuple('Backend', 'Dirac, Job')(Mock(return_value=mock_dirac), Mock(return_value=mock_dirac_job))
        job_submitter = submitter.get_submitter({'backend': mock_backend})
        result = job_submitter.submit(job_id='foo')
        self.assertEqual(result, submitter.SubmitterResponse.ERROR_BACKEND)

if __name__ == '__main__':
    unittest.main(verbosity=2)