""" jobmanagertest_helpers.py

A small library of unrelated functions which are shared by tests involving the job manager

"""

from mossaictest_helpers import separate_auth

def write_jobmanager_config(url, task_db='mossaic_task', state_db='mossaic_state', data_db=None, task=True, job=True, input=True, tracker=False,
        cycle=2, retries=3, job_limit=None, total_job_limit=None, workers=5):
    """ Write config file for job sentinels """
    config_data = ['[master]\nurl = %s' % url]
    sentinel_data = 'task_db = %s\nstate_db = %s\nworkers = %s\nmax_work = 20000\ncycle = %s\nretries = %s' % (task_db, state_db, workers, cycle, retries)
    if task:
        config_data += ['[task]']
        config_data += [sentinel_data]
        if job_limit:
            config_data += ['job_limit = %s' % (job_limit)]
        if data_db:
            config_data += ['data_db = %s' % (data_db)]
    if job:
        config_data += ['[job]']
        config_data += [sentinel_data]
        if total_job_limit:
            config_data += ['job_limit = %s' % (total_job_limit)]
    if tracker:
        config_data += ['[tracker]']
        config_data += [sentinel_data]
    if input:
        config_data += ['[input]']
        config_data += [sentinel_data]
        if data_db:
            config_data += ['data_db = %s' % (data_db)]
    with file('jobmanager.ini', 'w') as config_file:
        config_file.write('\n'.join(config_data))

def write_submitter_config(url, data_db='mossaic', task_db='mossaic_task', state_db='mossaic_state', release_db='mossaic_release', backend='local',
        owner='test', http_retries=3, auth_file=None, initial_backoff_window="0"):
    """ Write config file for job submitter """
    jobwrapper_url = url
    if (auth_file):
        jobwrapper_url, auth = separate_auth(url)
        with file(auth_file, 'w') as auth_output:
            auth_output.write('%s\n%s' % (auth['username'], auth['password']))
    config_data = '[submitter]\nurl = %s\ndata_db = %s\nrelease_db = %s\ntask_db = %s\nstate_db = %s\ntype = %s\nowner = %s\njobwrapper_http_retries = %s\ninitial_backoff_window = %s' %\
            (jobwrapper_url, data_db, release_db, task_db, state_db, backend, owner, http_retries, initial_backoff_window)
    if (auth_file):
        config_data += '\njobwrapper_auth_file = %s' % (auth_file)
    with file('submitter.ini', 'w') as config_file:
        config_file.write(config_data)