""" sentinel_t.py

Unit tests for sentinels.

These test specific function of the sentinels, generally using mocks. Full functionality covered by jobmanager_t tests.

"""

import unittest

from mock import Mock

from couch.CouchRequest import CouchError
from mossaic.jobmanager import sentinel

class TestSentinel(unittest.TestCase):
    """ Main sentinel unit tests """
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def __get_couch_server(self, couch_server=None):
        """ Helper method which prepares mocks, swaps them into the sentinel module and returns the couch server """
        couch = couch_server
        if couch == None:
            couch = Mock()
            couch.connectDatabase.side_effect = Exception('Things went wrong')
        sentinel.CouchServer = Mock()
        sentinel.CouchServer.return_value = couch
        return couch

    def test_retry_on_connectDatabase_fail(self):
        """ Test required number of retries happen if database connection fails """
        couch_server = self.__get_couch_server()
        # Using self.assertRaises does not work here - the call count of connectDatabase isn't correct
        try:
            sentinel.Sentinel('foo', 'foo', 'foo', Mock(), Mock(), retries=8)
        except:
            pass
        self.assertEquals(couch_server.connectDatabase.call_count, 9)

    def test_log_message_and_quit_on_connectDatabase_fail(self):
        """ Test when connectDatabase fails (after retry) we get appropriate log message and sentinel dies """
        couch_server = self.__get_couch_server()
        logger = Mock()
        sentinel.Sentinel('foo', 'foo', 'foo', Mock(), logger, retries=8)
        print logger.error.call_args
        error_msg = logger.error.call_args[0][0]
        self.assertTrue(error_msg.startswith('Could not connect to CouchDB server at: foo'))

    def __get_work_fail_helper(self, exception_to_throw, logger_test):
        """ Helper for test_get_work_ methods """
        couch = Mock()
        db = Mock()
        db.loadView.side_effect = exception_to_throw
        couch.connectDatabase = Mock()
        couch.connectDatabase.return_value = db
        couch_server = self.__get_couch_server(couch)
        logger = Mock()
        s = sentinel.Sentinel('foo', 'foo', 'foo', 'task', logger, retries=2, cycle=1)
        s.start()
        logger_test(logger)

    def test_get_work_fail(self):
        """ Test that failed calls to get work are handled gracefully """
        def logger_test(logger):
            self.assertEqual(logger.exception.call_count, 3)
            error_msg = logger.exception.call_args[0][0]
            self.assertTrue(error_msg.startswith('Could not get work'))
        self.__get_work_fail_helper(Exception('Crazy stuff happening right now'), logger_test)

    def test_get_work_fail_couch_error(self):
        """ Test that failed calls to get work due to CouchError are handled gracefully """
        def logger_test(logger):
            self.assertEqual(logger.warning.call_count, 3)
            error_msg = logger.warning.call_args[0][0]
            self.assertTrue(error_msg.startswith('Could not get work'))
        self.__get_work_fail_helper(CouchError('Crazy stuff happening right now', 'wat', 'wat'), logger_test)

if __name__ == '__main__':
    unittest.main(verbosity=2)