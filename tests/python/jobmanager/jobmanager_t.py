""" jobmanager_t.py

Unit tests for the job manager module.

Failure cases:
 - task specification for application that doesn't exist
 - task specification for application version that doesn't exist
 - badly formed task specifications?
 - task specification for non-production application without appropriate permissions (later?)
 - task specification for user/group with no permissions (later?)

"""
import glob
import json
import os
import shutil
import signal
import sys
import time
import unittest

import subprocess

from couch.Couch import CouchServer
from couch.CouchRequest import CouchNotFoundError, CouchConflictError, CouchError
from couchtest_helpers import upload_couchapp, upload_release, results_on_precondition, init_dbs, delete_dbs
from jobmanagertest_helpers import write_submitter_config, write_jobmanager_config

__MOSSAIC_PATH__ = os.getenv("MOSSAIC")

__COUCHAPP_DIR__ = os.path.join('..', '..', '..', 'src', 'couchapps')
__EXPECTED_DIR__ = os.path.join('..', '..', 'couchapps', 'mossaic', 'shows', 'expected')
__TEST_DATA__ = os.path.join('..', '..', 'data')
__INPUT_FILES__ = os.path.join(__TEST_DATA__, 'input_files')
__INPUT_FILES_RAW__ = os.path.join(__TEST_DATA__, 'raw_input_files')
__DUMMY_EXECUTABLES__ = os.path.join(__TEST_DATA__, 'dummy_executables')
__RELEASES__ = os.path.join(__TEST_DATA__, 'releases')
if __MOSSAIC_PATH__:
    __JOBMANAGER_DIR__ = os.path.join(__MOSSAIC_PATH__, 'lib', 'python2.7', 'site-packages', 'mossaic', 'jobmanager')
else:
    __JOBMANAGER_DIR__ = os.path.join('..', '..', '..', 'src', 'python', 'mossaic', 'jobmanager')
__QUORUM__ = {
    'q': os.getenv('QUORUM_Q'),
    'n': os.getenv('QUORUM_N'),
    'r': os.getenv('QUORUM_R'),
    'w': os.getenv('QUORUM_W')
}
__GRACE_PERIOD__ = 30 # Time before kill -9

def _spawn_master(dbname_task, dbname_state, config=None):
    """ Spawn the sentinel master """
    if config == None:
        config = 'jobmanager_test.ini'
    return subprocess.Popen(['python %s -d -c %s --task-db %s --state-db %s' % (os.path.join(__JOBMANAGER_DIR__, 'master.py'), config, dbname_task, dbname_state)], shell=True)

def _kill_process(process):
    """ Try to kill gracefully, if that doesn't work nuke it """
    returncode = process.poll()
    if returncode == None:
        process.send_signal(signal.SIGHUP)
    for i in range(__GRACE_PERIOD__):
        returncode = process.poll()
        if returncode != None:
            # Process finished
            return
        else:
            time.sleep(1)
    # Process still not dead
    process.send_signal(signal.SIGKILL)


class TestSentinelInput(unittest.TestCase):
    """ Test the input sentinel

    Note that the input sentinel is a temporary method of loading input data and will be replaced by a decent UI based on Jon Wakelin's prototype. This code,
    and the input sentinel plugin, should eventually be removed from the codebase. Preferably before we hit version 1.0.

    """
    def setUp(self):
        self.url = os.getenv('COUCHURL', 'http://admin:pass@localhost:5984')
        self.server = CouchServer(self.url)
        testname = self.id().split('.')[-1]
        self.dbs = init_dbs(testname=testname, dbname_prefix='sentinel_input_unittest', server=self.server)

        self.__populate_dbs()
        write_jobmanager_config(self.url, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'], data_db=self.dbs['data']['name'],
                task=False, job=False, input=True, tracker=False)

    def tearDown(self):
        os.unlink('jobmanager.ini')
        if sys.exc_info()[0] == None:
            # This test has passed, clean up after it
            delete_dbs(self.dbs, self.server)

    def __populate_dbs(self):
        """ Helper method for setUp - populates DB with data needed for these tests """
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'mossaic'), self.dbs['data']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'gui'), self.dbs['data']['name'], self.url)

    def __test_sentinel_input(self, file_map, postprocess_actual, postprocess_expected, dummy=False, spawn=True):
        """ Helper for unit tests - tests the input sentinel works for supplied file map

        Call with dummy=True to avoid doing any results comparisons

        """
        for input_file_type, input_output in file_map.items():
            print 'Testing: %s' % input_file_type
            test_doc = json.loads(file(input_output['test_input']).read())
            test_doc.pop('_id')  # Let CouchDB generate the uid - avoids problems with many revisions of the same doc in this test
            test_doc['destination_id'] = 'dest_id'  # Add a specific _id for
                                                    # the converted doc
            results_dict = self.dbs['data']['db'].commitOne(test_doc, w=__QUORUM__['n'])
            raw_input = file(input_output['attachment']).read()
            retry = 10
            for i in (range(retry)):
                try:
                    self.dbs['data']['db'].addAttachment(str(results_dict[0]['id']), str(results_dict[0]['rev']), raw_input, 'attachment_name', 'text/plain', w=__QUORUM__['n'])
                    break
                except CouchError, e:
                    if i == retry - 1:
                        raise e
                    else:
                        time.sleep(5)
            if not dummy:
                if spawn:
                    master_process = _spawn_master(self.dbs['task']['name'], self.dbs['state']['name'], config='jobmanager.ini')
                raw_view_results = results_on_precondition(function=self.dbs['data']['db'].loadView,
                        args=['gui', 'docs_by_type', {'reduce': False, 'include_docs': True}],
                    test=lambda x:len(x['rows']) == 1 and x['rows'][0]['doc'] != None, wait=2, retry=60)
                actual_result = raw_view_results['rows'][0]['doc']
                if actual_result == None:
                    print raw_view_results
                    self.fail('Intermittent error - please debug me')
                actual_result_id = actual_result['_id']
                print "Actual result id: %s" % actual_result_id
                postprocess_actual(actual_result)
                expected_result = json.loads(file(input_output['expected_output']).read())
                postprocess_expected(expected_result)
                self.assertEqual(actual_result, expected_result)
                for i in range(10): # Retry just in case we are accessing the doc between commit and delete
                    try:    # Check original doc set to converted=true
                        doc = self.dbs['data']['db'].document(
                            results_dict[0]['id'], None, __QUORUM__['n'])
                        self.assertEquals(True, doc['converted']) 
                    except (AssertionError, KeyError) as e:
                        if i < 9:
                            time.sleep(1)
                        else:
                            raise e, None, sys.exc_info()[2]
                # Delete the converted JSON doc so it isn't picked up by the next iteration
                self.dbs['data']['db'].delete_doc(actual_result_id, r=__QUORUM__['n'], w=__QUORUM__['n'])
                if spawn:
                    _kill_process(master_process)

    def __postprocess_actual(self, actual_result):
        """ Postprocessing for actual result before test comparison """
        actual_result.pop('name')   # Name values won't match our test data
        actual_result_id = actual_result.pop('_id')    # _id won't match test data
        actual_result.pop('_rev')   # Neither will _rev

    def __postprocess_expected(self, expected_result):
        """ Postprocessing for expected result before test comparison """
        expected_result.pop('name') # Name values won't match
        expected_result.pop('_id')  # _id values won't match
        if 'geometry' in expected_result and 'location' in expected_result['geometry']:
            expected_result['geometry'].pop('location')

    def test_success_geometry(self):
        """ Test geometry file successfully converted to JSON doc """
        file_map = {
            'chasm': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'geometry.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'geometry', 'success'),
                'expected_output': os.path.join(__INPUT_FILES__, 'geometry.json')
            },
            'questa_soil_approximated': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'geometry.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'geometry', 'success_questa_soil_approximated'),
                'expected_output': os.path.join(__INPUT_FILES__, 'geometry_questa_soil_approximated.json')
            },
            'questa_soil_known': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'geometry.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'geometry', 'success_questa_soil_known'),
                'expected_output': os.path.join(__INPUT_FILES__, 'geometry_questa_soil_known.json')
            }
        }

        def postprocess_expected(expected_result):
            self.__postprocess_expected(expected_result)
            if 'road' in expected_result['geometry']:
                expected_result['geometry']['road']['upslope_shoulder']['value'] = 3.3000000000000007 # Kludge in lieiu of assertAlmostEqual for dicts...

        self.__test_sentinel_input(file_map, self.__postprocess_actual, postprocess_expected)

    def test_success_stability(self):
        """ Test stability file successfully converted to JSON doc """
        file_map = {
            'bishop': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'stability.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'stability', 'success_bishop'),
                'expected_output': os.path.join(__INPUT_FILES__, 'stability_bishop.json')
            },
            'janbu_automated': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'stability.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'stability', 'success_janbu_automated'),
                'expected_output': os.path.join(__INPUT_FILES__, 'stability_janbu_automated.json')
            },
            'janbu_userdef': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'stability_2.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'stability', 'success_janbu_userdef'),
                'expected_output': os.path.join(__INPUT_FILES__, 'stability_janbu_userdef.json')
            }
        }

        self.__test_sentinel_input(file_map, self.__postprocess_actual, self.__postprocess_expected)

    def test_success_boundary_conditions(self):
        """ Test boundary conditions file successfully converted to JSON doc """
        file_map = {
            'deterministic': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'boundary_conditions.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'boundary_conditions', 'success_deterministic'),
                'expected_output': os.path.join(__INPUT_FILES__, 'boundary_conditions_deterministic.json')
            },
            'stochastic': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'boundary_conditions.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'boundary_conditions', 'success_stochastic'),
                'expected_output': os.path.join(__INPUT_FILES__, 'boundary_conditions_stochastic.json')
            },
            'questa_design_single': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'boundary_conditions.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'boundary_conditions', 'success_questa_design_single'),
                'expected_output': os.path.join(__INPUT_FILES__, 'boundary_conditions_questa_design_single.json')
            },
            'questa_design_series': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'boundary_conditions.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'boundary_conditions', 'success_questa_design_series'),
                'expected_output': os.path.join(__INPUT_FILES__, 'boundary_conditions_questa_design_series.json')
            },
            'questa_design_realistic': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'boundary_conditions.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'boundary_conditions', 'success_questa_realistic'),
                'expected_output': os.path.join(__INPUT_FILES__, 'boundary_conditions_questa_realistic.json')
            }
        }

        self.__test_sentinel_input(file_map, self.__postprocess_actual, self.__postprocess_expected)

    def test_success_soils(self):
        """ Test soils file successfully converted to JSON doc """
        file_map = {
            'deterministic': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'soils.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'soils', 'success_deterministic'),
                'expected_output': os.path.join(__INPUT_FILES__, 'soils_deterministic.json')
            },
            'stochastic': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'soils.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'soils', 'success_stochastic'),
                'expected_output': os.path.join(__INPUT_FILES__, 'soils_stochastic.json')
            }
        }

        self.__test_sentinel_input(file_map, self.__postprocess_actual, self.__postprocess_expected)

    def test_engineering_cost(self):
        """ Test engineering cost files successfully converted to JSON doc """
        file_map = {
            'engineering_cost': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'engineering_cost.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'engineering_cost', 'success'),
                'expected_output': os.path.join(__INPUT_FILES__, 'engineering_cost.json')
            }
        }

        self.__test_sentinel_input(file_map, self.__postprocess_actual, self.__postprocess_expected)

    def test_success_following_failure(self):
        """ Test that causing the sentinel to crash (by uploading bad input) does not prevent subsequent uploads """
        file_map = {
            'engineering_cost': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'engineering_cost_bad.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'engineering_cost', 'success'),
                'expected_output': os.path.join(__INPUT_FILES__, 'engineering_cost.json')
            }
        }

        self.__test_sentinel_input(file_map, self.__postprocess_actual, self.__postprocess_expected, dummy=True, spawn=False)
        master_process = _spawn_master(self.dbs['task']['name'], self.dbs['state']['name'], config='jobmanager.ini')

        results_on_precondition(function=self.dbs['data']['db'].loadView, args=['mossaic', 'raw_input', {'reduce': False}],
            test=lambda x:len(x['rows']) == 0, wait=2, retry=60)

        # Test input docs view shows failed doc
        failed_doc = results_on_precondition(function=self.dbs['data']['db'].loadView,
                args=['gui', 'docs_by_type', {'reduce': False, 'include_docs': True, 'key': 'failed_input'}],
                test=lambda x:len(x['rows']) > 0, wait=2, retry=60)['rows'][0]['doc']
        self.dbs['data']['db'].delete_doc(failed_doc['_id'], r=__QUORUM__['n'], w=__QUORUM__['n'])

        file_map = {
            'stability_bishop': {
                'test_input': os.path.join(__INPUT_FILES_RAW__, 'stability.json'),
                'attachment': os.path.join(__EXPECTED_DIR__, 'stability', 'success_bishop'),
                'expected_output': os.path.join(__INPUT_FILES__, 'stability_bishop.json')
            }
        }

        self.__test_sentinel_input(file_map, self.__postprocess_actual, self.__postprocess_expected, spawn=False)
        _kill_process(master_process)

    def test_fail_if_invalid_config(self):
        """ If a config file specifies the input sentinel but not data_db, should fail explicitly """
        write_jobmanager_config(self.url, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'],
                task=False, job=False, input=True, tracker=False)
        self.master_process = _spawn_master(self.dbs['task']['name'], self.dbs['state']['name'], config='jobmanager.ini')
        return_code = self.master_process.wait()
        self.assertEqual(return_code, 1)


class TestSentinelTask(unittest.TestCase):
    """ Tests task sentinel using the local job submitter """
    def setUp(self):
        # Make an instance of the server
        self.url = os.getenv("COUCHURL", 'http://admin:pass@localhost:5984')
        self.url_submitter = os.getenv("COUCHURL_HTTPS", self.url)  # Job submitter should have https url, as it is passed to jobwrapper
        self.server = CouchServer(self.url)
        testname = self.id().split('.')[-1]
        self.dbs = init_dbs(testname=testname, dbname_prefix='sentinel_task_unittest', server=self.server)

        self.input_filenames = {
            'geometry':'geometry', 'soils':'soils_deterministic', 'stability':'stability_bishop', 'boundary_conditions':'boundary_conditions_deterministic',
            'reinforcements':'reinforcements_both', 'vegetation':'vegetation_deterministic_evapotranspiration_canopy', 'stochastic_parameters':'stochastic_parameters',
            'initial_slope':'geometry_questa_soil_approximated', 'engineering_cost':'engineering_cost', 'road_network':'road_network'
        }

        self.__populate_dbs()
        self.maxDiff = None
        self.wait = 1
        self.retry = 180
        write_submitter_config(self.url_submitter, data_db=self.dbs['data']['name'], task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'],
                release_db=self.dbs['release']['name'], backend='local', auth_file='jobwrapper_auth')

    def tearDown(self):
        try:
            _kill_process(self.master_process)
        except AttributeError:
            # If self.master_process doesn't exist, don't worry about it
            pass
        os.unlink('submitter.ini')
        os.unlink('jobmanager.ini')
        os.unlink('jobwrapper_auth')
        ssl_tmpfiles = glob.glob(os.path.join('..', '..', 'ssl', 'tmp*'))
        [shutil.rmtree(tmpdir) for tmpdir in ssl_tmpfiles]
        if sys.exc_info()[0] == None:
            # This test has passed, clean up after it
            testname = self.id().split('.')[-1]
            delete_dbs(self.dbs, self.server)

    def __populate_dbs(self):
        """ Helper method for setUp - populates DB with data needed for these tests """
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'workflow'), self.dbs['task']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'state-machine'), self.dbs['state']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'mossaic'), self.dbs['data']['name'], self.url)
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'chasm_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'chasm_dummy.sh'))
        input_files = [os.path.join(__INPUT_FILES__, '%s.json' % input_filename) for input_filename in self.input_filenames.values()]
        input_docs = dict((input_file, json.loads(file(input_file).read())) for input_file in input_files)
        for input_doc in input_docs.values():
            self.dbs['data']['db'].commitOne(input_doc, w=__QUORUM__['n'])

    def test_success_task(self):
        """ We upload a simple task definition and check that a corresponding job definition document is created """
        write_jobmanager_config(self.url, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'], data_db=self.dbs['data']['name'],
                task=True, job=False, input=False, tracker=False)
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'task_definition_dummy.json')).read()), w=__QUORUM__['n'])
        pending_tasks = results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state', {'key': 'new', 'reduce': False}],
                test=lambda x:len(x['rows']) == 1, wait=self.wait, retry=self.retry)['rows']
        self.assertEqual(len(pending_tasks), 1)
        self.master_process = _spawn_master(self.dbs['task']['name'], self.dbs['state']['name'], config='jobmanager.ini')
        results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state', {'key': 'new', 'reduce': False}],
                test=lambda x:len(x['rows']) == 0, wait=self.wait, retry=self.retry)
        state_docs = self.dbs['state']['db'].loadView('state-machine', 'jobs_by_state', {'reduce': False, 'include_docs': True})['rows']
        self.assertEqual(len(state_docs), 1)
        state_doc = state_docs[0]['doc']
        self.assertEqual(state_doc['state'], 'ready_to_submit')
        self.assertTrue('state_history' in state_doc and 'splitting' in state_doc['state_history'] and 'ready_to_submit' in state_doc['state_history'])
        job_id = state_docs[0]['doc']['job_id']
        actual_job_definition = self.dbs['task']['db'].document(job_id)
        actual_job_definition.pop('_id')
        actual_job_definition.pop('_rev')
        actual_job_definition.pop('timestamp')
        expected_job_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'job_definition_dummy.json')).read())
        expected_job_definition.pop('_id')
        expected_job_definition['task_id'] = pending_tasks[0]['id']
        self.assertEqual(actual_job_definition, expected_job_definition)

    def test_task_with_too_many_job_splits_fails(self):
        """ Test that task which splits into jobs that exceed job_limit set for task plugin fail

        This behaviour is tested in plugins_t.py. This test checks that the sentinel master reads the job limit from the config and passes it
        to the plugin.

        Don't worry about CouchConflict exceptions when writing state documents in the job plugin. This is because the jobwrapper has already
        completed (the jobs fail fast) and marked the state as failed.

        """
        write_jobmanager_config(self.url, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'], data_db=self.dbs['data']['name'],
                task=True, job=False, input=False, tracker=False, job_limit=2)
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'task_definition_dummy_complex.json')).read()), w=__QUORUM__['n'])
        pending_tasks = results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state', {'key': 'new', 'reduce': False}],
                test=lambda x:len(x['rows']) == 1, wait=self.wait, retry=self.retry)['rows']
        self.assertEqual(len(pending_tasks), 1)
        self.master_process = _spawn_master(self.dbs['task']['name'], self.dbs['state']['name'], config='jobmanager.ini')
        results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state', {'key': 'new', 'reduce': False}],
                test=lambda x:len(x['rows']) == 0, wait=self.wait, retry=self.retry)
        state_docs = self.dbs['state']['db'].loadView('state-machine', 'jobs_by_state', {'reduce': False, 'include_docs': True})['rows']
        self.assertEqual(len(state_docs), 0)
        task_doc = results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state', {'reduce': False, 'include_docs': True}],
                test=lambda x:len(x['rows']) == 1, wait=self.wait, retry=self.retry)['rows'][0]['doc']
        self.assertEqual(task_doc['state'], 'rejected - too many jobs: 4')

    def test_total_job_limit(self):
        """ Test that the job sentinel will stop putting new jobs into the system once an initial limit is exceeded

        This behaviour is tested in plugins_t.py. This test checks that the sentinel master reads the job limit from the config and passes it
        to the plugin.

        """
        job_limit = 4
        total_job_limit = 6
        write_jobmanager_config(self.url, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'], data_db=self.dbs['data']['name'],
                task=True, job=False, input=False, tracker=False, job_limit=job_limit, total_job_limit=total_job_limit)
        # Commit two complex tasks = 8 total jobs
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'task_definition_dummy_complex.json')).read()), w=__QUORUM__['n'])
        self.dbs['task']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, 'task_definition_dummy_complex.json')).read()), w=__QUORUM__['n'])
        pending_tasks = results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state', {'key': 'new', 'reduce': False}],
                test=lambda x:len(x['rows']) == 2, wait=self.wait, retry=self.retry)['rows']
        self.assertEqual(len(pending_tasks), 2)
        self.master_process = _spawn_master(self.dbs['task']['name'], self.dbs['state']['name'], config='jobmanager.ini')
        results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state', {'key': 'new', 'reduce': False}],
                test=lambda x:len(x['rows']) == 0, wait=self.wait, retry=self.retry)
        state_docs = self.dbs['state']['db'].loadView('state-machine', 'jobs_by_state', {'reduce': False, 'include_docs': True, 'key': 'ready_to_submit'})['rows']
        self.assertEqual(len(state_docs), 8)
        _kill_process(self.master_process)
        os.unlink('jobmanager.ini')
        write_jobmanager_config(self.url, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'], data_db=self.dbs['data']['name'],
                task=True, job=True, input=False, tracker=False, job_limit=job_limit, total_job_limit=total_job_limit, cycle=10, workers=20)
        self.master_process = _spawn_master(self.dbs['task']['name'], self.dbs['state']['name'], config='jobmanager.ini')
        ready_jobs = results_on_precondition(function=self.dbs['state']['db'].loadView,
                args=['state-machine', 'jobs_by_state', {'key': 'ready_to_submit', 'reduce': False}],
                test=lambda x:len(x['rows']) == 2, wait=self.wait, retry=self.retry)['rows']
        self.assertEqual(len(ready_jobs), 2)    # There should be two jobs left in the ready state by this stage

    def test_complex_task_fails(self):
        """ Upload a complex task definition and check that the task is rejected """
        write_jobmanager_config(self.url, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'], data_db=self.dbs['data']['name'],
                task=True, job=False, input=False, tracker=False)
        task_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'stochastic_task_definition.json')).read())
        self.dbs['task']['db'].commitOne(task_definition, w=__QUORUM__['n'])
        pending_tasks = results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state', {'key': 'new', 'reduce': False}],
                test=lambda x:len(x['rows']) == 1, wait=self.wait, retry=self.retry)['rows']
        self.master_process = _spawn_master(self.dbs['task']['name'], self.dbs['state']['name'], config='jobmanager.ini')
        results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state', {'key': 'new', 'reduce': False}],
                test=lambda x:len(x['rows']) == 0, wait=self.wait, retry=self.retry)
        self.assertEqual(len(self.dbs['task']['db'].loadView('workflow', 'state', {'key': 'new', 'reduce': False})['rows']), 0)
        self.assertEqual(len(self.dbs['state']['db'].loadView('state-machine', 'jobs_by_state', {'reduce': False})['rows']), 0)
        rejected_doc = self.dbs['task']['db'].document(id=task_definition['_id'])
        self.assertEqual(rejected_doc['state'], 'rejected - not implemented')

    def test_malformed_task_fails(self):
        """ Upload a badly formed task definition and check the task is cleanly marked as failed """
        write_jobmanager_config(self.url, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'], data_db=self.dbs['data']['name'],
                task=True, job=False, input=False, tracker=False)
        task_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'stochastic_task_definition.json')).read())
        task_definition['simulation_parameters'].pop('rainfall')
        self.dbs['task']['db'].commitOne(task_definition, w=__QUORUM__['n'])
        pending_tasks = results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state', {'key': 'new', 'reduce': False}],
                test=lambda x:len(x['rows']) == 1, wait=self.wait, retry=self.retry)['rows']
        self.master_process = _spawn_master(self.dbs['task']['name'], self.dbs['state']['name'], config='jobmanager.ini')
        results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state', {'key': 'new', 'reduce': False}],
                test=lambda x:len(x['rows']) == 0, wait=self.wait, retry=self.retry)
        self.assertEqual(len(self.dbs['task']['db'].loadView('workflow', 'state', {'key': 'new', 'reduce': False})['rows']), 0)
        self.assertEqual(len(self.dbs['state']['db'].loadView('state-machine', 'jobs_by_state', {'reduce': False})['rows']), 0)
        rejected_doc = self.dbs['task']['db'].document(id=task_definition['_id'])
        self.assertEqual(rejected_doc['state'], "rejected - <type 'exceptions.KeyError'>:'number_of_jobs'")

    def test_success_job(self):
        """ Upload a simple task definition and check that the appropriate state transitions have happened

        Note: This test needed modifying when the job wrapper was updated to close jobs, as job doc and state doc now have state 'done' when
        everything is finished.

        """
        write_jobmanager_config(self.url, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'], data_db=self.dbs['data']['name'],
                task=True, job=True, input=False, tracker=False)
        self.master_process = _spawn_master(self.dbs['task']['name'], self.dbs['state']['name'], config='jobmanager.ini')
        task_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'task_definition_dummy.json')).read())
        self.dbs['task']['db'].commitOne(task_definition, w=__QUORUM__['n'])
        results_on_precondition(function=self.dbs['state']['db'].loadView, args=['state-machine', 'jobs_by_state',
                {'key': 'done', 'reduce': False, 'include_docs': True}], test=lambda x:len(x['rows']) == 1, wait=self.wait, retry=self.retry)['rows']
        # state machine should show job as submitted
        # state history should have splitting, ready_to_submit and submitted
        submitted_jobs = self.dbs['state']['db'].loadView('state-machine', 'jobs_by_state', {'key': 'done', 'reduce': False, 'include_docs': True})['rows']
        self.assertEqual(len(submitted_jobs), 1)
        job_state = submitted_jobs[0]['doc']
        self.assertEqual(job_state['state'], 'done')
        self.assertTrue('state_history' in job_state and 'splitting' in job_state['state_history'] and 'ready_to_submit' in job_state['state_history'] and 
                'submitted' in job_state['state_history'] and 'done' in job_state['state_history'])

@unittest.skipIf(os.getenv("SKIP_DIRAC_TESTS", False), "SKIP_DIRAC_TESTS is set so skipping DIRAC tests")
class TestSentinelTaskSubmitterDirac(TestSentinelTask):
    """ Test task sentinel with the DIRAC submitter """
    def setUp(self):
        super(TestSentinelTaskSubmitterDirac, self).setUp()
        self.wait = 10
        self.retry = 60
        write_submitter_config(self.url_submitter, data_db=self.dbs['data']['name'], task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'],
                release_db=self.dbs['release']['name'], backend='dirac', auth_file='jobwrapper_auth')

    def test_total_job_limit(self):
        """ Don't actually run this test against the DIRAC submitter - it doesn't test anything that isn't already covered and it fills up DIRAC with junk """
        write_jobmanager_config(self.url, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'], # Write this anyway so tearDown doesn't fail
                task=True, job=False, input=False, tracker=False, job_limit=4, total_job_limit=6)
        pass

class TestSentinelTracker(unittest.TestCase):
    def setUp(self):
        # Make an instance of the server
        self.url = os.getenv("COUCHURL", 'http://admin:pass@localhost:5984')
        self.server = CouchServer(self.url)
        testname = self.id().split('.')[-1]
        self.dbs = init_dbs(testname=testname, dbname_prefix='sentinel_tracker_unittest', server=self.server)

        self.input_filenames = {
            'geometry':'geometry', 'soils':'soils_deterministic', 'stability':'stability_bishop', 'boundary_conditions':'boundary_conditions_deterministic',
            'reinforcements':'reinforcements_both', 'vegetation':'vegetation_deterministic_evapotranspiration_canopy', 'stochastic_parameters':'stochastic_parameters',
            'initial_slope':'geometry_questa_soil_approximated', 'engineering_cost':'engineering_cost', 'road_network':'road_network'
        }

        self.__populate_dbs()
        write_submitter_config(self.url, data_db=self.dbs['data']['name'], task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'],
                release_db=self.dbs['release']['name'], auth_file='jobwrapper_auth')
        self.maxDiff = None

    def __populate_dbs(self):
        """ Helper method for setUp - populates DB with data needed for these tests """
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'workflow'), self.dbs['task']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'state-machine'), self.dbs['state']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'mossaic'), self.dbs['data']['name'], self.url)
        upload_release(self.dbs['release']['db'], os.path.join(__RELEASES__, 'chasm_dummy.json'), os.path.join(__DUMMY_EXECUTABLES__, 'chasm_dummy.sh'))
        input_files = [os.path.join(__INPUT_FILES__, '%s.json' % input_filename) for input_filename in self.input_filenames.values()]
        input_docs = dict((input_file, json.loads(file(input_file).read())) for input_file in input_files)
        for input_doc in input_docs.values():
            self.dbs['data']['db'].commitOne(input_doc, w=__QUORUM__['n'])

    def __results_on_precondition(self, function, args, test, wait, retry):
        """ Retry given function until the results pass the function specified in test, then return it """
        for attempt in range(retry):
            results = function(*args)
            if test(results):
                return results
            else:
                time.sleep(wait)
        raise StandardError

    def tearDown(self):
        _kill_process(self.master_process)
        os.unlink('submitter.ini')
        os.unlink('jobmanager.ini')
        os.unlink('jobwrapper_auth')
        if sys.exc_info()[0] == None:
            # This test has passed, clean up after it
            delete_dbs(self.dbs, self.server)

    def test_success(self):
        """ Test a succesfully completed job results in a closed task and deleted state doc """
        write_jobmanager_config(self.url, task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'], data_db=self.dbs['data']['name'],
                task=True, job=True, input=False, tracker=True)
        self.master_process = _spawn_master(self.dbs['task']['name'], self.dbs['state']['name'], config='jobmanager.ini')
        task_definition = json.loads(file(os.path.join(__INPUT_FILES__, 'task_definition_dummy.json')).read())
        self.dbs['task']['db'].commitOne(task_definition, w=__QUORUM__['n'])
        results = self.__results_on_precondition(function=self.dbs['task']['db'].loadView, args=['workflow', 'state',
                {'key': 'done', 'reduce': False, 'include_docs': False}], test=lambda x:len(x['rows']) == 1, wait=2, retry=60)['rows']
        state_docs = self.__results_on_precondition(function=self.dbs['state']['db'].loadView, args=['state-machine', 'jobs_by_state',
                {'key': 'done', 'reduce': False, 'include_docs': True}], test=lambda x:len(x['rows']) == 0, wait=2, retry=60)['rows']
        self.assertEqual(len(state_docs), 0)


if __name__ == '__main__':
    unittest.main(verbosity=2)