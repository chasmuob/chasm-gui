""" proxy_factory_t.py

Unit tests for the proxy factory.

This test requires an HTTP server that requires SSL certificate authentication and supports proxy certificates.

"""

import base64
import glob
import httplib2
import os
import shutil
import unittest

from mossaic.jobmanager import proxy_factory
from couchtest_helpers import get_url_and_auth

class TestProxyFactory(unittest.TestCase):
    def setUp(self):
        self.url, self.auth = get_url_and_auth(os.getenv("COUCHURL_HTTPS", 'https://admin:pass@localhost:6984'), '')

    def tearDown(self):
        ssl_tmpfiles = glob.glob(os.path.join('..', '..', 'ssl', 'tmp*'))
        [shutil.rmtree(tmpdir) for tmpdir in ssl_tmpfiles]

    def test_get_proxy(self):
        """ Test we get a valid proxy """
        proxy = proxy_factory.get_proxy()
        h = httplib2.Http('.')
        h.add_certificate(proxy['key'], proxy['cert'], '')
        headers = {'Authorization': "Basic " + self.auth}
        response = h.request(self.url, headers=headers)
        self.assertEquals(response[0]['status'], '200')

    def test_get_proxy_repeat(self):
        """ Test that repeat request for a proxy return an existing proxy, unless the existing proxy is older than specified """
        proxy = proxy_factory.get_proxy(hours=20)
        self.assertEquals(proxy, proxy_factory.get_proxy(hours=20))
        self.assertEquals(proxy, proxy_factory.get_proxy(hours=21))
        self.assertEquals(proxy, proxy_factory.get_proxy(hours=22))
        self.assertEquals(proxy, proxy_factory.get_proxy(hours=23.9))
        proxy2 = proxy_factory.get_proxy(hours=24)
        self.assertNotEqual(proxy, proxy2)
        self.assertEqual(proxy2['days'], 1)
        proxy3 = proxy_factory.get_proxy(hours=25)
        self.assertNotEqual(proxy, proxy3)
        self.assertNotEqual(proxy2, proxy3)
        self.assertEqual(proxy3['days'], 2)

if __name__ == '__main__':
    unittest.main(verbosity=2)