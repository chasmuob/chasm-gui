""" plugins_t.py

Unit tests for job, task and tracker sentinel plugins.

These tests are more isolated than the tests in jobmanager_t.py (which test more of the system than just the plugins).
This allows us to simulate failure modes easily.

"""

import os
import json
import logging
import random
import sys
import time
import unittest

from mock import Mock

from couch.Couch import CouchServer
from couch.CouchRequest import CouchError
from mossaic.jobmanager import plugins

from couchtest_helpers import upload_couchapp, results_on_precondition, init_dbs, delete_dbs
from jobmanagertest_helpers import write_submitter_config

__COUCHAPP_DIR__ = os.path.join('..', '..', '..', 'src', 'couchapps')
__INPUT_FILES__ = os.path.join('..', '..', 'data', 'input_files')
__QUORUM__ = {
    'q': os.getenv('QUORUM_Q'),
    'n': os.getenv('QUORUM_N'),
    'r': os.getenv('QUORUM_R'),
    'w': os.getenv('QUORUM_W')
}

def _get_fake_state_doc(job_id='whatever', task_id='whatever'):
    """ Generate a fake state doc with specified job id, in state required for job wrapper test ('submitted') """
    return {
        'job_id': job_id,
        'state': 'ready_to_submit',
        'state_history': {
           "ready_to_submit": {
               "timestamp": 1300188082,
               "modified_by": "Task"
           },
           "splitting": {
               "timestamp": 1300187895,
               "modified_by": "Task"
           }
        },
        'task_id': task_id
    }

def _get_fake_task_doc(task_id, type=None):
    """ Generate a fake task doc with specified job id, in state required for job wrapper test ('submitted') """
    if type == None:
        return {
            "_id": task_id,
            "type": "task",
            "requestor": "Joe Bloggs",
            "group": "Geography",
            "name": "Joe's test task",
            "timestamp": 1298939743.0,
            "executable": {
                "application": "chasm",
                "version": "dummy"
            }
        }
    elif type == 'deterministic':
        task = _get_fake_task_doc(task_id)
        task['simulation_parameters'] = {
            'deterministic': {
                '/geometry/slope_sections/upslope/1/angle/value': {
                    'type': 'geometry',
                    'value': [55, 65, 75, 85]
                },
                '/geometry/slope_sections/upslope/0/angle/value': {
                    'type': 'geometry',
                    'value': [35, 40, 45]
                }
            }
        }
        return task
    elif type == 'deterministic_range':
        task = _get_fake_task_doc(task_id)
        task['simulation_parameters'] = {
            'deterministic': {
                '/geometry/slope_sections/upslope/1/angle/value': {
                    'type': 'geometry',
                    'range': [55, 85],
                    'increment': 10
                },
                '/geometry/slope_sections/upslope/0/angle/value': {
                    'type': 'geometry',
                    'range': [35, 45],
                    'increment': 5
                }
            }
        }
        return task
    elif type == 'stochastic':
        task = _get_fake_task_doc(task_id)
        task['simulation_parameters'] = {
            'number_of_jobs': 10,
            'stochastic': {
                '/soils/0/Ksat/value': {
                    'type': 'soils',
                    'mean': 1e-06,
                    'standard_deviation': 1e-07
                },
                '/soils/0/saturated_moisture_content/value': {
                    'type': 'soils',
                    'mean': 0.41,
                    'standard_deviation': 0.041
                }
            }
        }
        return task
    elif type == 'stochastic_bad':
        task = _get_fake_task_doc(task_id, type='stochastic')
        task['simulation_parameters'].pop('number_of_jobs')
        return task
    elif type == 'rainfall':
        task = _get_fake_task_doc(task_id)
        task['simulation_parameters'] = {'rainfall': 'foo'}
        return task

def _check_doc_state_history(that, db, doc_id, state_history):
    """ Helper - check for equivalence, except with timestamps """
    task_doc = db.document(doc_id, r=__QUORUM__['n'])
    for key, value in state_history.items():
        that.assertTrue('timestamp' in task_doc['state_history'][key])
        that.assertEqual(task_doc['state_history'][key]['modified_by'], value['modified_by'])

class TestPlugin(unittest.TestCase):
    """ Test sentinel plugins """
    def setUp(self):
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger('Job plugin test')
        # Make an instance of the server
        self.url = os.getenv("COUCHURL", 'http://admin:pass@localhost:5984')
        self.server = CouchServer(self.url)
        self.maxDiff = None
        self.wait = 2
        self.retry = 30
        testname = '_'.join(self.id().split('.')[-2:])
        self.dbs = init_dbs(testname=testname, dbname_prefix='plugin_unittest', server=self.server)
        write_submitter_config(self.url, data_db=self.dbs['data']['name'], task_db=self.dbs['task']['name'], state_db=self.dbs['state']['name'],
                release_db=self.dbs['release']['name'], backend='local', auth_file='jobwrapper_auth')
        from mossaic.jobmanager.submitter import SubmitterResponse
        self.SubmitterResponse = SubmitterResponse

    def tearDown(self):
        os.unlink('submitter.ini')
        os.unlink('jobwrapper_auth')
        if sys.exc_info()[0] == None:
            # This test has passed, clean up after it
            delete_dbs(self.dbs, self.server)

class TestPluginTracker(TestPlugin):
    """ Test tracker sentinel plugin """
    def setUp(self):
        super(TestPluginTracker, self).setUp()
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'workflow'), self.dbs['task']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'state-machine'), self.dbs['state']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'gui-js'), self.dbs['data']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'mining-js'), self.dbs['data']['name'], self.url)

    def __check_state(self, task, job):
        """ Helper method - checks number of documents in supplied states """
        if task:
            input_docs = results_on_precondition(function=self.dbs['task']['db'].loadView,
                    args=['workflow', 'state', {'reduce': False, 'include_docs': True, 'key': task['state']}],
                    test=lambda x:len(x['rows']) == task['num'], wait=self.wait, retry=self.retry)['rows']
            self.assertEquals(len(input_docs), task['num'])
        if job:
            input_docs = results_on_precondition(function=self.dbs['state']['db'].loadView,
                    args=['state-machine', 'jobs_by_state', {'reduce': False, 'include_docs': True, 'key': job['state']}],
                    test=lambda x:len(x['rows']) == job['num'], wait=self.wait, retry=self.retry)['rows']
            self.assertEquals(len(input_docs), job['num'])

    def __get_fake_job_docs(self, task_id, num_jobs):
        """ Generate fake job state documents """
        return [_get_fake_state_doc(task_id=task_id) for num in range(num_jobs)]

    def __close_job(self, state_doc, final_state='done'):
        """ Helper method - closes the state document """
        state_doc['state'] = final_state
        self.dbs['state']['db'].commitOne(state_doc, w=__QUORUM__['n'])

    def __commit_docs(self, db, docs):
        """ Helper method - can't use bulk commit as won't support quorum values """
        for doc in docs:
            results_dict = db.commitOne(doc, w=__QUORUM__['n'])
            doc['_id'] = str(results_dict[0]['id'])
            doc['_rev'] = str(results_dict[0]['rev'])

    def __check_doc(self, doc_id, items_to_check):
        task_doc = self.dbs['task']['db'].document(doc_id, r=__QUORUM__['n'])
        for key, value in items_to_check.items():
            self.assertEquals(task_doc[key], value)

    def test_success(self):
        """ Test task is closed when all jobs are closed """
        num_jobs = 3
        task_doc = _get_fake_task_doc(task_id='foo')
        task_doc['state'] = 'in_progress'
        task_doc['state_history'] = {'in_progress': {'timestamp': int(time.time()), 'modified_by': 'Task'}}
        job_docs = self.__get_fake_job_docs(task_id='foo', num_jobs=num_jobs)
        for job_doc in job_docs:
            job_doc['state'] = 'submitted'
        self.__commit_docs(self.dbs['task']['db'], [task_doc])
        self.__commit_docs(self.dbs['state']['db'], job_docs)
        tracker_plugin = plugins.Tracker(self.dbs['task']['db'], self.dbs['state']['db'], self.logger, data_db=self.dbs['data']['db'], quorum=__QUORUM__)
        self.__check_state(task={'state': 'in_progress', 'num': 1}, job={'state': 'submitted', 'num': num_jobs})

        self.__close_job(job_docs[0])
        work = results_on_precondition(function=plugins.Tracker.get_work,
                    args=[{'state': self.dbs['state']['db']}],
                    test=lambda x:len(x) == 0, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 0)
        self.__check_state(task={'state': 'in_progress', 'num': 1}, job={'state': 'submitted', 'num': num_jobs - 1})
        self.__check_state(task={'state': 'done', 'num': 0}, job={'state': 'done', 'num': 1})

        for job_doc in job_docs[1:]:
            self.__close_job(job_doc)
        work = results_on_precondition(function=plugins.Tracker.get_work,
                    args=[{'state': self.dbs['state']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)

        tracker_plugin(work[0])
        self.__check_state(task={'state': 'in_progress', 'num': 0}, job={'state': 'submitted', 'num': 0})
        self.__check_state(task={'state': 'done', 'num': 1}, job={'state': 'done', 'num': 0})
        self.__check_doc('foo', {'summary': {'total_jobs': 3, 'completed_jobs': 3, 'failed_jobs': 0}})
        _check_doc_state_history(self, self.dbs['task']['db'], 'foo',
                {'in_progress': {'timestamp': 0, 'modified_by': 'Task'}, 'done': {'timestamp': 0, 'modified_by': 'Tracker'}})

    def test_fail(self):
        """ Test task is closed when all jobs have failed, and correct interim state is recorded """
        num_jobs = 3
        task_doc = _get_fake_task_doc(task_id='foo')
        task_doc['state'] = 'in_progress'
        task_doc['state_history'] = {'in_progress': {'timestamp': int(time.time()), 'modified_by': 'Task'}}
        job_docs = self.__get_fake_job_docs(task_id='foo', num_jobs=num_jobs)
        for job_doc in job_docs:
            job_doc['state'] = 'submitted'
        self.__commit_docs(self.dbs['task']['db'], [task_doc])
        self.__commit_docs(self.dbs['state']['db'], job_docs)
        tracker_plugin = plugins.Tracker(self.dbs['task']['db'], self.dbs['state']['db'], self.logger, data_db=self.dbs['data']['db'], quorum=__QUORUM__)
        self.__check_state(task={'state': 'in_progress', 'num': 1}, job={'state': 'submitted', 'num': num_jobs})

        self.__close_job(job_docs[0], final_state='failed')
        work = results_on_precondition(function=plugins.Tracker.get_work,
                    args=[{'state': self.dbs['state']['db']}],
                    test=lambda x:len(x) == 0, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 0)
        self.__check_state(task={'state': 'in_progress', 'num': 1}, job={'state': 'submitted', 'num': num_jobs - 1})
        self.__check_state(task={'state': 'done', 'num': 0}, job={'state': 'failed', 'num': 1})

        for job_doc in job_docs[1:]:
            self.__close_job(job_doc, final_state='done')
        work = results_on_precondition(function=plugins.Tracker.get_work,
                    args=[{'state': self.dbs['state']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)
        tracker_plugin(work[0])
        self.__check_state(task={'state': 'in_progress', 'num': 0}, job={'state': 'submitted', 'num': 0})
        self.__check_state(task={'state': 'done', 'num': 1}, job={'state': 'done', 'num': 0})
        self.__check_state(task=None, job={'state': 'failed', 'num': 0})
        self.__check_doc('foo', {'summary': {'total_jobs': 3, 'completed_jobs': 2, 'failed_jobs': 1}})
        _check_doc_state_history(self, self.dbs['task']['db'], 'foo',
                {'in_progress': {'timestamp': 0, 'modified_by': 'Task'}, 'done': {'timestamp': 0, 'modified_by': 'Tracker'}})

class TestPluginInput(TestPlugin):
    """ Test input sentinel plugin """
    def setUp(self):
        super(TestPluginInput, self).setUp()
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'mossaic'), self.dbs['data']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'gui'), self.dbs['data']['name'], self.url)

    def __get_fake_input_doc(self, input_id, malformed=False):
        """ Generate a fake input document """
        doc = {
            "_id": input_id,
            "destination_id": "foofoo",
            "parent_id": "foofoofoo",
            "destination_type": "engineering_cost",
            "name": "my engineering cost data"
        }
        if malformed:
            doc['destination_type'] = 'stability'
        return doc

    def __get_expected_keys(self):
        """ Get expected keys and values """
        return {
            '_id': 'foofoo',
            'parent_id': 'foofoofoo',
            'name': 'my engineering cost data'
        }

    def __test_input_docs(self, key, expected=1, keys_to_test=None):
        """ Helper function - hits the input docs view and checks that number of results is as expected """
        input_docs = results_on_precondition(function=self.dbs['data']['db'].loadView,
                args=['gui', 'docs_by_type', {'reduce': False, 'include_docs': True, 'key': key}],
                test=lambda x:len(x['rows']) == expected, wait=self.wait, retry=self.retry)['rows']
        self.assertEqual(len(input_docs), expected)
        if keys_to_test:
            doc = self.dbs['data']['db'].document(keys_to_test['_id'])
            for key, value in keys_to_test.items():
                self.assertEquals(doc[key], value)

    def test_success(self):
        """ Test input file is successfully converted by plugin """
        raw_input = file(os.path.join('..', '..', 'couchapps', 'mossaic', 'shows', 'expected', 'engineering_cost', 'success')).read()
        results_dict = self.dbs['data']['db'].commitOne(self.__get_fake_input_doc('foo'), w=__QUORUM__['n'])
        for i in (range(self.retry)):
            try:
                self.dbs['data']['db'].addAttachment(str(results_dict[0]['id']), str(results_dict[0]['rev']), raw_input, 'attachment_name', 'text/plain', w=__QUORUM__['n'])
                break
            except CouchError, e:
                if i == self.retry - 1:
                    raise e
                else:
                    time.sleep(5)
        time.sleep(5)   # BigCouch doesn't honour w parameter for attachments, so sleep for now
        work = results_on_precondition(function=plugins.Input.get_work,
                    args=[{'data': self.dbs['data']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 1)
        input_plugin = plugins.Input(self.dbs['task']['db'], self.dbs['state']['db'], self.logger, quorum=__QUORUM__, data_db=self.dbs['data']['db'])
        input_plugin(work[0]['doc'])
        self.__test_input_docs(key='engineering_cost', expected=1,
                keys_to_test=self.__get_expected_keys())
        results_on_precondition(function=plugins.Input.get_work,
                args=[{'data': self.dbs['data']['db']}],
                test=lambda x:len(x) == 0, wait=self.wait, retry=self.retry)

    def test_fail(self):
        """ Test input file is marked as failed if conversion fails """
        raw_input = file(os.path.join('..', '..', 'couchapps', 'mossaic', 'shows', 'expected', 'engineering_cost', 'success')).read()
        results_dict = self.dbs['data']['db'].commitOne(self.__get_fake_input_doc('foo', malformed=True), w=__QUORUM__['n'])
        for i in (range(self.retry)):
            try:
                self.dbs['data']['db'].addAttachment(str(results_dict[0]['id']), str(results_dict[0]['rev']), raw_input, 'attachment_name', 'text/plain', w=__QUORUM__['n'])
                break
            except CouchError, e:
                if i == self.retry - 1:
                    raise e
                else:
                    time.sleep(5)
        time.sleep(5)
        work = results_on_precondition(function=plugins.Input.get_work,
                    args=[{'data': self.dbs['data']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)

        self.assertEquals(len(work), 1)
        input_plugin = plugins.Input(self.dbs['task']['db'], self.dbs['state']['db'], self.logger, quorum=__QUORUM__, data_db=self.dbs['data']['db'])
        input_plugin(work[0]['doc'])
        self.__test_input_docs(key='failed_input', expected=1)
        results_on_precondition(function=plugins.Input.get_work,
                args=[{'data': self.dbs['data']['db']}],
                test=lambda x:len(x) == 0, wait=self.wait, retry=self.retry)

    def test_no_attachment(self):
        """ Test input file is marked as failed after required number of retries if there is no attachment """
        self.dbs['data']['db'].commitOne(self.__get_fake_input_doc('foo'), w=__QUORUM__['n'])
        work = results_on_precondition(function=plugins.Input.get_work,
                    args=[{'data': self.dbs['data']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 1)
        input_plugin = plugins.Input(self.dbs['task']['db'], self.dbs['state']['db'], self.logger, quorum=__QUORUM__, data_db=self.dbs['data']['db'],retries=2)
        input_plugin(work[0]['doc'])
        work = results_on_precondition(function=plugins.Input.get_work,
                    args=[{'data': self.dbs['data']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 1)
        input_plugin(work[0]['doc'])
        work = results_on_precondition(function=plugins.Input.get_work,
                    args=[{'data': self.dbs['data']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 1)
        input_plugin(work[0]['doc'])
        work = results_on_precondition(function=plugins.Input.get_work,
                    args=[{'data': self.dbs['data']['db']}],
                    test=lambda x:len(x) == 0, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 0)
        self.__test_input_docs(key='failed_input', expected=1)


class TestPluginTask(TestPlugin):
    """ Test task sentinel plugin """
    def setUp(self):
        super(TestPluginTask, self).setUp()
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'workflow'), self.dbs['task']['name'], self.url)
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'state-machine'), self.dbs['state']['name'], self.url)

    def __test_doc_states(self, results):
        """ Helper for test methods - checks doc states are as expected """
        jobs = results_on_precondition(function=self.dbs['task']['db'].loadView,
                args=['workflow', 'jobs_by_task', {'include_docs': True, 'key': ['foo', 'new'], 'reduce': False}],
                test=lambda x:len(x['rows']) == results[0], wait=self.wait, retry=self.retry)['rows']
        self.assertEquals(len(jobs), results[0])
        states = results_on_precondition(function=self.dbs['state']['db'].loadView,
                args=['state-machine', 'jobs_by_state', {'include_docs': True, 'key': 'ready_to_submit', 'reduce': False}],
                test=lambda x:len(x['rows']) == results[1], wait=self.wait, retry=self.retry)['rows']
        self.assertEquals(len(states), results[1])
        task = self.dbs['task']['db'].document('foo')
        self.assertEquals(task['state'], results[2])

    def __commit_and_execute_plugin(self, task_doc, options={'job_limit': None}):
        """ Helper method - commits task, checks work and runs task plugin on work """
        self.dbs['task']['db'].commitOne(task_doc, w=__QUORUM__['n'])
        work = results_on_precondition(function=plugins.Task.get_work,
                    args=[{'task': self.dbs['task']['db'], 'state': self.dbs['state']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)

        self.assertEquals(len(work), 1)
        task_plugin = plugins.Task(self.dbs['task']['db'], self.dbs['state']['db'], self.logger, quorum=__QUORUM__, data_db=self.dbs['data']['db'],
                                                                                                                                            options=options)
        task_plugin(work[0]['doc'])

    def __test_values_in_jobs(self, values, jobs, parameter_type, pointers):
        """ Helper method - test whether each group of values is found within jobs/simulation_parameters/parameter_type/pointer """
        for expected_value in values:
            job_has_value = False
            for job in jobs:
                value = [job['doc']['simulation_parameters'][parameter_type][pointer]['value'] for pointer in pointers]
                if value == expected_value:
                    job_has_value = True
            self.assertTrue(job_has_value)

    def test_success(self):
        """ Test that successful task splitting results in correct state changes """
        task_doc = _get_fake_task_doc(task_id='foo')
        self.__commit_and_execute_plugin(task_doc)
        self.__test_doc_states((1, 1, 'in_progress'))
        _check_doc_state_history(self, self.dbs['task']['db'], 'foo',
                {'in_progress': {'timestamp': 0, 'modified_by': 'Task'}})

    def __test_job_names(self, task_doc, number_of_jobs=1):
        """ Helper method - tests all expected job names are created """
        self.__commit_and_execute_plugin(task_doc)
        jobs = results_on_precondition(function=self.dbs['task']['db'].loadView,
                args=['workflow', 'jobs_by_task', {'include_docs': True, 'key': ['foo', 'new'], 'reduce': False}],
                test=lambda x:len(x['rows']) == number_of_jobs, wait=self.wait, retry=self.retry)['rows']
        expected_names = [str(i) for i in range(0, number_of_jobs)]
        for job in jobs:
            self.assertTrue(job['doc']['name'] in expected_names)
            expected_names.remove(job['doc']['name'])

    def test_success_job_name_stochastic(self):
        """ Test that stochastic task splitting results in correct job names """
        task_doc = _get_fake_task_doc(task_id='foo', type='stochastic')
        self.__test_job_names(task_doc, number_of_jobs=10)

    def test_success_job_name_deterministic(self):
        """ Test that deterministic task splitting results in correct job names """
        task_doc = _get_fake_task_doc(task_id='foo', type='deterministic')
        self.__test_job_names(task_doc, number_of_jobs=12)

    def test_success_job_name_no_split(self):
        """ Test that 1:1 task "split" results in correct job names """
        task_doc = _get_fake_task_doc(task_id='foo')
        self.__test_job_names(task_doc, number_of_jobs=1)

    def test_success_parameters_stochastic_from_file_soils(self):
        input_file = 'soils_stochastic.json'
        input_files = {"soils": "soils_stochastic"}
        params=sorted(['/soils/0/Ksat/value', '/soils/0/saturated_moisture_content/value', '/soils/0/saturated_bulk_density/value',
                '/soils/0/unsaturated_bulk_density/value', '/soils/0/effective_cohesion/value', '/soils/0/effective_angle_of_internal_friction/value'])
        self.__test_success_parameters_stochastic_from_file(input_file, input_files, params)

    def test_success_parameters_stochastic_from_file_geometry_soils_approximated(self):
        """ Test successful task splitting of task where stochastic parameters are specified in geometry input file with approximated soils """
        input_file = 'geometry_questa_soil_approximated_stochastic.json'
        input_files = {"geometry": "geometry_questa_soil_approximated_stochastic"}
        params=sorted(['/geometry/water_table/0/depth/value', '/geometry/water_table/1/depth/value', '/geometry/water_table/2/depth/value',
                '/geometry/soil_strata/interfaces/depth/value', '/geometry/soil_strata/interfaces/angle/value'])
        self.__test_success_parameters_stochastic_from_file(input_file, input_files, params)

    def test_success_parameters_stochastic_from_file_geometry_soils_known(self):
        """ Test successful task splitting of task where stochastic parameters are specified in geometry input file with known soils """
        input_file = 'geometry_questa_soil_known_stochastic.json'
        input_files = {"geometry": "geometry_questa_soil_known"}
        params=sorted(['/geometry/water_table/0/depth/value', '/geometry/water_table/1/depth/value', '/geometry/water_table/2/depth/value',
                '/geometry/soil_strata/0/depth/value', '/geometry/soil_strata/1/depth/value', '/geometry/soil_strata/2/depth/value'])
        self.__test_success_parameters_stochastic_from_file(input_file, input_files, params)

    def __test_success_parameters_stochastic_from_file(self, input_file, input_files, params):
        """ Test successful task splitting of task where stochastic parameters are specified in soils input file """
        self.dbs['data']['db'].commitOne(json.loads(file(os.path.join(__INPUT_FILES__, input_file)).read()), w=__QUORUM__['n'])
        task_doc = _get_fake_task_doc(task_id='foo')
        task_doc['input_files'] = input_files
        task_doc['simulation_parameters'] = {
            'number_of_jobs': 10
        }
        self.__commit_and_execute_plugin(task_doc)
        self.__test_doc_states((10, 10, 'in_progress'))
        seed = self.dbs['task']['db'].loadView(
                'workflow', 'tasks_by_user', {'include_docs': True, 'reduce': False})['rows'][0]['doc']['simulation_parameters']['seed']
        random.seed(seed)
        jobs = self.dbs['task']['db'].loadView('workflow', 'jobs_by_task', {'include_docs': True, 'key': ['foo', 'new'], 'reduce': False})['rows']
        task_doc = self.dbs['task']['db'].document('foo', r=__QUORUM__['n'])  # Re-fetch task doc as will now have the params from input file
        values = self.__get_stochastic_values(task_doc, params=params)
        self.__test_values_in_jobs(values, jobs, 'stochastic', params)

    def __get_stochastic_values(self, task_doc, params):
        """ Helper for stochastic task tests """
        values_dict = {}
        for param in params:
            mean = task_doc['simulation_parameters']['stochastic'][param]['mean']
            standard_deviation = task_doc['simulation_parameters']['stochastic'][param]['standard_deviation']
            if (type(mean) == list):
                values_dict[param] = [[random.gauss(mean[j], standard_deviation[j]) for j in range(len(mean))]
                        for i in range(task_doc['simulation_parameters']['number_of_jobs'])]
            else:
                values_dict[param] = [random.gauss(mean, standard_deviation) for i in range(task_doc['simulation_parameters']['number_of_jobs'])]
        values = []
        for i in range(task_doc['simulation_parameters']['number_of_jobs']):
            row = []
            for param in params:
                row.append(values_dict[param][i])
            values.append(row)
        return iter(values)

    def test_success_parameters_stochastic(self):
        """ Test that successful task splitting of stochastic parameters results in correct state changes and job docs """
        task_doc = _get_fake_task_doc(task_id='foo', type='stochastic')
        self.__commit_and_execute_plugin(task_doc)
        self.__test_doc_states((10, 10, 'in_progress'))
        seed = self.dbs['task']['db'].loadView(
                'workflow', 'tasks_by_user', {'include_docs': True, 'reduce': False})['rows'][0]['doc']['simulation_parameters']['seed']
        random.seed(seed)
        jobs = self.dbs['task']['db'].loadView('workflow', 'jobs_by_task', {'include_docs': True, 'key': ['foo', 'new'], 'reduce': False})['rows']
        params = ['/soils/0/Ksat/value', '/soils/0/saturated_moisture_content/value']
        values = self.__get_stochastic_values(task_doc, params=params)
        self.__test_values_in_jobs(values, jobs, 'stochastic', params)

    def test_fail_parameters_rainfall(self):
        """ Test that attempting to split a task with rainfall parameters results in a failure """
        task_doc = _get_fake_task_doc(task_id='foo', type='rainfall')
        self.__commit_and_execute_plugin(task_doc)
        self.__test_doc_states((0, 0, 'rejected - not implemented'))

    def test_fail_parameters_bad(self):
        """ Test that attempting to split a task with bad parameters results in a failure """
        task_doc = _get_fake_task_doc(task_id='foo', type='stochastic_bad')
        self.__commit_and_execute_plugin(task_doc)
        self.__test_doc_states((0, 0, "rejected - <type 'exceptions.KeyError'>:'number_of_jobs'"))

    def __test_success_parameters_deterministic(self, task_doc):
        """ Helper method - tests deterministic parameter sets """
        self.__commit_and_execute_plugin(task_doc)
        self.__test_doc_states((12, 12, 'in_progress'))
        jobs = results_on_precondition(function=self.dbs['task']['db'].loadView,
                args=['workflow', 'jobs_by_task', {'include_docs': True, 'key': ['foo', 'new'], 'reduce': False}],
                test=lambda x:len(x['rows']) == 12, wait=self.wait, retry=self.retry)['rows']
        s0 = task_doc['simulation_parameters']['deterministic']['/geometry/slope_sections/upslope/0/angle/value']
        s1 = task_doc['simulation_parameters']['deterministic']['/geometry/slope_sections/upslope/1/angle/value']
        if 'value' not in s0 and 'value' not in s1:
            s0['value'] = range(s0['range'][0], s0['range'][1] + 1, s0['increment'])
            s1['value'] = range(s1['range'][0], s1['range'][1] + 1, s1['increment'])
        slope0 = s0['value']
        slope1 = s1['value']
        values = iter([[value0, value1] for value0 in slope0 for value1 in slope1])
        self.__test_values_in_jobs(values, jobs, 'deterministic',
                ['/geometry/slope_sections/upslope/0/angle/value', '/geometry/slope_sections/upslope/1/angle/value'])

    def test_success_parameters_deterministic(self):
        """ Test that successful task splitting of deterministic parameters results in correct state changes and job docs """
        task_doc = _get_fake_task_doc(task_id='foo', type='deterministic')
        self.__test_success_parameters_deterministic(task_doc)

    def test_success_parameters_deterministic_range(self):
        """ Test that successful task splitting of deterministic parameter range results in correct state changes and job docs """
        task_doc = _get_fake_task_doc(task_id='foo', type='deterministic_range')
        self.__test_success_parameters_deterministic(task_doc)

    def test_success_parameters_deterministic_stochastic(self):
        """ Test that successful task splitting of job with deterministic and stochastic parameters results in correct number of jobs """
        task_doc = _get_fake_task_doc(task_id='foo', type='stochastic')
        task_doc['simulation_parameters']['deterministic'] = _get_fake_task_doc(task_id='foo', type='deterministic')['simulation_parameters']['deterministic']
        self.__commit_and_execute_plugin(task_doc)
        self.__test_doc_states((120, 120, 'in_progress')) # Just check there are 120 job docs

    def test_fail_too_many_jobs(self):
        """ Test that task is rejected if number of resulting jobs exceeds job limit """
        task_doc = _get_fake_task_doc(task_id='foo', type='stochastic')
        task_doc['simulation_parameters']['deterministic'] = _get_fake_task_doc(task_id='foo', type='deterministic')['simulation_parameters']['deterministic']
        self.__commit_and_execute_plugin(task_doc, options={'job_limit': 100})
        self.__test_doc_states((0, 0, 'rejected - too many jobs: 120'))
        _check_doc_state_history(self, self.dbs['task']['db'], 'foo',
                {'rejected': {'timestamp': 0, 'modified_by': 'Task'}})

class TestPluginJob(TestPlugin):
    """ Test job sentinel plugin """
    def setUp(self):
        super(TestPluginJob, self).setUp()
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'state-machine'), self.dbs['state']['name'], self.url)

    def __get_mock_submitter(self, response):
        """ Create a mock job submitter that returns the supplied error status """
        mock_submitter = Mock()
        mock_submitter.submit.return_value = response
        mock_submitter_module = Mock()
        mock_submitter_module.SubmitterResponse = self.SubmitterResponse
        mock_submitter_module.get_submitter.return_value = mock_submitter
        return mock_submitter_module

    def __get_jobs_by_state(self, key, expected):
        """ Helper function - hits the jobs_by_state view with supplied key """
        return results_on_precondition(function=self.dbs['state']['db'].loadView,
                args=['state-machine', 'jobs_by_state', {'include_docs': True, 'key': key, 'reduce': False}],
                test=lambda x:len(x['rows']) == expected, wait=self.wait, retry=self.retry)['rows']

    def test_success(self):
        """ Test that successful job submission results in correct state change """
        self.dbs['state']['db'].commitOne(_get_fake_state_doc(job_id='foo'), w=__QUORUM__['n'])
        work = results_on_precondition(function=plugins.Job.get_work,
                    args=[{'state': self.dbs['state']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 1)
        job_plugin = plugins.Job(self.dbs['task']['db'], self.dbs['state']['db'], self.logger, quorum=__QUORUM__)
        job_plugin(work[0]['doc'], self.__get_mock_submitter(response=self.SubmitterResponse.OK))
        self.assertEqual(len(self.__get_jobs_by_state('ready_to_submit', 0)), 0)
        self.assertEqual(len(self.__get_jobs_by_state('submitted', 1)), 1)
        state_doc_id = self.dbs['state']['db'].loadView('state-machine', 'states_by_job', {'include_docs': True, 'key': 'foo'})['rows'][0]['id']
        _check_doc_state_history(self, self.dbs['state']['db'], state_doc_id,
                {
                    'ready_to_submit': {'timestamp': 1300188082, 'modified_by': 'Task'},
                    'splitting': {'timestamp': 1300187895, 'modified_by': 'Task'},
                    'submitted': {'timestamp': 0, 'modified_by': 'Job'}
        })

    def __test_failure_hard(self, error):
        """ Helper for test_failure_bad_job and test_failure_unknown """
        self.dbs['state']['db'].commitOne(_get_fake_state_doc(job_id='foo'), w=__QUORUM__['n'])
        work = results_on_precondition(function=plugins.Job.get_work,
                    args=[{'state': self.dbs['state']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 1)
        job_plugin = plugins.Job(self.dbs['task']['db'], self.dbs['state']['db'], self.logger, quorum=__QUORUM__)
        job_plugin(work[0]['doc'], self.__get_mock_submitter(response=error))
        self.__test_state_docs({'ready_to_submit': 0, 'submitted': 0, 'failed_submit': 1})
        job_doc = self.dbs['state']['db'].loadView('state-machine', 'states_by_job', {'include_docs': True, 'key': 'foo'})['rows'][0]['doc']
        self.assertTrue('failed_submit' in job_doc['state_history'])

    def test_failure_bad_job(self):
        """ Test that a failed job submission due to a bad job results in correct state change """
        self.__test_failure_hard(self.SubmitterResponse.ERROR_JOB)

    def test_failure_unknown(self):
        """ Test that a failed job submission due to unknown results in correct state change """
        self.__test_failure_hard(self.SubmitterResponse.ERROR_UNKNOWN)

    def __test_state_docs(self, states):
        """ Helper - checks number of docs in given states """
        for state, num in states.items():
            self.assertEquals(len(self.__get_jobs_by_state(state, num)), num)

    def __submit_and_test(self, job_plugin, retry, max_retries):
        """ Helper for test_failure_backend_error - gets work, calls plugin, checks state """
        work = results_on_precondition(function=plugins.Job.get_work,
                    args=[{'state': self.dbs['state']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 1)
        job_plugin(work[0]['doc'], self.__get_mock_submitter(response=self.SubmitterResponse.ERROR_BACKEND))
        time.sleep(5) # Wait for the cluster to settle...
        job_doc = self.dbs['state']['db'].loadView('state-machine', 'states_by_job', {'include_docs': True, 'key': 'foo'})['rows'][0]['doc']
        self.assertEquals(job_doc['submit_fail_count'], retry + 1)
        if retry < max_retries:
            self.__test_state_docs({'ready_to_submit': 1, 'submitted': 0, 'failed_submit': 0})
        else:
            self.__test_state_docs({'ready_to_submit': 0, 'submitted': 0, 'failed_submit': 1})
            self.assertTrue('failed_submit' in job_doc['state_history'])

    def test_failure_backend_error(self):
        """ Test that a failed job submission due to a backend error results in correct retry strategy """
        self.dbs['state']['db'].commitOne(_get_fake_state_doc(job_id='foo'), w=__QUORUM__['n'])
        job_plugin = plugins.Job(self.dbs['task']['db'], self.dbs['state']['db'], self.logger, quorum=__QUORUM__, retries=2)
        for i in range(0, 3):
            self.__submit_and_test(job_plugin, retry=i, max_retries=2)

    def test_jobs_not_submitted_once_max_jobs_exceeded(self):
        """ Once a maximum number of submitted jobs, no further jobs are submitted """
        state_doc = _get_fake_state_doc(task_id='foo')
        state_doc['state'] = 'submitted'
        state_doc['state_history']['submitted'] = {"timestamp": 1300188252, "modified_by": "Job"}
        task1_state_docs = [self.dbs['state']['db'].commitOne(state_doc, w=__QUORUM__['n'])[0] for i in range(10)]
        task2_state_docs = [self.dbs['state']['db'].commitOne(_get_fake_state_doc(task_id='bar'), w=__QUORUM__['n'])[0] for i in range(10)]
        self.__test_state_docs({'ready_to_submit': 10, 'submitted': 10})
        job_plugin = plugins.Job(self.dbs['task']['db'], self.dbs['state']['db'], self.logger, quorum=__QUORUM__, options={'job_limit': 15})
        work = results_on_precondition(function=plugins.Job.get_work,
                    args=[{'state': self.dbs['state']['db']}],
                    test=lambda x:len(x) == 5, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 5)
        for work_item in work:
            job_plugin(work_item['doc'], self.__get_mock_submitter(response=self.SubmitterResponse.OK))
        self.__test_state_docs({'ready_to_submit': 5, 'submitted': 15})
        self.dbs['state']['db'].delete_doc(id=task1_state_docs[0]['id'], rev=task1_state_docs[0]['rev'], w=__QUORUM__['n']) # Simulate a task completing
        self.__test_state_docs({'ready_to_submit': 5, 'submitted': 14})
        work = results_on_precondition(function=plugins.Job.get_work,
                    args=[{'state': self.dbs['state']['db']}],
                    test=lambda x:len(x) == 1, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 1)
        for work_item in work:
            job_plugin(work_item['doc'], self.__get_mock_submitter(response=self.SubmitterResponse.OK))
        self.__test_state_docs({'ready_to_submit': 4, 'submitted': 15})
        work = results_on_precondition(function=plugins.Job.get_work,
                    args=[{'state': self.dbs['state']['db']}],
                    test=lambda x:len(x) == 0, wait=self.wait, retry=self.retry)
        self.assertEquals(len(work), 0)

if __name__ == '__main__':
    unittest.main(verbosity=2)