""" mossaicparser_t.py

Unit tests for the mossaicparser module.

"""

import json
import os
import unittest

from mossaic.mossaicparser import MossaicParser
from mossaic.mossaicparser import MossaicParserException

from mossaic.mossaicparser import Stability, BoundaryConditions, Soils, Geometry, EngineeringCost

INPUT_FILES_JSON = os.path.join('..', 'data', 'input_files')     # MOSSAIC input files, not test input files
OUTPUT_FILES = os.path.join('..', 'data', 'output_files')   # Ditto for output files
INPUT_FILES_RAW = os.path.join('..', 'couchapps', 'mossaic', 'shows', 'expected')

class TestMossaicParser(unittest.TestCase):
    """ Test the MossaicParser superclass for all parsers """
    def test_null_filename(self):
        """ Test MossaicParser fails gracefully if null filename is provided """
        self.assertRaises(IOError, MossaicParser.get_parser, '')
    
    def test_unknown_file_type(self):
        """ Test MossaicParser fails gracefully if unknown file type is provided """
        mystery_file = os.path.join(OUTPUT_FILES, 'mystery.filename')
        self.assertRaises(MossaicParserException, MossaicParser.get_parser, mystery_file)
    
    def test_no_file(self):
        """ Test MossaicParser fails gracefully if file does not exist """
        self.assertRaises(IOError, MossaicParser.get_parser, 'non_existent.out')

class TestQuestaOutputSummary(unittest.TestCase):
    """ Test the parser for QUESTA output summary files """
    def setUp(self):
        self.file_map = {
            'bad_output_summary': {
                'test_input': os.path.join(OUTPUT_FILES, 'bad_output_summary.out')
            },
            'output_summary': {
                'test_input': os.path.join(OUTPUT_FILES, 'output_summary.out'),
                'expected_output': os.path.join(OUTPUT_FILES, 'output_summary.json')
            },
            'output_summary_no_failure': {
                'test_input': os.path.join(OUTPUT_FILES, 'output_summary_no_failure.out'),
                'expected_output': os.path.join(OUTPUT_FILES, 'output_summary_no_failure.json')
            },
            'output_summary_partial_failure': {
                'test_input': os.path.join(OUTPUT_FILES, 'output_summary_partial_failure.out'),
                'expected_output': os.path.join(OUTPUT_FILES, 'output_summary_partial_failure.json')
            }
        }
        self.maxDiff = None
    
    def __get_expected_output(self, filename):
        """ Helper method - opens file, parses json and pops the '_id' element which we don't want in the output """
        expected_data = {}
        with open(filename) as output_summary:
            expected_data = json.loads(output_summary.read())
        return expected_data
    
    def tearDown(self):
        pass
    
    def test_success(self):
        """ Test QuestaOutputSummary successfully parses .out file """
        parser = MossaicParser.get_parser(self.file_map['output_summary']['test_input'])
        output_summary = json.loads(json.dumps(parser.parse())) # Dump and load means diff is easier to understand if test fails
        expected_output = self.__get_expected_output(self.file_map['output_summary']['expected_output'])
        self.assertEqual(output_summary, expected_output)
    
    def test_success_no_failure(self):
        """ Test QuestaOutputSummary successfully parses .out file that contains no failure """
        parser = MossaicParser.get_parser(self.file_map['output_summary_no_failure']['test_input'])
        output_summary = json.loads(json.dumps(parser.parse()))
        expected_output = self.__get_expected_output(self.file_map['output_summary_no_failure']['expected_output'])
        self.assertEqual(output_summary, expected_output)

    def test_success_partial_failure(self):
        """ Test QuestaOutputSummary successfully parses .out file that contains partial failure data """
        parser = MossaicParser.get_parser(self.file_map['output_summary_partial_failure']['test_input'])
        output_summary = json.loads(json.dumps(parser.parse())) # Dump and load means diff is easier to understand if test fails
        expected_output = self.__get_expected_output(self.file_map['output_summary_partial_failure']['expected_output'])
        self.assertEqual(output_summary, expected_output)
    
    def test_bad_file(self):
        """ Test QuestaOutputSummary fails gracefully if given a bad output file """
        parser = MossaicParser.get_parser(self.file_map['bad_output_summary']['test_input'])
        self.assertRaises(MossaicParserException, parser.parse)

class TestFactorOfSafety(unittest.TestCase):
    """ Test the parser for CHASM/QUESTA factor of safety (.fos) files """
    def setUp(self):
        self.file_map = {
            'bad_fos': {
                'test_input': os.path.join(OUTPUT_FILES, 'bad_fos.fos')
            },
            'fos': {
                'test_input': os.path.join(OUTPUT_FILES, 'fos.fos'),
                'expected_output': os.path.join(OUTPUT_FILES, 'fos.json')
            },
            'fos_extra_hr_col': {
                'test_input': os.path.join(OUTPUT_FILES, 'fos_extra_hr_col.fos'),
                'expected_output': os.path.join(OUTPUT_FILES, 'fos.json')
            }
        }
        self.maxDiff = None

    def tearDown(self):
        pass

    def test_success(self):
        """ Test FactorOfSafety successfully parses factor of safety file """
        parser = MossaicParser.get_parser(self.file_map['fos']['test_input'])
        fos = json.loads(json.dumps(parser.parse(), sort_keys=True))
        expected_output = json.loads(file(self.file_map['fos']['expected_output']).read())
        self.assertEqual(fos, expected_output)

    def test_success_extra_hr_col(self):
        """ Test FactorOfSafety successfully parses factor of safety file with extra End Hr column (current Chasm format - known bug) """
        parser = MossaicParser.get_parser(self.file_map['fos_extra_hr_col']['test_input'])
        fos = json.loads(json.dumps(parser.parse(), sort_keys=True))
        expected_output = json.loads(file(self.file_map['fos_extra_hr_col']['expected_output']).read())
        self.assertEqual(fos, expected_output)

    def test_bad_file(self):
        """ Test FactorOfSafety fails gracefully if given a bad output file """
        parser = MossaicParser.get_parser(self.file_map['bad_fos']['test_input'])
        self.assertRaises(MossaicParserException, parser.parse)


class TestInputFileParser(unittest.TestCase):
    """ Test parsers for CHASM/QUESTA input files """
    def setUp(self):
        self.file_map = {}
        self.maxDiff = None
        self.Parser = None

    def _amend_expected_output(self, expected_output):
        """ Make any pre-comparison changes to the expected output (removing _id, but may be overridden by subclasses) """
        expected_output.pop('_id')
        expected_output.pop('name')

    def test_success(self):
        """ Test parser successfully parses input files """
        for input_file_type, input_output in self.file_map.items():
            print 'Testing ' + input_file_type
            parser = self.Parser(input_output['test_input'])
            if 'expected_output' in input_output:
                actual_output = json.loads(json.dumps(parser.parse(), sort_keys=True))
                expected_output = json.loads(file(input_output['expected_output']).read())
                if 'parent_id' in expected_output:
                    expected_output.pop('parent_id')
                self._amend_expected_output(expected_output)
                self.assertEqual(actual_output, expected_output)
            else:
                self.assertRaises(ValueError, parser.parse)


class TestStability(TestInputFileParser):
    """ Test the parser for CHASM/QUESTA stability files """
    def setUp(self):
        super(TestStability, self).setUp()
        self.Parser = Stability
        self.file_map = {
            'bishop': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'stability', 'success_bishop'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'stability_bishop.json')
            },
            'janbu_automated': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'stability', 'success_janbu_automated'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'stability_janbu_automated.json')
            },
            'janbu_userdef': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'stability', 'success_janbu_userdef'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'stability_janbu_userdef.json')
            },
            'bad_file': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'engineering_cost', 'success')
            }
        }


class TestBoundaryConditions(TestInputFileParser):
    """ Test the parser for CHASM/QUESTA boundary conditions files """
    def setUp(self):
        super(TestBoundaryConditions, self).setUp()
        self.Parser = BoundaryConditions
        self.file_map = {
            'deterministic': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'boundary_conditions', 'success_deterministic'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'boundary_conditions_deterministic.json')
            },
            'stochastic': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'boundary_conditions', 'success_stochastic'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'boundary_conditions_stochastic.json')
            },
            'questa_design_single': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'boundary_conditions', 'success_questa_design_single'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'boundary_conditions_questa_design_single.json')
            },
            'questa_design_series': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'boundary_conditions', 'success_questa_design_series'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'boundary_conditions_questa_design_series.json')
            },
            'questa_design_realistic': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'boundary_conditions', 'success_questa_realistic'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'boundary_conditions_questa_realistic.json')
            },
            'bad_file': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'engineering_cost', 'success')
            }
        }


class TestSoils(TestInputFileParser):
    """ Test the parser for CHASM/QUESTA soils files """
    def setUp(self):
        super(TestSoils, self).setUp()
        self.Parser = Soils
        self.file_map = {
            'deterministic': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'soils', 'success_deterministic'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'soils_deterministic.json')
            },
            'stochastic': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'soils', 'success_stochastic'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'soils_stochastic.json')
            },
            'deterministic_grade': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'soils', 'success_deterministic_grade'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'soils_deterministic_grade.json')
            },
            'stochastic_grade': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'soils', 'success_stochastic_grade'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'soils_stochastic_grade.json')
            },
            'bad_file': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'engineering_cost', 'success')
            }
        }

class TestGeometry(TestInputFileParser):
    """ Test the parser for QUESTA and CHASM geometry files """
    def setUp(self):
        super(TestGeometry, self).setUp()
        self.Parser = Geometry
        self.file_map = {
            'questa_soil_approximated': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'geometry', 'success_questa_soil_approximated'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'geometry_questa_soil_approximated.json')
            },
            'questa_soil_known': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'geometry', 'success_questa_soil_known'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'geometry_questa_soil_known.json')
            },
            'bad_file': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'engineering_cost', 'success')
            },
            'chasm': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'geometry', 'success'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'geometry.json')
            }
        }

    def _amend_expected_output(self, expected_output):
        """ Amend expected output """
        super(TestGeometry, self)._amend_expected_output(expected_output)
        if 'road' in expected_output['geometry']:
            expected_output['geometry']['road']['upslope_shoulder']['value'] = 3.3000000000000007    # Hack - as we calculate this ourselves we have fp issues
        if 'location' in expected_output['geometry']:
            expected_output['geometry'].pop('location')


class TestEngineeringCost(TestInputFileParser):
    """ Test the parser for QUESTA engineering cost files """
    def setUp(self):
        super(TestEngineeringCost, self).setUp()
        self.Parser = EngineeringCost
        self.file_map = {
            'engineering_cost': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'engineering_cost', 'success'),
                'expected_output': os.path.join(INPUT_FILES_JSON, 'engineering_cost.json')
            },
            'bad_file': {
                'test_input': os.path.join(INPUT_FILES_RAW, 'geometry', 'success_questa_soil_known')
            }
        }


class TestResults(unittest.TestCase):
    """ Test the parser for CHASM/QUESTA slope cell (.results) files """
    def setUp(self):
        self.file_map = {
            '03_001': {
                'test_input': os.path.join(OUTPUT_FILES, '03_001.results'),
                'expected_output': os.path.join(OUTPUT_FILES,
                    '03_001.results.json')
            }
        }
        self.maxDiff = None

    def tearDown(self):
        pass

    def test_success(self):
        """ Test FactorOfSafety successfully parses factor of safety file """
        parser = MossaicParser.get_parser(self.file_map['03_001']['test_input'])
        results = json.loads(json.dumps(parser.parse()))
        expected_output = json.loads(file(self.file_map['03_001']['expected_output']).read())
        self.assertEqual(results, expected_output)


if __name__ == '__main__':
    unittest.main(verbosity=2)