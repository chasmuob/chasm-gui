#!/usr/bin/env python
"""
This unit test isn't really a unit test, more an explaination of how to use the
CMSCouch library to work with CouchDB. It currently assumes you have a running
CouchDB instance, and is not going to work in an automated way just yet - we'll
need to add Couch as an external, include it in start up scripts etc.

These unit tests _should_ pass against a BigCouch cluster configured for
eventual consistency with read-your-writes quorum constants (r + w > n).
Unfortunately the way this is done is by sleeping after a write. We do this
because BigCouch currently does not support supplying quorum constants
in either _bulk_docs or _all_docs queries which are used extensively in
this test. This should change in future BigCouch releases but for now
we rely on writes propagating whilst we sleep (not always guaranteed).

Once quorum constants are supported in _bulk_docs and _all_docs we
can remove all the time.sleep calls.

"""

from couch.Couch import CouchServer, Document, Database, CouchType
from couch.CouchRequest import CouchInternalServerError, CouchNotFoundError, CouchConflictError
import random
import unittest
import os
import subprocess
import sys
import time
import hashlib
import base64
import StringIO
import tarfile

from couchtest_helpers import upload_couchapp

class CMSCouchTest(unittest.TestCase):
    test_counter = 0
    def setUp(self):
        # Make an instance of the server
        self.url = os.getenv("COUCHURL", 'http://admin:pass@localhost:5984')
        self.server = CouchServer(self.url)
        testname = self.id().split('.')[-1]
        # Create a database, drop an existing one first
        self.dbname = 'cmscouch_unittest_%s' % testname.lower()

        if self.dbname in self.server.listDatabases():
            self.server.deleteDatabase(self.dbname)

        self.server.createDatabase(self.dbname)
        self.db = self.server.connectDatabase(self.dbname)

    def tearDown(self):
        if sys.exc_info()[0] == None:
            # This test has passed, clean up after it
            testname = self.id().split('.')[-1]
            dbname = 'cmscouch_unittest_%s' % testname.lower()
            self.server.deleteDatabase(dbname)

    def testShowWithOptions(self):
        """
        Test retrieving the results of a CouchDB Show function that has been passed an option.
        """
        upload_couchapp(os.path.join('couchapps', 'test_show'), self.dbname, self.url)
        self.assertEqual('spam,eggs,extra detail', self.db.loadShow('test_show', 'get_as_text', 'test doc', {'details': 'true'}))

    def testShow(self):
        """
        Test retrieving the results of a CouchDB Show function.
        """
        upload_couchapp(os.path.join('couchapps', 'test_show'), self.dbname, self.url)
        self.assertEqual('spam,eggs', self.db.loadShow('test_show', 'get_as_text', 'test doc'))

    def testCommitOne(self):
        # Can I commit one dict
        doc = {'foo':123, 'bar':456}
        id = self.db.commitOne(doc, returndocs=True)[0]['id']
        # What about a Document
        doc = Document(dict=doc)
        id = self.db.commitOne(doc, returndocs=True)[0]['id']

    def testCommitOneWithQueue(self):
        """
        CommitOne bypasses the queue, but it should maintain the queue if
        present for a future call to commit.
        """
        # Queue up five docs
        doc = {'foo':123, 'bar':456}
        for i in range(1,6):
            self.db.queue(doc)
        # Commit one Document
        doc = Document(dict=doc)
        id = self.db.commitOne(doc, returndocs=True)[0]['id']
        time.sleep(1)
        self.assertEqual(1, len(self.db.allDocs()['rows']))
        self.db.commit()
        time.sleep(1)
        self.assertEqual(6, len(self.db.allDocs()['rows']))

    def testTimeStamping(self):
        doc = {'foo':123, 'bar':456}
        id = self.db.commitOne(doc, timestamp=True, returndocs=True)[0]['id']
        time.sleep(1)
        doc = self.db.document(id)
        self.assertTrue('timestamp' in doc.keys())

    def testDeleteDoc(self):
        doc = {'foo':123, 'bar':456}
        self.db.commitOne(doc)
        time.sleep(1)
        all_docs = self.db.allDocs()
        self.assertEqual(1, len(all_docs['rows']))

        # The db.delete_doc is immediate
        id = all_docs['rows'][0]['id']
        self.db.delete_doc(id)
        time.sleep(1)
        all_docs = self.db.allDocs()
        self.assertEqual(0, len(all_docs['rows']))

    def testDeleteQueuedDocs(self):
        doc1 = {'foo':123, 'bar':456}
        doc2 = {'foo':789, 'bar':101112}
        self.db.queue(doc1)
        self.db.queue(doc2)
        self.db.commit()
        time.sleep(1)
        all_docs = self.db.allDocs()
        self.assertEqual(2, len(all_docs['rows']))
        for res in all_docs['rows']:
            id = res['id']
            doc = self.db.document(id)
            self.db.queueDelete(doc)

        all_docs = self.db.allDocs()
        self.assertEqual(2, len(all_docs['rows']))

        self.db.commit()
        time.sleep(1)
        all_docs = self.db.allDocs()
        self.assertEqual(0, len(all_docs['rows']))

    def testWriteReadDocNoID(self):
        doc = {}

    def testSlashInDBName(self):
        """
        Slashes are a valid character in a database name, and are useful as it
        creates a directory strucutre for the couch data files.
        """
        db_name = 'wmcore/unittests'
        try:
            self.server.deleteDatabase(db_name)
        except:
            # Ignore this - the database shouldn't already exist
            pass

        db = self.server.createDatabase(db_name)
        info = db.info()
        assert info['db_name'] == db_name

        db_name = 'wmcore/unittests'
        db = self.server.connectDatabase(db_name)
        info = db.info()
        assert info['db_name'] == db_name

        self.server.deleteDatabase(db_name)

    def testInvalidName(self):
        """
        Capitol letters are not allowed in database names.
        """
        db_name = 'Not A Valid Name'
        self.assertRaises(ValueError, self.server.createDatabase, db_name)
        self.assertRaises(ValueError, self.server.deleteDatabase, db_name)
        self.assertRaises(ValueError, self.server.connectDatabase, db_name)
        self.assertRaises(ValueError, Database, db_name)

    def __add_attachment_ignore_conflict(self, id, rev, attachment, **kwargs):
        """ Add attachment and ignore 409 response

        See https://wikis.bris.ac.uk/display/mossaicgrid/BigCouch+cluster+tests for rationale

        """
        try:
            return self.db.addAttachment(id, rev, attachment, **kwargs)
        except CouchConflictError:
            # If we got a conflict here we ignore and just get the newest id and rev to return
            doc = self.db.document(id)
            return {'id': doc['_id'], 'rev': doc['_rev']}

    def testAttachments(self):
        """
        Test uploading attachments with and without checksumming
        """
        doc = self.db.commitOne({'foo':'bar'}, timestamp=True, returndocs=True)[0]
        attachment1 = "Hello"
        attachment2 = "How are you today?"
        attachment3 = "I'm very well, thanks for asking"
        attachment4 = "Lovely weather we're having"
        attachment5 = "Goodbye"
        keyhash = hashlib.md5()
        keyhash.update(attachment5)
        attachment5_md5 = keyhash.digest()
        attachment5_md5 = base64.b64encode(attachment5_md5)
        attachment6 = "Good day to you, sir!"
        doc = self.__add_attachment_ignore_conflict(doc['id'], doc['rev'], attachment1)
        doc = self.__add_attachment_ignore_conflict(doc['id'], doc['rev'], attachment2, contentType="foo/bar")
        doc = self.__add_attachment_ignore_conflict(doc['id'], doc['rev'], attachment3, name="my_greeting")
        doc = self.__add_attachment_ignore_conflict(doc['id'], doc['rev'], attachment4, add_checksum=True)
        doc = self.__add_attachment_ignore_conflict(doc['id'], doc['rev'], attachment5, checksum=attachment5_md5)

        self.assertRaises(CouchInternalServerError, self.__add_attachment_ignore_conflict, doc['id'], doc['rev'], attachment6, checksum='123')

    def testGetAttachments(self):
        """
        Test retrieving uploaded attachment
        """
        doc = self.db.commitOne({'foo':'bar'}, timestamp=True, returndocs=True)[0]
        attachment = "Hello"
        doc = self.__add_attachment_ignore_conflict(doc['id'], doc['rev'], attachment, name="my attachment")
        retrieved_attachment = self.db.getAttachment(doc['id'], name="my attachment")
        self.assertEqual(attachment, retrieved_attachment)

    def testAttachmentsBinary(self):
        """
        Test uploading of binary attachments
        """
        doc = self.db.commitOne({'foo':'bar'}, timestamp=True, returndocs=True)[0]
        tar_buffer = StringIO.StringIO()
        tar_file = tarfile.open('test', fileobj=tar_buffer, mode='w:gz')
        tar_file.add(__file__, arcname='test')
        tar_file.close()
        doc = self.__add_attachment_ignore_conflict(doc['id'], doc['rev'], tar_buffer.getvalue(), contentType='application/gzip-compressed')
        self.assertEqual(self.db.getAttachment(doc['id']), tar_buffer.getvalue())

    def testReplicate(self):
        repl_db = self.server.connectDatabase(self.db.name + 'repl')

        doc_id = self.db.commitOne({'foo':123}, timestamp=True, returndocs=True)[0]['id']
        time.sleep(1)
        doc_v1 = self.db.document(doc_id)
        #replicate
        self.server.replicate(self.db.name, repl_db.name)

        self.assertEqual(self.db.document(doc_id), repl_db.document(doc_id))
        self.server.deleteDatabase(repl_db.name)

    def testRevisionHandling(self):
        # This test won't work from an existing database, as conflicts will be preserved, so
        # ruthlessly remove the databases to get a clean slate.
        try:
            self.server.deleteDatabase(self.db.name)
        except CouchNotFoundError:
            pass # Must have been deleted already

        try:
            self.server.deleteDatabase(self.db.name + 'repl')
        except CouchNotFoundError:
            pass # Must have been deleted already

        # I'm going to create a conflict, so need a replica db
        self.db = self.server.connectDatabase(self.db.name)
        repl_db = self.server.connectDatabase(self.db.name + 'repl')

        # Push couchapp containing our test view to the db and replica
        upload_couchapp(os.path.join('couchapps', 'test_revision_handling'), self.dbname, self.url)
        upload_couchapp(os.path.join('couchapps', 'test_revision_handling'), self.dbname + 'repl', self.url)

        doc_id = self.db.commitOne({'foo':123}, timestamp=True, returndocs=True)[0]['id']
        time.sleep(1)
        doc_v1 = self.db.document(doc_id)
        #replicate
        self.server.replicate(self.db.name, repl_db.name)

        doc_v2 = self.db.document(doc_id)
        doc_v2['bar'] = 456
        doc_id_rev2 = self.db.commitOne(doc_v2, returndocs=True)[0]
        doc_v2 = self.db.document(doc_id)

        #now update the replica
        conflict_doc = repl_db.document(doc_id)
        conflict_doc['bar'] = 101112
        repl_db.commitOne(conflict_doc)
        time.sleep(1)
        #replicate, creating the conflict
        self.server.replicate(self.db.name, repl_db.name)
        data = repl_db.loadView('test_revision_handling', 'conflicts')

        # Should have one conflict in the repl database
        self.assertEquals(data['total_rows'], 1)
        # Should have no conflicts in the source database
        self.assertEquals(self.db.loadView('test_revision_handling', 'conflicts')['total_rows'], 0)
        self.assertTrue(repl_db.documentExists(data['rows'][0]['id'], rev=data['rows'][0]['key'][0]))

        repl_db.delete_doc(data['rows'][0]['id'], rev=data['rows'][0]['key'][0])
        time.sleep(1)
        data = repl_db.loadView('test_revision_handling', 'conflicts')

        self.assertEquals(data['total_rows'], 0)
        self.server.deleteDatabase(repl_db.name)

        #update it again
        doc_v3 = self.db.document(doc_id)
        doc_v3['baz'] = 789
        doc_id_rev3 = self.db.commitOne(doc_v3, returndocs=True)[0]
        doc_v3 = self.db.document(doc_id)

        #test that I can pull out an old revision
        doc_v1_test = self.db.document(doc_id, rev=doc_v1['_rev'])
        self.assertEquals(doc_v1, doc_v1_test)

        #test that I can check a revision exists
        self.assertTrue(self.db.documentExists(doc_id, rev=doc_v2['_rev']))

        self.assertFalse(self.db.documentExists(doc_id, rev='1'+doc_v2['_rev']))

        #why you shouldn't rely on rev
        if (self.server.couch_type == CouchType.COUCHDB):
            self.db.compact(blocking=True)
            self.assertFalse(self.db.documentExists(doc_id, rev=doc_v1['_rev']))
            self.assertFalse(self.db.documentExists(doc_id, rev=doc_v2['_rev']))
            self.assertTrue(self.db.documentExists(doc_id, rev=doc_v3['_rev']))

    def testBigCouchCompactionNotSupported(self):
        """
        If we have a BigCouch instance, tests that compaction calls fail with a NotImplementedError
        """
        if (self.server.couch_type == CouchType.BIGCOUCH):
            self.assertRaises(NotImplementedError, self.db.compact)

    def testNanValuesError(self):
        """
        Floating point values of inf should json-serialize to Infinity and commit to Couch with no errors
        """
        self.assertRaises(ValueError, self.db.commitOne, {'_id': 'infdoc', 'value': float('inf')})
        self.assertRaises(ValueError, self.db.commitOne, {'_id': 'infdoc', 'value': float('NaN')})

if __name__ == "__main__":
    unittest.main(verbosity=2)
