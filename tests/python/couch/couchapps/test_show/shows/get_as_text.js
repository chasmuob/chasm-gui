function(doc, req) {
    var returnData = [doc.foo, doc.bar];
    log(req.query);
    if (req.query.details == "\"true\"") {
        returnData.push("extra detail");
    }
    return {
        "headers": {
          "Content-Type": "text/plain"
        },
        "body": returnData.join(",")
    }
}