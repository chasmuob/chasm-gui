#!/bin/bash 

# Simple script which uploads a bunch of json files
# Needs extending in fairly obvious ways but this may be good enough

if [ $1 ]; then
    if [ `curl -X GET $1 | grep no_db_file` ]; then
        curl -X PUT $1
    fi
    for file in *.json;
    do
        echo $file
        curl -X POST $1 -d "@$file" -H "Content-Type:application/json"
    done
else
    echo "Usage: push_json.sh database_url"
fi
