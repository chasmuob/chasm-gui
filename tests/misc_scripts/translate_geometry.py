#!/usr/local/bin/python

import json
import sys

g = json.load(file(sys.argv[1]))

new_geometry = []

def translate_col(col):
    new_cells = {
        'cell_depth': {
            'unit': 'm',
            'value': []
        },
        'soil_type': []
    }
    def translate_cell(cell):
        new_cells['cell_depth']['value'].append(cell['cell_depth']['value'])
        new_cells['soil_type'].append(cell['soil_type'])
    map(translate_cell, col['cells'])
    col['cells'] = new_cells
    new_geometry.append(col)

map(translate_col, g['geometry'])

g['geometry'] = new_geometry

print json.dumps(g, indent=4)