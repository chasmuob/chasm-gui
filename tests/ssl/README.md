# SSL keys/certificates

These are SSL keys and certificates for test purposes.

Contents:

 - ca.crt Certificate for our dummy CA
 - kairos.localdomain.key/cert Server key/certificate, signed by dummy CA
 - userkey/cert.pem User key/certificate, signed by dummy CA

These can be used to set up a test SSL proxy, which should be configured
to use SSL certificate authentication.
