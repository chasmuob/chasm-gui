Testing couchapps with python
=============================

Easy
----

 - documents
 - views
 - shows
 - lists
 - validations

Harder
------

 - UI javascript
 - evently callbacks

Rather than test directly we define a tight interface for all data (model) and interactions (controller) and have unit tests for these. 

Why not mock?
-------------

Time consuming. No benefit as any devs will have a local couch anyway.

Ideas
-----

Would be useful to have a profiler that just measures response time for a URL. Can then be used to quicly performance test different view/show/list implementations.

Could profile all unit test times (and log results in couchdb...?).

Framework
---------

 - Delete/recreate test db
 - Push CouchApp
 - Push data required for test
 - Call list/show/view under test via URL
 - Optional: record response time somewhere
 - Check response as required by test:
  - Compare to expected correct value, or;
  - Compare to expected error doc, or;
  - Compare to http error code.

This is so generic that we can probably just have a json file that looks something like this:
    {
        "metadata": {couch instance, couchapp location, etc},
        "name_of_test": {
            "type": "list/view/show/whatevs",
            "query_name": "steering",
            "target": "doc_id, view params, etc",
            "expected_output": "path/to/outputdoc",
            "expected_error_code": "http error code"
        },
        ...more tests...
    }

Not sure how to modularise - one doc per show/list/etc under test?
One doc per test?
One doc per feature?

Thought: Since these docs are .json they too can live in a couchdb instance. Benefits?

For more elaborate tests we can do $STUFF and then just check whatever documents are created by $STUFF.
