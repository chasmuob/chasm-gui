#!/usr/bin/python

""" run_tests

Runs all tests for this couchapp.

Couchapp-specific information is stored in config.json in the root of the test tree.
Tests are specified in json files in each directory for each function type (show/view/list).

"""

import base64
import glob
import json
import optparse
import os
import shutil
import subprocess
import sys
import time
import unittest
import urllib
import urlparse

import httplib2
from couch import Couch, CouchRequest
from couchtest_helpers import upload_couchapp, get_url_and_auth

__COUCHAPPS_TO_TEST__ = ['mossaic', 'workflow', 'state-machine', 'release',
                                        'gui', 'mining', 'gui-js', 'mining-js']
__SUPPORTED_COUCH_FUNCTIONS__ = ['shows', 'views', 'lists']
__VERSION__ = '0.1'
__CONFIG__ = 'config.json'
__USAGE_STRING__ = """Usage: python run_tests.py [FUNCTIONS,...]

Where FUNCTIONS is either "all" or one or more of the following:
    shows views lists"""
__RETRIES__ = 30    # Number of times to retry the function under test
                    # We would want to retry if testing against an eventually-consistent bigcouch cluster
__WAIT__ = 1
__QUORUM__ = {
    'q': os.getenv('QUORUM_Q'),
    'n': os.getenv('QUORUM_N'),
    'r': os.getenv('QUORUM_R'),
    'w': os.getenv('QUORUM_W')
}

def assert_nested_almost_equal(test, item1, item2, places=7):
    """ Test supplied nested items (dict or list) are almost equal

    Just return true/false, don't actually fail the test, to allow for
    retrying

    """
    if type(item1) is dict and type(item2) is dict:
        keys = item1.keys()
        if keys != item2.keys():
            return False
        for key in keys:
            if (type(item1[key]) is dict and type(item2[key]) is dict) or\
                    (type(item1[key]) is list and type(item2[key]) is list):
                return assert_nested_almost_equal(test, item1[key], item2[key], places=places)
            elif type(item1[key]) == float:
                try:
                    test.assertAlmostEqual(item1[key], item2[key], places=places)
                except:
                    print sys.exc_info()[2]
                    return False
            else:
                return item1[key] == item2[key]
    elif type(item1) is list and type(item2) is list:
        if len(item1) != len(item2):
            return False
        for i in range(len(item1)):
            if (type(item1[i]) is dict and type(item2[i]) is dict) or\
                    (type(item1[i]) is list and type(item2[i]) is list):
                return assert_nested_almost_equal(test, item1[i], item2[i], places=places)
            elif type(item1[i]) == float:
                try:
                    test.assertAlmostEqual(item1[i], item2[i], places=places)
                except:
                    print sys.exc_info()[2]
                    return False
            else:
                return item1[i] == item2[i]
    else:
        print "Insane types provided: %s and %s" % (type(item1), type(item2))
        return False

def get_test_class(config, spec, basedir):
    """ Create a TestCase class for a given CouchApp function

    Arguments:
    config -- a configuration object for the CouchApp test suite which must contain:
        - test_server: host name of the development server (usually localhost)
        - test_db: name of the test database
        - couchapp_path: path to the source code for the CouchApp under test
        - couchapp_target: name of the desired target as defined in .couchapprc
        - test_data: base directory for data required by tests for this CouchApp
    spec -- a specification object for the test case which must contain:
        - design_doc: the name of the design document containing the target function
        - function_type: the type (show/view/list) of the target function
        - function_under_test: the name of the function under test
        - test_data: list of files relative to config.test_data that are required by this TestCase
    basedir -- path of the base directory for the couchapp under test, relative to the run_tests.py script

    Note: We dynamically generate the class using the type(name, bases, dict) method which allows us to give it a name related to the CouchApp function
    under test. This is less clear than using class ClassName(base) syntax however the tradeoff is that the visible output from the test runner is much
    more useful.

    """
    class_name = str(''.join((spec['design_doc'].capitalize(), spec['function_type'].capitalize(), spec['function_under_test'].capitalize())))
    CouchAppTest = type(class_name, (unittest.TestCase,), {})

    def setUp(self):
        """ Create fresh test database, populate with data required by test case and create instance variables for test case """
        testname = self.id().split('.')[-1]
        url = os.getenv("COUCHURL", config['test_server'])
        self.server = Couch.CouchServer(url, timeout=0)
        self.db_name = str(''.join((config['test_db_prefix'], '-', spec['function_type'], '-', spec['function_under_test'], '-', testname)))
        self.url_base, auth = get_url_and_auth(url, self.db_name)
        try:
            self.server.deleteDatabase(self.db_name)
        except CouchRequest.CouchNotFoundError:
            pass
        db = self.server.connectDatabase(self.db_name)
        upload_couchapp(couchapp_target=os.path.join(basedir, config['couchapp_path']), target_db=self.db_name, url=url)
        for test_file in spec['test_data']:
            test_data = json.loads(file(os.path.join(basedir, config['test_data'], test_file), 'r').read())
            db.commitOne(test_data, w=__QUORUM__['n'])
        self.url_loader = httplib2.Http()
        if auth:
            self.headers = {'Authorization': 'Basic %s' % auth}
        else:
            self.headers = {}

    def tearDown(self):
        """ Delete test database only if test has passed """
        if sys.exc_info()[0] == None:
            self.server.deleteDatabase(self.db_name)

    CouchAppTest.setUp = setUp
    CouchAppTest.tearDown = tearDown
    return CouchAppTest

def get_test_function(spec, testname, basedir):
    """ Create a test method for a given CouchApp function

    Arguments:
    spec -- a specification object for the test case which must contain:
        - function_type: type of CouchDB function under test (show/view/list)
        - function_under_test: name of CouchDB function under test
        - design_doc: name of design document containing the function under test
        - tests: a dictionary of specified tests where key = testname and value contains the following keys:
            - body: name of a file containing the expected HTTP content
            - request_id: name of show/list/view being requested
            - code: expected http status code
            - content-type: expected http content-type
    testname -- name of the test to be created
    basedir -- path of the base directory for the couchapp under test, relative to the run_tests.py script

    """
    test_spec = spec['tests'][testname]

    def test_function(self):
        """ Generic test function for CouchApps

        Retrieves a resource and compares resulting http code, body and content-type against expected values.
        This docstring is intentionally re-written by the description field in the test specification file.

        """
        expected_output = file(os.path.join(basedir, ''.join((spec['function_type'], 's')), 'expected', test_spec['body'])).read()
        url = str(''.join((self.url_base, '/_design/', spec['design_doc'], '/_', spec['function_type'], '/',
                spec['function_under_test'])))
        if spec['function_type'] == 'list' and 'view_name' in spec:
            url = "%s/%s" % (url, spec['view_name'])
        if 'request_id' in test_spec:
            url = '%s/%s' % (url, test_spec['request_id'])
        if 'options' in test_spec:
            encodedOptions = {}
            encoder = json.JSONEncoder()
            for k,v in test_spec['options'].iteritems():
                encodedOptions[k] = encoder.encode(v)
            url = '%s?%s' % (url, urllib.urlencode(encodedOptions, doseq=True))
        for i in range(__RETRIES__):    # Retry until we get a pass - this allows tests to run against eventually consistent bigcouch cluster
            response, actual_output = self.url_loader.request(url, method="GET", headers=self.headers)
            if spec['function_type'] == 'show' or spec['function_type'] == 'list':
                if actual_output == expected_output:
                    self.assertEqual(response['status'], str(test_spec['code']))
                    self.assertEqual(response['content-type'], test_spec['content_type'])
                    return
            elif spec['function_type'] == 'view':
                results_equal = False
                if 'almost_equal' in test_spec and test_spec['almost_equal']:
                    places = 7
                    if 'places' in test_spec:
                        places = test_spec['places']
                    results_equal = assert_nested_almost_equal(self, json.loads(actual_output), json.loads(expected_output), places=places)
                else:
                    results_equal = json.loads(actual_output) == json.loads(expected_output)
                if results_equal:
                    self.assertEqual(response['status'], str(test_spec['code']))
                    self.assertEqual(response['content-type'], test_spec['content_type'])
                    return
        self.assertEqual(self.url_loader.request(url, method="GET", headers=self.headers)[1], expected_output) # Last ditch attempt...

    test_function.__doc__ = test_spec['description']
    return test_function

def get_couch_functions(options, args):
    """ Get the CouchDB function types to be tested from the command line arguments """
    if 'all' in args:
        return __SUPPORTED_COUCH_FUNCTIONS__
    else:
        couch_functions = [couch_function for couch_function in args if couch_function in __SUPPORTED_COUCH_FUNCTIONS__]
        if len(couch_functions) < 1:
            parser.error('No supported couch functions were supplied')
        else:
            return couch_functions

def parse_args():
    """ Return options and args """
    couch_functions = []
    couchapps = []
    parser = optparse.OptionParser(usage=__USAGE_STRING__, version=__VERSION__)
    parser.add_option("-c", "--couchapps", dest="couchapps",
            help="Comma separated list of couchapps to be tested")
    options, args = parser.parse_args()
    if len(args) < 1:
        parser.error('Incorrect number of arguments')
    if 'all' in args:
        couch_functions = __SUPPORTED_COUCH_FUNCTIONS__
    else:
        couch_functions = [couch_function for couch_function in args if couch_function in __SUPPORTED_COUCH_FUNCTIONS__]
    if len(couch_functions) < 1:
        parser.error('No supported couch functions were supplied')
    if (options.couchapps):
        couchapps = options.couchapps.split(",")
    else:
        couchapps = __COUCHAPPS_TO_TEST__
    return couch_functions, couchapps

def build_test_suite(couch_functions, couchapp_name):
    """ Create test suite for specified couch functions using module-level __CONFIG__ """
    config = json.loads(file(os.path.join(couchapp_name, __CONFIG__)).read())
    test_suite = unittest.TestSuite()

    for function_dir in couch_functions:
        specfiles = glob.glob(os.path.join(couchapp_name, function_dir, '*json'))
        for specfile in specfiles:
            spec = json.loads(file(specfile).read())
            CouchTest = get_test_class(config, spec, basedir=couchapp_name)

            for testname in spec['tests']:
                test_function = get_test_function(spec, testname, basedir=couchapp_name)
                setattr(CouchTest, testname, test_function)

            test_suite.addTests(unittest.TestLoader().loadTestsFromTestCase(CouchTest))

    return test_suite

def run_tests():
    """ Build test suite and run tests, returning the result to the caller """
    couch_functions, couchapps_to_test = parse_args()
    test_suites = [build_test_suite(couch_functions, couchapp) for couchapp in couchapps_to_test]
    return [unittest.TextTestRunner(verbosity=2).run(test_suite) for test_suite in test_suites]

if __name__ == '__main__':
    if True in [len(result.failures) > 0 or len(result.errors) > 0 for result in run_tests()]:
        exit(1)
    else:
        exit(0)