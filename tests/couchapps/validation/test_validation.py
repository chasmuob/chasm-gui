""" test_validation

Test the validate_doc_update functions in couchapps.

Users and roles are defined in self.test_data['users']. Test documents, and the rules regarding who can update them, are defined
in self.test_data using a specific key (e.g. 'state_files').

"""

import json
import os
import sys
import time
import unittest

import httplib2

from couch import Couch, CouchRequest
from couchtest_helpers import upload_couchapp, get_url_and_auth, create_userdoc

__COUCHAPP_DIR__ = os.path.join('..', '..', '..', 'src', 'couchapps')
__INPUT_FILES__ = os.path.join('..', '..', 'data', 'input_files')
__STATE_FILES__ = os.path.join('..', '..', 'data', 'state_files')
__QUORUM__ = {
    'q': os.getenv('QUORUM_Q'),
    'n': os.getenv('QUORUM_N'),
    'r': os.getenv('QUORUM_R'),
    'w': os.getenv('QUORUM_W')
}

def _get_server_for_user(url, url_base, dbname, username, users):
    """ Helper function which returns CouchServer based on URL with appropriate auth details for user """
    if username in users:
        userconf = users[username]
        user_url = "".join(("http://", ":".join((username, userconf['password'])), "@", url_base.split("http://")[1].split('/' + dbname)[0]))
    elif username == 'user_none':
        user_url = url_base.split('/' + dbname)[0]
    elif username == 'admin':
        user_url = url
    return Couch.CouchServer(user_url)

class TestValidation(unittest.TestCase):
    def setUp(self):
        """ Create couch instance and prepare """
        # Make an instance of the server
        self.url = os.getenv("COUCHURL", 'http://admin:pass@localhost:5984')
        self.server = Couch.CouchServer(self.url)
        testname = self.id().split('.')[-1]
        # Create a database, drop an existing one first
        self.dbname = 'validation_unittest_%s' % testname.lower()

        if self.dbname in self.server.listDatabases():
            self.server.deleteDatabase(self.dbname)

        self.server.createDatabase(self.dbname)
        self.db = self.server.connectDatabase(self.dbname)

        # This isn't pretty but it handles the regular and BigCouch style _users DB.
        # We need to call with create=True first to trigger a CouchBadRequestError if the _users DB does not exist on regular port
        # we then look for it on the admin port.
        # If the _users DB is on the regular port but is the BigCouch _users DB we get CouchPreconditionFailedError when using create=True, so
        # we re-call with create=False.
        # If the _users DB is a regular CouchDB instance then the original statement passes anyway.
        try:
            self.db_users = self.server.connectDatabase('_users')
        except CouchRequest.CouchUnauthorisedError:
            self.db_users = self.server.connectDatabase('_users', create=False)
        except CouchRequest.CouchPreconditionFailedError:
            self.db_users = self.server.connectDatabase('_users', create=False)
        except CouchRequest.CouchBadRequestError:
            url_no_port, port = ':'.join(self.url.split(':')[:-1]), self.url.split(':')[-1]
            admin_url = ':'.join((url_no_port, str(int(port) + 2))) # Assume admin port is 2 higher than regular port
            self.db_users = Couch.CouchServer(admin_url).connectDatabase('_users')

        self.url_base, auth = get_url_and_auth(self.url, self.dbname)

        self.test_data = {
            'users': {
                'user_no_roles': {
                    'roles': [],
                    'password': 'pwd_no_roles'
                },
                'user_task_requestor': {
                    'roles': ['requestor'],
                    'password': 'pwd_task_requestor',
                },
                'user_job_manager': {
                    'roles': ['job_manager'],
                    'password': 'pwd_job_manager'
                },
                'user_job_wrapper': {
                    'roles': ['job_wrapper'],
                    'password': 'pwd_job_wrapper'
                },
                'user_mossaicadmin': {
                    'roles': ['mossaicadmin'],
                    'password': 'pwd_admin'
                }
            },
            'input_files': {
                'deterministic_task_definition.json': {
                    'user_none': False,
                    'user_no_roles': False,
                    'user_task_requestor': True,
                    'user_job_manager': True,
                    'user_job_wrapper': False,
                    'admin': True
                },
                'job_definition.json': {
                    'user_none': False,
                    'user_no_roles': False,
                    'user_task_requestor': False,
                    'user_job_manager': True,
                    'user_job_wrapper': True,
                    'admin': True
                },
                'job_definition_done.json': {
                    'user_none': False,
                    'user_no_roles': False,
                    'user_task_requestor': False,
                    'user_job_manager': True,
                    'user_job_wrapper': True,
                    'admin': True
                },
                'job_definition_failed.json': {
                    'user_none': False,
                    'user_no_roles': False,
                    'user_task_requestor': False,
                    'user_job_manager': True,
                    'user_job_wrapper': True,
                    'admin': True
                }
            },
            'state_files': {
                'ready_to_submit.json': {
                    'user_none': False,
                    'user_no_roles': False,
                    'user_task_requestor': False,
                    'user_job_manager': True,
                    'user_job_wrapper': False,
                    'admin': True
                },
                'failed.json': {
                    'user_none': False,
                    'user_no_roles': False,
                    'user_task_requestor': False,
                    'user_job_manager': True,
                    'user_job_wrapper': True,
                    'admin': True
                },
                'submitted.json': {
                    'user_none': False,
                    'user_no_roles': False,
                    'user_task_requestor': False,
                    'user_job_manager': True,
                    'user_job_wrapper': False,
                    'admin': True
                },
                'splitting.json': {
                    'user_none': False,
                    'user_no_roles': False,
                    'user_task_requestor': False,
                    'user_job_manager': True,
                    'user_job_wrapper': False,
                    'admin': True
                },
                'done.json': {
                    'user_none': False,
                    'user_no_roles': False,
                    'user_task_requestor': False,
                    'user_job_manager': True,
                    'user_job_wrapper': True,
                    'admin': True
                }
            }
        }

        self.__populate_db()
        self.maxDiff=None

    def __delete_users(self, users):
        """ Helper function - delete specified users """
        for user in users:
            try:
                self.db_users.delete_doc(id='org.couchdb.user:' + user, w=__QUORUM__['n'])
            except CouchRequest.CouchError:
                # Do nothing
                pass

    def __create_users(self, users):
        """ Helper function - creates users with roles """
        # Delete existing users
        self.__delete_users(users.keys())
        uuids = iter(self.db_users.getUuids(count=len(users)))
        for user, user_conf in users.items():
            self.db_users.commitOne(create_userdoc(user, user_conf['roles'], user_conf['password'], uuids.next()), w=__QUORUM__['n'])
        # Committing to users DB means talking directly to the BigCouch admin port - this means no quorum
        # We therefore resort to non-deterministic "sleep until all changes have probably replicated" method
        time.sleep(5)

    def _upload_couchapp(self):
        pass

    def __populate_db(self):
        """ Helper function - adds the users and uploads the couchapp """
        self._upload_couchapp();
        self.__create_users(self.test_data['users'])

    def tearDown(self):
        self.__delete_users(self.test_data['users'].keys())
        if sys.exc_info()[0] == None:
            # This test has passed, clean up after it
            self.server.deleteDatabase(self.dbname)

    def _get_urls(self, username):
        """ Helper method which returns urls with and without user auth credentials """
        auth_pass = self.test_data['users'][username]['password']
        auth_url = "".join(("http://", ":".join((username, auth_pass)), "@", self.url_base.split("http://")[1].split('/' + self.dbname)[0]))
        unauth_url = self.url_base.split('/' + self.dbname)[0]
        return auth_url, unauth_url


class TestValidationWorkflow(TestValidation):
    def _upload_couchapp(self):
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'workflow'), self.dbname, self.url)

    def testValidationTaskAndJob(self):
        """ Test validation function as specified by self.test_data """
        for input_file, expected_result in self.test_data['input_files'].items():
            input_doc = json.loads(file(os.path.join(__INPUT_FILES__, input_file)).read())
            input_doc.pop('_id') # We don't want a manual _id for this test
            for user in expected_result:
                user_server = _get_server_for_user(self.url, self.url_base, self.dbname, user, self.test_data['users'])
                user_db = user_server.connectDatabase(self.dbname)
                if user == 'user_none':
                    self.assertRaises(CouchRequest.CouchUnauthorisedError, user_db.commitOne, (input_doc))
                else:
                    if expected_result[user]:
                        self.assertTrue('error' not in user_db.commitOne(input_doc)[0])
                    else:
                        self.assertRaises(CouchRequest.CouchForbidden, user_db.commitOne, (input_doc))

    def testValidationAnyDoc(self):
        """ Test validation function for any type of document """
        auth_url, unauth_url = self._get_urls('user_no_roles')
        responseAuth = Couch.CouchServer(auth_url).connectDatabase(self.dbname).commitOne({'empty': 'doc'})[0]
        self.assertTrue('error' not in responseAuth)
        self.assertRaises(CouchRequest.CouchUnauthorisedError, Couch.CouchServer(unauth_url).connectDatabase(self.dbname).commitOne, ({'empty': 'doc'}))


class TestValidationAuth(TestValidation):
    def _upload_couchapp(self):
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'auth'), self.dbname, self.url)

    def testValidationAnyDoc(self):
        """ Test validation function for any type of document """
        auth_url, unauth_url = self._get_urls('user_no_roles')
        responseAuth = Couch.CouchServer(auth_url).connectDatabase(self.dbname).commitOne({'empty': 'doc'})[0]
        self.assertTrue('error' not in responseAuth)
        self.assertRaises(CouchRequest.CouchUnauthorisedError, Couch.CouchServer(unauth_url).connectDatabase(self.dbname).commitOne, ({'empty': 'doc'}))

    def testNoUpdatesForRegularUsers(self):
        """ Test that regular users can create but not update documents """
        auth_url, unauth_url = self._get_urls('user_no_roles')
        doc = {'_id': 'doc', 'empty': 'doc'}
        db = Couch.CouchServer(auth_url).connectDatabase(self.dbname)
        meta = db.commitOne(doc)[0]
        doc['_rev'] = meta['rev']
        doc['empty'] = 'this field has been changed'
        self.assertRaises(CouchRequest.CouchUnauthorisedError, db.commitOne, doc)

    def testUpdatesAllowedForSpecialUsers(self):
        """ Test that admins and jobwrapper user can create and update documents """
        for user in ['user_mossaicadmin', 'user_job_wrapper']:
            auth_url, unauth_url = self._get_urls(user)
            doc = {'_id': 'doc_%s' % user, 'empty': 'doc'}
            db = Couch.CouchServer(auth_url).connectDatabase(self.dbname)
            meta = db.commitOne(doc)[0]
            doc['_rev'] = meta['rev']
            doc['empty'] = 'this field has been changed'
            responseAuth = db.commitOne(doc)
            self.assertTrue('error' not in responseAuth)

class TestValidationAuthJs(TestValidationAuth):
    def _upload_couchapp(self):
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'auth-js'), self.dbname, self.url)


class TestValidationStateMachine(TestValidation):
    def _upload_couchapp(self):
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'state-machine'), self.dbname, self.url)

    def testValidationState(self):
        """ Test validation function as specified by self.test_data """
        for input_file, expected_result in self.test_data['state_files'].items():
            input_doc = json.loads(file(os.path.join(__STATE_FILES__, input_file)).read())
            input_doc.pop('_id') # We don't want a manual _id for this test
            for user in expected_result:
                user_server = _get_server_for_user(self.url, self.url_base, self.dbname, user, self.test_data['users'])
                user_db = user_server.connectDatabase(self.dbname)
                if user == 'user_none':
                    self.assertRaises(CouchRequest.CouchUnauthorisedError, user_db.commitOne, (input_doc))
                else:
                    if expected_result[user]:
                        self.assertTrue('error' not in user_db.commitOne(input_doc)[0])
                    else:
                        self.assertRaises(CouchRequest.CouchForbidden, user_db.commitOne, (input_doc))

    def testValidationAnyDoc(self):
        """ Test validation function for any type of document """
        auth_url, unauth_url = self._get_urls('user_no_roles')
        self.assertRaises(CouchRequest.CouchForbidden, Couch.CouchServer(auth_url).connectDatabase(self.dbname).commitOne, ({'empty': 'doc'}))
        self.assertRaises(CouchRequest.CouchUnauthorisedError, Couch.CouchServer(unauth_url).connectDatabase(self.dbname).commitOne, ({'empty': 'doc'}))


class TestValidationRelease(TestValidation):
    def _upload_couchapp(self):
        upload_couchapp(os.path.join(__COUCHAPP_DIR__, 'release'), self.dbname, self.url)

    def testValidationAnyDoc(self):
        """ Test validation function for any type of document """
        auth_url, unauth_url = self._get_urls('user_no_roles')
        responseAuth = Couch.CouchServer(auth_url).connectDatabase(self.dbname).commitOne({'empty': 'doc'})[0]
        self.assertTrue('error' not in responseAuth)
        self.assertRaises(CouchRequest.CouchUnauthorisedError, Couch.CouchServer(unauth_url).connectDatabase(self.dbname).commitOne, ({'empty': 'doc'}))


if __name__ == '__main__':
    unittest.main(verbosity=2)