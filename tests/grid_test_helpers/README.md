Grid test helpers
-----------------

Code that is needed for running grid-based tests.

Currently this is just a copy of httplib2. We need this so that we can reliably dump it on a worker node.

On a deployed version we can grab httplib2 from the python site-packages directory.