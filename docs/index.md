# MoSSaiC Application Platform {{VERSION}}

Welcome to the online documentation for the MoSSaiC Application Platform (MAP).

## Overview

The documentation is divided into two parts:

 - [User documentation](user)
  - Start here if you want to access simulation/modelling software through the web interface
 - [Developer/operations documentation](dev)
  - Start here if you are interested in developing/maintaining/operating the platform

## What is MAP?

MAP is a data and workflow management platform based on [Apache CouchDB](http://couchdb.apache.org/) (and compatible with its clustered fork, [BigCouch](http://bigcouch.cloudant.com/)). It is essentially a Software-as-a-Service platform which is capable of providing access to executable software running on thousands of compute nodes through a simple web interface.

Currently two related applications are available through the platform:

 - CHASM
  - Models the saturated and unsaturated slope hydrology using a finite difference model coupled to a slope stability search
  - Determines the size and runout of landslides in a slope
 - QUESTA
  - Computes the economic impact of a landslide
  - Uses CHASM to model landslides in a slope and a separate econometric model to determine the impact

## How do I use it?

MAP is accessed via a web GUI. This provides the following functions:

 - Composing and submitting simulation task requests
 - Creating and viewing simulation inputs
 - Viewing and downloading simulation results
 - Viewing status of simulation tasks
 - Uploading datasets

See the [user documentation](user) for instructions on how to use the web GUI.

## How does it work?

When a task request is submitted a document (the task definition) is written to CouchDB that specifies:

 - The application and version that should be run
 - The simulation inputs to be used
 - The duration of the simulation
 - The number of individual jobs that should be created

This task definition document is read by a sentinel process which creates a job definition document for every individual job. If stochastic parameters are specified then these are generated for each job and stored in the job definition document.

> A quick note on stochastic parameters: The seed used to generate the
> stochastic parameters is saved in the task definition. This means that the
> complete simulation can be recreated at a later date by re-generating the
> exact same parameters for each job definition.

A second sentinel process reads the job definition documents and submits a job request (essentially a [JSDL](http://www.gridforum.org/documents/GFD.56.pdf) document) to a third-party workload management system. Currently only one system, [DIRAC](http://diracgrid.org/), is supported. DIRAC is configured with various compute resources, the exact details of which are the concern of the DIRAC system administrator. DIRAC ensures that the job request is executed on a suitable worker node.

The job itself is a script that:

 - Downloads the job definition document
 - Downloads the input datasets
 - Downloads the executable
 - Runs the executable
 - Parses results and uploads them to CouchDB
 - Updates the status of the job appropriately

Once the job results have been uploaded and the job status updated, the job terminates. The results can then be viewed through the GUI using the task analyser.

A third sentinel monitors the status of the jobs and marks task definition documents as "done" once all jobs belonging to that task are completed.

A fourth sentinel, which is not related to the workflow but should be mentioned here for completeness, looks for uploaded input datasets in raw CHASM/QUESTA formats and converts them to JSON so they can be viewed and edited in the GUI.

See the [developer/operations documentation](dev) for more detail.

## Health warning

Please be aware that this is demonstration software, made available for development and test purposes only.

Bug reports should be sent to <{{ADMIN_EMAIL}}>.