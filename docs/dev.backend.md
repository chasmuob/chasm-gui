# MoSSaiC Application Platform Developer/Operations Documentation - Back-end

## Resources

 - [PyDoc-generated documentation for Python back-end](python/mossaic.html)

## Overview

The backend consists of the following components:

 - Four sentinel programs (daemons written in Python) that interact with an Apache CouchDB-compatible database and a DIRAC instance
 - A master process which spawns the sentinels
 - A job wrapper which is submitted to DIRAC via a JSDL request
 - A modified version of the CMSCouch library, used for accessing Apache CouchDB-compatible databases
 - A standalone version of simplejson which is bundled with the submitted job so that it can be used on worker nodes with older versions of Python

## MAP Sentinels

### Master

File: `src/python/mossaic/jobmanager/master.py`

The master sentinel is responsible for spawning the four sentinel processes and controlling them in response to OS signals from the operator. The operator interacts with the master sentinel using the `src/python/mossaic/jobmanager/init.d/jobmanager` script which sends the required OS signals to the master.

The Python [multiprocessing module](http://docs.python.org/2/library/multiprocessing.html) is used in order to avoid the GIL, which means each sentinel is an entirely separate OS process. Communication between the master and the actual sentinels is via an OS pipe.

### Sentinels

File `src/python/mossaic/jobmanager/sentinel.py`

The individual sentinels are instances of the Sentinel class, which defines a Master process that uses a plugin (defined in `src/python/mossaic/jobmanager/plugins.py`) to poll the database for work. When work is found it is dispatched to a pool of worker threads which use process the work using the plugin.

Four plugins are defined (one for each sentinel):

 - Task
  - Polls for simulation requests, determines the jobs required to fulfil each request, and writes the job definitions for each job into the database
  - Users the task splitters defined in `src/python/mossaic/jobmanager/splitters.py` to break task requests down into multiple jobs
 - Job
  - Polls for job definitions and submits each job definition to DIRAC
  - Uses the job submitters defined in `src/python/mossaic/jobmanager/submitter.py` to submit to a compute resource (currently DIRAC)
  - The job submitter uses `src/python/mossaic/jobmanager/proxy_factory.py` to generate proxy credentials that the jobs can use to authenticate to the load balancer via SSL authentication
  - The job submitted by the submitter includes a python wrapper script (see `src/python/mossaic/jobwrapper.py`) and parsers for CHASM/QUESTA output files (see `src/python/mossaic/mossaicparsers.py`)
 - Tracker
  - Polls for tasks where all related jobs are in state `done`, closes the task and triggers an index build in the database
 - Input
  - Polls for raw CHASM/QUESTA input and converts it into JSON
  - Raw input is parsed using parsers defined in `src/python/mossaic/mossaicparsers.py`

Each plugin has a `get_work` classmethod which is used to poll for work. Each plugin is also callable, so work is processed by calling the plugin and passing the work as a parameter.

