# MoSSaiC Application Platform Developer/Operations Documentation

## Resources

 - [The main README file](README)
 - [The INSTALL file](INSTALL)
 - [Documentation for the HTML5 GUI](dev.frontend)
 - [Documentation for the Python back-end](dev.backend)
 - [Documentation for operators/system administrators](dev.ops)

## Overview

The MAP platform is based on [Apache CouchDB](http://couchdb.apache.org/) and consists of two main components:

 - An HTML5 application, served directly out of Apache CouchDB, which provides a GUI for creating task requests and browsing/editing/creating domain-specific data. This is located in the MAP source distribution in `src/couchapps/gui`.
 - The Job Manager - a back-end written in Python which consists of four daemons that interact with documents in CouchDB, handling task splitting, job submission, task tracking and converting uploaded input data to JSON. This is located in the MAP source distribution in `src/python`.

Both the HTML5 GUI and the backend depend on various CouchDB view and show functions which are defined in `src/couchapps`.

## Aside: What is a CouchApp?

The term CouchApp originated as the name for a tool that transformed files stored on a filesystem into a CouchDB design document. These files could contain anything from a single map function for a CouchDB view to a full-blown HTML5 application.

It has also been fairly commonplace to refer to an HTML5 application being served directly out of CouchDB as a "CouchApp", though this creates confusion by implying that such a CouchApp is somehow different to a regular HTML5 application. It isn't.

The term CouchApp, as used in this documentation, refers to a single set of files that are transformed into a single CouchDB design document by the CouchApp tool. These are all found in the `src/couchapps` directory and are described in more detail elsewhere.

## Architecture

From the front-end perspective the basic architecture is two-tier, with a browser-based application tier and a CouchDB-based data tier. A third "tier", the python back-end, is essentially a worker process which operates on data and state defined in the data tier.

The asynchronous replication provided by CouchDB means that this architecture can support two separate usage models which are discussed below.

### Client/server usage model

As with a typical web service, remote users access a central CouchDB instance using their web browser. The HTML5 application is loaded into their browser and AJAX requests transfer data between the browser and the central CouchDB instance.

### Ground computing usage model

A central CouchDB instance exists but remote users also have their own local CouchDB instances. These contains replicas of the central database and CouchDB is configured to continuously replicate between the local and central instances.

Users access their local CouchDB instance using their web browser and all AJAX requests are between their web browser and their local CouchDB instance. The asynchronous replication built into CouchDB ensures that the local CouchDB instance is updated whenever the central CouchDB instance is updated with new input/output data. Similarly, task requests made to the local CouchDB instance will be replicated to the central CouchDB instance so that the Job Manager can handle them appropriately.

This usage model has implications for security which are discussed further in the [operations documentation](dev.ops).

## Databases

MAP uses six separate databases which are automatically created by the deployment script if they do not already exist. These are:

 - Data (e.g. `map`)
  - User generated input data and model-generated output data
  - The HTML5 application that provides the GUI
 - Task (e.g. `map_task`)
  - Task definition documents and job definition documents
 - State (e.g. `map_state`)
  - Temporal state data for individual jobs
 - Release (e.g. `map_release`)
  - Metadata for the modelling applications
 - Release (private) (e.g. `map_release_private`)
  - Metadata for the modelling applications and *the executables themselves*
 - Login (e.g. `map_login`)
  - Publicly accessible HTML5 application which allows users to log in

A seventh databases, Docs (e.g. `map_docs`), is used to host the documentation.

The private release database is the only place where the actual executables for the modelling applications are stored. Access is restricted by the deployment script to the credentials used by the job wrapper only, so regular users cannot access the executables themselves. The contents of the metadata should be the same which will be the case as long as all releases are uploaded using the map-release-upload.py script found in `scripts/utils`.

## CouchApps

There are nine CouchApps which are pushed into the six databases defined above by the deployment script.

<table>
  <tr><th>Name   </th><th>Target DBs  </th><th>Design Doc Language</th><th class="desc">Description</th></tr>
  <tr><td>auth   </td><td>Data        </td><td>JavaScript, Erlang</td><td>Restricts document updates to logged-in-users only</td></tr>
  <tr><td>docs   </td><td>Docs        </td><td>N/A</td><td>Provides user, developer and operations documentation</td></tr>
  <tr><td>gui    </td><td>Data        </td><td>JavaScript, Erlang</td><td>Provides the HTML5 GUI application and the map/reduce views required to support it</td></tr>
  <tr><td>login  </td><td>Login       </td><td>N/A</td><td>Publicly accessible DB that provides an HTML5 application allowing users to log in</td></tr>
  <tr><td>mining </td><td>Data        </td><td>JavaScript, Erlang</td><td>Defines map/reduce views for analysing model outputs</td></tr>
  <tr><td>mossaic</td><td>Data        </td><td>JavaScript</td><td>Defines show functions that convert input data from JSON into the formats required by CHASM/QUESTA</td></tr>
  <tr><td>release</td><td>Release, Release (private)</td><td>JavaScript</td><td>Provides a view for listing available releases of modelling software</td></tr>
  <tr><td>state-machine</td><td>State </td><td>JavaScript</td><td>Defines map/reduce views for tracking job and task states</td></tr>
  <tr><td>workflow</td><td>Task</td><td>JavaScript</td><td>Defines map/reduce views for tracking task requests and a show function for rendering job definitions in the format required by CHASM/QUESTA</td></tr>
</table>

Three CouchApps, auth, gui and mining, have two implementations - one using native Erlang view/show functions and one using JavaScript view/show functions. The deployment script will try to deploy the Erlang versions by default as they are preferred for performance reasons. It is possible to manually specify the JavaScript versions be installed, which would be necessary if deploying to a CouchDB instance where support for native Erlang views is not enabled.

## Supported platforms

This software is tested and supported on the following platforms:

 - Scientific Linux 5.x
 - Scientific Linux 6.x.
 
It should work with no modifications on any RHEL-based Linux distro. It should also work on other Linux and Unix platforms, though configuring dependencies may be a little tricky.