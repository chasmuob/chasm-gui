# MoSSaiC Application Platform User Documentation

Welcome to the user documentation for the MoSSaiC Application Platform web interface - a simple way of requesting CHASM and QUESTA simulations and viewing results.

## Supported features

 * Support for CHASM and QUESTA modelling software
 * Support for simple (1 task:1 job) task requests
 * Support for complex task requests with the following stochastic parameters:
  * Soil parameters:
   * Cohesion
   * Angle of internal friction
   * Ksat
  * Soil strata depths (measured or estimated) (QUESTA simulations only)
  * Water table depths (QUESTA simulations only)
 * Forms and graphical tools for creating and browsing input datasets
 * Task level visualisations:
  * Factor of safety envelope
 * Job level visualisations:
  * Slope cell moisture content and pore pressure
 * Input data upload in native format for stability, boundary conditions, soils, geometry and engineering costs data
 * Task requests can be built from uploaded input data, or new data can be entered and saved with the task request
 * A simple means of viewing task status is provided

## A quick note about the data model

### Overview

The MoSSaiC Application Platform uses a simple data model which makes it easy to find the relevant input datasets for a particular simulation.

         Slope datum
              |
             /|\
      Slope cross section
              |
             /|\
         Slope profile
              |
             /|\
          Stability

The top level dataset is the slope datum, which will have one or more slope cross sections. Each cross section can have one or more slope profiles. A slope profile can have zero, one or more stability datasets.

The remaining datasets are simulation parameter datasets. These are:

 - Soil types
 - Vegetation (currently unsupported)
 - Reinforcements (currently unsupported)
 - Boundary conditions
 - Engineering cost (QUESTA only)
 - Road network (QUESTA only)

There are restrictions on the simulation parameters that can be used for any given slope profile. The user interface will show only those datasets that are applicable to the currently selected slope profile.

The restrictions are as follows:

 - Soil types: The number of soil types defined must be equal to or greater than the number of soil strata in the slope profile

All other datasets are currently unrestricted.

### Practicalities

The nature of the data model means that if you have a particular slope datum selected, you will only be able to select slope cross sections that are associated with the selected datum. Furthermore, any new cross sections will be associated with the selected datum when they are saved. This also applies to all other related datasets, e.g. slope cross sections and slope profiles, slope profiles and stability.

## Login/signup

Enter your username and password in the box on the right of the login page and click the _Login_ button to log in. If you do not have an account click the _Signup_ link and enter your desired username and password.

Note that new accounts cannot create new simulation requests by default - you will need to contact the system administrator to arrange this.

## Quick start

There are four basic tasks that can be accessed from the home screen:

 * Task composer
  * Create new simulations from new or existing data
 * Task analyser
  * View results of simulation tasks
 * Task status
  * View the status of a task
 * Data uploader
  * Upload input datasets

### Task composer

The task composer workflow is as follows:

 * Select a datum and cross-section (new datum and cross-section datasets can be created if required)
 * Select an existing slope profile for the selected cross-section *or* create a new slope profile by choosing a simulation application and entering the number of sections required
 * If you are creating a new slope:
  * For a CHASM simulation you will then need to set the datum location relative to the slope profile - you can either select an existing point, or choose a location on the surface, in which case the selecting section will be split at the selected point
  * Define each slope section by entering two of: Section angle, surface length, section height
  * Make any final adjustments to the slope
  * Enter soil strata and water table data
  * The slope profile needs to be saved before the workflow can advance to the simulation parameters, so you will be prompted for a name. Note that once saved a slope profile cannot be modified further. You can, however, unlock the profile and save any edits under a new name by navigating to a slope profile tab.
 * Editing simulation parameters
  * Simulation parameters can be edited once a valid slope profile is selected. These are soil types, boundary conditions, stability and for QUESTA, engineering costs and road network.
 * Once the last set of parameters have been selected or saved, you will advance to the task finalise stage where you can review the task:
  * Enter a name for the task and provide any notes you want to include
  * Provide names for any new or modified datasets
  * Submit the task

Note: You will not be able to submit a simulation request unless you are logged in and you have the appropriate privileges. If you cannot submit simulation requests and you think you should be able to please e-mail the system administrator.

### Task analyser

The task analyser workflow is as follows:

 * Choose a slope datum and cross section
 * Select a slope profile for the chosen datum and cross section
 * Select a task associated with the chosen slope profile
 * Choose from available visualisations:
  * Factor of Safety
   * A factor of safety envelope for all jobs in the selected task
  * Slope cells
   * The moisture content and pore pressure of each cell at each time step for a single job from the selected task

### Task status

The task status workflow is as follows:

 * Choose a slope datum and cross section
 * Select a slope profile for the chosen datum and cross section
 * View the job status for the chosen task

### Data uploader

The data upload workflow is as follows:

 * Select the type of input data you want to upload
 * If the data you are uploading requires associating with existing data, you will need to choose one or more of the following:
  * Slope datum
  * Slope cross section
  * Slope profile
 * Provide a name for the input dataset
 * Choose the file
 * Click the upload button

Note that it will take a few minutes before uploaded input data becomes available for use in simulation tasks.

## Health warning

Please be aware that this is beta software made available for development and test purposes only.