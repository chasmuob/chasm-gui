# MoSSaiC Application Platform Developer/Operations Documentation - Front-end

## Resources

 - [JSDoc-generated documentation for HTML5 front-end](js/Mossaic.html)

## Overview

The MAP GUI is an HTML5 application which is served directly out of CouchDB. It makes heavy use of the following JavaScript libraries:

> [Backbone.js](http://backbonejs.org/) - an MVC-like framework for
> stucturing the app

> [underscore.js](http://underscorejs.org/) - functional programming
> support for the JavaScript language

> [d3.js](http://d3js.org/) - data visualisation library

> [mustache.js](https://github.com/janl/mustache.js/) - logic-less templating

It also uses [Twitter Bootstrap](http://twitter.github.com/bootstrap/) for layout and styles, and [jquery-ui](http://jqueryui.com/) for an autocomplete widget.

## Structure

The top-level entity is the [Mossaic](js/Mossaic.html) namespace. This contains the [Router](js/Mossaic.Router.html) as well as the sub-namespaces.

### Mossaic.Router

The [Router](js/Mossaic.Router.html) is responsible for displaying the appropriate content for a given URL. As the top-level object, it is also responsible for creating the global models, the widgets required to browse and save those models, and any sub-components that may be needed.

The following global models are created:

 - [Mossaic.models.Meta](js/Mossaic.models.Meta.html)
  - Slope datum metadata
 - [Mossaic.models.CrossSection](js/Mossaic.models.CrossSection.html)
  - Slope cross section
 - [Mossaic.models.Geometry](js/Mossaic.models.Geometry.html)
  - Slope profile data
 - [Mossaic.models.Task](js/Mossaic.models.Task.html)
  - Simulation request/task definition

The following sub-components are created:

 - [Mossaic.editors.slope](js/Mossaic.editors.slope.html)
  - Display and edit slope profile data and simulation parameters
 - [Mossaic.widgets.task_analyser](js/Mossaic.widgets.task_analyser.html)
  - Visualise simulation task outputs
 - [Mossaic.widgets.task_status](js/Mossaic.widgets.task_status.html)
  - Display the status of all jobs for a selected task
 - [Mossaic.widgets.input_uploader](js/Mossaic.widgets.input_uploader.html)
  - Allows raw input data to be uploaded
 - [Mossaic.widgets.task_builder](js/Mossaic.widgets.task_builder.html)
  - Create a task request from the currently selected data
 - [Mossaic.widgets.ls](js/Mossaic.widgets.ls.html)
  - Browse models of a particular type
  - Can be linked to another ls widget to only show models that have a selected model as a parent
 - [Mossaic.widgets.save](js/Mossaic.widgets.save.html)
  - Display a button that causes the associated model to be saved
 - [Mossaic.widgets.ls_save](js/Mossaic.widgets.ls_save.html)
  - Composition of ls and save widgets

### Mossaic.editors.slope

The [Mossaic.editors](js/Mossaic.editors.html) namespace provides tools for viewing and editing simulation input data. Currently only [Mossaic.editors.slope](js/Mossaic.editors.slope.html) is provided. This is given references to the global models created by Mossaic.Router. It instantiates a number of additional models which representing simulation parameters:

 - [Mossaic.models.Soils](js/Mossaic.models.Soils.html)
  - Soil parameter definitions
 -  [Mossaic.models.BoundaryConditions](js/Mossaic.models.BoundaryConditions.html)
  - Rainfall parameter definitions
 - [Mossaic.models.Stability](js/Mossaic.models.Stability.html)
  - Stability search grid for the slope profile
 - [Mossaic.models.EngineeringCost](js/Mossaic.models.EngineeringCost.html)
  - Engineering cost parameters
 - [Mossaic.models.RoadNetwork](js/Mossaic.models.RoadNetwork.html)
  - Road network definition

The editor consists of two elements. The first, an interactive graphical display, comprises a [Mossaic.vis.Slope](js/Mossaic.vis.Slope.html) and a [Mossaic.vis.Stability](js/Mossaic.vis.Stability.html), both of with are Backbone Views that use d3.js to render SVG to the DOM. The second is a linear series of forms (a [workflow](js/Mossaic.workflow.html)) which take the user through a fixed sequence of editing steps using  [Mossaic.workflow.slope](js/Mossaic.workflow.slope.html).