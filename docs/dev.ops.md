# MoSSaiC Application Platform Operations Documentation

Note: In the following docs, PREFIX should be replaced with the installation prefix for MAP.

## Starting and stopping the jobmanager

SSH onto the node on which MAP is installed, assume the identify of the mossaic user and source the environment:

    ssh root@vm-mossaic.dice.cluster
    su - mossaic
    source PREFIX/mossaicenv

Start the job manager:

    PREFIX/etc/init.d/jobmanager start

Stop the job manager:

    PREFIX/etc/init.d/jobmanager stop

## Checking status

Manual verification of the service status can be performed as follows:

SSH onto the node MAP is running on (e.g. vm-mossaic), assume the identity of the mossaic user and source the environment:

    ssh root@vm-mossaic.dice.cluster
    su - mossaic
    source PREFIX/mossaicenv

Verify the job manager is up and running:

    [mossaic@vm-mossaic ~]$ PREFIX/etc/init.d/jobmanager status
    Job manager is running

Verify the status of the job manager sentinels:

    [mossaic@vm-mossaic ~]$ PREFIX/etc/init.d/jobmanager sentinel-status
    [input sentinel] Queue size: 0 Fail count: 0[job sentinel] Queue size: 0 Fail count: 0[task sentinel] Queue size: 0 Fail count: 0[tracker sentinel] Queue size: 0 Fail count: 0

Verify the BigCouch cluster nodes are up:

    [mossaic@vm-mossaic ~]$ sk status-http
    bc-37-00.dice.cluster is up
    bc-37-01.dice.cluster is up
    bc-37-02.dice.cluster is up

All the above commands follow the POSIX conventions of returning exit status 0 if all is well and nonzero otherwise, so integration into monitoring infrastructure is straightforward.

It is also worth checking the jobmanager log files which are located in `PREFIX/var/log`. At default log levels nothing will be written to the log file unless there is a problem.

## Administrative tasks

There are a number of scripts which can be used to perform common administrative tasks. To use them you need to become the mossaic user and source the environment:

    ssh -A -t dice-io-37-01.acrc.bris.ac.uk ssh -A vm-mossaic.dice.cluster
    sudo su - mossaic
    source PREFIX/mossaicenv

### Creating users

Users can be created with:

    map-user-create.py -u COUCHDB_URL --username REQUESTED_USERNAME --password REQUESTED_PASSWORD --roles ROLES

Where:

 - `COUCHDB_URL` is the URL of the couchdb instance, e.g.: `http://ADMIN_USER:ADMIN_PASSWORD@bc-37-00.dice.cluster:5984`
 - `REQUESTED_USERNAME` and REQUESTED_PASSWORD are the username/password of the user being created
 - `ROLES` is a comma separated list of roles ("mossaic,requestor" for a typical user)

This can also be used to update an existing user by appending the argument `--force`

Available roles are `mossaic` and `requestor`. Users with just the role `mossaic` can browse the input and output data but not create simulation requests. Users who also have the `requestor` role have all the rights of `mossaic` users and can also request simulations.

### Upload a new simulation executable

New versions of simulation executables can be uploaded as follows:

    map-release-upload.py -u COUCHDB_URL -r RELEASE_DB -n VERSION -m RELEASE_MANAGER -a APPLICATION -t DESCRIPTION -p PATH_TO_EXECUTABLE

Where:

 - `COUCHDB_URL` is the URL of the couchdb instance, e.g.: `http://ADMIN_USER:ADMIN_PASSWORD@bc-37-00.dice.cluster:5984`
 - `RELEASE_DB` is the name of the user-accessible release metadata database (e.g.: map_release)
 - `VERSION` is the version string for this simulation executable
 - `RELEASE_MANAGER` is the name of the person responsible for this release
 - `APPLICATION` is the type of application being uploaded (either `chasm` or `questa`)
 - `DESCRIPTION` is a human-readable description of this release
 - `PATH_TO_EXECUTABLE` is the path to the executable stored on the disk of the machine being used to upload it

### Show the usage for a given user

To print basic accounting information for a user:

    map-user-usage.py -u COUCHDB_URL -w WORKFLOW_DB -s REPORTING_PERIOD_START -e REPORTING_PERIOD_END USERNAME

Where:

 - `COUCHDB_URL` is the URL of the couchdb instance, e.g.: `http://ADMIN_USER:ADMIN_PASSWORD@bc-37-00.dice.cluster:5984`
 - `WORKFLOW_DB` is the name of the workflow database (e.g.: map_task)
 - `REPORTING_PERIOD_START` and `REPORTING_PERIOD_END` are dates in format `YYYYMM` of the start and end of the required reporting period
 - `USERNAMES` a whitespace separated list of usernames to be included in the report

### Resubmit jobs in failed_submit state

Jobs can end up in failed_submit state if there was a problem with the DIRAC server. If you think the problem has been resolved it is safe to resubmit all jobs in this state which can be done as follows:

    map-job-resub.py COUCHDB_URL DB_PREFIX

Where:

 - `COUCHDB_URL` is the URL of the couchdb instance, e.g.: `http://ADMIN_USER:ADMIN_PASSWORD@bc-37-00.dice.cluster:5984`
 - `DB_PREFIX` is the prefix for the MAP databases (e.g. map)

### Reset a specific task to the "new" state

Sometimes things just go horribly wrong and a task can end up in a partially completed state with no chance of completing. These tasks can be reset to the "new" state (effectively resubmitting the task) so that the simulations can be re run.

Note: This is only worth doing if you know there was some technical reason that prevented the task completing which has now been resolved. If the task is just a badly specified task then resetting the task will just cause more failing jobs to run and waste resources.

    map-task-reset.py COUCHDB_URL DB_PREFIX TASK_ID

Where:

 - `COUCHDB_URL` is the URL of the couchdb instance, e.g.: `http://ADMIN_USER:ADMIN_PASSWORD@bc-37-00.dice.cluster:5984`
 - `DB_PREFIX` is the prefix for the MAP databases (e.g. map)
 - `TASK_ID` is the _id of the task to be reset

### Compacting a database

Compaction must be carried out every now and then so as to reclaim space on the disks of the database server. In traditional CouchDB compaction is carried out per DB but in BigCouch this must be carried out per shard, hence a script is provided which finds the individual shards and compacts them.

    map-db-compact.py --url BIGCOUCH_ADMIN_URL --db DB_NAME

Where:

 - `BIGCOUCH_ADMIN_URL` is the URL for the administration port on a cluster node (e.g.: `http://ADMIN_USER:ADMIN_PASWORD@bc-37-00.dice.cluster:5986`)
 - `DB_NAME` is the name of the DB to be compacted.

Note that it doesn't matter which node is given in `BIGCOUCH_ADMIN_URL` as the script will happily find all the other nodes.

To compact views, add the parameter `--design-doc` to specify the design document that contains the views you want to compact.

### Finding the shards that hold a given document

Sometimes it is useful to know which actual shard contains a given document. This can be done as follows:

    map-db-shards.py -BIGCOUCH_ADMIN_URL DB_NAME DOC_ID

Where:

 - `BIGCOUCH_ADMIN_URL` is the URL for the administration port on a cluster node (e.g.: `http://ADMIN_USER:ADMIN_PASWORD@bc-37-00.dice.cluster:5986`)
 - `DB_NAME` is the name of the DB containing the doc
 - `DOC_ID` is the id of the doc to be found

## Load balancer configuration

It is highly recommended that MAP runs on the same local network as the BigCouch cluster and that one or more load balancers are used to handle incoming requests.

Configuring the load balancer is outside the scope of this document, however a typical configuration would be:

 - One outward facing port with some external authentication and SSL encryption
  - This would be the entry point for human users
 - One outward facing port with SSL authentication and SSL encryption
  - This would be the entry point for jobs contacting the BigCouch cluster
  - It is recommended that the total number of concurrent connections to the BigCouch nodes is limited, so as to prevent overloading the database in the pathological case of all jobs finishing at the same time