#!/bin/bash

# Deploy MoSSaiC Application Platform

usage="Usage: ./deploy.sh [OPTION] DATABASE_PREFIX
    -u  BigCouch administrator username
    -p  BigCouch administrator password
    -d  Path to directory containing default data
    -s  Fully qualified domain name (with port) of the BigCouch server as accessed internally
    -m  Magic cookie for distributed erlang
    -j  Password for jobmanager account
    -w  Password for jobwrapper account
    -x  Fully qualified domain name (with port) of the BigCouch server as accessed from the outside world
    -b  Fully qualified domain name (with port) of the BigCouch server admin port as accessed internally
    -f  Force pushing of couchapps to databases
    -n  Don't manage bigcouch
    -i  Don't manage DIRAC proxies
    -J  Choose javascript views over erlang views"

function print_usage {
    echo "$usage"
}

if test ! $MOSSAIC
then
    echo Exiting: MOSSAIC environment variable not present
    exit 1
fi

$MOSSAIC/etc/init.d/jobmanager status
if [ $? == 0 ]
then
    echo Job manager daemons still running. Please stop them before attempting deployment.
    exit 1
fi

set -e  # Exit on any nonzero exit status

COUCHAPP_DIR=$MOSSAIC/couchapps
ETC_DIR=$MOSSAIC/etc

while getopts ":u:p:d:s:m:j:w:b:x:fniJ" opt; do
    case $opt in
        u)
            username=$OPTARG
            ;;
        p)
            password=$OPTARG
            ;;
        d)
            data_dir=$OPTARG
            ;;
        s)
            server=$OPTARG
            ;;
        m)
            magic_cookie=$OPTARG
            ;;
        j)
            jobmanager_password=$OPTARG
            ;;
        w)
            jobwrapper_password=$OPTARG
            ;;
        b)
            bigcouch_admin_server=$OPTARG
            ;;
        x)
            server_external=$OPTARG
            ;;
        f)
            force_db="on"
            ;;
        n)
            no_manage_bigcouch=true
            ;;
        i)
            no_dirac_proxy=true
            ;;
        J)
            javascript_couchapps=true
            ;;
        \?)
            echo "Invalid option: -$OPTARG"
            print_usage
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument."
            print_usage
            exit 1
    esac
done

shift $(($OPTIND - 1))

if test ! $1
then
    echo No database prefix supplied
    exit 1
fi

db_prefix=$1
data_db=$db_prefix
task_db="$db_prefix"_task
state_db="$db_prefix"_state
release_db="$db_prefix"_release
release_db_private="$release_db"_private
login_db="$db_prefix"_login
docs_db="$db_prefix"_docs

if [ $username ] && [ $password ]
then
    authstring=$username:$password
else
    authstring=""
fi

if test ! $server
then
    server="localhost:5984"
fi

if test ! $bigcouch_admin_server
then
    bigcouch_admin_server=$server
fi

if test ! $no_manage_bigcouch
then
    # Set the magic cookie and push to all nodes
    if [ -z $magic_cookie ]
    then
        echo "Enter magic cookie for distributed erlang:"
        read prefix
    fi
    sed -e "s/monster/$magic_cookie/" -i $ETC_DIR/vm.args
    sk push-config $ETC_DIR/vm.args

    # Start BigCouch if it is not already running
    set +e
    sk status-http
    if [ $? != 0 ]
    then
        sk spawn
    fi
fi

# Check that all BigCouch nodes are up and responding to http requests
for i in {1..30}
do
    sk status-http
    if [ $? == 0 ]
    then
        break
    fi
    if [ $i == 30 ]
    then
        echo "BigCouch not running on all nodes"
        exit 1
    fi
    sleep 5
done

# Set admin account on BigCouch nodes
response=`curl --write-out %{http_code} --silent --output /dev/null -X GET http://$authstring@$server`
if [ $response == 200 ]
then
    # This account exists so lets check the user is an admin
    response=`curl --write-out %{http_code} --silent --output /dev/null -X GET http://$authstring@$server/_config/admins/$username`
    if [ $response != 200 ]
    then
        echo "BigCouch admin username/password do not correspond to a valid administrator"
        exit 1
    else
        echo "Admin username/password checked"
    fi
else
    # Admin account doesn't exist so go ahead and set it
    sk set-admin $username $password
    if [ $? != 0 ]
    then
        echo "Could not set BigCouch admin username and password"
        exit 1
    fi
fi

set -e

# Populate our BigCouch nodes
sk -u $username -p $password populate-nodes

# Check for databases and exit if any exist and force_db is not set
for db in $data_db $task_db $state_db $release_db $release_db_private
do
    response=`curl --write-out %{http_code} --silent --output /dev/null -X GET http://$authstring@$server/$db`
    if [ $response == 200 ] && test ! $force_db
    then
        echo "Database $db already exists - specify -f to force updating of existing databases. Exiting."
        exit 1
    fi
done

# Push couchapps (this also creates our databases)

# Manually create state db so we can set q=1 and n=1
# This means state is consistent, though not durable
# However as state is temporal, worst case is we have
# to resubmit some jobs
curl -X PUT http://$authstring@$server/$state_db?n=1\&q=1

if test $javascript_couchapps
then
    auth_couchapp="auth-js"
    gui_couchapp="gui-js"
    mining_couchapp="mining-js"
else
    auth_couchapp="auth"
    gui_couchapp="gui"
    mining_couchapp="mining"
fi

COUCHAPPS="mossaic,$data_db
workflow,$task_db
state-machine,$state_db
release,$release_db
release,$release_db_private
docs,$docs_db
$gui_couchapp,$data_db
$auth_couchapp,$data_db
$mining_couchapp,$data_db
login,$login_db"

for db in `echo $COUCHAPPS`
do
    dir=`echo $db | cut -d ',' -f 1`
    name=`echo $db | cut -d ',' -f 2`
    cd $COUCHAPP_DIR/$dir
    couchapp push http://$authstring@$server/$name
done

# Set admin and readers for dbs
for db in $data_db $task_db $state_db $release_db
do
   curl -X PUT http://$authstring@$server/$db/_security -d "{\"admins\":{\"names\":[\"$username\"]}, \"readers\":{\"roles\":[\"mossaic\"]}}" -H 'Content-type: application/json'
done

# Only job_wrapper role for private release db, as this is where the actual executables live
curl -X PUT http://$authstring@$server/$release_db_private/_security -d "{\"admins\":{\"names\":[\"$username\"]}, \"readers\":{\"roles\":[\"job_wrapper\"]}}" -H 'Content-type: application/json'

# Only admin, not readers for login - this is intentionally public
curl -X PUT http://$authstring@$server/$login_db/_security -d "{\"admins\":{\"names\":[\"$username\"]}}" -H 'Content-type: application/json'

if [ $QUORUM_N ]
then
    W="w=$QUORUM_N"
fi

if [ $data_dir ]
then
    curl -X POST http://$authstring@$server/$release_db?$W -d @$data_dir/releases/chasm_4.12.5.json -H 'Content-type: application/json' | cut -d '"' -f 10
    rev=`curl -X POST http://$authstring@$server/$release_db_private?$W -d @$data_dir/releases/chasm_4.12.5.json -H 'Content-type: application/json' | cut -d '"' -f 10`
    curl -X PUT http://$authstring@$server/$release_db_private/chasm_4.12.5/executable?$W\&rev=$rev --data-binary @$data_dir/executables/chasm -H 'Content-type: application/octet-stream'
    curl -X POST http://$authstring@$server/$release_db?$W -d @$data_dir/releases/questa_4.21.json -H 'Content-type: application/json' | cut -d '"' -f 10
    rev=`curl -X POST http://$authstring@$server/$release_db_private?$W -d @$data_dir/releases/questa_4.21.json -H 'Content-type: application/json' | cut -d '"' -f 10`
    curl -X PUT http://$authstring@$server/$release_db_private/questa_4.21/executable?$W\&rev=$rev --data-binary @$data_dir/executables/questa -H 'Content-type: application/octet-stream'
    curl -X POST http://$authstring@$server/$data_db?$W -d @$data_dir/input_files/boundary_conditions_deterministic.json -H 'Content-type: application/json'
    curl -X POST http://$authstring@$server/$data_db?$W -d @$data_dir/input_files/engineering_cost.json -H 'Content-type: application/json'
    curl -X POST http://$authstring@$server/$data_db?$W -d @$data_dir/input_files/geometry.json -H 'Content-type: application/json'
    curl -X POST http://$authstring@$server/$data_db?$W -d @$data_dir/input_files/geometry_questa_soil_approximated.json -H 'Content-type: application/json'
    curl -X POST http://$authstring@$server/$data_db?$W -d @$data_dir/input_files/reinforcements_earthnail.json -H 'Content-type: application/json'
    curl -X POST http://$authstring@$server/$data_db?$W -d @$data_dir/input_files/road_network.json -H 'Content-type: application/json'
    curl -X POST http://$authstring@$server/$data_db?$W -d @$data_dir/input_files/soils_deterministic.json -H 'Content-type: application/json'
    curl -X POST http://$authstring@$server/$data_db?$W -d @$data_dir/input_files/stability_bishop.json -H 'Content-type: application/json'
fi

# Create job manager and job wrapper users
if [ -z $jobmanager_password ]
then
    stty -echo
    read jobmanager_password
    stty echo
fi
map-user-create.py -u http://$authstring@$bigcouch_admin_server --username job_manager --password $jobmanager_password --roles job_manager,mossaic --force

if [ -z $jobwrapper_password ]
then
    stty -echo
    read jobwrapper_password
    stty echo
fi
map-user-create.py -u http://$authstring@$bigcouch_admin_server --username job_wrapper --password $jobwrapper_password --roles job_wrapper,mossaic --force

# Set jobmanager.ini and submitter.ini so that url contains appropriate credentials and database names,
# then push to all nodes
sed -e "s!^url =.*!url = http://job_manager:$jobmanager_password@$server!" -i $ETC_DIR/jobmanager.ini
sed -e "s!^task_db = .*!task_db = $task_db!" -i $ETC_DIR/jobmanager.ini
sed -e "s!^state_db = .*!state_db = $state_db!" -i $ETC_DIR/jobmanager.ini
sed -e "s!^data_db = .*!data_db = $data_db!" -i $ETC_DIR/jobmanager.ini
sed -e "s!^url =.*!url = https://$server_external!" -i $ETC_DIR/submitter.ini
sed -e "s!^task_db = .*!task_db = $task_db!" -i $ETC_DIR/submitter.ini
sed -e "s!^state_db = .*!state_db = $state_db!" -i $ETC_DIR/submitter.ini
sed -e "s!^data_db = .*!data_db = $data_db!" -i $ETC_DIR/submitter.ini
sed -e "s!^release_db = .*!release_db = $release_db_private!" -i $ETC_DIR/submitter.ini
echo job_wrapper > $ETC_DIR/jobwrapper_auth
echo $jobwrapper_password >> $ETC_DIR/jobwrapper_auth

if [ -z $no_dirac_proxy ]
then
    echo "Uploading proxy for ls_user"
    dirac-proxy-init
    dirac-proxy-init --group=ls_user --valid=8760:00 --upload
fi

# Start jobmanager
$ETC_DIR/init.d/jobmanager start

echo "Deployment complete. Don't forget to run postdeploy.sh as root to set up logrotate and cron."
