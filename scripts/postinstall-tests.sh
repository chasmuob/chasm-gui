#!/bin/bash

# Runs unit tests post-install on single-node local BigCouch "cluster"
# Needs the MOSSAIC environment variable to be set (source mossaicenv)

if test ! $MOSSAIC
then
    echo Exiting: MOSSAIC environment variable not present
    exit 1
fi

ETCDIR=$MOSSAIC/etc

if test -z $SKIP_DIRAC_TESTS
then
    echo "Uploading proxy for ls_user"
    dirac-proxy-init
    dirac-proxy-init --group=ls_user --upload
fi

$ETCDIR/init.d/bigcouch start

if $ETCDIR/init.d/bigcouch httptest
then
    curl -f -X PUT http://localhost:5986/_config/admins/admin -d '"pass"'
    if [ $? != 0 ]
    then
        echo "Setting of admin password failed. Retrying in 5 seconds..." # Hmm...?
        sleep 5
        curl -X PUT http://localhost:5986/_config/admins/admin -d '"pass"'
    fi
    if test ! $COUCHURL
    then
        export COUCHURL=http://admin:pass@localhost:5984
    fi
    if test ! $COUCHURL_HTTPS
    then
        export COUCHURL_HTTPS=https://admin:pass@localhost:6984
    fi
    mkdir -p $ETCDIR/ssl
    cp tests/ssl/userkey.pem $ETCDIR/ssl/jobwrapper.key.pem
    cp tests/ssl/usercert.pem $ETCDIR/ssl/jobwrapper.cert.pem
    cp config/bigcouch_nodes.test $ETCDIR/bigcouch_nodes
    make postinstall-tests
    curl -X DELETE http://admin:pass@localhost:5986/_config/admins/admin
else
    echo "Tests did not run"
    exit 1
fi

$ETCDIR/init.d/bigcouch stop