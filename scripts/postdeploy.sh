#!/bin/bash

# Post-deployment tasks
#
# To be run as root

usage="Usage: ./postdeploy.sh MOSSAIC_USER
  MOSSAIC_USER = username of the account that runs the MoSSaiC Application Platform"

if test ! $1
then
    echo Exiting: MOSSAIC_USER not specified
    echo $usage
    exit 1
fi

mossaic_user = $1

if test ! $MOSSAIC
then
    echo Exiting: MOSSAIC environment variable not present
    exit 1
fi

MOSSAIC_DIR=$MOSSAIC
NODES_FILE=$MOSSAIC/etc/bigcouch_nodes

if [ ! -e $NODES_FILE ]
then
    echo "Cannot find nodelist at $NODES_FILE"
    exit 1
fi

nodes=`cat $NODES_FILE`
node_commands=''
exitstatus=0

bigcouch_logrotate="$MOSSAIC/var/log/bigcouch.log $MOSSAIC/var/log/bigcouch.nohup.stdout $MOSSAIC/mossaic/var/log/bigcouch.nohup.stderr {
    copytruncate
    compress
    size 100M
    rotate 10
    missingok
}"

mossaic_logrotate="$MOSSAIC/var/log/jobmanager.log $MOSSAIC/mossaic/var/log/jobmanager.nohup.stdout $MOSSAIC/mossaic/var/log/jobmanager.nohup.stderr {
    copytruncate
    compress
    size 100M
    rotate 10
    missingok
}"

mossaic_cron="0 4 * * * su $mossaic_user 'find $MOSSAIC/var/ssl/* -mtime +3 -exec rm {} \;'"

for nodename in ${nodes[@]}
do
    IFS='@' read -ra node <<< "$nodename"
    nodehost=${node[1]}
    echo "$bigcouch_logrotate" | ssh $nodehost "cat - > /etc/logrotate.d/bigcouch"
    echo "$mossaic_logrotate" | ssh $nodehost "cat - > /etc/logrotate.d/mossaic"
    echo "$mossaic_cron" | ssh $nodehost "cat - > /etc/cron.d/mossaic"
done