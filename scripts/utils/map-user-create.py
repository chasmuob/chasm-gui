#!/usr/bin/env python

import logging
import sha

from optparse import OptionParser

from couch.Couch import CouchServer
from couch.CouchRequest import CouchConflictError

__USERS_DB__ = '_users'

def do_options():
    """
    Read options and set up a logger
    """
    parser = OptionParser()
    parser.add_option("-u", "--url", dest="couch_url", default="http://localhost:5984",
                    help="the URL where the CouchDB is hosted", metavar="URL")
    parser.add_option("-v", "--verbose", dest="verbose",
                    action="store_true", default=False, help="Be more verbose")
    parser.add_option("-d", "--debug", dest="debug",
                    action="store_true", default=False, help="print debugging statements")
    parser.add_option("--username", dest="username",
                    help="Username for the new user")
    parser.add_option("--password", dest="password",
                    help="Password for the new user")
    parser.add_option("--roles", dest="roles",
                    help="Comma separated list of roles for the new user")
    parser.add_option("--force", action="store_true", dest="force")
    # parse the input
    options, args = parser.parse_args()
    options.couch_url = options.couch_url.strip('/')
    options.roles = options.roles.split(',')
    #set up the logger
    log_level = logging.WARNING
    if options.verbose:
        log_level = logging.INFO
    if options.debug:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level)
    options.logger = logging.getLogger('create user script')
    return options

def create_userdoc(username, roles, password, salt):
    """ Create userdoc with hashed password using salt """
    return {
        "_id": "org.couchdb.user:" + username,
        "name": username,
        "type": "user",
        "roles": roles,
        "salt": salt,
        "password_sha": sha.sha(password + salt).hexdigest()
    }

if __name__ == '__main__':
    print 'Creating user'
    opts = do_options()
    db = CouchServer(opts.couch_url).connectDatabase(__USERS_DB__)
    uuid = db.getUuids(count=1)[0]
    user = create_userdoc(opts.username, opts.roles, opts.password, uuid)
    try:
        db.commitOne(user)
    except CouchConflictError:
        if opts.force:
            doc = db.document('org.couchdb.user:%s' % user['name'])
            for role in user['roles']:
                if role not in doc['roles']:
                    doc['roles'].append(role)
            doc['salt'] = user['salt']
            doc['password_sha'] = user['password_sha']
            doc['salt'] = user['salt']
            db.commitOne(doc)
        else:
            print 'Cannot create user: %s already exists (override with --force flag)' % user['name']
            exit(1)
    print 'User created successfully'