#!/usr/bin/env python
""" map-db-compact.py

Initiate compaction on all BigCouch shards that belong to a specified database.

This script requires the URL of any BigCouch node admin port - from there it gets a list of all nodes that hold shards for the specified DB. It doesn't matter
which node in a cluster we point it to initially as they all have a record of where each shard is.

We assume the port specified in the url is the port used for all BigCouch admin instances.

NOTE: Generally this script will only work if called from inside a cluster, unless said cluster is set up such that all nodes are visible to the outside world.

"""

import sys
import urlparse
from optparse import OptionParser

from couch.Couch import CouchServer, Database

from mossaic.version import __version__

def do_options():
    """
    Read options
    """
    parser = OptionParser(version="%prog " + str(__version__))
    parser.add_option("-v", "--verbose", dest="verbose",
                    action="store_true", default=False, help="Be more verbose")
    parser.add_option("-d", "--debug", dest="debug",
                    action="store_true", default=False, help="print debugging statements")
    parser.add_option("--db", dest="dbname", help="Database to be compacted")
    parser.add_option("--url", dest="url", help="Url of BigCouch admin console")
    parser.add_option("--design-doc", dest="designdoc", help="Specific design document whose views are to be compacted")
    # parse the input
    options, args = parser.parse_args()
    if not options.dbname:
        print "Please specify a db name"
        sys.exit(1)
    if not options.url:
        print "Please specify a url for the BigCouch admin port"
        sys.exit(1)
    return options

def get_node_url(node, original_url):
    """ Get node url from node name and original url spec """
    hostname = node.split('@')[1]
    url = urlparse.urlparse(original_url)
    authstring = ''
    if url.username and url.password:
        authstring = '%s:%s@' % (url.username, url.password)
    portstring = ''
    if url.port:
        portstring = ':%s' % url.port
    return '%s://%s%s%s' % (url.scheme, authstring, hostname, portstring)

def get_dbname_from_shard_name(shard_name):
    """ Extract the unsharded database name from a BigCouch shard name """
    return '/'.join(shard_name.split('/')[2:]).split('.')[0]

def get_shards(server, dbname):
    """ Get list of all shards for specified db at BigCouch instance """
    all_dbs = server.listDatabases()
    shards = [db for db in all_dbs if get_dbname_from_shard_name(db) == dbname]
    return shards

def compact(node_url, dbname, designdoc=None):
    """ Call compact on all shards of specified dbname at BigCouch instance node_url """
    server = CouchServer(node_url)
    shard_dbs = get_shards(server, dbname)
    for shard in shard_dbs:
        db = server.connectDatabase(shard)
        if designdoc:
            print 'Initiating compaction on node %s for shard %s, for design document %s' % (node_url, shard, designdoc)
            db.compact(views=[designdoc])
        else:
            print 'Initiating compaction on node %s for shard %s' % (node_url, shard)
            db.compact()

if __name__ == '__main__':
    options = do_options()
    server = CouchServer(options.url)
    dbs = server.connectDatabase('dbs')
    nodes = dbs.document(options.dbname)['by_node']
    for node in nodes:
        node_url = get_node_url(node, options.url)
        compact(node_url, options.dbname, options.designdoc)