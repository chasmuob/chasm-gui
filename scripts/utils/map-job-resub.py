#!/usr/bin/env python
""" map-job-resub.py

Reset all state docs in failed_submit state to ready_to_submit

"""

import json
import logging
import sys
import time
import urlparse

import couch.Couch as couch

import logging
logging.basicConfig(level=logging.DEBUG)

# We're in a hurry so, lets just assume args are: url db

url = sys.argv[1]
db_prefix = sys.argv[2]

server = couch.CouchServer(url)
state_db = server.connectDatabase("%s_%s" % (db_prefix, 'state'))

view_opts = {
    'reduce': False,
    'key': 'failed_submit',
    'include_docs': True
}
state_docs = state_db.loadView(
        'state-machine', 'jobs_by_state', view_opts)['rows']

for state_doc in state_docs:
    logging.debug("Resetting state %s" % state_doc['id'])
    doc = state_doc['doc']
    if 'state_history' in doc and 'previous_state_histories' not in doc:
        doc['previous_state_histories'] = []
    doc['previous_state_histories'].append(doc['state_history'])
    doc.pop('state_history')
    doc['state_history'] = {
        'ready_to_submit': {
            'modified_by': 'utils',
            'timestamp': time.time()
        }
    }
    doc['state'] = 'ready_to_submit'
    if 'submit_fail_count' in doc:
        doc.pop('submit_fail_count')
    state_db.commitOne(doc)
    logging.debug("Reset state %s" % state_doc['id'])