#!/usr/bin/env python
""" map-db-shards.py

Find the shards that hold a given doc for a given db

"""

import json
import logging
import sys
import time
import urlparse

import couch.Couch as couch

import logging
logging.basicConfig(level=logging.DEBUG)

# We're in a hurry so, lets just assume args are: url db task_id

url = sys.argv[1]
db_name = sys.argv[2]
doc_id = sys.argv[3]

server = couch.CouchServer(url)

all_dbs = server.listDatabases()

# Format is shards/ID0-ID1/db_name.SOME_ID
for db in all_dbs:
    if not db.startswith('shards'):
        continue
    name = db.split('/')[2].split('.')[0]
    if db_name == name:
        conn = server.connectDatabase(db)
        docs = conn.allDocs()
        for doc in docs['rows']:
            if doc['id'] == doc_id:
                print db