#!/usr/bin/env python

import logging
import sha

from optparse import OptionParser

from couch.Couch import CouchServer
from couch.CouchRequest import CouchConflictError

def do_options():
    """
    Read options and set up a logger
    """
    parser = OptionParser(usage="usage: %prog [options] <list of usernames>")
    parser.add_option("-u", "--url", dest="couch_url", default="http://localhost:5984",
                    help="the URL where the CouchDB is hosted", metavar="URL")
    parser.add_option("-v", "--verbose", dest="verbose",
                    action="store_true", default=False, help="Be more verbose")
    parser.add_option("-d", "--debug", dest="debug",
                    action="store_true", default=False, help="print debugging statements")
    parser.add_option("-w", "--workflowdb", dest="db",
                    help="The name of the workflow database from which the report should be generated")
    parser.add_option("-s", "--start", dest="start",
                    help="Year and month (yyyymm) that mark the start of the reporting period")
    parser.add_option("-e", "--end", dest="end",
                    help="Year and month (yyyymm) that mark the end of the reporting period (inclusive)")
    # parse the input
    options, args = parser.parse_args()
    options.couch_url = options.couch_url.strip('/')
    #set up the logger
    log_level = logging.WARNING
    if options.verbose:
        log_level = logging.INFO
    if options.debug:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level)
    options.logger = logging.getLogger('create user script')
    return options, args

if __name__ == '__main__':
    opts, args = do_options()
    if not opts.db:
        print "You must specify the name of the workflow database"
        exit(1)
    db = CouchServer(opts.couch_url).connectDatabase(opts.db)
    if not opts.start:
        print "You must specify a start year/month for this report"
        exit(1)
    startkey_postfix = [int(opts.start[0:4]), int(opts.start[4:6])]
    if opts.end:
        endkey_postfix = [int(opts.end[0:4]), int(opts.end[4:6]), {}]
    else:
        endkey_postfix = startkey_postfix + [{}]
    for username in args:
        query_args = {
            'startkey': [username] + startkey_postfix,
            'endkey': [username] + endkey_postfix,
            'group': True,
            'group_level': 3
        }
        list_for_user = db.loadList("workflow", "usage", "accounting", query_args)
        print list_for_user