#!/usr/bin/env python

""" map-release-upload.py

Upload release metadata to the public and private release DBs and attach the
specified executable to only the private release DB.

"""

import logging
import time

from optparse import OptionParser

from couch.Couch import CouchServer
from couch.CouchRequest import CouchError

__PRIVATE_POSTFIX__ = "private"

def do_options():
    """
    Read options and set up a logger
    """
    parser = OptionParser()
    parser.add_option("-u", "--url", dest="couch_url", default="http://localhost:5984",
                    help="the URL where the CouchDB is hosted", metavar="URL")
    parser.add_option("-v", "--verbose", dest="verbose",
                    action="store_true", default=False, help="Be more verbose")
    parser.add_option("-d", "--debug", dest="debug",
                    action="store_true", default=False, help="print debugging statements")
    parser.add_option("-r", "--release_db", dest="release_db", default="map_release",
                    help="the name of the database for public release metadata")
    parser.add_option("-n", "--version", dest="version", default="",
                    help="the version number of the release being uploaded")
    parser.add_option("-m", "--release-manager", dest="release_manager", default="",
                    help="the name of the release manager for this release")
    parser.add_option("-a", "--application", dest="application", default="",
                    help="the application of which this is a release (e.g. chasm or questa)")
    parser.add_option("-t", "--description", dest="description", default="",
                    help="description of this release")
    parser.add_option("-p", "--path-to-executable", dest="path", default="",
                    help="path to the executable to be uploaded to the private DB")
    # parse the input
    options, args = parser.parse_args()
    options.couch_url = options.couch_url.strip('/')
    #set up the logger
    log_level = logging.WARNING
    if options.verbose:
        log_level = logging.INFO
    if options.debug:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level)
    options.logger = logging.getLogger('create user script')
    return options

def create_release_doc(options):
    """ Create release doc using options """
    return {
        "_id": "%s_%s" % (options.application, options.version),
        "type": "release",
        "software": options.application,
        "version": options.version,
        "release_manager": options.release_manager,
        "date": time.time(),
        "description": options.description,
        "for_production": True
    }

if __name__ == '__main__':
    print 'Creating release'
    opts = do_options()
    db = CouchServer(opts.couch_url).connectDatabase(opts.release_db)
    db_private = CouchServer(opts.couch_url).connectDatabase(
            "%s_%s" % (opts.release_db, __PRIVATE_POSTFIX__))
    metadata = create_release_doc(opts)
    try:
        db.commitOne(metadata)
    except CouchError, e:
        print "Error uploading release metadata to public DB: %s" % e
        exit(1)
    try:
        result = db_private.commitOne(metadata)[0]
        metadata["_rev"] = result["rev"]
    except CouchError, e:
        print "Error uploading release metadata to private DB: %s" % e
        exit(1)
    try:
        executable = file(opts.path).read()
        db_private.addAttachment(metadata["_id"], metadata["_rev"],
                executable, "executable",
                "application/octet-stream", add_checksum=True)
    except CouchError, e:
        print "Error uploading executable to private DB: %s" % e
        exit(1)
    print "Uploaded successful for software: %s, version: %s" %\
            (opts.application, opts.version)