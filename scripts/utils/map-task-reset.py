#!/usr/bin/env python
""" map-task-reset.py

Reset all jobs for a given task to the new state, meaning:

 - in results db
  - delete all results for task
 - in state db
  - delete all state docs
 - in task db
  - delete state field
  - delete state history field
  - delete start_time field, if it exists

"""

import json
import logging
import sys
import time
import urlparse

import couch.Couch as couch

import logging
logging.basicConfig(level=logging.DEBUG)

# We're in a hurry so, lets just assume args are: url db task_id

url = sys.argv[1]
db_prefix = sys.argv[2]
task_id = sys.argv[3]

server = couch.CouchServer(url)
results_db = server.connectDatabase(db_prefix)
state_db = server.connectDatabase("%s_%s" % (db_prefix, 'state'))
task_db = server.connectDatabase("%s_%s" % (db_prefix, 'task'))

view_opts = {
    'reduce': False,
    'startkey': [task_id],
    'endkey': [task_id, {}]
}
results_docs = results_db.loadView(
        'mossaic', 'results_by_task', view_opts)['rows']

logging.info("Deleting results docs: %s" % [r['id'] for r in results_docs])

for results_doc in results_docs:
    logging.debug("Deleting result %s" % results_doc['id'])
    results_db.delete_doc(results_doc['id'], w=3)
    logging.debug("Deleted result %s" % results_doc['id'])

state_docs = state_db.loadView(
        'state-machine', 'jobs_for_task', {'reduce': False, 'key': task_id})['rows']

logging.info("Deleting state docs: %s" % [s['id'] for s in state_docs])

for state_doc in state_docs:
    logging.debug("Deleting state %s" % state_doc['id'])
    state_db.delete_doc(state_doc['id'])
    logging.debug("Deleted state %s" % state_doc['id'])

view_opts = {
    'reduce': False,
    'include_docs': True,
    'startkey': [task_id],
    'endkey': [task_id, []]
}
job_docs = task_db.loadView(
        'workflow', 'jobs_by_task', view_opts)['rows']

logging.info("Resetting jobs: %s" % [s['id'] for s in job_docs])

for job_doc in job_docs:
    logging.debug('Checking job %s' % job_doc['id'])
    doc = job_doc['doc']
    has_changed = False
    if 'state_history' in doc and 'previous_state_histories' not in doc:
        doc['previous_state_histories'] = []
        doc['previous_state_histories'].append(doc['state_history'])
        doc.pop('state_history')
        has_changed = True
    if 'state' in doc:
        doc.pop('state')
        has_changed = True
    if 'start_time' in doc:
        doc.pop('start_time')
        has_changed = True
    if has_changed:
        logging.debug('Resetting job %s' % job_doc['id'])
        task_db.ecCommitOne(doc, w=3)
        logging.debug('Reset job %s' % job_doc['id'])
    state_doc = {
        '_id': job_doc['id'],
        'job_id': job_doc['id'],
        'task_id': doc['task_id'],
        'state': 'ready_to_submit',
        'state_history': {
            'ready_to_submit': {
                'modified_by': 'utils',
                'timestamp': int(time.time())
            }
        }
    }
    state_db.commitOne(state_doc)
    logging.debug('Creating state doc for job %s' % job_doc['id'])