#!/bin/bash

# Create a proxy for credentials in MOSSAIC/etc/ssl

# ./create_proxy.sh CN_SUFFIX OUTPUT_DIR FILE_PREFIX DAYS

if test ! $MOSSAIC
then
    if test -d "../../ssl"
    then
        extra_separator="../"
    fi
    SSL_DIR=$extra_separator../ssl
    jobwrapper_cert=$SSL_DIR/usercert.pem
    jobwrapper_key=$SSL_DIR/userkey.pem
    csr_conf=$extra_separator../../config/csr.conf
else
    SSL_DIR=$MOSSAIC/etc/ssl
    jobwrapper_cert=$SSL_DIR/jobwrapper.cert.pem
    jobwrapper_key=$SSL_DIR/jobwrapper.key.pem
    csr_conf=$SSL_DIR/csr.conf
fi

cn_suffix=$1
output_dir=$2
file_prefix=$3
days=$4

subject=`openssl x509 -in $jobwrapper_cert -noout -subject | awk -F '= ' '{print $2}'`/CN=$1
openssl req -new -config $csr_conf -subj "$subject" -out $output_dir/$file_prefix.csr -keyout $output_dir/$file_prefix.key.pem
openssl x509 -req -md5 -CAcreateserial -in $output_dir/$file_prefix.csr -days $days -CA $jobwrapper_cert -CAkey $jobwrapper_key -extfile $csr_conf -extensions v3_proxy -out $output_dir/$file_prefix.cert.pem
cat $output_dir/$file_prefix.cert.pem $jobwrapper_cert > $output_dir/$file_prefix.cert.chain.pem