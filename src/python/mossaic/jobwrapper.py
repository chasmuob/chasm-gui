#!/usr/bin/env python
"""
This module is the job wrapper for running CHASM/Questa jobs. It does the following:

1. download the job json (or whatever)
2. download the executable
3. download the input
4. run the exe
5. parse the output, upload json, attach a tar ball of the output data
"""

from __future__ import with_statement

from collections import defaultdict
from optparse import OptionParser
from couch.Couch import CouchServer
from couch.CouchRequest import CouchError
from mossaic.mossaicparser import MossaicParser
import logging
import sys
import stat
import os
import base64
import glob
import hashlib
import random
import subprocess
import tarfile
import tempfile
import time
import string
import StringIO
import shutil
import time
import urlparse
from version import __version__

class ExitStatus(object):
    OK = 0
    WARNING = 1
    CRITICAL = 2

def do_options():
    """
    Read options and set up a logger
    """
    parser = OptionParser(version="%prog " + str(__version__))
    parser.add_option("-j", "--job", dest="job_id",
                    help="read job definition for job ID", metavar="ID")
    parser.add_option("-u", "--url", dest="couch_url", default="http://localhost:5984",
                    help="the URL where the CouchDB is hosted", metavar="URL")
    parser.add_option("-v", "--verbose", dest="verbose",
                    action="store_true", default=False, help="Be more verbose")
    parser.add_option("-d", "--debug", dest="debug",
                    action="store_true", default=False, help="print debugging statements")
    parser.add_option("-a", "--auth-file", dest="auth_file",
                    help="File containing username and password separated by newline")
    parser.add_option("--task-db", dest="taskdb", default="mossaic-task",
                    help="Read/write tasks to a different database (default: mossaic-task)")
    parser.add_option("--data-db", dest="datadb", default="mossaic-data",
                    help="Read/write data to a different database (default: mossaic-data)")
    parser.add_option("--release-db", dest="releasedb", default="mossaic-release",
                    help="Database hosting the mossaic releases (default: mossaic-release)")
    parser.add_option("--state-db", dest="statedb", default="mossaic-state",
                    help="Database hosting the state data for tasks/jobs (default: mossaic-state)")
    parser.add_option("-k", "--keep", dest="keep", default=False, action="store_true",
                    help="Keep the job working dir after the wrapper exits, useful for debugging")
    parser.add_option("-n", "--noop", dest="noop", default=False, action="store_true",
                    help="Don't upload the job output to CouchDB, useful for debugging")
    parser.add_option("-r", "--http-retries", dest="http_retries", default=3,
                    help="number of times to retry http requests on failure")
    parser.add_option("--ssl-key", dest="ssl_key", default=None,
                    help="Path to file containing key for SSL certificate authentication")
    parser.add_option("--ssl-cert", dest="ssl_cert", default=None,
                    help="Path to file containing certificate for SSL certificate authentication")
    parser.add_option("--initial-backoff-window", dest="initial_backoff", default="60",
                    help="Length in seconds of the initial backoff window for retries")
    # parse the input
    options, args = parser.parse_args()
    options.couch_url = options.couch_url.strip('/')
    #set up the logger
    log_level = logging.WARNING
    if options.verbose:
        log_level = logging.INFO
    if options.debug:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level)
    options.logger = logging.getLogger('mossaic job wrapper')
    #Maybe change the formatting?
    #("%(asctime)s|%(levelname)s [%(module)s %(funcName)s] %(message)s")
    # exit if a job isn't provided
    if not options.job_id:
        logger.exception('You must provide an id for a job')
        print '===================================='
        print 'MoSSaic job failed'
        print '===================================='
        sys.exit(1)
    return options

def _validate_url(url):
    """ A url is considered valid if it is parseable and does not contain an auth string """
    python_version = sys.version_info[0:3]
    if (python_version[0] == 2 and python_version[1] <= 5):
        netloc = urlparse.urlparse(url)[1]
        if ('@' in netloc):
            raise Exception('URL contains authentication details')
    elif (python_version[0] == 2 and python_version[1] > 5):
        parsed = urlparse.urlparse(url)
        if (parsed.username or parsed.password):
            raise Exception('URL contains authentication details')
    else:
        raise Exception('Unsupported python version')

def _build_full_url(url, auth_file):
    """ Read username and password from auth_file and add to url """
    auth = file(auth_file, 'r').readlines()
    username, password = [line.strip() for line in auth]
    # Don't have to worry about python versions here as we access parsed url
    # using the old style, which is supported by the new version
    parsed = urlparse.urlparse(url)
    scheme = parsed[0]
    netloc = parsed[1]
    return '%s://%s:%s@%s' % (scheme, username, password, netloc)


class JobWrapperError(Exception):
    def __init__(self, reason):
        Exception.__init__(self, reason)


class JobWrapperPlugin(object):
    """ Functions that contain application-specific functionality are provided through a plugin object """
    def __init__(self, logger):
        self._default_steering_suffix = 'file'
        self._default_steering_name = 'steering'
        self.logger = logger

    def get_show_name(self, file_type):
        return file_type

    def get_steering_name(self, suffix=True):
        if suffix:
            return '.'.join((self._default_steering_name, self._default_steering_suffix))
        else:
            return self._default_steering_name

    def parse_output(self, job_id, working_dir):
        return {'type': 'results', 'job_id': job_id}


class JobWrapperPluginMossaic(JobWrapperPlugin):
    def __init__(self, logger):
        super(JobWrapperPluginMossaic, self).__init__(logger)
        self._default_steering_suffix = 'steering'
        self._default_steering_name = '0'

    def _concat_outputs(self, outputs):
        """ Helper which concatenates output - assumes output is pre-sorted

        It is assumed that each output is either a list of values which should be catted, or a dict
        with a key 'value' that should be catted

        """
        keys = outputs[0].keys()
        master_output = outputs.pop(0)
        exclude = ['soil_type']
        for output in outputs:
            for key in keys:
                if key in exclude:
                    continue
                if type(output[key]) == list:
                    master_output[key] += output[key]
                elif type(output[key]) == dict:
                    if 'value' in master_output[key]:
                        master_output[key]['value'] += output[key]['value']
                    else:
                        sub_keys = master_output[key].keys()
                        for sub_key in sub_keys:
                            if type(master_output[key][sub_key]) == list:
                                master_output[key][sub_key] += output[key][sub_key]
                            elif type(master_output[key][sub_key]) == dict:
                                master_output[key][sub_key]['value'] += output[key][sub_key]['value']
                            else:
                                raise JobWrapperError('Unexpected data type in output: %s' % type(output[key][sub_key]))
                else:
                    raise JobWrapperError('Unexpected data type in output: %s' % type(output[key]))
        return master_output

    def _sort_filenames(self, filenames):
        """ Helper which sorts a list of filenames in X_Y_Z.extension format in descending order """
        def filename_to_int(filename):
            tokens = os.path.basename(filename).split('.')[0].split('_')
            return int(''.join(tokens))
        def compare(a, b):
            return filename_to_int(a) - filename_to_int(b)
        return sorted(filenames, cmp=compare)

    def _parse_file_with_extension(self, extension, working_dir, allow_multiple=False):
        """ Parse files found with given extension

        If allow_multiple is set to true then we parse multiple files and cat them together intelligently, based on the fields
        that are present.

        File name format is expected to be X_Y_Z.extension, where X, Y and Z are positive integers. Files are sorted in
        ascending order.

        """
        files = glob.glob(os.path.join(working_dir, '*.%s' % extension))
        if len(files) == 1:
            parser = MossaicParser.get_parser(os.path.join(working_dir, files[0]))
            return parser.parse()
        elif len(files) > 1 and allow_multiple:
            sorted_filenames = self._sort_filenames(files)
            all_outputs = []
            for filename in sorted_filenames:
                parser = MossaicParser.get_parser(os.path.join(working_dir, filename))
                if hasattr(parser, "to_compress"):
                    all_outputs.append(parser.parse(compress=False))
                else:
                    all_outputs.append(parser.parse())
            catted_outputs = self._concat_outputs(all_outputs)
            if hasattr(parser, "to_compress"):
                for field in parser.to_compress:
                    catted_outputs[field]['value'] = parser.compress(catted_outputs[field]['value'])
            return catted_outputs
        elif len(files) > 1 and not allow_multiple:
            raise JobWrapperError('Unexpected number of .%s files found in working directory' % extension)
        elif len(files) == 0:
            raise JobWrapperError('No .%s files found in working directory' % extension)

    def parse_output(self, job_id, working_dir):
        """
        Parse the output files and build up a dictionary to upload to CouchDB

        If there is a problem with the expected output we return a document containing an error field

        All returned jobs have a job_id field
        """
        data = {'job_id': job_id, 'type': 'results'}
        data.update({'chasm_output': self._parse_file_with_extension('fos', working_dir, allow_multiple=True)})
        data['chasm_output'].update(self._parse_file_with_extension('results', working_dir, allow_multiple=True))
        return data


class JobWrapperPluginMossaicQuesta(JobWrapperPluginMossaic):
    def __init__(self, logger):
        super(JobWrapperPluginMossaicQuesta, self).__init__(logger)

    def get_show_name(self, file_type):
        return {'initial_slope': 'geometry'}.get(file_type, file_type)

    def parse_output(self, job_id, working_dir):
        data = {}
        try:
            data = super(JobWrapperPluginMossaicQuesta, self).parse_output(job_id, working_dir)
        except Exception, e:
            self.logger.exception(e)
            data['job_id'] = job_id
            data['type'] = 'results'
            data['chasm_output'] = str(e)
        data.update({'questa_output': self._parse_file_with_extension('out', working_dir, allow_multiple=False)})
        return data


def _get_plugin(logger, application=None):
    return {
        'chasm': JobWrapperPluginMossaic(logger),
        'questa': JobWrapperPluginMossaicQuesta(logger)
    }.get(application, JobWrapperPlugin(logger))

class JobWrapper(object):
    """ The job wrapper class

    This is a callable class that performs the job wrapper actions. Certain behaviours are specific to the application that is downloaded. These are
    provided via a plugin class called JobWrapperPlugins which groups the specialised functions for each application. The plugins object cannot
    be instantiated until we know what the application is, so this doesn't happen until the job definition has been downloaded.

    Adding support for a new application is just a matter of subclassing the JobWrapperPlugins class and adding a new entry to the _get_plugin function.

    """
    def __init__(self, config):
        """
        Initialise the wrapper by recording the logger and couch instance
        """
        try:
            _validate_url(config.couch_url)
            couch_url = _build_full_url(config.couch_url, config.auth_file)
            self.logger = config.logger
            self.keep_workdir = config.keep
            self.noop = config.noop
            self.http_retries = int(config.http_retries)
            self.initial_backoff = int(config.initial_backoff)
            if couch_url.startswith('https'):
                couch_server = self.__retry_on_status(CouchServer, args=[couch_url], kwargs={"ssl_key": config.ssl_key, "ssl_cert": config.ssl_cert}, max_tenacity=True)
            else:
                couch_server = self.__retry_on_status(CouchServer, args=[couch_url], max_tenacity=True)
            self.databases = {
                "task": self.__retry_on_status(couch_server.connectDatabase, kwargs={"dbname": config.taskdb}, max_tenacity=True),
                "data": self.__retry_on_status(couch_server.connectDatabase, kwargs={"dbname": config.datadb}, max_tenacity=True),
                "releases":self.__retry_on_status(couch_server.connectDatabase, kwargs={"dbname": config.releasedb}, max_tenacity=True),
                "state": self.__retry_on_status(couch_server.connectDatabase, kwargs={"dbname": config.statedb}, max_tenacity=True),
            }
            self.job_id = config.job_id
            assert(self.logger)
            assert(self.job_id)
            random.seed(self.job_id)
            time.sleep(random.random() * 30)
        except CouchError:
            trace = sys.exc_info()[2]
            raise JobWrapperError("Problem connecting to database"), None, trace
        except AssertionError:
            trace = sys.exc_info()[2]
            raise JobWrapperError("Required option not provided"), None, trace

    def __get_runtime(self, stderr):
        """ Extract runtime from stderr """
        line = [l for l in stderr.split('\n') if l.startswith('real')][0]
        return float(line.split(' ')[1])

    def _run(self, cmd, args=None, cwd=None):
        """
        Use subprocess to run a command, and return the stdout. Raise an exception for returncode != 0.

        """
        proc = subprocess.Popen('time -p ' + ' '.join([os.path.join('.', cmd)] + args), shell=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                cwd=cwd)
        stdout, stderr = proc.communicate()

        self.__write_and_close_file(os.path.join(self.working_dir, 'stdout'), stdout)
        self.__write_and_close_file(os.path.join(self.working_dir, 'stderr'), stderr)
        self.definition['stats']['runtime'] = self.__get_runtime(stderr)

        rc = proc.returncode
        if rc != 0:
            self.logger.critical("Non-zero return code (%d) from command: %s",
                                 rc, ''.join(cmd))
            raise JobWrapperError("Subprocess Error")

        return stdout

    def __retry_on_status(self, method, status=[503, 500], args=None, kwargs=None, times=None, wait=1, max_tenacity=False):
        """
        Retry the supplied method if a 503 error occurs, otherwise re-raise exception (but preserve the stack trace!)

        Setting max_tenacity to true will cause a retry on *any exception whatsoever*
        """
        if not times:
            times = self.http_retries + 1
        for i in range(0, times):
            try:
                if args and kwargs:
                    return method(*args, **kwargs)
                elif args:
                    return method(*args)
                elif kwargs:
                    return method(**kwargs)
                else:
                    return method()
            except CouchError, error:
                if (error.status not in status and not max_tenacity) or i == times - 1:
                    raise error, None, sys.exc_info()[2]
            except Exception, error:
                if not max_tenacity or i == times - 1:
                    raise error, None, sys.exc_info()[2]
            max_backoff = random.randint(0, self.initial_backoff) + (wait * pow(2, i)) / 2
            time.sleep(random.random() * max_backoff)

    def __update_job_definition(self):
        """ Set job start time in job definition """
        self.definition['stats'] = {}
        self.definition['stats']['start_time'] = time.time()
        metadata = self.__retry_on_status(self.databases['task'].ecCommitOne, args=[self.definition], max_tenacity=True)
        self.definition['_rev'] = metadata[0]['rev']

    def initialise_job(self):
        try:
            self.get_job_definition()
            
            # delete rogue files
            del self.definition['input_files']['meta']
            del self.definition['input_files']['cross_section']

            self.plugin = _get_plugin(self.logger, self.definition['executable']['application'])
            #Set up a working area under where we're running
            self.working_dir = tempfile.mkdtemp(dir=os.getcwd())
            self.logger.debug('created working dir %s' % self.working_dir)
            self.download_executable()

            self.download_input()

        except CouchError, error:
            trace = sys.exc_info()[2]
            raise JobWrapperError("Could not download input file"), None, trace

        self.create_steering_file()

        try:
            self.__update_job_definition()
        except CouchError, error:
            trace = sys.exc_info()[2]
            raise JobWrapperError("Could not update job with start time"), None, trace

    def create_steering_file(self):
        """
        From the job definition (held in self.definition) create a Chasm/Questa steering file.
        """
        self.steering_filename = self.plugin.get_steering_name()
        steering_file_data = self.__retry_on_status(self.__get_show_and_validate, args=[self.databases['task'], 'workflow', 'steering', self.job_id], max_tenacity=True)
        self.__write_and_close_file(os.path.join(self.working_dir, self.steering_filename), steering_file_data)

    def get_job_definition(self):
        """
        Get the couch document identified by _job_id_ and setup a dictionary containing:
            software - the name of the software to use
            version - the version of that software
            input - a dictionary of id's for the simulations input files. The job must
                    define the following input:
                                boundary
                                geometry
                                soils
                                stability
                                steering
                    and may also define:
                                reinforcements
                                stochastic
                                vegetation
        """
        self.logger.debug('- downloading job definition for job with id %s' % self.job_id)
        def get_job_definition_and_validate():
            definition = self.databases['task'].document(self.job_id)

            if definition['_id'] == self.job_id:
                return definition
            else:
                raise JobWrapperError("Downloaded job definition has invalid id. Expected %s but got %s" % (self.job_id, definition['_id']))
        self.definition = self.__retry_on_status(get_job_definition_and_validate, max_tenacity=True)

    def download_executable(self):
        """
        Executable's are stored in a database. Each release is its own document, with
        an id of software_version. The execuatable is attached to that document. The
        document also contains the release manager and a few other parameters that I
        have written down in the office but currently forget.
        """
        self.logger.debug('- downloading executable')
        software = self.definition['executable']
        application_id = "%s_%s" % (software['application'], software['version'])
        self.logger.debug('Executable: %s' % application_id)
        def get_executable_and_validate():
            try:
                executable_metadata = self.databases['releases'].document(application_id)
                executable_content = self.databases['releases'].getAttachment(application_id, name="executable")
            except CouchError, error:
                self.logger.critical('- download of executable failed')
                raise JobWrapperError("Could not download executable file"), None, sys.exc_info()[2]
            localsum = base64.b64encode(hashlib.md5(executable_content).digest())
            checksum = executable_metadata['_attachments']['executable']['digest'].split('md5-')[1]
            if localsum != checksum:
                raise JobWrapperError("Could not validate checksum for executable with id %s\n\t  Local checksum %s != remote checksum %s" % (application_id, localsum, checksum))
            else:
                return executable_content
        executable_content = self.__retry_on_status(get_executable_and_validate, max_tenacity=True)
        executable_filename = os.path.join(self.working_dir, application_id)
        self.__write_and_close_file(executable_filename, executable_content)
        os.chmod(executable_filename, stat.S_IXUSR | stat.S_IWUSR | stat.S_IRUSR | stat.S_IXGRP | stat.S_IWGRP | stat.S_IRGRP)
        self.executable = application_id

    def __write_and_close_file(self, filename, data):
        """ Helper method for one-shot writes, avoids repeated with statements in this module """
        with open(filename, 'w') as new_file:
            new_file.write(data)

    def __get_substitutions(self, file_type):
        """ Get any substitutions specified by the job definition for the given file type that may be required """
        if 'simulation_parameters' in self.definition and len(self.definition['simulation_parameters']) > 0:
            substitutions = {}
            for parameter_type in self.definition['simulation_parameters'].values():
                if len(parameter_type) > 0:
                    new_subs = dict([(pointer, parameters['value']) for pointer, parameters in parameter_type.items()
                                                        if 'type' in parameters and parameters['type'] == file_type])
                    substitutions.update(new_subs)
            return substitutions
        else:
            return None

    def __get_show_and_validate(self, db, ddoc, show, doc_id, options={}):
        """
        Download output of a show function and validate it using the sha1 option to get the sha1 sum
        """
        show_data = db.loadShow(ddoc, show, doc_id, options)
        new_options = {}
        new_options.update(options)
        new_options['sha1'] = True
        checksum = db.loadShow(ddoc, show, doc_id, new_options)
        localsum = hashlib.sha1(show_data).hexdigest()
        if localsum != checksum:
            raise JobWrapperError("Could not validate checksum for %s file with id %s\n\t  Local checksum %s != remote checksum %s" % (show, doc_id, localsum, checksum))
        return show_data

    def download_input(self):
        """
        Download all the required input via a show function in CouchDB, input_files is
        a dictionary of file_type: id
        """
        for file_type, filename in self.definition['input_files'].items():
            self.logger.debug('- downloading %s file' % file_type)
            try:
                show_name = self.plugin.get_show_name(file_type)
                options = {}
                substitutions = self.__get_substitutions(show_name)
                if substitutions:
                    options['substitutions'] = substitutions
                show_data = self.__retry_on_status(self.__get_show_and_validate, args=[self.databases['data'], 'mossaic', show_name, filename, options], max_tenacity=True)
            except CouchError, error:
                self.logger.critical('- download %s file failed' % file_type)
                raise JobWrapperError("Could not download input file"), None, sys.exc_info()[2]
            show_file = file(os.path.join(self.working_dir, filename), 'w')
            show_file.write(show_data)
            show_file.close()
        # Get a local copy of the geometry file so we can extract location to
        # put in the results doc
        if 'initial_slope' in self.definition['input_files']:
            geometry_id = self.definition['input_files']['initial_slope']
        else:
            geometry_id = self.definition['input_files']['geometry']
            self.logger.debug('- downloading geometry with id %s' % geometry_id)
        def get_geometry_and_validate():
            geometry = self.databases['data'].document(geometry_id)
            if geometry['_id'] == geometry_id:
                return geometry
            else:
                raise JobWrapperError("Downloaded geometry doc has invalid id. Expected %s but got %s" % (geometry_id, geometry['_id']))
        self.geometry = self.__retry_on_status(get_geometry_and_validate, max_tenacity=True)

    def __get_error_document(self, error):
        """
        Return an output summary document with supplied error as field
        """
        document_type = 'results'
        return {'type': document_type, 'error': error.message, 'job_id':self.job_id}

    def process_output(self):
        """
        Parse the job output, record it in couch, tar up the resulting data and attach to the
        result document.
        """
        try:
            results = self.plugin.parse_output(job_id=self.job_id, working_dir=self.working_dir)
        except Exception, error:
            results = self.__get_error_document(error)
        results['task_id'] = self.definition['task_id']
        results['name'] = self.definition['name']
        if (self.geometry and 'geometry' in self.geometry and 'location' in self.geometry['geometry']):
            results['location'] = self.geometry['geometry']['location']
        if not self.noop:
            result_dict = self.upload_result(results)
            time.sleep(1)   # TODO: temporary fix for conflicts when adding attachments to new doc in BigCouch - there will be better ways of
                            # fixing this but for now we just wait a second after our write
            self.logger.warning('Committed results document id = %s' % result_dict['id'])

            self.logger.debug('Uploading result tarball as attachment')
            self.tar_output()
            filename = 'output_%s.tar.gz' % (self.job_id)
            self.__retry_on_status(self.databases['data'].addAttachment, status=[503, 500, 404], args=[
                                                                        result_dict['id'],
                                                                        result_dict['rev'],
                                                                        self.results_tarball,
                                                                        filename,
                                                                        'application/x-gzip'
                                                                    ], max_tenacity=True)
        else:
            self.logger.warning("Not uploading to CouchDB - run without the -n/--noop option to persist data")

    def upload_result(self, result):
        """
        Upload the result, creating a new document in CouchDB, return the id and rev of that
        document in a dictionary
        """
        return self.__retry_on_status(self.databases['data'].ecCommitOne, args=[result], max_tenacity=True)[0]

    def tar_output(self):
        """
        tar.gz the output files
        """
        tar_buffer = StringIO.StringIO()
        tar_file = tarfile.open(name='output_%s' % str(self.job_id), # Force type of job_id to string as it could be unicode, in which case tar operation fails
                                fileobj=tar_buffer, mode='w:gz')

        def filter_executables(filename):
            """ Remove named executable and any other executable files """
            if filename == self.executable:
                return True
            else:
                return False

        working_files = [filename for filename in os.listdir(self.working_dir) if not filter_executables(filename)]
        tar_file.add(os.path.join(self.working_dir), arcname=self.job_id, recursive=False)
        for working_file in working_files:
            tar_file.add(os.path.join(self.working_dir, working_file), arcname=os.path.join(self.job_id, working_file))
        #tar_file.add(self.working_dir, arcname=self.job_id)
        tar_file.close()
        self.results_tarball = tar_buffer.getvalue()
        tar_buffer.close()

    def __steering_filename_to_argument(self, steering_filename):
        """
        Convert steering filename to argument for passing to executables
        """
        return steering_filename[0:-len(JobWrapper.steering_file_suffix)]

    def run_executable(self):
        """
        Run the executable process

        Returns true if executable completes with no errors, false otherwise
        """
        self.logger.debug('Initialising %s' % self.executable)
        try:
            local_steering_file_name = self.plugin.get_steering_name(suffix=False)
            output = self._run(self.executable, args=[local_steering_file_name], cwd=self.working_dir)
            self.logger.debug('Execution of %s complete' % self.executable)
        except Exception, error:
            self.logger.debug('Execution of %s failed with error: %s' % (self.executable, error.message))
            raise error, None, sys.exc_info()[2]

    def __get_final_state(self, exit_status):
        """ Map exit status to job state """
        if exit_status == ExitStatus.OK:
            return 'done'
        else:
            return 'failed'

    def close_job(self, exit_status):
        """ Close the job (and state) documents """
        final_state = self.__get_final_state(exit_status)
        state_docs = self.__retry_on_status(self.databases['state'].loadView, args=['state-machine', 'states_by_job',
                {'key': self.job_id, 'reduce': False, 'include_docs': True, 'stale': 'ok'}], max_tenacity=True)['rows']
        if len(state_docs) < 1:
            state_docs = self.__retry_on_status(self.databases['state'].loadView, args=['state-machine', 'states_by_job',
                    {'key': self.job_id, 'reduce': False, 'include_docs': True}], max_tenacity=True)['rows']
        state_doc = state_docs[0]['doc']
        state_doc['state'] = final_state
        state_doc['state_history'][final_state] = {'timestamp': time.time(), 'modified_by': self.__class__.__name__}
        self.__retry_on_status(self.databases['state'].ecCommitOne, args=[state_doc], max_tenacity=True)
        self.definition['state'] = final_state
        self.definition['state_history'] = state_doc['state_history']
        self.__retry_on_status(self.databases['task'].ecCommitOne, args=[self.definition], kwargs={'viewlist': ['workflow/state']}, max_tenacity=True)

    def clean_up(self):
        """
        Delete the working directory and gtfo
        """
        # if not self.keep_workdir and hasattr(self, 'working_dir'):
        #     self.logger.debug('Cleaning up working dir')
        #     shutil.rmtree(self.working_dir)
        #     self.logger.debug('Deleted working dir')

    def __call__(self):
        """
        The steps to run the job

        Error handling scheme is as follows:
            - Errors when initialising job abort the whole job and run the close job and clean up code
            - Errors when running job do not abort the job as we still want to upload whatever output is available for troubleshooting
            - Errors when processing output do not abort the job as the next step is to close job and clean up anyway

        All errors are logged and cause non-zero exit status to be returned

        """
        exit_status = ExitStatus.OK

        try:
            self.logger.debug("Running job %s" % self.job_id)
            self.initialise_job()
            try:
                self.run_executable()
            except Exception, error:
                self.logger.critical('Error whilst running executable: %s' % error)
                exit_status = ExitStatus.CRITICAL
            try:
                self.process_output()
            except Exception, error:
                self.logger.critical('Error whilst processing output: %s' % error)
                exit_status = ExitStatus.CRITICAL
        except Exception, error:
                self.logger.critical('Error whilst initialising job: %s' % error)
                exit_status = ExitStatus.CRITICAL
        finally:
            try:
                self.close_job(exit_status)
            except Exception, error:
                self.logger.critical('Error whilst closing job: %s' % error)
                exit_status = ExitStatus.CRITICAL

            self.clean_up()

        return exit_status

if __name__ == '__main__':
    print '===================================='
    print 'MoSSaic job wrapper v.%s' % __version__
    print '===================================='

    opts = do_options()
    job = JobWrapper(opts)
    exit_status = job()

    print '===================================='
    print 'MoSSaic job complete'
    print '===================================='

    exit(exit_status)
