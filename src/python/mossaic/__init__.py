"""
MoSSaiC Application Platform
============================

These are the python modules that, together with a series of CouchApps, form
the MoSSaiC Application Platform, a platform for massively parallel execution
of simulation and modelling software.

Available subpackages
---------------------

jobmanager
    Daemons for managing tasks and jobs

Available modules
-----------------

jobwrapper
    Runs on worker nodes, obtains and runs the modelling software
mossaicparser
    Parses input and output files for supported software

"""