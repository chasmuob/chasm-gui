""" mossaicparser.py

This module is a collection of parsers for reading CHASM and QUESTA input/output files.

This consists of a parent class that:
    Has method parse() which returns dict.

Various subclasses:
    Override the parse() method with parsing logic for
    particular type of file.

We use a factory method for instantiating. This method
selects the appropriate subclass based on filename.
CHASM/QUESTA filename formats are considered a local standard.

May be some benefit in using a Factory object instead.

"""
import base64
import csv
import json
import os
import re
import string
import sys
import zlib

from collections import defaultdict

def iterator_ignore_empty(iterator):
    """ Return an iterator that ignores empty rows """
    class IteratorIgnoreEmpty(object):
        def __init__(self, iterator):
            self.iterator = iterator

        def next(self):
            row = self.iterator.next()
            while(len(row) == 0):
                row = self.iterator.next()
            return row

        def __iter__(self):
            return self

    return IteratorIgnoreEmpty(iterator)

def float_nan_as_string(value):
    value = float(value)
    if value != value:  # If a float does not equal itself, it is a NaN (needed for python 2.5 compatibility)
        return u'NaN'
    elif value == float('inf'):
        return u'Infinity'
    else:
        return value

class MossaicParserException(Exception):
    pass


class MossaicParser:
    """ Superclass for parsing Mossaic input/output data """
    @staticmethod
    def get_filetype(filename):
        """ Return the filetype of the supplied filename """
        return {'out': 'questa_output_summary',
            'fos': 'factor_of_safety',
            'results': 'results'}[filename.split('.')[-1]]

    @staticmethod
    def get_parser(filename):
        """ Return the appropriate subclass depending on filename """
        if len(filename) == 0:
            raise IOError('Cannot open file as no filename was provided')
        if not os.path.exists(filename):
            raise IOError(string.join(('File does not exist:', filename)))
        try:
            return filetype_to_parser[MossaicParser.get_filetype(filename)](filename)
        except KeyError, e:
            raise MossaicParserException(string.join(('Cannot create parser for file with unknown type:', filename)))
    
    def parse(self):
        pass


class EngineeringCost(MossaicParser):
    labour_types = ['skilled', 'unskilled']
    equipment_capacities = {
        'lorry': 't',
        'backhoe': 'm3',
        'shovel': 'm3'
    }

    """ Parses and represents QUESTA engineering cost files """
    def __init__(self, filename=None, data=None):
        self.filename = filename
        self.data = data

    def __get_new_item_costs(self, data):
        """ Get new item dict

        data should be an array as follows:
            [item_type, capacity, cost, quantity]

        """
        if len(data) != 4:
            raise ValueError(data)

        item = {
            'cost': {
                'unit': 'EC$h-1',
                'value': [float(data[2])]
            },
            'quantity': [int(data[3])]
        }

        if data[1]:
            item['capacity'] = {
                'unit': EngineeringCost.equipment_capacities[data[0]],
                'value': [float(data[1])]
            }

        return item

    def __get_new_labour_costs(self, data):
        """ Get new labour costs dict

        data should be an array as follows:
            [capacity, cost, quantity]

        """
        if len(data) != 3:
            raise ValueError(data)

        return {
            'cost': {
                'unit': 'EC$h-1',
                'value': float(data[1])
            },
            'quantity': int(data[2])
        }

    def get_costs(self, reader):
        """ Get the engineering costs from the supplied rowset

        Format is:
            item\tcapacity\tcost\tquantity

        For items where a capacity value does not make sense (e.g. skilled/unskilled labour) the capacity value is blank

        """
        equipment = {}
        labour = {}

        try:
            while True:
                row = reader.next()
                if len(row) != 4:
                    raise ValueError(row)
                if row[0] in EngineeringCost.labour_types:
                    labour[row[0]] = self.__get_new_labour_costs(row[1:])
                elif row[0] in EngineeringCost.equipment_capacities:
                    if row[0] in equipment:
                        equipment[row[0]]['capacity']['value'].append(float(row[1]))
                        equipment[row[0]]['cost']['value'].append(float(row[2]))
                        equipment[row[0]]['quantity'].append(int(row[3]))
                    else:
                        equipment[row[0]] = self.__get_new_item_costs(row)
        except StopIteration:
            pass

        return {
            'equipment': equipment,
            'labour': labour
        }

    def parse(self):
        raw_data = ''
        if self.filename:
            raw_data = open(self.filename).read()
        elif self.data:
            raw_data = self.data
        rows = [row.strip() for row in raw_data.split('\n')]
        reader = iterator_ignore_empty(csv.reader(iter(rows), delimiter='\t'))
        data = {'type': 'engineering_cost'}
        data.update(self.get_costs(reader))
        return data


class Geometry(MossaicParser):
    """ Parses and represents QUESTA and CHASM geometry files """
    def __init__(self, filename=None, data=None):
        self.filename = filename
        self.data = data

    def __get_platform(self, row):
        """ Get the platform specified in the geometry file """
        if row[0].lower() != 'platform':
            raise ValueError(row)
        return row[1].lower()

    def __get_road_geometry(self, row):
        """ Get road geometry from supplied row

        Road geometry is in the following format:
            pavement_width total_width downslope_shoulder

        total_width is pavement_width + upslope_shoulder + downslope_shoulder, so we calculate upslope_shoulder

        """
        if len(row) != 3:
            raise ValueError(row)
        road = {
            'upslope_shoulder': {
                'unit': 'm',
                'value': float(row[1]) - float(row[0]) - float(row[2])
            },
            'downslope_shoulder': {
                'unit': 'm',
                'value': float(row[2])
            }
        }
        return road

    def __get_road_link(self, row):
        """ Get road link data from supplied row

        Road link data is in the following format:
            from_node to_node

        """
        if len(row) != 2:
            raise ValueError(row)
        return [int(row[0]), int(row[1])]

    def __get_breadth(self, row):
        """ Get breadth data

        Breadth data format is:
            breadth

        """
        if len(row) != 1:
            raise ValueError(row)
        return {
            'unit': 'm',
            'value': float(row[0])
        }

    def __get_number_of_sections(self, row):
        """ Get number of upslope and downslope sections """
        if len(row) != 2:
            raise ValueError(row)
        return int(row[0]), int(row[1])

    def __get_slope_sections(self, reader):
        """ Get slope sections

        Slope section data is in the following format:
            number_of_upslope_sections number_of_downslope_sections
            section_height surface_length angle
            section_height surface_length angle
            ...

        Note that only one of section_height and surface_length will be a real value, the other will be 0.

        CODE QUALITY ALERT: This function also retrieves the investment parameter from the final upslope section, if available.
        This is some fairly nasty coupling and results in some odd looking code, however due to the format of the file we are parsing
        we have to do something like this, or read the whole file into memory rather than using a line-based iterator.
        We also retrieve the index of the road section and pass it to the calling code, where it is added to the road
        data. Again, not ideal but we are forced into this by the file format.

        """
        number_of_upslope_sections, number_of_downslope_sections = self.__get_number_of_sections(reader.next())
        number_of_sections = number_of_upslope_sections + number_of_downslope_sections + 1
        sections = []
        investment = 0
        for i in range(number_of_sections):
            row = reader.next()
            if not (len(row) == 3 or (len(row) == 4 and i + 1 == number_of_upslope_sections)):
                raise ValueError(row)
            height, length, angle = float(row[0]), float(row[1]), float(row[2])
            section = {}
            if height == 0:
                section['surface_length'] = {
                    'unit': 'm',
                    'value': length
                }
            elif length == 0:
                section['surface_height'] = {
                    'unit': 'm',
                    'value': height
                }
            else:
                raise ValueError(row)
            section['angle'] = {
                'unit': 'deg',
                'value': angle
            }
            if i + 1 == number_of_upslope_sections and len(row) == 4:
                investment = float(row[3])
            sections.append(section)

        investment = {
            "cut_slope_increment": {
                "unit": "deg",
                "value": investment
            }
        }
        road_section_index = number_of_upslope_sections
        return sections, investment, road_section_index

    def __get_depth_measurements(self, reader, number_of_sections, invert_surface_distance=False):
        """ Get depth measurements - could be water table or soil strata

        invert_surface_distance is required if we are getting depth measurements of upslope sections because
        we store all sections as absolute from-cut-slope-toe measurements.

        Depth measurements are of the following format:

            surface_distance_from_cut_toe depth_1 depth_2 ... depth_n
            surface_distance_from_cut_toe depth_1 depth_2 ... depth_n
            ...
            surface_distance_from_cut_toe depth_1 depth_2 ... depth_n

        If only one depth measurement is provided then depth is a single dict, if more than one depth measurement then it is an array of dicts.
        It would be logical to just have an array with one item for a single depth but this doesn't fit with how we have modelled water table
        depth measurements (there can only ever be one water table, so will only be one depth measurement).

        """
        sections = []
        for i in range(number_of_sections):
            row = reader.next()
            if len(row) < 2:
                raise ValueError(row)
            section = {
                'depth': {
                    'unit': 'm'
                },
                'surface_distance_from_cut_toe': {
                    'unit': 'm'
                }
            }
            section['surface_distance_from_cut_toe']['value'] = float(row[0])
            if invert_surface_distance:
                section['surface_distance_from_cut_toe']['value'] *= -1
            if len(row) == 2:
                section['depth']['value'] = float(row[1])
            else:
                section['depth']['value'] = [];
                for strata in range(1, len(row)):
                    section['depth']['value'].append(float(row[strata]))
            sections.append(section)
        return sections

    def __get_water_table(self, reader):
        """ Get water table data

        Water table data is in the following format:
            number_of_soil_layers number_of_upslope_sections number_of_downslope_sections
            surface_distance_from_cut_toe depth
            surface_distance_from_cut_toe depth
            ...

        """
        number_of_upslope_sections, number_of_downslope_sections = self.__get_number_of_sections(reader.next())
        upslope_sections = self.__get_depth_measurements(reader, number_of_upslope_sections,
                invert_surface_distance=True)
        downslope_sections = self.__get_depth_measurements(reader, number_of_downslope_sections,
                invert_surface_distance=False)
        return upslope_sections + downslope_sections;

    def __get_soil_strata_approximate(self, reader):
        """ Get soil strata data

        """
        row = reader.next()
        return {
            'surface_distance_from_cut_toe': {
                'unit': 'm',
                'value': float(row[0])
            },
            'interfaces': {
                'depth': {
                    'unit': 'm',
                    'value': [float(value) for value in row[1::2]]
                },
                'angle': {
                    'unit': 'deg',
                    'value': [float(value) for value in row[2::2]]
                }
            }
        }

    def __get_soil_strata(self, reader):
        """ Get soil strata data

        Soil strata can be specified either using measurements or estimations.
        For estimations:

            number_of_soil_layers unused_number 99
            height_above_cut_toe depth_of_interface_1 angle_of_interface_1 ... depth_of_interface_n angle_of_interface_n

        Interface 1 is the interface between the top layer of soil and the layer below.

        For measurements:
            number_of_soil_layers number_of_upslope_sections number_of_downslope_sections
            surface_distance_from_cut_toe depth_1 depth_2 ... depth_n
            surface_distance_from_cut_toe depth_1 depth_2 ... depth_n
            ...

        This is identical to the format used for water table, execpt multiple depth measurements will exist for each soil interface

        """
        row = reader.next()
        if len(row) != 3:
            raise ValueError(row)
        number_of_soils = int(row[0])
        if int(row[2]) == 99:
            return self.__get_soil_strata_approximate(reader)
        else:   # Anything other than magic number 99 means we are using measurements
            number_of_upslope_sections, number_of_downslope_sections = int(row[1]), int(row[2])
            upslope_sections = self.__get_depth_measurements(reader, number_of_upslope_sections,
                    invert_surface_distance=True)
            downslope_sections = self.__get_depth_measurements(reader, number_of_downslope_sections,
                    invert_surface_distance=False)
            return upslope_sections + downslope_sections

    def __get_mesh(self, reader):
        """ Get mesh

        Mesh data is in the following format:

            column_width cell_depth

        """
        row = reader.next()
        if len(row) != 2:
            raise ValueError(row)
        return {
            'column_width': {
                'unit': 'm',
                'value': float(row[0])
            },
            'cell_depth': {
                'unit': 'm',
                'value': float(row[1])
            }
        }

    def __get_geometry(self, reader):
        """ Get the geometry """
        geometry = {}
        geometry['road'] = self.__get_road_geometry(reader.next())
        geometry['road_link'] = self.__get_road_link(reader.next())
        geometry['breadth'] = self.__get_breadth(reader.next())
        geometry['slope_sections'], investment, geometry['road']['slope_section'] = self.__get_slope_sections(reader)
        geometry['water_table'] = self.__get_water_table(reader)
        #self.__add_water_table(geometry['slope_sections'], geometry['road']['slope_section'], reader)
        geometry['soil_strata'] = self.__get_soil_strata(reader);
        #self.__add_soil_strata(geometry, geometry['road']['slope_section'], reader)
        geometry['mesh'] = self.__get_mesh(reader)
        return geometry, investment

    def __get_column_cells(self, reader, number_of_cells):
        """ Get cells for a column of CHASM geometry data """
        raw_cells = reader.next()
        if len(raw_cells) != number_of_cells * 2:
            raise ValueError(raw_cells)

        cells = {
            'cell_depth': {
                'value': [],
                'unit': 'm'
            },
            'soil_type': []
        }
        for i in range(len(raw_cells))[0::2]:
            cells['cell_depth']['value'].append(float(raw_cells[i]))
            cells['soil_type'].append(int(raw_cells[i + 1]))

        return cells
    
    def __get_geometry_column(self, reader):
        """ Get a column of CHASM geometry data """
        metadata = reader.next()
        number_of_cells = int(metadata[0])
        column = {}
        column['number_of_saturated_cells'] = int(metadata[1])
        column['column_width'] = {
            'value': float(metadata[2]),
            'unit': 'm'
        }
        column['column_breadth'] = {
            'value': float(metadata[3]),
            'unit': 'm'
        }
        column['initial_surface_suction'] = {
            'value': float(metadata[4]),
            'unit': 'm'
        }
        column['cells'] = self.__get_column_cells(reader, number_of_cells)
        return column

    def __get_geometry_chasm(self, reader, number_of_columns):
        """ Get geometry for CHASM """
        geometry = []
        for i in range(number_of_columns):
            geometry.append(self.__get_geometry_column(reader))
        if len(geometry) != number_of_columns:
            raise ValueError(geometry)
        return geometry

    def __is_questa(self, row):
        """ Determine whether file is a QUESTA geometry file """
        return row[0].lower() == 'platform'

    def __get_end_boundary_condition(self, reader):
        """ Get end boundary condition from CHASM geometry file """
        boundary_conditions = reader.next()
        return {
            'soil_type': int(boundary_conditions[0]),
            'number_of_saturated_cells': int(boundary_conditions[1])
        }

    def parse(self):
        raw_data = ''
        if self.filename:
            raw_data = open(self.filename).read()
        elif self.data:
            raw_data = self.data
        rows = [row.strip() for row in raw_data.split('\n')]
        reader = iterator_ignore_empty(csv.reader(iter(rows), delimiter=' '))
        data = {'type': 'geometry'}
        first_row = reader.next()
        if self.__is_questa(first_row) == True:
            data['platform'] = self.__get_platform(first_row)
            data['geometry'], data['investment'] = self.__get_geometry(reader)
        else:
            number_of_columns = int(first_row[0])
            data['geometry'] = {
                'mesh_data': self.__get_geometry_chasm(reader, number_of_columns)
            }
            data['end_boundary_condition'] = self.__get_end_boundary_condition(reader)

        return data


class Soils(MossaicParser):
    """ Parses and represents CHASM and QUESTA soils input files """
    class CurveModel(object):
        """ Suction moisture curve models """
        MILLINGTON_QUIRK = 0
        VAN_GENUCHTEN = 1

    def __init__(self, filename=None, data=None):
        self.filename = filename
        self.data = data

    def __get_curve_model(self, number):
        """ Return the curve model for the supplied number """
        return {
            0: self.CurveModel.MILLINGTON_QUIRK,
            1: self.CurveModel.VAN_GENUCHTEN
        }[number]

    def __get_suction_moisture_curve(self, reader):
        """ Get suction moisture curve for a given soil entry

        Format is:
            curve_model_type
        And then, if curve_model_type is 1 (Van Genuchten):
            van_genuchten_parameters_id
        Or, if curve_model_type is 0 (Millington Quirk):
            moisture_content_point_1 ... moisture_content_point_n
            suction_point_1 ... suction_point_n

        """
        row = reader.next()
        if len(row) != 1:
            raise ValueError(row)
        curve_model = self.__get_curve_model(int(row[0]))
        if curve_model == self.CurveModel.MILLINGTON_QUIRK:
            row = reader.next()
            if len(row) != 1:
                raise ValueError(row)
            number_of_data_points = int(row[0])
            curve = {
                'moisture_content': [float(moisture_value) for moisture_value in reader.next()],
                'suction': [float(suction_value) for suction_value in reader.next()]
            }
            if len(curve['moisture_content']) != number_of_data_points:
                raise ValueError(curve['moisture_content'])
            if len(curve['suction']) != number_of_data_points:
                raise ValueError(curve['suction'])
            return curve
        elif curve_model == self.CurveModel.VAN_GENUCHTEN:
            row = reader.next()
            if len(row) != 1:
                raise ValueError(row)
            return int(row[0])

    def __get_soil_parameters_stochastic(self, row):
        """ Get stochastic soil parameters """
        num_params = len(row)
        if num_params != 12 and num_params != 13:
            raise ValueError(row)
        params = {}
        params['Ksat'] = {
            'unit': 'ms-1',
            'mean': float(row[0]),
            'standard_deviation': float(row[1])
        }
        params['saturated_moisture_content'] = {
            'unit': 'm3m-3',
            'mean': float(row[2]),
            'standard_deviation': float(row[3])
        }
        params['saturated_bulk_density'] = {
            'unit': 'kNm-3',
            'mean': float(row[4]),
            'standard_deviation': float(row[5])
        }
        params['unsaturated_bulk_density'] = {
            'unit': 'kNm-3',
            'mean': float(row[6]),
            'standard_deviation': float(row[7])
        }
        params['effective_cohesion'] = {
            'unit': 'kNm-2',
            'mean': float(row[8]),
            'standard_deviation': float(row[9])
        }
        params['effective_angle_of_internal_friction'] = {
            'unit': 'deg',
            'mean': float(row[10]),
            'standard_deviation': float(row[11])
        }
        if num_params == 13:
            params['grade'] = {
                'value': int(row[12])
            }
        return params

    def __get_soil_parameters_deterministic(self, row):
        """ Get deterministic soil parameters """
        num_params = len(row)
        if num_params != 6 and num_params != 7:
            raise ValueError(row)
        params = {}
        params['Ksat'] = {
            'unit': 'ms-1',
            'value': float(row[0])
        }
        params['saturated_moisture_content'] = {
            'unit': 'm3m-3',
            'value': float(row[1])
        }
        params['saturated_bulk_density'] = {
            'unit': 'kNm-3',
            'value': float(row[2])
        }
        params['unsaturated_bulk_density'] = {
            'unit': 'kNm-3',
            'value': float(row[3])
        }
        params['effective_cohesion'] = {
            'unit': 'kNm-2',
            'value': float(row[4])
        }
        params['effective_angle_of_internal_friction'] = {
            'unit': 'deg',
            'value': float(row[5])
        }
        if num_params == 7:
            params['grade'] = {
                'value': int(row[6])
            }
        return params

    def __get_soil(self, reader):
        """ Get values for a single soil entry """
        row = reader.next()
        soil = {}
        if len(row) == 6 or len(row) == 7:
            soil.update(self.__get_soil_parameters_deterministic(row))
        elif len(row) == 12 or len(row) == 13:
            soil.update(self.__get_soil_parameters_stochastic(row))
        else:
            raise ValueError(row)
        soil['suction_moisture_curve'] = self.__get_suction_moisture_curve(reader)
        return soil

    def __get_soils(self, reader, number_of_soils):
        """ Get soil values """
        soils = []
        try:
            while True:
                soils.append(self.__get_soil(reader))
        except StopIteration:
            if len(soils) != number_of_soils:
                raise ValueError(soils)
        return soils

    def parse(self):
        raw_data = ''
        if self.filename:
            raw_data = open(self.filename).read()
        elif self.data:
            raw_data = self.data
        rows = [row.strip() for row in raw_data.split('\n')]
        reader = iterator_ignore_empty(csv.reader(iter(rows), delimiter=' '))
        data = {'type': 'soils'}
        row = reader.next()
        if len(row) != 1:
            raise ValueError(row)
        number_of_soils = int(row[0])
        data['soils'] = self.__get_soils(reader, number_of_soils)
        return data


class BoundaryConditions(MossaicParser):
    """ Parses and represents CHASM and QUESTA boundary conditions input data """
    def __init__(self, filename=None, data=None):
        self.filename = filename
        self.data = data

    def __get_upslope_recharge(self, row):
        """ Get upslope recharge """
        if len(row) != 1:
            raise ValueError(row)
        return {
            'unit': 'mh-1',
            'value': float(row[0])
        }

    def __get_detention_capacity_and_soil_evaporation(self, row):
        """ Get detention capacity and soil evaporation - in one method as data is on one row """
        detention_capacity = {'unit': 'm'}
        soil_evaporation = {'unit': 'ms-1'}

        if len(row) == 2:
            detention_capacity['value'] = float(row[0])
            soil_evaporation['value'] = float(row[1])
        elif len(row) == 4:
            detention_capacity['mean'] = float(row[0])
            detention_capacity['standard_deviation'] = float(row[1])
            soil_evaporation['mean'] = float(row[2])
            soil_evaporation['standard_deviation'] = float(row[3])
        else:
            raise ValueError(row)

        return detention_capacity, soil_evaporation

    def __get_rainfall_design(self, reader, storm_meta):
        """ Get design storm rainfall values

        Design storm data is in the following format:
            storm_start_hour storm_end_hour
            intensity intensity ... intensity
            intensity intensity ... intensity
            ...
            intensity intensity ... intensity

        The number of intensities per line is not fixed. Line breaks can occur at any point. The storm data ends when an intensity value has been given for
        each storm hour.

        """
        storm_data = {}
        storm_data['start'] = {
            'unit': 'h',
            'value': int(storm_meta[0])
        }
        storm_end = int(storm_meta[1])   # Not used but we need it to know when to stop reading values
        storm_length = 1 + storm_end - storm_data['start']['value']

        if len(storm_meta) == 3:
            storm_data['probability'] = float(storm_meta[2])

        intensities = []
        for row in reader:
            intensities.extend([float(intensity) for intensity in row])
            if len(intensities) == storm_length:
                break

        storm_data['rainfall_intensities'] = {
            'unit': 'mh-1',
            'value': intensities
        }

        return storm_data

    def __get_rainfall_realistic(self, reader, storm_meta):
        """ Get realistic rainfall values

        Realistic rainfall values are in the following format:

            probability
            intensity_hour_1 intensity_hour_2 ... intensity_hour_24
            probability
            intensity_hour_1 intensity_hour_2 ... intensity_hour_24
            ...

        """
        rainfall = {'unit': 'mh-1', 'days':[]}
        days = int(storm_meta[0])
        try:
            while(True):
                row = reader.next()
                if len(row) == 1:
                    rainfall['days'].append({
                        'probability': float(row[0]),
                        'intensities': [float(intensity) for intensity in reader.next()]
                    })
        except StopIteration:
            pass
        return rainfall

    def __get_storm(self, reader):
        """ Get data for one storm

        Read storm metadata and call the appropriate method to read in the rainfall data

        Metadata is in the following format:
            storm_start_hour storm_end_hour

        Or for realistic ranfall:
            total_storm_dats

        """
        storm_meta = reader.next()

        if len(storm_meta) == 1:
            return self.__get_rainfall_realistic(reader, storm_meta)
        else:
            return self.__get_rainfall_design(reader, storm_meta)

    def __is_realistic(self, storm_data):
        """ Determine whether supplied storm data is for a realistic storm (as opposed to a design storm) """
        return "days" in storm_data

    def __get_storms(self, reader):
        """ Get data for storms """
        first_storm = self.__get_storm(reader)
        if self.__is_realistic(first_storm):
            return {'rainfall': first_storm}
        else:
            storm_data = [first_storm]
            try:
                while True:
                    storm_data.append(self.__get_storm(reader))
            except StopIteration:
                pass
            return {'storm': storm_data}

    def parse(self):
        raw_data = ''
        if self.filename:
            raw_data = open(self.filename).read()
        elif self.data:
            raw_data = self.data
        rows = [row.strip() for row in raw_data.split('\n')]
        reader = iterator_ignore_empty(csv.reader(iter(rows), delimiter=' '))
        data = {'type': 'boundary_conditions'}
        data['upslope_recharge'] = self.__get_upslope_recharge(reader.next())
        data['detention_capacity'], data['soil_evaporation'] = self.__get_detention_capacity_and_soil_evaporation(reader.next())
        data.update(self.__get_storms(reader))
        return data


class Stability(MossaicParser):
    """ Parses and represents CHASM stability input data """
    def __init__(self, filename=None, data=None):
        self.filename = filename
        self.data = data

    def __get_grid_search_parameters(self, row):
        """ Get grid search parameter object from row """
        if len(row) != 8:
            raise ValueError(row)
        return {
            'origin': {
                'unit': 'm',
                'x': float(row[0]),
                'y': float(row[1])
            },
            'spacing': {
                'unit': 'm',
                'x': float(row[2]),
                'y': float(row[3])
            },
            'grid_size': {
                'unit': 'm',
                'x': float(row[4]),
                'y': float(row[5])
            },
            'radius': {
                'unit': 'm',
                'initial': float(row[6]),
                'increment': float(row[7])
            }
        }

    def __get_coordinates(self, reader):
        """ Get a set of coordinates from rowset

        Format is:

            number_of_coordinates
            x_1 y_1
            x_2 y_2
            ...
            x_n y_n

        """
        row = reader.next()
        if len(row) != 1:
            raise ValueError(row)
        number_of_coordinates = int(row[0])
        coordinates = [reader.next() for i in range(number_of_coordinates)]
        x = [float(row[0]) for row in coordinates]
        y = [float(row[1]) for row in coordinates]
        return {
            "x": x,
            "y": y
        }

    def __get_slip_surface_coordinates(self, reader):
        """ Get slip surface coordinates (janbu user-defined) from rowset

        Format is:

            surface_y_coordinate_at_slope_top surface_y_coordinate_at_slope_bottom
            y_coordinate_2_lower_bound y_coordinate_2_upper_bound
            ...
            y_coordinate_n-1_lower_bound y_coordinate_n-1_upper_bound
            x_coordinate_1
            ...
            x_coordinate_n

        """
        coordinates = {}
        janbu_type = reader.next()
        if len(janbu_type) != 1:
            raise ValueError(janbu_type)
        automated = bool(int(janbu_type[0]))
        if automated:
            max_min = reader.next()
            if len(max_min) != 2:
                raise ValueError(max_min)
            coordinates['y'] = {
                'max': float(max_min[0]),
                'min': float(max_min[1])
            }
            coordinates['y']['bounds'] = {}
            rest = [row for row in reader]
            if False in [len(row) <= 2 for row in rest]:
                raise ValueError(rest)
            coordinates['y']['bounds']['lower'] = [float(row[0]) for row in rest if len(row) == 2]
            coordinates['y']['bounds']['upper'] = [float(row[1]) for row in rest if len(row) == 2]
            coordinates['x'] = [float(row[0]) for row in rest if len(row) == 1]
        else:
            return self.__get_coordinates(reader)
        return coordinates

    def __get_stability_analysis_algorithm(self, row):
        """ Get stability analysis algorithm from row """
        if len(row) != 1:
            raise ValueError(row)
        return row[0]

    def parse(self):
        raw_data = ''
        if self.filename:
            raw_data = open(self.filename).read()
        elif self.data:
            raw_data = self.data
        rows = [row.strip() for row in raw_data.split('\n')]
        data = {'type': 'stability'}
        reader = iterator_ignore_empty(csv.reader(iter(rows), delimiter=' '))
        data['stability_analysis_algorithm'] = self.__get_stability_analysis_algorithm(reader.next())
        if data['stability_analysis_algorithm'].lower() == 'bishop':
            data['grid_search_parameters'] = self.__get_grid_search_parameters(reader.next())
            data['slope_surface_coordinates'] = self.__get_coordinates(reader)
        elif data['stability_analysis_algorithm'].lower() == 'janbu':
            data['slope_surface_coordinates'] = self.__get_coordinates(reader)
            data['slip_surface_coordinates'] = self.__get_slip_surface_coordinates(reader)
        else:
            raise ValueError(data['stability_analysis_algorithm'])
        return data

class FactorOfSafety(MossaicParser):
    """ Parses and represents CHASM/QUESTA factor of safety data """

    column_mapping = {
        'End Hr': {
            'name': 'time',
            'unit': 'h',
            'type': int
        },
        'FOS': {
            'name': 'factor_of_safety',
            'type': float_nan_as_string
        },
        'Radius': {
            'name': 'radius',
            'type': float_nan_as_string,
            'unit': 'm'
        },
        'Mass': {
            'name': 'mass',
            'type': float_nan_as_string,
            'unit': 'm3'
        },
        'Moisture': {
            'name': 'moisture',
            'type': float_nan_as_string,
            'unit': 'm3'
        },
        'Infux': {
            'name': 'infux',
            'type': float_nan_as_string,
            'unit': 'ms-1'
        },
        'Precip': {
            'name': 'precipitation',
            'type': float_nan_as_string,
            'unit': 'ms-1'
        },
        'Runout': {
            'name': 'runout',
            'type': float_nan_as_string,
            'unit': 'm'
        },
        'Seismicity': {
            'name': 'seismicity',
            'type': float_nan_as_string,
            'unit': 'g'
        }
    }

    column_arrays = {
        'Centre': {
            'name': 'centre',
            'type': float_nan_as_string,
            'unit': 'm',
            'children': ['x', 'y']
        }
    }

    def __init__(self, filename):
        """ Initialises the parser making filename an object variable """
        self.filename = filename

    def __prepare_output_dict(self, column_headers, output_dict):
        """ Prepares the output dict which will contain the parsed data, returning a map of column numbers to output dict element and a list of column data types

        The output dict is prepared by reading the column headers and looking them up in the column_mapping and column_arrays class dicts.
        There are three cases to handle:
            - column header in column_mapping, column_mapping specifies no unit
                - a new entry is created in the output dict where key = name specified in column_mapping and value is an empty list
                - column_to_output_map points to empty list
            - column header in column_mapping, column mapping specified unit
                - a new entry is created in the output dict where key = name specified in column_mapping and value is dict containing:
                    - 'unit' <-- The unit specified in column_mapping
                    - 'value' <-- An empty list
                - column_to_output_map points to the empty list in the 'value' element in the dict
            - column header in column_arrays
                - a new entry is created in the output dict where key = name specified in column_arrays and value is a dict containing keys
                  for each of the children specified in the column_arrays dict which each have a value of an empty list
                - a new column_to_output_map element is appended for each child which points to the empty list for that child

        Note: This is a bit of a confusing method as it does three different things, however those things are all interdependent and separating them out would
        create repitition.

        Arguments:
        column_headers -- an iterator containing the header for each column
        output_dict -- the dictionary that will contain the parsed data

        Returns:
        column_to_output_map -- an array that maps column values to their intended location in the output dict
        column_types -- an array that contains the type of each column value

        """
        column_to_output_map = []
        column_types = []
        try:
            while(True):
                header = column_headers.next()
                if header in FactorOfSafety.column_mapping:
                    column_metadata = FactorOfSafety.column_mapping[header]
                    if 'unit' in column_metadata:
                        output_dict[column_metadata['name']] = {
                            'unit': column_metadata['unit'],
                            'value': []
                        }
                        column_to_output_map.append(output_dict[column_metadata['name']]['value'])
                        column_types.append(column_metadata['type'])
                    else:
                        output_dict[column_metadata['name']] = []
                        column_to_output_map.append(output_dict[column_metadata['name']])
                        column_types.append(column_metadata['type'])
                elif header in FactorOfSafety.column_arrays:
                    column_metadata = FactorOfSafety.column_arrays[header]
                    output_dict[column_metadata['name']] = {}
                    for i in range(len(column_metadata['children'])):
                        output_dict[column_metadata['name']][column_metadata['children'][i]] = []
                        column_to_output_map.append(output_dict[column_metadata['name']][column_metadata['children'][i]])
                        column_types.append(column_metadata['type'])
        except StopIteration:
            pass

        return column_to_output_map, column_types

    def __preprocess_row(self, row, expected_length):
        """ Workaround for bug in CHASM 4.15 - if there are too many columns, and the first and third columns are equal, remove the third column """
        if len(row) != expected_length and row[0] == row[2]:
            return row[0:2] + row[3:]
        else:
            return row

    def parse(self):
        """ Parse factor of safety file

        Obtain a map of column numbers to data structures in the output dict, and a list containing the data type of each column
        We then iterate through each row, converting each column value to the required type and appending it to the appropriate data structure in the output dict.

        """
        rows = [row.strip() for row in open(self.filename).read().split('\n')]
        reader = csv.reader(iter(rows), delimiter='\t')
        data = {}
        try:
            column_to_output_map, column_types = self.__prepare_output_dict(iter(reader.next()), data)
            for row in reader:
                if len(row) == 0:   # Skip blank lines
                    continue
                row = self.__preprocess_row(row, len(column_to_output_map))    # Remove extra column introduced by CHASM bug if it is present
                if len(row) != len(column_to_output_map):
                    raise ValueError('Unexpected row length')
                for column in range(len(row)):
                    column_to_output_map[column].append(column_types[column](row[column]))
        except Exception, e:
            raise MossaicParserException(string.join(('Unable to parse chasm factor of safety file:', str(e)))), None, sys.exc_info()[2]
        return data


class QuestaOutputSummary(MossaicParser):
    """ Parses and represents QUESTA output summary data 
    
    Most of the data is found in tab separated rows. Each row defines a slope state from the simulation.
    
    Methods which read values from the data advance the iterator to the state expected by the next method. This means the methods in parse() must be called
    in the written order.
    
    Values can either be single cell values or multi-cell arrays. Each multi-cell array is one of a tuple of arrays (x,y or m,c).
    Arrays can be part of a series of arrays.
    
    We make the following assumptions about the output file:
    
        - We cannot depend upon the order of the fields for slope definition
        - The number of soil definitions is variable
        - The number of coordinates for slope and soil definitions are variable
        - The number of coordinates for the slope and each soil definition may not always match
        - Arrays are always specified using the [number] postfix
    
    In order to parse the output file under these assumptions we need to carry out the following steps:
        - For single cell values:
            - A dict slope_data_mapping maps the column name in the slope data to a name, unit and data type
            - We read the column names from the mapping dict and determine the column indices of the data we wish to read
            - The column indices, along with the unit and type, are stored in a slope_format dict, keyed by the name that will be used in the output
            - We then build the output dict for each row, using the slope_format dict to find the column index for each value
        - For multi-cell arrays:
            - A dict slope_data_arrays maps the name of the first cell in the array to the name of the array, its parent object and the cell spacing
            - The 'common_regexp' field is used to find multiple arrays that belong to a single series
            - We read the array names from the mapping dict and determine the start and end columns for each array
            - The start, end and cell spacing data are stored in the slope_format_dict, keyed by the name of the parent object
            - Arrays that are part of a series of arrays are stored using an arrays of start, end and cell spacing values rather than single values
    
    The slope_format dict is used by the __get_slope method to find the appropriate values for the json output. The output json format is hard-coded into 
    this method.
    
    """
    # Map field names to full name, type and unit
    slope_data_mapping = {
        'invest EC$': {
            'name': 'investment',
            'unit': 'EC$',
            'type': float_nan_as_string
        },
        'profile h': {
            'name': 'profile_height',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'profile l': {
            'name': 'profile_length',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'mesh h': {
            'name': 'mesh_height',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'mesh l': {
            'name': 'mesh_length',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'slope area': {
            'name': 'slope_area',
            'unit': 'm2',
            'type': float_nan_as_string
        },
        'nb_scenario': {
            'name': 'number_of_design_storms',
            'type': int
        },
        'hour': {
            'unit': 'h',
            'name': 'failure_time',
            'type': int
        },
        'FOS': {
            'name': 'factor_of_safety',
            'type': float_nan_as_string
        },
        'failure type': {
            'name': 'failure_type',
            'type': int
        },
        'failed area': {
            'name': 'failed_area',
            'unit': 'm2',
            'type': float_nan_as_string
        },
        'changed area': {
            'name': 'changed_area',
            'unit': 'm2',
            'type': float_nan_as_string
        },
        'failed vol': {
            'name': 'failed_volume',
            'unit': 'm3',
            'type': float_nan_as_string
        },
        'cut vol': {
            'name': 'cut_volume',
            'unit': 'm3',
            'type': float_nan_as_string
        },
        'clear time': {
            'name': 'clear_time',
            'unit': 'h',
            'type': float_nan_as_string
        },
        'clear cost': {
            'name': 'clear_cost',
            'unit': 'EC$',
            'type': float_nan_as_string
        },
        'cut time': {
            'name': 'cut_time',
            'unit': 'h',
            'type': float_nan_as_string
        },
        'cut cost': {
            'name': 'cut_cost',
            'unit': 'EC$',
            'type': float_nan_as_string
        },
        'total time': {
            'name': 'total_time',
            'unit': 'h',
            'type': float_nan_as_string
        },
        'total cost': {
            'name': 'total_cost',
            'unit': 'EC$',
            'type': float_nan_as_string
        },
        'x centre': {
            'name': 'centre_x',
            'type': float_nan_as_string
        },
        'y centre': {
            'name': 'centre_y',
            'type': float_nan_as_string
        },
        'crest x': {
            'name': 'crest_x',
            'type': float_nan_as_string
        },
        'crest y': {
            'name': 'crest_y',
            'type': float_nan_as_string
        },
        'toe x': {
            'name': 'toe_x',
            'type': float_nan_as_string
        },
        'toe y': {
            'name': 'toe_y',
            'type': float_nan_as_string
        },
        'radius': {
            'name': 'radius',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'H[0]': {
            'name': 'landslide_height_finley',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'H[1]': {
            'name': 'landslide_height_weighted_angle',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'D[0]': {
            'name': 'landslide_depth_finley',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'D[1]': {
            'name': 'landslide_depth_weighted_angle',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'A[0]': {
            'name': 'landslide_slope_angle_finley',
            'unit': 'deg',
            'type': float_nan_as_string
        },
        'A[1]': {
            'name': 'landslide_slope_angle_weighted_angle',
            'unit': 'deg',
            'type': float_nan_as_string
        },
        'R[0]': {
            'name': 'cutslope_toe_to_runout_toe_finley',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'R[1]': {
            'name': 'cutslope_toe_to_runout_toe_weighted_angle',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'H4[0]': {
            'name': 'runout_depth_at_cutslope_toe_finley',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'H4[1]': {
            'name': 'runout_depth_at_cutslope_toe_weighted_angle',
            'unit': 'm',
            'type': float_nan_as_string
        },
        'cut angle 1': {
            'name': 'cut_angle_upper',
            'unit': 'deg',
            'type': float_nan_as_string
        },
        'cut angle 2': {
            'name': 'cut_angle_lower',
            'unit': 'deg',
            'type': float_nan_as_string
        }
    }
    
    # Map slope data arrays to parent name, tuple name and cell spacing
    slope_data_arrays = {
        'slope x[0]': {
            'parent': 'slope',
            'name': 'x',
            'cell_spacing': 2
        },
        'slope m[0]': {
            'parent': 'slope',
            'name': 'm',
            'cell_spacing': 2
        },
        'slope c[0]': {
            'parent': 'slope',
            'name': 'c',
            'cell_spacing': 2
        },
        'water m[0]': {
            'parent': 'water',
            'name': 'm',
            'cell_spacing': 2
        },
        'water c[0]': {
            'parent': 'water',
            'name': 'c',
            'cell_spacing': 2
        },
        'soil[0] m[0]': {
            'parent': 'soils',
            'name': 'm',
            'cell_spacing': 2,
            'common_regexp': r'soil\[[0-9]+\] m\[[0-9]+\]' # Prefix that all arrays of this type share
        },
        'soil[0] c[0]': {
            'parent': 'soils',
            'name': 'c',
            'cell_spacing': 2,
            'common_regexp': r'soil\[[0-9]+\] c\[[0-9]+\]'
        },
        'intersection x[0]': {
            'parent': 'intersection',
            'name': 'x',
            'cell_spacing': 2
        },
        'intersection y[0]': {
            'parent': 'intersection',
            'name': 'y',
            'cell_spacing': 2
        }
    }
    
    def __init__(self, filename):
        """ Initialises the parser making filename an object variable """
        self.filename = filename
    
    def __get_field_names(self, reader):
        """ Extract field names from raw_data 
        
        Arguments:
        reader -- iterator of lists, expected to be at the position of the field names row
                
        """
        return reader.next()
    
    def __get_slope_rows(self, reader):
        """ Extract rows of slope data from raw_data 
        
        Arguments:
        reader -- iterator of lists, expected to be at the position of the first slope row.
        
        Note: This method is impolite in that it consumes a line that would be better consumed by another method (the 'summary of costs' line). The only 
        way we can avoid this is by modifying the file format so that it tells us how many slopes to process, or by using a list instead of an iterator.
        
        """
        rows = []
        row = reader.next()
        while (row[0] != "summary of costs:"):
            rows.append(row)
            row = reader.next()
        return rows
    
    def __get_cost_rows(self, reader, number_of_rows):
        """ Extract cost summary rows 
        
        Arguments:
        reader -- iterator of lists, expected to be at the position of the first cost row
        number_of_rows -- number of cost rows to read
        
        """
        return [reader.next() for i in range(number_of_rows)]
    
    def __get_cost_summary(self, reader, number_of_failed_slopes):
        """ Return dictionary containing summary of costs 
        
        Arguments:
        reader -- iterator of lists, expected to be at the position of the first cost row
        number_of_failed_slopes -- the number of failed slopes that will have generated cost rows
        
        Note: This method assumes absolute location of values in the list and doesn't check column names. This is because the line we need to figure these 
        things out has already been consumed by __get_slope_rows.
        
        """
        cost_rows = self.__get_cost_rows(reader, number_of_rows=number_of_failed_slopes)
        cost_summary = {}
        cost_summary['daily_landslide_probability'] = [float(row[1]) for row in cost_rows]
        cost_summary['cost'] = {'unit': 'EC$'} # TODO - fix hard coded value
        cost_summary['cost']['value'] = [float(row[2]) for row in cost_rows] # TODO - currency as float? bad idea...
        return cost_summary
    
    def __get_number_of_failed_slopes(self, slopes):
        """ Return number of failed slopes 
        
        Arguments:
        slopes -- array of slope dicts
        
        """
        return len([slope for slope in slopes if 'failure' in slope])
    
    def __advance_iterator(self, iterator, count):
        """ Advance supplied iterator 'count' times """
        for i in range(count):
            iterator.next()
    
    def __get_array_location(self, field_names, start_cell_name, cell_prefix, cell_spacing):
        """ Helper method for __get_slope_format
        
        Finds the start and end points for an array within the field_names list with provided start cell name, cell prefix and cell spacing
        
        Arguments:
        field_names -- list of field names
        start_cell_name -- name of the first cell in the array
        cell_prefix -- the prefix of the following array cells
        cell_spacing -- the spacing between the array cells (usually 2)
        
        """
        start = field_names.index(start_cell_name)
        search_from = start + cell_spacing
        
        iter_field_names = iter(field_names[search_from::cell_spacing]) # Start searching for end from next cell in array
        end = search_from
        pattern = re.compile(cell_prefix + r'\[[0-9]+\]')
        
        for field_name in iter_field_names:
            if not pattern.match(field_name):
                break
            else:
                end += cell_spacing
        
        return start, end
    
    def __get_slope_format_single_cell(self, field_names):
        """ Helper method for __get_slope_format - returns slope format data for single cell values
        
        Arguments:
        field_names -- list of field names
        
        This method uses the class variable slope_data_mapping which maps field names to full name, unit and type.
        
        """
        slope_format = {}
        for field_name, mapping in QuestaOutputSummary.slope_data_mapping.items():
            full_field_name = mapping['name']
            slope_format[full_field_name] = {
                'column': field_names.index(field_name),
                'type': mapping['type']
            }
            if 'unit' in mapping:
                slope_format[full_field_name]['unit'] = mapping['unit']
        return slope_format
    
    def __add_array_format_to_slope_format(self, field_names, array_id, array_mapping, slope_format):
        """ Helper method for __get_slope_format_array 
        
        The array definition will be one of a tuple of arrays that belong to a common parent dict. We therefore determine whether the parent dict exists
        before either creating it and adding the new array definition, or updating it with the new array definition.
        
        We produce the following dict:
        {   # This is the slope_format dict top level
            'parent': {
                {
                    'name1': {'start':value, 'end': value, 'cell_spacing': value},
                    'name2': {'start':value, 'end': value, 'cell_spacing': value}
                }
            }
        }
        
        Arguments:
        field_names -- list of field names from which definition is determined
        array_id -- name of the array whose definition is to be added to slope_format
        array_mapping -- the array that defines the mapping for the array
        slope_format -- the slope format dict to which the resulting array should be added
        
        Warning: This method mutates the supplied slope_format object. This is not the same pattern used everywhere else in this class.
                
        """
        start, end = self.__get_array_location(
                field_names, start_cell_name=array_id, cell_prefix=array_mapping['name'], cell_spacing=array_mapping['cell_spacing'])
        slope_format[array_mapping['parent']][array_mapping['name']] = {'start':start, 'end':end, 'cell_spacing': array_mapping['cell_spacing']}
        return  # We intentionally return nothing
    
    def __add_array_series_format_to_slope_format(self, field_names, array_mapping, slope_format):
        """ Helper method for __get_slope_format_array 
        
        Adds a definition of an array series to slope_format dict. Each array in the series will belong to a different parent dict object (where it 
        will be one of a tuple of arrays belonging to that dict).
        
        We produce the following dict:
        {   # This is the slope_format dict top level
            'parent': {
                {
                    'name1': {'start':[value0, value1, ...], 'end': [value0, value1, ...], 'cell_spacing': [value0, value1, ...]},
                    'name2': {'start':[value0, value1, ...], 'end': [value0, value1, ...], 'cell_spacing': [value0, value1, ...]}
                }
            }
        }
        
        So to find the array definition for name1 we do the following:
        - Find all array start cells by matching the array_mapping field 'common_regexp'
        - For each array start cell:
         - Find the start and end points
         - If there is no dict for this array in the parent object then create one
         - Append the start, end and cell spacing values to the arrays under the start, end and cell_spacing keys in the array dict
        
        Arguments:
        field_names -- list of field names from which definition is determined
        array_mapping -- the array that defines the mapping for the array
        slope_format -- the slope format dict to which the resulting array should be added
        
        Warning: This method mutates the supplied slope_format object. This is not the same pattern used everywhere else in this class.
        
        """
        pattern = re.compile(array_mapping['common_regexp'])
        start_cells = [cell for cell in field_names if pattern.match(cell)] # Get start cell name for each array in the series
        
        for cell_number in range(len(start_cells)):
            cell = start_cells[cell_number]
            start, end = self.__get_array_location(field_names, start_cell_name=cell, cell_prefix=array_mapping['name'],
                    cell_spacing=array_mapping['cell_spacing'])
                    
            if (not array_mapping['name'] in slope_format[array_mapping['parent']]):
                slope_format[array_mapping['parent']][array_mapping['name']] = defaultdict(list)
            
            slope_format[array_mapping['parent']][array_mapping['name']]['start'].append(start)
            slope_format[array_mapping['parent']][array_mapping['name']]['end'].append(end)
            slope_format[array_mapping['parent']][array_mapping['name']]['cell_spacing'].append(array_mapping['cell_spacing'])
        
        return  # We intentionally return nothing
    
    def __get_slope_format_array(self, field_names):
        """ Helper method for __get_slope_format - returns slope format data for array values 
        
        Arguments:
        field_names -- list of field names
        
        Each array is one of a tuple of arrays that belong to a parent object. We therefore need to check whether the parent object exists and create it if it
        doesn't.
        
        An array may be part of an array of arrays. In this case we need to find all other sub-arrays and get their parameters.
        
        """
        slope_format = defaultdict(dict)
        for array_id, array_mapping in QuestaOutputSummary.slope_data_arrays.items():
            if not 'common_regexp' in array_mapping:
                self.__add_array_format_to_slope_format(field_names, array_id, array_mapping, slope_format)
            else:
                self.__add_array_series_format_to_slope_format(field_names, array_mapping, slope_format)
        return slope_format
    
    def __get_slope_format(self, reader):
        """ Determine the format of the slope data from the field names 
        
        Parses the column names in order to determine the format of the slope data. The format data is returned in a dict with the following structure:
        
        {   # Single cell values
            'real_name': {              # <-- The name the slope value is referred to in the json representation
                'unit': 'UNIT',         # <-- UNIT is read from slope_data_mapping dict
                'column': column,       # <-- column is the index of the column containing this value
                'type': type            # <-- data type of the value
            },
            # Arrays
            'array_name': {             # <-- The name of this array (slope_data_arrays['parent'])
                'tuple_element_1': {    # <-- First element of the tuple
                    'start': start,     # <-- Start column
                    'end': end,         # <-- End column
                    'cell_spacing': step        # <-- Cell spacing
                },
                'tuple_element_2': {
                    'start': start,
                    'end': end,
                    'cell_spacing': step
                }
            },
            'array_name': {             # <-- Arrays can be part of a series of arrays that have a common parent
                'tuple_element_1': {    # <-- First element of the tuple
                    'start': [start0, start1, ...],  # <-- Start column of each array in series
                    'end': [end0, end1, ...],
                    'cell_spacing': [step0, step1, ...]
                },
                'tuple_element_2': {
                    'start': [start0, start1, ...],
                    'end': [end0, end1, ...],
                    'cell_spacing': [step0, step1, ...]
                }
            }
        }
        
        Note that the position of the value in the final output is not specified (i.e. what the parent object should be, etc). This is all hardcoded 
        in the __get_slope method.
        
        Arguments:
        reader -- iterator of lists, expected to be at the position of the slope field names
        
        """
        field_names = reader.next()
        slope_format = {}
        slope_format.update(self.__get_slope_format_single_cell(field_names))
        slope_format.update(self.__get_slope_format_array(field_names))
        return slope_format
    
    def __get_array(self, slope_data, array_format, array_number=None):
        """ Helper method for __get_slope
        
        Extracts array from slope_data given supplied array_format object
        
        Arguments:
        array_format -- format dict for this array, provides start, end and cell spacing information
        slope_data -- list of data from which the array is to be extracted
        
        """
        raw_array = []
        if array_number == None:
            raw_array = slope_data[array_format['start']:array_format['end']:array_format['cell_spacing']]
        else:
            raw_array = slope_data[array_format['start'][array_number]:array_format['end'][array_number]:array_format['cell_spacing'][array_number]]
        trimmed_array = [float_nan_as_string(element) for element in raw_array if element != '']
        if '' in raw_array and len(trimmed_array) < raw_array.index(''): # If there are any null strings we verify they were not within the array content
            raise ValueError('Unexpected value in array')
        return trimmed_array
    
    def __slope_has_failure(self, slope_format, slope_data):
        """ Helper method for __get_slope - returns True if slope data has failure
        
        Arguments:
        slope_format -- dict describing the format of the slope_data
        slope_data -- list of slope data values
        
        """
        try:
            return slope_data[slope_format['failure_time']['column']]
        except IndexError:
            return False
    
    def __get_value(self, slope_format_element, slope_data):
        """ Extracts value and unit specified by the slope_format_element from slope_data and casts to value_type 
        
        Empty strings become 0.
        
        Returns the value if no unit is specified. If a unit is specified a dict with 'unit' and 'value' keys is returned.
        
        Arguments:
        slope_format_element -- dict describing the format of this slope data element
        slope_data -- list of slope data values
        
        """
        string_value = slope_data[slope_format_element['column']]
        value_type = slope_format_element['type']
        if len(string_value) > 0:
            numeric_value = value_type(string_value)
        else:
            numeric_value = value_type(0)
        if 'unit' in slope_format_element:
            return {'unit': slope_format_element['unit'], 'value': numeric_value}
        else:
            return numeric_value
    
    def __get_slope_definition(self, slope_format, slope_data):
        """ Helper method for __get_slope - builds and returns a slope definition dict 
        
        Arguments:
        slope_format -- dict describing the format of the slope_data
        slope_data -- list of slope data values
        
        """
        slope_definition = {}
        
        for field in ['profile_height', 'profile_length', 'mesh_height', 'mesh_length', 'slope_area']:
            slope_definition[field] = self.__get_value(slope_format[field], slope_data)
        
        for array in ['slope', 'water']:
            slope_definition[array] = {}
            for name in slope_format[array]:
                slope_definition[array][name] = self.__get_array(slope_data, array_format=slope_format[array][name])
        
        slope_definition['soils'] = []
        for name, format in slope_format['soils'].items():
            for soil_number in range(len(format['start'])):
                soil_array = self.__get_array(slope_data, array_format=format, array_number=soil_number)
                if len(soil_array) > 0:                                             # Only add if we have some data
                    if soil_number >= len(slope_definition['soils']):               # If this soil number is not yet in the list, append the array
                        slope_definition['soils'].append({name: soil_array})
                    else:                                                           # Otherwise add to list element for this soil number
                        slope_definition['soils'][soil_number][name] = soil_array

        return slope_definition
    
    def __get_slope_investment(self, slope_format, slope_data):
        """ Helper method for __get_slope - returns slope investment dict
        
        Although this is a trivial operation it is in a seperate method to maintain a consistent level of abstraction in __get_slope
        
        Arguments:
        slope_format -- dict describing the format of the slope_data
        slope_data -- list of slope data values
        
        """
        return self.__get_value(slope_format['investment'], slope_data)
    
    def __get_slope_failure_slip_circle(self, slope_format, slope_data):
        """ Helper method for __get_slope_failure - returns a dict representing the slip circle 
        
        Arguments:
        slope_format -- dict describing the format of the slope_data
        slope_data -- list of slope data values
        
        """
        slip_circle = {}
        slip_circle['centre'] = {
            'x': self.__get_value(slope_format['centre_x'], slope_data),
            'y': self.__get_value(slope_format['centre_y'], slope_data)
        }
        slip_circle['crest'] = {
            'x': self.__get_value(slope_format['crest_x'], slope_data),
            'y': self.__get_value(slope_format['crest_y'], slope_data)
        }
        slip_circle['toe'] = {
            'x': self.__get_value(slope_format['toe_x'], slope_data),
            'y': self.__get_value(slope_format['toe_y'], slope_data)
        }
        slip_circle['radius'] = self.__get_value(slope_format['radius'], slope_data)
        return slip_circle
    
    def __get_slope_failure_intersection(self, slope_format, slope_data):
        """ Helper method for __get_slope_failure - returns dict representing the slope/slip-circle intersection 
        
        Arguments:
        slope_format -- dict describing the format of the slope_data
        slope_data -- list of slope data values
        
        """
        intersection = {}
        for name in slope_format['intersection']:
            intersection[name] = self.__get_array(slope_data, array_format=slope_format['intersection'][name])
        return intersection
    
    def __get_slope_failure_landslide(self, slope_format, slope_data):
        """ Helper method for __get_slope_failure - returns dict representing the landslide definition 
        
        Arguments:
        slope_format -- dict describing the format of the slope_data
        slope_data -- list of slope data values
        
        """
        landslide_definition = {}
        for approximation in ['finley', 'weighted_angle']:
            approximation_key = string.join((approximation, 'approximation'), '_')
            landslide_definition[approximation_key] = {}
            for parameter in ['landslide_height', 'landslide_depth', 'landslide_slope_angle', 
                    'cutslope_toe_to_runout_toe', 'runout_depth_at_cutslope_toe']:
                slope_format_parameter = string.join((parameter, approximation), '_')
                landslide_definition[approximation_key][parameter] =\
                        self.__get_value(slope_format[slope_format_parameter], slope_data)
        return landslide_definition
    
    def __get_slope_failure_cut_angles(self, slope_format, slope_data):
        """ Helper method for __get_slope_failure - returns dict representing the cut angles 

        Arguments:
        slope_format -- dict describing the format of the slope_data
        slope_data -- list of slope data values

        """
        cut_angles = {}
        cut_angles['upper'] = self.__get_value(slope_format['cut_angle_upper'], slope_data)
        cut_angles['lower'] = self.__get_value(slope_format['cut_angle_lower'], slope_data)
        return cut_angles

    def __get_slope_failure(self, slope_format, slope_data):
        """ Helper method for __get_slope - returns dict representing the slope failure 
        
        Arguments:
        slope_format -- dict describing the format of the slope_data
        slope_data -- list of slope data values
        
        Errors here are ignored as it is possible (and presumable legit) for slope failure data to have no values in questa output.

        """
        slope_failure = {}
        
        for field in ['failed_area', 'changed_area', 'failed_volume', 'cut_volume', 'clear_time', 'clear_cost', 'cut_time', 'cut_cost',
                'total_time', 'total_cost', 'factor_of_safety', 'number_of_design_storms', 'failure_type', 'failure_time']:
            try:
                slope_failure[field] = self.__get_value(slope_format[field], slope_data)
            except:
                pass

        try:
            slope_failure['cut_angles'] = self.__get_slope_failure_cut_angles(slope_format, slope_data)
        except:
            pass

        try:
            slope_failure['slip_circle'] = self.__get_slope_failure_slip_circle(slope_format, slope_data)
        except:
            pass

        try:
            slope_failure['intersection'] = self.__get_slope_failure_intersection(slope_format, slope_data)
        except:
            pass

        try:
            slope_failure['landslide_definition'] = self.__get_slope_failure_landslide(slope_format, slope_data)
        except:
            pass

        return slope_failure
    
    def __get_slope(self, slope_format, slope_data):
        """ Extracts the slope data from the supplied slope_data list according to the specified slope_format 
        
        Arguments:
        slope_format -- dict describing the format of the slope_data
        slope_data -- list of slope data values
        
        """
        slope = {}
        slope['investment'] = self.__get_slope_investment(slope_format, slope_data)
        slope['definition'] = self.__get_slope_definition(slope_format, slope_data)
        if self.__slope_has_failure(slope_format, slope_data):
            slope['failure'] = self.__get_slope_failure(slope_format, slope_data)
        return slope
    
    def __get_slopes(self, reader):
        """ Extract all slope instances from reader and return as an array 
        
        Arguments:
        reader -- iterator of lists, expected to be at the position of the first row of slope data
        
        """
        slope_format = self.__get_slope_format(reader)
        slope_rows = self.__get_slope_rows(reader)
        return [self.__get_slope(slope_format, slope_data=row) for row in slope_rows]
    
    def __get_nepv(self, reader):
        """ Extract net expected present value from reader "
        
        Arguments:
        reader -- iterator of lists, expected to be at the position of the NEPV row
        
        """
        return float(reader.next()[2])
    
    def parse(self):
        """ Parse output summary file 
        
        All the work is carried out by object methods. Each object assumes that the file iterator is at the correct location for reading the data. This method
        is responsible for advancing the iterator to the correct place.
         
        The alternative would be to have each method be responsible for advancing the iterator to the row needed by the following method but this would
        introduce coupling between parsing methods.
        
        With the approach below we have coupling between this method and the methods it calls, but no coupling between those called methods.
                
        Note: Might be better just to read in to a list and then pass out the correct line from here.
        
        """
        reader = csv.reader(open(self.filename), delimiter='\t')
        data = {}
        try:
            self.__advance_iterator(reader, 3)  # Advance to row required by __get_slopes
            data['slopes'] = self.__get_slopes(reader)
            number_of_failed_slopes = self.__get_number_of_failed_slopes(slopes=data['slopes'])
            if number_of_failed_slopes > 0:
                data['costs'] = self.__get_cost_summary(reader, number_of_failed_slopes)
                data['net_expected_present_value'] = self.__get_nepv(reader)
        except Exception, e:
            raise MossaicParserException(string.join(('Unable to parse questa output summary file:', str(e)))), None, sys.exc_info()[2]
        return data


class Results(MossaicParser):
    """ Parser for .results files generated by Chasm and Questa

    These files provide the soil type, moisture content and pore pressure of
    every slope cell of each time step of the simulation. The parsed data only
    stores values at every timestamp for moisture content and pore pressure as
    soil type does not change.

    Values for non-slope cells are thrown away if they run to the end of the
    mesh.

    """
    field_mapping = {
        '\'MOISTURE CONTENT m3/m3\'': {
            'key': 'moisture_content',
            'unit': 'm3/m3'
        },
        '\'PRESSURE HEAD    m\'': {
            'key': 'pressure_head',
            'unit': 'm'
        },
        '\'SOIL TYPE       \'': {
            'key': 'soil_type',
            'unit': None
        }
    }

    to_compress = ('moisture_content', 'pressure_head')

    def __init__(self, filename):
        """ Initialises the parser making filename an object variable """
        self.filename = filename
        self._strip_non_slope_cells = True

    def _strip_tail(self, row, value_to_strip):
        """ Remove all consecutive instances of a value from from the end

        Helper for the parse method. Starts at the end of the array and removes
        all instances of the supplied value until a non-matching value is found.

        """
        leftmost_index = None
        for i in range(len(row) - 1, 0, -1):
            if (row[i] == value_to_strip):
                leftmost_index = i
            else:
                break
        if leftmost_index:
            return row[0:leftmost_index]
        else:
            return row

    def _process_header(self, results, keys):
        """ Process the .results header """
        results.readline()
        number_of_vars = int(results.readline().rstrip())
        for i in range(number_of_vars):
            line = results.readline().rstrip()
            if line in Results.field_mapping:
                keys += [Results.field_mapping[line]]
            else:
                print line
                raise Exception('Could not find all variables')

    def _process_mesh(self, results):
        """ Read the mesh dimensions from the .results file """
        mesh_dims = results.readline().rstrip().split('\t')
        return {
            'rows': int(mesh_dims[0]),
            'cols': int(mesh_dims[1])
        }

    def _read_grid(self, results, mesh, transform=None):
        """ Read a grid of mesh values from the .results file """
        if not transform:
            transform = lambda x:x
        return [[transform(results.readline().rstrip())
                for i in range(mesh['rows'])]
                for i in range(mesh['cols'])]

    def compress(self, values):
        """ Compress the slope cells data using zlib

        Use zlib to compress the jsonified slope cells data and return the
        base64 encoded string of the compressed data.

        This yields a resulting data format that:

          1. is smaller, but more importantly:
          2. yields faster write times in CouchDB

        The data can then be uncompressed in a CouchDB view to provide user
        access to the original array. Obviously the trade-off here is view
        build times, which will now be longer.

        """
        cells_as_json = json.dumps(values)
        compressed = zlib.compress(cells_as_json)
        b64encoded = base64.b64encode(compressed)
        return b64encoded

    def parse(self, compress=True):
        """ Parse the .results file """
        results = file(self.filename, 'r')
        keys = []
        self._process_header(results, keys)
        mesh = self._process_mesh(results)
        coords_x = self._read_grid(results, mesh, transform=int)
        coords_y = self._read_grid(results, mesh, transform=int)
        slope_cells = self._read_grid(results, mesh,
                transform=lambda x: x == "-1")

        timestep = []
        data = {}
        for item in keys:
            data[item['key']] = {
                'value': []
            }
            if (item['unit']):
                data[item['key']]['unit'] = item['unit']

        while True:
            line = results.readline()
            if (line == ''):
                break;
            else:
                tokens = line.rstrip().split()
                value = tokens[0]
                field = ' '.join(tokens[1:])
                if field != 'time step':
                    print value, field
                    raise Exception('Expected time step but got %s' % field)
                else:
                    timestep.append(value)
                    for item in keys:
                        transform = float
                        if item['key'] == 'soil_type':
                            transform = int
                        values = self._read_grid(results, mesh,
                                transform=transform)
                        if self._strip_non_slope_cells:
                            values = map(
                                    lambda x:self._strip_tail(
                                            x, transform('9999')),
                                    values)
                        data[item['key']]['value'].append(values)

        data['soil_type']['value'] = data['soil_type']['value'][0]

        if compress:
            for field in (Results.to_compress):
                if field in data:
                    data[field]['value'] = self.compress(data[field]['value'])

        return data


# Mapping of file types to MossaicParser subclasses
filetype_to_parser = {
    'questa_output_summary': QuestaOutputSummary,
    'factor_of_safety': FactorOfSafety,
    'results': Results
}
