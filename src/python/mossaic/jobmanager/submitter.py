""" submitter.py

Job submitter.

This will ultimately be capable of submitting jobs to a number of compute backends.

Initially we only support the local machine.

"""

import logging
import os
import random
import shutil
import subprocess
import tempfile

from ConfigParser import SafeConfigParser
from multiprocessing import Process
from collections import namedtuple

from mossaic import jobwrapper
from mossaic.jobmanager import proxy_factory

TEST = False

class SubmitterResponse(object):
    OK = 0
    ERROR_JOB = 1
    ERROR_BACKEND = 2
    ERROR_UNKNOWN = 3


def _get_config():
    """ Read the configuration file either from $MOSSAIC/etc or from local directory

    """
    filename = 'submitter.ini'
    if not os.path.exists(os.path.join(os.getcwd(), filename)):
        mossaic_path = os.getenv('MOSSAIC')
        if mossaic_path:
            filename = os.path.join(mossaic_path, 'etc', filename)
        else:
            # An envvar isn't set and there's no local copy of the file so use the one from config
            filename = os.path.normpath(os.path.join(__file__, '../../../../config/%s' % filename))
    config = SafeConfigParser()
    if os.path.exists(filename):
        config.read(filename)
        return config.get('submitter', 'url'), config.get('submitter', 'data_db'), config.get('submitter', 'release_db'),\
                config.get('submitter', 'task_db'), config.get('submitter', 'state_db'), config.get('submitter', 'type'),\
                config.get('submitter', 'owner'), config.getint('submitter', 'jobwrapper_http_retries'),\
                config.get('submitter', 'jobwrapper_auth_file'), config.getint('submitter', 'initial_backoff_window')
    else:
        print "Could not find config file %s" % filename
        return None, None, None, None, None, None, None, None, None, None

_url, _data_db, _release_db, _task_db, _state_db, _type, _owner, _http_retries, _auth_file, _jw_initial_backoff = _get_config()

class Submitter():
    def __init__(self, logger=None):
        if _url.startswith('https'):
            self._use_ssl = True
        else:
            self._use_ssl = False
        self.logger = logger
        if not self.logger:
            logging.basicConfig(level=logging.WARNING)
            self.logger = logging.getLogger('mossaic job submitter')
        self.mossaic_path = os.getenv('MOSSAIC')
        if self.mossaic_path:
            site_packages = os.path.join(self.mossaic_path, 'lib', 'python2.7', 'site-packages')
            mossaic_package = os.path.join(site_packages, 'mossaic')
            self._jobwrapper_filename = os.path.join(mossaic_package, 'jobwrapper.py')
            self._mossaicparser_filename = os.path.join(mossaic_package, 'mossaicparser.py')
            self._couch_filename = os.path.join(site_packages, 'couch', 'Couch.py')
            self._couch_request_filename = os.path.join(site_packages, 'couch', 'CouchRequest.py')
            self._httplib2_filename = os.path.join(site_packages, 'httplib2', '__init__.py')
            self._iri2uri_filename = os.path.join(site_packages, 'httplib2', 'iri2uri.py')
            self._start_job_filename = os.path.join(self.mossaic_path, 'scripts', 'start_job.sh')
            self._version_filename = os.path.join(mossaic_package, 'version.py')
            self._simplejson_init_filename = os.path.join(site_packages, 'simplejson', '__init__.simplejson.py')
            self._simplejson_decoder_filename = os.path.join(site_packages, 'simplejson', 'decoder.py')
            self._simplejson_encoder_filename = os.path.join(site_packages, 'simplejson', 'encoder.py')
            self._simplejson_ordered_dict_filename = os.path.join(site_packages, 'simplejson', 'ordered_dict.py')
            self._simplejson_scanner_filename = os.path.join(site_packages, 'simplejson', 'scanner.py')
            self._simplejson_tool_filename = os.path.join(site_packages, 'simplejson', 'tool.py')
            self._jobwrapper_auth_filename = os.path.join(self.mossaic_path, 'etc', 'jobwrapper_auth')
        else:
            self._jobwrapper_filename = os.path.join('..', '..', '..', 'src', 'python', 'mossaic', 'jobwrapper.py')
            self._mossaicparser_filename = os.path.join('..', '..', '..', 'src', 'python', 'mossaic', 'mossaicparser.py')
            self._couch_filename = os.path.join('..', '..', '..', 'src', 'python', 'couch', 'Couch.py')
            self._couch_request_filename = os.path.join('..', '..', '..', 'src', 'python', 'couch', 'CouchRequest.py')
            self._httplib2_filename = os.path.join('..', '..', 'grid_test_helpers', 'httplib2', '__init__.py')
            self._iri2uri_filename = os.path.join('..', '..', 'grid_test_helpers', 'httplib2', 'iri2uri.py')
            self._start_job_filename = os.path.join('..', '..', '..', 'src', 'python', 'mossaic', 'jobmanager', 'scripts', 'start_job.sh')
            self._version_filename = os.path.join('..', '..', '..', 'src', 'python', 'mossaic', 'version.py')
            self._simplejson_init_filename = os.path.join('..', '..', '..', 'src', 'python', 'simplejson', '__init__.simplejson.py')
            self._simplejson_decoder_filename = os.path.join('..', '..', '..', 'src', 'python', 'simplejson', 'decoder.py')
            self._simplejson_encoder_filename = os.path.join('..', '..', '..', 'src', 'python', 'simplejson', 'encoder.py')
            self._simplejson_ordered_dict_filename = os.path.join('..', '..', '..', 'src', 'python', 'simplejson', 'ordered_dict.py')
            self._simplejson_scanner_filename = os.path.join('..', '..', '..', 'src', 'python', 'simplejson', 'scanner.py')
            self._simplejson_tool_filename = os.path.join('..', '..', '..', 'src', 'python', 'simplejson', 'tool.py')
            self._jobwrapper_auth_filename = os.path.join('..', '..', '..', 'config', 'jobwrapper_auth')

    def _get_jobwrapper_proxy(self):
        """ Create a proxy signed by the jobwrapper private key

        This lets the jobwarpper authenticate from worker nodes via SSL using the proxy, which will expire after a given amount of time.

        """
        if not self._use_ssl:
            raise Exception('Called _make_jobwrapper_proxy but submitter not using SSL')
        else:
            return proxy_factory.get_proxy()

    def submit(self, job_id):
        """ Submit the job """
        pass

class SubmitterLocal(Submitter):
    """ Local submitter

    We call the job wrapper via python rather than the shell as we can do this in test and production without messing about with paths

    """
    def __init__(self, logger=None, backend=None):
        Submitter.__init__(self, logger=logger)
        if TEST and backend:
            self.Backend = backend
        else:
            self.Backend = Process

    def __submit_and_track(self, job):
        job()

    def submit(self, job_id):
        """ Run the job on the local machine """
        class Opts:
            pass
        opts = Opts()
        opts.job_id = job_id
        opts.couch_url = _url
        opts.auth_file = _auth_file
        if self._use_ssl:
            proxy = self._get_jobwrapper_proxy()
            opts.ssl_key = proxy['key']
            opts.ssl_cert = proxy['cert']
        opts.verbose = True
        opts.taskdb = _task_db
        opts.datadb = _data_db
        opts.releasedb = _release_db
        opts.statedb = _state_db
        logging.basicConfig(level=logging.WARNING)
        opts.logger = self.logger
        opts.keep = False
        opts.noop = False
        opts.debug = False
        opts.http_retries = _http_retries
        opts.initial_backoff = _jw_initial_backoff
        try:
            job = jobwrapper.JobWrapper(opts)
        except (jobwrapper.JobWrapperError, ValueError), error:
            self.logger.error('Could not create job from supplied options: %s' % (str(error)))
            return SubmitterResponse.ERROR_JOB
        try:
            job_process = self.Backend(target=self.__submit_and_track, args=[job])
            job_process.start()
        except Exception, error:   # We do actually want to catch all exceptions here
            self.logger.error('Could not submit to local compute backend: %s' % (str(error)))
            return SubmitterResponse.ERROR_BACKEND
        if TEST:
            self.job = job
        return SubmitterResponse.OK


class SubmitterDirac(Submitter):
    """ DIRAC submitter

    The DIRAC job submitter assumes the following:
      We have a valid DIRAC proxy and DIRAC client
      The correct environment is set up on the target worker nodes

    Needs to make API calls equivalent to the following JDL:
      JobName = "landslides_job_id";
      Executable = "start_job.sh";
      InputSandbox = {"start_job.sh", "jobwrapper.py"};

    """
    def __init__(self, logger=None, backend=None):
        Submitter.__init__(self, logger=logger)
        if TEST and backend:
            self.dirac = backend.Dirac()
            self.Job = backend.Job
        else:
            from DIRAC.Interfaces.API.Dirac import Dirac
            from DIRAC.Interfaces.API.Job import Job
            self.dirac = Dirac()
            self.Job = Job

    def __submit_job(self, job):
        """ Actually submit the job """
        if TEST:
            mode = 'local'
        else:
            mode = 'wms'
        return self.dirac.submit(job, mode=mode)

    def submit(self, job_id):
        """ Submit job to DIRAC backend """
        try:
            job = self.Job()
        except Exception, error:   # We do want to catch all exceptions here
            self.logger.error('Could not submit to DIRAC compute backend: %s' % (str(error)))
            return SubmitterResponse.ERROR_BACKEND
        results = []
        results.append(job.setName(str(job_id)))    # Convert to string as it may be unicode, which will upset DIRAC
        results.append(job.setOwner(_owner))        # This perhaps should be whoever requested the original task - need some refactoring to do this <-- TODO
        args = str('--job=%s --url=%s --data-db=%s --task-db=%s --release-db=%s --state-db=%s --http-retries=%s --auth-file=%s --initial-backoff-window=%s' %
                                (job_id, _url, _data_db, _task_db, _release_db, _state_db, _http_retries, _auth_file, _jw_initial_backoff))
        sandbox = [self._start_job_filename, self._jobwrapper_filename, self._mossaicparser_filename, self._couch_filename, self._couch_request_filename,
                    self._httplib2_filename, self._iri2uri_filename, self._version_filename, self._jobwrapper_auth_filename]
        sandbox.extend([self._simplejson_init_filename, self._simplejson_decoder_filename, self._simplejson_encoder_filename,
                    self._simplejson_ordered_dict_filename, self._simplejson_scanner_filename, self._simplejson_tool_filename])
        if self._use_ssl:
            proxy = self._get_jobwrapper_proxy()
            args = ' '.join((args, str('--ssl-key=%s --ssl-cert=%s' % (os.path.split(proxy['key'])[-1], os.path.split(proxy['cert'])[-1]))))
            sandbox.extend([proxy['key'], proxy['cert']])
        results.append(job.setInputSandbox(sandbox))
        results.append(job.setExecutable(os.path.split(self._start_job_filename)[-1], args))
        bad_results = [result for result in results if not result['OK']]
        if len(bad_results) > 0:
            self.logger.error('Could not create job from supplied options, args: %s, %s' % (bad_results[0]['Message'], args))
            return SubmitterResponse.ERROR_JOB
        if TEST:
            self.job = job
        result = self.__submit_job(job)
        if result['OK']:
            return SubmitterResponse.OK
        else:
            self.logger.error('Could not submit to DIRAC compute backend: %s' % result['Message'])
            return SubmitterResponse.ERROR_BACKEND


_submitters = {
    'local': SubmitterLocal,
    'dirac': SubmitterDirac
}

def get_submitter(kwargs={}):
    return _submitters[_type](**kwargs)