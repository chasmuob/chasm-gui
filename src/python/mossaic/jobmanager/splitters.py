""" splitters.py

Job splitters which can be plugged into the Task plugin.

"""

import copy
import os
import random

from collections import defaultdict

class Splitter(object):
    def __call__(self, work):
        raise NotImplementedError('Implement splitter here, fool')


class BasicSplitter(object):
    def __calculate_jobs_for_parameters(self, parameters):
        """ Calculate total number of jobs for this set of parameters """
        if 'value' in parameters:
            return len(parameters['value'])
        elif 'range' in parameters and 'increment' in parameters:
            return len(range(parameters['range'][0], parameters['range'][1] + 1, parameters['increment']))
        else:
            raise ValueError(parameters)

    def calculate_total_jobs(self, task):
        """ Calculate how many jobs this task will create """
        if 'simulation_parameters' not in task:
            return 1
        deterministic_jobs = 1
        if 'deterministic' in task['simulation_parameters'] and len(task['simulation_parameters']['deterministic']) > 0:
            jobs_per_parameter = [self.__calculate_jobs_for_parameters(parameters) for parameters in task['simulation_parameters']['deterministic'].values()]
            deterministic_jobs = reduce(lambda x, y: x * y, jobs_per_parameter)
        number_of_runs = 1
        if 'number_of_jobs' in task['simulation_parameters']:
            number_of_runs = task['simulation_parameters']['number_of_jobs']
        return number_of_runs * deterministic_jobs

    def __postprocess_job(self, job):
        """ Carry out application-specific postprocessing for each job """
        if job['executable']['application'] == 'questa':
            # Create a copy so we don't modify the data in the task request
            input_files = job['input_files']
            job['input_files'] = {}
            for file_type, filename in input_files.items():
                job['input_files'][file_type] = filename
            job['input_files']['initial_slope'] = job['input_files']['geometry']
            job['input_files'].pop('geometry')

    def __get_basic_job(self, work):
        """ Get a simple job from a task """
        job = {}
        ignore_these = ['_id', '_rev', 'parent_id', 'timestamp', 'simulation_parameters']
        rename_these = {'name': 'task_name'}
        for k,v in work.items():
            if k not in ignore_these:
                if k in rename_these:
                    job[rename_these[k]] = v
                else:
                    job[k] = v
        job['task_id'] = work['_id']
        job['type'] = 'job'
        self.__postprocess_job(job)
        return job

    def __update_parameters(self, job, parameter_type, pointer, doc_type, value):
        if 'simulation_parameters' not in job:  # This kinda sucks - must be a better way
            job['simulation_parameters'] = {}
        if parameter_type not in job['simulation_parameters']:
            job['simulation_parameters'][parameter_type] = {}
        job['simulation_parameters'][parameter_type][pointer] = {
            'type': doc_type,
            'value': value
        }
        return job

    def __quasi_deepcopy(self, source, key):
        """ Return a shallow copy of the supplied dict, with a deepcopy of the top level field specified by key """
        new_dict = {}
        for k, v in source.items():
            if k == key:
                new_dict[k] = copy.deepcopy(v)  # Only deepcopy when we have to
            else:
                new_dict[k] = v
        return new_dict

    def __split_deterministic_job(self, job, pointer, parameters):
        """ Perform job split with deterministic parameters on a single job """
        new_jobs = []
        for value in parameters['value']:
            new_job = self.__quasi_deepcopy(job, 'simulation_parameters')
            new_jobs.append(self.__update_parameters(new_job, 'deterministic', pointer, parameters['type'], value))
        return new_jobs

    def __split_deterministic(self, work, jobs_for_task):
        """ Perform job split with deterministic parameters on all supplied jobs

        We assume the parameters specified in work['simulation_parameters']['deterministic'] are to be applied to each upstream job in the split.

        """
        new_jobs = jobs_for_task
        for pointer, parameters in sorted(work['simulation_parameters']['deterministic'].items()):
            if 'range' in parameters and 'increment' in parameters:
                parameters['value'] = range(parameters['range'][0], parameters['range'][1] + 1, parameters['increment'])
            new_jobs = [self.__split_deterministic_job(job, pointer, parameters) for job in new_jobs]
            new_jobs = [job for sublist in new_jobs for job in sublist]    # Flatten new_jobs
        return new_jobs

    def __split_stochastic(self, work, jobs_for_task):
        """ Perform job split with stochastic parameters

        We assume every job resulting from upstream splits is to be split into number_of_jobs.

        """
        seed = hash(os.urandom(255))
        random.seed(seed)
        work['simulation_parameters']['seed'] = seed
        number_of_jobs = work['simulation_parameters']['number_of_jobs']
        new_jobs = [self.__quasi_deepcopy(job, 'simulation_parameters') for job in jobs_for_task for i in range(number_of_jobs)]
        for pointer, parameters in sorted(work['simulation_parameters']['stochastic'].items()):
            for job in new_jobs:
                if (type(parameters['mean']) == list):
                    value = [random.gauss(parameters['mean'][i], parameters['standard_deviation'][i]) for i in range(len(parameters['mean']))]
                else:
                    value = random.gauss(parameters['mean'], parameters['standard_deviation'])
                self.__update_parameters(job, 'stochastic', pointer, parameters['type'], value)
        return new_jobs

    def __name_jobs(self, jobs_for_task):
        for index in range(0, len(jobs_for_task)):
            jobs_for_task[index]['name'] = str(index)
        return jobs_for_task

    def __call__(self, work):
        """ Create jobs for a given task defined by work, return one or more job documents """
        jobs_for_task = [self.__get_basic_job(work)]
        if 'simulation_parameters' in work:
            if 'rainfall' in work['simulation_parameters']:
                raise NotImplementedError(
                        'Simulation parameters currently not supported - please only submit simple, deterministic or stochastic task requests for now')
            if 'stochastic' in work['simulation_parameters']:
                jobs_for_task = self.__split_stochastic(work, jobs_for_task)
            if 'deterministic' in work['simulation_parameters']:
                jobs_for_task = self.__split_deterministic(work, jobs_for_task)
        return self.__name_jobs(jobs_for_task)


def get_splitter(splitter_type):
    return defaultdict(lambda: BasicSplitter, {
        'basic': BasicSplitter
    })[splitter_type]()