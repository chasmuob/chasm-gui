"""
MoSSaiC Job Manager
===================

Daemons which manage tasks and jobs.

Available modules
-----------------

master
    Master daemon, reads configuration files and spawns appropriate sentinels
plugins
    Plugins define the actions performed by a sentinel
sentinel
    Generic sentinel process which is configured to call a specific plugin
submitter
    Job submitter, reads a configuration file, submits to appropriate backend

"""