"""

Work sentinel

A work sentinel manages a thread pool. It is given a plugin which it uses to
poll for work, which is then dispatched to the pool and processed. It then
sleeps for some duration and repeats the process.
"""

from threading import Thread
from Queue import Queue, Full, Empty
import os
import datetime
import time
import sys
import logging

from couch.Couch import CouchServer
from couch.CouchRequest import CouchError
from mossaic.jobmanager.plugins import Task, Job, Input, Tracker

function_dict = {}
function_dict['task'] = Task
function_dict['job'] = Job
function_dict['input'] = Input
function_dict['tracker'] = Tracker

def _posix_time_to_date_tuple(timestamp):
    """ Convert a posix timestamp to a date tuple like: [YYYY, MM, DD, HH, MM, SS]"""
    full_date = datetime.datetime.fromtimestamp(timestamp)
    return (full_date.year, full_date.month, full_date.day, full_date.hour, full_date.minute, full_date.second)

class Sentinel:
    def __init__(self, url, task_db_name, state_db_name, plugin, logger, workers=5, max_work=10000, cycle=60, quorum=None,
                    data_db_name=None, retries=None, options={'job_limit': None}):
        """
        Initialise the sentinel and it's workers
        """
        self.remote_function = {
            'status': self.pipe_status,
            'stop': self.stop
        }
        if retries != None:
            self.retries = retries
        else:
            self.retries = 0
        self.logger = logger
        self.logger.name = 'Mossaic %s sentinel' % plugin
        self.cycle = cycle
        self.plugin_type = plugin
        couch = CouchServer(url)
        try:
            self.task_db = self.__connect_and_retry(couch, task_db_name, retries=retries)
            self.state_db = self.__connect_and_retry(couch, state_db_name, retries=retries)
            if data_db_name:
                self.data_db = self.__connect_and_retry(couch, data_db_name, retries=retries)
            else:
                self.data_db = None
        except Exception, e:
            self.logger.error('Could not connect to CouchDB server at: %s. Exception: %s' % (url, e))
            return
        self.queue = Queue(max_work) #TODO: check how this behaves when over filled
        self.workers = []
        for i in range(workers):
            #Spawn and record worker threads
            worker = Worker(i, self.logger)
            worker.queue = self.queue
            # Create db connections for each worker - sharing a single connection is not threadsafe
            worker_couch = CouchServer(url)
            task_db = self.__connect_and_retry(worker_couch, task_db_name, retries=retries)
            state_db = self.__connect_and_retry(worker_couch, state_db_name, retries=retries)
            if data_db_name:
                data_db = self.__connect_and_retry(worker_couch, data_db_name, retries=retries)
            else:
                data_db = None
            worker.plugin = function_dict[self.plugin_type](task_db, state_db, logging.getLogger('%s plugin-%s' % (self.plugin_type, i)),
                                                                                                            data_db, quorum, retries, options)
            worker.daemon = True
            worker.start()
            self.workers.append(worker)
            self.logger.debug('Initialised worker %s' % i)
        self.logger.debug('Initialised')
        self._flag = False
        self._last_ran = 0
        self._fail_count = 0

    def __connect_and_retry(self, couchserver, dbname, retries=3):
        """ Helper method which connects to couchserver and retries on any exception """
        for i in range(0, retries):
            try:
                return couchserver.connectDatabase(dbname)
            except Exception:
                pass
        return couchserver.connectDatabase(dbname)

    def start(self, pipe=None):
        """
        Set our flag to true and get working
        """
        self.pipe = pipe
        self._flag = True
        self.run()

    def stop(self):
        """
        Put a None into the queue for each worker, and let them finish
        """
        for i in range(len(self.workers)):
            self.queue.put(None)
        for worker in self.workers:
            worker.join()
            self.logger.debug('%s is done' % worker.name)
        self._flag = False

    def status(self):
        """
        Return some simple status information about the sentinel
        """
        return {'queue_size': len(self.queue.queue),
                'fail_count': self._fail_count,
                'plugin': self.plugin_type}

    def pipe_status(self):
        """
        Print some simple status information
        """
        self.pipe.send(self.status())

    def run(self):
        """
        The sentinel algoritm:
            Poll CouchDB view for work
            Queue work (worker threads handle the work asynchronously)
            Sleep
        """
        while self._flag:
            self.logger.debug('%s Sentinel looking for work ' % self.plugin_type.title())
            duration = 0
            start = int(time.time())
            work = []
            try:
                # If the sentinel cannot contact couch then it should sleep and
                # retry later. Failures are counted and we stop if a threshold
                # is exceeded.
                self.queue.join()   # Wait until queued tasks are completed before continuing to prevents an issue where the same work can get pushed onto the
                                    # queue again if the worker thread has not yet updated its status. Drawback is whilst we block here we cannot check
                                    # the pipe for start/stop/status requests.
                work = function_dict[self.plugin_type].get_work({'task': self.task_db, 'state': self.state_db, 'data': self.data_db})
                self._last_ran = int(time.time())
                # Reset the fail counter
                self._fail_count = 0
            except CouchError, e:
                # TODO: better handling of fatal cases?
                self._fail_count += 1
                msg = "Could not get work, got %s:%s (fail count is %s)"
                self.logger.warning(msg % (type(e), str(e), self._fail_count))
            except Exception, e:
                self._fail_count += 1
                msg = "Could not get work, got %s:%s (fail count is %s)"
                self.logger.exception(msg % (type(e), str(e), self._fail_count))
            finally:
                if self._fail_count > self.retries: #TODO: make this a configurable?
                    self.logger.critical("I've seen too much horror, I'm out of here")
                    self._flag = False
            for work_item in work:
                if isinstance(work_item, dict):
                    self.logger.debug('Queueing %s' % work_item['id'])
                    if 'doc' in work_item and work_item['doc'] != None: # include_docs can sometimes return 'doc': None, so check for this
                        self.queue.put(work_item['doc'], False)
                    else:
                        self.logger.warning('Ignoring %s for now - no doc' % str(work_item['id']))
                else:
                    self.logger.debug('Queueing %s' % str(work_item))
                    self.queue.put(work_item, False)
            duration = int(time.time()) - start
            sleep_time = self.cycle - duration
            if sleep_time > 0 and self._flag:
                self.logger.debug('Sleeping for %s seconds' % sleep_time)
                # TODO: I'm not sure a call to sleep is actually what's needed
                # here, since it's blocking...
                start = time.time()
                while time.time() - start < sleep_time and self._flag:
                    self.__check_pipe() # If we were using Queues we could do a blocking read here, BUT process Queue = more complexity
                    time.sleep(1)
            else:
                self.__check_pipe()

    def __check_pipe(self):
        """ Poll the pipe """
        if self.pipe:
            while self.pipe.poll():
                self.remote_function[self.pipe.recv()]()


class Worker(Thread):
    def __init__(self, name, logger):
        Thread.__init__(self, name='Worker-%s' % name)
        self.queue = None
        self.plugin = None
        self.name = name
        self.logger = logger

    def __process_work(self, work):
        """ Process an item of work - return True once task is completed, return False if task is a None """
        self.logger.debug("Processing " + str(work))
        if work == None:
            return False
        else:
            self.plugin(work)
            self.queue.task_done()
            return True

    def __consume_queue(self):
        """ Consume unprocessed items on the queue """
        while not self.queue.empty():
            work = self.queue.get(block=False)
            self.__process_work(work)

    def run(self):
        """
        Process the queued work using the plugin, writing new jobs into CouchDB
        """
        if self.queue and self.plugin:
            try:
                while True:
                    task = self.queue.get(block=True)
                    if not self.__process_work(task):
                        return
                    self.__consume_queue()
            except Exception, e:
                raise e, None, sys.exc_info()[2] # TODO
        else:
            print 'Whole lotta fail just happened'
            return



if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG,
                        format="%(levelname)s [%(asctime)s] %(name)s: %(message)s",
                        datefmt='%d/%m/%Y %H:%M:%S')
    logger = logging.getLogger('Mossaic work sentinel')
    sentinel = Sentinel('http://localhost:5984',
                        'mossaic', 'state_machine', 'job', logger, cycle=10)
    sentinel.start()