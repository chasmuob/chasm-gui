#!/usr/bin/env python
# encoding: utf-8
"""
Define plugins to process work of a given type (task or job). Plugins take a
single argument that represents the piece of work they should process.

The documents return a list of couch documents to be committed back to couch.
"""

import sys
import time

from collections import defaultdict

from couch.Couch import Document
from mossaic import mossaicparser
from mossaic.jobmanager.splitters import BasicSplitter, get_splitter

def _delete(db, doc, logger, w=None):
    """ Delete a document we already have """
    doc_to_delete = Document(dict=doc)
    doc_to_delete.delete()
    logger.debug('Deleting doc with id %s' % doc['_id'])
    db.ecCommitOne(doc_to_delete, ['_deleted'], w=w)

def _stochastify(data, work, type, make_json_pointer):
    for i in range(len(data)):
        for parameter, value in data[i].items():
            if isinstance(value, dict) and 'mean' in value and 'standard_deviation' in value:
                if 'simulation_parameters' not in work:
                    work['simulation_parameters'] = {}
                if 'stochastic' not in work['simulation_parameters']:
                    work['simulation_parameters']['stochastic'] = {}
                work['simulation_parameters']['stochastic'][make_json_pointer(i, parameter)] = {
                    'type': type,
                    'mean': value['mean'],
                    'standard_deviation': value['standard_deviation']
                }

class Plugin():
    def __init__(self, task_db, state_db, logger, data_db=None, quorum=None, retries=None, options={}):
        """ Initialise plugin with config values

        Arguments:
         quorum - dict of quorum values (q, r, n, w) used by persistence layer

        """
        self.task_db = task_db
        self.state_db = state_db
        self.data_db = data_db
        self.logger = logger
        self.quorum = defaultdict(lambda: None)
        if quorum:
            self.quorum.update(quorum)
        self.retries = retries

    def __call__(self, work):
        raise NotImplementedError('Implement plugin here, fool')

class Input(Plugin):
    """ Turns raw input data into a structured JSON document

    This functionality will be replaced with a GUI based on Jon Wakelin's prototype - this will be the standard way for users to enter input data. The
    upload of raw input files and parsing by this sentinel is a temporary measure that should be dropped by the time we hit 1.0.

    """
    parser_dict = {
        'geometry': mossaicparser.Geometry,
        'stability': mossaicparser.Stability,
        'boundary_conditions': mossaicparser.BoundaryConditions,
        'soils': mossaicparser.Soils,
        'engineering_cost': mossaicparser.EngineeringCost
    }

    def __call__(self, work):
        self.logger.debug('Converting raw input with id %s' % work['_id'])
        if '_attachments' in work:
            try:
                raw_input = self.data_db.getAttachment(work['_id'], name=work['_attachments'].keys()[0])
            except Exception, e:
                self.logger.error('Could not download attachment for input %s' %
                        (work['_id']))
                self.__handle_error(work, str(e))
                return
            jsonified = {}
            try:
                parser = Input.parser_dict[work['destination_type']](data=raw_input)
                jsonified = parser.parse()
            except Exception, e:
                # If this failed then we have a duff file - we need to mark the original document as failed and exit gracefully so the queue is notified
                self.logger.error('Could not parse attachment for input %s' % (work['_id']))
                self.__handle_error(work, str(e))
                return
            jsonified['name'] = work['name']
            if 'destination_id' in work:
                jsonified['_id'] = work['destination_id']
            else:
                message = ('Could not get destination _id for input %s' %
                        (work['_id']))
                self.logger.error(message)
                self.__handle_error(work, message)
                return
            if 'parent_id' in work:
                jsonified['parent_id'] = work['parent_id']
            self.data_db.ecCommitOne(jsonified, w=self.quorum['w'])   # Don't use W=N here, we doni't need it (inconsistency canc cause no harm here)
            work['converted'] = True
            self.data_db.ecCommitOne(work, w=self.quorum['n'])
        else:
            self.logger.debug('No raw input found for input with id %s' % work['_id'])
            self.__handle_soft_error(work)

    def __handle_soft_error(self, work):
        """ Retry until self.retries reached """
        if 'fail_count' in work:
            work['fail_count'] += 1
        else:
            work['fail_count'] = 1
        if work['fail_count'] > self.retries:
            self.__handle_error(work, 'Max retries reached')
        else:
            # If we are here because getAttachment failed, the document may have been modified and so we need to handle a conflict error
            self.data_db.ecCommitOne(work, ['fail_count'], r=self.quorum['n'],
                                                            w=self.quorum['n'])

    def __handle_error(self, work, message):
        """ Mark input as failed """
        work['failed'] = message
        work['type'] = 'failed_input'
        self.data_db.ecCommitOne(work, ['fail_count', 'failed', 'type'],
                                        r=self.quorum['n'], w=self.quorum['n'])

    @classmethod
    def get_work(cls, dbs):
        """ Get an item of work to be processed by the Input sentinel """
        options = {
            'include_docs': True,
            'reduce': False
        }
        return dbs['data'].loadView('mossaic', 'raw_input', options)['rows']


class Job(Plugin):
    """ Submits a job from a given state document """
    def __init__(self, task_db, state_db, logger, data_db=None, quorum=None, retries=None, options={'job_limit': 1000}):
        Plugin.__init__(self, task_db, state_db, logger, data_db, quorum, retries)
        Job.job_limit = options['job_limit']    # We set a class variable because get_work is called via a class method

    def __call__(self, work, submitter=None):
        if not submitter:
            from submitter import get_submitter, SubmitterResponse # Only import the submitter module if a job sentinel is called
        else:
            get_submitter = submitter.get_submitter
            SubmitterResponse = submitter.SubmitterResponse
        job_submitter = get_submitter({'logger': self.logger})
        result = job_submitter.submit(work['job_id'])
        self.logger.debug('%s' % (result))
        if result == SubmitterResponse.OK:
            self.__handle_submit_success(work)
        else:
            if result == SubmitterResponse.ERROR_JOB or result == SubmitterResponse.ERROR_UNKNOWN:
                self.__handle_hard_error(work)
            elif result == SubmitterResponse.ERROR_BACKEND:
                self.__handle_soft_error(work)

    def __handle_hard_error(self, work):
        """ Mark job as failed """
        self.logger.debug('Rejected job with id %s' % work['job_id'])
        state_doc = work
        state_doc['state'] = 'failed_submit'
        state_doc['state_history']['failed_submit'] = {'modified_by': self.__class__.__name__, 'timestamp': int(time.time())}
        self.state_db.ecCommitOne(state_doc, timestamp=True, viewlist=['state-machine/jobs_by_state'], w=self.quorum['n'])

    def __handle_soft_error(self, work):
        """ Apply retry strategy """
        state_doc = work
        if 'submit_fail_count' in state_doc:
            state_doc['submit_fail_count'] += 1
        else:
            state_doc['submit_fail_count'] = 1
        if state_doc['submit_fail_count'] > self.retries:
            self.__handle_hard_error(state_doc)
        else:
            self.logger.debug('Re-queued job with id %s' % work['job_id'])
            self.state_db.ecCommitOne(state_doc, timestamp=True, viewlist=['state-machine/jobs_by_state'], w=self.quorum['n'])

    def __handle_submit_success(self, work):
        """ Update state on succes """
        self.logger.debug('Submitted job with id %s' % work['job_id'])
        state_doc = work
        state_doc['state'] = 'submitted'
        state_doc['state_history']['submitted'] = {'modified_by': self.__class__.__name__, 'timestamp': int(time.time())}
        self.state_db.ecCommitOne(state_doc, timestamp=True, viewlist=['state-machine/jobs_by_state'], w=self.quorum['n'])

    @classmethod
    def get_work(cls, dbs):
        """ Get an item of work to be processed by the Job sentinel """
        limit = None
        if hasattr(Job, "job_limit") and Job.job_limit:
            options = {
                'key': 'submitted',
                'reduce': True
            }
            jobs_query = dbs['state'].loadView('state-machine', 'jobs_by_state', options)['rows']
            if len(jobs_query) > 0:
                number_of_submitted_jobs = jobs_query[0]['value']
            else:
                number_of_submitted_jobs = 0
            max_new_jobs = Job.job_limit - number_of_submitted_jobs
            if max_new_jobs <= 0:
                return []
            else:
                limit = max_new_jobs
        options = {
            'key': 'ready_to_submit',
            'include_docs': True,
            'reduce': False
        }
        if limit:
            options['limit'] = limit
        return dbs['state'].loadView('state-machine', 'jobs_by_state', options)['rows']


class Task(Plugin):
    """ Produces one or more job definitions for a given task definition

    Will eventually perform the various task splitting tasks for given
    scenarios, but currently just does a one-to-one mapping for simple
    task requests.

    """
    def __init__(self, task_db, state_db, logger, data_db=None, quorum=None, retries=None, options={'splitter': 'basic', 'job_limit': 100}):
        Plugin.__init__(self, task_db, state_db, logger, data_db, quorum, retries)
        if 'splitter' in options:
            self.splitter = get_splitter(options['splitter'])
        else:
            self.splitter = get_splitter(None)
        self.job_limit = options['job_limit']

    def __retry(self, func, args, times=30, wait=2):
        """ Retry function on _any_ exception """
        fail_count = 0
        while True:
            if fail_count == times:
                raise Exception
            try:
                doc = func(*args)
                if doc == None:
                    fail_count += 1
                else:
                    return doc
            except Exception, e:
                fail_count += 1
                time.sleep(wait)

    def __preprocess_work(self, work):
        """ Preprocessing

        Check input files that may have stochastic parameters and copy into task if they do.

        """
        if 'input_files' in work:
            if 'soils' in work['input_files']:
                soils_doc = self.__retry(self.data_db.document, (work['input_files']['soils'],))
                def make_json_pointer(index, param):
                    return '/soils/%s/%s/value' % (index, param)
                _stochastify(soils_doc['soils'], work, type='soils', make_json_pointer=make_json_pointer)
            if 'geometry' in work['input_files']:
                geometry_doc = self.__retry(self.data_db.document, (work['input_files']['geometry'],))
                is_valid_questa_geometry = ('water_table' in geometry_doc['geometry'] and
                        'soil_strata' in geometry_doc['geometry'])
                if (not is_valid_questa_geometry):
                    # We've either got a chasm geometry file or a duff questa geometry, so leave it alone
                    return
                # Stochastic params can be in soil_strata or water_table. At some point we should
                # drop this code and just do a full walk of the doc
                def make_json_pointer_water_table(index, param):
                    return '/geometry/water_table/%s/%s/value' % (index, param)
                _stochastify(geometry_doc['geometry']['water_table'], work, type='geometry', make_json_pointer=make_json_pointer_water_table)
                if (type(geometry_doc['geometry']['soil_strata']) == list):
                    def make_json_pointer_soil_strata(index, param):
                        return '/geometry/soil_strata/%s/%s/value' % (index, param)
                    _stochastify(geometry_doc['geometry']['soil_strata'], work, type='geometry', make_json_pointer=make_json_pointer_soil_strata)
                else:
                    def make_json_pointer_soil_strata(index, param):
                        return '/geometry/soil_strata/interfaces/%s/value' % param
                    _stochastify([geometry_doc['geometry']['soil_strata']['interfaces']], work, type='geometry',
                            make_json_pointer=make_json_pointer_soil_strata)

    def __call__(self, work):
        state_doc = {
            'task_id': work['_id'],
            'job_id': None,
            'state': 'splitting',
            'state_history':{'splitting': {'modified_by': self.__class__.__name__, 'timestamp': int(time.time())}}
        }
        state_metadata = self.state_db.ecCommitOne(state_doc, timestamp=True, viewlist=['state-machine/jobs_by_state'], w=self.quorum['n'])[0]
        state_doc['_id'] = state_metadata['id']
        state_doc['_rev'] = state_metadata['rev']
        try:
            self.__preprocess_work(work)
            total_jobs = self.splitter.calculate_total_jobs(work)
            if self.job_limit and total_jobs > self.job_limit:
                self.__reject_task(work, state_doc,
                        'rejected - too many jobs: %s' %
                        total_jobs, state_metadata)
                return
            jobs = self.splitter(work)  # Attempt to split the jobs first, so if this fails we don't create any state docs
            self.logger.debug('Task %s has %s jobs' % (work['_id'], len(jobs)))
            uuids = self.task_db.getUuids(count=len(jobs))
            _delete(self.state_db, state_doc, self.logger, w=self.quorum['n'])
            state_doc.pop('_id')
            state_doc.pop('_rev')
            for job in jobs:
                uuid = uuids.pop()
                job['_id'] = uuid
                self.task_db.ecCommitOne(job, timestamp=True, viewlist=['workflow/state'], w=self.quorum['n'])
                state_doc['job_id'] = uuid
                state_doc['state'] = 'ready_to_submit'
                state_doc['state_history']['ready_to_submit'] = {'modified_by': self.__class__.__name__, 'timestamp': int(time.time())}
                self.state_db.ecCommitOne(state_doc, timestamp=True, viewlist=['state-machine/jobs_by_state'], w=self.quorum['n'])
            work['state'] = 'in_progress'
            work['state_history'] = {'in_progress': {'timestamp': int(time.time()), 'modified_by': self.__class__.__name__}}
            self.task_db.ecCommitOne(work, viewlist=['workflow/state'], w=self.quorum['n'])
        except NotImplementedError, e:
            self.logger.exception('Rejecting task with id %s' % (work['_id']))
            self.__reject_task(work, state_doc, 'rejected - not implemented', state_metadata)
        except Exception, e:
            self.logger.exception('Rejecting task with id %s' % (work['_id']))
            self.__reject_task(work, state_doc, 'rejected - %s:%s' % (type(e), str(e)), state_metadata)

    def __reject_task(self, work, state_doc, message, state_metadata):
        """ Mark supplied task as rejected, setting supplied message, and delete associated state doc """
        work['state'] = message
        work['state_history'] = {'rejected': {'timestamp': int(time.time()), 'modified_by': self.__class__.__name__}}
        _delete(self.state_db, state_doc, self.logger, w=self.quorum['n'])
        self.task_db.ecCommitOne(work, viewlist=['workflow/state'], w=self.quorum['n'])
        self.logger.debug('Rejecting task with id %s - %s' % (work['_id'], message))

    @classmethod
    def get_work(cls, dbs):
        """ Get an item of work to be processed by the Task sentinel """
        options = {
            'key': 'new',
            'include_docs': True,
            'reduce': False
        }
        return dbs['task'].loadView('workflow', 'state', options)['rows']


class Tracker(Plugin):
    """
    A sentinel plugin which monitors job states. Needs to do the following as a minimum:

     - When all jobs for a task are closed, close the task
     - Check for stale jobs and resubmit as required
    """
    def __close_tasks(self, task_id):
        """
        Identify tasks where all the jobs are complete and mark the task as
        completed, then delete the related jobs.
        """
        self.logger.info('Closing completed task %s' % task_id)
        view_options = {'reduce': False, 'include_docs': True, 'key': task_id}
        states = self.state_db.loadView('state-machine', 'done_tasks', view_options)['rows']
        summary = {
            'total_jobs': len(states),
            'completed_jobs': len([state for state in states if state['doc']['state'] == 'done']),
            'failed_jobs': len([state for state in states if state['doc']['state'] == 'failed'])
        }
        task_doc = self.task_db.document(task_id)
        task_doc['state'] = 'done'
        task_doc['summary'] = summary
        task_doc['state_history']['done'] = {
            'timestamp': int(time.time()),
            'modified_by': self.__class__.__name__
        }
        self.task_db.ecCommitOne(task_doc, viewlist=['workflow/state'], w=self.quorum['n'])
        self.logger.info('Deleting %s state documents for task %s' % (len(states), task_id))
        for state_doc in states:
            # little double check...
            if state_doc['doc']['task_id'] == task_id:
                self.state_db.delete_doc(state_doc['doc']['_id'], viewlist=['state-machine/jobs_for_task'], w=self.quorum['n'])
        # Now hit the views that index results docs, so they are ready for the users
        self.data_db.loadView('gui', 'docs_by_type', {
            'stale': 'update_after',
            'limit': 0
        })
        self.data_db.loadView('mining', 'fos', {
            'stale': 'update_after',
            'limit': 0
        })

    def __resubmit_stale(self):
        """
        Identify tasks that have failed in Dirac, been modified and are ready
        for resubmission.
        """
        pass

    def __call__(self, work):
        self.__close_tasks(work)
        self.__resubmit_stale()

    @classmethod
    def get_work(cls, dbs):
        """ Get an item of work to be processed by the Tracker sentinel """
        # Return a dummy iterator so plugin gets triggered
        options = {
            'include_docs': False,
            'reduce': True,
            'group': True
        }
        done_tasks = dbs['state'].loadView('state-machine', 'done_tasks', options)['rows']
        return [task['key'] for task in done_tasks if task['value']]