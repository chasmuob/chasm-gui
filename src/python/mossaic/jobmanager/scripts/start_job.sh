#!/bin/bash

mkdir couch
touch couch/__init__.py
mv Couch.py couch/
mv CouchRequest.py couch/

mkdir mossaic
touch mossaic/__init__.py
mv mossaicparser.py mossaic/

mkdir httplib2
mv __init__.py httplib2/
mv iri2uri.py httplib2/

mkdir simplejson
mv __init__.simplejson.py simplejson/__init__.py
mv decoder.py simplejson/decoder.py
mv encoder.py simplejson/encoder.py
mv ordered_dict.py simplejson/ordered_dict.py
mv scanner.py simplejson/scanner.py
mv tool.py simplejson/tool.py

python jobwrapper.py $@