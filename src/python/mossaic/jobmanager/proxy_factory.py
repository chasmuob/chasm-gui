""" proxy_factory.py

The main job of this module is to allow reuse of existing proxies over multiple jobs (if a task has 1000 jobs, we don't want to create a new proxy
for every job). We use a module as a more pythonic alternative to Singleton classes.

The get_proxy method is thread safe. The module is only imported once for all threads, so there is a single module level lock. The lock is acquired at
the beginning of get_proxy and released at the end. Unless actually creating a new proxy (which will happen every hour or so) this method will return
quickly so the lock should have minimal impact on other threads

"""

import os
import math
import random
import subprocess
import sys
import tempfile
import time

from threading import Lock

_mossaic_path = os.getenv('MOSSAIC')
_lock = Lock()
_proxy_creation = None
_proxy = None

if _mossaic_path:
    _create_proxy_script = os.path.join(_mossaic_path, 'bin', 'create_proxy.sh')
else:
    if 'scripts' in os.listdir(os.path.join('..', '..')):
        _create_proxy_script = os.path.join('..', '..', 'scripts', 'create_proxy.sh')
    else:
        _create_proxy_script = os.path.join('..', '..', '..', 'scripts', 'create_proxy.sh')

def _create_proxy(days):
    global _proxy_creation, _proxy
    if _mossaic_path:
        proxy_dir = os.path.join(_mossaic_path, 'var', 'ssl')
    else:
        if 'ssl' in os.listdir('..'):
            proxy_dir = tempfile.mkdtemp(dir=os.path.join('..', 'ssl'))
        else:
            proxy_dir = tempfile.mkdtemp(dir=os.path.join('..', '..', 'ssl'))
    cn_suffix = str(hash(random.random()))[0:8]
    file_prefix = cn_suffix
    proxy_process = subprocess.Popen(['%s %s %s %s %s' % (_create_proxy_script, cn_suffix, proxy_dir, file_prefix, days)], shell=True)
    return_code = proxy_process.wait()
    if return_code != 0:
        raise Exception('Something went wrong getting a proxy')
    _proxy_creation = time.time()
    key = os.path.join(proxy_dir, '%s.key.pem' % file_prefix)
    cert = os.path.join(proxy_dir, '%s.cert.chain.pem' % file_prefix)
    _proxy = {'key': key, 'cert': cert, 'days': days}

def _have_proxy(min_hours):
    """ If proxy has > min_hours left, we consider it valid """
    if _proxy_creation:
        hours_elapsed = (time.time() - _proxy_creation) / 60.0 / 60.0
        hours_left = _proxy['days'] * 24 - hours_elapsed
        if hours_left > min_hours:
            return True
        else:
            return False
    else:
        return False

def get_proxy(hours=20):
    """ Return an existing proxy if we have a valid one, otherwise create a new one.

    We acquire a lock from checking to creating as this operation needs to be threadsafe.

    """
    _lock.acquire()
    try:
        if not _have_proxy(min_hours=hours):
            _create_proxy(days=int(math.ceil(hours / 24.0)))
    except Exception, err:
        raise err, None, sys.exc_info()[2]
    finally:
        _lock.release()
    return _proxy