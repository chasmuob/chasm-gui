"""

Sentinel master

Loads configuration data, sets up logging environment and spawns sentinels via the multiprocessing module.
We then listen for OS signals and handle as follows:

 - SIGHUP 1
  - Stop sentinels and end program
 - SIGUSR1 10
  - Write sentinel status to stdout or file
 - SIGCONT 18
  - Continue executing if stopped
 - SIGUSR2 12
  - Stop execution but don't end program

OS signals will be sent by an init.d script in response to start, stop, restart, status requests.

"""

import logging
import os
import signal
import sys
import time

from ConfigParser import SafeConfigParser, NoOptionError
from multiprocessing import Pipe
from multiprocessing import Process
from optparse import OptionParser

from mossaic.version import __version__

from mossaic.jobmanager.sentinel import Sentinel

__TIMEOUT__ = 20

def do_options():
    """
    Read options and set up a logger
    """
    parser = OptionParser(version="%prog " + str(__version__))
    parser.add_option("-c", "--config-file", dest="config_file",
                    help="Configuration file for the sentinel master")
    parser.add_option("-q", "--quorum-file", dest="quorum_file",
                    help="File containing the quorum constants", default=None)
    parser.add_option("-v", "--verbose", dest="verbose",
                    action="store_true", default=False, help="Be more verbose")
    parser.add_option("-d", "--debug", dest="debug",
                    action="store_true", default=False, help="print debugging statements")
    parser.add_option("-l", "--log-file", dest="log_file",
                    help="Log file - if not specified log statements go to stdout")
    parser.add_option("-s", "--status-file", dest="status_file",
                    help="Status file - if not specified status lines go to stdout")
    parser.add_option("--task-db", dest="task_db", help="Target database for workflow - overrides config file")
    parser.add_option("--state-db", dest="state_db", help="Target database for state machine - overrides config file")
    parser.add_option("--data-db", dest="data_db", help="Target database for data - overrides config file")
    # parse the input
    options, args = parser.parse_args()
    #set up the logger
    log_level = logging.WARNING
    if options.verbose:
        log_level = logging.INFO
    if options.debug:
        log_level = logging.DEBUG
    if options.log_file:
        logging.basicConfig(level=log_level, filename=options.log_file)
    else:
        logging.basicConfig(level=log_level)
    options.logger = logging.getLogger('sentinel master')
    #Maybe change the formatting?
    #("%(asctime)s|%(levelname)s [%(module)s %(funcName)s] %(message)s")
    # exit if a config file isn't provided
    if not options.config_file:
        options.logger.exception('You must provide a configuration file')
        print '===================================='
        print 'Sentinel master not initialised'
        print '===================================='
        sys.exit(1)
    return options

def _spawn_sentinel(url, config, sentinel, sentinel_logger, pipe, quorum):
    """ Create and start a sentinel instance

    This method is forked by the Process object. We create the sentinel in the forked method so that the workers are created in that fork rather than this
    process.

    """
    options = {}
    if 'job_limit' in config:
        options['job_limit'] = config['job_limit']
    sentinel_instance = Sentinel(url, config['task_db'], config['state_db'], sentinel, sentinel_logger, config['workers'], config['max_work'], config['cycle'],
            data_db_name=config['data_db'], retries=config['retries'], quorum=quorum, options=options)
    sentinel_instance.start(pipe)

class SentinelMaster:
    """ Sentinel Master

    Spawns job sentinels via the multiprocess module.

    """
    def __init__(self, options):
        """ Read config data and initialise

        The configuration file has a [master] section which defines the couch url and then a section for each sentinel where the section name
        is the sentinel plugin (this must be equal to one of the keys in sentinel.function_dict).

        The master will only spawn sentinels that are defined in the config file. This means if you only want to run a task sentinel, you just
        have a [task] section and no [job] section.

        """
        self.logger = options.logger
        self.quorum = self.__get_quorum_config(options.quorum_file)
        config = SafeConfigParser(defaults={'data_db': None})
        config.read(options.config_file)
        self.url = config.get('master', 'url')
        sentinels_to_spawn = config.sections()
        sentinels_to_spawn.pop(sentinels_to_spawn.index('master'))
        self.sentinels = {}
        try:
            for sentinel in sentinels_to_spawn:
                self.sentinels[sentinel] = self.__get_sentinel_config(config, sentinel)
                if sentinel == 'input' and not self.sentinels[sentinel]['data_db']:
                    raise NameError('data_db not set for input sentinel')
        except NameError, error:
            self.logger.error("Invalid jobmanager configuration: %s", str(error))
            exit(1)
        for sentinel in self.sentinels.values():
            if options.task_db:
                sentinel['task_db'] = options.task_db
            if options.state_db:
                sentinel['state_db'] = options.state_db
            if options.data_db:
                sentinel['data_db'] = options.data_db
        self.status_file = options.status_file

    def __get_quorum_config(self, quorum_file):
        """ Get a dict containing the quorum values

        Arguments:
         quorum_file - config file which specifies quorum values in a section titled [cluster]

        Environment variables take precedence over values specified in the file. Method has some extra complexity as we want to use
        the environment variables even if the file doesn't exist.

        """
        quorum_dict = {
            'q': os.getenv('QUORUM_Q'),
            'n': os.getenv('QUORUM_N'),
            'r': os.getenv('QUORUM_R'),
            'w': os.getenv('QUORUM_W')
        }
        if quorum_file:
            quorum_config = SafeConfigParser()
            quorum_config.read(quorum_file)
            for quorum_constant in ('q', 'n', 'r', 'w'):
                if not quorum_dict[quorum_constant]:
                    quorum_dict[quorum_constant] = quorum_config.get('cluster', quorum_constant)
        if all([value == None for value in quorum_dict.values()]):
            return None
        else:
            return quorum_dict

    def __get_sentinel_config(self, config, sentinel):
        """ Get a dict containing the configuration data for a sentinel defined in a configuration file

        Arguments:
        config -- ConfigParser object containing the data
        sentinel -- name of the sentinel whose config is to be retrieved

        """
        sentinel_config = {
            'task_db': config.get(sentinel, 'task_db'),
            'state_db': config.get(sentinel, 'state_db'),
            'workers': config.getint(sentinel, 'workers'),
            'max_work': config.getint(sentinel, 'max_work'),
            'cycle': config.getint(sentinel, 'cycle'),
            'data_db': config.get(sentinel, 'data_db')
        }
        try:
            sentinel_config['retries'] = config.getint(sentinel, 'retries')
        except NoOptionError:
            sentinel_config['retries'] = 0
        if sentinel == 'task' or sentinel == 'job':
            try:
                sentinel_config['job_limit'] = config.getint(sentinel, 'job_limit')
            except NoOptionError:
                sentinel_config['job_limit'] = None
        return sentinel_config

    def __call__(self):
        """ Initialise and start sentinels """
        for sentinel, config in self.sentinels.items():
            if 'process' in config and config['process'].is_alive():
                self.logger.warn('Not spawning %s sentinel as it is already running' % sentinel)
            else:
                self.logger.debug('Spawning %s sentinel' % sentinel)
                sentinel_logger = logging.getLogger('%s sentinel' % sentinel)
                config['pipe'], child_pipe = Pipe()
                config['process'] = Process(target=_spawn_sentinel, args=(self.url, config, sentinel, sentinel_logger, child_pipe, self.quorum))
                config['process'].start()

    def stop(self, signum=None, frame=None):
        """ Stop sentinels """
        for key, config in self.sentinels.items():
            config['pipe'].send('stop')
            config['process'].join(__TIMEOUT__)
            if config['process'].is_alive():
                self.logger.warn('%s sentinel is still alive - terminating' % key)
                config['process'].terminate()

    def resume(self, signum=None, frame=None):
        """ Resume sentinels if stopped """
        self()  # Just try to start - the __call__ method will refuse to start a sentinel if it is already running

    def status(self, signum=None, frame=None):
        """ Print sentinel status to stdout or, if defined, status file """
        status_text = []
        for sentinel, config in self.sentinels.items():
            while config['pipe'].poll():    # Flush incoming pipe
                config['pipe'].recv()
            config['pipe'].send('status')
            retries = 10
            attempt = 0
            while attempt < retries and not config['pipe'].poll():    # Retry pipe a set number of times
                time.sleep(0.1)
                attempt += 1
            if not config['pipe'].poll():
                status_text.append('[%s sentinel] Status unknown' % sentinel)
            else:
                status = config['pipe'].recv()
                status_text.append('[%s sentinel] Queue size: %s Fail count: %s' % (status['plugin'], status['queue_size'], status['fail_count']))
        self.__write_status(status_text)

    def __write_status(self, status_text):
        """ Write the status to stdout or, if defined, the status file """
        if self.status_file:
            with file(self.status_file, 'w') as status_output:
                for status_line in status_text:
                    status_output.write(status_line)
        else:
            for status_line in status_text:
                print status_line

    def quit(self, signum=None, frame=None):
        """ Stop and quit """
        self.stop()
        exit(0)


if __name__ == '__main__':
    print '===================================='
    print 'Sentinel master v.%s' % __version__
    print '===================================='

    opts = do_options()
    master = SentinelMaster(opts)
    master()

    master.status()

    signal.signal(signal.SIGHUP, master.quit)
    signal.signal(signal.SIGUSR1, master.status)
    signal.signal(signal.SIGUSR2, master.resume)
    signal.signal(signal.SIGTERM, master.stop)

    while True:
        signal.pause()  # Sleep until we get a signal