"""
CMS CouchDB Library
===================

Provides a library for interacting with Apache CouchDB. This version has been
adapted for use with Cloudant's BigCouch.

Available modules
-----------------

Couch
    Implementation of the CouchDB API
CouchRequest
    Handles the actual HTTP requests required by the API

Using
-----

See the documentation for the Couch module for instructions.

"""