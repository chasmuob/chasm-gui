#!/usr/bin/env python

from distutils.core import setup

execfile('mossaic/version.py')

setup(name = 'Mossaic',
    version = __version__,
    description = 'Python modules for Mossaic project',
    author = 'Mike Wallace (couch package provided by CMS team, customised for BigCouch by Mike Wallace)',
    author_email = 'michael.wallace@bristol.ac.uk',
    packages = ['mossaic', 'mossaic.jobmanager', 'couch', 'simplejson'],
    py_modules = ['version'],
	requires = ['httplib2'],
	data_files = [('etc/init.d', ['mossaic/jobmanager/init.d/jobmanager']), ('scripts', ['mossaic/jobmanager/scripts/start_job.sh'])]
)
