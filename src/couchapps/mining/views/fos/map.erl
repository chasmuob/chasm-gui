fun({Doc}) ->
    case couch_util:get_value(<<"type">>, Doc) of
        undefined ->
           ok;
        Type when Type == <<"results">> ->
            TaskId = couch_util:get_value(<<"task_id">>, Doc),
            JobId = couch_util:get_value(<<"job_id">>, Doc),
            EmitFos = fun([Fos | Rest], Acc, CB) ->
                Emit([TaskId, JobId, Acc], Fos),
                case Rest of
                   [] ->
                        ok;
                    _ ->
                        CB(Rest, Acc + 1, CB)
                end
            end,
            case couch_util:get_value(<<"chasm_output">>, Doc) of
                undefined ->
                    ok;
                Output when Output == <<"unavailable">> ->
                    ok;
                Output when is_list(Output) ->
                    ok;
                Output ->
                    try
                       Fos = couch_util:get_nested_json_value(Output, [<<"factor_of_safety">>]),
                       EmitFos(Fos, 0, EmitFos)
                    catch
                       {not_found, _} ->
                          ok
                    end;
                _ ->
                   ok
            end;
        _ ->
            ok
    end
end.