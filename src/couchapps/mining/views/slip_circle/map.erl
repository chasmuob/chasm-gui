fun({Doc}) ->
    case couch_util:get_value(<<"type">>, Doc) of
        undefined ->
           ok;
        Type when Type == <<"results">> ->
           TaskId = couch_util:get_value(<<"task_id">>, Doc),
           JobId = couch_util:get_value(<<"job_id">>, Doc),
           EmitSlip = fun([X_H|X_T], [Y_H|Y_T], [R_H|R_T], Acc, CB) ->
              Emit([TaskId, JobId, Acc], {[{<<"x">>, X_H}, {<<"y">>, Y_H}, {<<"r">>, R_H}]}),
              case X_T of
                 [] ->
                    ok;
                 _ ->
                    CB(X_T, Y_T, R_T, Acc + 1, CB)
              end
           end,
           case couch_util:get_value(<<"chasm_output">>, Doc) of
              undefined ->
                   ok;
              Output when Output == <<"unavailable">> ->
                   ok;
              Output when is_list(Output) ->
                   ok;
              Output ->
                   try
                      X = couch_util:get_nested_json_value(Output, [<<"centre">>, <<"x">>]),
                      Y = couch_util:get_nested_json_value(Output, [<<"centre">>, <<"y">>]),
                      R = couch_util:get_nested_json_value(Output, [<<"radius">>, <<"value">>]),
                      EmitSlip(X, Y, R, 0, EmitSlip)
                   catch
                      {not_found, _} ->
                         ok
                   end;
               _ ->
                  ok
           end;
        _ ->
           ok
    end
end.