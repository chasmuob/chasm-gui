fun({Doc}) ->
   case couch_util:get_value(<<"type">>, Doc) of
       undefined ->
          ok;
       Type when Type == <<"results">> ->
          TaskId = couch_util:get_value(<<"task_id">>, Doc),
          JobId = couch_util:get_value(<<"job_id">>, Doc),
          EmitCellData = fun(Data, [Key | Rest], CB) ->
             case Key of
                <<"soil_type">> ->
                   try
                      SoilData = couch_util:get_nested_json_value(Data, [Key, <<"value">>]),
                      Emit([TaskId, Key, JobId, 0], SoilData)
                   catch
                      {not_found, _} ->
                         ok
                   end;
                _ ->
                   Decompress = fun(CellData) ->
                      Decoded = base64:decode(CellData),
                      Z = zlib:open(),
                      ok = zlib:inflateInit(Z),
                      Uncompressed = zlib:inflate(Z, Decoded),
                      ok = zlib:inflateEnd(Z),
                      AsList = bitstring_to_list(iolist_to_binary(Uncompressed)),
                      {ok, Tokens, _} = erl_scan:string(AsList),
                      {ok, Term} = erl_parse:parse_term(lists:append(Tokens, [{dot, 1}])),
                      Term
                   end,
                   EmitCellsTimestep = fun([CellData | Rest], Acc, CB) ->
                      Emit([TaskId, Key, JobId, Acc], CellData),
                      case Rest of
                         [] ->
                            ok;
                         _ ->
                            CB(Rest, Acc + 1, CB)
                      end
                   end,
                   try
                      case couch_util:get_nested_json_value(Data, [Key, <<"value">>]) of
                         CellData when is_binary(CellData) ->
                            Decompressed =  Decompress(CellData),
                            EmitCellsTimestep(Decompressed, 0, EmitCellsTimestep);
                         _ ->
                            ok
                      end
                   catch
                      {not_found, _} ->
                         ok
                   end
             end,
             case Rest of
                [] ->
                   ok;
                _ ->
                   CB(Data, Rest, CB)
             end
          end,
          KeysToEmit = [<<"moisture_content">>, <<"soil_type">>, <<"pressure_head">>],
          case couch_util:get_value(<<"chasm_output">>, Doc) of
             undefined ->
                  ok;
             Output when Output == <<"unavailable">> ->
                  ok;
             Output when is_list(Output) ->
                  ok;
             Output ->
                  EmitCellData(Output, KeysToEmit, EmitCellData);
             _ ->
                 ok
          end;
       _ ->
          ok
   end
end.