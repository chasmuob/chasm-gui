function(doc) {
  if (doc.type === "results" && doc.location &&
      "questa_output" in doc) {
    emit({
        type: "Point",
        coordinates: [doc.location["lat"], doc.location["long"]]
      },
      doc.questa_output.net_expected_present_value
    );
  }
};