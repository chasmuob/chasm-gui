function(head, req) {
  send("latitude,longitude,NEPV\n");
  var row;
  while (row = getRow()) {
    send(row.geometry.coordinates.join(",") + "," + row.value + "\n");
  }
}