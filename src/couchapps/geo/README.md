# GeoSpatial queries with GeoCouch

This is a proof-of-concept to demonstrate how the spatial query and spatial
list functions of [GeoCouch](https://github.com/couchbase/geocouch/) can be
used to provide data in a format that can be consumed by a Geographical
Information System such as ArcGIS.

Assuming MAP is running in a geospatial-enabled CouchDB distribution then this
can be pushed to the main database and used to run bounding-box queries on the
net expected present value (NEPV) of QUESTA simulation outputs.

# Examples

An example document, which contains only the fields required for the spatial query, is shown below (although this query will work with any QUESTA results document).

    {
      "type": "results",
      "location": {
        "lat": 54.123,
        "long": -2.1
      },
      "questa_output": {
        "net_expected_present_value": 23232323
      }
    }

A bounding-box query can be carried out using the following URL:

    http://COUCHDB_INSTANCE:COUCHDB_PORT/DB_NAME/_design/geo/_spatial/results?bbox=-10,-10,100,100

Which will produce something like this:

    {
      "update_seq":14,
      "rows":[{
        "id":"59a8bf09b212a43fdc3071bb98004935",
        "bbox":[54.123,-2.1,54.123,-2.1],
        "geometry":{
          "type":"Point",
          "coordinates":[54.123,-2.1]
        },
        "value":23232323
      }]
    }

The query results can be obtained in CSV format using this URL:

    http://COUCHDB_INSTANCE:COUCHDB_PORT/DB_NAME/_design/geo/_spatial/_list/csv/results?bbox=-10,-10,100,100

Which will produce a simple CSV output that can be imported directly into ArcGIS:

    latitude,longitude,NEPV
    54.123,-2.1,23232323