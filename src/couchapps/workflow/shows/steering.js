/** Show for steering document
*
* Validate requested document.
* If invalid: return HTTP error code.
* If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
*/

/**
 * Entry point function, called by couchdb.
 *
 * @param {object} doc The document to be rendered by this function.
 * @param {object} req The HTTP request to which this function is responding.
 * @return {object} HTTP response object which is returned by couchdb to the caller.
 */
function(doc, req) {
    var errors = require('lib/errors').init();
    var utils = require('lib/mossaicutils').init();
    var sha1 = require('lib/sha1').init().sha1;

    /**
     * Get a string representation of the bits that define ouput files and output variables.
     *
     * @param {object} output Flags for output data.
     * @return {string} String representation of boolean flags in supplied output data.
     */
    var getOutputBits = function(output) {
        var outputBits = [];
        outputBits.push(utils.boolToInt(output.factor_of_safety));
        outputBits.push(utils.boolToInt(output.pressure_head));
        outputBits.push(utils.boolToInt(output.soil_moisture_content));
        outputBits.push(utils.boolToInt(output.total_soil_moisture));
        outputBits.push(utils.boolToInt(output.vegetation_interception));
        return outputBits.join(' ');
    };

    /**
     * Determine whether supplied document is a CHASM steering file.
     *
     * @param {object} doc The document to be tested.
     * @return {boolean} True if suppled document is a CHASM steering file, false otherwise.
     */
    var isChasmSteeringFile = function(doc) {
        return ("executable" in doc && "application" in doc.executable &&
            doc.executable.application === "chasm");
    };

    /**
     * Get the required econometric variables for the steering file.
     * As we will always be running single runs in the job wrapper the second variable will always be 1.
     * The first variable will be 0 for design storms (default) and 2 for realistic rainfall.
     *
     * @param {object} doc Document containing data from which econometric variables are determined.
     * @return {string} String representation of the econometric variables.
     */
    var getEconometricVariables = function(doc) {
        return [utils.boolToInt(doc.realistic_rainfall), 1].join(" ");
    }

    /**
     * Get the required input files for the steering file.
     *
     * @param {object} input_files Object containing the input file type and id
     * @return {string} String representation of the input files.
     */
    var getInputFiles = function(doc) {
        var input_files = doc.input_files;
        var inputFiles = [];
        if (isChasmSteeringFile(doc)) {
          inputFiles.push("geometry file: " + input_files.geometry);
        } else {
          inputFiles.push("geometry file: not_used_by_questa");
        }
        inputFiles.push("soils database file: " + input_files.soils);
        inputFiles.push("stability file: " + input_files.stability);
        inputFiles.push("boundary conditions file: " + input_files.boundary_conditions);
        if (input_files.reinforcements) {
            inputFiles.push("reinforcements: " + input_files.reinforcements);
        }
        if (input_files.vegetation) {
            inputFiles.push("vegetation file: " + input_files.vegetation);
        }
        if (input_files.stochastic_parameters) {
            inputFiles.push("stochastic parameters: " + input_files.stochastic_parameters);
        }
        return inputFiles.join("\n");
    }

    var getInputFilesQuesta = function(input_files) {
        var inputFiles = [];
        inputFiles.push("initial slope: " + input_files.initial_slope);
        inputFiles.push("engineering cost file: " + input_files.engineering_cost);
        inputFiles.push("road network file: " + input_files.road_network);
        return inputFiles.join("\n");
    }

    /**
     * Create the text content of a steering file.
     *
     * @param {object} doc The document containing the job definition data.
     * @return {string} The text content of a steering file.
     */
    var getSteeringFile = function(doc) {
        var steeringFile = [];
        steeringFile.push(getInputFiles(doc));
        if (isChasmSteeringFile(doc)) {
            steeringFile.push("output files: " + getOutputBits(doc.output_files));
        } else {    // We have a QUESTA steering file
            steeringFile.push("");
            steeringFile.push("report filename: " + doc.report_filename);
            steeringFile.push(getInputFilesQuesta(doc.input_files));
            steeringFile.push("");
        }
        steeringFile.push("output variables: " + getOutputBits(doc.output_variables));
        steeringFile.push("duration of the simulation (" + doc.duration.unit + "): " + doc.duration.value);
        steeringFile.push("time step (" + doc.time_step.unit +"): " + doc.time_step.value);
        if (!isChasmSteeringFile(doc)) {
            steeringFile.push("econometric variables: " + getEconometricVariables(doc));
        }
        return steeringFile.join("\n");
    };

    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "job") {
        return errors.invalid_request("steering");
    }
    try {
        var steeringFile = getSteeringFile(doc);
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = steeringFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
         "body": body
    };
};
