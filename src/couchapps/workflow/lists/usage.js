function(head, req) {
  start({
    "headers": {
       "Content-Type": "text/plain"
     }
  });
  send("User,Year,Month,Number of jobs,Total runtime\n");
  var row;
  while (row = getRow()) {
    if (row.key && row.key.length >= 3 &&
        row.value && row.value.count && row.value.sum) {
      send([row.key[0],row.key[1],row.key[2],row.value.count,row.value.sum].join(",") + "\n");
    }
  }
}