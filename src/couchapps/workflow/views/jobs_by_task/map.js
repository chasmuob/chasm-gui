function(doc) {
  if (doc.type == "job") {
    var state = doc.state || "new";
    emit([doc.task_id, state], doc.name);
  }
}