function(doc) {
  if (typeof(doc.type) !== "undefined" && doc.type === "job" &&
      doc.state == "done") {
    if (typeof(doc.stats) !== "undefined") {
      var start = new Date(doc.stats.start_time * 1000);
      emit([
          doc.requestor,
          start.getFullYear(),
          start.getMonth() + 1,
          start.getDate()
        ], doc.stats.runtime);
    }
  }
}