function(doc) {
  if (doc.type == "job" && doc.input_files &&
      (doc.input_files.geometry || doc.input_files.initial_slope)) {
    var geometry_id;
    var state = doc.state || "new";
    if ("initial_slope" in doc.input_files) {
      geometry_id = doc.input_files.initial_slope;
    } else {
      geometry_id = doc.input_files.geometry;
    }
    emit([geometry_id, doc.task_name, state], 1);
  }
}