function(doc) {
  if (doc.type == "task") {
    if ("state" in doc) {
      var state = doc.state;
    } else {
      var state = "new";
    }
    emit([doc.parent_id, doc.requestor, state], doc.name);
  }
}