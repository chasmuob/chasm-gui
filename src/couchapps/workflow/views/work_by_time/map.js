/** View which returns unclaimed work ordered by time
 */

/**
 * Entry point for view function, called by view server.
 *
 * @param {object} doc Document to be indexed.
 */
function(doc) {
    if ((doc.type == "job" || doc.type == "task") && !("state" in doc)) {
        d = new Date(doc.timestamp * 1000);
        emit([doc.type, d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate(), d.getUTCHours(), d.getUTCMinutes(), d.getUTCSeconds()], 1);
    }
}