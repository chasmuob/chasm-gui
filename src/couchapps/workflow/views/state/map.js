function(doc) {
  // States are: ready_to_submit (or null), submitted, done/failed
  // ready_to_submit = TaskSentinel
  // ready_to_submit -> submitted = JobSentinel
  // submitted -> done = JobWrapper
  // submitted -> failed = JobWrapper
  // TaskTracker then picks up done/failed and closes them
  if (doc.type == 'task'){
    if (doc.state){
      emit(doc.state, 1);
    } else {
      emit('new', 1);
    }
  }
}