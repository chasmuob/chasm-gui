function(doc) {
  if (doc.type == "task") {
    if ("state" in doc) {
      var state = doc.state;
    } else {
      var state = "new";
    }
    emit([doc.requestor, state], doc.name);
  }
}