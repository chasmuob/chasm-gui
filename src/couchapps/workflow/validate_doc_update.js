var arrayContains = function(array, term) {
  for (var i = 0; i < array.length; i++) {
    if (term == array[i]) {
      return true;
    }
  }
  return false;
}

function (newDoc, oldDoc, userCtx) {
  //if (oldDoc && oldDoc.type != newDoc.type) { // TODO - reinstate when we have data and workflow in separate DBs
  //  throw({forbidden: "Changing of document type is not allowed"});
  //}
  if (newDoc.type == "task") {
    if (!userCtx.name) {
      throw({unauthorized: "Please log in before attempting to create new tasks"});
    } else if (!arrayContains(userCtx.roles, "requestor") && (!arrayContains(userCtx.roles, "job_manager")) && !arrayContains(userCtx.roles, "_admin")) {
      throw({forbidden: "You do not have permission to create new tasks"});
    }
  } else if (newDoc.type == "job") {
    if (!userCtx.name) {
      throw({unauthorized: "Please log in before attempting to create new jobs"})
    } else {
      if (!arrayContains(userCtx.roles, "job_manager") && !arrayContains(userCtx.roles, "job_wrapper") && !arrayContains(userCtx.roles, "_admin")) {
        throw({forbidden: "You do not have permission to create new jobs"});
      }
    }
  }
  if (!userCtx.name) {
    throw({unauthorized: "Please log in before attempting to modify database content"});
  }
}