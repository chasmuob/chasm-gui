function (newDoc, oldDoc, userCtx) {
  if (!userCtx.name) {
    throw({unauthorized: "Please log in before attempting to modify database content"});
  }
}