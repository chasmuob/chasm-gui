var arrayContains = function(array, term) {
  for (var i = 0; i < array.length; i++) {
    if (term == array[i]) {
      return true;
    }
  }
  return false;
}

function (newDoc, oldDoc, userCtx) {
  if (!userCtx.name) {
    throw({unauthorized: "Please log in before attempting to modify database content"});
  }
  if (oldDoc) {
    if (!userCtx.roles) {
      throw({unauthorized: "You do not have the required permissions to modify data"});
    }
    if (!arrayContains(userCtx.roles, "mossaicadmin") && !arrayContains(userCtx.roles, "job_wrapper")) {
      throw({unauthorized: "You do not have the required permissions to modify data"});
    }
  }
}