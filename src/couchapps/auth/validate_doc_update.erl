fun(_Doc, DiskDoc, {JsonCtx}, _SecObj) ->
    case couch_util:get_value(<<"name">>, JsonCtx) of
        null ->
            {[{<<"unauthorized">>, "Please log in before attempting to modify database content"}]};
        _ ->
            case DiskDoc of
               null ->
                  1;
               _ ->
                  case couch_util:get_value(<<"roles">>, JsonCtx) of
                     Roles when is_list(Roles) ->
                        MatchRoles = fun(Role) ->
                           case Role of
                              <<"mossaicadmin">> ->
                                 true;
                              <<"job_wrapper">> ->
                                 true;
                              _ ->
                                 false
                           end
                        end,
                        case lists:filter(MatchRoles, Roles) of
                           [_H | _] ->
                              1;
                           _ ->
                              {[{<<"unauthorized">>, "You do not have the required permissions to modify data"}]}
                        end;
                     null ->
                        {[{<<"unauthorized">>, "You do not have the required permissions to modify data"}]};
                     _ ->
                        {[{<<"unauthorized">>, "You do not have the required permissions to modify data"}]}
                  end
            end
    end
end.
