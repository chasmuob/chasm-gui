var gulp = require('gulp');
var watch = require('gulp-watch');
var exec = require('child_process').exec;
var livereload = require('gulp-livereload');
var jshint = require('gulp-jshint');
var minifyHtml = require('gulp-minify-html');
var templateCache = require('gulp-angular-templatecache');
var ngAnnotate = require('gulp-ng-annotate');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var angularFileSort = require('gulp-angular-filesort');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var less = require('gulp-less');

//var concat = require('gulp-concat');
//var uglify = require('gulp-uglify');
//var del = require('del');

var request = 'couchapp push --docid _design/gui ./ http://chasm:chasm@127.0.0.1:4000/chasm';

var paths = {
  assets: ['templates/**/*','_attachments/**/*','views/**/*'],
  source: {
    js: ['app/**/*.js'],
    templates: ['app/**/*.html']
  },
  build:{
    js: '_attachments/app/'
  }
};

gulp.task('upload',function(){
  exec(request,function(err,stdout,stderr){
    if(err || stderr){
      console.log(stderr);
      console.log(err);
    }
    livereload.reload();
  });
});

gulp.task('jshint',function(){
  gulp.src(paths.source.js)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('css',function(){
  return gulp.src('less/styles.less')
    .pipe(less())
    .pipe(gulp.dest('./_attachments/style'));
});
/**
 * JS
 */
gulp.task('js', ['jshint'], function(done) {
  gulp.src(paths.source.js)
    // .pipe(sourcemaps.init({
    //   loadMaps: true
    // }))
    .pipe(angularFileSort())
    .pipe(concat('app.js'))
    /*jshint camelcase: false */
    .pipe(ngAnnotate({
      single_quotes: true
    }))
    .pipe(gulp.dest(paths.build.js))
    // .pipe(uglify())
    // .pipe(rename({
    //   extname: '.min.js'
    // }))
    // .pipe(sourcemaps.write(paths.build.maps))
    .pipe(gulp.dest(paths.build.js))
    .on('end', done);
});


/**
 * Templates
 */
gulp.task('templates', function(done) {
  gulp.src(paths.source.templates)
    .pipe(minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe(templateCache({
      standalone: true,
      module: 'chasm.templates'
    }))
    /*jshint camelcase: false */
    .pipe(ngAnnotate({
      single_quotes: true
    }))
    .pipe(uglify())
    .pipe(rename({
      extname: '.min.js'
    }))
    .pipe(gulp.dest(paths.build.js))
    .on('end', done);
});


gulp.task('watch',function(){
  livereload.listen({start: true});
  watch(paths.source.js,function(){
    gulp.start('js');
  });
  watch(paths.source.templates,function(){
    gulp.start('templates');
  });

  return watch(paths.assets,function(){
    gulp.start('upload');
  });
});

gulp.task('default',['watch']);
