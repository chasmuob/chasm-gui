/**
 * @file Creates the Mossaic.forms namespace and loads the forms
 */
(function() {
  var scripts_to_load = [
      "lib/mossaic.forms.datum.js",
      "lib/mossaic.forms.cross_section.js",
      "lib/mossaic.forms.geometry_metadata.js",
      "lib/mossaic.forms.mesh.js",
      "lib/mossaic.forms.cross_section.js",
      "lib/mossaic.forms.soils_data.js",
      "lib/mossaic.forms.soil_depths.js",
      "lib/mossaic.forms.stability.js",
      "lib/mossaic.forms.engineering_cost.js",
      "lib/mossaic.forms.rainfall.js",
      "lib/mossaic.forms.water_table.js"
  ];

  /**
   * @description Provides Backbone.View classes which render form
   * representations of models defined in Mossaic.models
   * @namespace
   */
  Mossaic.forms = {};

  /**
   * Backbone View from which many Mossaic form views inherit
   * @constructor
   * @extends Backbone.View
   */
  Mossaic.forms.Basic = Backbone.View.extend({
    /**
     * @memberof Mossaic.forms.Basic
     * @instance
     * @param {object} model Backbone.Model object to be represented
     *                       by this form
     * @param {object} options Hash of options, used by child forms
     */
    initialize: function(model, options) {
      _.bindAll(this);
      /**
       * The model to be represented by this form.
       * @type Backbone.Model
       */
      this.model = model;
      this.model.bind('change', this.handle_change);
    },
    /**
     * Set the model to be represented by the form.
     * @memberof Mossaic.forms.Basic
     * @instance
     * @param {Mossaic.models.Model} model Model to be represented
     *                                     by this form
     */
    set_model: function(model) {
      if (typeof(this.model) !== "undefined" &&
          typeof(this.model.unbind) === "function") {
        this.model.unbind("change");
      }
      this.model = model;
      this.model.bind("change", this.handle_change);
    },
    /**
     * Hide the form by removing it from the DOM
     * @memberof Mossaic.forms.Basic
     * @instance
     */
    hide: function() {
      d3.select(this.form_id).remove();
      this.hidden = true;
    },
    /**
     * Show the form
     * @memberof Mossaic.forms.Basic
     * @instance
     */
    show: function() {
      this.hidden = false;
      this.render();
      $(this.form_id + " input:enabled:first").focus();
    },
    /**
     * Handle changes to the collection represented by this form
     * @memberof Mossaic.forms.Basic
     * @instance
     */
    handle_change: function() {
      // Override for custom change handling
      this.render();
    },
    /**
     * Render the form. Should be triggered either by Mossaic.forms.Basic.show
     * or Mossaic.forms.Basic.handle_change.
     * @memberof Mossaic.forms.Basic
     * @instance
     */
    render: function() {
      // Form rendering code goes here
    }
  });

  var load_scripts = function(scripts) {
    for (var i=0; i < scripts.length; i++) {
      document.write('<script src="'+scripts[i]+'"><\/script>');
    }
  };

  load_scripts(scripts_to_load);

  /**
   * Build any forms that require templates from the design doc. This must
   * be run *after* the design document has loaded, otherwise the templates
   * won't be loaded.
   * This is triggered by the Mossaic.Router which, as top-level object, is
   * responsible for ensuring any deferred forms are built.
   * @function
   */
  Mossaic.forms.build_deferred = function() {
    _.each(Mossaic.forms.deferred, function(form) {
      form();
    });
  };
})();