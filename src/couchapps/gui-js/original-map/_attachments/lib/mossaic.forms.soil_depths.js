/**
 * @file Define the form for displaying soil depths for a
 *       Mossaic.models.Geometry model.
 */
(function() {
  /**
   * Backbone View for Mossaic.models.Geometry which displays soil strata
   * depth data on a form.
   *
   * Depth measurements are located by the surface distance from the datum.
   * Depth measurements for a particular surface distance are displayed in a
   * single column.
   * An empty column is displayed for adding a new group of depth measurements.
   * When this column is filled in the new depth is added to the model.
   *
   * This View also has a reference to layers, a Backbone.Collection which
   * is bound to a Mossaic.models.Geometry model.
   *
   * This is a legacy view which builds the form using d3.js rather than the
   * more sensible jQuery/templating approach.
   * @constructor
   * @extends Mossaic.forms.Basic
   */
  Mossaic.forms.SoilDepths = Mossaic.forms.Basic.extend({
    /**
     * Initialize the form.
     * Called implicitly with "new Mossaic.forms.SoilDepths".
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @param {Mossaic.models.Geometry} model Model to be represented
     *                                        by this form
     * @param {object} options Hash of options
     * @param {string} options.divname jQuery selector for div to which this
     *                                 form should be appended
     * @param {object} options.layers Backbone.Collection containing a model
     *                                for each soil strata interface
     * @param {boolean} options.stochastic True if the form should default
     *                                     to showing stochastic fields
     */
    initialize: function(model, options) {
      Mossaic.forms.Basic.prototype.initialize.apply(this, [model, options]);
      this.divname = options.divname || "#soil_depths_form";
      this.form_id = this.divname + "_soil_depths";
      this.new_depths = {depth: {}, surface_distance_from_cut_toe: undefined};
      this.layers = options.layers; // TODO - don't like reference to layers here, better way?
      this.number_of_measurements = this.layers.length - 1;
      if (typeof(options.stochastic) !== "undefined") {
        this.stochastic = options.stochastic;
      } else {
        this.stochastic = this.model.is_stochastic("soil_strata");
      }
      this.model.default_stochastic = this.stochastic;
    },
    /**
     * Set the geometry model to be represented by the form.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @param {Mossaic.models.Geometry} geometry Model to be represented
     *                                           by this form
     */
    set_geometry: function(geometry) {
      if (typeof(this.model) !== "undefined" &&
          typeof(this.model.unbind) === "function") {
        this.model.unbind("change");
      }
      this.layers = geometry.layers;
      this.model = geometry;
      this.stochastic = this.model.is_stochastic("soil_strata");
      this.model.bind("change", this.handle_change);
    },
    /**
     * Set the layers collection to be represented by the form.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @param {Backbone.Collection} layers Collection of soil strata interfaces
     */
    set_layers: function(layers) {
      this.layers = layers;
    },
    /**
     * If form is stochastic, make it deterministic. If it's deterministic,
     * make it stochastic.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     */
    flip_stochastic: function() {
      var that = this;
      this.stochastic = !this.stochastic;
      this.model.default_stochastic = this.stochastic;
      var geometry = _.clone(this.model.get("geometry"));
      var soil_strata = _.clone(geometry.soil_strata);
      if (typeof(soil_strata) === "undefined") {
        return;
      }
      if (this.model.is_soil_known()) {
        var to_convert = ["depth"];
        var all_depths = soil_strata.concat(this.new_depths);
        _.each(all_depths, function(_measurement, i) {
          var measurement = _.clone(_measurement);
          if (that.stochastic) {
            _.each(to_convert, function(field) {
              var parameter = _.clone(measurement[field]);
              parameter.mean = parameter.value;
              if (typeof(parameter.mean) !== "undefined") {
                parameter.standard_deviation = _.map(
                  _.range(0, parameter.mean.length),
                  function() {
                    return 0;
                  }
                );
              }
              delete parameter.value;
              measurement[field] = parameter;
            });
          } else {
            _.each(to_convert, function(field) {
              var parameter = _.clone(measurement[field]);
              parameter.value = parameter.mean;
              delete parameter.mean;
              delete parameter.standard_deviation;
              measurement[field] = parameter;
            });
          }
          if (i < soil_strata.length) {
            soil_strata[i] = measurement;
          }
        });
      } else {
        var to_convert = ["depth", "angle"];
        var interfaces = _.clone(soil_strata.interfaces);
        if (that.stochastic) {
          _.each(to_convert, function(field) {
            var parameter = _.clone(interfaces[field]);
            parameter.mean = parameter.value;
            if (typeof(parameter.mean) !== "undefined") {
              parameter.standard_deviation = _.map(
                _.range(0, parameter.mean.length),
                function() {
                  return 0;
                }
              );
            }
            delete parameter.value;
            interfaces[field] = parameter;
          });
        } else {
          _.each(to_convert, function(field) {
            var parameter = _.clone(interfaces[field]);
            parameter.value = parameter.mean;
            delete parameter.mean;
            delete parameter.standard_deviation;
            interfaces[field] = parameter;
          });
        }
        soil_strata.interfaces = interfaces;
      }
      geometry.soil_strata = soil_strata;
      this.model.changed_by = this;
      this.model.set({
        geometry: geometry
      });
      this.render();
    },
    /**
     * Determine whether all depth measurements are present
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @private
     * @return true if all depth measurements & surface distance exist, or false
     */
    have_all_measurements: function() {
      var number_of_new_depths = 0;
      if (this.stochastic) {
        _.each(this.new_depths.depth.mean, function() {
          number_of_new_depths++;
        });
      } else {
        _.each(this.new_depths.depth.value, function() {
          number_of_new_depths++;
        });
      }
      return (
        typeof(this.new_depths.surface_distance_from_cut_toe) !== "undefined" &&
        typeof(this.new_depths.depth) !== "undefined" &&
        ("value" in this.new_depths.depth ||
          ("mean" in this.new_depths.depth &&
          "standard_deviation" in this.new_depths.depth)) &&
        number_of_new_depths === this.number_of_measurements
      );
    },
    /**
     * Add a new set of depth measurements to the Geometry model if we now have
     * all depth measurements and surface distance.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    maybe_update_depth: function(event) {
      var target = $(d3.event.target);
      var id = target.attr("id").split("_");
      var index = parseInt(id[2]);
      var strata = parseInt(id[4]);
      var depth_element = $("#depth_section_" + index + "_strata_" + strata);
      var raw_depth = depth_element.attr("value");
      var depth;
      if (typeof(raw_depth) === "string" && raw_depth.length > 0) {
        depth = parseFloat(raw_depth);
      } else {
        depth = 0;
      }
      if (this.stochastic) {
        if (typeof(this.new_depths.depth.mean) === "undefined") {
          this.new_depths.depth.mean = [];
        }
        this.new_depths.depth.mean[strata] = depth;
        if (typeof(this.new_depths.depth.standard_deviation) === "undefined") {
          this.new_depths.depth.standard_deviation = [];
        }
        var sd_id = "#" + depth_element.attr("id") + "_sd";
        var sd_raw = $(sd_id).attr("value");
        if (typeof(sd_raw) === "string" && sd_raw.length > 0) {
          this.new_depths.depth.standard_deviation[strata] = parseFloat(sd_raw);
        } else {
          this.new_depths.depth.standard_deviation[strata] = 0;
        }
      } else {
        if (typeof(this.new_depths.depth.value) === "undefined") {
          this.new_depths.depth.value = [];
        }
        this.new_depths.depth.value[strata] = depth;
      }
      if (this.have_all_measurements()) {
        var new_depth = this.new_depths.depth;
        var new_surface_distance =this.new_depths.surface_distance_from_cut_toe;
        this.new_depths = {depth: {}, surface_distance_from_cut_toe: undefined};
        this.model.set_soil_depth(new_depth, undefined, {
            surface_distance_from_cut_toe:
                new_surface_distance,
            index: index,
            changed_by: this
        });
      }
    },
    /**
     * Update a depth measurement for an existing set of measurements.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    update_depth: function(event) {
      var target = $(d3.event.target);
      var id = target.attr("id").split("_");
      var index = parseInt(id[2]);
      var strata = parseInt(id[4]);
      var depth_element = $("#depth_section_" + index + "_strata_" + strata);
      if (this.stochastic) {
        var mean = parseFloat(depth_element.attr("value"));
        var sd_id = "#" + depth_element.attr("id") + "_sd";
        var sd = parseFloat($(sd_id).attr("value"));
        this.model.set_soil_depth({mean: mean, standard_deviation: sd}, strata,{
            index: index,
            changed_by: this
        });
      } else {
        var depth = parseFloat(depth_element.attr("value"));
        this.model.set_soil_depth({value: depth}, strata, {
            index: index,
            changed_by: this
        });
      }
    },
    /**
     * Add a new set of depth measurements to the Geometry model if we now have
     * all depth measurements.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    maybe_update_surface_distance: function(event) {
      var target = $(d3.event.target);
      var id = target.attr("id").split("_");
      var index = parseInt(id[2]);
      var surface_distance = parseFloat(target.attr("value"));
      this.new_depths.surface_distance_from_cut_toe = surface_distance;
      if (this.have_all_measurements()) {
        var new_depth = this.new_depths.depth;
        var new_surface_distance =this.new_depths.surface_distance_from_cut_toe;
        this.new_depths = {depth: {}, surface_distance_from_cut_toe: undefined};
        this.model.set_soil_depth(new_depth, undefined, {
            surface_distance_from_cut_toe:
                new_surface_distance,
            index: index,
            changed_by: this
        });
      }
    },
    /**
     * Update surface distance for an existing set of measurements.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    update_surface_distance: function(event) {
      var target = $(d3.event.target);
      var id = target.attr("id").split("_");
      var index = parseInt(id[2]);
      var surface_distance = parseFloat(target.attr("value"));
      this.model.set_soil_depth(undefined, undefined, {index: index,
        surface_distance_from_cut_toe: surface_distance,
        changed_by: this
      });
    },
    /**
     * Update soil estimate for an existing set of measurements.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    update_soil_estimate: function(event) {
      var target = $(d3.event.target);
      var id = target.attr("id");
      if (id === "surface_distance_from_cut_toe") {
        this.model.set_soil_estimate({
          surface_distance_from_cut_toe: parseFloat(target.attr("value")),
          changed_by: this
        });
      } else {
        var field = id.split("_")[0];
        var index = parseInt(id.split("_")[1]);
        var depth_element = $("#" + field + "_" + index);
        var value = {};
        if (this.stochastic) {
          var value_raw = depth_element.attr("value");
          if (typeof(value_raw) === "string" && value_raw.length > 0) {
            value.mean = parseFloat(value_raw);
          }
          var sd_id = "#" + depth_element.attr("id") + "_sd";
          var sd_raw = $(sd_id).attr("value");
          if (typeof(sd_raw) === "string" && sd_raw.length > 0) {
            value.standard_deviation = parseFloat(sd_raw);
          }
        } else {
          value.value = parseFloat(target.attr("value"));
        }
        this.model.set_soil_estimate({
          field: field,
          index: index,
          value: value,
          changed_by: this
        });
      }
    },
    /**
     * Convenience function for building form.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @private
     * @param {d3.selection} table d3 selection from which the labels should be
     *                             selected
     * @param {integer} index Column for which the labels are required. Used to
     *                        determine if the labels are for the zeroth column.
     * @return {d3.selection} d3 selection containing the stochastic labels
     */
    get_tr_stochastic_labels: function(table, index) {
      var tr_stochastic_labels;
      if (typeof(index) !== undefined && index == 0) {
        tr_stochastic_labels = table.append("xhtml:tr")
          .attr("id", "stochastic_labels");
        tr_stochastic_labels.append("xhtml:th");
      } else {
        tr_stochastic_labels = table.select("#stochastic_labels");
      }
      return tr_stochastic_labels;
    },
    /**
     * Convenience function for building form.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @private
     * @param {d3.selection} table d3 selection from which the elements should
     *                             be selected
     * @param {integer} index Column for which the elements are required
     * @return {d3.selection} d3 selection containing the surface distance
     *                        elements
     */
    get_tr_surface_distance: function(table, index) {
      var tr_surface_distance;
      if (typeof(index) !== undefined && index == 0) {
        tr_surface_distance = table.append("xhtml:tr")
          .attr("id", "surface_distance");
        tr_surface_distance
          .append("xhtml:th")
            .style("min-width", "120px")
            .html("Surface distance (m)");
      } else {
        tr_surface_distance = table.select("#surface_distance");
      }
      return tr_surface_distance;
    },
    /**
     * Convenience function for building form
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @private
     * @param {d3.selection} table d3 selection from which the elements should
     *                             be selected
     * @param {integer} index Column for which the depth elements required. Used
     *                        to determine if the labels are for the zeroth
     *                        column.
     * @param {integer} j Strata index of the depth element to be retrieved
     * @return {d3.selection} d3 selection containing the depth elements
     */
    get_tr_depth: function(table, index, j) {
      var tr_depth;
      if (index == 0) {
        tr_depth = table.append("xhtml:tr")
          .attr("id", "depth_row_" + j);
        tr_depth
          .append("xhtml:th")
            .html("Depth (m)");
      } else {
        tr_depth = table.select("#depth_row_" + j);
      }
      return tr_depth;
    },
    /**
     * Convenience function, builds column for a new set of measurements
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @private
     * @param {d3.selection} table d3 selection to which the column should be
     *                             added
     * @param {integer} index Index of the column to be built
     * @param {integer} depths Number of depths to be displayed in the column
     */
    build_blank_column: function(table, index, depths) {
      var that = this;
      var tr_stochastic_labels;
      var tr_surface_distance = this.get_tr_surface_distance(table, index);
      var input_surface_distance = tr_surface_distance
        .append("xhtml:td")
          .append("xhtml:input");
      input_surface_distance
        .attr("id", "surface_section_" + index)
        .attr("class", "input-thin")
        .on("change", that.maybe_update_surface_distance);
      if (typeof(this.new_depths) !== "undefined" &&
          typeof(this.new_depths.surface_distance_from_cut_toe) !=="undefined"){
        input_surface_distance.attr("value",
            this.new_depths.surface_distance_from_cut_toe);
      } else {
        input_surface_distance.attr("value", "");
      }
      if (this.stochastic && depths > 0) {
        tr_stochastic_labels = this.get_tr_stochastic_labels(table, index);
        tr_stochastic_labels.append("xhtml:td").append("xhtml:label")
          .html("Mean");
        tr_stochastic_labels.append("xhtml:td").append("xhtml:label")
          .html("Sd");
      }
      for (var j = 0; j < depths; ++j) {
        var tr_depth = this.get_tr_depth(table, index, j);
        var input = tr_depth
          .append("xhtml:td").append("xhtml:input");
        if (this.stochastic) {
          input
              .attr("id", "depth_section_" + index + "_strata_" + j)
              .attr("class", "input-thin")
              .on("change", that.maybe_update_depth);
          if (typeof(this.new_depths) !== "undefined" &&
              typeof(this.new_depths.depth) !== "undefined" &&
              typeof(this.new_depths.depth.mean) !== "undefined" &&
              this.new_depths.depth.mean.hasOwnProperty("length")) {
            input.attr("value", this.new_depths.depth.mean[j]);
          } else {
            input.attr("value", "");
          }
          var input_sd = tr_depth.append("xhtml:td").append("xhtml:input");
          input_sd
              .attr("id", "depth_section_" + index + "_strata_" + j + "_sd")
              .attr("class", "input-thin")
              .on("change", that.maybe_update_depth);
          if (typeof(this.new_depths) !== "undefined" &&
              typeof(this.new_depths.depth) !== "undefined" &&
              typeof(this.new_depths.depth.standard_deviation) !=="undefined" &&
              this.new_depths.depth.standard_deviation.hasOwnProperty("length")) {
            input_sd.attr("value", this.new_depths.depth.standard_deviation[j]);
          } else {
            input_sd.attr("value", "");
          }
        } else {
          input
              .attr("id", "depth_section_" + index + "_strata_" + j)
              .attr("class", "input-thin")
              .on("change", that.maybe_update_depth);
          if (typeof(this.new_depths) !== "undefined" &&
              typeof(this.new_depths.depth) !== "undefined" &&
              typeof(this.new_depths.depth.value) !== "undefined" &&
              this.new_depths.depth.value.hasOwnProperty("length")) {
            input.attr("value", this.new_depths.depth.value[j]);
          } else {
            input.attr("value", "");
          }
        }
      }
    },
    /**
     * Build column for existing set of measurements
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @private
     * @param {d3.selection} table d3 selection to which the column should be
     *                             added
     * @param {integer} index Index of the column to be built
     * @param {object} section Depths to be displayed in the column
     * @param {object} section.surface_distance_from_cut_toe Surface distance
     *                                                       from datum for
     *                                                       these measurements
     */
    build_column: function(table, index, section) {
      var that = this;
      var tr_stochastic_labels;
      var tr_surface_distance = this.get_tr_surface_distance(table, index);
      tr_surface_distance
        .append("xhtml:td")
        .append("xhtml:input")
          .attr("id", "surface_section_" + index)
          .attr("class", "input-thin")
          .attr("value", parseFloat(Mossaic.utils.round(
              section.surface_distance_from_cut_toe.value, 3)))
          .on("change", that.update_surface_distance);

      var values = (this.stochastic && section.depth.mean)
          || section.depth.value;

      if (that.stochastic && values.length > 0) {
        tr_surface_distance.append("xhtml:td");
        tr_stochastic_labels = this.get_tr_stochastic_labels(table, index)
        tr_stochastic_labels.append("xhtml:td").append("xhtml:label")
          .html("Mean");
        tr_stochastic_labels.append("xhtml:td").append("xhtml:label")
          .html("Sd");
      }

      _.each(values, function(soil_depth, j) {
        var tr_depth = that.get_tr_depth(table, index, j);
        tr_depth
          .append("xhtml:td")
            .append("xhtml:input")
              .attr("id", "depth_section_" + index + "_strata_" + j)
              .attr("class", "input-thin")
              .attr("value", parseFloat(Mossaic.utils.round(soil_depth, 3)))
              .on("change", that.update_depth);
        if (that.stochastic) {
          var soil_depth_sd = section.depth.standard_deviation[j];
          tr_depth
            .append("xhtml:td")
              .append("xhtml:input")
                .attr("id", "depth_section_" + index + "_strata_" + j + "_sd")
                .attr("class", "input-thin")
                .attr("value", parseFloat(Mossaic.utils.round(soil_depth_sd, 3)))
                .on("change", that.update_depth);
        }
      })
    },
    /**
     * Handle change events triggered by the model. Only redraw if the
     * model was changed by a different view.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     * @param {Mossaic.model.Geometry} model Model that generated the form
     */
    handle_change: function(model) {
      this.stochastic = this.model.is_stochastic("soil_strata");
      if (model.changed_by != this) {
        this.render();
      } else {
        var old_geometry = this.model.previous("geometry");
        var geometry = this.model.get("geometry");
        var redraw = (typeof(old_geometry.soil_strata) !== "undefined" &&
            typeof(geometry.soil_strata) !== "undefined" &&
            old_geometry.soil_strata.length !== geometry.soil_strata.length);
        if (redraw) {
          this.render();
        }
      }
    },
    /**
     * Render the form.
     * Will be called implicitly by show or handle_change so should not need
     * to be called directly.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     */
    render: function() {
      if (this.hidden) {
        return;
      }
      var that = this;

      d3.select(this.form_id).remove();

      var div = d3.select(this.divname)
        .append("xhtml:div")
        .attr("id", this.form_id.slice(1));

      var form = d3.select(this.form_id);
      form.append("xhtml:hr");

      var select_soil_strata_row = form.append("xhtml:table")
          .attr("class", "wide")
          .append("xhtml:tr");
      select_soil_strata_row.append("xhtml:th")
        .style("min-width", "120px")
        .append("xhtml:label")
          .style("float", "left")
          .html("Soil strata definition");
      var strata_type = select_soil_strata_row.append("xhtml:td")
        .append("xhtml:select")
          .attr("id", "strata_type")
          .on("change", function(event) {
            var type = $("#strata_type option:selected").val();
            var geometry = _.clone(that.model.get("geometry"));
            if (type === "Estimated") {
              geometry.soil_strata = that.model.get_blank_soil_estimate(that.stochastic);
            } else {
              geometry.soil_strata = [];
            }
            that.layers.changed_by = that;
            that.layers.reset({});
            that.model.changed_by = that;
            that.model.set({geometry: geometry});
          });
      var options = {
        Estimated: false,
        Measured: false
      }
      if (this.model.is_soil_known()) {
        options.Measured = true;
      } else {
        options.Estimated = true;
      }
      _.each(_.keys(options), function(key) {
        var new_option = strata_type.append("xhtml:option")
          .html(key);
        if (options[key]) {
          new_option.attr("selected", true);
        }
      });

      form.append("xhtml:br");

      // Only allow stochastic input to be enabled for questa geometries
      if (this.model.mode === "questa") {
        form.append("xhtml:input")
          .attr("id", "soil_depths_stochastic")
          .attr("type", "checkbox")
          .attr("checked", this.stochastic && "checked" || null)
          .on("change", this.flip_stochastic);
        form.append("xhtml:label")
          .attr("for", "soil_depths_stochastic")
          .html("Enable stochastic depths");
        form.append("xhtml:br");
      }

      var table = form.append("xhtml:table")
          .attr("class", "wide");
      if (this.model.is_soil_known()) {
        var sections = this.model.get("geometry").soil_strata;
        var number_of_sections = 0;
        this.number_of_measurements = this.layers.length - 1;
        if (typeof(sections) !== "undefined" &&
            typeof(sections[0]) !== "undefined") {
          number_of_sections = sections.length;
        }
        var that = this;
        _.each(sections, function(section, i) {
          that.build_column(table, i, section);
        });
        that.build_blank_column(table, number_of_sections,
            that.number_of_measurements);
        // Now fix tabindex so tabbing goes by column not row
        var tabindex = 1;
        _.each(_.range(0, number_of_sections + 1), function(col) {
          $("#surface_section_" + col).attr("tabindex", tabindex++);
          _.each(_.range(0, that.number_of_measurements), function(row) {
            $("#depth_section_" + col + "_strata_" + row).
                attr("tabindex", tabindex++);
            if (that.stochastic) {
              $("#depth_section_" + col + "_strata_" + row + "_sd").
                  attr("tabindex", tabindex++);
            }
          });
        });
      } else {
        var strata = this.model.get("geometry").soil_strata;
        var surface_distance_obj = (strata &&
          strata.surface_distance_from_cut_toe &&
          typeof(strata.surface_distance_from_cut_toe.value) !== "undefined" &&
          strata.surface_distance_from_cut_toe) || {};
        var surface_distance = surface_distance_obj.value;
        var depths, angles, depths_sd, angles_sd;
        if (this.stochastic) {
          var depths = (strata && strata.interfaces && strata.interfaces.depth &&
            strata.interfaces.depth.mean) || "";
          var angles = (strata && strata.interfaces && strata.interfaces.angle &&
            strata.interfaces.angle.mean) || "";
          var depths_sd = (strata && strata.interfaces && strata.interfaces.depth &&
            strata.interfaces.depth.standard_deviation) || "";
          var angles_sd = (strata && strata.interfaces && strata.interfaces.angle &&
            strata.interfaces.angle.standard_deviation) || "";
        } else {
          var depths = (strata && strata.interfaces && strata.interfaces.depth &&
            strata.interfaces.depth.value) || "";
          var angles = (strata && strata.interfaces && strata.interfaces.angle &&
            strata.interfaces.angle.value) || "";
        }
        var tr_surface_distance = table.append("xhtml:tr");
        tr_surface_distance.append("xhtml:th")
          .style("min-width", "120px")
          .append("xhtml:label")
            .html("Surface distance (m)");
        tr_surface_distance.append("xhtml:td")
          .append("xhtml:input")
            .attr("id", "surface_distance_from_cut_toe")
            .attr("class", "input-thin")
            .attr("value", surface_distance)
            .on("change", this.update_soil_estimate);

        if (this.layers.length > 0 && this.stochastic) {
          var append_mean_sd = function(tr) {
            tr.append("xhtml:td").append("xhtml:label")
              .html("Mean");
            tr.append("xhtml:td").append("xhtml:label")
              .html("Sd");
          };
          var tr_stochastic_labels = table.append("xhtml:tr");
          tr_stochastic_labels.append("xhtml:th");
          append_mean_sd(tr_stochastic_labels);
          tr_stochastic_labels.append("xhtml:td");
          append_mean_sd(tr_stochastic_labels);
        }

        _.each(_.range(0, this.layers.length - 1), function(i) {
          var depth = depths[i];
          var angle = angles[i];
          if (typeof(depth) === "undefined") {
            depth = "";
          }
          if (typeof(angle) === "undefined") {
            angle = "";
          }
          if (that.stochastic) {
            var depth_sd = depths_sd[i];
            var angle_sd = angles_sd[i];
            if (typeof(depth_sd) === "undefined") {
              depth_sd = "";
            }
            if (typeof(angle_sd) === "undefined") {
              angle_sd = "";
            }
          }
          var new_tr = table.append("xhtml:tr");
          new_tr.append("xhtml:th")
            .append("xhtml:label")
              .html("Depth (m)");
          new_tr.append("xhtml:td")
            .append("xhtml:input")
              .attr("id", "depth_" + i)
              .attr("class", "input-thin")
              .attr("value", depth)
              .on("change", that.update_soil_estimate);
          if (that.stochastic) {
            new_tr.append("xhtml:td")
              .append("xhtml:input")
                .attr("id", "depth_" + i + "_sd")
                .attr("class", "input-thin")
                .attr("value", depth_sd)
                .on("change", that.update_soil_estimate);
          }
          new_tr.append("xhtml:th")
            .style("width", "auto")
            .append("xhtml:label")
              .html("Angle");
          new_tr.append("xhtml:td")
            .append("xhtml:input")
              .attr("id", "angle_" + i)
              .attr("class", "input-thin")
              .attr("value", angle)
              .on("change", that.update_soil_estimate);
          if (that.stochastic) {
            new_tr.append("xhtml:td")
              .append("xhtml:input")
                .attr("id", "angle_" + i + "_sd")
                .attr("class", "input-thin")
                .attr("value", angle_sd)
                .on("change", that.update_soil_estimate);
          }
        });
      }
    }
  });
})();