/**
 * @file Define the form for displaying Mossaic.models.Meta data
 */
(function() {
  /**
   * Backbone View for slope metadata represented by Mossaic.models.Meta which
   * displays values on a form.
   * @constructor
   * @extends Mossaic.forms.Basic
   */
  Mossaic.forms.Datum = Mossaic.forms.Basic.extend({
    /**
     * Template used to create the form
     * @memberof Mossaic.forms.Datum
     * @instance
     * @type string
     */
    template: '<div id="{{form_id}}" class="mossaic-form"><table class="">\
      <tr><td>Location</td></tr>\
      <tr>\
        <td><label>Northing</label></td>\
        <td><input id="lat" class="input-thin" value="{{lat}}"></input></td>\
      </tr>\
      <tr>\
        <td><label>Easting</label></td>\
        <td><input id="long" class="input-thin" value="{{long}}"></input></td>\
      </tr>\
      <tr>\
        <td><label>Slope breadth (m)</label></td>\
        <td><input id="breadth" class="input-thin" value="{{breadth}}" /></td>\
      </tr>\
      <tr>\
        <td><label>Road link start</label></td>\
        <td><input id="road_link_from" class="input-thin" value="{{road_link_from}}" /></td>\
      </tr>\
      <tr>\
        <td><label>Road link end</label></td>\
        <td><input id="road_link_to" class="input-thin" value="{{road_link_to}}" /></td>\
      </tr>\
      </table></div>',
    /**
     * Initialize the form.
     * Called implicitly with "new Mossaic.forms.Datum".
     * @memberof Mossaic.forms.Datum
     * @instance
     * @param {Mossaic.models.Meta} model Model to be represented
     *                                    by this form
     * @param {object} options Hash of options
     * @param {string} options.divname jQuery selector for div to which this
     *                                 form should be appended
     * @param {boolean} options.readonly Set to true if model data should not
     *                                   be editable on the form
     */
    initialize: function(model, options) {
      Mossaic.forms.Basic.prototype.initialize.apply(this, [model, options]);
      this.divname = options.divname || "#meta_form";
      this.form_id = this.divname + "_meta";
      this.readonly = options.readonly;
    },
    /**
     * Update the location field in the model
     * @memberof Mossaic.forms.Datum
     * @instance
     * @param {String} key The location field to be updated (lat or long)
     * @param {float} value The value to replace location[key]
     */
    update_location: function(key, value) {
      var location = _.clone(this.model.get("location"));
      if (typeof(location) === "undefined") {
        location = {};
      }
      location[key] = value;
      this.model.changed_by = this;
      this.model.set({location: location});
    },
    /**
     * Update the breadth field in the model
     * @memberof Mossaic.forms.Datum
     * @instance
     * @param {object} event The event that triggered the function
     */
    update_breadth: function(event) {
      var val = parseFloat($("#breadth").val());
      if (!Mossaic.utils.validate_number(val,
          "Please enter a positive number for the slope breadth",
          event.target,
          [function(value) { return value > 0; }])) {
        return;
      }
      var breadth = _.clone(this.model.get("breadth"));
      breadth.value = val;
      this.model.changed_by = this;
      this.model.set({breadth: breadth});
    },
    /**
     * Update the road_link field in the model
     * @memberof Mossaic.forms.Datum
     * @instance
     * @param {int} value The road link id
     * @param {int} index The index of the link (0: from, 1: to)
     */
    update_road_link: function(value, index) {
      var road_link = _.clone(this.model.get("road_link"));
      road_link[index] = value;
      this.model.changed_by = this;
      this.model.set({road_link: road_link});
    },
    /**
     * Handle change events triggered by the model. Only redraw if the
     * model was changed by a different view.
     * @memberof Mossaic.forms.Datum
     * @instance
     */
    handle_change: function() {
      // Change may have originated from this view or somewhere else. So check
      // called_by attribute. We only want to render if the model was changed
      // by a different view as this view will already be up-to-date.
      if (this.model.changed_by != this) {
        this.render();
      }
    },
    /**
     * Render the form.
     * Will be called implicitly by show or handle_change so should not need
     * to be called directly.
     * @memberof Mossaic.forms.Datum
     * @instance
     */
    render: function() {
      if (this.hidden) {
        return;
      }
      var that = this;
      var location = this.model.get("location") || {};
      var breadth = this.model.get("breadth") || {};
      var road_link = this.model.get("road_link") || [];
      $(this.form_id).remove();
      $(this.divname).append($.mustache(this.template, {
        form_id: this.form_id.split("#")[1],
        lat: location.lat,
        "long": location["long"],
        breadth: breadth.value,
        road_link_from: road_link[0],
        road_link_to: road_link[1]
      }));
      if (this.readonly) {
        $(this.form_id + " input").attr("disabled", "disabled");
      } else {
        $("#lat").change(function(event) {
          var new_value = parseFloat($("#lat").val());
          if (!Mossaic.utils.validate_number(new_value,
              "Please enter a numeric value for latitude", event.target)) {
            return;
          }
          that.update_location("lat", new_value);
        });
        $("#long").change(function(event) {
          var new_value = parseFloat($("#long").val());
          if (!Mossaic.utils.validate_number(new_value,
              "Please enter a numeric value for longitude", event.target)) {
            return;
          }
          that.update_location("long", new_value);
        });
        $("#breadth").change(that.update_breadth);
        $("#road_link_from").change(function(event) {
          var new_value = parseInt($("#road_link_from").val(), 10);
          if (!Mossaic.utils.validate_number(new_value,
              "Please enter a non-negative integer for the start road link " +
              "number",
              event.target,
              [function(val) { return val >= 0; }])) {
            return;
          }
          that.update_road_link(new_value, 0);
        });
        $("#road_link_to").change(function(event) {
          var new_value = parseInt($("#road_link_to").val(), 10);
          if (!Mossaic.utils.validate_number(new_value,
              "Please enter a non-negative integer for the end road link " +
              "number",
              event.target,
              [function(val) { return val >= 0; }])) {
            return;
          }
          that.update_road_link(new_value, 1);
        });
      }
    }
  });
})();