/**
 * @file Define the form for displaying water table depths for a
 *       Mossaic.models.Geometry model.
 */
(function() {
  /**
   * Backbone View which displays the water table in a Mossaic.models.Geometry
   * model on a form.
   *
   * Depth measurements are located by the surface distance from the datum.
   * Depth measurements for a particular surface distance are displayed in a
   * single column.
   * An empty column is displayed for adding a new group of depth measurements.
   * When this column is filled in the new depth is added to the model.
   *
   * This is a legacy view which builds the form using d3.js rather than the
   * more sensible jQuery/templating approach.
   * @constructor
   * @extends Mossaic.forms.Basic
   */
  Mossaic.forms.WaterTable = Mossaic.forms.Basic.extend({
    /**
     * @memberof Mossaic.forms.WaterTable
     * @instance
     * @param {Mossaic.models.Geometry} model Geometry model containing the
     *                                        water table measurements to be
     *                                        rendered
     * @param {object} options Hash of options
     * @param {string} options.divname jQuery selector for div to which this
     *                                 form should be appended
     * @param {boolean} options.stochastic True if the form should default
     *                                     to showing stochastic fields
     */
    initialize: function(model, options) {
      Mossaic.forms.Basic.prototype.initialize.apply(this, [model, options]);
      this.divname = options.divname || "#water_table_form";
      this.form_id = this.divname + "_water_table";
      this.new_depth = {depth: {}, surface_distance_from_cut_toe: {}};
      if (typeof(options.stochastic) !== "undefined") {
        this.stochastic = options.stochastic;
      } else {
        this.stochastic = this.model.is_stochastic("water_table");
      }
      this.model.default_stochastic = this.stochastic;
    },
    /**
     * Set the geometry model to be represented by the form.
     * @memberof Mossaic.forms.WaterTable
     * @instance
     * @param {Mossaic.models.Geometry} geometry Model to be represented
     *                                           by this form
     */
    set_geometry: function(geometry) {
      if (typeof(this.model) !== "undefined" &&
          typeof(this.model.unbind) === "function") {
        this.model.unbind("change");
      }
      this.model = geometry;
      this.stochastic = this.model.is_stochastic("water_table");
      this.model.bind("change", this.handle_change);
    },
    /**
     * If form is stochastic, make it deterministic. If it's deterministic,
     * make it stochastic.
     * @memberof Mossaic.forms.WaterTabls
     * @instance
     */
    flip_stochastic: function() {
      var that = this;
      var to_convert = ["depth"];
      this.stochastic = !this.stochastic;
      this.model.default_stochastic = this.stochastic;
      var geometry = _.clone(this.model.get("geometry"));
      var water_table = _.clone(geometry.water_table);
      var all_depths = water_table.concat(this.new_depth);
      _.each(all_depths, function(_measurement, i) {
        var measurement = _.clone(_measurement);
        if (that.stochastic) {
          _.each(to_convert, function(field) {
            var parameter = _.clone(measurement[field]);
            parameter.mean = parameter.value;
            parameter.standard_deviation = 0;
            delete parameter.value;
            measurement[field] = parameter;
          });
        } else {
          _.each(to_convert, function(field) {
            var parameter = _.clone(measurement[field]);
            parameter.value = parameter.mean;
            delete parameter.mean;
            delete parameter.standard_deviation;
            measurement[field] = parameter;
          });
        }
        if (i < water_table.length) {
          water_table[i] = measurement;
        }
      });
      geometry.water_table = water_table;
      this.model.changed_by = this;
      this.model.set({
        geometry: geometry
      });
      this.render();
    },
    /**
     * Determine whether all depth measurements are present
     * @memberof Mossaic.forms.WaterTable
     * @instance
     * @private
     * @return true if all depth measurements & surface distance exist, or false
     */
    have_all_measurements: function() {
      return (typeof(this.new_depth.surface_distance_from_cut_toe) !==
          "undefined") &&
        (typeof(this.new_depth.depth) !== "undefined" &&
          ("value" in this.new_depth.depth ||
            ("mean" in this.new_depth.depth &&
            "standard_deviation" in this.new_depth.depth)));
    },
    /**
     * Add a new depth measurement if we have both surface distance and depth
     * @memberof Mossaic.forms.WaterTable
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    maybe_update_depth: function(event) {
      var index = parseInt($(d3.event.target).attr("id").split("_")[1]);
      var depth_element = $("#depth_" + index);
      var raw_depth = depth_element.attr("value");
      var depth;
      if (typeof(raw_depth) === "string" && raw_depth.length > 0) {
        depth = parseFloat(raw_depth);
      }
      else {
        depth = 0;
      }
      if (this.stochastic) {
        this.new_depth.depth.mean = depth;
        var sd_id = "#" + depth_element.attr("id") + "_sd";
        var sd_raw = $(sd_id).attr("value");
        if (typeof(sd_raw) === "string" && sd_raw.length > 0) {
          this.new_depth.depth.standard_deviation = parseFloat(sd_raw);
        } else {
          this.new_depth.depth.standard_deviation = 0;
        }
      } else {
        this.new_depth.depth.value = depth;
      }
      if (this.have_all_measurements()) {
        var new_depth = this.new_depth.depth;
        var new_surface_distance =this.new_depth.surface_distance_from_cut_toe;
        this.new_depth = {depth: {}, surface_distance_from_cut_toe: undefined};
        this.model.set_water_depth(new_depth, {
            surface_distance_from_cut_toe:
                new_surface_distance,
            index: index,
            changed_by: this
        });
      }
    },
    /**
     * Update an existing depth measurement
     * @memberof Mossaic.forms.WaterTable
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    update_depth: function(event) {
      var index = parseInt($(d3.event.target).attr("id").split("_")[1]);
      var depth_element = $("#depth_" + index);
      if (this.stochastic) {
        var mean = parseFloat(depth_element.attr("value"));
        var sd_id = "#" + depth_element.attr("id") + "_sd";
        var sd = parseFloat($(sd_id).attr("value"));
        this.model.set_water_depth({mean: mean, standard_deviation: sd}, {
          index: index,
          changed_by: this
        })
      } else {
        var depth = parseFloat(depth_element.attr("value"));
        this.model.set_water_depth({value: depth}, {
          index: index,
          changed_by: this
        });
      }
    },
    /**
     * Add a new depth measurement if we have both surface distance and depth
     * @memberof Mossaic.forms.WaterTable
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    maybe_update_surface_distance: function(event) {
      var id_tokens = $(d3.event.target).attr("id").split("_");
      var index = parseInt(id_tokens[id_tokens.length - 1]);
      var surface_distance = parseFloat($(d3.event.target).attr("value"));
      this.new_depth.surface_distance_from_cut_toe = surface_distance;
      if (this.have_all_measurements()) {
        var new_depth = this.new_depth.depth;
        var new_surface_distance =this.new_depth.surface_distance_from_cut_toe;
        this.new_depth = {depth: {}, surface_distance_from_cut_toe: undefined};
        this.model.set_water_depth(new_depth, {
            surface_distance_from_cut_toe:
                new_surface_distance,
            index: index,
            changed_by: this
        });
      }
    },
    /**
     * Update surface distance for an existing depth measurement
     * @memberof Mossaic.forms.WaterTable
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    update_surface_distance: function(event) {
      var id_tokens = $(d3.event.target).attr("id").split("_");
      var index = parseInt(id_tokens[id_tokens.length - 1]);
      var surface_distance = parseFloat($(d3.event.target).attr("value"));
      this.model.set_water_depth(undefined, {index: index,
          surface_distance_from_cut_toe: surface_distance,
          changed_by: this
      });
    },
    /**
     * Convenience function to build a depth form column
     * @memberof Mossaic.forms.WaterTable
     * @instance
     * @private
     * @param {array} rows Array of rows to be added to the column
     * @param {object} options Hash of options
     * @param {integer} options.index Index of the column to be built
     */
    build_column: function(rows, options) {
      var that = this;
      _.each(rows, function(row) {
        var value;
        var stochastic_allowed = row.stochastic_allowed;
        var td = row.tr.append("xhtml:td")
          .style("min-width", "100px");
        var input = td
          .append("xhtml:input");
        input
            .attr("id", row.id_prefix + options.index)
            .attr("class", "input-thin")
            .on("change", row.change);
        if (typeof(row.value) !== "undefined") {
          if (that.stochastic && stochastic_allowed) {
            value = row.value.mean;
          } else {
            value = row.value.value;
          }
        }
        if (!isNaN(value)) {
          input
              .attr("value",
                  parseFloat(Mossaic.utils.round(
                    value, 3)))
        }
        if (that.stochastic && stochastic_allowed) {
          var input_sd = td.append("xhtml:input")
            .attr("id", row.id_prefix + options.index + "_sd")
            .attr("class", "input-thin")
            .on("change", row.change)
          if (!isNaN(row.value.standard_deviation)) {
            input_sd
              .attr("value",
                  parseFloat(Mossaic.utils.round(
                      row.value.standard_deviation, 3)));
          }
        }
      });
    },
    /**
     * Handle change events triggered by the model. Only redraw if the
     * model was changed by a different view.
     * @memberof Mossaic.forms.WaterTable
     * @instance
     */
    handle_change: function() {
      this.stochastic = this.model.is_stochastic("water_table");
      if (this.model.changed_by != this) {
        this.render();
      } else {
        var old_geometry = this.model.previous("geometry");
        var geometry = this.model.get("geometry");
        var redraw =
            (("water_table" in old_geometry && !("water_table" in geometry)) ||
             ("water_table" in geometry && !("water_table" in old_geometry)) ||
             (("water_table" in old_geometry && "water_table" in geometry) &&
                old_geometry.water_table.length !== geometry.water_table.length)
            );
        if (redraw) {
          this.render();
        }
      }
    },
    /**
     * Render the form.
     * Will be called implicitly by show or handle_change so should not need
     * to be called directly.
     * @memberof Mossaic.forms.WaterTable
     * @instance
     */
    render: function() {
      if (this.hidden) {
        return;
      }
      var that = this;

      d3.select(this.form_id).remove();

      var div = d3.select(this.divname)
        .append("xhtml:div")
        .attr("id", this.form_id.slice(1));

      var form = d3.select(this.form_id);

      // Only allow stochastic input to be enabled for questa geometries
      if (this.model.mode === "questa") {
        form.append("xhtml:input")
          .attr("id", "water_table_stochastic")
          .attr("type", "checkbox")
          .attr("checked", this.stochastic && "checked" || null)
          .on("change", this.flip_stochastic);
        form.append("xhtml:label")
          .attr("for", "water_table_stochastic")
          .html("Enable stochastic input");
        form.append("xhtml:br");
      }

      var table = form.append("xhtml:table")
        .attr("class", "wide");
      var that = this;
      var tr_surface_distance = table.append("xhtml:tr");
      tr_surface_distance.append("xhtml:th")
        .style("min-width", "120px")
        .html("Surface distance (m)");
      var tr_stochastic_labels = table.append("xhtml:tr");
      tr_stochastic_labels.append("xhtml:th");
      var tr_depth = table.append("xhtml:tr");
      tr_depth.append("xhtml:th")
        .html("Depth (m)");
      var water_table = this.model.get("geometry").water_table;
      var number_of_sections = 0;
      if (typeof(water_table) !== "undefined" &&
          typeof(water_table.length) !== "undefined") {
        number_of_sections = water_table.length;
      }
      _.each(water_table, function(section, i) {
        that.build_column(
            [{
                tr: tr_surface_distance,
                change: that.update_surface_distance,
                id_prefix: "surface_distance_",
                value: section.surface_distance_from_cut_toe,
                stochastic_allowed: false
            },
            {
                tr: tr_depth,
                change: that.update_depth,
                id_prefix: "depth_",
                value: section.depth,
                stochastic_allowed: true
            }],
            {index: i}
        );
        if (that.stochastic) {
          var td_stochastic_labels = tr_stochastic_labels.append("xhtml:td");
          td_stochastic_labels.append("xhtml:label")
            .style("width", "50px")
            .style("float", "left")
            .html("Mean");
          td_stochastic_labels.append("xhtml:label")
            .style("width", "50px")
            .style("float", "left")
            .html("Sd");
        }
        form.append("xhtml:br");
      });
      this.build_column(
          [{
              tr: tr_surface_distance,
              change: that.maybe_update_surface_distance,
              id_prefix: "surface_distance_",
              value: this.new_depth.surface_distance_from_cut_toe,
              stochastic_allowed: false
          },
          {
              tr: tr_depth,
              change: that.maybe_update_depth,
              id_prefix: "depth_",
              value: this.new_depth.depth,
              stochastic_allowed: true
          }],
          {index: number_of_sections}
      );
      if (this.stochastic) {
        var td_stochastic_labels = tr_stochastic_labels.append("xhtml:td");
        td_stochastic_labels.append("xhtml:label")
          .style("width", "50px")
          .style("float", "left")
          .html("Mean");
        td_stochastic_labels.append("xhtml:label")
          .style("width", "50px")
          .style("float", "left")
          .html("Sd");
      }
    },
    set_stochastic_depths: function(stochastic) {
      if (stochastic !== this.stochastic) {
        this.flip_stochastic();
      }
    }
  });
})();