/**
 * @file Defines a widget for analysing slope cells data
 */
(function() {
  /**
   * Create a slope_cells_analyser widget. Uses the slope_cells view to
   * retrieve slope cells data for a given task and job.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {string} options.divname CSS selector of the div to append to
   * @param {Mossaic.widgets.ls} options.parent ls widget that sets the parent
   *                                            dataset (most likely a Task)
   * @param {Mossaic.widgets.ls} options.jobs_browser ls widget that sets the
   *                                                  job dataset
   * @param {boolean} options.active Set to false to make inactive (and hidden)
   */
  Mossaic.widgets.slope_cells_analyser = function(options) {
    var divname = options.divname;
    var parent = options.parent;
    var parent_id = parent.get_selected().id;  // Probably doesn't exist yet
    var active = options.active;
    var variables = [
      {
        id: "moisture_content",
        name: "Moisture content",
        unit: "m3/m3",
        color: {
          min: "lightcyan",
          max: "midnightblue"
        }
      },
      {
        id: "pressure_head",
        name: "Pore pressure",
        unit: "m",
        color: {
          min: "lightblue",
          max: "firebrick"
        }
      }
    ];
    var selected_variable = variables[0];
    var jobs_browser = options.jobs_browser;
    var job_id = jobs_browser.get_selected().id;
    var current_time = new Backbone.Model({value: 0});

    var collection = new Backbone.Collection();
    collection.view = "slope_cells";
    collection.ddocName = "mining";
    collection.doreduce = false;
    collection.success = function(result) {
      return _.map(result.rows, function(row) {
        return {id: row.id, value: row.value};
      });
    };
    collection.startkey = [parent_id, selected_variable.id, job_id];
    collection.endkey = [parent_id, selected_variable.id, job_id, {}];

    var fos_collection = new Backbone.Collection();
    fos_collection.view = "fos";
    fos_collection.ddocName = "mining";
    fos_collection.doreduce = false;
    fos_collection.success = function(result) {
      return _.map(result.rows, function(row) {
        return {fos: row.value, time: row.key[2]};
      });
    };
    fos_collection.startkey = [parent_id, job_id];
    fos_collection.endkey = [parent_id, job_id, {}];

    var slip_collection = new Backbone.Collection();
    slip_collection.view = "slip_circle";
    slip_collection.ddocName = "mining";
    slip_collection.doreduce = false;
    slip_collection.success = function(result) {
      return _.map(result.rows, function(row) {
        return row.value;
      });
    }
    slip_collection.startkey = [parent_id, job_id];
    slip_collection.endkey = [parent_id, job_id, {}];

    // Timestep View to become factor-of-safety-based timestep slider
    var TimestepView = Backbone.View.extend({
      initialize: function(collection, options) {
        _.bindAll(this);
        this.collection = fos_collection;
        this.collection.bind("reset", this.render);
        this.divname = options.divname;
      },
      redraw: function() {
        this.chart.selectAll("#current_time")
          .attr("x1", this.x(current_time.get("value")))
          .attr("x2", this.x(current_time.get("value")))
          .attr("y1", this.y(this.height))
          .attr("y2", this.y(0));
      },
      /**
       * TODO: Put me somewhere generic - I am duplicate code (see slope_graphic.js 279)
       * Get the coordinates of an event that occured in the screen coordinate
       * system in the svg coordinate system.
       *
       * @memberof Mossaic.widgets.slope_cells_analyser
       * @instance
       * @param {object} event D3 event object containing screen coordinates
       * @return Coordinates of event in svg space
       */
      screen_to_svg_transform: function(event) {
        // Get current transform matrix (why the [0][0] ?)
        var ctm = d3.select(event.target)[0][0].getScreenCTM();
        var point = this.svg[0][0].createSVGPoint();
        point.x = event.clientX;
        point.y = event.clientY;
        return transformed_point = point.matrixTransform(ctm.inverse());
      },
      show_placeholder: function() {
        this.collection.reset([]);
      },
      render: function() {
        var that = this;
        if (this.svg) {
          this.svg.remove();
        }
        if (!active) {
          return;
        }
        var div = d3.select(this.divname);
        this.width = $(this.divname).width();
        this.height = $(document).height() * 0.2;

        if (typeof(this.collection.at(0)) === "undefined") {
          return;
        }
        var margin = {
          x: 40,
          y: 40
        };

        this.svg = div.append("svg:svg")
          .attr("class", "chart time_step_viewer")
          .attr("width", this.width)
          .attr("height", this.height);
        this.chart = this.svg.append("svg:g");

        var data = this.collection.toJSON();
        var min_fos = _.min(data, function(d) {
          return d.fos;
        }).fos;
        var max_fos = _.max(data, function(d) {
          return d.fos;
        }).fos;
        // CouchDB view is sorted, so can just use first and last to get time
        var min_time = data[0].time;
        var max_time = _.last(data).time;

        this.x = d3.scale.linear()
          .domain([min_time, max_time])
          .range([0 + margin.x, this.width - margin.x]);
        this.y = d3.scale.linear()
          .domain([min_fos, max_fos])
          .range([this.height - margin.y, 0 + margin.y]);

        this.chart.append("svg:line")
          .attr("x1", 0 + margin.x)
          .attr("x2", 0 + margin.x)
          .attr("y1", 0 + margin.y)
          .attr("y2", this.height - margin.y);
        this.chart.append("svg:line")
          .attr("x1", this.width - margin.x)
          .attr("x2", this.width - margin.x)
          .attr("y1", 0 + margin.y)
          .attr("y2", this.height - margin.y);
        this.chart.append("svg:line")
          .attr("x1", 0 + margin.x)
          .attr("x2", this.width - margin.x)
          .attr("y1", this.height - margin.y)
          .attr("y2", this.height - margin.y);

        // Add fos ticks
        this.chart.selectAll(".fos_tick")
          .data(this.y.ticks(5))
          .enter().append("svg:line")
            .attr("class", "fos_tick noselect")
            .attr("x1", this.width - margin.x)
            .attr("x2", this.width - margin.x + 5)
            .attr("y1", function(d) {
              return that.y(d);
            })
            .attr("y2", function(d) {
              return that.y(d);
            });

        // Add labels for fos
        this.chart.selectAll(".fos_label")
          .data(this.y.ticks(5))
          .enter().append("svg:text")
            .attr("class", "fos_label noselect")
            .text(String)
            .attr("x", this.width - margin.x + 15)
            .attr("y", function(d) {
              return that.y(d);
            })
            .attr("text-anchor", "middle")
            .attr("dy", 4);

        // Add time step ticks
        this.chart.selectAll(".time_tick")
          .data(this.x.ticks(5))
          .enter().append("svg:line")
            .attr("class", "time_tick noselect")
            .attr("x1", function(d) {
              return that.x(d);
            })
            .attr("x2", function(d) {
              return that.x(d);
            })
            .attr("y1", this.height - margin.y)
            .attr("y2", this.height - margin.y + 5);

        // Add time step labels
        this.chart.selectAll(".time_label")
          .data(this.x.ticks(5))
          .enter().append("svg:text")
            .attr("class", "time_label noselect")
            .text(String)
            .attr("x", function(d) {
              return that.x(d);
            })
            .attr("y", this.height - margin.y + 15)
            .attr("text-anchor", "middle")
            .attr("dy", 4);

        var fos_title = {
          x: this.width - 10,
          y: this.height / 4
        };
        this.chart.append("svg:text")
          .attr("class", "fos_title noselect")
          .attr("x", fos_title.x)
          .attr("y", fos_title.y)
          .text("Factor of safety")
          .attr("transform", "rotate(90, " + fos_title.x + ", " +
            fos_title.y + ")");

        this.chart.append("svg:text")
          .attr("class", "noselect")
          .attr("x", this.width / 2)
          .attr("y", this.height - (margin.y * 0.2))
          .text("Time step");

        this.chart.selectAll(".fos_point")
          .data(data)
          .enter()
            .append("svg:line")
              .attr("class", "fos_single")
              .attr("x1", function(d, i) {
                if (i > 0) {
                  return that.x(data[i - 1].time);
                } else {
                  return that.x(d.time);
                }
              })
              .attr("y1", function(d, i) {
                if (i > 0) {
                  return that.y(data[i - 1].fos);
                } else {
                  return that.y(d.fos);
                }
              })
              .attr("x2", function(d, i) {
                return that.x(d.time);
              })
              .attr("y2", function(d, i) {
                return that.y(d.fos);
              });

        this.chart.selectAll("#current_time")
          .data([current_time])
          .enter()
            .append("svg:line")
              .attr("id", "current_time")
              .attr("x1", this.x(current_time.get("value")))
              .attr("x2", this.x(current_time.get("value")))
              .attr("y1", this.height - margin.y)
              .attr("y2", 0 + margin.y);

        var mousedown = false;
        this.svg
          .on("mousedown", function(event) {
            var point = that.screen_to_svg_transform(d3.event);
            if (that.x.invert(point.x) < 0) {
              point.x = that.x(0);
            } else if (that.x.invert(point.x) > max_time) {
              point.x = that.x(max_time);
            }
            d3.select("#current_time")
              .attr("x1", point.x)
              .attr("x2", point.x);
            current_time.set({value: Math.round(that.x.invert(point.x))});
            mousedown = true;
          })
          .on("mouseup", function() { mousedown = false; })
          .on("mousemove", function(event) {
            if (mousedown) {
              var point = that.screen_to_svg_transform(d3.event);
              if (that.x.invert(point.x) < 0) {
                point.x = that.x(0);
              } else if (that.x.invert(point.x) > max_time) {
                point.x = that.x(max_time);
              }
              d3.select("#current_time")
                .attr("x1", point.x)
                .attr("x2", point.x);
              current_time.set({value: Math.round(that.x.invert(point.x))});
            }
          })
      }
    });

    var View = Backbone.View.extend({
      initialize: function(collection, options) {
        _.bindAll(this);
        this.divname = options.divname;
        this.collection = collection;
        this.collection.bind("reset", this.render);
        Backbone.View.prototype.initialize.apply(this, [collection, options]);
      },
      render_variable_select: function(div) {
        var that = this;
        if (div.select("select").empty()) {
          if (div.select(".time_step_viewer").empty()) {
            this.select = div.append("xhtml:select");
          } else {
            this.select = div.insert("xhtml:select", ".time_step_viewer");
          }
          this.select
            .attr("id", "variable_select");
          _.each(variables, function(variable) {
            var option = that.select.append("xhtml:option")
              .attr("id", variable.id)
              .html(variable.name + " (" + variable.unit + ")");
            if (variable === selected_variable) {
              option.attr("selected", "selected");
            }
          });
          this.select.on("change", function() {
            var selected = $("#variable_select option:selected");
            selected_variable = _.find(variables, function(variable) {
              return variable.id === selected.attr("id");
            });
            collection.startkey[1] = selected_variable.id;
            collection.endkey[1] = selected_variable.id;
            collection.fetch();
          });
        }
      },
      render_job_output_link: function(div) {
        if (div.select(".time_step_viewer").empty()) {
          this.job_output_link = div.append("xhtml:div");
        } else {
          this.job_output_link = div.insert("xhtml:div", ".time_step_viewer");
        }
        var results_id = this.collection.at(0).id;
        var job_id = this.collection.startkey[2];
        this.job_output_link
          .html('<a href="../../' + results_id + "/output_" +
            job_id + ".tar.gz" + '">Get full job output (.tar.gz)</a>')
          .style("float", "right");
      },
      redraw: function() {
        var that = this;
        var data = this.flattened_values[current_time.get("value")];
        this.chart.selectAll("rect")
          .data(data)
            .attr("fill", function(d) {
              return that.color(d.value);
            });
      },
      draw_legend: function(chart, options) {
        var legend_width = options.width;
        var legend_line_length = options.line_length;
        var legend_line_height = options.line_height;
        var legend_margin_top = options.margin_top;
        var legend_x = options.legend_x;
        var legend_y = 0 + legend_margin_top;
        var legend_y_spacing = options.item_spacing;
        var legend_padding_y = options.padding_y;
        var legend_padding_x = options.padding_x;
        var legend_box_x = legend_x - legend_padding_x;
        var legend_box_y = legend_y - legend_padding_y;
        var increments = options.increments;
        var color_domain = options.color_domain;
        var color = options.color;
        var min = color_domain[0];
        var max = _.last(color_domain);
        var inc = (max - min) / increments;
        var data = _.map(_.range(increments), function(index) {
          return min + inc * index;
        });

        chart.append("svg:rect")
          .attr("class", "legend_box")
          .attr("x", legend_box_x)
          .attr("y", legend_box_y)
          .attr("width", legend_width + legend_padding_x)
          .attr("height", legend_box_y +
            (legend_y_spacing * (increments - 1)) +
            legend_padding_y);

        chart.selectAll(".legend_rect")
          .data(data)
          .enter()
            .append("svg:rect")
              .attr("class", "legend_rect")
              .attr("x", legend_x)
              .attr("y", function(d, i) {
                return legend_y + legend_y_spacing * i - legend_line_height / 2;
              })
              .attr("width", legend_line_length)
              .attr("height", legend_line_height)
              .style("fill", function(d) {
                return color(d);
              });

        chart.selectAll(".legend_text")
          .data(data)
          .enter()
            .append("svg:text")
              .attr("class", "legend_text")
              .attr("x", legend_x + legend_line_length + legend_padding_x)
              .attr("y", function(d, i) {
                return legend_y + (legend_y_spacing * i);
              })
              .attr("dominant-baseline", "central")
              .text(function(d) { return Mossaic.utils.round(d, 4); });
      },
      show_placeholder: function() {
        this.collection.reset([]);
      },
      render: function() {
        var that = this;
        if (this.svg) {
          this.svg.remove();
          this.select.remove();
        }
        if (this.job_output_link) {
          this.job_output_link.remove();
        }
        if (!active ||
            typeof(this.collection.at(current_time.get("value")))
              == "undefined") {
          return;
        }
        var div = d3.select(this.divname);
        this.width = $(this.divname).width();
        this.height = $(document).height() * 0.45;
        var values = this.collection;
        var size = values.size();
        if (size == 0) {
          return;
        }

        this.render_variable_select(div);
        this.render_job_output_link(div);

        var min_value, max_value, max_x, max_y;
        var max_time = size - 1;
        var min_time = 0;
        this.flattened_values = _.toArray(values.map(function(values_at) {
          return _.flatten(_.toArray(_.map(values_at.toJSON().value, function(values_y, i) {
            return _.map(values_y, function(value, j) {
              var x = parseInt(i);
              var y = parseInt(j);
              if (typeof(min_value) === "undefined" || (value < min_value)) {
                min_value = value;
              }
              if (typeof(max_value) === "undefined" || (value > max_value)) {
                max_value = value;
              }
              if (typeof(max_x) === "undefined" || (x > max_x)) {
                max_x = x;
              }
              if (typeof(max_y) === "undefined" || (y > max_y)) {
                max_y = y;
              }
              return {
                x: parseInt(i),
                y: parseInt(j),
                value: value
              };
            });
          })));
        }));

        var color_domain = [min_value, max_value];
        this.color = d3.scale.linear()
          .domain(color_domain)
          .range([selected_variable.color.min, selected_variable.color.max]);

        var margin = 30;

        this.x = d3.scale.linear()
          .domain([0, max_x])
          .range([0 + margin, this.width - margin]);
        this.y = d3.scale.linear()
          .domain([0, max_y])
          .range([this.height - margin, 0 + margin]);

        if (div.select(".time_step_viewer").empty()) {
          this.svg = div.append("svg:svg");
        } else {
          this.svg = div.insert("svg:svg", ".time_step_viewer");
        }

        this.svg
          .attr("class", "chart")
          .attr("width", this.width)
          .attr("height", this.height);
        this.chart = this.svg.append("svg:g");

        // Add x labels
        this.chart.selectAll(".slope_cell_label_x")
          .data(this.x.ticks(5))
          .enter().append("svg:text")
            .attr("class", "slope_cell_label_x noselect")
            .text(String)
            .attr("x", function(d) {
              return that.x(d);
            })
            .attr("y", that.height - margin + 20)
            .attr("text-anchor", "middle")
            .attr("dy", 4);

        // Add y labels
        this.chart.selectAll(".slope_cell_label_y")
          .data(this.y.ticks(5))
          .enter().append("svg:text")
            .attr("class", "slope_cell_label_y noselect")
            .text(String)
            .attr("x", margin - 15)
            .attr("y", function(d) {
              return that.y(d);
            })
            .attr("text-anchor", "start")
            .attr("dy", 15);

        this.chart.selectAll("rect")
          .data(this.flattened_values[current_time.get("value")])
            .enter()
            .append("svg:rect")
              .attr("id", function(d) {
                return "cell_" + d.x + "_" + d.y;
              })
              .attr("x", function(d) {
                return that.x(d.x);
              })
              .attr("y", function(d) {
                return that.y(d.y);
              })
              .attr("width", this.x(1) - this.x(0))
              .attr("height", Math.abs(this.y(1) - this.y(0)))
              .attr("fill", function(d) {
                return that.color(d.value);
              });

        var legend_width = 80;
        this.draw_legend(this.chart, {
          width: legend_width,
          margin_top: this.height * 0.1,
          item_spacing: 15,
          legend_x: this.width - legend_width - 20,
          padding_y: 15,
          padding_x: 10,
          color_domain: color_domain,
          color: this.color,
          increments: 8,
          line_length: this.x(1) - this.x(0),
          line_height: Math.abs(this.y(1) - this.y(0))
        });

        if (typeof(this.do_after_render) === "function") {
          this.do_after_render();
        }
      }
    });

    var SlipView = Backbone.View.extend({
      initialize: function(collection, options) {
        _.bindAll(this);
        this.divname = options.divname;
        this.collection = collection;
        this.collection.bind("reset", this.render);
        this.get_chart = options.get_chart;
        this.get_x = options.get_x;
        this.get_y = options.get_y;
        Backbone.View.prototype.initialize.apply(this, [collection, options]);
      },
      redraw: function() {
        var circle = this.collection.at(current_time.get("value"));
        var chart = this.get_chart();
        chart.select("#slip_circle")
          .attr("cx", this.get_x()(circle.get("x")))
          .attr("cy", this.get_y()(circle.get("y")))
          .attr("rx", Math.abs(this.get_x()(circle.get("r")) - this.get_x()(0)))
          .attr("ry", Math.abs(this.get_y()(circle.get("r")) - this.get_y()(0)));
      },
      render: function() {
        var circle = this.collection.at(current_time.get("value"));
        if (typeof(circle) === "undefined" || circle.length === 0) {
          return;
        }
        var chart = this.get_chart();
        chart.insert("svg:ellipse", ".legend_box")
          .attr("id", "slip_circle")
          .attr("cx", this.get_x()(circle.get("x")))
          .attr("cy", this.get_y()(circle.get("y")))
          .attr("rx", Math.abs(this.get_x()(circle.get("r")) - this.get_x()(0)))
          .attr("ry", Math.abs(this.get_y()(circle.get("r")) - this.get_y()(0)))
          .attr("fill", "none")
          .attr("stroke", "#000");
      }
    });

    var view = new View(collection, {
      divname: divname
    });
    var slip_view = new SlipView(slip_collection, {
      get_chart: function() {
        return view.chart;
      },
      get_x: function() {
        return view.x;
      },
      get_y: function() {
        return view.y;
      }
    });
    view.do_after_render = function() {
      slip_collection.fetch();
    };
    current_time.bind("change", view.redraw);
    current_time.bind("change", slip_view.redraw);

    var timestep_view = new TimestepView(fos_collection, {
      divname: divname
    });

    jobs_browser.register_listener(function() {
      view.show_placeholder();
      timestep_view.show_placeholder();
      var new_job_id = jobs_browser.get_selected().id;
      if ((new_job_id != job_id ||
          typeof(job_id) === "undefined") &&
          active) {
        job_id = new_job_id;
        parent_id = parent.get_selected().id;
        slip_collection.startkey = [parent_id, job_id];
        slip_collection.endkey = [parent_id, job_id, {}];
        collection.startkey = [parent_id, selected_variable.id, job_id];
        collection.endkey = [parent_id, selected_variable.id, job_id, {}];
        collection.fetch();
        fos_collection.startkey = [parent_id, job_id];
        fos_collection.endkey = [parent_id, job_id, {}];
        fos_collection.fetch();
      }
    });

    return {
      /**
       * Reset the analyser by re-fetching the underlying collections
       * @memberof Mossaic.widgets.slope_cells_analyser
       * @instance
       */
      reset: function() {
        collection.fetch();
        fos_collection.fetch();
      },
      /**
       * Show the widget
       * @memberof Mossaic.widgets.slope_cells_analyser
       * @instance
       */
      show: function() {
        view.render();
        timestep_view.render();
      },
      /**
       * Make the widget active/inactive
       * @memberof Mossaic.widgets.slope_cells_analyser
       * @instance
       * @param {boolean} state true if widget should become active, false
       *                        otherwise
       */
      set_active: function(state) {
        active = state;
        if (job_id !== jobs_browser.get_selected().id ||
            parent_id !== parent.get_selected().id) {
          parent_id = parent.get_selected().id;
          job_id = jobs_browser.get_selected().id;
          slip_collection.startkey = [parent_id, job_id];
          slip_collection.endkey = [parent_id, job_id, {}];
          collection.startkey = [parent_id, selected_variable.id, job_id];
          collection.endkey = [parent_id, selected_variable.id, job_id, {}];
          collection.fetch();
          fos_collection.startkey = [parent_id, job_id];
          fos_collection.endkey = [parent_id, job_id, {}];
          fos_collection.fetch();
        }
      }
    };
  };
})();