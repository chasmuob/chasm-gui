/**
 * @file Utility functions used in the app
 */
(function() {
  /**
   * Utility functions for the Mossaic app
   * @namespace
   */
  Mossaic.utils = {};

  /**
   * Round a number to the specified number of digits
   * @param {float} number Number to be rounded
   * @param {integer} digits Number of digits to be rounded to
   * @return {float} Rounded number
   */
  Mossaic.utils.round = function(number, digits) {
    var multiple = Math.pow(10, digits);
    var rounded = Math.round(number * multiple) / multiple;
    if (!isNaN(rounded) && typeof(rounded) !== "undefined") {
      return rounded;
    } else {
      return "";
    }
  }
  /**
   * Determine whether two numbers are within a specified delta
   * @param {float} x The first number
   * @param {float} y The second number
   * @param {float} delta The maximum difference that the numbers can be
   *                      considered equal
   * @return {boolean} true if the difference between the numbers is within
   *                   the given delta
   */
  Mossaic.utils.almost_equal = function(x, y, delta) {
    return Math.abs(x - y) < delta;
  }
  /**
   * Mapping of model type strings to descriptive name
   * @type object
   */
  Mossaic.utils.type_to_pretty = {
    boundary_conditions: "Boundary conditions",
    engineering_cost: "Engineering cost",
    geometry: "Slope profile",
    meta: "Slope datum",
    cross_section: "Slope cross section",
    reinforcements: "Reinforcements",
    road_network: "Road network",
    soils: "Soils",
    stability: "Stability",
    vegetation: "Vegetation"
  }
  /**
   * Mapping of task states to descriptive names
   * @type object
   */
  Mossaic.utils.state_to_pretty = {
    "new": "In Progress",
    "ready_to_submit": "Ready to submit",
    "submitted": "Submitted",
    "done": "Done",
    "failed": "Failed"
  };
  /**
   * Tries to find a name for the model and prompts the user for one if
   * necessary. Validates that the parent of this model has been saved and sets
   * the parent_id field of the model to the id of the parent. We then save the
   * model and update the name_source widget on success so it is showing the
   * newly saved model as selected.
   *
   * The id of the model is generated here, using the following deterministic
   * method: Take the parent id, append a "|" character, then append the name
   * of the model. Because the id field must be unique, this system means that
   * models saved using this method must have a name that is unique within the
   * namespace of the parent model.
   *
   * @param {Mossaic.models.Basic} model Model to be saved
   * @param {Mossaic.utils.ls_widget} name_source Selection widget used to
   *                                              hold the model id/name
   * @param {object} options Hash of options
   * @param {function} options.success Function to call if the save is
   *                                   successful (or if the model is already
   *                                   clean)
   */
  Mossaic.utils.save_model = function(model, name_source, options) {
    var _id;
    var options = options || {};
    var success = options.success;
    // If model isn't dirty, just call the success function
    if (!model.is_dirty) {
      return success();
    }
    var name;
    if (name_source.is_input_only()) {
      name = name_source.get_name();
    } else {
      name = prompt("Enter name for this " +
          Mossaic.utils.type_to_pretty[model.get("type")] +
          " document:");
    }
    if (typeof(name) !== "string" || name.length == 0) {
      alert("Please choose a valid name for this " +
          Mossaic.utils.type_to_pretty[model.get("type")]);
      console.log("Invalid name", name);
      return;
    }
    // We have a new name so lets save
    // Note: Check for conflicts anyway - we don't know for sure we're ok,
    // as perhaps the list widget didn't populate, or someone has manually
    // messed with the script, etc
    model.unset("id", {silent: true});  // Model may have an existing id so
    model.unset("_id", {silent: true}); // kill it
    model.unset("_rev", {silent: true});// No seriously, kill it
    delete model.id;                    // Really kill it
    delete model.cid;                   // DIE!
    try {
      var parent_id = name_source.get_parent_id();
      if (typeof(parent_id) !== "undefined") {
        model.set({parent_id: parent_id}, {silent: true});
        _id = parent_id + "|" + model.get("type") + ":" + name;
      } else {
        _id = model.get("type") + ":" + name;
      }
      model.save({_id: _id, name: name}, {
        error: function() {
          alert("Save failed");
        },
        success: function() {
          name_source.set_default_selection(name);
          if (typeof(success) === "function") {
            success();
          }
        },
        silent: true
      });
    } catch(e) {
      alert(e);
    }
  };

  /**
   * Validate a number obtained from DOM element target by ensuring it fails
   * isNaN. Additional validation functions can be passed in the extra_funs
   * parameter.
   * @param {number} value Value to be validated
   * @param {string} message Message to be displayed if number is invalid
   * @param {object} target The DOM element from which the value was obtained
   * @param {array} extra_funs Extra validation functions the value must pass
   * @return {boolean} True if valid, false if not
   */
  Mossaic.utils.validate_number = function(value, message, target, extra_funs) {
    var invalid_value = isNaN(value) || !_.every(extra_funs, function(fun) {
      return fun(value);
    });
    if (invalid_value) {
      alert(message);
      $(target).focus().select();
      return false;
    } else {
      return true;
    }
  }
})();