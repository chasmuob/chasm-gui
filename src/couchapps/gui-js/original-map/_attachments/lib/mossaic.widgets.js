/**
 * @file Defines the widgets used by the app
 *
 * Defines the following widgets, all of which are instantiated by functions
 * added to the Mossaic namespace. These functions return closures for
 * interacting with the widgets, where appropriate.
 *
 *  - ls_widget Allows browsing of datasets for a specifiable type
 *              Supports parent/child relationships, so a widget can be
 *              instantiated which will only show data that has a parent
 *              selected in another ls_widget.
 *
 *  - save      Saves one or more datasets. Capable of indicating whether
 *              the datasets it is responsible for are dirty.
 *
 *  - ls_save   Composite of ls_widget and save widgets
 *
 *  - results_viewer Displays individual results documents in a futon-style
 *                   tree view
 *
 *  - task_builder  Provides forms for creating, reviewing and saving tasks
 *                  and input datasets
 *
 *  - fos_analyser  View factor of safety over time for a task
 *
 *  - slope_cells_analyser  View slope cell variables over time for a job
 *
 *  - input_uploader  Upload input datasets
 *
 *  - task_status View status of jobs for a task
 */
(function() {
  var scripts_to_load = [
      "vendor/couchapp/jquery.resizer.js",
      "vendor/couchapp/jquery.editinline.js",
      "vendor/couchapp/jquery.couchDoc.js",
      "vendor/jquery-ui/jquery-ui-1.8.18.custom.min.js",
      "lib/mossaic.widgets.ls.js",
      "lib/mossaic.widgets.results_viewer.js",
      "lib/mossaic.widgets.save.js",
      "lib/mossaic.widgets.task_builder.js",
      "lib/mossaic.widgets.fos_analyser.js",
      "lib/mossaic.widgets.slope_cells_analyser.js",
      "lib/mossaic.widgets.task_analyser.js",
      "lib/mossaic.widgets.input_uploader.js",
      "lib/mossaic.widgets.task_status.js",
      "lib/mossaic.widgets.tabs.js",
      "lib/mossaic.widgets.clear_model.js",
      "lib/mossaic.widgets.ls_save.js",
      "lib/mossaic.widgets.dashboard.js"
  ];

  /**
   * Namespace for Mossaic GUI widgets.
   *
   * Widgets use the module pattern to provide actual private members. They
   * are instantiated simply by calling the function *without* the new keyword.
   * A hash of functions is returned which form closures with variables
   * inside the function.
   *
   * Unfortunately JSDoc doesn't give us the right tools to properly document
   * the module pattern, so we cheat and use the constructor tag. This means
   * the widgets are listed in the documentation as classes which should be
   * instantiated with the new keyword. This is untrue and should be ignored
   * for all widgets.
   *
   * @namespace
   */
  Mossaic.widgets = {};

  var load_scripts = function(scripts) {
    for (var i=0; i < scripts.length; i++) {
      document.write('<script src="'+scripts[i]+'"><\/script>');
    }
  };

  load_scripts(scripts_to_load);

  /**
   * Build any widgets that require templates from the design doc
   * This should be triggered by the top level object (probably a router)
   * _after_ initialisation (so we know that Mossaic.db.ddoc.templates exists)
   */
  Mossaic.widgets.build_deferred = function() {
    _.each(Mossaic.widgets.deferred, function(widget) {
      widget();
    });
  }
})();