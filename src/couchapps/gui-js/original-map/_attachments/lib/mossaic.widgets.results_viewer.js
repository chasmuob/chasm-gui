/**
 * @file Defines a results_viewer widget for viewing results documents as
 *       JSON trees
 */
(function() {
  /**
   * Create a results_viewer widget which consists of an ls widget for viewing
   * result document names, and a jquery.couchDoc for viewing the data as a
   * JSON tree.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @extends Mossaic.widgets.ls
   * @param {object} options Hash of options
   *                {object} db           $.couch.db object
   *                {String} divname      Id selector of the div to which
   *                                      this widget should be appended
   *                {object} parent       Mossaic.widgets.ls_widget that selects
   *                                      the parent model for the results
   *                                      dataset
   */
  Mossaic.widgets.results_viewer = function(options) {
    var type = "results";
    var db = options.db;
    var divname = options.divname;
    var parent = options.parent;
    var prefix = divname.split("#")[1];
    var ls_div = prefix + "_ls";
    var viewer_div = prefix + "_viewer";
    var template = '<div id="' + ls_div + '"></div>' +
        '<div id="' + viewer_div + '"></div>';

    $(divname).html(template);

    var ls_widget = Mossaic.widgets.ls({
      divname: "#" + ls_div,
      label: "Results",
      type: "results",
      parent: parent,
      view: "AutoBox",
      success: function(result) {
        return _.map(result.rows, function(row) {
          return {name: row.id, id: row.id};
        });
      }
    });

    ls_widget.register_listener(function() {
      var selected = ls_widget.get_selected();
      var id = selected.id;
      if (typeof(id) !== "undefined") {
        var Model = Mossaic.models.map[type];
        var model = new Model({id: id}, {});
        model.fetch({
          silent: true,
          success: function() {
            console.log(model.toJSON());
            $("#" + viewer_div).couchDoc({
              db: db,
              doc: model.toJSON()
            });
          },
          error: function() {
            alert("Could not load data " + model.id);
          }
        });
      } else {
        console.log("Nothing to load", selected, type);
      }
    });

    return ls_widget;
  };
})();