/**
 * @file Define the form for displaying mesh data for a Mossaic.models.Geometry
 *       model.
 *
 *       Note that importing this script adds the function that builds the form
 *       to the Mossaic.forms.deferred array. The actual form class will be
 *       created when triggered by the Mossaic.Router. This is required because
 *       the form is dependent upon a template stored in the CouchDB design doc.
 */
(function() {
  if (typeof(Mossaic.forms.deferred) === "undefined") {
    Mossaic.forms.deferred = [];
  }
  Mossaic.forms.deferred = Mossaic.forms.deferred.concat([
    function () {
      /**
       * Backbone View for slope mesh data represented by
       * Mossaic.models.Geometry
       * @constructor
       * @extends Mossaic.forms.Basic
       */
      Mossaic.forms.Mesh = Mossaic.forms.Basic.extend({
        /**
         * Initialize the form.
         * Called implicitly with "new Mossaic.forms.Mesh".
         * @memberof Mossaic.forms.Mesh
         * @instance
         * @param {Mossaic.models.Geometry} model Model to be represented
         *                                        by this form
         * @param {object} options Hash of options
         * @param {string} options.divname jQuery selector for div to which this
         *                                 form should be appended
         */
        initialize: function(model, options) {
          Mossaic.forms.Basic.prototype.initialize.apply(this, [model, options]);
          this.divname = options.divname || "#geometry_mesh_form";
          this.form_id = this.divname + "_mesh";
        },
        /**
         * Template used to create the form
         * @memberof Mossaic.forms.Mesh
         * @instance
         * @type string
         */
        template: Mossaic.db.ddoc.templates.mesh,
        /**
         * Handle change events triggered by the model. Only redraw if the
         * model was changed by a different view.
         * @memberof Mossaic.forms.Mesh
         * @instance
         */
        handle_change: function() {
          // Change may have originated from this view or somewhere else. So check
          // called_by attribute. We only want to render if the model was changed
          // by a different view as this view will already be up-to-date.
          if (this.model.changed_by != this) {
            this.render();
          }
        },
        /**
         * Render the form.
         * Will be called implicitly by show or handle_change so should not need
         * to be called directly.
         * @memberof Mossaic.forms.Mesh
         * @instance
         */
        render: function() {
          if (this.hidden) {
            return;
          }
          var that = this;
          var mesh = this.model.get_mesh();
          $(this.form_id).remove();
          $(this.divname).html($.mustache(this.template, {
            form_id: this.form_id.split("#")[1],
            column_width: mesh.x,
            cell_depth: mesh.y
          }));
          $(".mesh").change(function(event) {
            var id = event.target.id;
            var value = parseInt(event.target.value, 10);
            if (!Mossaic.utils.validate_number(value,
                "Please enter a positive numeric value for the mesh " +
                  "dimensions",
                event.target,
                [function(val) { return val > 0; }])) {
              return;
            }
            var geometry = _.clone(that.model.get("geometry"));
            var mesh = _.clone(geometry.mesh);
            var attr_to_change = _.clone(mesh[id]);
            attr_to_change.value = value;
            mesh[id] = attr_to_change;
            geometry.mesh = mesh;
            that.model.changed_by = that;
            that.model.set({geometry: geometry});
          });
        }
      });
    }
  ]);
})();