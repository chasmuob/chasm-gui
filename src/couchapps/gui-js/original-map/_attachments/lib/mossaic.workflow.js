/**
 * @file Creates the Mossaic.workflow namespace and imports the scripts to
 *       create the workflows
 */
(function() {
  var scripts_to_load = [
    "lib/mossaic.workflow.slope.js"
    ],
    load_scripts;

  load_scripts = function(scripts) {
    for (var i=0; i < scripts.length; i++) {
      document.write('<script src="'+scripts[i]+'"><\/script>');
    }
  };

  if (typeof(Mossaic) === "undefined") {
    Mossaic = {};
  }
  /**
   * Workflows used in the application.
   * A workflow is an ordered combination of widgets which take the user through
   * the required sequence of activities in order to achive a task.
   *
   * Workflows use the module pattern to provide actual private members. They
   * are instantiated simply by calling the function *without* the new keyword.
   * A hash of functions is returned which form closures with variables
   * inside the function.
   *
   * Unfortunately JSDoc doesn't give us the right tools to properly document
   * the module pattern, so we cheat and use the constructor tag. This means
   * the workflowss are listed in the documentation as classes which should be
   * instantiated with the new keyword. This is untrue and should be ignored
   * for all workflows.
   *
   * @namespace
   */
  Mossaic.workflow = {};

  load_scripts(scripts_to_load);
})();