/**
 * @file Defines the task_status widget
 */
(function() {
  /**
   * Create task_status widget. A simple widget for viewing the overall status
   * of the jobs for a given task.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {string} options.divname      Id selector of div to append to
   * @param {string} options.key_divname  Id selector of div the key appends to
   * @param {Mossaic.widgets.ls} options.parent
   *                                      ls widget that selects the parent
   *                                      dataset for this widget - in this case
   *                                      a slope profile object (i.e. geometry)
   */
  Mossaic.widgets.task_status = function(options) {
    var divname = options.divname;
    var key_divname = options.key_divname;
    var parent = options.parent;
    var parent_id = parent.get_selected().id;
    var collection = new Backbone.Collection();
    collection.view = "jobs_by_profile";
    collection.type = "job";  // Manually specify type for _changes handling
                              // in backbone.couch.js
    collection.urlRoot = Mossaic.db.get_db_name({type: "task"});
    collection.ddocName = "workflow";
    collection.doreduce = true;
    collection.group = true;
    collection.group_level = 3;
    collection.startkey = [parent_id];
    collection.endkey = [parent_id, {}];
    collection.docChangeHandler = function(collection, doc, id) {
      collection.fetch();
    };
    collection.success = function(result) {
      return result.rows;
    };

    Backbone.couch._changes({
      db: Mossaic.db.get_db_name({type: "task"}),
      ddocName: collection.view
    });

    /**
     * View for the task_status widget
     * @constructor
     * @private
     */
    var View = Backbone.View.extend({
      initialize: function(collection, options) {
        _.bindAll(this);
        this.divname = options.divname;
        this.key_divname = options.key_divname;
        this.collection = collection;
        this.collection.bind("reset", this.render);
        $(window).resize(this.render);
        Backbone.View.prototype.initialize.apply(this, [collection, options]);
      },
      states: [
        //"new",
        //"ready_to_submit",
        //"submitted",
        "new",
        "done",
        "failed"
      ],
      colors: [
        //"silver",
        //"lightblue",
        //"steelblue",
        "steelblue",
        "springgreen",
        "firebrick"
      ],
      /**
       * Render a row from a task object
       * @memberof View
       * @instance
       * @private
       */
      render_row: function(task) {
        var that = this;
        var tr = this.table.append("xhtml:tr");
        tr.append("xhtml:td")
          .html(task.task_name);
        tr.append("xhtml:td")
          .html(task.total);
        var td_status = tr.append("xhtml:td");
        // Created ordered list of data
        var total_width = parseInt(td_status.style("width").split("px")[0]);
        var scale = d3.scale.linear()
          .domain([0, task.total])
          .range([0, total_width]);
        var largest = {
          value: 0,
          state: undefined
        }
        var total_displayed_width = 0;
        td_status.selectAll(".task_status")
          .data(this.states)
            .enter().append("xhtml:div")
              .attr("id", function(d) {
                return d;
              })
              .style("width", function(d) {
                var state_count = task.states[d];
                if (typeof(state_count) === "undefined") {
                  state_count = 0;
                }
                var required_width = Math.round(scale(state_count));
                if (required_width > 0 && required_width < 1) {
                  required_width = 1;
                }
                if (required_width > largest.value) {
                  largest.value = required_width;
                  largest.state = d;
                }
                total_displayed_width += required_width;
                return required_width + "px";
              })
              .style("float", "left")
              .style("height", "20px")
              .style("background-color", function(d, i) {
                return that.colors[i];
              });
        // If we haven't exactly filled the space (due to ratios of state
        // frequency) then modify the largest state so it fits properly
        if (total_width - total_displayed_width !== 0) {
          var diff = total_width - total_displayed_width;
          var new_width = largest.value + diff;
          td_status.select("#" + largest.state)
            .style("width", new_width + "px");
        }
      },
      /**
       * Render the key for the visualisation
       * @memberof View
       * @instance
       * @private
       */
      render_key: function() {
        var that = this;
        if (this.key) {
          this.key.remove();
        }
        var div = d3.select(this.key_divname);
        this.key = div.append("xhtml:div");
        _.each(this.states, function(state, i) {
          that.key.append("xhtml:div")
            .style("float", "left")
            .style("width", "50px")
            .style("height", "20px")
            .style("background-color", that.colors[i]);
          that.key.append("xhtml:p")
            .style("padding-left", "60px")
            .html(Mossaic.utils.state_to_pretty[state]);
        });
      },
      /**
       * Render the visualisation
       * @memberof View
       * @instance
       * @private
       */
      render: function() {
        this.render_key();
        var that = this;
        var tasks = {};
        if (this.table) {
          this.table.remove();
        }
        this.div = d3.select(this.divname);
        if (this.div.length === 0 || this.div[0][0] === null) {
          return;
        }
        this.table = this.div.append("xhtml:table");
        var task_name_width = 120;
        var task_total_width = 70;
        var parent_width = parseInt(this.div.style("width").split("px")[0]);
        var status_width = parent_width - task_name_width - task_total_width;
        var title = this.table.append("xhtml:tr");
        title.append("xhtml:th")
          .style("width", task_name_width + "px")
          .html("Task name");
        title.append("xhtml:th")
          .style("width", task_total_width + "px")
          .html("Total jobs");
        title.append("xhtml:th")
          .style("width", status_width + "px")
          .html("Status summary");
        collection.forEach(function(item) {
          var key = item.get("key");
          var profile_id = key[0];
          var task_name = key[1];
          var state = key[2];
          var number_of_jobs_in_state = item.get("value");
          if (!(task_name in tasks)) {
            tasks[task_name] = {task_name: task_name,
                total: number_of_jobs_in_state, states: {}};
            tasks[task_name].states[state] = number_of_jobs_in_state;
          } else {
            var task = tasks[task_name];
            if (!(state in task.states)) {
              task.states[state] = number_of_jobs_in_state;
            } else {
              task.states[state] += number_of_jobs_in_state;
            }
            task.total += number_of_jobs_in_state;
          }
        });
        _.each(_.values(tasks), function(task) {
          that.render_row(task);
        });
      }
    });

    view = new View(collection, {
      divname: divname,
      key_divname: key_divname
    });

    if (typeof(parent) !== "undefined") {
      parent.register_listener(function() {
        var new_parent_id = parent.get_selected().id;
        if (new_parent_id != parent_id ||
            typeof(parent_id) === "undefined") {
          parent_id = new_parent_id;
          collection.startkey = [parent_id];
          collection.endkey = [parent_id, {}];
          collection.fetch();
        }
      });
    }

    collection.fetch();

    return {
      /**
       * Show the widget
       * @memberof Mossaic.widgets.task_status
       * @instance
       */
      show: function() {
        view.render();
      }
    };
  };
})();