This folder is here as a historical reference to the original Mossaic codebase,
before any changes were made by ILRT staff.  A lot of code (particually around Questa)
was removed in the new version, and if Questa modelling is re-instated it will be useful
to refer to the code in this folder.

This should also obviously be available through SVN/Git, this folder is really a backup in case 
something goes wrong with the repos.