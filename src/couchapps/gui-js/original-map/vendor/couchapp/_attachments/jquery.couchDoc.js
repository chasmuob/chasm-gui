// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

// This file consists mostly of elements of futon.browse.js and futon.format.js
// from the Apache CouchDB project.

(function($) {
  var template = '<table id="fields" class="listing" cellspacing="0">' +
      '<col class="field"><col class="value">' +
      '<caption>Fields</caption>' +
      '<thead>' +
      '<tr>' +
        '<th>Field</th>' +
        '<th>Value</th>' +
      '</tr>' +
    '</thead>' +
    '<tbody class="content">' +
    '</tbody>' +

    '<tbody class="source" style="display: none">' +
      '<tr><td colspan="2"></td></tr>' +
    '</tbody>' +
    '<tbody class="footer">' +
      '<tr>'
        '<td colspan="2">' +
          '<div id="paging">' +
            '<a class="prev">← Previous Version</a> | ' +
            '<a class="next">Next Version →</a>' +
          '</div>' +
          '<span></span>' +
        '</td>' +
      '</tr>' +
    '</tbody>' +
  '</table>';

  var escape = function(string) {
    return string.replace(/&/g, "&amp;")
                 .replace(/</g, "&lt;")
                 .replace(/>/g, "&gt;")
                 .replace(/"/, "&quot;")
                 .replace(/'/, "&#39;")
                 ;
  };

  // JSON pretty printing
  var formatJSON = function(val, options) {
    options = $.extend({
      escapeStrings: true,
      indent: 4,
      linesep: "\n",
      quoteKeys: true
    }, options || {});
    var itemsep = options.linesep.length ? "," + options.linesep : ", ";

    function format(val, depth) {
      var tab = [];
      for (var i = 0; i < options.indent * depth; i++) tab.push("");
      tab = tab.join(" ");

      var type = typeof val;
      switch (type) {
        case "boolean":
        case "number":
        case "string":
          var retval = val;
          if (type == "string" && !options.escapeStrings) {
            retval = indentLines(retval.replace(/\r\n/g, "\n"), tab.substr(options.indent));
          } else {
            if (options.html) {
              retval = escape(JSON.stringify(val));
            } else {
              retval = JSON.stringify(val);
            }
          }
          if (options.html) {
            retval = "<code class='" + type + "'>" + retval + "</code>";
          }
          return retval;

        case "object": {
          if (val === null) {
            if (options.html) {
              return "<code class='null'>null</code>";
            }
            return "null";
          }
          if (val.constructor == Date) {
            return JSON.stringify(val);
          }

          var buf = [];

          if (val.constructor == Array) {
            buf.push("[");
            for (var index = 0; index < val.length; index++) {
              buf.push(index > 0 ? itemsep : options.linesep);
              buf.push(tab, format(val[index], depth + 1));
            }
            if (index >= 0) {
              buf.push(options.linesep, tab.substr(options.indent));
            }
            buf.push("]");
            if (options.html) {
              return "<code class='array'>" + buf.join("") + "</code>";
            }

          } else {
            buf.push("{");
            var index = 0;
            for (var key in val) {
              buf.push(index > 0 ? itemsep : options.linesep);
              var keyDisplay = options.quoteKeys ? JSON.stringify(key) : key;
              if (options.html) {
                if (options.quoteKeys) {
                  keyDisplay = keyDisplay.substr(1, keyDisplay.length - 2);
                }
                keyDisplay = "<code class='key'>" + escape(keyDisplay) + "</code>";
                if (options.quoteKeys) {
                  keyDisplay = '"' + keyDisplay + '"';
                }
              }
              buf.push(tab, keyDisplay,
                ": ", format(val[key], depth + 1));
              index++;
            }
            if (index >= 0) {
              buf.push(options.linesep, tab.substr(options.indent));
            }
            buf.push("}");
            if (options.html) {
              return "<code class='object'>" + buf.join("") + "</code>";
            }
          }

          return buf.join("");
        }
      }
    }

    function indentLines(text, tab) {
      var lines = text.split("\n");
      for (var i in lines) {
        lines[i] = (i > 0 ? tab : "") + escape(lines[i]);
      }
      return lines.join("<br>");
    }

    return format(val, 1);
  };

  // File size pretty printing
  var formatSize = function(size) {
    var jump = 512;
    if (size < jump) return size + " bytes";
    var units = ["KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    var i = 0;
    while (size >= jump && i < units.length) {
      i += 1;
      size /= 1024
    }
    return size.toFixed(1) + ' ' + units[i - 1];
  };

  var CouchDocumentPage = function(db, editable) {
    var urlParts = location.search.substr(1).split("/");
    var dbName = decodeURIComponent(urlParts.shift());
    if (urlParts.length) {
      var idParts = urlParts.join("/").split("@", 2);
      var docId = decodeURIComponent(idParts[0]);
      var docRev = (idParts.length > 1) ? idParts[1] : null;
      this.isNew = false;
    } else {
      var docId = $.couch.newUUID();
      var docRev = null;
      this.isNew = true;
    }

    var db = db;
    var editable = editable;

    this.dbName = dbName;
    this.db = db;
    this.docId = docId;
    this.doc = null;
    this.isDirty = this.isNew;
    page = this;

    this.activateTabularView = function() {
      if ($("#fields tbody.source textarea").length > 0)
        return;

      $("#tabs li").removeClass("active").filter(".tabular").addClass("active");
      $("#fields thead th:first").text("Field").attr("colspan", 1).next().show();
      $("#fields tbody.content").show();
      $("#fields tbody.source").hide();
      return false;
    }

    this.activateSourceView = function() {
      $("#tabs li").removeClass("active").filter(".source").addClass("active");
      $("#fields thead th:first").text("Source").attr("colspan", 2).next().hide();
      $("#fields tbody.content").hide();
      $("#fields tbody.source").find("td").each(function() {
        var view = $(this).html($("<pre></pre>").html(formatJSON(page.doc, {html: true})))
        if (editable) {
          view.makeEditable({allowEmpty: false,
              createInput: function(value) {
              var rows = value.split("\n").length;
              return $("<textarea rows='" + rows + "' cols='80' spellcheck='false'></textarea>").enableTabInsertion();
            },
            prepareInput: function(input) {
              $(input).makeResizable({vertical: true});
            },
            end: function() {
              $(this).html($("<pre></pre>").html(formatJSON(page.doc, {html: true})));
            },
            accept: function(newValue) {
              page.doc = JSON.parse(newValue);
              page.isDirty = true;
              page.updateFieldListing(true);
            },
            populate: function(value) {
              return formatJSON(page.doc);
            },
            validate: function(value) {
              try {
                var doc = JSON.parse(value);
                if (typeof doc != "object")
                  throw new SyntaxError("Please enter a valid JSON document (for example, {}).");
                return true;
              } catch (err) {
                var msg = err.message;
                if (msg == "parseJSON" || msg == "JSON.parse") {
                  msg = "There is a syntax error in the document.";
                }
                $("<div class='error'></div>").text(msg).appendTo(this);
                return false;
              }
            }
          });
        }
      }).end().show();
      return false;
    }

    this.addField = function() {
      if (!$("#fields tbody.content:visible").length) {
        location.hash = "#tabular";
        page.activateTabularView();
      }
      var fieldName = "unnamed";
      var fieldIdx = 1;
      while (page.doc.hasOwnProperty(fieldName)) {
        fieldName = "unnamed " + fieldIdx++;
      }
      page.doc[fieldName] = null;
      var row = _addRowForField(page.doc, fieldName);
      page.isDirty = true;
      row.find("th b").dblclick();
    }

    var _sortFields = function(a, b) {
      var a0 = a.charAt(0), b0 = b.charAt(0);
      if (a0 == "_" && b0 != "_") {
        return -1;
      } else if (a0 != "_" && b0 == "_") {
        return 1;
      } else if (a == "_attachments" || b == "_attachments") {
        return a0 == "_attachments" ? 1 : -1;
      } else {
        return a < b ? -1 : a != b ? 1 : 0;
      }
    }

    this.updateFieldListing = function(noReload) {
      $("#fields tbody.content").empty();

      function handleResult(doc, revs) {
        page.doc = doc;
        var propNames = [];
        for (var prop in doc) {
          propNames.push(prop);
        }
        // Order properties alphabetically, but put internal fields first
        propNames.sort(_sortFields);
        for (var pi = 0; pi < propNames.length; pi++) {
          _addRowForField(doc, propNames[pi]);
        }
        if (revs.length > 1) {
          var currentIndex = 0;
          for (var i = 0; i < revs.length; i++) {
            if (revs[i].rev == doc._rev) {
              currentIndex = i;
              break;
            }
          }
          if (currentIndex < revs.length - 1) {
            var prevRev = revs[currentIndex + 1].rev;
            $("#paging a.prev").attr("href", "?" + encodeURIComponent(dbName) +
              "/" + $.couch.encodeDocId(docId) + "@" + prevRev);
          }
          if (currentIndex > 0) {
            var nextRev = revs[currentIndex - 1].rev;
            $("#paging a.next").attr("href", "?" + encodeURIComponent(dbName) +
              "/" + $.couch.encodeDocId(docId) + "@" + nextRev);
          }
          $("#fields tbody.footer td span").text("Showing revision " +
            (revs.length - currentIndex) + " of " + revs.length);
        }
      }

      if (noReload) {
        handleResult(page.doc, []);
        return;
      }

      if (!page.isNew) {
        db.openDoc(docId, {revs_info: true,
          success: function(doc) {
            var revs = doc._revs_info || [];
            delete doc._revs_info;
            if (docRev != null) {
              db.openDoc(docId, {rev: docRev,
                error: function(status, error, reason) {
                  alert("The requested revision was not found. You will " +
                        "be redirected back to the latest revision.");
                  location.href = "?" + encodeURIComponent(dbName) +
                    "/" + $.couch.encodeDocId(docId);
                },
                success: function(doc) {
                  handleResult(doc, revs);
                }
              });
            } else {
              handleResult(doc, revs);
            }
          }
        });
      } else {
        handleResult({_id: docId}, []);
        $("#fields tbody td").dblclick();
      }
    }

    this.deleteDocument = function() {
      $.showDialog("dialog/_delete_document.html", {
        submit: function(data, callback) {
          db.removeDoc(page.doc, {
            success: function(resp) {
              callback();
              location.href = "database.html?" + encodeURIComponent(dbName);
            }
          });
        }
      });
    }

    this.saveDocument = function() {
      db.saveDoc(page.doc, {
        error: function(status, error, reason) {
          alert("Error: " + error + "\n\n" + reason);
        },
        success: function(resp) {
          page.isDirty = false;
          location.href = "?" + encodeURIComponent(dbName) +
            "/" + $.couch.encodeDocId(page.docId);
        }
      });
    }

    this.uploadAttachment = function() {
      if (page.isDirty) {
        alert("You need to save or revert any changes you have made to the " +
              "document before you can attach a new file.");
        return false;
      }
      $.showDialog("dialog/_upload_attachment.html", {
        load: function(elem) {
          $("input[name='_rev']", elem).val(page.doc._rev);
        },
        submit: function(data, callback) {
          if (!data._attachments || data._attachments.length == 0) {
            callback({_attachments: "Please select a file to upload."});
            return;
          }
          var form = $("#upload-form");
          form.find("#progress").css("visibility", "visible");
          form.ajaxSubmit({
            url: db.uri + $.couch.encodeDocId(page.docId),
            success: function(resp) {
              form.find("#progress").css("visibility", "hidden");
              page.isDirty = false;
              location.href = "?" + encodeURIComponent(dbName) +
                "/" + $.couch.encodeDocId(page.docId);
            }
          });
        }
      });
    }

    function _addRowForField(doc, fieldName) {
      var row = $("<tr><th></th><td></td></tr>")
        .find("th").append($("<b></b>").text(fieldName)).end()
        .appendTo("#fields tbody.content");
      if (fieldName == "_attachments") {
        row.find("td").append(_renderAttachmentList(doc[fieldName]));
      } else {
        row.find("td").append(_renderValue(doc[fieldName]));
        _initKey(doc, row, fieldName);
        _initValue(doc, row, fieldName);
      }
      $("#fields tbody.content tr").removeClass("odd").filter(":odd").addClass("odd");
      row.data("name", fieldName);
      return row;
    }

    function _initKey(doc, row, fieldName) {
      if (fieldName == "_id" || fieldName == "_rev") {
        return;
      }

      var cell = row.find("th");

      if (editable) {
        $("<button type='button' class='delete' title='Delete field'></button>").click(function() {
          delete doc[fieldName];
          row.remove();
          page.isDirty = true;
          $("#fields tbody.content tr").removeClass("odd").filter(":odd").addClass("odd");
        }).prependTo(cell);
      }

      if (editable) {
        cell.find("b").makeEditable({allowEmpty: false,
          accept: function(newName, oldName) {
            doc[newName] = doc[oldName];
            delete doc[oldName];
            row.data("name", newName);
            $(this).text(newName);
            page.isDirty = true;
          },
          begin: function() {
            row.find("th button.delete").hide();
            return true;
          },
          end: function(keyCode) {
            row.find("th button.delete").show();
            if (keyCode == 9) { // tab, move to editing the value
              row.find("td").dblclick();
            }
          },
          validate: function(newName, oldName) {
            $("div.error", this).remove();
            if (newName != oldName && doc[newName] !== undefined) {
              $("<div class='error'>Already have field with that name.</div>")
                .appendTo(this);
              return false;
            }
            return true;
          }
        });
      }
    }

    function _initValue(doc, row, fieldName) {
      if ((fieldName == "_id" && !page.isNew) || fieldName == "_rev") {
        return;
      }
      if (editable) {
        row.find("td").makeEditable({acceptOnBlur: false, allowEmpty: true,
          createInput: function(value) {
            value = doc[row.data("name")];
            var elem = $(this);
            if (elem.find("dl").length > 0 ||
                elem.find("code").is(".array, .object") ||
                typeof(value) == "string" && (value.length > 60 || value.match(/\n/))) {
              return $("<textarea rows='1' cols='40' spellcheck='false'></textarea>");
            }
            return $("<input type='text' spellcheck='false'>");
          },
          end: function() {
            $(this).children().remove();
            $(this).append(_renderValue(doc[row.data("name")]));
          },
          prepareInput: function(input) {
            if ($(input).is("textarea")) {
              var height = Math.min(input.scrollHeight, document.body.clientHeight - 100);
              $(input).height(height).makeResizable({vertical: true}).enableTabInsertion();
            }
          },
          accept: function(newValue) {
            var fieldName = row.data("name");
            try {
              doc[fieldName] = JSON.parse(newValue);
            } catch (err) {
              doc[fieldName] = newValue;
            }
            page.isDirty = true;
            if (fieldName == "_id") {
              page.docId = page.doc._id = doc[fieldName];
              $("h1 strong").text(page.docId);
            }
          },
          populate: function(value) {
            value = doc[row.data("name")];
            if (typeof(value) == "string") {
              return value;
            }
            return formatJSON(value);
          },
          validate: function(value) {
            $("div.error", this).remove();
            try {
              var parsed = JSON.parse(value);
              if (row.data("name") == "_id" && typeof(parsed) != "string") {
                $("<div class='error'>The document ID must be a string.</div>")
                  .appendTo(this);
                return false;
              }
              return true;
            } catch (err) {
              return true;
            }
          }
        });
      }
    }

    function _renderValue(value) {
      function isNullOrEmpty(val) {
        if (val == null) return true;
        for (var i in val) return false;
        return true;
      }
      function render(val) {
        var type = typeof(val);
        if (type == "object" && !isNullOrEmpty(val)) {
          var list = $("<dl></dl>");
          for (var i in val) {
            $("<dt></dt>").text(i).appendTo(list);
            $("<dd></dd>").append(render(val[i])).appendTo(list);
          }
          return list;
        } else {
          var html = formatJSON(val, {
            html: true,
            escapeStrings: false
          });
          var n = $(html);
          if (n.text().length > 140) {
            // This code reduces a long string in to a summarized string with a link to expand it.
            // Someone, somewhere, is doing something nasty with the event after it leaves these handlers.
            // At this time I can't track down the offender, it might actually be a jQuery propogation issue.
            var fulltext = n.text();
            var mintext = n.text().slice(0, 140);
            var e = $('<a href="#expand">...</a>');
            var m = $('<a href="#min">X</a>');
            var expand = function (evt) {
              n.empty();
              n.text(fulltext);
              n.append(m);
              evt.stopPropagation();
              evt.stopImmediatePropagation();
              evt.preventDefault();
            }
            var minimize = function (evt) {
              n.empty();
              n.text(mintext);
              // For some reason the old element's handler won't fire after removed and added again.
              e = $('<a href="#expand">...</a>');
              e.click(expand);
              n.append(e);
              evt.stopPropagation();
              evt.stopImmediatePropagation();
              evt.preventDefault();
            }
            e.click(expand);
            n.click(minimize);
            n.text(mintext);
            n.append(e)
          }
          return n;
        }
      }
      var elem = render(value);

      elem.find("dd:has(dl)").hide().prev("dt").addClass("collapsed");
      elem.find("dd:not(:has(dl))").addClass("inline").prev().addClass("inline");
      elem.find("dt.collapsed").click(function() {
        $(this).toggleClass("collapsed").next().toggle();
      });

      return elem;
    }

    function _renderAttachmentList(attachments) {
      var ul = $("<ul></ul>").addClass("attachments");
      $.each(attachments, function(idx, attachment) {
        _renderAttachmentItem(idx, attachment).appendTo(ul);
      });
      return ul;
    }

    function _renderAttachmentItem(name, attachment) {
      var attachmentHref = db.uri + $.couch.encodeDocId(page.docId)
        + "/" + encodeAttachment(name);
      var li = $("<li></li>");
      $("<a href='' title='Download file' target='_top'></a>").text(name)
        .attr("href", attachmentHref)
        .wrapInner("<tt></tt>").appendTo(li);
      $("<span>()</span>").text("" + formatSize(attachment.length) +
        ", " + attachment.content_type).addClass("info").appendTo(li);
      if (name == "tests.js") {
        li.find('span.info').append(', <a href="/_utils/couch_tests.html?'
          + attachmentHref + '">open in test runner</a>');
      }
      _initAttachmentItem(name, attachment, li);
      return li;
    }

    function _initAttachmentItem(name, attachment, li) {
      if (editable) {
        $("<button type='button' class='delete' title='Delete attachment'></button>").click(function() {
          if (!li.siblings("li").length) {
            delete page.doc._attachments;
            li.parents("tr").remove();
            $("#fields tbody.content tr").removeClass("odd").filter(":odd").addClass("odd");
          } else {
            delete page.doc._attachments[name];
            li.remove();
          }
          page.isDirty = true;
          return false;
        }).prependTo($("a", li));
      }
    }
  };

  function encodeAttachment(name) {
    var encoded = [], parts = name.split('/');
    for (var i=0; i < parts.length; i++) {
      encoded.push(encodeURIComponent(parts[i]));
    };
    return encoded.join('%2f');
  }

  $.fn.enableTabInsertion = function(chars) {
    chars = chars || "\t";
    var width = chars.length;
    return this.keydown(function(evt) {
      if (evt.keyCode == 9) {
        var v = this.value;
        var start = this.selectionStart;
        var scrollTop = this.scrollTop;
        if (start !== undefined) {
          this.value = v.slice(0, start) + chars + v.slice(start);
          this.selectionStart = this.selectionEnd = start + width;
        } else {
          document.selection.createRange().text = chars;
          this.caretPos += width;
        }
        return false;
      }
    });
  };

  $.fn.couchDoc = function(options) {
    var defaults = {
      to_render: {}
    };
    $.extend(defaults, options);

    return this.each(function(index) {
      if (index > 0) {
        console.log("Currently only supporting one CouchDoc per page");
        return;
      }
      var el = $(this);
      el.html(template);
      var page = new CouchDocumentPage(options.db, true);
      page.docId = options.doc._id;
      page.doc = options.doc;
      page.db = options.db;
      page.activateTabularView();
      page.updateFieldListing(true);
    });
  };
})(jQuery);