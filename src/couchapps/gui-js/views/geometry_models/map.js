function(doc) {
  if (typeof(doc) !== "undefined" &&
      doc.type != null) {
    emit(doc.type, doc.name);
  }
}
