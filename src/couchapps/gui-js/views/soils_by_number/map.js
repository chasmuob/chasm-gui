function(doc) {
  if (doc.type === "soils") {
    emit(doc.soils.length, doc.name);
  }
}