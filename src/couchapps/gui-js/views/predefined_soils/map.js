function(doc) {
  if (typeof(doc) !== "undefined" && doc.type == "predefined_soil") {
    emit(doc.name, doc);
  }
}