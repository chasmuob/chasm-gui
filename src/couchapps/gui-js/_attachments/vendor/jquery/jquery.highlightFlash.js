  $.fn.highlightFlash = function (highlightColor, duration) {
        var highlightBg = highlightColor || "#FFFF9C";
        var animateMs = duration || 1000;
        var originalBg = this.css("background-color");

        if (!originalBg) {
          originalBg = "none"
        } 
      
        jQuery(this)
            .css("backgroundColor", highlightBg)
            .animate({ backgroundColor: originalBg }, animateMs, null, function () {
                jQuery(this).css("backgroundColor", ""); 
            }); 
    };