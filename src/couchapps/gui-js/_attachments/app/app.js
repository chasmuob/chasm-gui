(function() {
  'use strict';
  angular
    .module('chasm.home',['chasm.core'])
    .config(['$stateProvider', function($stateProvider){
      $stateProvider.state('home',{
        url: '/',
        templateUrl: 'home/home.html',
        controller: 'HomeCtrl as ctrl'
      });
    }]);
}());

(function() {
  'use strict';
  angular
    .module('chasm.home')
    .controller('HomeCtrl',HomeCtrl);

  function HomeCtrl($q,Couch){
    var vm = this;
    var models;
    vm.data = {};

    var promises = {};
    promises.datum = Couch.getByType('meta');
    promises.crossSection = Couch.getByType('cross_section');
    promises.geometry = Couch.getByType('geometry');

    $q.all(promises).then(function(results){
      // angular.extend(vm.data,results);
      models = results;
      vm.data.datum = results.datum;
      console.log(results);
    });

    vm.selectDatum = function(datum){
      console.log(datum);
      Couch.getByParent('cross_section',datum.id).then(function(values){
        vm.data.crossSection = values;
      });
    };

    vm.selectCrossSection = function(crossSection){
      Couch.getByParent('geometry',crossSection.id).then(function(values){
        vm.data.geometry = values;
      });
    };

    vm.selectGeometry = function(datum){

    };
  }
  HomeCtrl.$inject = ['$q', 'Couch'];
}());

(function() {
  'use strict';
  angular
    .module('chasm.home')
    .directive('chasmBrowser', ['$location', '$q', '$uibModal', 'Couch', function($location, $q, $uibModal, Couch) {
      return {
        restrict: 'E',
        templateUrl: 'home/chasm-browser.directive.html',
        replace: true,
        link: function(scope, element, attributes) {
          scope.data = {};
          scope.active = {};
          scope.actives = {};

          scope.taskClassMap = {
            done: 'task-done',
            in_progress: 'task-in-progress',
          };

          scope.taskStatusMap = {
            done: 'Done',
            in_progress: 'Running'
          };

          // This shows us the heirarchy of data
          var dataHeirarchy = ['meta', 'cross_section', 'geometry', 'task'];

          Couch.getByType('meta').then(function(data) {
            scope.data.meta = data;
          });

          function clearData(model) {
            // Go backwards down the heirarchy until we get to our model
            for (var i = dataHeirarchy.length - 1; i >= 0; i--) {
              if (dataHeirarchy[i] === model.type) {
                break;
              }
              scope.data[dataHeirarchy[i]] = [];
            }
            scope.active = null;
            scope.actives[model.type] = null;
          }

          function isActive(model) {
            return model === scope.active || scope.actives[model.type] === model;
          }

          function setActive(model, type) {
            scope.actives[type] = model;
            model.type = type;
            scope.active = model;
          }

          function modal(options) {
            return $uibModal.open(angular.extend({}, {
              animate: true,
              scope: scope,
              resolve: {},
            }, options || {}));
          }

          scope.editDatum = function(model) {
            modal({
              templateUrl: 'home/forms/datum-edit.html',
              resolve: {
                data: function() {
                  return model ? Couch.get(model.id) : undefined;
                }
              },
              controller: function($scope, $modalInstance, data) {
                $scope.data = data || {
                  breadth: {},
                  location: {}
                };
                $scope.title = data ? 'Edit Datum' : 'New Datum';

                $scope.save = function() {
                  var instance = new Mossaic.models.Meta();
                  instance.set($scope.data);
                  instance.save(null, {
                    success: function(response) {
                      if (model) {
                        angular.extend(model, response);
                      } else {
                        model = response;
                        scope.data.meta.push(model);
                      }
                      model.value = $scope.data.name;
                      $scope.$apply(function() {
                        $modalInstance.close(model);
                      });
                    }
                  });
                };
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
                };
              }
            });
          };

          scope.deleteDatum = function(model) {
            if (confirm('Are you sure?')) {
              Couch.get(model.id).then(function(attributes) {
                var instance = new Mossaic.models.Meta();
                instance.set(attributes);
                instance.set('id', attributes._id);
                instance.destroy();
                clearData(model);
                scope.data.meta.splice(scope.data.meta.indexOf(model), 1);
              });
            }
          };

          scope.editCrossSection = function(model) {
            modal({
              templateUrl: 'home/forms/cross_section-edit.html',
              resolve: {
                data: function() {
                  return model ? Couch.get(model.id) : undefined;
                }
              },
              controller: function($scope, $modalInstance, data) {
                $scope.data = data || {
                  start: {},
                  end: {},
                  parent_id: scope.actives.meta.id
                };
                $scope.title = data ? 'Edit Cross Section' : 'New Cross Section';

                $scope.save = function() {
                  var instance = new Mossaic.models.CrossSection();
                  instance.set($scope.data);
                  instance.save(null, {
                    success: function(response) {
                      if (model) {
                        angular.extend(model, response);
                      } else {
                        model = response;
                        scope.data.cross_section.push(model);
                      }
                      model.value = $scope.data.name;
                      $scope.$apply(function() {
                        $modalInstance.close(model);
                      });
                    }
                  });
                };
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
                };
              }
            });
          };

          scope.deleteCrossSection = function(model) {
            if (confirm('Are you sure?')) {
              Couch.get(model.id).then(function(attributes) {
                var instance = new Mossaic.models.CrossSection();
                instance.set(attributes);
                instance.set('id', attributes._id);
                instance.destroy();
                clearData(model);
                scope.data.cross_section.splice(scope.data.cross_section.indexOf(model), 1);
              });
            }
          };

          scope.editGeometry = function(model, taskMode) {
            var url;
            if (model) {
              url = 'index.html#task-composer/edit-params/id/' + model.id + '/confirmation';
              if (taskMode && taskMode === true) {
                url = url + '?task_mode=true';
              } else {
                return modal({
                  templateUrl: 'home/forms/edit-geometry-confirm.html',
                  controller: function($scope, $modalInstance) {
                    $scope.edit = function() {
                      $modalInstance.dismiss('cancel');
                      window.location = url;
                    };
                    $scope.cancel = function() {
                      $modalInstance.dismiss('cancel');
                    };
                  }
                });
              }
            } else {
              window.router.state.geometry.clear();
              window.router.state.geometry.set('parent_id', scope.actives.cross_section.id);
              url = 'index.html#task-composer/edit-params/define/choose';
            }
            window.location = url;
          };

          scope.copyGeometry = function(model) {
            modal({
              templateUrl: 'home/forms/copy-geometry.html',
              controller: function($scope, $modalInstance) {
                $scope.data = {};
                $scope.createCopy = function() {
                  $scope.data.doingCopy = true;
                  doCopyGeometry(model, $scope.data.name, function() {
                    $modalInstance.dismiss('cancel');
                  });
                };
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
                };
              }
            });
          };
          /**
           * Copy an entire geometry!
           */
          function doCopyGeometry(model, name, cb) {
            // Instantiate our base model
            var id, base, copy;
            var promises = [];
            base = new Mossaic.models.Geometry();

            base.set(model);
            id = model.id;

            base.fetch({
              success: function() {


                // Create a copy of it
                copy = base.clone();
                copy.unset('id');
                copy.unset('_id');
                copy.unset('_rev');
                copy.unset('key');
                copy.set('parent_id', scope.actives.cross_section.id);
                copy.set('name', name);
                copy.set('value', name);
                copy.set('isNew', true);
                copy.set_complete('surface');
                copy.set_complete('soil_strata');
                copy.set_complete('water_table');

                // Get children of the base
                $.couch.db('chasm').view('gui/docs_by_parent', {
                  reduce: false,
                  descending: false,
                  include_docs: true,
                  skip: 0,
                  inclusive_end: true,
                  update_seq: false,
                  success: function(data) {

                    var soils = _.find(data.rows, function(item) {
                      return item.doc.type === 'soils' && item.doc.parent_id === id;
                    });
                    var boundary_conditions = _.find(data.rows, function(item) {
                      return item.doc.type === 'boundary_conditions' && item.doc.parent_id === id;
                    });
                    var stability = _.find(data.rows, function(item) {
                      return item.doc.type === 'stability' && item.doc.parent_id === id;
                    });

                    soils = new Mossaic.models.Soils(soils && soils.doc || {
                      parent_id: id
                    });
                    boundary_conditions = new Mossaic.models.BoundaryConditions(boundary_conditions && boundary_conditions.doc || {
                      parent_id: id
                    });
                    stability = new Mossaic.models.Stability(stability && stability.doc || {
                      parent_id: id
                    }, {
                      geometry: base
                    });
                    soils.unset('id');
                    soils.unset('_id');
                    soils.unset('_rev');
                    boundary_conditions.unset('id');
                    boundary_conditions.unset('_id');
                    boundary_conditions.unset('_rev');
                    stability.unset('id');
                    stability.unset('_id');
                    stability.unset('_rev');
                    // Save the copy
                    copy.save(null, {
                      success: function() {
                        var copyId = copy.get('id');
                        console.log('New ID', copyId);
                        soils.set('parent_id', copyId);
                        boundary_conditions.set('parent_id', copyId);
                        stability.set('parent_id', copyId);
                        $.when.apply($, _.invoke([soils, boundary_conditions, stability], 'save')).done(function() {
                          scope.$apply(function() {
                            scope.data.geometry.push(copy.attributes);
                            cb();
                          });
                        });
                      }
                    });
                  }
                });
              }
            });
          }

          scope.deleteGeometry = function(model) {
            if (confirm('Are you sure?')) {
              Couch.get(model.id).then(function(attributes) {
                var instance = new Mossaic.models.Geometry();
                instance.set(attributes);
                instance.set('id', attributes._id);
                instance.destroy();
                clearData(model);
                scope.data.geometry.splice(scope.data.geometry.indexOf(model), 1);
              });
            }
          };

          scope.newTask = function() {
            scope.editGeometry(scope.actives.geometry);
          };

          scope.selectDatum = function(datum) {
            datum.type = 'meta';
            clearData(datum);
            window.router.state.meta.set(datum);
            Couch.getByParent('cross_section', datum.id).then(function(values) {
              scope.data.cross_section = values;
            });
            setActive(datum, 'meta');
          };

          scope.selectCrossSection = function(crossSection) {
            crossSection.type = 'cross_section';
            clearData(crossSection);
            window.router.state.cross_section.set(crossSection);
            Couch.getByParent('geometry', crossSection.id).then(function(values) {
              scope.data.geometry = values;
            });
            setActive(crossSection, 'cross_section');
          };

          scope.selectGeometry = function(geometry) {
            geometry.type = 'geometry';
            clearData(geometry);
            window.router.state.geometry.set(geometry);
            Couch.getTasksByGeometry(geometry.id).then(function(values) {
              scope.data.task = values;
            });
            setActive(geometry, 'geometry');
          };

          scope.selectTask = function(task) {
            task.type = 'task';
            window.router.state.task.set(task);
            setActive(task, 'task');
          };

          scope.analyzeTask = function(task) {
            var encoded = encodeURI('#/task_analyser/analyse_task/id/' + task.id);
            console.log(encoded);
            window.location = encoded;
          };
        }
      };
    }]);
}());

/*
 * angular-ui-bootstrap
 * http://angular-ui.github.io/bootstrap/

 * Version: 0.14.2 - 2015-10-17
 * License: MIT
 */
angular.module("ui.bootstrap",["ui.bootstrap.tpls","ui.bootstrap.modal","ui.bootstrap.stackedMap"]),angular.module("ui.bootstrap.tpls",["template/modal/backdrop.html","template/modal/window.html"]),angular.module("ui.bootstrap.modal",["ui.bootstrap.stackedMap"]).factory("$$multiMap",function(){return{createNew:function(){var e={};return{entries:function(){return Object.keys(e).map(function(a){return{key:a,value:e[a]}})},get:function(a){return e[a]},hasKey:function(a){return!!e[a]},keys:function(){return Object.keys(e)},put:function(a,n){e[a]||(e[a]=[]),e[a].push(n)},remove:function(a,n){var t=e[a];if(t){var o=t.indexOf(n);-1!==o&&t.splice(o,1),t.length||delete e[a]}}}}}}).directive("uibModalBackdrop",["$animate","$injector","$uibModalStack",function(e,a,n){function t(a,t,r){t.addClass("modal-backdrop"),r.modalInClass&&(o?o(t,{addClass:r.modalInClass}).start():e.addClass(t,r.modalInClass),a.$on(n.NOW_CLOSING_EVENT,function(a,n){var l=n();o?o(t,{removeClass:r.modalInClass}).start().then(l):e.removeClass(t,r.modalInClass).then(l)}))}var o=null;return a.has("$animateCss")&&(o=a.get("$animateCss")),{replace:!0,templateUrl:"template/modal/backdrop.html",compile:function(e,a){return e.addClass(a.backdropClass),t}}}]).directive("uibModalWindow",["$uibModalStack","$q","$animate","$injector",function(e,a,n,t){var o=null;return t.has("$animateCss")&&(o=t.get("$animateCss")),{scope:{index:"@"},replace:!0,transclude:!0,templateUrl:function(e,a){return a.templateUrl||"template/modal/window.html"},link:function(t,r,l){r.addClass(l.windowClass||""),r.addClass(l.windowTopClass||""),t.size=l.size,t.close=function(a){var n=e.getTop();n&&n.value.backdrop&&"static"!==n.value.backdrop&&a.target===a.currentTarget&&(a.preventDefault(),a.stopPropagation(),e.dismiss(n.key,"backdrop click"))},r.on("click",t.close),t.$isRendered=!0;var s=a.defer();l.$observe("modalRender",function(e){"true"==e&&s.resolve()}),s.promise.then(function(){var s=null;l.modalInClass&&(s=o?o(r,{addClass:l.modalInClass}).start():n.addClass(r,l.modalInClass),t.$on(e.NOW_CLOSING_EVENT,function(e,a){var t=a();o?o(r,{removeClass:l.modalInClass}).start().then(t):n.removeClass(r,l.modalInClass).then(t)})),a.when(s).then(function(){var e=r[0].querySelectorAll("[autofocus]");e.length?e[0].focus():r[0].focus()});var i=e.getTop();i&&e.modalRendered(i.key)})}}}]).directive("uibModalAnimationClass",function(){return{compile:function(e,a){a.modalAnimation&&e.addClass(a.uibModalAnimationClass)}}}).directive("uibModalTransclude",function(){return{link:function(e,a,n,t,o){o(e.$parent,function(e){a.empty(),a.append(e)})}}}).factory("$uibModalStack",["$animate","$timeout","$document","$compile","$rootScope","$q","$injector","$$multiMap","$$stackedMap",function(e,a,n,t,o,r,l,s,i){function d(){for(var e=-1,a=h.keys(),n=0;n<a.length;n++)h.get(a[n]).value.backdrop&&(e=n);return e}function u(e,a){var t=n.find("body").eq(0),o=h.get(e).value;h.remove(e),p(o.modalDomEl,o.modalScope,function(){var a=o.openedClass||b;w.remove(a,e),t.toggleClass(a,w.hasKey(a)),c(!0)}),m(),a&&a.focus?a.focus():t.focus()}function c(e){var a;h.length()>0&&(a=h.top().value,a.modalDomEl.toggleClass(a.windowTopClass||"",e))}function m(){if($&&-1==d()){var e=C;p($,C,function(){e=null}),$=void 0,C=void 0}}function p(a,n,t){function o(){o.done||(o.done=!0,v?v(a,{event:"leave"}).start().then(function(){a.remove()}):e.leave(a),n.$destroy(),t&&t())}var l,s=null,i=function(){return l||(l=r.defer(),s=l.promise),function(){l.resolve()}};return n.$broadcast(k.NOW_CLOSING_EVENT,i),r.when(s).then(o)}function f(e,a,n){return!e.value.modalScope.$broadcast("modal.closing",a,n).defaultPrevented}var v=null;l.has("$animateCss")&&(v=l.get("$animateCss"));var $,C,g,b="modal-open",h=i.createNew(),w=s.createNew(),k={NOW_CLOSING_EVENT:"modal.stack.now-closing"},y=0,S="a[href], area[href], input:not([disabled]), button:not([disabled]),select:not([disabled]), textarea:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable=true]";return o.$watch(d,function(e){C&&(C.index=e)}),n.bind("keydown",function(e){if(e.isDefaultPrevented())return e;var a=h.top();if(a&&a.value.keyboard)switch(e.which){case 27:e.preventDefault(),o.$apply(function(){k.dismiss(a.key,"escape key press")});break;case 9:k.loadFocusElementList(a);var n=!1;e.shiftKey?k.isFocusInFirstItem(e)&&(n=k.focusLastFocusableElement()):k.isFocusInLastItem(e)&&(n=k.focusFirstFocusableElement()),n&&(e.preventDefault(),e.stopPropagation())}}),k.open=function(e,a){var r=n[0].activeElement,l=a.openedClass||b;c(!1),h.add(e,{deferred:a.deferred,renderDeferred:a.renderDeferred,modalScope:a.scope,backdrop:a.backdrop,keyboard:a.keyboard,openedClass:a.openedClass,windowTopClass:a.windowTopClass}),w.put(l,e);var s=n.find("body").eq(0),i=d();if(i>=0&&!$){C=o.$new(!0),C.index=i;var u=angular.element('<div uib-modal-backdrop="modal-backdrop"></div>');u.attr("backdrop-class",a.backdropClass),a.animation&&u.attr("modal-animation","true"),$=t(u)(C),s.append($)}var m=angular.element('<div uib-modal-window="modal-window"></div>');m.attr({"template-url":a.windowTemplateUrl,"window-class":a.windowClass,"window-top-class":a.windowTopClass,size:a.size,index:h.length()-1,animate:"animate"}).html(a.content),a.animation&&m.attr("modal-animation","true");var p=t(m)(a.scope);h.top().value.modalDomEl=p,h.top().value.modalOpener=r,s.append(p),s.addClass(l),k.clearFocusListCache()},k.close=function(e,a){var n=h.get(e);return n&&f(n,a,!0)?(n.value.modalScope.$$uibDestructionScheduled=!0,n.value.deferred.resolve(a),u(e,n.value.modalOpener),!0):!n},k.dismiss=function(e,a){var n=h.get(e);return n&&f(n,a,!1)?(n.value.modalScope.$$uibDestructionScheduled=!0,n.value.deferred.reject(a),u(e,n.value.modalOpener),!0):!n},k.dismissAll=function(e){for(var a=this.getTop();a&&this.dismiss(a.key,e);)a=this.getTop()},k.getTop=function(){return h.top()},k.modalRendered=function(e){var a=h.get(e);a&&a.value.renderDeferred.resolve()},k.focusFirstFocusableElement=function(){return g.length>0?(g[0].focus(),!0):!1},k.focusLastFocusableElement=function(){return g.length>0?(g[g.length-1].focus(),!0):!1},k.isFocusInFirstItem=function(e){return g.length>0?(e.target||e.srcElement)==g[0]:!1},k.isFocusInLastItem=function(e){return g.length>0?(e.target||e.srcElement)==g[g.length-1]:!1},k.clearFocusListCache=function(){g=[],y=0},k.loadFocusElementList=function(e){if((void 0===g||!g.length)&&e){var a=e.value.modalDomEl;a&&a.length&&(g=a[0].querySelectorAll(S))}},k}]).provider("$uibModal",function(){var e={options:{animation:!0,backdrop:!0,keyboard:!0},$get:["$injector","$rootScope","$q","$templateRequest","$controller","$uibModalStack",function(a,n,t,o,r,l){function s(e){return e.template?t.when(e.template):o(angular.isFunction(e.templateUrl)?e.templateUrl():e.templateUrl)}function i(e){var n=[];return angular.forEach(e,function(e){n.push(angular.isFunction(e)||angular.isArray(e)?t.when(a.invoke(e)):angular.isString(e)?t.when(a.get(e)):t.when(e))}),n}var d={},u=null;return d.getPromiseChain=function(){return u},d.open=function(a){function o(){return v}var d=t.defer(),c=t.defer(),m=t.defer(),p={result:d.promise,opened:c.promise,rendered:m.promise,close:function(e){return l.close(p,e)},dismiss:function(e){return l.dismiss(p,e)}};if(a=angular.extend({},e.options,a),a.resolve=a.resolve||{},!a.template&&!a.templateUrl)throw new Error("One of template or templateUrl options is required.");var f,v=t.all([s(a)].concat(i(a.resolve)));return f=u=t.all([u]).then(o,o).then(function(e){var t=(a.scope||n).$new();t.$close=p.close,t.$dismiss=p.dismiss,t.$on("$destroy",function(){t.$$uibDestructionScheduled||t.$dismiss("$uibUnscheduledDestruction")});var o,s={},i=1;a.controller&&(s.$scope=t,s.$modalInstance=p,angular.forEach(a.resolve,function(a,n){s[n]=e[i++]}),o=r(a.controller,s),a.controllerAs&&(a.bindToController&&angular.extend(o,t),t[a.controllerAs]=o)),l.open(p,{scope:t,deferred:d,renderDeferred:m,content:e[0],animation:a.animation,backdrop:a.backdrop,keyboard:a.keyboard,backdropClass:a.backdropClass,windowTopClass:a.windowTopClass,windowClass:a.windowClass,windowTemplateUrl:a.windowTemplateUrl,size:a.size,openedClass:a.openedClass}),c.resolve(!0)},function(e){c.reject(e),d.reject(e)}).finally(function(){u===f&&(u=null)}),p},d}]};return e}),angular.module("ui.bootstrap.modal").value("$modalSuppressWarning",!1).directive("modalBackdrop",["$animate","$injector","$modalStack","$log","$modalSuppressWarning",function(e,a,n,t,o){function r(a,r,s){o||t.warn("modal-backdrop is now deprecated. Use uib-modal-backdrop instead."),r.addClass("modal-backdrop"),s.modalInClass&&(l?l(r,{addClass:s.modalInClass}).start():e.addClass(r,s.modalInClass),a.$on(n.NOW_CLOSING_EVENT,function(a,n){var t=n();l?l(r,{removeClass:s.modalInClass}).start().then(t):e.removeClass(r,s.modalInClass).then(t)}))}var l=null;return a.has("$animateCss")&&(l=a.get("$animateCss")),{replace:!0,templateUrl:"template/modal/backdrop.html",compile:function(e,a){return e.addClass(a.backdropClass),r}}}]).directive("modalWindow",["$modalStack","$q","$animate","$injector","$log","$modalSuppressWarning",function(e,a,n,t,o,r){var l=null;return t.has("$animateCss")&&(l=t.get("$animateCss")),{scope:{index:"@"},replace:!0,transclude:!0,templateUrl:function(e,a){return a.templateUrl||"template/modal/window.html"},link:function(t,s,i){r||o.warn("modal-window is now deprecated. Use uib-modal-window instead."),s.addClass(i.windowClass||""),s.addClass(i.windowTopClass||""),t.size=i.size,t.close=function(a){var n=e.getTop();n&&n.value.backdrop&&"static"!==n.value.backdrop&&a.target===a.currentTarget&&(a.preventDefault(),a.stopPropagation(),e.dismiss(n.key,"backdrop click"))},s.on("click",t.close),t.$isRendered=!0;var d=a.defer();i.$observe("modalRender",function(e){"true"==e&&d.resolve()}),d.promise.then(function(){var o=null;i.modalInClass&&(o=l?l(s,{addClass:i.modalInClass}).start():n.addClass(s,i.modalInClass),t.$on(e.NOW_CLOSING_EVENT,function(e,a){var t=a();l?l(s,{removeClass:i.modalInClass}).start().then(t):n.removeClass(s,i.modalInClass).then(t)})),a.when(o).then(function(){var e=s[0].querySelectorAll("[autofocus]");e.length?e[0].focus():s[0].focus()});var r=e.getTop();r&&e.modalRendered(r.key)})}}}]).directive("modalAnimationClass",["$log","$modalSuppressWarning",function(e,a){return{compile:function(n,t){a||e.warn("modal-animation-class is now deprecated. Use uib-modal-animation-class instead."),t.modalAnimation&&n.addClass(t.modalAnimationClass)}}}]).directive("modalTransclude",["$log","$modalSuppressWarning",function(e,a){return{link:function(n,t,o,r,l){a||e.warn("modal-transclude is now deprecated. Use uib-modal-transclude instead."),l(n.$parent,function(e){t.empty(),t.append(e)})}}}]).service("$modalStack",["$animate","$timeout","$document","$compile","$rootScope","$q","$injector","$$multiMap","$$stackedMap","$uibModalStack","$log","$modalSuppressWarning",function(e,a,n,t,o,r,l,s,i,d,u,c){c||u.warn("$modalStack is now deprecated. Use $uibModalStack instead."),angular.extend(this,d)}]).provider("$modal",["$uibModalProvider",function(e){angular.extend(this,e),this.$get=["$injector","$log","$modalSuppressWarning",function(a,n,t){return t||n.warn("$modal is now deprecated. Use $uibModal instead."),a.invoke(e.$get)}]}]),angular.module("ui.bootstrap.stackedMap",[]).factory("$$stackedMap",function(){return{createNew:function(){var e=[];return{add:function(a,n){e.push({key:a,value:n})},get:function(a){for(var n=0;n<e.length;n++)if(a==e[n].key)return e[n]},keys:function(){for(var a=[],n=0;n<e.length;n++)a.push(e[n].key);return a},top:function(){return e[e.length-1]},remove:function(a){for(var n=-1,t=0;t<e.length;t++)if(a==e[t].key){n=t;break}return e.splice(n,1)[0]},removeTop:function(){return e.splice(e.length-1,1)[0]},length:function(){return e.length}}}}}),angular.module("template/modal/backdrop.html",[]).run(["$templateCache",function(e){e.put("template/modal/backdrop.html",'<div uib-modal-animation-class="fade"\n     modal-in-class="in"\n     ng-style="{\'z-index\': 1040 + (index && 1 || 0) + index*10}"\n></div>\n')}]),angular.module("template/modal/window.html",[]).run(["$templateCache",function(e){e.put("template/modal/window.html",'<div modal-render="{{$isRendered}}" tabindex="-1" role="dialog" class="modal"\n    uib-modal-animation-class="fade"\n    modal-in-class="in"\n    ng-style="{\'z-index\': 1050 + index*10, display: \'block\'}">\n    <div class="modal-dialog" ng-class="size ? \'modal-\' + size : \'\'"><div class="modal-content" uib-modal-transclude></div></div>\n</div>\n')}]);
(function() {
  'use strict';
  angular.module('chasm.core',['ui.router','chasm.templates','ui.bootstrap']);
}());

(function() {
  'use strict';
  angular
    .module('chasm.core')
    .directive('listGroupItem', function() {
      return {
        restrict: 'C',
        link: function(scope, element) {
          element.bind('click', function() {
            if (element.hasClass('active')) {
              element.removeClass('active');
            } else {
              element.addClass('active').siblings().removeClass('active');
            }
          });
        }
      };
    });
}());

(function() {
  'use strict';
  angular
    .module('chasm.core')
    .factory('Couch',Couch);
  function Couch($http){
    var api = {
      getByType: getByType,
      getByParent: getByParent,
      getTasksByGeometry: getTasksByGeometry,
      get: get
    };

    return api;

    function getByType(type){
      var url = '_view/docs_by_type?descending=false&skip=0&reduce=false&include_docs=false&inclusive_end=true&update_seq=false&key=\"' + type + '\"';
      return $http.get(url).then(function(response){
        return response.data.rows;
      });
    }

    function getByParent(type,id){
      var s = '["'+type+'","'+id+'"]';
      var e = '["'+type+'","'+id+'",{}]';
      var url = '_view/docs_by_parent?descending=false&skip=0&reduce=false&include_docs=false&inclusive_end=true&update_seq=false&startkey='+s+'&endkey='+e;
      return $http.get(url).then(function(response){
        return response.data.rows;
      });
    }

    function getTasksByGeometry(id){
      var url = '/chasm_task/_design/workflow/_view/tasks_by_slope?descending=false&skip=0&reduce=false&include_docs=false&inclusive_end=true&update_seq=false&startkey=%5B"'+id+'"%5D&endkey=%5B"'+id+'"%2C%7B%7D%5D';
      return $http.get(url).then(function(response){
        return response.data.rows;
      });
    }

    function get(id){
      return $http.get('/chasm/'+id).then(function(response){
        return response.data;
      });
    }
  }
  Couch.$inject = ['$http'];
}());

(function() {
  'use strict';
  angular.module('chasm',[
    'chasm.core',
    'chasm.home'
  ]);
}());
