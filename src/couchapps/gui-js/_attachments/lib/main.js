require.config({
  paths: {
    text: "../vendor/require/text",
    domReady: "../vendor/require/domReady"
  }
});

require(['domReady!', "router"],
        function(document, Router) {
          var url = unescape(document.location.href).split('/');
          var login_db = url[3] + "_login";
          var login_url = [url[0], url[1], url[2], login_db,
            ["_design/login/index.html"]].join("/");
          var docs_db = url[3] + "_docs";
          $("#login_widget").couchLogin({
            loggedOut: function() {
              window.location.href = login_url;
            }
          });
          var docs_url = [url[0], url[1], url[2], docs_db, ["_design/docs/index"]]
                  .join("/");
          $("#link_to_docs").html('<a href="' + docs_url + '">Need help?</a>');
          var retries = 0;

          // Save the users info into Mossaic object..
          $.couch.session({
            success: function(response) {
              Mossaic.session = {};
              Mossaic.session.userCtx = response.userCtx;
            }
          });

          var db = Backbone.couch.db();
          var ddoc_uri = "_design/" + Backbone.couch.ddocName;
          db.getDbProperty(ddoc_uri, {
            success: function(doc) {
              // We've now got the templates, so can crack on with the rest of the app.

              /**
               * JS representation of the application design document
               * @type object
               */
              Mossaic.db.ddoc = doc;

              // Recursively add the Mustache templates.  Folders in the Couchapp are seperated with dots and we
              // don't need the html extension so e.g.  the template "widgets/button.html" becomes "widget.button"
              var add_templates_to_mustache = function(element, parent) {
                for (var key in element) {
                  if (typeof(element[key]) === 'object') {
                    add_templates_to_mustache(element[key], parent + "." + key);
                  } else {
                    $.Mustache.add((parent + "." + key).substring(1), element[key]);  // Remove the leading "."
                  }
                }
              };
              add_templates_to_mustache(doc.templates, "");

              // Launch the router which does the rest of the work
              window.router = new Router({container: "#mossaic-app"});
              Backbone.history.start();

            },
            error: function() {
              console.log("Could not fetch design document", ddoc_uri);
            }
          });
        }
);
