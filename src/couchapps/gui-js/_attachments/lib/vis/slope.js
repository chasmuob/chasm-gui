// Legacy slope graphic, but this is possibly still used by non-task_composer screens?

/**
 * @file Define the visualisation for slope profile/geometry data
 */
(function () {
  /**
   * Default offset for determining soil colors
   * @constant
   * @type integer
   */
  var SOIL_COLOR_OFFSET = 11;
  /**
   * Backbone View which provides a graphical display of a
   * Mossaic.models.Geometry model and allows point and click editing.
   *
   * This View uses three different coordinate systems.
   *  - model space -> The coordinate system used in the model domain (the same
   *                   domain used by chasm and questa)
   *  - svg space -> The coordinates system used for drawing the model into an
   *                 SVG element
   *  - screen space - > The coordinates system used by the browser to receive
   *                     user click events, etc
   * @constructor
   * @extends Backbone.View
   */
  Mossaic.vis.Slope = Backbone.View.extend({
    /**
     * Initialise the view. Called implicitly by "new Mossaic.vis.Slope"
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @param {Mossaic.models.Geometry} model Geometry model to be displayed
     * @params {object} options Hash of options
     * @params {bool} options.draw_slope Enable slope drawing mode                                        
     * @params {string} options.divname CSS selector of the div to which
     *                                  the view be appended
     * @param {object} options.quantize {boolean} quantize Quantize to the
     *                                                     mesh defined in the
     *                                                     model
     */
    initialize: function(model, options) {
      var that = this;
      _.bindAll(this);
      /**
       * The model to be represented by this visualisation.
       * @type Mossaic.models.Geometry
       */
      this.model = model;
      this.model.bind('change', function() {
        if (that.model.changed_by != that) that.redraw();
      })
      /**
       * CSS selector of the div to which this view renders
       * @type string
       */
      this.divname = options.divname || "slope_display";
      /**
       * CSS celector of the parent div that holds this.divname
       * @type string
       */
      this.parent_divname = options.parent_divname,
      /**
       * Selection of the div to which the view renders
       * @type d3.selection
       */
      this.div = d3.select(this.divname);
      /**
       * Desired margin for the visualisation
       * @type integer
       */
      this.margin = 30;
      
      
      /**
       * Whether Mossaic.models.Geometry coordinates should be quantized to
       * the mesh or not
       */
      this.quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        this.quantize = options.quantize;
      }
      /**
       * Reference to DOM element that triggered the current event. Used to
       * support dragging events, etc.
       * @type d3.event.target
       */
      this.target = undefined;
      var soil_domain = (function () { 
        var d = [];
        for (var i = 0; i < 20; ++i) {
          d.push(i);
        }
        return d;
      })();
      /**
       * Soil color scale
       * @type d3.scale
       */
      this.soil_colors = d3.scale.category20b()
        .domain(soil_domain);
      /**
       * Function to be called when drawing is complete
       * @type function
       */
      this.on_draw_callback;
    },
    /**
     * Show the visualisation
     * @memberof Mossaic.vis.Slope
     * @instance
     */
    show: function() {
      this.div = d3.select(this.divname);
      if (this.vis) this.vis.remove();
      delete this.vis;
      this.draw();
    },

    /**
     * Hide the visualisation
     * @memberof Mossaic.vis.Slope
     * @instance
     */
    hide: function() {
      if (this.vis) this.vis.remove();
      delete this.vis;
    },

    /**
     * Set the model to be represented by the form.
     * @memberof Mossaic.vis.Slope
     * @instance
     * @param {Mossaic.models.Geometry} model Model to be represented
     *                                        by this visualisation
     */
    set_model: function(model) {
      this.model = model;
      this.coords = [];
      this.model.bind("change", this.redraw);
    },
    /**
     * Set a callback function to be triggered when the display is drawn.
     * This allows another widget (the stability grid) to redraw itself
     * taking into account anything that might have changed.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @param {function} func Function to be triggered after drawing
     */
    on_draw: function(func) {
      this.on_draw_callback = func;
    },
          
    /**
     * Sets a callback function to be triggered when the user ends drawing a slope freehand
     * @memberof Mossaic.vis.Slope
     * @param {function} Function
     */
    on_drawable_ended: function(func) {
      this.on_drawable_ended_callback = func;
    },
          
    /**
     * Determine whether a coordinate at index is moveable.
     *
     * The coordinates at the beginning and end of the user-defined sections
     * cannot be moved, neither can any autogenerated coordinates.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {int} index Index of coordinate under test
     * @return {boolean} true if coordinate is moveable, false otherwise
     */
    is_moveable: function(index) {
      var total_coords = this.coords.length;
      return (index > this.model.get_number_of_extra_sections().number_of_top_sections &&
          index < total_coords - this.model.get_number_of_extra_sections().number_of_bottom_sections - 1);
    },
    /**
     * Determine whether a coordinate at index is selectable.
     *
     * Selecting only happens when users choose a section to be the datum.
     * This must be a user-defined section. Coordinates describing other
     * sections are not selectable.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {int} index Index of coordinate under test
     * @return {boolean} true if coordinate is selectable, false otherwise
     */
    is_selectable: function(index) {
      var total_coords = this.coords.length;
      return (index > this.model.get_number_of_extra_sections().number_of_top_sections &&
          index < total_coords - this.model.get_number_of_extra_sections().number_of_bottom_sections);
    },
    /**
     * Create a clip path using the slope coordinates and the bottom of the
     * slope. The clip path will have id #slope_clip and may be used by other
     * Views that also draw to this View.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     */
    set_clip_path: function() {
      var coords = this.coords;
      var line = this.line;
      var draw_clip_path = function(coords) {
        var clip_coords = [{x: 0, y: 0}];
        clip_coords.push.apply(clip_coords, coords);
        clip_coords.push({x: 0, y: 0});
        var l = line(clip_coords);
        return l;
      }
      var clip_path = this.g.select("#slope_clip");
      if (!clip_path[0][0]) {
        clip_path = this.g.append("svg:clipPath").attr("id", "slope_clip");
      }
      clip_path.selectAll("path").remove();
      clip_path.selectAll("path")
        .data([coords]).enter().append("svg:path")
        .attr("d", draw_clip_path);
    },
    /**
     * Update the profile sections in the Mossaic.models.Geometry model from
     * the current set of coordinates.
     *
     * Note that the coordinates are the slope coordinates in the model
     * coordinate system. They were originally calculated from the
     * model slope profile sections but may have been changed through user
     * interaction with the view.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {array} coords Array of coordinates where each coordinate is an
     *                       object with x and y fields.
     */
    update_sections: function(coords) {
      var coords = (this.quantize && this.model.quantize(coords)) || coords;
      var geometry = _.clone(this.model.get("geometry"));
      var slope_sections = _.clone(geometry.slope_sections);
      for (var i = 0; i < slope_sections.length; ++i) {
        i_c = this.model.section_index_to_coords_index(i);
        var section = _.clone(slope_sections[i]);
        section.surface_height = _.clone(section.surface_height);
        section.angle = _.clone(section.angle);
        section.surface_height.value = coords[i_c].y - coords[i_c + 1].y;
        if (section.surface_height.value == 0) {
          section.angle.value = 0;
        } else {
          section.angle.value = Math.atan(section.surface_height.value /
              Math.abs(coords[i_c + 1].x - coords[i_c].x)) / Mossaic.utils.degs_to_rads;
        }
        var surface_length = undefined;
        if (section.surface_height.value == 0) {
          surface_length = Math.abs(coords[i_c + 1].x - coords[i_c].x);
        }
        if (coords[i_c + 1].x - coords[i_c].x > 0) {
          section.surface_length.value = surface_length || section.surface_height.value /
              Math.sin(Mossaic.utils.degs_to_rads * section.angle.value);
        } else {
          section.surface_length.value = surface_length || section.surface_height.value /
              Math.sin(Math.PI - Mossaic.utils.degs_to_rads * section.angle.value);
        }
        slope_sections[i] = section;
      }
      geometry.slope_sections = slope_sections;
      this.model.changed_by = this;
      this.model.set({geometry: geometry});
      for (var i = 0; i < this.water_depths.length; ++i) {
        this.model.set_water_depth(this.water_depths[i].depth, {
          index: i,
          surface_distance_from_cut_toe: this.water_depths[i].surface_distance_from_cut_toe,
          changed_by: this
        });
      }
      var soil_depths_by_section = this.model.get_soil_depths_by_section({
        soil_depths: this.soil_depths
      });
      for (var i = 0; i < soil_depths_by_section.length; ++i) {
        var depths = _.map(soil_depths_by_section[i], function(depth) {
          return depth.depth;
        });
        this.model.set_soil_depth(depths, undefined, {
          index: i,
          surface_distance_from_cut_toe: soil_depths_by_section[i].surface_distance_from_cut_toe,
          changed_by: this
        });
      }
      return this;
    },
    /**
     * Get the coordinates of an event that occured in the screen coordinate
     * system in the svg coordinate system.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {d3.event.target} target The event target
     * @param {d3.event} event D3 event object containing screen coordinates
     * @return Coordinates of event in svg space
     */
    screen_to_svg_transform: function(target, event) {
      // Get current transform matrix (why the [0][0] ?)
      var ctm = d3.select(target)[0][0].getScreenCTM();
      var point = this.vis[0][0].createSVGPoint();
      point.x = event.clientX;
      point.y = event.clientY;
      return transformed_point = point.matrixTransform(ctm.inverse());
    },
    /**
     * Quantize a point in svg coordinates to the mesh defined in the
     * Mossaic.models.Geometry model
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} point Coordinates in svg space
     * @return {object} Coordinates quantized to the model mesh, in svg space
     */
    quantize_svg: function(point) {
      var mesh = this.model.get_mesh();
      var svg_grid_x = this.x(mesh.x) - this.x(0);
      var svg_grid_y = this.y(mesh.y) - this.y(0);
      point.x = Math.round((point.x - this.x(0)) / svg_grid_x) * svg_grid_x +
                                                                      this.x(0);
      point.y = Math.round((point.y + this.y(0)) / svg_grid_y) * svg_grid_y -
                                                                      this.y(0);
      return point;
    },
    /**
     * Update a point on the screen in response to a user-generated event
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {d3.event.target} target Event target
     * @param {d3.event} event D3 event object containing new screen coordinates
     * @param {object} options Hash of options
     * @param {object} options.quantize {boolean} quantize Quantize point to the
     *                                                     mesh defined in the
     *                                                     model
     */
    update_point_on_screen: function(target, event, options) {
      var options = options || {};
      var lock = options.lock || {};
      var quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        quantize = options.quantize;
      }
      var transformed_point = this.screen_to_svg_transform(target, event);
      if (quantize) {
        var point = this.quantize_svg(transformed_point);
      } else {
        var point = transformed_point;
      }
      if (!lock.x) {
        d3.select(target).attr("cx", point.x);
      }
      if (!lock.y) {
        d3.select(target).attr("cy", point.y);
      }
    },
    /**
     * Update depth points on the screen in response to a user generated event
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {d3.event.target} target Event target
     * @param {array} coords Slope surface coordinates
     * @param {array} depths Depth measurements for each soil strata interface
     * @param {integer} index Index of soil strata interface to be updated
     * @param {String} css_class CSS class used to select the depth measurements
     */
    update_depths_on_screen: function(target, coords, depths, index, css_class){
      var that = this;
      var new_y = this.y.invert(-1 * d3.select(target).attr("cy"));
      // depth = difference between new_y and the polyline
      // Extract the comptue_depth_x_y function to the bit that calculates the line x and y
      // Then we just call that
      // TODO Code debt: By using a dummy depth equal to zero we can get the y coord of the relevant
      // polyline section. This is naughty, but will do for now.
      var dummy_depth = {depth: 0, distance: depths[index].distance};
      var polyline_y = this.model.compute_depth_x_y(dummy_depth, coords, {
          quantize: this.quantize
      }).y;
      depths[index] = {depth: polyline_y - new_y, distance: depths[index].distance};
      this.g.selectAll("." + css_class)
        .data(depths)
          .attr("d", function(d, i) {
            return that.draw_depth_section(d, i, depths, coords, {
                quantize: that.quantize});
          });
    },
    /**
     * Update View coordinates (in model space) in response to a user generated
     * event. We get the svg coordinate of the target element, convert it to
     * model space, and update the view coordinates with the new values.
     *
     * Then the slope is redrawn, depth coordinates are recalculated, and depth
     * sections are redrawn.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {d3.event.target} target Event target
     * @param {array} coords Array of slope profile coordinates
     * @param {integer} index Index of slope coordinate to be updated
     */
    update_coords: function(target, coords, index) {
      var that = this;
      var quantize_depths = this.quantize;
      var new_x, new_y;
      if (target !== null) {
        var new_x = this.x.invert(d3.select(target).attr("cx"));
        var new_y = this.y.invert(-1 * d3.select(target).attr("cy"));
        coords[index] = {x: new_x, y: new_y};
      }
      var coords_for_depth_draw = (this.quantize && this.model.quantize(coords)) ||
          coords;
      this.g.selectAll(".slope_sections")
        .data(coords)
          .attr("d", this.draw_slope_section);
      this.g.selectAll(".water_table")
        .data(this.water_depths)
          .attr("d", function(d, i) {
            return that.draw_depth_section(d, i, that.water_depths, coords_for_depth_draw, {quantize: quantize_depths});
          });
      this.g.selectAll(".water_table_points")
        .data(this.water_depths)
          .attr("cx", function(d) { return that.x(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).x); })
          .attr("cy", function(d) { return -1 * that.y(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).y); })
      var total_strata = this.soil_depths.length;
      for (var strata = 0; strata < total_strata; ++strata) {
        (function() { // Wrap in a function so depths has scope
          var depths = that.soil_depths[strata];
          if (that.model.is_soil_known()) {
            that.g.selectAll("." + "soil_strata_" + strata)
              .data(depths)
                .attr("d", function(d, i) {
                  return that.draw_depth_section(d, i, depths, coords_for_depth_draw, {quantize: quantize_depths});
                });
            that.g.selectAll("." + "soil_strata_" + strata + "_points")
              .data(depths)
                .attr("cx", function(d) { return that.x(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).x); })
                .attr("cy", function(d) { return -1 * that.y(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).y); });
          } else {
            that.g.selectAll("." + "soil_strata_" + strata + "_point")
              .data([depths])
                .attr("cx", function(d) { return that.x(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).x); })
                .attr("cy", function(d) { return -1 * that.y(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).y); });
            that.g.selectAll("." + "soil_strata_" + strata)
              .data([depths])
                .attr("d", function(d, i) {
                  return that.draw_estimate_line(d, i, coords_for_depth_draw, quantize_depths);
                });
          }
        })();
      }
    },
    /**
     * Draw estimate line
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} d Depth estimate
     * @param {integer} i Index of depth
     * @param {array} coords Array of slope coordinates
     * @param {boolean} quantize Quantize the coordinates of the estimate line
     */
    draw_estimate_line: function(d, i, coords, quantize) {
      if (typeof(d.angle) === "undefined" ||
          typeof(d.depth) === "undefined" ||
          typeof(d.distance) === "undefined") {
        return;
      }
      // Get estimate origin x y in model space
      var estimate_origin = this.model.compute_depth_x_y(d, coords, {
        quantize: quantize
      });
      var estimate = this.model.compute_estimate_coords(d, estimate_origin,
          coords);
      return this.line([estimate.start, estimate.end]);
    },
    /**
     * Draw estimated depth lines
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} options Hash of options
     * @param {boolean} options.quantize Quantize the line coordinates
     * @param {array} options.coords Array of slope surface coordinates
     * @param {array} options.depths Array of depth estimates
     * @param {string} options.css_class CSS class to be applied to the line
     * @param {string} options.stroke Line color
     * @param {string} options.stroke_widget Line width
     */
    draw_depth_estimate: function(options) {
      var that = this;
      var quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        quantize = options.quantize;
      }
      var coords = options.coords;
      var depths = options.depths;
      var css_class = options.css_class;
      var stroke = options.stroke || "steelblue";
      var stroke_width = options.stroke_width || "3px";

      this.g.selectAll("." + css_class)
        .data([depths])
        .enter().append("svg:path")
          .attr("d", function(d, i) {
            return that.draw_estimate_line(d, i, coords, quantize);
          })
          .attr("class", css_class)
          .attr("clip-path", "url(#slope_clip)")
          .style("stroke", stroke)
          .style("stroke-width", stroke_width);

      this.g.selectAll("." + css_class + "_point")
        .data([depths])
        .enter().append("svg:circle")
          .attr("class", css_class + "_point")
          .attr("cx", function(d) {
            return that.x(that.model.compute_depth_x_y(
              d, coords, {quantize: quantize}).x);
          })
          .attr("cy", function(d) {
            return -1 * that.y(that.model.compute_depth_x_y(
              d, coords, {quantize: quantize}).y);
          })
          .attr("r", 2)
          .style("fill", "#A64D4D");
    },
    /**
     * Draw depth line with points
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} options Hash of options
     * @param {array} options.coords Array of slope surface coordinates
     * @param {array} options.depths Array of depth measurements
     * @param {string} options.css_class CSS class to be used for depth line elements
     * @param {string} options.stroke Stroke color for depth line
     * @param {string} options.stroke_width Width of depth line
     * @param {boolean} options.quantize Quantize depth line to model mesh
     */
    draw_depth_line: function(options) {
      var that = this;
      var quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        quantize = options.quantize;
      }
      var coords = options.coords;
      var depths = options.depths;
      var css_class = options.css_class;
      var stroke = options.stroke || "steelblue";
      var stroke_width = options.stroke_width || "3px";
      var editable_test = options.editable_test || function() { return true };

      this.g.selectAll("." + css_class)
        .data(depths)
        .enter().append("svg:path")
          .attr("d", function (d, i) {
            return that.draw_depth_section(d, i, depths, coords, {
                quantize: quantize});
          })
          .attr("class", css_class)
          .attr("clip-path", "url(#slope_clip)")
          .style("stroke", stroke)
          .style("stroke-width", stroke_width);

      this.g.selectAll("circle").select("." + css_class + "_points")
        .data(depths)
        .enter().append("svg:circle")
          .attr("class", "node")
          .attr("cx", function(d) { return that.x(that.model.compute_depth_x_y(d, coords, {quantize: quantize}).x); })
          .attr("cy", function(d) {
            if (!isNaN(d.depth) && typeof(d.depth) !== "undefined") {
              return -1 * that.y(that.model.compute_depth_x_y(d, coords, {quantize: quantize}).y);
            } else {
              return -1 * that.y(0);
            }
          })
          .attr("r", 5)
          .attr("class", css_class + "_points")
          .style("fill", "#A64D4D");
      this.g.selectAll("." + css_class + "_points")
        .on("mouseover", function(d, i) {
          if (editable_test() && !that.target) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 8);
          }
        })
        .on("mouseout", function(d, i) {
          if (!that.target) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 5);
          }
        })
        .on("mousedown", function(d, i) {
          if (editable_test()) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 8);
            that.target = d3.event.target;
            that.vis
              .on("mousemove", function(d, j) {
                if (that.target) {
                  that.update_point_on_screen(that.target, d3.event, {
                      lock: {x: true}, quantize: that.quantize
                  });
                  that.update_depths_on_screen(that.target, coords, depths, i,
                      css_class);
                  that.update_sections(coords);
                }
              });            
          }
        });
    },
    /**
     * Draw a section of depth line
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} depth Depth of section end
     * @param {integer} index Index of depth at end of section
     * @param {array} depths Array of depth measurements for the whole line
     * @param {array} coords Array of coordinates for the slope profile
     * @param {object} options Hash of options
     * @param {boolean} options.quantize Quantize the depth line section to the
     *                                   model mesh
     * @return {d3.svg.line} SVG Line representing this depth line section
     */
    draw_depth_section: function(depth, index, depths, coords, options) {
      var quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        quantize = options.quantize;
      }
      if (index > 0) {
        var slope_coords = coords[index - 1];
        var d_slope_coords = coords[index];
        var depth_x_y_prev = this.model.compute_depth_x_y(depths[index - 1], coords, {quantize: quantize});
        var depth_x_y = this.model.compute_depth_x_y(depth, coords, {quantize: quantize});
        var depth_coords = this.model.compute_line_coords(depth_x_y_prev, depth_x_y, index,
            coords, depths);
        return this.line(depth_coords);
      } else {
        return;
      }
    },
    /**
     * Return an svg line representing the desired slope section
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} coord Coordinate of section end point
     * @param {integer} index Index of end point coordinate
     * @return {d3.svg.line} SVG line representing this section of slope
     */
    draw_slope_section: function(coord, index) {
      if (index > 0) {
        return this.line([this.coords[index - 1], coord]);
      } else {
        return;
      }
    },
    /**
     * Draw cells if mesh data is available
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {d3.svg.g} SVG g element to which cells should be drawn
     */
    draw_cells: function(g) {
      var that = this;
      var geometry = this.model.get("geometry");
      if (typeof(geometry) === "undefined" ||
          typeof(geometry.mesh_data) === "undefined") {
        return;
      }
      var mesh_data = geometry.mesh_data;
      // Convert mesh data to array of {x: i, y: j, soil_type: k}
      var cells = [];
      var width_so_far = 0;
      _.each(mesh_data, function(col) {
        var number_of_cells = col.cells.soil_type.length;
        var height_so_far = 0;
        cells = cells.concat(_.map(_.range(number_of_cells),
          function(i) {
            var row_from_bottom = number_of_cells - 1 - i;
            var depth = col.cells.cell_depth.value[row_from_bottom];
            height_so_far += depth;
            var cell = {
              x: width_so_far,
              y: height_so_far,
              w: col.column_width.value,
              h: depth,
              soil_type: col.cells.soil_type[row_from_bottom]
            };
            return cell;
        }));
        width_so_far += col.column_width.value;
      });
      g.selectAll(".cells")
        .data(cells)
        .enter()
          .append("svg:rect")
          .attr("x", function(d) {
            return that.x(d.x);
          })
          .attr("y", function(d) {
            return -1 * that.y(d.y);
          })
          .attr("width", function(d) {
            return that.x(d.w) - that.x(0);
          })
          .attr("height", function(d) {
            return Math.abs(that.y(d.h) - that.y(0));
          })
          .attr("fill", function(d) {
            return that.soil_colors(SOIL_COLOR_OFFSET - d.soil_type);
          });
    },
    /**
     * Re-draw the slope view
     * @memberof Mossaic.vis.Slope
     * @instance
     */
    redraw: function() {
      if (!this.target) {
        if (this.g) {
          this.g.remove();
        }
        if (this.mesh) {
          this.mesh.remove();
        }
      
        if (this.draw_g) {
          this.draw_g.remove();
        }
        this.draw();
      }
    },

    /**
     * Draws the graphs chrome.  Axis, labels, grid etc
     * @param {type} x_max Width in meters
     * @param {type} y_max Height in meters
     * @returns {undefined}
     */
    draw_empty_graph: function(x_max, y_max) {
      var that = this;
      // Draw the mesh
      var mesh = this.model.get_mesh();
      var mesh_cols = _.map(_.range(0, Math.ceil(x_max * 1.05), mesh.x), function(x) {
        return { x1: x, x2: x, y1: 0, y2: y_max * 1.05 };
      });
      var mesh_rows = _.map(_.range(0, Math.ceil(y_max * 1.05), mesh.y), function(y) {
        return { x1: 0, x2: x_max * 1.05, y1: y, y2: y};
      });
      this.mesh.selectAll(".mesh")
        .data(mesh_cols.concat(mesh_rows))
        .enter().append("svg:line")
          .attr("class", "mesh")
          .attr("x1", function(d) { return that.x(d.x1); })
          .attr("x2", function(d) { return that.x(d.x2); })
          .attr("y1", function(d) { return -1 * that.y(d.y1); })
          .attr("y2", function(d) { return -1 * that.y(d.y2); });

      this.g = this.vis.append("svg:g")
        .attr("transform", "translate(0, " + this.h + ")")
        .attr("id", "slope_g");
      // Draw axes
      this.g.append("svg:line")
        .attr("x1", this.x(0))
        .attr("y1", -1 * this.y(0))
        .attr("x2", this.x(x_max * 1.05))
        .attr("y2", -1 * this.y(0));

      this.g.append("svg:line")
        .attr("x1", this.x(0))
        .attr("y1", -1 * this.y(0))
        .attr("x2", this.x(0))
        .attr("y2", -1 * this.y(y_max * 1.05));

      // Now add labels...
      var tmp_x_max = 1.05 * x_max;
      var xticks = _.filter(this.x.ticks(5), function(x) {
        return x < tmp_x_max;
      });
      this.g.selectAll(".x_label")
        .data(xticks)
        .enter().append("svg:text")
          .attr("class", "x_label noselect")
          .text(String)
          .attr("x", function(d) {
            return that.x(d);
          })
          .attr("y", -this.margin / 3)
          .attr("text-anchor", "middle");

      var tmp_y_max = 1.05 * y_max;
      var yticks = _.filter(this.y.ticks(5), function(y) {
        return y < tmp_y_max;
      });
      this.g.selectAll(".y_label")
        .data(yticks)
        .enter().append("svg:text")
          .attr("class", "y_label noselect")
          .text(String)
          .attr("x", this.margin / 2)
          .attr("y", function(d) {
            return -1 * that.y(d);
          })
          .attr("text-anchor", "middle")
          .attr("dy", 4);

      // And the ticks...
      this.g.selectAll(".x_tick")
        .data(xticks)
        .enter().append("svg:line")
          .attr("x1", function(d) {
            return that.x(d);
          })
          .attr("x2", function(d) {
            return that.x(d);
          })
          .attr("y1", -this.margin + 5)
          .attr("y2", -this.margin);

      this.g.selectAll("y.tick")
        .data(yticks)
        .enter().append("svg:line")
          .attr("x1", this.margin - 5)
          .attr("x2", this.margin)
          .attr("y1", function(d) {
            return -1 * that.y(d);
          })
          .attr("y2", function(d) {
            return -1 * that.y(d);
          });
    },
    
    draw_soil_strata: function() {
      var that = this;
      // Draw soil strata
      if (this.model.is_soil_known()) {
        this.soil_depths = this.model.get_soil_depths();
        for (var strata = 0; strata < this.soil_depths.length; ++strata) {
          if (typeof(this.soil_depths[strata]) !== "undefined" ) {
            this.draw_depth_line({
              coords: this.coords,
              depths: this.soil_depths[strata],
              stroke: this.soil_colors(SOIL_COLOR_OFFSET - strata),
              stroke_width: "3px",
              css_class: "soil_strata_" + strata,
              quantize: this.quantize,
              editable_test: function() {
                return this.model.editable.soil_strata;
              }
            });
          }
        }
      } else {
        this.soil_depths = this.model.get_soil_estimates();
        for (var strata = 0; strata < this.soil_depths.length; ++strata) {
          if (typeof(this.soil_depths[strata]) !== "undefined") {
            this.draw_depth_estimate({
              coords: that.coords,
              depths: this.soil_depths[strata],
              stroke: this.soil_colors(SOIL_COLOR_OFFSET - strata),
              stroke_width: "3px",
              css_class: "soil_strata_" + strata,
              quantize: this.quantize
            });
          }
        }
      }
      
    },

    draw_slope_segments: function() {
      var that = this;
       // Draw slope segments
      this.g.selectAll("path").select(".slope_sections")
        .data(this.coords)
        .enter().append("svg:path")
          .attr("d", this.draw_slope_section)
          .attr("class", "slope_sections");
      var road_prism = this.model.get("geometry").road;
      this.g.selectAll(".slope_sections")
        .style("stroke", function(d, i) {
          if (road_prism.slope_section == that.model.coords_index_to_section_index(i) - 1) {
            return "sienna";
          } else {
            return "steelblue";
          }
        })
        .on("mouseover", function(d, i) {
          if (that.model.editable.datum && that.is_selectable(i) &&
              !that.target) {
            if (that.model.mode === "questa") {
              d3.select(d3.event.target)
                .transition()
                  .duration(100)
                  .style("stroke-width", 10);
            }
          }
        })
        .on("mouseout", function(d, i) {
          if (that.model.editable.datum) {
            if (that.model.mode === "questa") {
              d3.select(d3.event.target)
                .transition()
                  .duration(100)
                  .style("stroke-width", 6);
            }
          }
        })
        .on("click", function(d, i) {
          if (that.model.editable.datum && that.is_selectable(i)) {
            if (that.model.mode === "questa") {
              var geometry = _.clone(that.model.get("geometry"));
              if (typeof(geometry.road) === "undefined") {
                geometry.road = {};
              } else {
                geometry.road = _.clone(geometry.road);
              }
              geometry.road.slope_section =
                  that.model.coords_index_to_section_index(i) - 1;
              that.model.changed_by = that;
              that.model.set({geometry: geometry});
            }
          }
        });

    },
      
    draw_datum: function(x_max, y_min, y_max) {
      var that = this;
      var datum = this.model.get("geometry").datum;
      var datum_as_depth = {distance: datum.value, depth: 0};
      var datum_coords = this.model.compute_depth_x_y(datum_as_depth, this.coords, {
          quantize: this.quantize,
          origin: 0
      });
      if (typeof(this.model.get("geometry").datum) !== "undefined" &&
          typeof(this.model.get("geometry").datum.value) === "undefined" ||
          typeof(this.model.get("geometry").datum.index) === "undefined") {
        this.g.selectAll(".datum_bar_v")
          .data([datum_coords])
          .enter().append("svg:line")
            .attr("class", "datum_bar_v")
            .attr("x1", function(d) { return that.x(d.x); })
            .attr("x2", function(d) { return that.x(d.x); })
            .attr("y1", function(d) { return -1 * that.y(y_min) })
            .attr("y2", function(d) { return -1 * that.y(y_max * 1.05) });
        this.g.selectAll(".datum_bar_h")
          .data([datum_coords])
          .enter().append("svg:line")
            .attr("class", "datum_bar_h")
            .attr("x1", function(d) { return that.x(0); })
            .attr("x2", function(d) { return that.x(x_max * 1.05); })
            .attr("y1", function(d) { return -1 * that.y(d.y) })
            .attr("y2", function(d) { return -1 * that.y(d.y) });
      }
    
      this.vis.on("mousemove", function(d, i) {
        if (that.model.editable.datum) {
          var new_point_svg = that.screen_to_svg_transform("g", event);
          var new_x = that.x.invert(new_point_svg.x);
          var new_datum_distance = that.model.compute_surface_distance_from_x(new_x, that.coords);
          var new_datum_as_depth = {distance: new_datum_distance, depth: 0};
          var new_datum_coords = that.model.compute_depth_x_y(new_datum_as_depth, that.coords, {
            quantize: that.quantize,
            origin: 0
          });
          that.g.selectAll(".datum_bar_v")
            .attr("x1", function(d) { return that.x(new_datum_coords.x); })
            .attr("x2", function(d) { return that.x(new_datum_coords.x); })
            .attr("y1", function(d) { return -1 * that.y(y_min) })
            .attr("y2", function(d) { return -1 * that.y(y_max * 1.05) });
          that.g.selectAll(".datum_bar_h")
            .attr("x1", function(d) { return that.x(0); })
            .attr("x2", function(d) { return that.x(x_max * 1.05); })
            .attr("y1", function(d) { return -1 * that.y(new_datum_coords.y) })
            .attr("y2", function(d) { return -1 * that.y(new_datum_coords.y) });
        }
      });
    }, 
       
    draw_point_circles: function() {
      var that = this;
       // Draw point circles
      var datum_index;
      var geometry = this.model.get("geometry");
      if (typeof(geometry) !== "undefined" &&
          typeof(geometry.datum) !== "undefined") {
        datum_index = geometry.datum.index;
      }
      this.g.selectAll("circle").select(".slope_points")
        .data(this.coords)
        .enter().append("svg:circle")
          .attr("class", "node")
          .attr("cx", function(d) { return that.x(d.x); })
          .attr("cy", function(d) { return -1 * that.y(d.y); })
          .attr("r", 5)
          .attr("class", "slope_points")
          .style("fill", function(d, i) {
            if (i == datum_index) {
              return "red";
            } else {
              return "orange";
            }
          });
      this.g.selectAll(".slope_points")
        .on("mouseover", function(d, i) {
          var geometry = that.model.get("geometry");
          var datum = geometry.datum || {};
          if (((that.model.editable.surface && that.is_moveable(i)) ||
              that.model.editable.datum && typeof(datum.index) === "undefined") &&
              !that.target) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 8);
          }
        })
        .on("mouseout", function(d, i) {
          if (!that.target) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 5);
          }
        })
        .on("mousedown", function(d, i) {
          if (that.model.editable.surface && that.is_moveable(i)) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 8);
            that.target = d3.event.target;
            that.vis
              .on("mousemove", function(d, j) {
                if (that.target) {
                  that.update_point_on_screen(that.target, d3.event, {
                      quantize: false // Always false - it's prettier that way
                  });
                  that.update_coords(that.target, that.coords, i); // i, not j
                  that.update_sections(that.coords);
                  that.set_clip_path();
                }
              });
          }
        })
        .on("mouseup", function(d, i) {
          var geometry = _.clone(that.model.get("geometry"));
          var datum = geometry.datum;
          if (that.model.editable.datum && (typeof(datum.index) === "undefined")) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 5);
            if (typeof(geometry.datum) === "undefined") {
              geometry.datum = {};
            } else {
              geometry.datum = _.clone(geometry.datum);
            }
            geometry.datum.index = i;
            that.model.changed_by = that;
            that.model.set({geometry: geometry});
            that.model.editable.datum = false;
          }
        }); 
    },
    
    draw_water_table: function() {
      var that = this;
      this.draw_depth_line({
        coords: that.coords,
        depths: this.water_depths,
        css_class: "water_table",
        stroke: "lightskyblue",
        stroke_width: "3px",
        quantize: this.quantize,
        editable_test: function() { return that.model.editable.water_table }
      });
    },
      
    set_drawable_surface: function (x_max, y_max) {
      var that = this;
      var mouseon = true;
      var x,y;
      
      var data = [[that.x(0),-1*that.y(y_max)]];
      
      that.draw_g = this.vis.append("svg:g", "*")
        .attr("transform", "translate(0, " + this.h + ")")
        .attr("id", "draw_g");      
   
      var rect = that.draw_g.append("rect")
        .attr("x", that.x(0))
        .attr("y", -1 * (that.y(y_max)+ that.y(0)))
        .attr("width", that.x(x_max))
        .attr("height",that.y(y_max))
        .style("fill", "red")   // This feels like a bodge.  If we do fill:none, then it doesn't catch mouse clicks
        .style("opacity","0")     // so fill:white and opacity:0 instead.
        .on("mousemove", function(d,i) {
          x = d3.mouse(this)[0];
          y = d3.mouse(this)[1];
          if (x < data[data.length - 1][0]) x = data[data.length - 1][0];
          if (y < data[data.length - 1][1]) y = data[data.length - 1][1];
          if (-1 * y < that.y(0) + 10) y = -1 * that.y(0);
          that.draw_g.select("#line")
            .attr("x1", data[data.length - 1][0])
            .attr("y1", data[data.length - 1][1])
            .attr("x2", x)
            .attr("y2", y);
        });

      that.draw_g.append("line").attr("id","line");

      var line = d3.svg.line()
        .x(function(d) { return d[0]; })
        .y(function(d) { return d[1]; });

      that.draw_g.append("path").attr("class","drawable");
      
      that.draw_g.on("click",function(){
        // If drawing has ended...
        if (!that.model.get("drawable")) return;

        data.push([x,y]);
        
        // Update the SVG elements
        that.draw_g.select("path").attr("d",line(data));    
        that.draw_g.selectAll("circle.drawable")
          .data(data)
          .enter().append("circle")
          .attr("class","drawable")
          .attr("cx", function(d) { return d[0];})
          .attr("cy", function(d) {return d[1];})
          .attr("r",5);
        
        // We've hit the x-axis.  End drawing, and update the model with the drawn lines
        if (y === -1 * that.y(0)) {
          that.draw_g.select("line").remove();

          var model_data = [];
          data.forEach(function(point) {
            model_data.push([
              that.x.invert(point[0]),
              that.y.invert(-1 * point[1])  // Invert the y, because onscreen it's negative
            ]);
          });
        
          that.model.changed_by = that;

          // For historical reasons, we need to add slope sections to the model individually, using height and slope
          // length rather than the coords 
          for (var i = 0; i < model_data.length -1; i++) {
            var length = Math.sqrt(
              Math.pow(model_data[i+1][0] - model_data[i][0] , 2) + 
              Math.pow(model_data[i][1] - model_data[i+1][1] , 2) 
            );
            var height = (model_data[i][1] - model_data[i+1][1]);
            that.model.add_section({"length":length, "height":height});
          }
          that.model.set("drawable",  false);

          // Redraw the slope using the model sections, and without drawing mode enabled
          that.show();
          
          if (typeof(that.on_drawable_ended_callback) === "function") {
            that.on_drawable_ended_callback();
          }
        }
      });

  },
    
    
    /**
     * Draw the slope display and, if specified, call this.on_draw_callback when
     * complete.
     * 
     * SVG origin is the top left, and vertical (i.e. y) coords move *down* as they increase.  This is inconvient 
     * for drawing slopes, where the bottom left would be the natural origin.  So we insert the SVG g elements
     * with a translation of the height of the graph, which puts them *out* of the graph.  We then use *negative* 
     * y coordinates to move up back *up* into the graph.  This makes (0,0) = bottom left, and (0,-height) the top left.
     * 
     * Before converting from screen coords, to model coords, need to multiply y by -1;
     * 
     * @memberof Mossaic.vis.Slope
     * @instance
     */
    draw: function() {
      var that = this;
      this.w = $(this.parent_divname).width();
      this.h = $(window).height() * 0.4;

      var get_y_limit = function(min_or_max) {
        if (typeof(that.coords) === "undefined") {
          return 0;
        }
        var candidates = [d3[min_or_max](that.coords, function(d) {
          return d.y;
        })];
        candidates.push(d3[min_or_max](that.coords, function(d, i) {
          if (typeof(that.water_depths) !== "undefined") {
            return d.y - that.water_depths[i];
          } else {
            return d.y;
          }
        }));
        candidates.push(d3[min_or_max](that.coords, function(d, i) {
          var candidates = _.map(that.soil_depths, function(strata) {
            if (typeof(strata) !== "undefined") {
              return d.y - strata[i];
            } else {
              return d.y;
            }
          });
          return d3[min_or_max](candidates);
        }));
        return d3[min_or_max](candidates);
      }
      var old_coords = _.clone(this.coords);
      try {
        this.coords = this.model.compute_coords({
            old_coords: old_coords,
            quantize: this.quantize // Set this to true for "quantize on ui"
        });
      } catch(e) {
        console.log("Cannot draw geometry:" + e);
        return;
      }
      this.water_depths = this.model.get_water_depths();
      var x_max, y_min, y_max;

      // If we don't have any coords from the model, then use the ones supplied when instantiated
      if (that.model.get("drawable") === true) {
        x_max = this.model.get("initial_width");
        y_min = 0;
        y_max = this.model.get("initial_height");
        this.coords = [{x:0,y:y_max}];
      } else {
        x_max = d3.max(this.coords, function (d) { return d.x; });
        y_min = get_y_limit("min");
        y_max = get_y_limit("max");
      }
      
      // TODO(1) - Is this the best way of exiting when we don't have a grid?
      if (x_max === y_max === 0) return;
      
      var range_max, domain_max;
      var range_aspect_ratio = this.h / this.w;
      var domain_aspect_ratio = y_max / x_max;
      if (range_aspect_ratio < domain_aspect_ratio) {
        range_max = this.h - this.margin;
        domain_max = y_max;
      } else {
        range_max = this.w - this.margin;
        domain_max = x_max;
      }

      this.y = d3.scale.linear()
        .domain([y_min, domain_max])
        .range([0 + this.margin, range_max]);
      this.x = d3.scale.linear()
        .domain([0, domain_max])
        .range([0 + this.margin, range_max]);

      if (!this.vis) {
        this.vis = this.div.append("svg:svg");
      }
      var svg_width = this.x(x_max) * 1.1;
      this.vis
        .style("width", svg_width)
        .style("height", this.h)
        .style("margin-left", (this.w - svg_width) / 2)
        .style("margin-right", (this.w - svg_width) / 2);

      this.mesh = this.vis.insert("svg:g", "*")
        .attr("transform", "translate(0, " + this.h + ")")
        .attr("id", "mesh_g");

      this.draw_cells(this.mesh);

      // Draw the graphs chrome, axis etc
      this.draw_empty_graph(x_max, y_max);

      this.line = d3.svg.line()
          .x(function(d,i) { return that.x(d.x); })
          .y(function(d) { return -1 * that.y(d.y); });

      if (that.model.get("drawable") === true) {
        this.set_drawable_surface(x_max,y_max);
      } else {
        this.set_clip_path();
        this.draw_water_table();
        this.draw_soil_strata();
        this.draw_slope_segments();
        this.draw_point_circles();
        if (this.model.mode === "chasm") {
          this.draw_datum(x_max,y_min,y_max);
        }
      }
    
    
      // Handle all mouseup events
      this.vis.on("mouseup", function(d, i) {
        var geometry = _.clone(that.model.get("geometry"));
        var datum = _.clone(geometry.datum) || {};
        // Place the datum point
        if (that.model.editable.datum && (typeof(datum.index) === "undefined")) {
          var new_point_svg = that.screen_to_svg_transform("g", event);
          var new_x = that.x.invert(new_point_svg.x);
          var new_datum_distance = that.model.compute_surface_distance_from_x(new_x, that.coords);
          var new_section_index;
          try {
            new_section_index = that.model.split_section(new_datum_distance);
          } catch(e) {
            console.log(e);
            return;
          }
          var geometry = _.clone(that.model.get("geometry"));
          var datum = _.clone(geometry.datum) || {};
          datum.index = new_section_index;
          geometry.datum = datum;
          that.model.changed_by = this;
          that.model.set({geometry: geometry});
          that.model.editable.datum = false;
        }
        var redraw = that.target && true || false;
        d3.select(that.target)
          .on("mousemove", null)
          .transition()
            .duration(100)
              .attr("r", 5);
        that.vis.on("mousemove", null);
        that.target = undefined;
        if (redraw) {
          that.redraw();
        }
      });

      if (typeof(this.on_draw_callback) === "function") {
        this.on_draw_callback();
      }
    }
  });
})(); 