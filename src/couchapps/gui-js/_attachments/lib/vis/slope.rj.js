define(["views/_basic.rj"], function (BasicView) {
  /**
   * Slope graphic used by the slope workflow.  At it's simplest, it draws a slope with the supplied geometry to the
   * screen.  It can also have "modes" enabled using set_mode(), which activates event handlers, enabling the user to
   * graphically draw slope & strata elements.  It then updates the model with new values.
   *
   * This is a combination of legacy code from Mike, and reworked code by Mark.
   *
   */
  /**
   * Default offset for determining soil colors
   * @constant
   * @type integer
   */
  var SOIL_COLOR_OFFSET = 11;
  /**
   * Backbone View which provides a graphical display of a
   * Mossaic.models.Geometry model and allows point and click editing.
   *
   * This View uses three different coordinate systems.
   *  - model space -> The coordinate system used in the model domain (the same
   *                   domain used by chasm and questa)
   *  - svg space -> The coordinates system used for drawing the model into an
   *                 SVG element
   *  - screen space - > The coordinates system used by the browser to receive
   *                     user click events, etc
   * @constructor
   * @extends Backbone.View
   */
  return BasicView.extend({
    /**
     * Initialise the view. Called implicitly by "new Mossaic.vis.Slope"
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @param {Mossaic.models.Geometry} model Geometry model to be displayed
     * @params {object} options Hash of options
     * @params {bool} options.
     * @params {integer} options.max_height Maximum height (in px) of the slope graphic
     * @params {integer} options.max_width Maximum width (in px) of the slope graphic
     */
    initialize: function(options) {
      var that = this;
      _.bindAll(this);
      /**
       * The model to be represented by this visualisation.
       * @type Mossaic.models.Geometry
       */
      this.model = options.model;   //(Backbone does this anyway, but this makes it more explicit)

      this.listenTo(this.model, 'change', function() {
        if (that.model.changed_by !== that) that.redraw();
      });

      // Convienience/efficiancy to save doing the d3.select all the time
      this.div = d3.select(this.el);

      /**
       * Desired margin for the visualisation
       * @type integer
       */
      this.margin = 30;

      // The SVG will be resized to fit as large as possible withing the maximums specified
      // Set sane defaults (in pixels) if we don't get initialized with them.
      this.max_width = options.max_width || 600;
      this.max_height = options.max_height || 400;

      // Set sane defaults (in m) for the graphic.  These will generally be overridden either by the parent calling
      // set_initial_dimensions(), or calculated from the model.coords, but it helps to have sensible defaults here.
      this.initial_width = this.max_width / 10;
      this.initial_height = this.max_height / 10;

      this.boreholes = {};    // Object to hold borehole data - for soils & water table

      this.enabled_modes = [];  // Array to hold modes which are currently enabled.  See set_mode() & end_mode()

      /**
       * Whether Mossaic.models.Geometry coordinates should be quantized to
       * the mesh or not
       */
      this.quantize = false;
      if (options && typeof(options.quantize) !== "undefined") {
        this.quantize = options.quantize;
      }
      /**
       * Reference to DOM element that triggered the current event. Used to
       * support dragging events, etc.
       * @type d3.event.target
       */
      this.target = undefined;
      var soil_domain = (function () {
        var d = [];
        for (var i = 0; i < 20; ++i) {
          d.push(i);
        }
        return d;
      })();
      /**
       * Soil color scale
       * @type d3.scale
       */
      this.soil_colors = d3.scale.category20b().domain(soil_domain);

      if (typeof(options.mousemove_callback) === "function") {
        this.on_mousemove_callback = options.mousemove_callback;
      }

      /**
       * Singleton tooltip div
       */
      this.tooltip = d3
        .select("body")
        .append("div")
        .attr("class","tooltip")
        .style("opacity",0);

      /**
       * Function to be called when drawing is complete
       * @type function
       */
      this.on_draw_callback;
      this.draw();
    },

    /**
     * Show the visualisation
     * @memberof Mossaic.vis.Slope
     * @instance
     */
    show: function() {
      this.$el.show();
    },

    /**
     * Hide the visualisation
     * @memberof Mossaic.vis.Slope
     * @instance
     */
    hide: function() {
      this.$el.hide();
    },

  /**
   * Set the initial dimensions (in meters) for the slope graphic.  Used for initial size when freehand drawing the
   * slope.
   * @param {object} dimensions Object
   * @param {integer} dimensions.height Initial height (m)
   * @param {integer} dimensions.width Initial width (m)
   */
    set_initial_dimensions: function(dimensions) {
      this.initial_height = dimensions.height;
      this.initial_width = dimensions.width;

    },

    /**
     * Set the model to be represented by the form.
     * @memberof Mossaic.vis.Slope
     * @instance
     * @param {Mossaic.models.Geometry} model Model to be represented
     *                                        by this visualisation
     */
    set_model: function(model) {
      this.model = model;
      this.coords = [];
      this.model.bind("change", this.redraw);
    },
    /**
     * Set a callback function to be triggered when the display is drawn.
     * This allows another widget (the stability grid) to redraw itself
     * taking into account anything that might have changed.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @param {function} func Function to be triggered after drawing
     */
    on_draw: function(func) {
      this.on_draw_callback = func;
    },

    /**
     * Set a callback function to be triggered when the user has set the datum point.
     * @param {function} func Function to be triggered when the user sets the datum.
     * @returns {undefined}
     */
    on_editable_datum_ended: function(func) {
      this.on_editable_datum_ended_callback = func;
    },

    /**
     * Sets a mode on the display.  It will call a method named "set_<mode>".  This method should then register any
     * mouse etc event handlers and draw extra display elements it needs.  If the method doesn't exist this will just
     * silently return.  Multiple modes can be set at once - but they might conflict - so it's down to the caller
     * to ensure a sensible combination is set.
     *
     * @param {string} mode     Mode name, must be a method in this object named "set_<mode>"
     * @param {object} options  Optional object hash that is passed to the set_<mode> method.  Often used to register
     *                          callbacks for the method to call when it's finished, but see indvidual set_<mode>
     *                          methods for details.
     */
    set_mode: function(mode, options) {
      var options = options || {};
      if (typeof(this["set_" + mode]) === "function") {
        if (this.enabled_modes.indexOf(mode) === -1 ) {
          this.enabled_modes.push(mode);
        }
        this["set_" + mode](options);
      }
    },

    /**
     * Ends a mode on the display.  It will call a method named "end_<mode>".  This method should then unregister any
     * mouse etc event handlers and remove display elements the mode needed.  The "end_<mode>" method should be callable
     * without problems even if the mode wasn't active.  It also shouldn't do anything other than end the graphical
     * display of the mode.  e.g. saving data that might have been drawn/entered in the mode should be done seperately.
     *
     * If the method doesn't exist this will just silently return
     * @param {type} mode     Mode name, must be a method in this object named "end_<mode>"
     * @param {type} options  Optional object hash that is passed to the end_<mode> method.
     */
    end_mode: function(mode, options) {
      var options = options || {};
      if (typeof(this["end_" + mode]) === "function") {
        this.enabled_modes.splice(this.enabled_modes.indexOf(mode),1)   // Remove mode from array
        this["end_" + mode](options);
      }
    },

    /**
     * Ends all set modes, so will leave the slope in a basic non-interactive display.
     */
    end_all_modes: function() {
      var that = this;
      this.enabled_modes.forEach(function(mode) {
        that.end_mode(mode);
      });
    },


    /**
     * Determine whether a coordinate at index is moveable.
     *
     * The coordinates at the beginning and end of the user-defined sections
     * cannot be moved, neither can any autogenerated coordinates.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {int} index Index of coordinate under test
     * @return {boolean} true if coordinate is moveable, false otherwise
     */
    is_moveable: function(index) {
      var total_coords = this.coords.length;
      return (index > this.model.get_number_of_extra_sections().number_of_top_sections &&
          index < total_coords - this.model.get_number_of_extra_sections().number_of_bottom_sections - 1);
    },
    /**
     * Determine whether a coordinate at index is selectable.
     *
     * Selecting only happens when users choose a section to be the datum.
     * This must be a user-defined section. Coordinates describing other
     * sections are not selectable.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {int} index Index of coordinate under test
     * @return {boolean} true if coordinate is selectable, false otherwise
     */
    is_selectable: function(index) {
      var total_coords = this.coords.length;
      return (index > this.model.get_number_of_extra_sections().number_of_top_sections &&
          index < total_coords - this.model.get_number_of_extra_sections().number_of_bottom_sections);
    },
    /**
     * Create a clip path using the slope coordinates and the bottom of the
     * slope. The clip path will have id #slope_clip and may be used by other
     * Views that also draw to this View.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     */
    set_clip_path: function() {
      var coords = this.coords;
      var line = this.line;
      var draw_clip_path = function(coords) {
        var clip_coords = [{x: 0, y: 0}];
        clip_coords.push.apply(clip_coords, coords);
        clip_coords.push({x: 0, y: 0});
        var l = line(clip_coords);
        return l;
      }
      var clip_path = this.slope_g.select("#slope_clip");
      if (!clip_path[0][0]) {
        clip_path = this.slope_g.append("svg:clipPath").attr("id", "slope_clip");
      }
      clip_path.selectAll("path").remove();
      clip_path.selectAll("path")
        .data([coords]).enter().append("svg:path")
        .attr("d", draw_clip_path);
    },
    /**
     * Update the profile sections in the Mossaic.models.Geometry model from
     * the current set of coordinates.
     *
     * Note that the coordinates are the slope coordinates in the model
     * coordinate system. They were originally calculated from the
     * model slope profile sections but may have been changed through user
     * interaction with the view.
     *
     * ( All this geometry should really be in the model -  and the model provide an API to update it's sections -
     *   rather than use doing it directly here)
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {array} coords Array of coordinates where each coordinate is an
     *                       object with x and y fields.
     */
    update_sections: function(coords) {
      var coords = (this.quantize && this.model.quantize(coords)) || coords;
      var geometry = _.clone(this.model.get("geometry"));
      var slope_sections = _.clone(geometry.slope_sections);
      for (var i = 0; i < slope_sections.length; ++i) {
        var i_c = this.model.section_index_to_coords_index(i);
        var section = _.clone(slope_sections[i]);
        delete(section.surface_length); // Delete these two because they need to be recalculated from the width & height
        delete(section.angle);

        section.surface_height.value = coords[i_c].y - coords[i_c + 1].y; // Get the new height
        section.surface_width.value = coords[i_c + 1].x - coords[i_c].x; // Get the new width
        section = Mossaic.utils.triangle_with_pointless_units(section); // Populate the rest of the triangle
        slope_sections[i] = section;
      }
      geometry.slope_sections = slope_sections;
      this.model.changed_by = this;
      this.model.set({geometry: geometry});
      for (var i = 0; i < this.water_depths.length; ++i) {
        this.model.set_water_depth(this.water_depths[i].depth, {
          index: i,
          surface_distance_from_cut_toe: this.water_depths[i].surface_distance_from_cut_toe,
          changed_by: this
        });
      }
      var soil_depths_by_section = this.model.get_soil_depths_by_section({
        soil_depths: this.soil_depths
      });
      for (var i = 0; i < soil_depths_by_section.length; ++i) {
        var depths = _.map(soil_depths_by_section[i], function(depth) {
          return depth.depth;
        });
        this.model.set_soil_depth(depths, undefined, {
          index: i,
          surface_distance_from_cut_toe: soil_depths_by_section[i].surface_distance_from_cut_toe,
          changed_by: this
        });
      }
      return this;
    },
    /**
     * Get the coordinates of an event that occured in the screen coordinate
     * system in the svg coordinate system.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {d3.event.target} target The event target
     * @param {d3.event} event D3 event object containing screen coordinates
     * @return Coordinates of event in svg space
     */
    screen_to_svg_transform: function(target, event) {
      // Get current transform matrix (why the [0][0] ?)
      var ctm = d3.select(target)[0][0].getScreenCTM();
      var point = this.vis[0][0].createSVGPoint();
      point.x = event.clientX;
      point.y = event.clientY;
      return transformed_point = point.matrixTransform(ctm.inverse());
    },
    /**
     * Quantize a point in svg coordinates to the mesh defined in the
     * Mossaic.models.Geometry model
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} point Coordinates in svg space
     * @return {object} Coordinates quantized to the model mesh, in svg space
     */
    quantize_svg: function(point) {
      var mesh = this.model.get_mesh();
      var svg_grid_x = this.x(mesh.x) - this.x(0);
      var svg_grid_y = this.y(mesh.y) - this.y(0);
      point.x = Math.round((point.x - this.x(0)) / svg_grid_x) * svg_grid_x +
                                                                      this.x(0);
      point.y = Math.round((point.y + this.y(0)) / svg_grid_y) * svg_grid_y -
                                                                      this.y(0);
      return point;
    },
    /**
     * Update a point on the screen in response to a user-generated event
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {d3.event.target} target Event target
     * @param {d3.event} event D3 event object containing new screen coordinates
     * @param {object} options Hash of options
     * @param {object} options.quantize {boolean} quantize Quantize point to the
     *                                                     mesh defined in the
     *                                                     model
     */
    update_point_on_screen: function(target, event, options) {
      var options = options || {};
      var lock = options.lock || {};
      var quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        quantize = options.quantize;
      }
      var transformed_point = this.screen_to_svg_transform(target, event);
      if (quantize) {
        var point = this.quantize_svg(transformed_point);
      } else {
        var point = transformed_point;
      }
      if (!lock.x) {
        d3.select(target).attr("cx", point.x);
      }
      if (!lock.y) {
        d3.select(target).attr("cy", point.y);
      }
    },
    /**
     * Update depth points on the screen in response to a user generated event
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {d3.event.target} target Event target
     * @param {array} coords Slope surface coordinates
     * @param {array} depths Depth measurements for each soil strata interface
     * @param {integer} index Index of soil strata interface to be updated
     * @param {String} css_class CSS class used to select the depth measurements
     */
    update_depths_on_screen: function(target, coords, depths, index, css_class){
      var that = this;
      var new_y = this.y.invert(-1 * d3.select(target).attr("cy"));
      // depth = difference between new_y and the polyline
      // Extract the comptue_depth_x_y function to the bit that calculates the line x and y
      // Then we just call that
      // TODO Code debt: By using a dummy depth equal to zero we can get the y coord of the relevant
      // polyline section. This is naughty, but will do for now.
      var dummy_depth = {depth: 0, distance: depths[index].distance};
      var polyline_y = this.model.compute_depth_x_y(dummy_depth, coords, {
          quantize: this.quantize
      }).y;
      depths[index] = {depth: polyline_y - new_y, distance: depths[index].distance};
      this.slope_g.selectAll("." + css_class)
        .data(depths)
          .attr("d", function(d, i) {
            return that.draw_depth_section(d, i, depths, coords, {
                quantize: that.quantize});
          });
    },
    /**
     * Update View coordinates (in model space) in response to a user generated
     * event. We get the svg coordinate of the target element, convert it to
     * model space, and update the view coordinates with the new values.
     *
     * Then the slope is redrawn, depth coordinates are recalculated, and depth
     * sections are redrawn.
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {d3.event.target} target Event target
     * @param {array} coords Array of slope profile coordinates
     * @param {integer} index Index of slope coordinate to be updated
     */
    update_coords: function(target, coords, index) {
      var that = this;
      var quantize_depths = this.quantize;
      var new_x, new_y;
      if (target !== null) {
        var new_x = this.x.invert(d3.select(target).attr("cx"));
        var new_y = this.y.invert(-1 * d3.select(target).attr("cy"));
        coords[index] = {x: new_x, y: new_y};
      }
      var coords_for_depth_draw = (this.quantize && this.model.quantize(coords)) ||
          coords;
      this.slope_g.selectAll(".slope_sections")
        .data(coords)
          .attr("d", this.draw_slope_section);
      this.slope_g.selectAll(".water_table")
        .data(this.water_depths)
          .attr("d", function(d, i) {
            return that.draw_depth_section(d, i, that.water_depths, coords_for_depth_draw, {quantize: quantize_depths});
          });
      this.slope_g.selectAll(".water_table_points")
        .data(this.water_depths)
          .attr("cx", function(d) { return that.x(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).x); })
          .attr("cy", function(d) { return -1 * that.y(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).y); })
      var total_strata = this.soil_depths.length;
      for (var strata = 0; strata < total_strata; ++strata) {
        (function() { // Wrap in a function so depths has scope
          var depths = that.soil_depths[strata];
          if (that.model.is_soil_known()) {
            that.slope_g.selectAll("." + "soil_strata_" + strata)
              .data(depths)
                .attr("d", function(d, i) {
                  return that.draw_depth_section(d, i, depths, coords_for_depth_draw, {quantize: quantize_depths});
                });
            that.slope_g.selectAll("." + "soil_strata_" + strata + "_points")
              .data(depths)
                .attr("cx", function(d) { return that.x(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).x); })
                .attr("cy", function(d) { return -1 * that.y(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).y); });
          } else {
            that.slope_g.selectAll("." + "soil_strata_" + strata + "_point")
              .data([depths])
                .attr("cx", function(d) { return that.x(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).x); })
                .attr("cy", function(d) { return -1 * that.y(that.model.compute_depth_x_y(d, coords_for_depth_draw, {quantize: quantize_depths}).y); });
            that.slope_g.selectAll("." + "soil_strata_" + strata)
              .data([depths])
                .attr("d", function(d, i) {
                  return that.draw_estimate_line(d, i, coords_for_depth_draw, quantize_depths);
                });
          }
        })();
      }
    },
    /**
     * Draw estimate line
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} d Depth estimate
     * @param {integer} i Index of depth
     * @param {array} coords Array of slope coordinates
     * @param {boolean} quantize Quantize the coordinates of the estimate line
     */
    draw_estimate_line: function(d, i, coords, quantize) {
      if (typeof(d.angle) === "undefined" ||
          typeof(d.depth) === "undefined" ||
          typeof(d.distance) === "undefined") {
        return;
      }
      // Get estimate origin x y in model space
      var estimate_origin = this.model.compute_depth_x_y(d, coords, {
        quantize: quantize
      });
      var estimate = this.model.compute_estimate_coords(d, estimate_origin,
          coords);
      return this.line([estimate.start, estimate.end]);
    },
    /**
     * Draw estimated depth lines
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} options Hash of options
     * @param {boolean} options.quantize Quantize the line coordinates
     * @param {array} options.coords Array of slope surface coordinates
     * @param {array} options.depths Array of depth estimates
     * @param {string} options.css_class CSS class to be applied to the line
     * @param {string} options.stroke Line color
     * @param {string} options.stroke_widget Line width
     */
    draw_depth_estimate: function(options) {
      var that = this;
      var quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        quantize = options.quantize;
      }
      var coords = options.coords;
      var depths = options.depths;
      var css_class = options.css_class;
      var stroke = options.stroke || "steelblue";
      var stroke_width = options.stroke_width || "3px";

      this.slope_g.selectAll("." + css_class)
        .data([depths])
        .enter().append("svg:path")
          .attr("d", function(d, i) {
            return that.draw_estimate_line(d, i, coords, quantize);
          })
          .attr("class", css_class)
          .attr("clip-path", "url(#slope_clip)")
          .style("stroke", stroke)
          .style("stroke-width", stroke_width);

      this.slope_g.selectAll("." + css_class + "_point")
        .data([depths])
        .enter().append("svg:circle")
          .attr("class", css_class + "_point")
          .attr("cx", function(d) {
            return that.x(that.model.compute_depth_x_y(
              d, coords, {quantize: quantize}).x);
          })
          .attr("cy", function(d) {
            return -1 * that.y(that.model.compute_depth_x_y(
              d, coords, {quantize: quantize}).y);
          })
          .attr("r", 2)
          .style("fill", "#A64D4D");
    },
    /**
     * Draw depth line with points
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} options Hash of options
     * @param {array} options.coords Array of slope surface coordinates
     * @param {array} options.depths Array of depth measurements
     * @param {string} options.css_class CSS class to be used for depth line elements
     * @param {string} options.stroke Stroke color for depth line
     * @param {string} options.stroke_width Width of depth line
     * @param {boolean} options.quantize Quantize depth line to model mesh
     */
    draw_depth_line: function(options) {
      var that = this;
      var quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        quantize = options.quantize;
      }
      var coords = options.coords;
      var depths = options.depths;
      var css_class = options.css_class;
      var stroke = options.stroke || "steelblue";
      var circle_colour = options.circle_colour || "black";
      var stroke_width = options.stroke_width || "3px";
      var editable_test = options.editable_test || function() { return true; };

      this.slope_g.selectAll("." + css_class)
        .data(depths)
        .enter().append("svg:path")
          .attr("d", function (d, i) {
            return that.draw_depth_section(d, i, depths, coords, {
                quantize: quantize});
          })
          .attr("class", css_class)
          .attr("clip-path", "url(#slope_clip)")
          .style("stroke", stroke)
          .style("stroke-width", stroke_width);

      this.slope_g.selectAll("circle").select("." + css_class + "_points")
        .data(depths)
        .enter().append("svg:circle")
          .attr("class", "node")
          .attr("cx", function(d) { return that.x(that.model.compute_depth_x_y(d, coords, {quantize: quantize}).x); })
          .attr("cy", function(d) {
            if (!isNaN(d.depth) && typeof(d.depth) !== "undefined") {
              return -1 * that.y(that.model.compute_depth_x_y(d, coords, {quantize: quantize}).y);
            } else {
              return -1 * that.y(0);
            }
          })
          .attr("r", 5)
          .attr("class", css_class + "_points")
          .style("fill", stroke);

//      this.slope_g.selectAll("." + css_class + "_points")
//        .on("mouseover", function(d, i) {
//          if (editable_test() && !that.target) {
//            d3.select(d3.event.target)
//              .transition()
//                .duration(100)
//                  .attr("r", 8);
//          }
//        })
//        .on("mouseout", function(d, i) {
//          if (!that.target) {
//            d3.select(d3.event.target)
//              .transition()
//                .duration(100)
//                  .attr("r", 5);
//          }
//        })
//        .on("mousedown", function(d, i) {
//          if (editable_test()) {
//            d3.select(d3.event.target)
//              .transition()
//                .duration(100)
//                  .attr("r", 8);
//            that.target = d3.event.target;
//            that.vis
//              .on("mousemove", function(d, j) {
//                if (that.target) {
//                  that.update_point_on_screen(that.target, d3.event, {
//                      lock: {x: true}, quantize: that.quantize
//                  });
//                  that.update_depths_on_screen(that.target, coords, depths, i,
//                      css_class);
//                  that.update_sections(coords);
//                }
//              });
//          }
//        });
    },
    /**
     * Draw a section of depth line
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} depth Depth of section end
     * @param {integer} index Index of depth at end of section
     * @param {array} depths Array of depth measurements for the whole line
     * @param {array} coords Array of coordinates for the slope profile
     * @param {object} options Hash of options
     * @param {boolean} options.quantize Quantize the depth line section to the
     *                                   model mesh
     * @return {d3.svg.line} SVG Line representing this depth line section
     */
    draw_depth_section: function(depth, index, depths, coords, options) {
      var quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        quantize = options.quantize;
      }
      if (index > 0) {
        var slope_coords = coords[index - 1];
        var d_slope_coords = coords[index];
        var depth_x_y_prev = this.model.compute_depth_x_y(depths[index - 1], coords, {quantize: quantize});
        var depth_x_y = this.model.compute_depth_x_y(depth, coords, {quantize: quantize});
        var depth_coords = this.model.compute_line_coords(depth_x_y_prev, depth_x_y, index,
            coords, depths);
        return this.line(depth_coords);
      } else {
        return;
      }
    },
    /**
     * Return an svg line representing the desired slope section
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {object} coord Coordinate of section end point
     * @param {integer} index Index of end point coordinate
     * @return {d3.svg.line} SVG line representing this section of slope
     */
    draw_slope_section: function(coord, index) {
      if (index > 0) {
        return this.line([this.coords[index - 1], coord]);
      } else {
        return;
      }
    },
    /**
     * Draw cells if mesh data is available
     * @memberof Mossaic.vis.Slope
     * @instance
     * @private
     * @param {d3.svg.g} SVG g element to which cells should be drawn
     */
    draw_cells: function() {
      var g = this.mesh_g;
      var that = this;
      var geometry = this.model.get("geometry");
      if (typeof(geometry) === "undefined" ||
          typeof(geometry.mesh_data) === "undefined") {
        return;
      }
      var mesh_data = geometry.mesh_data;
      // Convert mesh data to array of {x: i, y: j, soil_type: k}
      var cells = [];
      var width_so_far = 0;
      _.each(mesh_data, function(col) {
        var number_of_cells = col.cells.soil_type.length;
        var height_so_far = 0;
        cells = cells.concat(_.map(_.range(number_of_cells),
          function(i) {
            var row_from_bottom = number_of_cells - 1 - i;
            var depth = col.cells.cell_depth.value[row_from_bottom];
            height_so_far += depth;
            var cell = {
              x: width_so_far,
              y: height_so_far,
              w: col.column_width.value,
              h: depth,
              soil_type: col.cells.soil_type[row_from_bottom]
            };
            return cell;
        }));
        width_so_far += col.column_width.value;
      });
      g.selectAll(".cells")
        .data(cells)
        .enter()
          .append("svg:rect")
          .attr('class','slope-cell')
          .attr("x", function(d) {
            return that.x(d.x);
          })
          .attr("y", function(d) {
            return -1 * that.y(d.y);
          })
          .attr("width", function(d) {
            return that.x(d.w) - that.x(0);
          })
          .attr("height", function(d) {
            return Math.abs(that.y(d.h) - that.y(0));
          })
          .attr("fill", function(d) {
            return that.soil_colors(SOIL_COLOR_OFFSET - d.soil_type);
          });
    },
    /**
     * Re-draw the slope view
     * @memberof Mossaic.vis.Slope
     * @instance
     */
    redraw: function() {
      this.vis.remove();
      delete this.vis;
      this.enabled_modes = [];  // Clear all the enabled modes
      this.draw();
    },

    /**
     * Draws the graphs "chrome".  Axis, labels, grid etc
     */
    draw_empty_graph: function() {
      var that = this;
      var x_max = this.x_max;
      var y_max = this.y_max;
      // Draw the mesh
      var mesh = this.model.get_mesh();
      var mesh_cols = _.map(_.range(0, Math.ceil(x_max * 1.05), mesh.x), function(x) {
        return { x1: x, x2: x, y1: 0, y2: y_max * 1.05 };
      });
      var mesh_rows = _.map(_.range(0, Math.ceil(y_max * 1.05), mesh.y), function(y) {
        return { x1: 0, x2: x_max * 1.05, y1: y, y2: y};
      });
      this.mesh_g.selectAll(".mesh")
        .data(mesh_cols.concat(mesh_rows))
        .enter().append("svg:line")
          .attr("class", "mesh")
          .attr("x1", function(d) { return that.x(d.x1); })
          .attr("x2", function(d) { return that.x(d.x2); })
          .attr("y1", function(d) { return -1 * that.y(d.y1); })
          .attr("y2", function(d) { return -1 * that.y(d.y2); });

      this.axis_g = this.vis.append("svg:g")
        .attr("transform", "translate(0, " + this.height + ")")
        .attr("id", "axis_g");

      this.slope_g = this.vis.append("svg:g")
        .attr("transform", "translate(0, " + this.height + ")")
        .attr("id", "slope_g");

      this.slope_g.append("rect")
        .attr("x", that.x(0))
        .attr("y", that.y2(y_max) + that.y2(0))
        .attr("width", that.x(x_max))
        .attr("height",that.y(y_max))
        .style("fill","none")
        .style("pointer-events", "all"); // needed because the above fill:none ignores clicks by default


      // Draw axes
      this.axis_g.append("svg:line")
        .attr("x1", this.x(0))
        .attr("y1", this.y2(0))
        .attr("x2", this.x(x_max * 1.05))
        .attr("y2", this.y2(0));

     this.axis_g.append("svg:line")
        .attr("x1", this.x(0))
        .attr("y1", this.y2(0))
        .attr("x2", this.x(0))
        .attr("y2", this.y2(y_max * 1.05));

      // Now add labels...
      var tmp_x_max = 1.05 * x_max;
      var xticks = _.filter(this.x.ticks(5), function(x) {
        return x < tmp_x_max;
      });
      this.axis_g.selectAll(".x_label")
        .data(xticks)
        .enter().append("svg:text")
          .attr("class", "x_label noselect")
          .text(String)
          .attr("x", function(d) {
            return that.x(d);
          })
          .attr("y", -this.margin / 3)
          .attr("text-anchor", "middle");

      var tmp_y_max = 1.05 * y_max;
      var yticks = _.filter(this.y.ticks(5), function(y) {
        return y < tmp_y_max;
      });
      this.axis_g.selectAll(".y_label")
        .data(yticks)
        .enter().append("svg:text")
          .attr("class", "y_label noselect")
          .text(String)
          .attr("x", this.margin / 2)
          .attr("y", function(d) {
            return -1 * that.y(d);
          })
          .attr("text-anchor", "middle")
          .attr("dy", 4);

      // And the ticks...
      this.axis_g.selectAll(".x_tick")
        .data(xticks)
        .enter().append("svg:line")
          .attr("x1", function(d) {
            return that.x(d);
          })
          .attr("x2", function(d) {
            return that.x(d);
          })
          .attr("y1", -this.margin + 5)
          .attr("y2", -this.margin);

      this.axis_g.selectAll("y.tick")
        .data(yticks)
        .enter().append("svg:line")
          .attr("x1", this.margin - 5)
          .attr("x2", this.margin)
          .attr("y1", function(d) {
            return -1 * that.y(d);
          })
          .attr("y2", function(d) {
            return -1 * that.y(d);
          });
    },

    /**
     * Draw Soil Strata lines
     */
    draw_soil_strata: function() {
      var that = this;
      // Draw soil strata
      if (this.model.is_soil_known()) {
        this.soil_depths = this.model.get_soil_depths();
        for (var strata = 0; strata < this.soil_depths.length; ++strata) {
          if (typeof(this.soil_depths[strata]) !== "undefined" ) {
            this.draw_depth_line({
              coords: this.coords,
              depths: this.soil_depths[strata],
              stroke: this.soil_colors(SOIL_COLOR_OFFSET - strata),
              stroke_width: "3px",
              css_class: "soil_strata_" + strata,
              quantize: this.quantize,
              editable_test: function() {
                return this.model.editable.soil_strata;
              }
            });
          }
        }
      } else {
        this.soil_depths = this.model.get_soil_estimates();
        for (var strata = 0; strata < this.soil_depths.length; ++strata) {
          if (typeof(this.soil_depths[strata]) !== "undefined") {
            this.draw_depth_estimate({
              coords: that.coords,
              depths: this.soil_depths[strata],
              stroke: this.soil_colors(SOIL_COLOR_OFFSET - strata),
              stroke_width: "3px",
              css_class: "soil_strata_" + strata,
              quantize: this.quantize
            });
          }
        }
      }

    },

  /**
   * Draw slope segments
   */
    draw_slope_segments: function() {
      var that = this;
       // Draw slope segments
      this.slope_g.selectAll("path").select(".slope_sections")
        .data(this.coords)
        .enter().append("svg:path")
          .attr("d", this.draw_slope_section)
          .attr("class", "slope_sections");
      var road_prism = this.model.get("geometry").road;

      // NOTE: Mouseover/click event code below is legacy and needs moving into a "set_<mode>" function
      this.slope_g.selectAll(".slope_sections")
        .style("stroke", function(d, i) {
          if (road_prism.slope_section == that.model.coords_index_to_section_index(i) - 1) {
            return "sienna";
          } else {
            return "steelblue";
          }
        })
        .on("mouseover", function(d, i) {
          if (that.model.editable.datum && that.is_selectable(i) && !that.target && that.model.mode === "questa") {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                .style("stroke-width", 10);
          }
        })
        .on("mouseout", function(d, i) {
          if (that.model.editable.datum && that.model.mode === "questa") {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                .style("stroke-width", 6);
          }
        })
        .on("click", function(d, i) {
          if (that.model.editable.datum && that.is_selectable(i) && that.model.mode === "questa") {
            var geometry = _.clone(that.model.get("geometry"));
            if (typeof(geometry.road) === "undefined") {
              geometry.road = {};
            } else {
              geometry.road = _.clone(geometry.road);
            }
            geometry.road.slope_section =
                that.model.coords_index_to_section_index(i) - 1;
            that.model.changed_by = that;
            that.model.set({geometry: geometry});
          }
        });

    },

    /**
     * Draw point circles on slope segments
     */
    draw_point_circles: function() {
      var that = this;
       // Draw point circles
      var datum_index;
      var geometry = this.model.get("geometry");
      if (typeof(geometry) !== "undefined" &&
          typeof(geometry.datum) !== "undefined") {
        datum_index = geometry.datum.index;
      }
      this.slope_g.selectAll("circle").select(".slope_points")
        .data(this.coords)
        .enter().append("svg:circle")
          .attr("class", "node")
          .attr("cx", function(d) { return that.x(d.x); })
          .attr("cy", function(d) { return -1 * that.y(d.y); })
          .attr("r", 5)
          .attr("class", "slope_points")
          .style("fill", function(d, i) {
            if (i == datum_index) {
              return "red";
            } else {
              return "orange";
            }
          });
    },

    /**
     * Render tooltips on all circles
     */
    update_tooltips: function(){
      var that = this;
      this.slope_g.selectAll('circle')
      .on('mouseover.tooltip',function(){
        that.toggle_tooltip(true);
      })
      .on('mouseout.tooltip',function(){
        that.toggle_tooltip(false);
      })
      ;
    },

    /**
     * Draw water table.
     */
    draw_water_table: function() {
      var that = this;
      this.draw_depth_line({
        coords: that.coords,
        depths: this.water_depths,
        css_class: "water_table",
        stroke: "lightskyblue",
        stroke_width: "3px",
        quantize: this.quantize,
        editable_test: function() { return that.model.editable.water_table }
      });
    },

    /**
     * Sets a mode that draws and updates a box showing the mouse cursor coords in meters
     */
    set_show_onscreen_coords: function() {
      this.$el.find("#cursor-coordinates").remove(); // Take any existing HTML out in case we are re-rendering.
      this.$el.mustache("vis.cursor-coords", {}, {method: "prepend"} );
      var that = this;
      this.vis.on("mousemove.coords", function(event) {
        that.$el.find("#coords_x_m").val(Mossaic.utils.round(that.x.invert(d3.mouse(that.mesh_g[0][0])[0]), 2));
        that.$el.find("#coords_y_m").val(Mossaic.utils.round(that.y2.invert(d3.mouse(that.mesh_g[0][0])[1]), 2));
      });
    },

    /**
     * End the show_onscreen_coords mode
     */
    end_show_onscreen_coords: function() {
      this.$el.find("#cursor-coordinates").remove();
      this.vis.on("mousemove.coords", null);
    },

    /**
     * Sets a mode for the user to freehand draw the slope surface.  The user clicks to draw slope sections from the
     * top left of the graph, until they hit the x-axis.  An optional callback function can be registered for
     * when slope is finished (they've hit the x-axis).
     * @param {object} options
     * @param {function} [options.callback] Callback function to be triggered when the users slope hits the x-axis
     */
    set_draw_surface: function (options) {
      var that = this;
      var options = options || {};
      var x,y;
      var x_max = that.x_max;
      var y_max = that.y_max;
      if (typeof(options.callback) === "function") {
        var callback = options.callback;
      }

      // An array to hold coords of clicks.  Start out at the top left of the slope
      var data = [[that.x(0),-1*that.y(y_max)]];

      var draw_g = this.slope_g;
      draw_g.append("svg:g", "*")
        .attr("transform", "translate(0, " + this.height + ")")
        .attr("id", "draw_g");

      // New empty rectangle to seperate mouse events from anything else
      draw_g.append("rect")
        .attr("x", that.x(0))
        .attr("y",  that.y2(y_max))
        .attr("width", that.x(x_max))
        .attr("height", that.y(y_max) - that.y(0))
        .style("fill","none")
        .style("pointer-events", "all"); // needed because the above fill:none ignores clicks by default

      draw_g.on("mousemove.draw", function(d,i) {
          // Find the thin black line (initially created below) and update it
          x = d3.mouse(this)[0];
          y = d3.mouse(this)[1];

          if (x < data[data.length - 1][0]) x = data[data.length - 1][0]; // force to be between vertical...
          if (y < data[data.length - 1][1]) y = data[data.length - 1][1]; // ... and horizontal of the last point
          if (y > that.y2(0) - 10) y = that.y2(0)   //  Snap if < 10px above to the x-axis.
                                                    //  Quirky calculations because of the y-axis inverse
          draw_g.select("#line")
            .attr("x1", data[data.length - 1][0])
            .attr("y1", data[data.length - 1][1])
            .attr("x2", x)
            .attr("y2", y);
        });

      // The slope itself
      draw_g.append("path").attr("class","drawable");

      // Thin black line to aid user in where their slope segment will go
      draw_g.append("line").attr("id","line");

      // Slope sections
      var line = d3.svg.line()
        .x(function(d) { return d[0]; })
        .y(function(d) { return d[1]; });

      draw_g.on("click.draw",function(){

        data.push([x,y]);

        // Update the SVG slope segments & circles
        draw_g.select("path").attr("d",line(data));
        draw_g.selectAll("circle.drawable")
          .data(data)
          .enter().append("circle")
          .attr("class","drawable")
          .attr("cx", function(d) { return d[0];})
          .attr("cy", function(d) {return d[1];})
          .attr("r",5);

        // We've hit the x-axis.  End drawing, and update the model with the drawn lines
        if (y ===  that.y2(0)) {
          draw_g.select("line").remove();

          var model_data = [];
          data.forEach(function(point) {
            model_data.push({
              x: that.x.invert(point[0]),      // "invert" converts from pixels to our "real life" domain, which is meters
              y: that.y2.invert(point[1])
            });
          });

          that.model.changed_by = that;

//          Commenting out until this works properly, see doc comments on
//            models.geometry.calculate_extra_sections_from_coords()
//           model_data = that.model.calculate_extra_sections_from_coords(model_data);

          // For historical reasons, we need to add slope sections to the model individually, using height and slope
          // length rather than the coords
          for (var i = 0; i < model_data.length -1; i++) {
            var surface_length = Math.sqrt(
              Math.pow(model_data[i+1].x - model_data[i].x , 2) +
              Math.pow(model_data[i].y - model_data[i+1].y , 2)
            );
            var surface_height = (model_data[i].y - model_data[i+1].y);
            that.model.add_section({"surface_length":surface_length, "surface_height":surface_height});
          }

          // Redraw the slope using the model sections, and without drawing mode enabled
          that.end_mode("draw_surface");
          that.redraw();

          if (typeof(callback === "function")) callback();
        }
      });
  },

    end_draw_surface: function() {
      this.slope_g.on("mousemove.draw",null);
      this.slope_g.on("click.draw",null);
    },

    /**
     * Update the tooltip
     */
    toggle_tooltip: function(show){
      if(show){
        var target = d3.event.target;
        var new_x = this.x.invert(d3.select(target).attr("cx"));
        var new_y = this.y2.invert(d3.select(target).attr("cy"));
        this.tooltip.transition()
          .duration(200)
          .style("opacity",0.9);
        this.tooltip.html("X: " + new_x.toFixed(2) + "<br/>Y: " + new_y.toFixed(2))
          .style("left",(d3.event.pageX)+"px")
          .style("top",(d3.event.pageY - 28)+"px");
      }else{
        this.tooltip.transition()
          .duration(500)
          .style("opacity",0);
      }
    },

    /**
     * Sets a mode for the user to drag existing slope points around.  Updates the model with new values if a section
     * is changed
     */
    set_edit_surface: function() {
      var that = this;


       this.slope_g.selectAll(".slope_points")
        .on("mouseover.edit", function(d, i) {
          if (that.is_moveable(i) && !that.target) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 8);
          }
        })
        .on("mouseout.edit", function(d, i) {
          if (!that.target) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 5);
          }
        })
        .on("mousedown.edit", function(d, i) {
          if (that.is_moveable(i)) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 8);
            that.target = d3.event.target;
            that.vis.on("mousemove.edit", function(d, j) {
              if (that.target) {
                that.update_point_on_screen(that.target, d3.event, {
                    quantize: false // Always false - it's prettier that way
                });
                that.update_coords(that.target, that.coords, i); // i, not j
                that.update_sections(that.coords);
                that.set_clip_path();
              }
            });
          }
        })
        .on("mouseup.edit", function(d, i) {
          that.target = undefined;
          d3.select(d3.event.target)
            .transition()
              .duration(100)
                .attr("r", 5);
          that.vis.on("mousemove.edit", null);
        });
    },

    end_edit_surface: function() {
      this.slope_g.selectAll(".slope_points")
        .on("mouseover.edit", null)
        .on("mousedown.edit", null)
        .on("mousemove.edit", null)
        .on("mouseup.edit", null);
    },

    /**
     * Sets a mode for the user to select their datum point.  An optional callback function can be registered for when
     * the user has set the datum.
     *
     * @param {object} options
     * @param {function} [options.callback] Callback function to be triggered when the users slope hits the x-axis
     */
    set_draw_datum: function(options) {
      var that = this;
      var options = options || {};
      if (typeof(options.callback) === "function") var callback = options.callback;

      var x,y;
      var height = 0;

      this.slope_g.append("line").attr("id", "datum_bar_v");
      this.slope_g.append("line").attr("id", "datum_bar_h");

      this.vis.on("mousemove.datum", function(d, i) {
        x = d3.mouse(this)[0];  // These are relative to container (svg), not element (g/rect).
        y = d3.mouse(this)[1];
        height = that.y2(that.model.compute_slope_height_from_x(that.x.invert(x), that.coords));
        if(isNaN(height)){
          return;
        }

        that.slope_g.selectAll("#datum_bar_v")
          .attr("x1", x)
          .attr("x2", x)
          .attr("y1", that.y2(that.y_min))
          .attr("y2", that.y2(that.y_max * 1.05));
        that.slope_g.selectAll("#datum_bar_h")
          .attr("x1", that.x(0))
          .attr("x2", that.x(that.x_max * 1.05))
          .attr("y1", height)
          .attr("y2", height);
      });

      this.slope_g.selectAll("circle.slope_points")
        .on("mouseover.datum", function(d, i) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 8);
        })
        .on("mouseout.datum", function(d, i) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 5);
        })
        .on("click.datum", function(d, i) {
          var geometry = _.clone(that.model.get("geometry"));
          var datum = geometry.datum;
          if (typeof(geometry.datum) === "undefined") {
            geometry.datum = {};
          } else {
            geometry.datum = _.clone(geometry.datum);
          }
          geometry.datum.index = i;
          that.model.set({geometry: geometry});
          that.model.editable.datum = false;
          d3.event.stopPropagation();   //Needed so the vis.click handler below doesn't fire as well
          that.end_draw_datum();
          that.redraw(true);
          if (typeof(callback === "function")) callback();
        });


      this.vis.on("click.datum", function(d, i) {
        var geometry = _.clone(that.model.get("geometry"));
        var datum = _.clone(geometry.datum) || {};

        var new_x = that.x.invert(x);
        var new_datum_distance = that.model.compute_surface_distance_from_x(new_x, that.coords);
        var new_section_index;

        new_section_index = that.model.split_section(new_datum_distance);
        var geometry = _.clone(that.model.get("geometry"));
        var datum = _.clone(geometry.datum) || {};
        datum.index = new_section_index;
        geometry.datum = datum;
        that.model.set({geometry: geometry});
        that.model.editable.datum = false;
        that.end_mode("draw_datum");
        that.redraw(true);
        if (typeof(callback === "function")) callback();
      });
    },

    end_draw_datum: function() {
      this.slope_g.selectAll("circle.slope_points").on("mouseover.datum", null);
      this.vis.on("click.datum", null);
    },

    /**
     * Sets a mode for the user to draw the soil strata boreholes.  See docs for _set_draw_boreholes() for more details.
     */
    set_draw_soil_boreholes: function() {
      this.boreholes.soils = [];  // We pass this to the fn below, which then populates it with the borehole data the
                                  // user drew.
      this._set_draw_boreholes(this.boreholes.soils);
    },

    end_draw_soil_boreholes: function() {
      this._end_draw_boreholes();
    },

    set_draw_water_table_boreholes: function() {
      this.boreholes.water_table = [];
      this._set_draw_boreholes(this.boreholes.water_table);
    },

    end_draw_water_table_boreholes: function() {
      this._end_draw_boreholes();
    },

    /**
     * Draw vertical boreholes.  A vertical line can be moved across the graph, and the user clicks to mark each
     * borehole.  Drawing ends when the 'private' "_end_draw_boreholes()" fn is called, which should be called by
     * a 'public' "end_xxx_boreholes()" fn, which is likely called by an external event.
     * Generally by an event handler bound to a button outside this graphic.
     *
     * An empty array must be passed in, which is where the boreholes will be saved into.  This can then also be passed
     * to other functions (such as "_set_draw_strata()").
     *
     * @param {array} boreholes An empty array that will be populated with objects containing the boreholes.
     */
    _set_draw_boreholes: function(boreholes) {
      var that = this;
      var x_max = this.x_max;
      var y_max = this.y_max;
      var y_min = this.y_min;
      var x, y, height;

      // data array holds the borehole data.  We init it with a borehole at x=0, height=y_max
      // data.interface_y_coords = array to hold y-coords for each interface
      boreholes.push({x: 0, height: y_max, interface_y_coords: []});

      var slope_g = this.slope_g;

      // Vertical moving borehole bar
      slope_g.append("line")
        .attr("id","borehole_bar_v")
        .attr("clip-path","url(#slope_clip)")   //#slope_clip is a clipping path to the slope surface, set in slope_g
        .attr("y1", that.y2(y_min))
        .attr("y2", that.y2(y_max));

//      slope_g.on("mouseleave.boreholes",function() {
//        slope_g.selectAll("#borehole_bar_v").style("display", "none");
//      });

      slope_g.on("mousemove.boreholes", function(d,i) {
        x = d3.mouse(this)[0];  // These are relative to container (svg), not element (g/rect).
        y = d3.mouse(this)[1];

        // Next borehole must be set to the right of the last one
        if (x < that.x((boreholes[boreholes.length-1].x))) x = that.x(boreholes[boreholes.length-1].x);

        // Move the vertical bar
        slope_g.selectAll("#borehole_bar_v")
          .style("display", "") // Make it visibile, if it was hidden
          .attr("x1", x)
          .attr("x2", x);

      });

      slope_g.on("click.boreholes", function(d, i) {
        boreholes.push({
          x : that.x.invert(x), // distance in meters
          // Slope height (in m) @ x
          height : that.model.compute_slope_height_from_x(that.x.invert(x), that.coords, {quantize: false}),
          interface_y_coords: []
        });

        // Draw permanent vertical line
        slope_g.selectAll("line.borehole")
          .data(boreholes).enter()
          .append("line")
          .attr("class","borehole")
          .attr("x1", function(d) { return that.x(d.x)})
          .attr("x2", function (d) { return that.x(d.x)})
          .attr("y1", function(d) { return that.y2(y_min); })
          .attr("y2", function(d) { return  that.y2(d.height);});
      });

    },

    /**
     * Ends borehole drawing mode.
     */
    _end_draw_boreholes: function() {
      this.slope_g.on("mouseleave.boreholes", null);
      this.slope_g.on("mousemove.boreholes", null);
      this.slope_g.on("click.boreholes", null);
      this.slope_g.selectAll("#borehole_bar_v").remove();
    },

    /**
     * Sets a mode for the user to draw the soil strata themselves.  See docs for _set_draw_strata() for more details.
     */
    set_draw_soils: function() {
      this._set_draw_strata(this.boreholes.soils, {
        colours: this.soil_colors
      });
    },

  /**
   * Saves the soils that have been drawn into the model.  Generally called in response to an external event.
   */
    save_soils_to_model: function() {
      this.model.add_soils_from_coords(this.boreholes.soils,{
        sigfigs:2
      });
    },

    end_draw_soils: function() {
      this._end_draw_strata();
    },

    set_draw_water_table: function(options) {
      var options = options || {};
      options.strata_limit = 1;
      options.colours = function() { return "lightskyblue";};
      this._set_draw_strata(this.boreholes.water_table, options);
    },

    save_water_table_to_model: function() {
      this.model.set_water_table_from_coords(this.boreholes.water_table,{
        sigfigs:2
      });
    },

    end_draw_water_table: function() {
      this._end_draw_strata();
    },


    /**
     * Function for drawing soil and water strata.  It shows a moveable circle on each vertical borehole, and
     * when the user clicks it draws a line between the boreholes, and moves to the next borehole.
     * @param {array} boreholes Array of boreholes containing objects of the form:
     *                            {x: <x coord in meters>,
     *                            height: <borehole height in meters>,
     *                            interface_y_coords: []}
     * @param {object} options     Hash of options.
     * @param {integer} [options.strata_limit] Maximum number of strata lines.  Strata drawing will end when this is hit.
     * @param {function} [options.colours] Function for strata line colours.
     */
    _set_draw_strata: function(boreholes, options) {
      var that = this;
      var options = options || {};
      var strata_limit = options.strata_limit || 0;
      var strata_colours = options.colours || this.soil_colors;

      var slope_g = this.slope_g; // Save typing "this." everywhere...
      var interface_lines = [[]]; // Used purely for the svg:paths to draw the soil lines onscreen,

      var borehole_index = 0;
      var strata_index = 0;
      var x,y;

      // Each time we are called we need a new id "namespace" for the strata lines.  Obviously not a true UUID.
      var uuid = "S" + Math.floor(Math.random()*1e6);

      // For svg:path strata sections
      var line = d3.svg.line()
        .x(function(d) { return d.x; })
        .y(function(d) { return d.y; });

       // The circle that moves up and down the boreholes
        slope_g.append("circle")
        .attr("id", "strata_interface_circle");


      var add_new_path = function(strata_index) {
        slope_g.append("path")
          .attr("id", uuid + "_" + strata_index)
          .attr("class","strata_interface_line")
          .attr("stroke", strata_colours(SOIL_COLOR_OFFSET - strata_index))
          .attr("clip-path","url(#slope_clip)");   //#slope_clip is a clipping path to the slope surface, set in slope_g
      };

      // 1st interface line, future ones get added when the previous one ends.
      add_new_path(0);

      slope_g.on("mousemove.strata", function() {
        // Constrain the moving circle to the current vertical borehole line
        x = that.x(boreholes[borehole_index].x);
        y = d3.mouse(this)[1];
        var borehole = boreholes[borehole_index];
        if ( y < that.y2(borehole.height)) y = that.y2(borehole.height);

        // Move the circle
        slope_g.selectAll("circle#strata_interface_circle")
          .attr("cx", x)
          .attr("cy", y)
          .attr("r",7);
      });

      slope_g.on("click.strata", function() {
         slope_g.append("circle")
          .attr("class","strata_interface_circle")
          .attr("fill", strata_colours(SOIL_COLOR_OFFSET - strata_index))
          .attr("cx", x)
          .attr("cy", y)
          .attr("r",5);;

        boreholes[borehole_index].interface_y_coords.push(that.y2.invert(y));
        interface_lines[strata_index].push({x: x, y: y});
        slope_g.select("#" + uuid + "_" +strata_index).attr("d",line(interface_lines[strata_index]));
        borehole_index = (borehole_index + 1) % boreholes.length;
        if (borehole_index === 0) {

          // Extend the line from the last borehole to the edge of the grid
          // It will be clipped by the slope edge/x-axis.  There's probably a shorter way, but this works.
          var x_1 = interface_lines[strata_index][boreholes.length-1].x;
          var x_2 = interface_lines[strata_index][boreholes.length-2].x;
          var x_max = that.x(that.x_max);
          var y_1 = interface_lines[strata_index][boreholes.length-1].y;
          var y_2 = interface_lines[strata_index][boreholes.length-2].y;
          var ratio = (y_1 - y_2) / (x_1 - x_2);
          interface_lines[strata_index].push({
            x: x_max,
            y: y_1 + ratio * (x_max - x_1)
          });
          slope_g.select("#" + uuid + "_" + strata_index).attr("d",line(interface_lines[strata_index]));
          strata_index++;
          if (strata_limit > 0 && strata_limit == strata_index) {
            that._end_draw_strata();
            if (typeof(options.on_strata_ended) === "function") {
              options.on_strata_ended();
            }
          } else {
            interface_lines[strata_index] = [];
            add_new_path(strata_index);
          }

        }
      });
    },

    _end_draw_strata: function() {
      this.slope_g.on("mousemove.strata", null);
      this.slope_g.on("click.strata", null);
      this.slope_g.selectAll("#strata_interface_circle").remove();
    },


    /**
     * Draw the slope display and, if specified, call this.on_draw_callback when
     * complete.
     *
     * SVG origin is the top left, and vertical (i.e. y) coords move *down* as they increase.  This is inconvient
     * for drawing slopes, where the bottom left would be the natural origin.  So we insert the SVG g elements
     * with a translation of the height of the graph, which puts them *out* of the main SVG.  We then use *negative*
     * y coordinates to move up back *up* into the graph.  This makes (0,0) = bottom left, and (0,-height) the top left.
     *
     * When converting from domain to range, the .x() and .y2() functions below handle this automatically.  It
     * mainly impacts comparisons e.g. to test if the mouse cursor is *above* a given y_coord, the test will be:
     *  if (mouse.y < y_coord) etc
     *
     * @memberof Mossaic.vis.Slope
     * @instance
     */
    draw: function() {
      var that = this;


      var get_y_max = function() {
        if (typeof(that.coords) === "undefined") {
          return 0;
        }
        var candidates = [d3.max(that.coords, function(d) {
          return d.y;
        })];
        candidates.push(d3.max(that.coords, function(d, i) {
          if (typeof(that.water_depths) !== "undefined") {
            return d.y - that.water_depths[i];
          } else {
            return d.y;
          }
        }));
        candidates.push(d3.max(that.coords, function(d, i) {
          var candidates = _.map(that.soil_depths, function(strata) {
            if (typeof(strata) !== "undefined") {
              return d.y - strata[i];
            } else {
              return d.y;
            }
          });
          return d3.max(candidates);
        }));
        return d3.max(candidates);
      }


      this.coords = this.model.compute_coords({
        quantize: this.quantize // Do we ever need to use all this quantize stuff?!  Isn't it always false...?
      });

      this.water_depths = this.model.get_water_depths();
      var x_min, x_max, y_min, y_max;
      x_min = 0;  // Always both 0.
      y_min = 0;

      // If we don't have enough coords from the model...
      if (this.coords.length < 2)  {
        /// ... but we have some defaults use them.
        if ( (!isNaN(this.initial_width)) && !isNaN(this.initial_height)) {
          x_max = this.initial_width;
          y_max = this.initial_height;
          this.coords = [{x:0,y:y_max}];  // Slope must start at the top
        } else {
          console.log("Have neither model coords nor initial dimensions.  Can't draw slope graphic");
          return;
        }
      } else {
        // Otherwise calculate maximums from the model coords.
        x_max = d3.max(this.coords, function (d) { return d.x; });
        y_max = get_y_max("max");
      }

      this.x_max = x_max; // Put them into the object for other methods to use
      this.y_max = y_max;
      this.y_min = y_min;
      this.x_min = x_min;

      // Calculate maximum svg size maintaining correct x/y ratio and within our maximum size on screen
      // Range = pixels.  Domain = meters.

      // Situations:



      var range_aspect_ratio = this.max_height / this.max_width;
      var domain_aspect_ratio = y_max / x_max;
      if (range_aspect_ratio > domain_aspect_ratio) {
        // Maximum width, adjust the height to scale
        this.width = this.max_width;
        this.height = (this.max_width * domain_aspect_ratio);
      } else {
        // Maximum height, adjust the width to scale
        this.height = this.max_height;
        this.width = this.max_height / domain_aspect_ratio;
      }



      // Legacy scale that *doesn't* invert y-axis (see jDoc comments above).  Don't use in new code.
      // (When no more code references "this.y" remove this scale, and rename this.y2 to this.y)
      //this.y = d3.scale.linear().domain([y_min, domain_max]).range([0, range_max]);
      this.y = d3.scale.linear().domain([y_min, y_max]).range([0 + this.margin, this.height - this.margin]);
      // Scale that *does* invert y-axis.  Use this in new code.
      this.y2 = d3.scale.linear().domain([y_min, y_max]).range([-1*(0 + this.margin), -1 *(this.height - this.margin)]);
      this.x = d3.scale.linear().domain([x_min, x_max]).range([0 + this.margin, this.width - this.margin]);

      if (!this.vis) {
        this.vis = this.div.append("svg:svg");
      }


      this.vis
        .style("width", this.width)
        .style("height", this.height)
        .style("margin", "0 auto")
        .style("display","block");

      this.mesh_g = this.vis.insert("svg:g", "*")
        .attr("transform", "translate(0, " + this.height + ")")
        .attr("id", "mesh_g");

      this.line = d3.svg.line()
          .x(function(d,i) { return that.x(d.x); })
          .y(function(d) { return that.y2(d.y); });

      this.draw_empty_graph();
      this.draw_slope_segments();
      this.draw_point_circles();
      this.draw_soil_strata();
      this.draw_water_table();
      if(this.show_cells){
        this.draw_cells();
      }else{
        this.hide_cells();
      }
      this.set_clip_path();
      this.update_tooltips();


      if (typeof(this.on_draw_callback) === "function") {
        this.on_draw_callback();
      }
    },

    enable_cells: function(){
      this.show_cells = true;
      this.draw_cells();
    },
    disable_cells: function(){
      this.show_cells = false;
      this.hide_cells();
    },
    hide_cells: function(){
      this.mesh_g.selectAll(".slope-cell").remove();
    },

    render : function() {
      this.show();
      return this.el;
    }
  });
});
