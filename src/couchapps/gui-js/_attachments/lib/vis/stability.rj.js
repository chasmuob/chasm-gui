
define(["views/_basic.rj"], function(BasicView) {
  /**
   * Backbone View for Mossaic.models.Stability model. Users can interact with
   * the graphical view in order to update the model.
   *
   * Draws itself onto the svg element of a Mossaic.vis.Slope.
   * @constructor
   * @extends Backbone.View
   */
  return BasicView.extend({
    /**
     * Initialise the model. Called implicitly by "new Mossaic.vis.Stability"
     * @memberof Mossaic.vis.Stability
     * @instance
     * @param {object} model Stability model to be displayed
     * @param {object} options Hash of options
     * @param {Mossaic.vis.Slope} slope_display Slope display View to which
     *                                          this View will render
     * @param {object} options.quantize {boolean} quantize Quantize point to the
     *                                                     mesh defined in the
     *                                                     model
     */
    initialize: function(options) {
      _.bindAll(this);
      var that = this;
      /**
       * The model to be represented by this visualisation.
       * @type Mossaic.models.Geometry
       */
      this.model = options.model;
      this.models = options.models;
      this.model.bind("change", this.redraw);
      /**
       * Whether Mossaic.models.Stability coordinates should be quantized to
       * the mesh or not
       * @type boolean
       */
      this.quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        this.quantize = options.quantize;
      }
      /**
       * Slope visualisation to which the stability visualisation should be
       * rendered
       * @type Mossaic.vis.Slope
       */
      this.slope_display = options.slope_display;
      this.slope_display.on_draw(function() {
        that.redraw({snap: that.quantize});
      });
      /**
       * Set to true to hide the stability grid, false to display
       * @type boolean
       */
      this.hidden = true;
      /**
       * Set to true to hide the slip circle radius display (which is the usual
       * case, as it requires drawing a lot of clipped circles which does not
       * render quickly).
       * @type boolean
       */
      this.radius_hidden = true;
      /**
       * Set to true whenever an element is being dragged by the mouse
       * @type boolean
       */
      this.dragging = false;
    },
    /**
     * Set the model to be represented by the form.
     * @memberof Mossaic.vis.Stability
     * @instance
     * @param {Mossaic.models.Stability} model Model to be represented
     *                                         by this visualisation
     */
    set_model: function(model) {
      this.model = model;
      this.model.bind("change", this.redraw);
    },
    /**
     * Hide the visualisation by removing it from the DOM
     * @memberof Mossaic.vis.Stability
     * @instance
     */
    hide: function() {
      if(this.g){
        this.g.remove();
      }
      this.hidden = true;
    },
    /**
     * Show the visualisation
     * @memberof Mossaic.vis.Stability
     * @instance
     */
    show: function() {
      this.hidden = false;
      this.draw();
    },
    /**
     * Hide the slip search radiuses
     * @memberof Mossaic.vis.Stability
     * @instance
     */
    hide_radius: function() {
      if (!this.hidden) {
        this.g.remove();
        this.radius_hidden = true;
        this.draw();
      }
    },
    /**
     * Show the slip search radiuses
     * @memberof Mossaic.vis.Stability
     * @instance
     */
    show_radius: function() {
      if (!this.hidden) {
        this.g.remove();
        this.radius_hidden = false;
        this.draw()
      }
    },
    /**
     * Get the coordinates of each point on the stability grid
     * @memberof Mossaic.vis.Stability
     * @instance
     * @private
     * @param {object} options Hash of options
     * @param {object} options.origin Grid origin x and y
     * @param {object} options.spacing Grid spacing x and y
     * @param {object} options.grid_size Grid size x and y
     * @return Array of points for stability grid
     */
    get_grid_points: function(options) {
      var origin = options.origin;
      var spacing = options.spacing;
      var grid_size = options.grid_size;
      var grid_points = [];
      for (var x = 0; x < grid_size.x; ++x) {
        for (var y = 0; y < grid_size.y; ++y) {
          if (!(x == 0 && y == 0)) {
            grid_points.push({
              x: origin.x + x * spacing.x,
              y: origin.y + y * spacing.y
            });
          }
        }
      }
      return grid_points;
    },
    /**
     * Get the circles representing slip searches from each stability grid
     * point
     * @memberof Mossaic.vis.Stability
     * @instance
     * @private
     * @param {object} options Hash of options
     * @param {object} options.radius Radius definition with fields initial and increment
     * @param {object} options.origin Point describing grid origin
     * @param {array} options.grid_points Array of stability grid points
     * @return {array} All stability slip search radii for the given grid
     */
    get_radiuses: function(options) {
      var radius = options.radius;
      var origin = options.origin;
      var grid_points = options.grid_points;
      grid_points.push({
        x: origin.x,
        y: origin.y
      })
      var all_radiuses = [];
      _.each(grid_points, function(point) {
        var max_radius = point.y;
        var radiuses = _.map(
            _.range(radius.initial, max_radius, radius.increment),
            function(radius) {
              return {
                x: point.x,
                y: point.y,
                radius: radius
              }
            }
        );
        all_radiuses.push(radiuses);
      });
      return _.flatten(all_radiuses);
    },
    /**
     * Redraw slip circle radiuses
     * @memberof Mossaic.vis.Stability
     * @instance
     * @private
     * @param {object} options Hash of options (see
     *                         Mossaic.vis.Slope.get_radiuses)
     */
    redraw_radius: function(options) {
      var that = this;
      var radiuses = this.get_radiuses(options);
      var circles = this.g.selectAll(".radius").data(radiuses);
      var screen_zero = that.slope_display.x(0);
      circles
        .enter().append("svg:ellipse")
          .attr("class", "radius")
          .attr("cx", function(d) {
            return that.slope_display.x(d.x);
          })
          .attr("cy", function(d) {
            return -1 * that.slope_display.y(d.y);
          })
          .attr("rx", function(d) {
            return that.slope_display.x(d.radius) - screen_zero;
          })
          .attr("ry", function(d) {
            return that.slope_display.y(d.radius) - screen_zero;
          })
          .attr("clip-path", "url(#slope_clip)")
          .style("stroke", "#AAAAAA")
          .style("stroke-width", "0.5")
          .style("fill", "none")
      circles
        .exit()
          .remove()
      circles
        .attr("cx", function(d) {
          return that.slope_display.x(d.x);
        })
        .attr("cy", function(d) {
          return -1 * that.slope_display.y(d.y);
        })
        .attr("rx", function(d) {
          return that.slope_display.x(d.radius) - screen_zero;
        })
        .attr("ry", function(d) {
          return that.slope_display.y(d.radius) - screen_zero;
        });
      circles.attr("clip-path", "url(#slope_clip)");
    },
    /**
     * Draw slip circle radiuses
     * @memberof Mossaic.vis.Stability
     * @instance
     * @private
     * @param {object} options Hash of options (see
     *                         Mossaic.vis.Slope.get_radiuses)
     */
    draw_radius: function(options) {
      var that = this;
      var radiuses = this.get_radiuses(options);
      var screen_zero = that.slope_display.x(0);
      this.g.selectAll(".radius")
        .data(radiuses).enter().append("svg:ellipse")
          .attr("class", "radius")
          .attr("cx", function(d) {
            return that.slope_display.x(d.x);
          })
          .attr("cy", function(d) {
            return -1 * that.slope_display.y(d.y);
          })
          .attr("rx", function(d) {
            return that.slope_display.x(d.radius) - screen_zero;
          })
          .attr("ry", function(d) {
            return that.slope_display.y(d.radius) - screen_zero;
          })
          .attr("clip-path", "url(#slope_clip)")
          .style("stroke", "#AAAAAA")
          .style("stroke-width", "0.5")
          .style("fill", "none");
    },
    /**
     * Redraw stability grid
     * @memberof Mossaic.vis.Stability
     * @instance
     * @param {object} options Hash of options
     * @param {boolean} options.snap Snap to geometry mesh
     */
    redraw: function(options) {
      if (this.hidden) {
        return false;
      }
      var that = this;
      var snap = (options && options.snap);
      var origin = this.model.get("origin") || {};
      var spacing = this.model.get("spacing") || {};
      var grid_size = this.model.get("grid_size") || {};
      var radius = this.model.get("radius") || {};
      this.g.select("#origin")
        .transition()
          .duration(100)
          .attr("cx", this.slope_display.x(origin.x))
          .attr("cy", -1 * this.slope_display.y(origin.y));
      var counter = 0;
      var grid_points = this.get_grid_points({
        origin: origin,
        spacing: spacing,
        grid_size: grid_size
      });
      if (!this.radius_hidden) {
        this.redraw_radius({
          grid_points: grid_points,
          radius: radius,
          spacing: spacing,
          origin: origin
        });
      }
      this.g.selectAll(".grid_point")
        .data(grid_points)
          .exit()
            .transition()
            .duration(100)
              .attr("cx", function(d) {
                return that.slope_display.x(origin.x);
              })
              .attr("cy", function(d) {
                return -1 * that.slope_display.y(origin.y);
              })
            .remove();
      this.g.selectAll(".grid_point")
        .data(grid_points)
          .enter().append("svg:circle")
            .attr("id", function(d) {
              return "stability_grid_" + d.x + "_" + d.y;
            })
            .attr("class", "grid_point")
            .attr("cx", function(d) {
              return that.slope_display.x(origin.x)
            })
            .attr("cy", function(d) {
              return -1 * that.slope_display.y(origin.y)
            })
            .attr("r", 1)
            .style("fill", "#6C7B8B");
      if (this.dragging) {
        this.g.selectAll(".grid_point")
          .data(grid_points)
            .attr("cx", function(d) {
              return that.slope_display.x(d.x);
            })
            .attr("cy", function(d) {
              return -1 * that.slope_display.y(d.y);
            });
      } else {
        this.g.selectAll(".grid_point")
          .data(grid_points)
            .transition()
              .duration(function(d) { return 100; })//5 * counter++ })
              .attr("cx", function(d) {
                return that.slope_display.x(d.x);
              })
              .attr("cy", function(d) {
                return -1 * that.slope_display.y(d.y);
              });
      }
      if (snap) {
        this.snap_to_mesh();
      }
    },
    /**
     * Snap the grid to the mesh defined in Mossaic.models.Geometry
     * @memberof Mossaic.vis.Stability
     * @instance
     * @private
     */
    snap_to_mesh: function() {
      var origin_point = this.g.select("#origin");
      var cx = parseFloat(origin_point.attr("cx"));
      var cy = parseFloat(origin_point.attr("cy"));
      var new_origin = this.slope_display.model.quantize_coord({
          x: this.slope_display.x.invert(cx),
          y: this.slope_display.y.invert(-1 * cy)
      });
      origin_point
        .attr("cx", this.slope_display.x(new_origin.x))
        .attr("cy", -1 * this.slope_display.y(new_origin.y));
      var origin = _.clone(this.model.get("origin")) || {};
      // Only update if the coordinates are actually different
      if (origin.x !== new_origin.x || origin.y !== new_origin.y) {
        origin.x = new_origin.x;
        origin.y = new_origin.y;
        this.model.changed_by = this;
        this.model.set({origin: origin});
      }
    },
    /**
     * Draw the visualisation
     * @memberof Mossaic.vis.Stability
     * @instance
     */
    draw: function() {
      if (this.hidden) {
        return false;
      }
      this.set_model(this.models.stability);
      var that = this;
      var dragmove = function(d, i) {
        if (that.model.editable) {
          var origin_point = that.g.select("#origin");
          var old_cx = parseFloat(origin_point.attr("cx"));
          var new_cx = old_cx + d3.event.dx;
          var old_cy = parseFloat(origin_point.attr("cy"));
          var new_cy = old_cy + d3.event.dy;
          origin_point
            .attr("cx", new_cx)
            .attr("cy", new_cy);
          var origin = _.clone(that.model.get("origin")) || {};
          origin.x = that.slope_display.x.invert(new_cx);
          origin.y = that.slope_display.y.invert(-1 * new_cy);
          that.model.changed_by = that;
          that.model.set({origin: origin});
        }
      };
      this.g = this.slope_display.vis.insert("svg:g")
        .attr("transform", "translate(0, " + this.slope_display.height + ")");
      var origin = this.model.get("origin") || {};
      var spacing = this.model.get("spacing") || {};
      var grid_size = this.model.get("grid_size") || {};
      var radius = this.model.get("radius") || {};
      var drag = d3.behavior.drag()
          .on("dragstart", function(d) {
            if (that.model.editable) {
              // Set target on slope display to a non-null, so transitions
              // don't fire on mouseovers
              that.slope_display.target = "dummy";
              that.dragging = true;
            }
          })
          .on("drag", dragmove)
          .on("dragend", function() {
            if (that.model.editable) {
              that.slope_display.target = undefined;
              that.dragging = false;
              d3.select("#origin")
                .transition()
                .duration(100)
                  .attr("r", 5);
              if (that.quantize) {
                that.snap_to_mesh();
              }
            }
          });
      var grid_points = this.get_grid_points({
        origin: origin,
        spacing: spacing,
        grid_size: grid_size
      });
      if (!this.radius_hidden) {
        this.draw_radius({
          grid_points: grid_points,
          radius: radius,
          spacing: spacing,
          origin: origin
        });
      }
      this.g.selectAll(".grid_point")
        .data(grid_points)
          .enter().append("svg:circle")
            .attr("id", function(d) {
              return "stability_grid_" + d.x + "_" + d.y;
            })
            .attr("class", "grid_point")
            .attr("cx", function(d) { return that.slope_display.x(d.x) })
            .attr("cy", function(d) { return -1 * that.slope_display.y(d.y) })
            .attr("r", 1)
            .style("fill", "#6C7B8B");
      this.g.append("svg:circle")
        .attr("id", "origin")
        .attr("cx", this.slope_display.x(origin.x))
        .attr("cy", -1 * this.slope_display.y(origin.y))
        .attr("r", 5)
        .style("fill", "#6C7B8B")
        .on("mouseover", function(d, i) {
          if (that.model.editable) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                .attr("r", 8);
          }
        })
        .on("mousedown", function(d, i) {
          if (that.model.editable) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 8);
          }
        })
        .on("mouseout", function(d, i) {
          if (that.model.editable) {
            d3.select(d3.event.target)
              .transition()
                .duration(100)
                  .attr("r", 5);
          }
        })
        .call(drag);
      if (this.quantize) {
        this.snap_to_mesh();
      }
    }
  });
});
