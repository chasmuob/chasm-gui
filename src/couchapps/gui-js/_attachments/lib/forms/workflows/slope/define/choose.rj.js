define(["forms/_basic.rj"],  function(BasicForm) {
/**
 * Form for the user to name the slope, and choose their method of defining a slope: Freehand drawing, or editing data.
 *
 * The name they enter is used to name the entire geometry model.
 *
 * If they choose to draw the slope, they need to specify the height and width, which are then set on the supplied
 * slope graphic.
 *
 * This form module does a bit more than just bind to a model - and could possible be put somewhere else instead - but
 * having it here at least keeps it with the other slope form(s) for now.
 */
return BasicForm.extend({

    template: "forms._workflow._slope._define.choose",

    events : {
      "click #draw_slope_btn": "draw_slope",
      "click #enter_slope_measurements_btn" : "type_slope",
      "change #slope_name" : "update_slope_name"
    },

    /**
     * @param {object} options
     * @param {vis/slope.rj} options.slope_display Slope Graphic
     * @param {Mossiac.models.geometry} options.model Geometry model to save the name into
     * @param {function} options.on_end Function to be called with the users choice
     * @constructor
     */
    initialize: function(options) {
      BasicForm.prototype.initialize.apply(this,[options]);
      options = options || {};
      this.slope_display = options.slope_display;
      this.model = options.model;
      this.on_end = options.on_end || {};

      this.slope_display.hide();
      this.$el.mustache(this.template,this.model.attributes,{method:"html"});
     },

    /**
     * Event handler for the "Draw Slope" button.
     */
    draw_slope : function() {
      var height = parseFloat(this.$el.find("#initial_height_input").val());
      var width = parseFloat(this.$el.find("#initial_width_input").val());
      var name = this.$el.find("#slope_name").val();

      var errors = false;

      if (!name.length) {
        this.$el.find("#slope_name").highlightFlash("#b94a48");
        errors = true;
      }

      if (isNaN(height) || height === 0) {
        this.$el.find("#initial_height_input").highlightFlash("#b94a48");
        errors = true;
      }
      if (isNaN(width) || width === 0) {
        this.$el.find("#initial_width_input").highlightFlash("#b94a48");
        errors = true;
      }
      if (!errors) {
        this.slope_display.set_initial_dimensions({height: height, width: width});
        this.on_end("draw");  // on_end() is a fn supplied by the parent when we are instantiated
      }
    },

    /**
     * Event handler for the "Enter Slope Measurements" button
     */
    type_slope: function() {
      var name = this.$el.find("#slope_name").val();
      if (!name.length) {
        this.$el.find("#slope_name").highlightFlash("#b94a48");
        return;
      }

      this.model.set({drawable : false}, {silent:true});
      this.on_end("edit");
    },

    /**
     * Update the name in the geometry model from the form.
     */
    update_slope_name: function() {
      this.model.set("name", this.$el.find("#slope_name").val());
    },


    render: function() {
      return this.el;
    }

  });
});
