define(["forms/_basic.rj"], function(BasicForm){
  
  /**
   * A subform that the soil_strata & water_table forms extend.  It displays a table where users can edit 
   * soil strata or the water table.  (The water table is essentially the same as soil strata, but limited to one 
   * strata).
   * 
   * User can click buttons to add new boreholes (columns), and new strata (rows).  When any inputs change, the
   * update_model() fn is called.   Here, update_model() doesn't do anything, subclasses need to implement their 
   * own functionality.
   * 
   * TODOs:
   *   - Similar to forms/workflows/slope/edit.rj, this is heavily tied to the DOM which makes it brittle.  Needs a 
   *     better form/model binding, and this should be lightweight, with the model doing validation etc.
   */

  // Don't use jQs .data() API! - http://stackoverflow.com/a/15651670/1116197
  return BasicForm.extend({
    events: {
      "click .soil_strata-add_borehole": "add_borehole",
      "click .soil_strata-remove_borehole": "remove_borehole",
      "click .soil_strata-add_strata": "add_strata",
      "click .soil_strata-remove_strata": "remove_strata",
      "change input[type=text]": "update_model"
    },

    /**
     * 
     * @param {object} options
     * @param {Backbone.model} options.model Backbone model - depends on what the subclass is.
     * @param {object} options.buttons Prev/Next buttons from the parent tab. We disable them if the form is invalid
     *                                  so the user can't move on.
     */
    initialize: function(options) {
      BasicForm.prototype.initialize.apply(this,[options]);
      this.$el.mustache("forms._workflow._slope._soil_strata.edit");  
      this.buttons = options.buttons;   // We need these to enable/disable the next button if the form is valid

      /** Maintains a running counter of the number of boreholes */
      this.borehole_count = 0;

      /** Maintains a running counter of the number of strata */
      this.strata_count = 0;
      
      /** Subclasses must populate this object with their strata.  Strata must take the form:
       * [{
       *  "surface_distance_from_cut_toe":{"unit":"m","value":"3"},
       *  "depth":{"unit":"m","value":[2.48,7.63]}
       *  },
       *  ...]
       *  Where:
       *    "surface_distance_from_cut_toe.value" is the surface distance from the datum
       *    depth.value is an array of strata depths at the borehole.  Each depth.value array for each object in the 
       *    main array must be the same length
       */
      this.strata = null;
    },

    
    handle_change: function() {
      this.strata = this.model.get("geometry").soil_strata;
    },

    /**
     * Adds a borehole to the table.  If there are existing strata rows, it adds a new column to the end of each row.
     * If there is only one borehole it renders two inputs at each strata: depth & angle (of strata).  
     * If there is > 1 borehole it renders just the depth input at each strata
     */
    add_borehole: function() {
      var that = this;

      // Remove the initial empty cell we needed for the template
      if (this.borehole_count === 0) this.$el.find("#soil_strata-strata_placeholder_cell").remove();

      // Add a new cell to the header, but before the final (empty) one from the template
      this.$el.find("thead tr:last th:last").prev().after(
        $.Mustache.render("forms._workflow._slope._soil_strata._edit.column_header",  {
          n: that.borehole_count, // index from 0 for us...
          n_human: that.borehole_count + 1  // ...but 1 for the humans.
        })
      );
      
      // Add new cells to the body (if we have a body, i.e. strata_count > 0)
      if (this.strata_count > 0) {
        this.$el.find("tbody tr").each(function(index, row) { 
          var $cell = $("<td></td>");  // Create new cell
          // Add the depth_input to the cell
          $cell.mustache("forms._workflow._slope._soil_strata._edit.depth_input",  {
              borehole: that.borehole_count, 
              strata: index
            });
            
          // If we don't currently have any boreholes, the one we're adding needs an "angle" input as well
          if (that.borehole_count === 0) {
            $cell.mustache("forms._workflow._slope._soil_strata._edit.angle_input",  {
              borehole: 0, 
              strata: index
            });
          }

          $(row).find("th:last").before($cell);
        });
        
        // If we already had one borehole, then we don't need the angle any more because we are adding a 2nd borehole.
        // so remove it.
        if (this.borehole_count === 1 ) {
          this.$el.find(".soil_strata-angle_cell").remove();
        }
        
        // Disable the "next" button until they have completed the new entries
        this.buttons.next.attr("disabled", "disabled"); 
      }

      // Increment our counter...
      this.borehole_count++;

      // ...and update the colspans in the headers
      this.$el.find("#soil_strata-boreholes_header").attr("colspan", this.borehole_count);
      this.$el.find("#soil_strata-boreholes_footer").attr("colspan", this.borehole_count);

    },
            
    /**
     * Removes a borehole from the table.  If there are strata rows, it removes the whole column, and then renumbers
     * everything above, so the borehole numbering remains sequential
     * @param {event} event HTML event that triggered this.
     */
    remove_borehole: function(event) {
      var borehole = event.currentTarget.dataset.borehole;  // Borehole number we are removing

      // Find any inputs for that borehole, and remove their parents i.e. the <td>s and <th>s
      this.$el.find("input[data-borehole=" + borehole + "]").closest("th").remove();
      this.$el.find("input[data-borehole=" + borehole + "]").closest("td").remove();
      
      // Shift all the data-borehole values above the one we just removed down one, so we still number everything 
      // sequentially without gaps.
      for (var i = borehole; i < this.borehole_count; i++) {
        this.$el.find("[data-borehole=" + i+ "]").attr("data-borehole", i-1);
      }

      // Do the decrement
      this.borehole_count--;
      
      // Tidy up the main header & footer
      this.$el.find("#soil_strata-boreholes_header").attr("colspan", this.borehole_count  );
      this.$el.find("#soil_strata-boreholes_footer").attr("colspan", this.borehole_count  );

      if (this.borehole_count === 1) { 
        // We're down to 1 borehole => add in "angle" inputs
        this.$el.find("tbody tr td").each(function(index, el) { 
          $(el).mustache("forms._workflow._slope._soil_strata._edit.angle_input",  {
            borehole: 0, 
            strata: index
          });
        });
      }
      
      if (this.borehole_count === 0) this.$el.find("thead tr:last").append("<th>&nbsp;</th>");
            
      this.update_model();
    },
    
    /**
     * Adds a new strata row to the table.  Adds a table cell and input for each borehole (if any)
     */
    add_strata: function() {
      var that = this;
      var $row = $("<tr></tr>"); 
      $row.mustache("forms._workflow._slope._soil_strata._edit.strata_row_start", {
        n: this.strata_count,
        n_human: this.strata_count + 1
      });
      
      _.times(this.borehole_count, function(index) {
        var $cell = $("<td></td>");
        $cell.mustache("forms._workflow._slope._soil_strata._edit.depth_input",  {
          borehole: index, 
          strata: that.strata_count
        });
        if (that.borehole_count == 1) {
          // We only have one borehole, so add "angle" inputs in.
          $cell.mustache("forms._workflow._slope._soil_strata._edit.angle_input",  {
            borehole: 0, 
            strata: that.strata_count
          });
        }
        $row.append($cell);
      });
      
      $row.mustache("forms._workflow._slope._soil_strata._edit.strata_row_end", {
        n: this.strata_count,
        n_human: this.strata_count + 1
      });
      
      this.$el.find("tbody").append($row);
      this.strata_count++;
      this.$el.find("#soil_strata-strata_header").attr("rowspan", this.strata_count  );
      
      this.buttons.next.attr("disabled", "disabled"); // Disable the "next" button until they have completed the new entries
    },
            
    remove_strata: function(event) {
      var strata = event.currentTarget.dataset.strata;
      $(event.currentTarget).closest("tr").remove();  // head up the DOM tree until we find the <tr>
      for (var i = strata; i < this.strata_count; i++) {
        this.$el.find("[data-strata=" + i+ "]").attr("data-strata", i-1); //Renumber remaining strata
      }
      // Renumber visible borehole numbers for the humans
      this.$el.find(".soil_strata-strata_human_number").html(function(){
        return parseFloat(this.dataset.strata)+1;
      });
      this.strata_count--;
      this.update_model();
    },
    
    /** 
     * Validates all HTML inputs in the table.  Currently just tests if they are valid numbers. 
     * @returns {Boolean} True if table validates, false otherwise. 
     */
    validate_inputs: function() {
      var all_inputs_pass = true;
      this.$el.find("input[type=text]").each(function(i, el) {
        if (isNaN(parseFloat(el.value)))  all_inputs_pass = false;
      });
      return all_inputs_pass;
    },

    /**
     * Empty fn here, subclasses should override this and do something useful to save the data into the model.      
     */
    update_model: function() {}, 
            
            
    render: function() {
      var that = this; 
      // We've already got some strata data, so render it in the form.
      // First create an empty form of the correct size, and then populate the input boxes.  
      // This is easier than trying to render the values directly using mustache.
      // this.strata has two formats - 
      //  Estimated soils: Only 1 borehole and a depth and angle for each strata
      //  Measured soils:  With > 1  borehole and just depths for each strata
      if (typeof(this.strata) === "object" && this.strata !== null ) {
        if (this.strata.hasOwnProperty("length") && this.strata.length > 0)  {  // 2 or more boreholes
          _.each(this.strata, function () {that.add_borehole()});  
          _.each(this.strata[0].depth.value, function() {that.add_strata()});  // Strata

          _.each(this.strata, function(strata, borehole_index) { // Populate inputs with values
            that.$el.find("#soil_strata-borehole_distance_"+borehole_index).val(
              strata.surface_distance_from_cut_toe.value
            )
            _.each(strata.depth.value, function(value, strata_index) {
              that.$el.find("#soil_strata-depth_" + borehole_index + "_" + strata_index).val(value);
            });
          });
        } else if (typeof(this.strata.surface_distance_from_cut_toe)==="object") { // 1 borehole
          this.add_borehole();
          that.$el.find("#soil_strata-borehole_distance_0").val(this.strata.surface_distance_from_cut_toe.value);
          _.times(this.strata.interfaces.angle.value.length, function(index) {
            that.add_strata();
            that.$el.find("#soil_strata-depth_0_"+index).val(that.strata.interfaces.depth.value[index]);
            that.$el.find("#soil_strata-angle_0_"+index).val(that.strata.interfaces.angle.value[index]);
          });
        }  
        
      }
      if (this.validate_inputs()) this.buttons.next.enable();
      return this.el;
    },
    
  });

});