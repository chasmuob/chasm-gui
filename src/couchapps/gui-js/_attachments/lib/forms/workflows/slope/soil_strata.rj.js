define(["forms/workflows/slope/_strata.rj"], function(StrataForm) {
  /**
   * Form for users to edit soil strata parameters.  See docs in ./_strata.rj
   * 
   * TODOs:
   *  - Better validation and helpful errors if there are problems
   */
  
  return StrataForm.extend({
    
    /**
     * 
     * @param {Mossaic.models.Geometry} options.model Geometry model that holds the soil strata.
     * @param {object} options.buttons Prev/Next buttons from the parent tab. We disable them if the form is invalid
     *                                  so the user can't move on.
     * @constructor
     */
    initialize: function(options) {
      StrataForm.prototype.initialize.apply(this,[options]); 

      // See docs in _strata.rj for details on below
      this.strata = this.model.get("geometry").soil_strata;
    },

   /**
    * Updates the model from the HTML table.  Similar to ./define/edit.rj, it takes a brutal approach of clearing
    * any existing soil strata, and then readding everything.  This isn't ideal but it's quick and easy for now...
    */
    update_model: function() {
      var that = this;
      if (!this.validate_inputs()) {
        this.buttons.next.attr("disabled", "disabled");
        // TODO - show errors somehow...
        return;
      }
      
      // Clear any existing soil strata
      this.model.clear_soil_strata();
      this.buttons.next.enable();
      
      if (this.borehole_count === 1) {  // We have a depth and an angle
        var surface_distance_from_datum  = 
          that.$el.find("input[data-borehole=0].soil_strata-borehole_distance").val();
        this.model.set_soil_estimate({surface_distance_from_cut_toe: surface_distance_from_datum});
        
        // This is a dirty hack until I've figured out how the hell the geometry model uses the layers/models/
        // collections and all the various functions like add_soil_layer().  Better yet, rewrite the Geometry model
        // entirely....
        _.times(this.strata_count, function() {
          that.model.add_soil_layer({},{changed_by:that});
        });
        
        that.$el.find("input[data-borehole=0].soil_strata-depth_cell").each(function(strata_index,el) {
          that.model.set_soil_estimate({
            field: "depth",
            index: strata_index,
            value: {value:el.value}
          });
        });
        that.$el.find("input[data-borehole=0].soil_strata-angle_cell").each(function(strata_index,el) {
          that.model.set_soil_estimate({
            field: "angle",
            index: strata_index,
            value: {value:el.value}
          });
        })
      } else {    // We have only depths
        var interface_depths;
        var surface_distance_from_datum;
        _.times(this.borehole_count, function(borehole) {
          interface_depths = [];
          that.$el.find("input[data-borehole=" + borehole + "].soil_strata-depth_cell").each(function(index, el) {
            interface_depths.push(el.value);
          })
          surface_distance_from_datum  = 
            that.$el.find("input[data-borehole=" + borehole + "].soil_strata-borehole_distance").val()

          that.model.set_soil_depth(interface_depths, borehole, {
              surface_distance_from_cut_toe: surface_distance_from_datum
          })
        });
      }

    }
  });
}); 