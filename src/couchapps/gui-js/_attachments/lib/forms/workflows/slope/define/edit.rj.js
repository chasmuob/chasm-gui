define(["forms/_basic.rj"], function(BasicForm) {
  /**
   * Form for the user to manually edit their slope parameters.
   *
   * Slope parameters are essentially a right angled triangle, and hence can be defined with 2 of the following:
   *  - Angle (from horizontal)
   *  - Height
   *  - Width
   *  - Length
   *
   * The user can enter any of these, but we need to know which 2 are the independent values from the user, and which
   * 2 we calculate.  Hence the user has to "lock" and "unlock" cells in each row and they can only unlock up to 2
   * cells.
   *
   * TODOs
   *  - Locked/Unlocked cells aren't saved, so if a user moves back in the workflow, it defaults back to height and
   *    width being unlocked.  These are then used if the user hits "next", so we are essentially doing circular
   *    calculations.
   *  - Only show calculations to a specified number of decimal places.  This needs the point above fixing first,
   *    otherwise we would lose accuracy each time the user navigates forward and back.
   *  - This relies heavily on manipulating and querying the DOM.  As with all these forms, a better form/model
   *    binding would massively help.  Really this form should be very lightweight, with the model doing all the
   *    validation.  Currently it's heavily tied to the template ids/classes/names, which makes it brittle.
   *  - Better error handling, where if the user clicks "Set Datum" (a button inserted into the DOM by the workflow tab)
   *    it validates this form and shows any errors, rather than just moving to the next screen.
   *
   */
  return BasicForm.extend({
    template: "forms._workflow._slope._define.edit",

    events: {
      "click #add_slope_section": "add_slope_section_btn",
    },

    /**
     * @param {object} options
     * @param {vis/slope.rj} options.slope_display Slope Graphic
     * @param {Mossiac.models.geometry} options.model Geometry model to save the name into
     * @param {boolean} [options.readonly] Display the form in readonly mode.  Defaults to false
     * @constructor
     */
    initialize: function(options) {
      BasicForm.prototype.initialize.apply(this,[options]);
      this.slope_display = options.slope_display;
      this.model = options.model;
      this.readonly = options.readonly || false;
      this.listenTo(this.model, "change", this.handle_change);
    },

    /**
     * Simple click handler for the add_slope_section_btn
     */
    add_slope_section_btn: function(event){
      this.add_slope_section();
    },
    /**
     * Adds a slope section to the table by adding a new row.  The supplied slope section is just displayed directly
     * in the form.  i.e. there is no validation if it is actually a valid triangle.
     *
     * @param {object} section Slope section object taken directly from the Geometry model, hence with the structure:
     * {
     *     "angle": {
     *         "unit": "deg",
     *         "value": <angle from horizontal>
     *     },
     *     "surface_height": {
     *         "unit": "m",
     *         "value": <vertial height of the slope section>
     *     },
     *     "surface_length": {
     *         "unit": "m",
     *         "value": <surface length>
     *     },
     *     "surface_width": {
     *         "unit": "m",
     *         "value": <horizontal width of the slope section>
     *     }
     *  }
     */
    add_slope_section: function(section) {
      var that = this;
      // var icons = {disabled: "icon-lock", enabled: "icon-lock-open"};
      var icons = {disabled: 'glyphicon-lock', enabled: 'glyphicon-pencil'};
      // Row template
      var $row = $($.Mustache.render(
        "forms._workflow._slope._define._edit.row",
        {
          icons : icons,
          section: section
        }
      ));
      $row.data('section',section);

      // Cell locking and unlocking event handler
      // $row.find("span." + icons.enabled + ", span." + icons.disabled).click(function() {
      $row.find('.lock-toggle').click(function(){
        var $this = $(this);
        var span = $this.find('span');
        var input = $this.siblings('input');

        if (span.attr("class").indexOf(icons.enabled) !== -1) {  // Icon clicked was "enabled"...
          // ...so just disable
          span.attr("class", "glyphicon " + icons.disabled);
          input.attr("disabled", true);
        } else {
          if ($row.find("span." + icons.enabled).length < 2) {   // Count how many other cells in the row are enabled...
            // ...and enable if < 2...
            span.attr("class", "glyphicon " + icons.enabled);
            input.attr("disabled", false);
          } else {
            // ...otherwise flash the ones that are still enabled and one will need disabling first
            $row.find("input:enabled").highlightFlash("#F52C2C", 500);
          }
        }
      });

      // Row removal event handler
      $row.find("button.remove_row").click(function() {
        $row.remove();
        that.update_model();
      });

      // If a user clicks into an input then remove any invalid background classes, as they are probably correcting
      // the input.  If the input is still invalid when they leave, the background will be put back again.
      $row.find("input").mousedown(function() {
        $(this).removeClass("error-background");
      });

      // Input blur event handler.  Calculates dependent triangle params and updates the model
      $row.find("input").blur(function() {
        // If the input that triggered the event is invalid:
        if (isNaN(parseFloat($(this).val()))) {
          $(this).addClass("error-background");
          return;
        }

        var triangle = {};
        // Find the enabled <input>s in the row, and populate an object with them.  If there are exactly 2 values,
        // calculate the dependent values, populate the remaining <input>s and call update_model().
        $row.find("input:enabled").each(function(index, el) {

          // If the enabled <input>s contain valid numbers, add them to the triangle:
          if (!isNaN(parseFloat(el.value) && parseFloat(el.value) > 0)) {
            triangle[el.name] = parseFloat(el.value);
          }
        });
        // And if we exactly 2 params, populate the remaining <input>s and upate the model
        if (_.size(triangle) === 2) {
          triangle = Mossaic.utils.triangle(triangle);
          for (var param in triangle) {
            var $cell = $row.find("input[name="+param+"]");
            $cell.val(triangle[param]);
            $cell.removeClass("error-background");
          }
          that.update_model();
        }
      }); // end of blur event handler

      this.$el.find("table#slope_sections tbody").append($row);
    },

    /**
    * Sets the form to be readonly.  A bit of a sledgehammer approach of just removing/disabling various DOM elements
    */
    set_readonly: function() {
      this.$el.find("i").remove();
      this.$el.find("button").remove();
      this.$el.find("#define_instructions").remove();
      this.$el.find("input").enable(false);
    },

    /**
     * Updates the model with row data.  Takes a sledgehammer approach of clearing the slope in the model, and then
     * inserting everything again.  This plays havoc with any model listeners, so we need to do the change silently and
     * then refresh the slope graphic manually.
     *
     * It's a dirty bodge, but the whole Geometry model (and all the models...) badly need reworking to have clearer
     * APIs.
     *
     * @returns {Boolean}
     */
    update_model: function() {
      var that = this;
      if ((this.$el.find(".error-background")).size() === 0) {
        var sections = [];
        this.$el.find("tr.slope-section").each(function(index, row) {
          var section = {};
          $(row).find("input:enabled").each(function(index,cell) {
            section[cell.name] = parseFloat(cell.value);
          });
          sections.push(section);
        });
        this.model.changed_by = this;
        this.model.clear_sections({silent: true});
        sections.forEach(function(section) {
          that.model.add_section(section,undefined,{silent:true});
        });
        this.model.save();
        that.slope_display.redraw();
        return true;
      } else {
        return false;
      }
    },

    /**
     * Handle changes to the model, such as from the slope graphic (the user can click and drag sections around)
     */
    handle_change: function() {
      if (this.model.changed_by !== this) {
        this.render();
      }
    },

    render: function() {
      var that = this;
      this.$el.mustache(this.template, {}, {method: "html"});
      var slope_sections = this.model.get("geometry").slope_sections;
      if (slope_sections.length > 0) {
        _.each(slope_sections, function(section) {
          that.add_slope_section(section);
        });
      } else {
        this.add_slope_section();
      }
      this.$el.find('tbody').sortable({
        handle: 'span.glyphicon-menu-hamburger',
        cancel: '',
        update: function(event,ui){
          that.update_model();
        }
      });

      if (this.readonly) this.set_readonly();
      return this.el;
    }
  });

});
