define(["forms/workflows/slope/_strata.rj"], function(StrataForm) {
   /**
   * Form for users to edit soil strata parameters.  See docs in ./_strata.rj
   * 
   * TODOs:
   *  - Better validation and helpful errors if there are problems
   *  - Water tables can't be specified with depth & angle.  Needs work on the geometry model to enable.
   */
  return StrataForm.extend({
  
    /**
     * 
     * @param {Mossaic.models.Geometry} options.model Geometry model that holds the water table
     * @param {object} options.buttons Prev/Next buttons from the parent tab. We disable them if the form is invalid
     *                                  so the user can't move on.
     * @constructor
     */
    initialize: function(options) {
      StrataForm.prototype.initialize.apply(this,[options]); 
      if (
        (typeof(this.model.get("geometry").water_table) === "object") &&  // We have a watertable in the model already 
        (this.model.get("geometry").water_table.length > 0)               // so display it in the form
      ) {
        this.strata = this.model.get("geometry").water_table;
        this.strata = _.clone(this.model.get("geometry").water_table);
        _.each(this.strata, function(strata) {
          strata.depth.value = [strata.depth.value];  // To match the "soil strata" object structure, wrap the depth 
                                                      // in an array.  This is only used for rendering the form
                                                      // The object in the model remains unchanged.
        });
      } else {  // We don't have an existing water table, so add the strata and some initial boreholes
        this.add_strata();
        this.add_borehole();
        this.add_borehole();  // Need at least 2 boreholes because currently water table can't have depth & angle
      }
      this.$el.find(".soil_strata-add_strata").remove();  // Water Tables have exactly one strata.  So remove
      this.$el.find(".soil_strata-remove_strata").remove(); // users ability to add/remove strata

    },
    
    /**
     * Remove borehole, but only to a minimum of 2.  Can't go less than that.
     * @returns {undefined}
     */
    remove_borehole: function() {
      if (this.borehole_count > 2) {
        StrataForm.prototype.remove_borehole.apply(this); 
      }
    },
    
  /**
    * Updates the model from the HTML table.  Similar to ./define/edit.rj, it takes a brutal approach of clearing
    * any existing water tables, and then readding everything.  This isn't ideal but it's quick and easy for now...
    */
    update_model: function() {
      var that = this;
      if (!this.validate_inputs()) {
        this.buttons.next.attr("disabled", "disabled");
        // TODO - show errors somehow
        return;
      }

      this.model.clear_water_table();
      this.buttons.next.enable();

      _.times(this.borehole_count, function(borehole_index) {
        var depth = that.$el.find("input[data-borehole=" + borehole_index + "].soil_strata-depth_cell").val();
        var distance = that.$el.find("input[data-borehole=" + borehole_index + "].soil_strata-borehole_distance").val()
        that.model.set_water_depth({value: depth}, {surface_distance_from_cut_toe: distance, index: borehole_index})
      });
      

    },
    
  });
}); 