/**  THIS WAS CALLED SOILS_DATA.JS **
 * @file Define the form for specifying the soils *types* (as opposed to depths)
 */
define(["forms/_basic.rj"],
  function(BasicForm) {
  /**
   * Backbone View for Mossaic.models.Soils which displays values on a form.
   *
   * This View has a reference to layers, a Backbone Collection which
   * is usually bound to a Mossaic.models.Geometry. This is used to determine
   * how many soils to show on the form.
   *
   * @constructor
   * @extends Mossaic.forms.Basic
   */
 return BasicForm.extend({
    /**
     * Initialize the form.
     * Called implicitly with "new Mossaic.forms.SoilsData".
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {Backbone.Collection} collection Collection of objects
     *                                         representing soil strata layers
     * @param {object} options Hash of options
     * @param {Mossaic.models.Soils} options.soils Model to be represented
     *                                             by this form
     * @param {string} options.mode Soils can be rendered in either "chasm" or
     *                              "questa" modes. The questa mode causes an
     *                              extra soil type to be displayed, which will
     *                              be used by questa for the road cells.
     * @param {boolean} options.stochastic Display form elements for editing
     *                                     mean/standard deviation of certain
     *                                     parameters, as opposed to just value.
     * @param {boolean} options.fixed_layers Setting this to false will provide
     *                                       elements to add/remove soil strata
     *                                       interfaces in the collection
     */
    initialize: function(options) {
      _.bindAll(this);
      var that = this;
      this.collection = options.layers;
      this.collection.bind("add", this.handle_change);
      this.collection.bind("change", this.handle_change);
      this.collection.bind("remove", this.handle_change);
      this.collection.bind("reset", this.handle_change);

      this.fixed_layers = options.fixed_layers;

      this.mode = options.mode;
      this.soils = options.soils;

      this.soils.bind("change", this.handle_change);
      if (typeof(options.stochastic) !== "undefined") {
        this.stochastic = options.stochastic;
      } else {
        this.stochastic = this.soils.is_stochastic();
      }

      this.predefined_soils = new Backbone.Collection([], {});
      this.predefined_soils.model = Mossaic.models.map.predefined_soils;
      this.predefined_soils.view = "predefined_soils";
      this.predefined_soils.doreduce = false;

      // Load (XHR) the predefined soils, and populate the "select" dropdowns
      this.predefined_soils.fetch({
        success: that.add_predefined_soils
      });
    },
    /**
     * Set the layers collection to be represented by the form.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {Backbone.Collection} layers Collection of soil strata interfaces
     */
    set_layers: function(layers) {
      this.collection.unbind("add");
      this.collection.unbind("change");
      this.collection.unbind("remove");
      this.collection.unbind("reset");
      this.collection = layers;
      this.collection.bind("add", this.handle_change);
      this.collection.bind("change", this.handle_change);
      this.collection.bind("remove", this.handle_change);
      this.collection.bind("reset", this.handle_change);
    },
    /**
     * Set the Mossaic.forms.SoilsData model to be represented by the form.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {Mossaic.models.Soils} soils Model to be represented
     *                                     by this form
     */
    set_soils: function(soils) {
      if (typeof(this.soils) !== "undefined" &&
          typeof(this.soils.unbind) === "function") {
        this.soils.unbind("change");
      }
      this.soils = soils;
      this.soils.bind("change", this.handle_change);
    },

    /**
     * If form is stochastic, make it deterministic. If it's deterministic,
     * make it stochastic.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     */
    flip_stochastic: function() {
      var that = this;
      this.stochastic = !this.stochastic;
      var to_convert = ["Ksat", "saturated_moisture_content",
          "saturated_bulk_density", "unsaturated_bulk_density",
          "effective_angle_of_internal_friction", "effective_cohesion"];
      var soils = _.clone(this.soils.get("soils"));
      _.each(soils, function(soil_ref, i) {
        var soil = _.clone(soil_ref);
        if (that.stochastic) {
          // For stochastic forms, value becomes mean and standard deviation is
          // 0
          _.each(to_convert, function(field) {
            var parameter = _.clone(soil[field]);
            parameter.mean = parameter.value;
            parameter.standard_deviation = 0;
            delete parameter.value;
            soil[field] = parameter;
          });
        } else {
          // For deterministic forms, mean becomes value and sd is thrown away
          _.each(to_convert, function(field) {
            var parameter = _.clone(soil[field]);
            parameter.value = parameter.mean;
            delete parameter.mean;
            delete parameter.standard_deviation;
            soil[field] = parameter;
          });
        }
        soils[i] = soil;
      });
      this.soils.set({
        soils: soils
      });
      this.render();
    },
    /**
     * Update the model data.
     * The actual field to be updated is defined by the id of the event target.
     * The id format is "FIELDNAME_INDEX-SUBFIELD".
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    update_data: function(event) {
      var target=event.target;
      var id = target.id.split("-")[0].split("_");
      var field = id.slice(0, id.length - 1).join("_");
      if (this.stochastic) {
        var subfield = target.id.split("-")[1];
      } else {
        var subfield = "value";
      }
      var soil_index = parseInt(id[id.length - 1]);

      var value = parseFloat(target.value);
      var soils = _.clone(this.soils.get("soils"));
      var soil = _.clone(soils[soil_index]);
      soil[field] = _.clone(soil[field]);
      soil[field][subfield] = value;
      soils[soil_index] = soil;
      this.soils.changed_by = this;
      this.soils.set({soils: soils});
    },

    update_soil_type: function(event) {
      var target=event.target;
      var id = target.id.split("-")[0].split("_");
      var soil_index = parseInt(id[id.length - 1]);
      var soils = _.clone(this.soils.get("soils"));
      var soil = _.clone(soils[soil_index]);
      soil["soil_type"] = target.value;
      soils[soil_index] = soil;
      this.soils.changed_by = this;
      this.soils.set({soils: soils});

    },

    /**
     * Remove layer of soil measurements from the soil strata layers.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    remove_layer: function(event) {
      var target = $(d3.event.target);
      var id = target.attr("id").split("_");
      var layer = id[id.length - 1];
      var to_remove = this.collection.at(layer);
      this.collection.remove(to_remove);
    },
    /**
     * Add a new soil strata to the soil strata layers.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     * @param {object} options Hash of options to be passed to
     *                         collection.add
     */
    add_new_layer: function(event, options) {
      this.collection.changed_by = this;
      this.collection.add(new Backbone.Model({}), options);
    },
    /**
     * Return the default set of soil attributes, with default suction moisture
     * curve.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @private
     * @return {object} Object representing a default soil definition
     */
    get_blank_soil: function() {
      var blank_soil = {
        soil_type: {
          value: "custom",
          unit: "string"
        },
        Ksat: {
          value: 0,
          unit: "ms-1"
        },
        saturated_moisture_content: {
          unit: "m3m-3",
          value: 0.413
        },
        saturated_bulk_density: {
          unit: "kNm-3",
          value: 20
        },
        unsaturated_bulk_density: {
          unit: "kNm-3",
          value: 18
        },
        effective_angle_of_internal_friction: {
          value: 0,
          unit: "deg"
        },
        effective_cohesion: {
          value: 0,
          unit: "kNm-2"
        },
        suction_moisture_curve: {
          moisture_content: [
            0.1774,
            0.1810,
            0.1863,
            0.1953,
            0.2030,
            0.2158,
            0.2241,
            0.2361,
            0.2559,
            0.2969,
            0.3415,
            0.3777
          ],
          suction: [
            -10,
            -8,
            -6,
            -4,
            -3,
            -2,
            -1.6,
            -1.2,
            -0.8,
            -0.4,
            -0.2,
            -0.1
          ]
        }
      };
      if (this.stochastic) {
        var keys = ["effective_cohesion",
            "effective_angle_of_internal_friction", "Ksat"];
        _.each(keys, function(key) {
          delete blank_soil[key].value;
          blank_soil[key].mean = 0;
          blank_soil[key].standard_deviation = 0;
        });
      }
      return blank_soil;
    },

    /**
     * Add soils if we have more layers than soil types
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @private
     * @params {object} options Hash of options
     * @params {integer} options.cols Number of columns to be built for this row
     */
    maybe_add_soils: function(options) {
      var options = options || {};
      var cols = options.cols;
      var soils_data = this.soils.get("soils");
      if (typeof(soils_data) === "undefined") {
        soils_data = [];
      }
      var number_of_layers = cols;
      // this.collection is geometry.layers, passed in from Router
      // model
      var layer_with_highest_index = this.collection.max(function(model) {
        return model.get("soil_index");
      });
      var highest_index = (layer_with_highest_index !== -Infinity
              && layer_with_highest_index.get("soil_index")) || 0;
      var soils_required = ((number_of_layers > highest_index + 1) &&
          number_of_layers) || highest_index + 1;
      var soils_to_add = soils_required - soils_data.length;
      if (soils_to_add > 0) {
        for (var i = soils_to_add; i > 0; --i) {
          soils_data.push(this.get_blank_soil());
        }
        this.soils.set({soils: soils_data}, {silent: true});
      }
    },
    /**
     * Handle change events triggered by the model. Only redraw if the
     * model was changed by a different view. Will also trigger redraw if the
     * changed object is a collection, in which case the number of layers has
     * changed, so a redraw is required.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {object} model Backbone.Model or Backbone.Collection that has
     *                       triggered the change
     */
    handle_change: function(model) {
      this.stochastic = this.soils.is_stochastic();
      if (typeof(model) !== "undefined" &&
          (model.changed_by != this || "collection" in model)) {
        this.render();
      }
    },

    /**
     * Adds entries in this.predefined_soils to the HTML "select" lists.
     * @returns void
     */
    add_predefined_soils: function(options) {
      var that = this;
      var $select = this.$el.find("select.predefined_soil_select");
      $select.append("<option value='custom'>Custom</option>");

      if (this.predefined_soils.length > 0) {
        _.each(this.predefined_soils.models, function(model) {
          $select.append($("<option></option").val(model.id).html(model.get("name")));
        });
      }

      _.each(this.soils.get("soils"), function(soil, index) {
        that.$el.find("#soil_type_"+index).val(soil.soil_type);
      });

    },

    /**
     * Render the form.
     * Will be called implicitly by show or handle_change so should not need
     * to be called directly.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     */
    render: function() {
      var that = this;
      var soil_strata = this.model.get("geometry").soil_strata;
      var number_of_soils = 1;

      if(soil_strata){
        number_of_soils = this.model.get("geometry").soil_strata[0].depth.value.length + 1;
        if (this.mode === "questa") {
          ++number_of_soils;
        }
      }

      // If needed, populate this.soils
      this.maybe_add_soils({cols: number_of_soils});

      // Main scaffolding template
      this.$el.mustache("forms._workflow._slope.soil_types",{}, {method:"html"});

      this.$el.find("#soil_form_stochastic")
        .change(function(){
          that.flip_stochastic();
        })
        .attr("checked", this.stochastic && "checked" || null);

      // Render table header
      if (this.stochastic) {
        this.$el.find("thead").mustache("forms._workflow._slope._soil_types.stochastic_header");
      } else {
        this.$el.find("thead").mustache("forms._workflow._slope._soil_types.deterministic_header");
      }

      // Render soil rows.  We can't loop within the template because we need the rownum.
      var $row;
      for (var rownum=0; rownum < number_of_soils; rownum++) {
        if (this.stochastic) {
          $row = $.Mustache.render(
            "forms._workflow._slope._soil_types.stochastic_row",
            {rownum:rownum, soil:this.soils.get("soils")[rownum]}
          );
        } else {
          var soil = this.soils.get("soils")[rownum];
          var value = soil.Ksat.value;
          var exp = value === 0 ? 0 : value.toExponential();
          $row = $.Mustache.render(
            "forms._workflow._slope._soil_types.deterministic_row",
            {rownum:rownum,
             soil:soil,
             exp:exp}
          );
        }
        this.$el.find("tbody").append($row);
      }

      // Bind the change handler to the table inputs, so any changes to the HTML form update the model
      this.$el.find("table input").change(that.update_data);
      this.$el.find("table select").change(that.update_soil_type);

      // Set the "soil_type" to custom if they change any inputs
      this.$el.find("table input").on("keyup", function(event) {
        var target=event.target;
        var id = target.id.split("-")[0].split("_");
        var soil_index = parseInt(id[id.length - 1]);
        that.$el.find("#soil_type_" + soil_index).val("custom");
      });

      // Register change handler so the predefined soils <select> updates the table <input>s
      this.$el.find("select").change(function(event) {
        if (event.target.value == 'custom') {
          var soil_num = event.target.id.split("_").pop();
          that.$el.find(".row_"+soil_num +" input").val("0");
        } else {
          var soil_num = event.target.id.split("_").pop();
          var model = that.predefined_soils.get(event.target.value);
          _.each(model.get("params"),function(value,key) {
            that.$el.find("#" + key + "_" + soil_num).val(model.get("params")[key].value).trigger('change');
          });
        }
      });

      // If the soils are loaded (cached), then add them into the DOM <option> elements.
      // If they are still loading, this will be done on the XHR success callback instead.
      if (this.predefined_soils.length > 0) {
        this.add_predefined_soils();
      }
      return this.el;

    }
  });
});
