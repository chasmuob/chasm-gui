define(["forms/_basic.rj"], function(BasicForm) {
/**
   * Backbone View for Mossaic.models.Stability which displays values on a
   * form.
   *
   * This is a legacy view which builds the form using d3.js rather than the
   * more sensible jQuery/templating approach.
   * @constructor
   * @extends Mossaic.forms.Basic
   */
  return  BasicForm.extend({
    /**
     * Initialize the form.
     * Called implicitly with "new Mossaic.forms.Stability".
     * @memberof Mossaic.forms.Stability
     * @instance
     * @param {Mossaic.models.Stability} model Stability model to be represented
     *                                         by the form
     * @param {object} options Hash of options
     * @param {string} options.divname jQuery selector for div to which this
     *                                 form should be appended
     */
    initialize: function(options) {
      BasicForm.prototype.initialize.apply(this, [options]);
      _.bindAll(this);
      this.stability_display = options.stability_display;
      this.model = options.model;
      this.display_visible = false;
      this.radius_display_visible = false;
    },

    /**
     * Update the grid search parameters.
     * The key(s) defining the field to be updated are obtained from the id
     * of the event target. The Stability Model takes care of adding the
     * attributes to the grid_search_parameters sub-field.
     * @memberof Mossaic.forms.Stability
     * @instance
     */
    update_gsp: function() {
      var target = $(d3.event.target);
      var value = parseFloat(target.attr("value"));
      var tokens = target.attr("id").split("_");
      var sub_field = tokens[tokens.length - 1];
      var field = tokens.slice(0, tokens.length - 1).join("_");
      var new_attrs = {};
      new_attrs[field] = _.clone(this.model.get(field));
      new_attrs[field][sub_field] = value;
      this.model.changed_by = this;
      this.model.set(new_attrs);
    },
    /**
     * Add a row to the stability table
     * Convenience function for building form
     * @memberof Mossaic.forms.Stability
     * @instance
     * @private
     * @param {d3.selection} table d3 selection from which the elements should
     *                             be selected
     * @param {object} options Hash of options
     * @param {string} options.name Name of the row, added as a label
     * @param {string} options.id Id prefix to be used for row elements
     * @param {float} options.x Value for the x column
     * @param {float} options.y Value for the y column
     */
    add_row: function(table, options) {
      var tr = table.append("xhtml:tr");
      tr.append("xhtml:th")
        .html(options.name);
      tr.append("xhtml:td").append("xhtml:input")
        .attr("id", options.id + "_x")
        .attr("class","form-control")
        .attr("value", options.x)
        .on("change", this.update_gsp);
      tr.append("xhtml:td").append("xhtml:input")
        .attr("id", options.id + "_y")
        .attr("class","form-control")
        .attr("value", options.y)
        .on("change", this.update_gsp);
    },
    /**
     * Handle change events triggered by the model. Only redraw if the
     * model was changed by a different view.
     * @memberof Mossaic.forms.Stability
     * @instance
     */
    handle_change: function() {
      // Change may have originated from this view or somewhere else. So check
      // called_by attribute.
      if (this.model.changed_by != this) {
        this.render();
      }
    },
    /**
     * Render the form.
     * Will be called implicitly by show or handle_change so should not need
     * to be called directly.
     * @memberof Mossaic.forms.Stability
     * @instance
     */
    render: function() {
      if (this.hidden) {
        return false;
      }

      var that = this;
      this.$el.html("");

      // Checkboxes
      var form = d3.select(this.el).append("xhtml:div");
      form.attr("class","checkbox");

      var label = form.append("xhtml:label")
        .attr("for", "show_stability_display");

      var show_radius_display_checkbox;
      var show_display_checkbox = label.append("xhtml:input")
        .attr("type", "checkbox")
        .attr("id", "show_stability_display")
        .on("change", function(event) {
          that.display_visible = $(d3.event.target).attr("checked");
          if ($(d3.event.target).attr("checked")) {
             that.stability_display.show();
             show_radius_display_checkbox.attr("disabled",null);
          } else {
            that.stability_display.hide();
            show_radius_display_checkbox.attr("disabled", true);
          }
        });
      if(this.display_visible){
        label.attr('checked',true);
      }
      label.append('xhtml:span').html('Show stability grid');

      if (this.display_visible) {
        show_display_checkbox.attr("checked", true);
      }

      form = d3.select(this.el).append("xhtml:div");
      form.attr("class","checkbox");

      label = form.append("xhtml:label")
        .attr("for", "show_stability_radius");

      show_radius_display_checkbox = label.append("xhtml:input")
        .attr("type", "checkbox")
        .attr("id", "show_stability_radius")
        .on("change", function(event) {
          if ($(d3.event.target).attr("checked")) {
            that.stability_display.show_radius();
          } else {
             that.stability_display.hide_radius();
          }
        });
      // form.append("xhtml:label")
      //   .attr("for", "show_stability_radius")
      //   .html("Show stability search radius");
      label.append('xhtml:span').html('Show stability search radius');

      if (this.radius_display_visible) {
        show_radius_display_checkbox.attr("checked", true);
      }
      if (!this.display_visible) {
        show_radius_display_checkbox.attr("disabled", true);
      }


      // form.append("xhtml:button")
      d3.select(this.el).append("xhtml:button")
        .attr("class", "add")
        .html("Auto-set stability grid")
        .on("click", function() {
          that.model.auto_set_values({}, {
              mesh: that.model.geometry.get_mesh()
          });
        });

      form = d3.select(this.el).append("xhtml:div");
      var grid_div = form.append("xhtml:div").attr("id", "stability_grid");
      var rad_div = form.append("xhtml:div").attr("id", "stability_radius");
      var table = grid_div.append("xhtml:table");
      var tr = table.append("xhtml:tr");
      tr.append("xhtml:th").style("width", 80);
      tr.append("xhtml:th")
        .html("x");
      tr.append("xhtml:th")
        .html("y");
      var origin = this.model.get("origin") || {};
      this.add_row(table, {
        name: "Origin",
        id: "origin",
        x: origin.x,
        y: origin.y
      });
      var spacing = this.model.get("spacing") || {};
      this.add_row(table, {
        name: "Spacing",
        id: "spacing",
        x: spacing.x,
        y: spacing.y
      });
      var grid_size = this.model.get("grid_size") || {};
      this.add_row(table, {
        name: "Grid size",
        id: "grid_size",
        x: grid_size.x,
        y: grid_size.y
      });
      table = rad_div.append("xhtml:table");
      tr = table.append("xhtml:tr");
      tr.append("xhtml:th").style("width", 80);
      tr.append("xhtml:th")
        .html("Initial");
      tr.append("xhtml:th")
        .html("Increment");
      tr = table.append("xhtml:tr");
      tr.append("xhtml:th")
        .html("Radius");
      var radius = this.model.get("radius") || {};
      tr.append("xhtml:td").append("input")
        .attr("id", "radius_initial")
        .attr("class","form-control")
        .attr("value", radius.initial)
        .on("change", this.update_gsp);
      tr.append("xhtml:td").append("input")
        .attr("id", "radius_increment")
        .attr("class","form-control")
        .attr("value", radius.increment)
        .on("change", this.update_gsp);

        return this.el;
    }
  });
});
