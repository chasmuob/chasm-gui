/**
  * Backbone View from which many Mossaic form views inherit
  * @constructor
  * @extends Backbone.View
  */
 Mossaic.forms.Basic = Backbone.View.extend({
   /**
    * @memberof Mossaic.forms.Basic
    * @instance
    * @param {object} model Backbone.Model object to be represented
    *                       by this form
    * @param {object} options Hash of options, used by child forms
    */
   initialize: function(model, options) {
     _.bindAll(this);
     /**
      * The model to be represented by this form.
      * @type Backbone.Model
      */
     this.model = model;
     this.model.bind('change', this.handle_change);
   },
   /**
    * Set the model to be represented by the form.
    * @memberof Mossaic.forms.Basic
    * @instance
    * @param {Mossaic.models.Model} model Model to be represented
    *                                     by this form
    */
   set_model: function(model) {
     if (typeof(this.model) !== "undefined" &&
         typeof(this.model.unbind) === "function") {
       this.model.unbind("change");
     }
     this.model = model;
     this.model.bind("change", this.handle_change);
   },
   /**
    * Hide the form by removing it from the DOM
    * @memberof Mossaic.forms.Basic
    * @instance
    */
   hide: function() {
     d3.select(this.form_id).remove();
     this.hidden = true;
   },
   /**
    * Show the form
    * @memberof Mossaic.forms.Basic
    * @instance
    */
   show: function() {
     this.hidden = false;
     this.render();
     $(this.form_id + " input:enabled:first").focus();
   },
   /**
    * Handle changes to the collection represented by this form
    * @memberof Mossaic.forms.Basic
    * @instance
    */
   handle_change: function() {
     // Override for custom change handling
     this.render();
   },
   /**
    * Render the form. Should be triggered either by Mossaic.forms.Basic.show
    * or Mossaic.forms.Basic.handle_change.
    * @memberof Mossaic.forms.Basic
    * @instance
    */
   render: function() {
     // Form rendering code goes here
   }
 });