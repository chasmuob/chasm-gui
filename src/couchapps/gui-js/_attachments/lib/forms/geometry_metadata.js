/**
 * @file Define the form for displaying Mossaic.models.Geometry data.
 *
 *       Note that importing this script adds the function that builds the form
 *       to the Mossaic.forms.deferred array. The actual form class will be
 *       created when triggered by the Mossaic.Router. This is required because
 *       the form is dependent upon a template stored in the CouchDB design doc.
 */
(function() {
  if (typeof(Mossaic.forms.deferred) === "undefined") {
    Mossaic.forms.deferred = [];
  }
  Mossaic.forms.deferred = Mossaic.forms.deferred.concat([
    function () {
      /**
       * Backbone View for geometry metadata, a subset of the data stored in a
       * Mossaic.models.Geometry, which displays values on a form.
       * Displays the executable for a given geometry, and the number of slope
       * sections. The format of the number of slope sections display depends
       * on the selected executable type. For CHASM we show the total number of
       * slope sections. For QUESTA, where the datum is the road, we show the
       * number of sections upslope and downslope of the road.
       * @constructor
       * @extends Backbone.View
       */
      Mossaic.forms.GeometryMetadata = Backbone.View.extend({
        /**
         * Initialize the form.
         * Called implicitly with "new Mossaic.forms.GeometryMetadata".
         * @memberof Mossaic.forms.GeometryMetadata
         * @instance
         * @param {Mossaic.models.Geometry} model Model to be represented
         *                                        by this form
         * @param {object} options Hash of options
         * @param {string} options.divname jQuery selector for div to which this
         *                                 form should be appended
         */
        initialize: function(model, options) {
          _.bindAll(this);
          this.hidden = true;
          this.model = model;
          this.divname = options.divname || "#slope_workflow";
          this.form_id = this.divname + "_geometry_metadata_form";
          this.upslope_sections_id = "upslope_sections";
          this.downslope_sections_id = "downslope_sections";

          this.default_executable = "chasm";
          this.executable = this.default_executable;
        },
        /**
         * Template used to create the form
         * @memberof Mossaic.forms.GeometryMetadata
         * @instance
         * @type string
         */
        template: "forms.geometry_metadata",
        /**
         * Set the geometry model to be represented by the form.
         * @memberof Mossaic.forms.GeometryMetadata
         * @instance
         * @param {Mossaic.models.Geometry} model Model to be represented
         *                                        by this form
         */
        set_geometry: function(geometry) {
          if (typeof(this.model) !== "undefined" &&
              typeof(this.model.unbind) === "function") {
            this.model.unbind("change");
          }
          this.model = geometry;
          this.model.bind("change", this.handle_change);
          this.handle_change();
        },
        /**
         * Hide the form by removing it from the DOM
         * @memberof Mossaic.forms.GeometryMetadata
         * @instance
         */
        hide: function() {
          $(this.form_id).remove();
          this.hidden = true;
        },
        /**
         * Show the form
         * @memberof Mossaic.forms.GeometryMetadata
         * @instance
         */
        show: function() {
          this.hidden = false;
          this.render();
        },
        /**
         * Handle change events triggered by the model. Only redraw if the
         * model was changed by a different view. This method also ensures
         * that the default number of sections held be the view are appropriate
         * for the mode (chasm/questa) of the Mossaic.models.Geometry model.
         * @memberof Mossaic.forms.GeometryMetadata
         * @instance
         */
        handle_change: function() {
          if (typeof(this.model) !== "undefined") {
            var geometry = this.model.get("geometry");
            if (typeof(geometry) !== "undefined" &&
                typeof(geometry.slope_sections) !== "undefined" &&
                geometry.slope_sections.length > 0) {
              if (this.model.mode === "questa") {
                var road_section = geometry.road.slope_section;
                if (typeof(road_section) !== "undefined") {
                  this.upslope_sections = road_section;
                  this.downslope_sections = geometry.slope_sections.length - (road_section + 1);
                }
              }
            }
            this.executable = this.model.mode;
          } else {
            this.executable = this.executable || this.default_executable;
          }
          this.render();
        },
        /**
         * Helper method called by render() which takes care of applying the
         * template.
         * @memberof Mossaic.forms.GeometryMetadata
         * @instance
         */
        render_form: function(form) {
          var mustache_opts = {};
          if (this.executable === "questa") {
            mustache_opts.upslope_sections = {
              id: this.upslope_sections_id,
              value: this.upslope_sections
            };
            mustache_opts.downslope_sections = {
              id: this.downslope_sections_id,
              value: this.downslope_sections
            };
          }
          $(this.form_id).mustache(this.template, mustache_opts,{method:"html"});
        },
        /**
         * Render the form.
         * Will be called implicitly by show or handle_change so should not need
         * to be called directly.
         * @memberof Mossaic.forms.GeometryMetadata
         * @instance
         */
        render: function() {
          var that = this;
          if (this.hidden) {
            return;
          }
          $(this.form_id).remove();
          $(this.divname).html('<div id="' + this.form_id.slice(1) + '"></div>');

          this.render_form();

          $(this.divname + " input:enabled:first").focus();
          $("#" + this.upslope_sections_id).change(function(event) {
            var new_value = parseInt(event.target.value, 10);
            if (!Mossaic.utils.validate_number(new_value,
                "Please enter a positive integer for the number of upslope " +
                  "sections",
                event.target,
                [function(val) { return val > 0; }])) {
              return;
            }
            that.upslope_sections = new_value;
            that.model.clear();
          });
          $("#" + this.downslope_sections_id).change(function(event) {
            var new_value = parseInt(event.target.value, 10);
            if (!Mossaic.utils.validate_number(new_value,
                "Please enter a positive integer for the number of downslope " +
                  "sections",
                event.target,
                [function(val) { return val > 0; }])) {
              return;
            }
            that.downslope_sections = new_value;
            that.model.clear();
          });
          $(this.divname + " :radio").change(function(event) {
            that.executable = event.target.value;
            that.model.mode = that.executable;
            that.model.clear();
            that.render();
          });
          _.each($(this.divname + " :radio"), function(element) {
            var value_to_set = that.executable || that.default_executable;
            if (element.value === value_to_set) {
              $(element).prop("checked", true);
            }
          });
        }
      });
    }
  ]);
})();