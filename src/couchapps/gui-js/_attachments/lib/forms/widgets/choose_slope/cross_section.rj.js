define(["forms/_basic.rj"], function(BasicForm) {
/**
 * Very simple form for displaying (not editing) Cross Section data in the "Choose Slope" widget. 
 */    
  return BasicForm.extend({

    template: "forms._widgets._choose_slope.cross_section",
            
    /**
     * Render the form.
     * Will be called implicitly by show or handle_change so should not need
     * to be called directly.
     */
    render: function() {
      // We only show comments at the moment
      this.$el.mustache(this.template, {comments : this.model.get("comments")},{method:"html"});
      return this.el;
    }

  })
});