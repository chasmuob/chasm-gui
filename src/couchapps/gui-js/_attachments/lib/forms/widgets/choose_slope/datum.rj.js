define(["forms/_basic.rj"], function(BasicForm) {
  /**
   * Very simple form for displaying (not editing) Cross Section data in the "Choose Slope" widget. 
   */  
  return BasicForm.extend({
    
    template: "forms._widgets._choose_slope.datum",

    /**
     * Render the form into this.el and return it.
     * @instance
     */
    render: function() {
      // Only show comments at the moment
      this.$el.mustache(this.template, {comments : this.model.get("comments")},{method:"html"});
      return this.el;
    }
  });
});