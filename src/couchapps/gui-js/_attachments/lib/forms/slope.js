define(["workflows/slope/define/_basic.rj"], function(BasicDefineSlopeWorkflow) {

  return BasicDefineSlopeWorkflow.extend({
    template: "forms.workflow._slope._define.edit",
  
    events: {
      "click #add_slope_section": "add_slope_section",
    },
    
    initialize: function(options) {
      BasicDefineSlopeWorkflow.prototype.initialize.apply(this,[options]);
      this.slope_display = options.slope_display;
      this.moveon = options.next || {};
      this.listenTo(this.model, "change", this.handle_change);
    },
    
    add_slope_section: function(section) {
      var that = this;
      var icons = {disabled: "icon-remove", enabled: "icon-ok"};
      var $row = $($.Mustache.render(
        "forms.workflow._slope._define._edit.row", 
        { 
          icons : icons,
          section: section
        }
      ));

      $row.find("i." + icons.enabled + ", i." + icons.disabled).click(function() {
        var $this = $(this);
        if ($this.attr("class").indexOf(icons.enabled) !== -1) {  // Icon clicked was "enabled"...
          // ...so just disable
          $this.attr("class", icons.disabled);
          $this.siblings("input").attr("disabled", true);
        } else {
          if ($row.find("i." + icons.enabled).length < 2) {   // Count how many other cells in the row are enabled...
            // ...and enable if < 2...
            $this.attr("class", icons.enabled);
            $this.siblings("input").attr("disabled", false);
          } else {
            // ...otherwise flash the ones that are still enabled and one will need disabling first
            $row.find("input:enabled").highlightFlash("#F52C2C", 500);
          }
        }
      });

      $row.find("i.icon-trash").click(function() {
        $row.remove();
      });


      $row.find("input").mousedown(function() {
        $(this).removeClass("error-background");
      });

      $row.find("input").blur(function() {
        if (isNaN(parseFloat($(this).val()))) {
          $(this).addClass("error-background");
          return;
        }
        var triangle = {};
        _.each($row.find("input:enabled"), function(el) {
          if ($(el).val().length > 0) {
            triangle[$(el).data("param")] = $(el).val();
          }
        });
        if (_.size(triangle) === 2) {
          triangle = Mossaic.utils.triangle(triangle);
          // _.each() doesn't work here, it doesn't like having a parameter called "length".
          for (var param in triangle) {
            var $cell = $row.find(".slope-" + param);
            $cell.val(triangle[param]);
            $cell.removeClass("error-background");
          }
          that.update_model();
        }
      });

      this.$el.find("table#slope_sections tbody").append($row);
    },
    
    update_model: function() {
      var that = this;
      if ((this.$el.find(".error-background")).size() === 0) {
        var sections = [];
        this.$el.find("tr.slope-section").each(function(index, row) {
          var section = {};
          $(row).find("input:enabled").each(function(index,cell) {
            section[cell.dataset.param] = parseFloat(cell.value)
          });
          sections.push(section);
        });
        this.model.clear_sections();
        sections.forEach(function(section) {
          that.model.add_section(section);
        })
        that.slope_display.redraw();
        this.slope_display.set_mode("edit_surface");
        return true;
      } else {
        return false;
      }
    },
    
    next: function() {
      if (this.update_model()) this.moveon();
    },

    
    handle_change: function() {
      if (this.model.changed_by !== this) {
        this.render();
      }
    },
            
    render: function() {
      var that = this;
      this.$el.mustache(this.template, {}, {method: "html"});
      var slope_sections = this.model.get("geometry").slope_sections;
      if (slope_sections.length > 0) {
        _.each(slope_sections, function(section) {
          that.add_slope_section(section);
        });
      } else {
        this.add_slope_section();
      }
      return this.el;
    }
  });

});