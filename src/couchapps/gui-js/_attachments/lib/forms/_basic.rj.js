define(["views/_basic.rj"], function(BasicView) {
/**
  * Backbone View from which forms inherit
  * 
  * Forms are a specialised view that are backed by, and instantiated with, a Backbone.model.  They then  
  * listenTo() changes on this model and update themselves accordingly, generally by calling render() again.
  * 
  * The idea is the form is the GUI/DOM representation of the model and they have a 1:1 relationship where any form
  * changes are automatically pushed into the model, and any model changes (possibly from outside this form), are
  * updated onscreen for the user.  This could however be vastly improved.  At the moment all the mapping code is 
  * written manually.  Some Model/Form binding library would be useful - Backbone.ModelBinder is good for basic 
  * bindings, but (as of v0.1.6) doesn't handle arrays and dynamic forms (e.g. adding rows) sufficiently well.
  * 
  * Subclasses/forms don't always confirm 100% to this ideal.  Not all subclasses implement immediate form->model 
  * binding - some only update the model when a save() button or similar is clicked.  And for pragmatism some subclasses 
  * do a bit more than just mapping to a model.  See subclass docs for more details
  * 
  */
  return BasicView.extend({
   
    /** 
     * Constructor
     * @param {object} options Hash of options
     * @param {object} options.model Backbone.Model object to be represented by this form
     */
    initialize: function(options) {
      BasicView.prototype.initialize.apply(this,[options]);
      /**
       * The model to be represented by this form.
       * @type Backbone.Model
       */
      this.model = options.model;
      this.listenTo(this.model, "change", this.handle_change);
    },
    
    /**
     * Set the model to be represented by the form.
     * @param {Backbone.Model} model Model to be represented by this form
     */
    set_model: function(model) {
      this.stopListening(this.model);
      this.model = model;
      this.listenTo(model, "change", this.handle_change);
      this.handle_change();
    },
            
    /**
     * Handle changes to the collection represented by this form
     */
    handle_change: function() {
      // Override for custom change handling
      this.render();
    },
  });
 });