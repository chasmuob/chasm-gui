/**
 * @file Define the form for displaying mesh data for a
 *       Mossaic.models.BoundaryConditions model.
 */
(function() {
  
  /**
   * Data structure for calculating volume for a given annual return period
   * This data is specific to a given geo-location, so at some point this should
   * be replaced with a query to the DB.
   */
  
  var volumes_map = [
  // <editor-fold defaultstate="collapsed">
  {
      duration: 1,
      magnitude: "1 in 5",
      volume: 60.96
  },
  {
      duration: 1,
      magnitude: "1 in 10",
      volume: 76
  },
  {
      duration: 1,
      magnitude: "1 in 20",
      volume: 90
  },
  {
      duration: 1,
      magnitude: "1 in 50",
      volume: 105
  },
  {
      duration: 1,
      magnitude: "1 in 100",
      volume: 110
  },
  {
      duration: 1,
      magnitude: "1 in 200",
      volume: 125
  },
  {
      duration: 1,
      magnitude: "1 in 500",
      volume: 140
  },
  {
      duration: 2,
      magnitude: "1 in 5",
      volume: 81.28
  },
  {
      duration: 2,
      magnitude: "1 in 10",
      volume: 100
  },
  {
      duration: 2,
      magnitude: "1 in 20",
      volume: 116
  },
  {
      duration: 2,
      magnitude: "1 in 50",
      volume: 140
  },
  {
      duration: 2,
      magnitude: "1 in 100",
      volume: 160
  },
  {
      duration: 2,
      magnitude: "1 in 200",
      volume: 170
  },
  {
      duration: 2,
      magnitude: "1 in 500",
      volume: 200
  },
  {
      duration: 3,
      magnitude: "1 in 5",
      volume: 95.25
  },
  {
      duration: 3,
      magnitude: "1 in 10",
      volume: 114
  },
  {
      duration: 3,
      magnitude: "1 in 20",
      volume: 132
  },
  {
      duration: 3,
      magnitude: "1 in 50",
      volume: 156
  },
  {
      duration: 3,
      magnitude: "1 in 100",
      volume: 180
  },
  {
      duration: 3,
      magnitude: "1 in 200",
      volume: 195
  },
  {
      duration: 3,
      magnitude: "1 in 500",
      volume: 216
  },
  {
      duration: 4,
      magnitude: "1 in 5",
      volume: 106.68
  },
  {
      duration: 4,
      magnitude: "1 in 10",
      volume: 124
  },
  {
      duration: 4,
      magnitude: "1 in 20",
      volume: 148
  },
  {
      duration: 4,
      magnitude: "1 in 50",
      volume: 172
  },
  {
      duration: 4,
      magnitude: "1 in 100",
      volume: 200
  },
  {
      duration: 4,
      magnitude: "1 in 200",
      volume: 224
  },
  {
      duration: 4,
      magnitude: "1 in 500",
      volume: 248
  },
  {
      duration: 5,
      magnitude: "1 in 5",
      volume: 114.3
  },
  {
      duration: 5,
      magnitude: "1 in 10",
      volume: 135
  },
  {
      duration: 5,
      magnitude: "1 in 20",
      volume: 160
  },
  {
      duration: 5,
      magnitude: "1 in 50",
      volume: 195
  },
  {
      duration: 5,
      magnitude: "1 in 100",
      volume: 210
  },
  {
      duration: 5,
      magnitude: "1 in 200",
      volume: 240
  },
  {
      duration: 5,
      magnitude: "1 in 500",
      volume: 270
  },
  {
      duration: 6,
      magnitude: "1 in 5",
      volume: 121.86
  },
  {
      duration: 6,
      magnitude: "1 in 10",
      volume: 138
  },
  {
      duration: 6,
      magnitude: "1 in 20",
      volume: 174
  },
  {
      duration: 6,
      magnitude: "1 in 50",
      volume: 198
  },
  {
      duration: 6,
      magnitude: "1 in 100",
      volume: 234
  },
  {
      duration: 6,
      magnitude: "1 in 200",
      volume: 252
  },
  {
      duration: 6,
      magnitude: "1 in 500",
      volume: 294
  },
  {
      duration: 10,
      magnitude: "1 in 5",
      volume: 152.4
  },
  {
      duration: 10,
      magnitude: "1 in 10",
      volume: 170
  },
  {
      duration: 10,
      magnitude: "1 in 20",
      volume: 200
  },
  {
      duration: 10,
      magnitude: "1 in 50",
      volume: 220
  },
  {
      duration: 10,
      magnitude: "1 in 100",
      volume: 250
  },
  {
      duration: 10,
      magnitude: "1 in 200",
      volume: 300
  },
  {
      duration: 10,
      magnitude: "1 in 500",
      volume: 340
  },
  {
      duration: 12,
      magnitude: "1 in 5",
      volume: 152.4
  },
  {
      duration: 12,
      magnitude: "1 in 10",
      volume: 168
  },
  {
      duration: 12,
      magnitude: "1 in 20",
      volume: 216
  },
  {
      duration: 12,
      magnitude: "1 in 50",
      volume: 246
  },
  {
      duration: 12,
      magnitude: "1 in 100",
      volume: 264
  },
  {
      duration: 12,
      magnitude: "1 in 200",
      volume: 312
  },
  {
      duration: 12,
      magnitude: "1 in 500",
      volume: 360
  },
  {
      duration: 24,
      magnitude: "1 in 5",
      volume: 182.88
  },
  {
      duration: 24,
      magnitude: "1 in 10",
      volume: 216
  },
  {
      duration: 24,
      magnitude: "1 in 20",
      volume: 252
  },
  {
      duration: 24,
      magnitude: "1 in 50",
      volume: 288
  },
  {
      duration: 24,
      magnitude: "1 in 100",
      volume: 336
  },
  {
      duration: 24,
      magnitude: "1 in 200",
      volume: 360
  },
  {
      duration: 24,
      magnitude: "1 in 500",
      volume: 432
  }
// </editor-fold>
  ];


  
  /**
   * Backbone View for Mossaic.models.BoundaryConditions which displays values
   * on a form.
   *
   * This is a legacy view which builds the form using d3.js rather than the
   * more sensible jQuery/templating approach.
   * @constructor
   * @extends Mossaic.forms.Basic
   */
  Mossaic.forms.Rainfall = Mossaic.forms.Basic.extend({
    /**
     * Preset storm magnitudes
     * @memberof Mossaic.forms.Rainfall
     * @instance
     * @private
     * @type array
     */
    magnitudes: [
      "1 in 5",
      "1 in 10",
      "1 in 20",
      "1 in 50",
      "1 in 100",
      "1 in 200",
      "1 in 500"
    ],
    /**
     * Preset storm durations
     * @memberof Mossaic.forms.Rainfall
     * @instance
     * @private
     * @type array
     */
    durations: [1, 2, 3, 4, 5, 6, 10, 12, 24],
    /**
     * Initialize the form.
     * Called implicitly with "new Mossaic.forms.Rainfall".
     * @memberof Mossaic.forms.Rainfall
     * @instance
     * @param {Mossaic.models.BoundaryContitions} model Model to be represented
     *                                                  by this form
     * @param {object} options Hash of options
     * @param {string} options.divname jQuery selector for div to which this
     *                                 form should be appended
     */
    initialize: function(model, options) {
      Mossaic.forms.Basic.prototype.initialize.apply(this, [model, options]);
      this.divname = options.divname || "#rainfall_form";
      this.form_id = this.divname + "_rainfall";
      this.stochastic = this.model.is_stochastic();
    },
    /**
     * Set the application that will consume this input data and update the
     * model appropriately. If CHASM is the application, probability values will
     * be removed.
     * @memberof Mossaic.forms.Rainfall
     * @instance
     * @param {string} application The name of the application (chasm or questa)
     */
    set_application: function(application) {
      if (this.application !== application) {
        this.application = application;
        // If we are a chasm application, strip out any probability values that
        // might exist, as they are not valid for chasm.
        if (this.application === "chasm") {
          var storm = _.clone(this.model.get("storm"));
          _.each(storm, function(instance_ref, i) {
            var instance = _.clone(instance_ref);
            delete instance.probability;
            storm[i] = instance;
          });
          this.model.set({storm: storm});
        }
        this.render();
      }
    },
    /**
     * If form is stochastic, make it deterministic. If it's deterministic,
     * make it stochastic.
     * The fields that can be either deterministic or stochastic are
     * detention_capacity and soil_evaporation.
     * @memberof Mossaic.forms.Rainfall
     * @instance
     */
    flip_stochastic: function() {
      this.stochastic = !this.stochastic;
      var detention_capacity = _.clone(this.model.get("detention_capacity"));
      var soil_evaporation = _.clone(this.model.get("soil_evaporation"));
      var to_convert = [detention_capacity, soil_evaporation];
      if (this.stochastic) {
        // For stochastic forms, value becomes mean and standard deviation is 0
        _.each(to_convert, function(field) {
          field.mean = field.value;
          field.standard_deviation = 0;
          delete field.value;
        });
      } else {
        // For deterministic forms, mean becomes value and sd is thrown away
        _.each(to_convert, function(field) {
          field.value = field.mean;
          delete field.mean;
          delete field.standard_deviation;
        });
      }
      this.model.set({
        detention_capacity: detention_capacity,
        soil_evaporation: soil_evaporation
      });
      this.render();
    },
    /**
     * Set the model to be represented by the form. Extends the implemenation
     * in Mossaic.forms.Basic by updating this.stochastic.
     * @memberof Mossaic.forms.Rainfall
     * @instance
     * @param {Mossaic.models.BoundaryConditions} model Model to be represented
     *                                                  by this form
     */
    set_model: function(model) {
      Mossaic.forms.Basic.prototype.set_model.apply(this, [model]);
      if (this.model.is_stochastic()) {
        this.stochastic = true;
      } else {
        this.stochastic = false;
      }
    },
    /**
     * Get the hourly rainfall volumes for the supplied magnitude (frequency,
     * annual return period) and duration.
     * Provides 7 days pre and post of zero rainfall.
     * @memberof Mossaic.forms.Rainfall
     * @instance
     * @private
     * @param {object} params Contains either both magnitude and duration, or
     *                        volume
     * @param {string} params.magnitude The magnitude of the storm
     *                                  (e.g. "1 in 5")
     * @param {integer} params.duration The duration of the storm in hours
     * @param {float} params.volume The volume of rainfall
     * @return {array} Array of intensities for each hour
     */
    get_rainfall_intensities: function(params) {
      var magnitude = params.magnitude;
      var duration = params.duration;
      var intensities = [];
      // 7 days pre storm
      _.each(_.range(24 * 7), function(i) {
        intensities.push(0);
      });
      // Now add appropriate volumes
      // Mapping in rhok/raintable.js gives us the total volume (in m) for
      // each magnitude/duration. Need to divide by 1000, then divide by
      // duration.
      var volume = params.volume || _.find(volumes_map, function(volume) {
        return volume.magnitude === magnitude && volume.duration === duration;
      }).volume;
      var volume_per_hour = volume / 1000 / duration;
      _.each(_.range(24), function(i) {
        if (i < duration) {
          intensities.push(volume_per_hour);
        } else {
          intensities.push(0);
        }
      });
      // 7 days post storm
      _.each(_.range(24 * 7), function(i) {
        intensities.push(0);
      });
      return intensities;
    },
    /**
     * Get the default set of rainfall attributes
     * @memberof Mossaic.forms.Rainfall
     * @instance
     * @private
     * @return {object} Hash of parameters for a default rainfall model
     */
    get_blank_rainfall: function() {
      var rainfall = {
        start: {
          unit: "h",
          value: 0
        },
        rainfall_intensities: {
          unit: "mh-1",
          value: this.get_rainfall_intensities({
            magnitude: "1 in 5",
            duration: 1
          })
        }
      };
      if (this.application === "questa") {
        rainfall.probability = 0.2;
      }
      return rainfall;
    },
    /**
     * Add a new design storm to the series
     * @memberof Mossaic.forms.Rainfall
     * @instance
     */
    add_new_rainfall: function() {
      var storm = _.clone(this.model.get("storm"));
      storm.push(this.get_blank_rainfall());
      this.model.changed_by = this;
      this.model.set({storm: storm});
    },
    /**
     * Remove a design storm from the series in response to an event triggered
     * by the DOM.
     * The storm is identified by the id of the event target.
     * @memberof Mossaic.forms.Rainfall
     * @instance
     */
    remove_rainfall: function() {
      var target = $(d3.event.target);
      var id = target.attr("id").split("_");
      var index = id[id.length - 1];
      var storm = _.clone(this.model.get("storm"));
      storm.splice(index, 1);
      this.model.changed_by = this;
      this.model.set({storm: storm});
    },
    /**
     * Update the boundary conditions in the model in response to an event
     * triggered by the DOM.
     * @memberof Mossaic.forms.Rainfall
     * @instance
     */
    update_boundary_conditions: function() {
      var subfield;
      var target = $(d3.event.target);
      var field = target.attr("id").split("-")[0];
      if (this.stochastic) {
        subfield = target.attr("id").split("-")[1];
      } else {
        subfield = "value";
      }
      var value = parseFloat(target.attr("value"));
      var attr = _.clone(this.model.get(field));
      attr[subfield] = value;
      this.model.changed_by = this;
      this.model.set(field, attr);
    },
    /**
     * Convert magnitude string (e.g. "1 in 5") to a daily probability
     * @memberof Mossaic.forms.Rainfall
     * @instance
     * @private
     * @param {string} magnitude Magnitude string (e.g. "1 in 5")
     * @return {float} Daily probability
     */
    magnitude_to_probability: function(magnitude) {
      var magnitude_tokens = magnitude.split(" in ");
      var annual_frequency = parseInt(magnitude_tokens[0], 10) /
        parseInt(magnitude_tokens[1], 10);
      return annual_frequency / 365;
    },
    /**
     * Update the rainfall series in the model.
     * Field to be updated and storm index are determined from the id of the
     * DOM element that triggered the event.
     * @memberof Mossaic.forms.Rainfall
     * @instance
     */
    update_rainfall: function() {
      var target = $(d3.event.target);
      var magnitude,
        duration,
        intensities,
        rainfall_intensities,
        volume;
      var id = target.attr("id").split("_");
      var value = target.val();
      var index = parseInt(id[id.length - 1], 10);
      var field = id.slice(1, id.length - 1).join("_");
      var storms = _.clone(this.model.get("storm"));
      var storm = _.clone(storms[index]);
      if (field === "duration" || field === "start") {
        value = parseInt(value, 10);
      } else if (field === "volume") {
        value = parseFloat(value);
      }
      if (field === "duration") {
        duration = value;
        magnitude = $("#" + id[0] + "_magnitude_" + index).val();
        var params = {
          duration: duration
        };
        if (typeof(magnitude) === "undefined" || magnitude.length === 0) {
          volume = parseFloat($("#" + id[0] + "_volume_" + index).val());
          params.volume = volume;
        } else {
          params.magnitude = magnitude;
        }
        intensities = this.get_rainfall_intensities(params);
        rainfall_intensities = _.clone(storm.rainfall_intensities);
        rainfall_intensities.value = intensities;
        storm.rainfall_intensities = rainfall_intensities;
      } else if (field === "magnitude") {
        magnitude = value;
        duration = parseInt($("#" + id[0] + "_duration_" + index).val(),10);
        intensities = this.get_rainfall_intensities({
            magnitude: magnitude,
            duration: duration
        });
        rainfall_intensities = _.clone(storm.rainfall_intensities);
        rainfall_intensities.value = intensities;
        storm.rainfall_intensities = rainfall_intensities;
        if (this.application === "questa") {
          storm.probability = this.magnitude_to_probability(magnitude);
        }
      } else if (field === "start") {
        var attr = _.clone(storm[field]);
        attr.value = value;
        storm[field] = attr;
      } else if (field === "volume") {
        volume = value;
        duration = parseInt($("#" + id[0] + "_duration_" + index).val(),10);
        intensities = this.get_rainfall_intensities({
            volume: volume,
            duration: duration
        });
        rainfall_intensities = _.clone(storm.rainfall_intensities);
        rainfall_intensities.value = intensities;
        storm.rainfall_intensities = rainfall_intensities;
      } else {
        storm[field] = value;
      }
      storms[index] = storm;
      this.model.changed_by = this;
      this.model.set({storm: storms});
    },
    /**
     * Determine the total volume of a given rainfall
     * @memberof Mossaic.forms.Rainfall
     * @instance
     * @private
     * @param {object} rainfall Rainfall parameters
     * @param {object} rainfall.rainfall_intensities Rainfall intensities
     * @param {array} rainfall.rainfall_intensities.value Intensity values
     * @param {integer} duration Duration of rainfall in hours
     * @return {float} Volume of rainfall for given intensities and duration
     */
    get_volume: function(rainfall, duration) {
      var duration = duration || this.get_duration(rainfall);
      var intensities = rainfall.rainfall_intensities.value;
      var volume = _.find(intensities, function(intensity) {
        return intensity != 0;
      }) * duration * 1000;
      return volume;
    },
    /**
     * Determine the magnitude (frequency) of a rainfall
     * @memberof Mossaic.forms.Rainfall
     * @instance
     * @private
     * @param {object} rainfall Rainfall parameters
     * @param {object} rainfall.rainfall_intensities Rainfall intensities
     * @param {array} rainfall.rainfall_intensities.value Intensity values
     * @return {string} Magnitude of rainfall
     */
    get_magnitude: function(rainfall) {
      var duration = this.get_duration(rainfall);
      var volume = this.get_volume(rainfall, duration);
      var almost_equal = Mossaic.utils.almost_equal;
      var magnitude = _.find(volumes_map, function(magnitude) {
        return almost_equal(magnitude.volume, volume, 0.01) &&
            magnitude.duration === duration;
      });
      if (typeof(magnitude) === "undefined") {
        return;
      } else {
        return magnitude.magnitude;
      }
    },
    /**
     * Determine the duration of a rainfall that is assumed to consist of 24
     * total hours with 7 days of padding before and after.
     * @memberof Mossaic.forms.Rainfall
     * @instance
     * @private
     * @param {object} rainfall Rainfall parameters
     * @param {object} rainfall.rainfall_intensities Rainfall intensities
     * @param {array} rainfall.rainfall_intensities.value Intensity values
     * @return {integer} Duration of rainfall
     */
    get_duration: function(rainfall) {
      var intensities = rainfall.rainfall_intensities.value;
      var padding = 24 * 7; // 7 days of padding
      var duration = 0;
      var done = false;
      // Check front padding values are zero
      _.each(_.range(padding), function(i) {
        if (intensities[i] != 0) {
          throw "Unexpected rainfall data format";
        }
      });
      _.each(_.range(padding, padding + 24), function(i) {
        if (intensities[i] == 0) {
          done = true;
        }
        if (!done) {
          ++duration;
        }
      });
      // Check remaining values are all zero
      _.each(_.range(padding + duration, intensities.length), function(i) {
        if (intensities[i] != 0) {
          throw "Undexpected rainfall data format";
        }
      });
      return duration;
    },
    /**
     * Handle change events triggered by the model. Only redraw if the
     * model was changed by a different view.
     * Changes that were made here only cause re-rendering if the number of
     * rainfalls in the series has changed, or the rainfall volume for a
     * rainfall instance has changed.
     * @memberof Mossaic.forms.Rainfall
     * @instance
     */
    handle_change: function(model) {
      if (this.model.changed_by != this) {
        this.render();
      } else {
        if ("storm" in this.model.changedAttributes()) {
          var has_number_of_storms_changed = (!this.model.previous("storm") ||
              (this.model.changedAttributes().storm.length !==
              this.model.previous("storm").length));
          var previous_storm = this.model.previous("storm") || [];
          var storm = this.model.get("storm") || [];
          var rainfall_volume_changed = !_.all(storm, function(instance, i) {
            var intensities = instance.rainfall_intensities.value;
            var previous_intensities =
              typeof(previous_storm[i]) !== "undefined" &&
                previous_storm[i].rainfall_intensities.value;
            return _.isEqual(intensities, previous_intensities);
          });
          if (has_number_of_storms_changed || rainfall_volume_changed) {
            this.render();
          }
        }
      }
    },
    /**
     * Render the form.
     * Will be called implicitly by show or handle_change so should not need
     * to be called directly.
     * @memberof Mossaic.forms.Rainfall
     * @instance
     */
    render: function() {
      if (this.hidden) {
        return;
      }
      var that = this;
      var storm = this.model.get("storm");
      if (typeof(storm) === "undefined") {
        this.model.set({storm: []}, {silent: true});
      }

      d3.select(this.form_id).remove();

      var div = d3.select(this.divname)
        .append("xhtml:div")
        .attr("id", this.form_id.slice(1));

      var form = d3.select(this.form_id);

      form.append("xhtml:input")
        .attr("id", "soil_form_stochastic")
        .attr("type", "checkbox")
        .attr("checked", this.stochastic && "checked" || null)
        .on("change", this.flip_stochastic);
      form.append("xhtml:label")
        .attr("for", "soil_form_stochastic")
        .html("Enable stochastic input");
      form.append("xhtml:br");

      var top_table = form.append("xhtml:table")
        .attr("class", "wide");
      var top_tr = top_table.append("xhtml:tr");
      top_tr.append("xhtml:th")
        .html("Upslope recharge (mh-1)");
      top_tr.append("xhtml:td").append("xhtml:input")
        .attr("id", "upslope_recharge")
        .attr("value", this.model.get("upslope_recharge").value)
        .on("change", this.update_boundary_conditions);
      if (this.stochastic) {
        top_tr = top_table.append("xhtml:tr");
        top_tr.append("xhtml:th");
        top_tr.append("xhtml:td").append("label").html("Mean");
        top_tr.append("xhtml:td").append("label").html("Sd");
      }
      top_tr = top_table.append("xhtml:tr");
      top_tr.append("xhtml:th")
        .html("Detention capacity (m)");
      if (this.stochastic) {
        top_tr.append("xhtml:td").append("xhtml:input")
          .attr("id", "detention_capacity-mean")
          .attr("value", this.model.get("detention_capacity").mean)
          .on("change", this.update_boundary_conditions);
        top_tr.append("xhtml:td").append("xhtml:input")
          .attr("id", "detention_capacity-standard_deviation")
          .attr("value",
              this.model.get("detention_capacity").standard_deviation)
          .on("change", this.update_boundary_conditions);
      } else {
        top_tr.append("xhtml:td").append("xhtml:input")
          .attr("id", "detention_capacity")
          .attr("value", this.model.get("detention_capacity").value)
          .on("change", this.update_boundary_conditions);        
      }
      top_tr = top_table.append("xhtml:tr");
      top_tr.append("xhtml:th")
        .html("Soil evaporation (ms-1)");
      if (this.stochastic) {
        top_tr.append("xhtml:td").append("xhtml:input")
          .attr("id", "soil_evaporation-mean")
          .attr("value", this.model.get("soil_evaporation").mean)
          .on("change", this.update_boundary_conditions);
        top_tr.append("xhtml:td").append("xhtml:input")
          .attr("id", "soil_evaporation-standard_deviation")
          .attr("value", this.model.get("soil_evaporation").standard_deviation)
          .on("change", this.update_boundary_conditions);
      } else {
        top_tr.append("xhtml:td").append("xhtml:input")
          .attr("id", "soil_evaporation")
          .attr("value", this.model.get("soil_evaporation").value)
          .on("change", this.update_boundary_conditions);
      }

      form.append("xhtml:button")
        .html("Add new rainfall")
        .attr("class", "add")
        .on("click", this.add_new_rainfall);
      var table = form.append("xhtml:table");
      var tr = table.append("xhtml:tr");
      tr.append("xhtml:th")
        .html("Start (h)");
      tr.append("xhtml:th")
        .append("xhtml:abbr")
          .attr("title", "Annual return period")
          .html("Frequency");
      tr.append("xhtml:th")
        .style("min-width", "100px")
        .html("Duration (h)");
      tr.append("xhtml:th")
        .html("Volume (mm3)");
      _.forEach(storm, function(rainfall, i) {
        var tr = table.append("xhtml:tr");
        tr.append("td").append("xhtml:input")
          .attr("id", "rainfall_start_" + i)
          .attr("class", "input-thin")
          .attr("value", rainfall.start.value)
          .on("change", that.update_rainfall);
        var magnitude = tr.append("td").append("xhtml:select")
          .attr("id", "rainfall_magnitude_" + i)
          .on("change", that.update_rainfall);
        var current_magnitude = that.get_magnitude(rainfall);
        if (typeof(current_magnitude) === "undefined") {
          // Convert probability to magnitude if it exists
          if (typeof(rainfall.probability) !== "undefined") {
            var annual_probability = rainfall.probability * 365;
            current_magnitude = "1 in " + Math.round(1 / annual_probability);
          } else {
            magnitude.append("xhtml:option");
          }
        }
        _.each(that.magnitudes, function(magnitude_value) {
          var option = magnitude.append("xhtml:option")
            .html(magnitude_value);
          if (current_magnitude === magnitude_value) {
            option.attr("selected", true);
          }
        });
        var duration = tr.append("td").append("xhtml:select")
          .attr("id", "rainfall_duration_" + i)
          .on("change", that.update_rainfall);
        _.each(that.durations, function(duration_value) {
          var option = duration.append("xhtml:option")
            .html(duration_value);
          if (that.get_duration(rainfall) === duration_value) {
            option.attr("selected", true);
          }
        });
        var volume = tr.append("td").append("xhtml:input")
          .attr("id", "rainfall_volume_" + i)
          .attr("value", that.get_volume(rainfall))
          .on("change", that.update_rainfall);
        tr.append("td").append("xhtml:button")
          .attr("class", "delete")
          .attr("id", "delete_" + i)
          .on("click", that.remove_rainfall);
      });
    }
  });
})();