/**
 * @file Defines various subclasses of Backbone.Model used in the app
 */
(function() {
  
  var scripts_to_load = [
    "lib/models/basic.js",
    "lib/models/boundary_conditions.js",
    "lib/models/cross_section.js",
    "lib/models/engineering_cost.js",
    "lib/models/geometry.js",
    "lib/models/job.js",
    "lib/models/meta.js",
    "lib/models/predefined_soils.js",
    "lib/models/reinforcements.js",
    "lib/models/results.js",
    "lib/models/road_network.js",
    "lib/models/soils.js",
    "lib/models/stability.js",
    "lib/models/task.js"
  ];
  
  /**
   * Backbone Models used in the application.
   *
   * @namespace
   */
  Mossaic.models = {}; 

  var load_scripts = function(scripts) {
    for (var i=0; i < scripts.length; i++) {
      document.write('<script src="'+scripts[i]+'"><\/script>');
    }
  };

  load_scripts(scripts_to_load);

  Mossaic.models.map_models = function() {
    /**
      * Convenience map for retrieving the relevant model type
      * This is called by Router so the models are actually loaded 
      * before we try and put them into the map.
      *
      * @type object
      */
    Mossaic.models.map = {
     basic: Mossaic.models.Basic,
     geometry: Mossaic.models.Geometry,
     stability: Mossaic.models.Stability,
     meta: Mossaic.models.Meta,
     soils: Mossaic.models.Soils,
     boundary_conditions: Mossaic.models.BoundaryConditions,
     engineering_cost: Mossaic.models.EngineeringCost,
     reinforcements: Mossaic.models.Reinforcements,
     road_network: Mossaic.models.RoadNetwork,
     results: Mossaic.models.Results,
     task: Mossaic.models.Task,
     job: Mossaic.models.Job,
     cross_section: Mossaic.models.CrossSection,
     predefined_soils: Mossaic.models.PredefinedSoils
    };
  }
})();
