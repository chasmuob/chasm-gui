/**
 * @file Defines the Backbone.Router for the application.
 *       Adds a class called Router to the Mossaic namespace. This is instantiated
 *       the same way as any other Backbone router.
 *
 *       UPDATE
 *       This file is currently a mix of legacy and new code.  The vast majority of code here is legacy code that does
 *       a lot of setup and work in this file itself. New code uses RequireJS and the main_action() function to
 *       delegate the actual functionaility to seperate files.  See inline comments in main_action() for details.
 */
define([],
  function() {
    /**
     * The top-level object for the application.
     * Responsible for providing the control flow of the application, rendering
     * appropriate content for a given url and holding the master state of
     * the application (basically a number of Models).
     * @constructor
     * @extends Backbone.Router
     */
    return Backbone.Router.extend({
      /**
       * Initialise the router. Called implicitly by "new Mossaic.Router"
       *
       * @memberof Mossaic.Router
       * @instance
       * @param {object} options Hash of options
       * @param {string} options.container CSS selector for the container div
       *                                   that holds the app. This should already
       *                                   exist on the page that imports the
       *                                   mossaic.js script.
       */
      initialize: function(options) {
        _.bindAll(this);
        Mossaic.forms.build_deferred();
        Mossaic.widgets.build_deferred();
        Mossaic.models.map_models();


        this.View = null;
        this.view = null;

        /**
         * CSS selector for the container div that holds the app
         * @type string
         */
        this.container = options.container;
        /**
         * Refernce to the CouchDB design document that the app is served from
         * @type object
         */
        this.ddoc = Mossaic.db.ddoc;
        /**
         * Application-wide object that stores the currently selected slope datum
         * metadata object
         * @type Mossaic.models.Meta
         */
        this.slope = new Mossaic.models.Meta();
        this.slope.is_dirty = true;
        /**
         * Application-wide object that stores the currently selected cross
         * section object
         * @type Mossaic.models.CrossSection
         */
        this.cross_section = new Mossaic.models.CrossSection();
        this.cross_section.is_dirty = true;
        /**
         * Application-wide object that stores the currently selected geometry
         * object
         * @type Mossaic.models.Geometry
         */
        this.geometry = new Mossaic.models.Geometry({}, {
          datum: this.slope,
          mode: "chasm"
        });

        this.stability = new Mossaic.models.Stability({},{
          geometry: this.geometry
        });
        this.stability.isOriginalOne = true;

        this.boundary_conditions = new Mossaic.models.BoundaryConditions();
        this.soils = new Mossaic.models.Soils();
        // this.stability = undefined;
        // this.boundary_conditions = undefined;
        // this.soils = undefined;
        /**
         * Application-wide object that stores the currently selected task
         * @type Mossaic.models.Task
         */
        this.task = new Mossaic.models.Task();
        this.task.is_dirty = true;
        this.task.metadata = {
          rainfall_type: "design",
          soils_type: "deterministic",
          depths_type: "deterministic",
          vegetation: false,
          reinforcements: false
        };

        this.action_id = null;

        this.state = {
          meta: this.slope,
          cross_section: this.cross_section,
          geometry: this.geometry,
          task: this.task,
          stability: this.stability,
          soils: this.soils,
          boundary_conditions: this.boundary_conditions
        };

        /**
         * List of current breadcrumbs
         * @type array
         */
        this.breadcrumbs = [];
        Backbone.Router.prototype.initialize.apply(this, [options]);
      },
      /**
       * Mapping of route urls to functions
       *
       * UPDATE:  New routes aren't explicitly specified, and instead there is a convention that maps any non-specified
       * routes to main_action().  This then delegates to subcontrollers in an MVC controller/action/params style
       *
       * @memberof Mossaic.Router
       * @type object
       */
      routes: {
        "": "default_route",
        "disclaimer": "disclaimer",
        // "task_composer/choose_slope": "task_composer_choose_slope",
        // "task_composer/create_slope": "task_composer_create_slope",
        // "task_composer/edit_inputs_linear": "task_composer_edit_inputs_linear",
        // "task_composer/edit_inputs_linear/:page": "task_composer_edit_inputs_linear",
        // "task_composer/finalise_task": "finalise_task",
        "task_analyser/choose_slope": "task_analyser_choose_slope",
        "task_analyser/analyse_task/id/:id": "analyse_task_by_id",
        "task_analyser/analyse_task": "analyse_task",
        // "task_status/choose_slope": "task_status_choose_slope",
        // "task_status/tasks": "task_status_tasks",
        "data_upload": "data_upload",
        ":controller" : "main_action",
        ":controller/:action" : "main_action",
        ":controller/:action/id/:id/*params" : "main_action_with_id",
        ":controller/:action/*params" : "main_action"
      },
      /**
       * Mapping of route urls to display name - used so we can have meaningful
       * text displayed for each breadcrumb.
       * @memberof Mossaic.Router
       * @type object
       */
      route_to_name: {
        "": "Home",
        "task-composer": "Choose slope",
        "task_composer/choose_slope": "Choose slope",
        "task_composer/create_slope": "Create slope",
        "task_composer/edit_inputs_linear": "Edit inputs",
        "task_composer/finalise_task": "Finalise_task",
        "task_analyser/choose_slope": "Choose slope",
        "task_analyser/analyse_task": "Analyse task",
        "task_status/choose_slope": "Choose slope",
        "task_status/tasks": "Task status",
        "data_upload": "Upload input data"
      },
      /**
       * Some urls have a default breadcrumb trail, so if they are visited
       * directly they can still take the user "back home"
       * @memberof Mossaic.Router
       * @type object
       */
      default_breadcrumbs: {
        "task_composer/choose_slope": [""],
        "task-composer": [""],
        "task_composer/create_slope": [""],
        "task_analyser/choose_slope": [""],
        "task_status/choose_slope": [""],
        "task_status/tasks": ["", "task_status/choose_slope"],
        "data_upload": [""]
      },


      load_models: function(id, cb){
        var that = this;
        this.state.geometry.set('id',id);
        this.state.geometry.set('isNew',false);

        this.state.geometry.fetch({
          success: function(){
            that.state.cross_section.set('id',that.state.geometry.get('parent_id'));
            that.state.cross_section.fetch({
              success: function(){
                that.state.meta.set('id',that.state.cross_section.get('parent_id'));
                that.state.meta.fetch({
                  success: function(){
                    $.couch.db('chasm').view('gui/docs_by_parent', {
                      reduce: false,
                      descending: false,
                      include_docs: true,
                      skip: 0,
                      inclusive_end: true,
                      update_seq: false,
                      success: function(data) {

                        var soils = _.find(data.rows,function(item){
                          return item.doc.type === 'soils' && item.doc.parent_id === id;
                        });
                        var boundary_conditions = _.find(data.rows,function(item){
                          return item.doc.type === 'boundary_conditions' && item.doc.parent_id === id;
                        });
                        var stability = _.find(data.rows,function(item){
                          return item.doc.type === 'stability' && item.doc.parent_id === id;
                        });
                        that.state.soils = new Mossaic.models.Soils(soils && soils.doc || {parent_id: id});
                        that.state.boundary_conditions = new Mossaic.models.BoundaryConditions(boundary_conditions && boundary_conditions.doc || {parent_id: id});
                        that.state.stability = new Mossaic.models.Stability(stability && stability.doc || {parent_id: id},{
                          geometry: that.state.geometry
                        });
                        cb();
                        // that.main_action(view_name,action,params);
                      }
                    });
                  }
                });
              }
            });
          }
        });
      },

      main_action_with_id : function(view_name, action, id, params){
        var that = this;

        if(this.action_id === id){
          return that.main_action(view_name,action,params);
        }
        this.action_id = id;

        this.state.geometry.set('id',id);
        this.state.geometry.set('isNew',false);
        this.load_models(id,function(){
          return that.main_action(view_name,action,params);
        });
      },
      /**
       * Manage the main view based on the route.
       *
       * @param {string} view_name View to create
       * @param {string} action Method on the view to call
       * @param {string} params Params to pass to the method.
       * @returns {undefined}
       */
      main_action : function(view_name, action, params) {
        var that = this;
        // This follows the classic MVC pattern that maps a URL to: /controller/action/param1/param2
        // So it loads the file "controller", executes the "action" method, and passes it an array of params.
        // There is some URL rewriting, see below.

        // Controller ("View")
        if (typeof(view_name) === "undefined") throw "Undefined controller";
        // TODO - default controller

        // Replace any hypens in the viewName with underscores so they map to the filesystem
        view_name = view_name.replace("-","_");

        // Action
        if (typeof(action) === "undefined") {
          this.action = "default_action";
        } else {
          // Convert any hypens to underscores, and append "_action" to the end, so they map to methods
          this.action = action.replace("-","_")+ "_action";

          // or camelCase instead....
          // this.action = action.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase() }) + "Action";
        }

        // Params
        if (typeof(params) === "undefined") {
          this.params = undefined;  //Reset this.params from any previous calls
        } else {
          this.params = params.split("/");
        }
        this.update_breadcrumbs();
        // GO!
        require(["views/" + view_name], function(View) {
          // Don't recreate the view if we're only changing action.  This enables the view to maintain state.
          // If we haven't changed view, then it's the views responsibility for it's new actions to clean up any
          // old actions, generally by new actions calling "this.empty()".
          // Otherwise if we have a new view, we clear up the old one here instead.
          if (View !== that.View) {
            if (typeof(that.view) !== "undefined" && that.view !== null) {
              that.view.remove(); // Clean up the old view
            }
            that.View = View; // "Class"
            that.view = new View(that.state); // instantiated "Object"

            // We don't specify an "el" when creating the view, because view.remove() removes the element entirely.
            // i.e. if we put it into an existing DOM element - <div id="subView /> - then this element would be
            // removed later, but we would still need it to put future subviews into.
            // Doing it this way, Backbone puts the view into a new div, and then we put that into the main DOM. When
            // view.remove() is called, it just removes the div it created and leaves the DOM as it was originally.

            // The views action should return the content...
            var output = that.view[that.action](that.params);
            // ...which we insert into the DOM.  This should be the *only* place that uses a non-scoped jQuery
            // selector.  i.e. $(...).
            $(that.container).html(output);
          } else {
            // If we haven't changed views we don't need to insert the view output into the DOM, because it's
            // already there.   The new action can change the live DOM by modifying it's "this.el" content.
            // The view will still return it's output as above, but we simply ignore it this time.
            that.view[that.action](that.params);
          }

        });

      },


      /**
       * Bind to the beforeunload event so we can catch instances where a user
       * might lose unsaved data if they refresh or navigate away from a page.
       * The supplied message is returned if the task model is dirty, blank
       * otherwise (in which case the users' navigation event is uninterrupted).
       *
       * Call with a blank msg to unset any previously bound function.
       *
       * @memberof Mossaic.Router
       * @instance
       * @private
       * @param {String} msg Message to be displayed on the beforeunload event
       */
      catch_unload: function(msg) {
        var that = this;
        if (typeof(msg) === "string") {
          $(window).bind("beforeunload", function() {
            if (that.task.is_dirty) {
              return msg;
            } else {
              return;
            }
          });
        } else {
          $(window).unbind("beforeunload");
        }
      },
      /**
       * Update the breadcrumbs so they show the correct trail for the current
       * route.
       *
       * The update process is:
       *  - If there are no breadrumbs but the route has default breadcrumbs,
       *    use them
       *  - If the route already exists in the breadcrumbs, remove everything
       *    after that route.
       *  - Otherwise, append the route to the breadcrumb trail
       *
       * @memberof Mossaic.Router
       * @instance
       * @private
       */
      update_breadcrumbs: function() {
        var that = this;
        var route = Backbone.history.fragment.split("/:")[0];
        var breadcrumb_index = -1;
        _.any(this.breadcrumbs, function(breadcrumb, i) {
          if (breadcrumb === route) {
            breadcrumb_index = i;
            return true;
          } else {
            return false;
          }
        });
        if (this.breadcrumbs.length === 0 && route in this.default_breadcrumbs) {
          this.breadcrumbs = _.clone(this.default_breadcrumbs[route]);
        }
        if (breadcrumb_index === -1) {
          this.breadcrumbs = this.breadcrumbs.concat(route);
        } else {
          this.breadcrumbs = _.first(this.breadcrumbs, breadcrumb_index + 1);
        }
        var rendered_breadcrumbs = _.map(this.breadcrumbs, function(breadcrumb) {
          return $.Mustache.render('tab', {
            link: "#" + breadcrumb,
            name: that.route_to_name[breadcrumb]
          });
        });
        $("#breadcrumbs").html(rendered_breadcrumbs.join(" > "));
      },
      /**
       * The default route renders the home template
       * @memberof Mossaic.Router
       * @instance
       */
      default_route: function() {
        this.catch_unload();
        this.update_breadcrumbs();
        delete this.View;   // Remove the view record so methods using MVC routing can change themselves
        $(this.container).mustache("home", {}, {method: "html"});
        this.angularEl = angular.element('<div><chasm-browser></chasm-browser></div>');
        angular.bootstrap(this.angularEl,['chasm']);
        $(this.container).prepend(this.angularEl);
      },
      /**
       * Disclaimer Page
       */
      disclaimer: function(){
        this.catch_unload();
        this.update_breadcrumbs();
        delete this.View;   // Remove the view record so methods using MVC routing can change themselves
        $(this.container).mustache("disclaimer", {}, {method: "html"});
      },
      /**
       * Render the task status page
       * @memberof Mossaic.Router
       * @instance
       */
      task_status_tasks: function() {
        this.update_breadcrumbs();
        $(this.container).mustache("task_status", {}, {method: "html"});
        if (typeof(this.profile_browser) === "undefined") {
          this.profile_browser = Mossaic.widgets.ls({
            divname: "#profile_browser",
            label: "Slope profiles",
            type: "geometry",
            parent: this.cross_section_browser,
            view: "FilterList",
            height: "150px"
          });
          this.profile_browser.reset();
        } else {
          this.profile_browser.show({height: "150px"});
        }
        if (typeof(this.task_status_viewer) === "undefined") {
          this.task_status_viewer = Mossaic.widgets.task_status({
            divname: "#task_status_viewer",
            key_divname: "#task_status_key",
            parent: this.profile_browser
          });
        } else {
          this.task_status_viewer.show();
        }
      },
      /**
       * Load a specific task to analyse
       */
      analyse_task_by_id: function(id){
        $.couch.db('chasm_task').openDoc(decodeURIComponent(id),{
          success: function(data){
            this.state.task.set(data);
            this.analyse_task();
          }.bind(this)
        });
      },
      /**
       * Render the task analyser page
       * @memberof Mossaic.Router
       * @instance
       */
      analyse_task: function() {
        this.catch_unload("Your current analysis will be discarded.");

        $(this.container).mustache("analyse_task", {}, {method: "html"});
        this.task_browser = Mossaic.widgets.ls({
          divname: "#task_browser",
          label: "Simulation tasks",
          type: "task",
          urlRoot: Mossaic.db.get_db_name({type: "task"}),
          url: "tasks_by_slope",
          ddocName: "workflow",
          load_model_metadata_only: true,
          parent: this.profile_browser,
          view: "FilterList",
          height: "100px"
        });
        this.task_browser.reset();
        this.state.task.id = this.state.task.get('_id');
        this.task_browser.set_selected(this.state.task);

        this.jobs_browser = Mossaic.widgets.ls({
          divname: "#jobs_browser",
          label: "Jobs",
          type: "job",
          urlRoot: Mossaic.db.get_db_name({type: "task"}),
          url: "jobs_by_task",
          ddocName: "workflow",
          load_model_metadata_only: true,
          parent: this.task_browser,
          view: "FilterList",
          height: "100px",
          selectFirstOnLoad: true
        });
        var jb = this.jobs_browser;
        this.jobs_browser.reset({success: function(collection){
          jb.set_selected(collection.at(0));
        }});

        this.results_viewer = Mossaic.widgets.task_analyser({
          divname: "#results_viewer",
          parent: this.task_browser,
          width: $("#results_viewer").width(),
          height: $(document).height() * 0.7,
          jobs_browser: this.jobs_browser
        });
        this.results_viewer.reset();
      },
      /**
       * Render the task finaliser page. This is where users can assign names
       * to unsaved datasets and actually submit the task.
       * @memberof Mossaic.Router
       * @instance
       */
      finalise_task: function() {
        this.catch_unload("This task has not yet been saved.");
        if ((typeof(this.slope_editor) === "undefined" &&
                typeof(this.slope_editor_linear) === "undefined") ||
                typeof(this.datum_browser) === "undefined")
        {
          window.location.href = "#";
          return;
        }
        this.update_breadcrumbs();
        $(this.container).mustache("finalise_task", {}, {method: "html"});

        var executable = this.task.get("executable");

        // Input sources are ls widgets (aka "browsers") for each of the models for the simulation params.
        var input_sources = {
          boundary_conditions: this.slope_editor_linear.browsers.boundary_conditions,
          geometry: this.profile_browser,
          soils: this.slope_editor_linear.browsers.soils,
          stability: this.slope_editor_linear.browsers.stability
        };
        if (this.task.metadata.vegetation) {
          input_sources.vegetation = this.vegetation_browser;
        }
        if (this.task.metadata.reinforcements) {
          input_sources.reinforcements = this.reinforcements_browser;
        }
        if (this.geometry.mode === "questa") {
          input_sources.road_network = this.slope_editor_linear.browsers.road_network;
          input_sources.engineering_cost = this.slope_editor_linear.browsers.engineering_cost;
        }
        Mossaic.widgets.task_builder({
          model: this.task,
          divname: "#task_creator",
          view: "finalise",
          meta_source: this.datum_browser,
          cross_section_source: this.cross_section_browser,
          input_sources: input_sources
        });
      },
      /**
       * Render the task composer screen in slope workflow
       * @memberof Mossaic.Router
       * @instance
       */
      task_composer_edit_inputs_linear: function(page) {
        var that = this;
        this.catch_unload("The current task and any changes to the input data " +
                "will be lost .");
        if (typeof(this.datum_browser) === "undefined") {
          window.location.href = "#";
          return;
        }

        var already_exists = !($(this.container + " #slope_editor").length === 0);

        if (!already_exists) {
          this.update_breadcrumbs();
          $(this.container).mustache("edit_inputs_linear", {}, {method: "html"});
        }

        if (typeof(page) === "undefined") {
          if (typeof(this.slope_editor_linear) === "undefined") {
            this.slope_editor_linear = Mossaic.editors.slope({
              divname: "#slope_editor",
              display_div: "#mossaic_slope_display",
              form_div: "#mossaic_slope_form",
              tip_div: "#specify_profile_tip",
              overspill_div: "#overspill",
              tab_div: "#mossaic_tabs",
              meta: this.slope,
              geometry: this.geometry,
              geometry_browser: this.profile_browser,
            number_of_sections: this.geometry_metadata_form.number_of_sections,
              upslope_sections: this.geometry_metadata_form.upslope_sections,
              downslope_sections: this.geometry_metadata_form.downslope_sections,
              next_route: "#task_composer/finalise_task",
              current_route: "#task_composer/edit_inputs_linear"
            });
          }

          this.slope_editor_linear.show({
          number_of_sections: this.geometry_metadata_form.number_of_sections,
            upslope_sections: this.geometry_metadata_form.upslope_sections,
            downslope_sections: this.geometry_metadata_form.downslope_sections,
            index: 0
          });
        if (typeof(this.dashboard) === "undefined") {
          this.dashboard = Mossaic.widgets.dashboard({
            divname: "#dashboard",
            datum_browser: this.datum_browser,
            cross_section_browser: this.cross_section_browser,
            profile_browser: this.profile_browser
          });
        }
          this.dashboard.show();
        } else {
          var index = parseInt(page.split(":")[1]);
          this.slope_editor_linear.show({index: index});
          this.dashboard.show();
        }
      },
      /**
       * Render the choose slope page for the task composer
       *
       * This creates the choose slope page with the option to create new
       * cross-sections.
       * @memberof Mossaic.Router
       * @instance
       */
      task_composer_choose_slope: function() {
        this.update_breadcrumbs();
        if (typeof(this.datum_browser) !== "undefined") {
          this.datum_browser.set_input_only(false);
        }
        if (typeof(this.cross_section_browser) !== "undefined") {
          this.cross_section_browser.set_input_only(false);
        }
        this.choose_slope({next_route: "task_composer/edit_inputs_linear"});
      },
      /**
       * Render the create slope page for the task composer
       * @memberof Mossaic.Router
       * @instance
       */
      task_composer_create_slope: function() {
        this.update_breadcrumbs();
        if (typeof(this.datum_browser) !== "undefined") {
          this.datum_browser.set_input_only(true);
          this.datum_browser.reset();
          this.slope.clear();
        }
        if (typeof(this.cross_section_browser) !== "undefined") {
          this.cross_section_browser.set_input_only(true);
          this.cross_section_browser.reset();
          this.cross_section.clear();
        }
        this.choose_slope({next_route: "task_composer/choose_task_type"});
      },
      /**
       * Render the choose slope page for the task analyser
       *
       * This does not allow new cross sections to be created
       * @memberof Mossaic.Router
       * @instance
       */
      task_analyser_choose_slope: function() {
        this.update_breadcrumbs();
        if (typeof(this.datum_browser) !== "undefined") {
          this.datum_browser.set_input_only(false);
        }
        if (typeof(this.cross_section_browser) !== "undefined") {
          this.cross_section_browser.set_input_only(false);
        }
        this.choose_slope({next_route: "task_analyser/analyse_task"});
      },
      /**
       * Render the choose slope page for the task status viewer
       *
       * This does not allow new cross sections to be created
       * @memberof Mossaic.Router
       * @instance
       */
      task_status_choose_slope: function() {
        this.update_breadcrumbs();
        if (typeof(this.datum_browser) !== "undefined") {
          this.datum_browser.set_input_only(false);
        }
        if (typeof(this.cross_section_browser) !== "undefined") {
          this.cross_section_browser.set_input_only(false);
        }
        this.choose_slope({next_route: "task_status/tasks"});
      },
      /**
       * Render the choose slope page
       *
       * @memberof Mossaic.Router
       * @instance
       * @param {object} options Hash of options:
       * @param {string} next_route The route to navigate to once
       *                            the slope is chosen/created
       */
      choose_slope: function(options) {
        var that = this,
                title,
                message,
                next_route = options.next_route,
                dirty_message;
        title = "Choose a slope";
        message = "<p>Select an existing datum and cross section.</p>";
        dirty_message = "Choose a slope";
        $(this.container).mustache('choose_slope', {
          title: title,
          message: message
      }, {method:"html"});
        if (typeof(this.datum_browser) === "undefined") {
        this.datum_browser = Mossaic.widgets.ls_save({
          type: "meta",
          ls: {
            divname: "#datum_browser",
            label: "Slope datum",
            view: "FilterList",
            set_model: function(model) {
              that.slope = model;
              that.geometry.datum = model;
              that.datum_form.set_model(model);
              that.slope.change();
            },
            focus: true,
            init_model: this.slope, // Model to be selected on creation
            height: "300px"
          },
          save: {
            divname: "#datum_edit",
            get_model: function() {
              return that.slope;
            },
            allow_save: true,
            dirty_message: "Save",
            clean_message: "Selected",
            class_ok: "btn-success",
            disable_on_success: true,
            css_class: "half-button"
          },
          clear_model: {
            divname: "#datum_clear"
          }
          });
          this.datum_browser.reset();
        } else {
          if (this.slope.is_dirty) {
            this.slope.clear();
            this.slope.changed_by = this;
            this.datum_browser.reset();
          }
          this.datum_browser.show({height: "300px"});
        }
      if (typeof(this.datum_form) === "undefined") {
        this.datum_form = new Mossaic.forms.Datum(this.slope, {
          divname: "#datum_form",
          readonly: false
        });
        this.datum_form.render();
      } else {
        this.datum_form.readonly = false;
        this.datum_form.render();
      }
        if (typeof(this.cross_section_browser) === "undefined") {
        this.cross_section_browser = Mossaic.widgets.ls_save({
            type: "cross_section",
          ls: {
            divname: "#cross_section_browser",
            label: "Cross section",
            parent: this.datum_browser,
            view: "FilterList",
            set_model: function(model) {
              that.cross_section = model;
              that.cross_section_form.set_model(model);
              that.cross_section.change();
            },
            focus: false,
            init_model: this.cross_section,
            clear_model_on_parent_change: true,
            height: "300px"
          },
          save: {
            divname: "#cross_section_edit",
            get_model: function() {
              return [that.slope, that.cross_section];
            },
            name_source: [this.datum_browser],
            allow_save: true,
            dirty_message: "Save",
            clean_message: "Selected",
            class_ok: "btn-success",
            disable_on_success: true,
            clear_model_on_parent_change: true,
            css_class: "half-button"
          },
          clear_model: {
            divname: "#cross_section_clear"
          }
          });
          this.cross_section_browser.reset();
        } else {
          if (this.cross_section.is_dirty) {
            this.cross_section.changed_by = this;
            this.cross_section.clear();
            this.cross_section_browser.reset();
          }
          this.cross_section_browser.show({height: "300px"});
          this.cross_section_browser.update_listeners();
        }
      if (typeof(this.cross_section_form) === "undefined") {
        this.cross_section_form = new Mossaic.forms.CrossSection(this.cross_section,{
          divname: "#cross_section_form",
          readonly: false
        });
        this.cross_section_form.render();
      } else {
        this.cross_section_form.readonly = false;
        this.cross_section_form.render();
      }
        if (typeof(this.profile_browser) === "undefined") {
          this.profile_browser = Mossaic.widgets.ls({
            divname: "#profile_browser",
          label: "Slope profile",
            type: "geometry",
            parent: this.cross_section_browser,
            view: "FilterList",
            set_model: function(model) {
              if (typeof(that.slope_editor) !== "undefined") {
                that.slope_editor.set_geometry(model);
              }
              if (typeof(that.slope_editor_linear) !== "undefined") {
                that.slope_editor_linear.set_geometry(model);
              }
              if (typeof(that.geometry_metadata_form) !== "undefined") {
                that.geometry_metadata_form.set_geometry(model);
              }
              that.geometry = model;
            },
            init_model: this.geometry,
            clear_model_on_parent_change: true,
            height: "300px"
          });
          this.profile_browser.reset();
        }
        this.profile_browser.show({height: "300px"});
        if (typeof(this.geometry_metadata_form) === "undefined") {
          this.geometry_metadata_form = new Mossaic.forms.GeometryMetadata(this.geometry, {
            divname: "#profile_form"
          });
        }
        this.geometry_metadata_form.show();
        this.profile_save = Mossaic.widgets.save({
        divname: "#profile_edit",
          type: "geometry",
          get_model: function() {
            return [that.slope, that.cross_section, that.geometry];
          },
          name_source: [this.datum_browser, this.cross_section_browser,
            this.profile_browser],
          success: function() {
            if (typeof(next_route) !== "undefined") {
              window.location.href = "#" + next_route;
            }
          },
          // This function will be called after parents are confirmed as clean.
          // The result is that once we know parent models are persisted we check
          // we have the required fields in the geometry metadata form then
          // navigate to the next route.
          instead_of_save: function() {
          if (that.geometry_metadata_form.executable === "chasm") {
            if ((typeof(that.geometry_metadata_form.number_of_sections) === "undefined" || that.geometry_metadata_form.number_of_sections === 0) && that.geometry.is_dirty) {
              alert("Enter number of sections");
              return;
            }
          } else if (that.geometry_metadata_form.executable === "questa") {
                if (((typeof(that.geometry_metadata_form.upslope_sections) === "undefined" ||
                        typeof(that.geometry_metadata_form.downslope_sections) === "undefined") ||
                        (that.geometry_metadata_form.downslope_sections === 0 &&
                                that.geometry_metadata_form.upslope_sections === 0)) &&
                        that.geometry.is_dirty) {
                  alert("Enter number of upslope and downslope sections");
                  return;
                }
                var datum_has_road_link = typeof(that.slope) !== "undefined" &&
                        typeof(that.slope.get("road_link")) !== "undefined" &&
                        that.slope.get("road_link").length === 2;
                if (!datum_has_road_link) {
                  alert("Cannot use a Questa slope profile for a datum with no " +
                          "defined road link");
                  return;
                }
              }
          if (typeof(that.profile_browser.get_selected().id) === "undefined") {
            that.geometry.clear();
            }
            if (typeof(next_route) !== "undefined") {
              window.location.href = "#" + next_route;
            }
          },
          allow_save: false,
          dirty_message: "Create new profile",
          clean_message: "Use this profile",
          clear_model_on_parent_change: true,
          css_class: "wide-button"
        });
      },
      /**
       * Render page for uploading input data
       * @memberof Mossaic.Router
       * @instance
       */
      data_upload: function() {
        this.update_breadcrumbs();
        $(this.container).mustache("data_upload", {}, {method: "html"});
        if (typeof(this.input_uploader) === "undefined") {
          this.input_uploader = Mossaic.widgets.input_uploader({
            parent: "#input_uploader"
          });
        }
      }
    });
  }
);
