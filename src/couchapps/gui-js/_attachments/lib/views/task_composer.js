define([
    "views/_basic.rj",
    "widgets/choose_slope.rj",
    "workflows/slope.rj"
  ],
  function(BasicView, ChooseSlopeWidget, SlopeWorkflow) {
    /**
     * View to manage the Task Composer.
     *
     * "Task Composer" is the part of the application for the users to create/manage slopes and create simulation tasks
     *
     * This sets up some initial models, and then shows the "choose slope" screen.  Once that is done it delegates
     * everything else to the "slope" workflow.
     *
     * URL routing is heavily used through out the workflow.
     *
     */
    return BasicView.extend({


      initialize: function(options) {
        BasicView.prototype.initialize.apply(this, [options]);

        // Object that holds the models that are currently selected.  It's passed to the choose_slope widget, which
        // then populates it with what the user chooses.  We can then pass it to the slope workflowee.
        // this.models= {meta: null, cross_section: null, geometry: null};
        // this.models.meta = new Mossaic.models.Meta();
        // this.models.cross_section = new Mossaic.models.CrossSection();
        // this.models.geometry = new Mossaic.models.Geometry();
        this.models = options;
        // Current action.  Used by actions to know whether to clear old subviews etc
        this.action = null;
      },

      /**
       * Display the "Choose Slope" widget, where users can create and select new datums, cross-sections
       * and geometries.  We pass it "this.models", which it populates with the models selected.
       * @param {object} params URL parameters
       * @returns {DOM element}
       */
      default_action: function(params) {
        this.empty();
        this.action = "default";
        var choose_slope_widget = new ChooseSlopeWidget({
          models: this.models
        });
        this.add_subview("choose_slope_widget", choose_slope_widget);
        this.$el.html(choose_slope_widget.render());
        return this.el;
      },

      /**
       * Displays the slope workflow, where users define their slope, and the simulation params.  Most of the work
       * is done in each workflow tab.
       *
       * We don't need a template or any extra HTML here, we just directly return the workflows output to the parent.
       *
       * @param {object} params URL parameters
       * @returns {DOM element}
       */
      edit_params_action: function(params) {
        // Default tab = "define"
        if (typeof(params) === "undefined"){
          params = ["define"];
        }

        // If we have changed from another action, remove the old view and create the workflow.
        if (this.action !== "edit_params") {
          this.empty();

          this.slope_workflow = new SlopeWorkflow({
            models: this.models
          });

          // This line is how the workflow gets rendered into the DOM.  Any other workflow changes can then
          // modify the existing DOM elements without needing to be re-rendered.
          this.$el.html(this.slope_workflow.render());
          this.action = "edit_params";
        }
        // Similar to the router calling the view, we generate the workflow method name to call...
        var action = params[0];
        var p = params.slice(1);

        if(action.indexOf('?') !== -1){
          var actionSplit = action.split('?');
          action = actionSplit[0];
          p = p.concat(actionSplit[1]);
        }
        action = action.replace('-','_');

        var tab_method = "show_" + action;
        // ...call it...
        this.slope_workflow[tab_method](p);
        // ...and then return our content.
        return this.el;
      }
    });
  }
);
