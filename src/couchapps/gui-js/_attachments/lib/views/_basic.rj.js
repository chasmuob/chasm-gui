define([], function() {
 /** 
  * Base view from which all other views (forms, widgets, dialogs etc) inherit.
  * 
  * Views *should not* insert or modify anything in the global DOM, and should be created *without* specifying
  * an "el" element.  All rendering should be done within this.el.  Calling render() should return this.el to the 
  * calling function, which is responsible for inserting the view content into the DOM, or more likely, into 
  * it's own view.  i.e. in the calling function:
  *   this.$el.find("#subView").html(new subView().render());
  *
  * This avoids problems caused when remove() is called.  If an "el" in the parent view is specified on creation, 
  * calling "remove()" on the view will also remove the parent element which means future sub views won't 
  * have the it in the DOM to be rendered into.
  * 
  * If a view needs to clear itself, call "this.empty()" rather than "this.remove()".  "empty()" will remove any 
  * subviews and empty this.el, but leave it in the DOM still so the view can draw new elements.  "remove()" takes
  * it out the DOM entirely.
  * 
  * If a view creates subviews, it must call:
  *   this.add_subview(new_view);
  * This adds the subview to a list, and is used when remove() is called on the parent in order to stopListening() and
  * remove() the subviews to avoid zombie events.
  */
 
  return Backbone.View.extend({
    
    // Backbone View inheritance:  Because of the way Javascript prototype inheritance and Backbone.extend
    // work, objects that are put *outside* of functions (i.e. here), apply to the prototype, which makes them
    // common to *all* objects that extend from here.   This may or may not be what we want, although it does provide a 
    // convienient way to store some global state.
    // 
    // If we want subclasses to have their own copies (as in the classic OOP sense), they need to be created inside
    // of the initialize function, and then subclasses must explicitly call this.  i.e. a sub class must do:
    // BasicView.extend({
    //  initialize : function(options) {
    //    BasicView.prototype.initialize.apply(this,[options]);
    //    this.foo = "bar";
    //    .... 
    //    }
    //  });
   
    initialize : function(options) {
      // Add events to the view
      // Other views can listen to this view - "listenTo(subviewName, "customEventName", action)".  
      // The subview view can then trigger an event - "this.trigger("customEventName")"
      _.extend(this, Backbone.Events);
      this.subviews = {};
    },

    /**
     * Add a view to the subview object hash.  If called as "add_subview(name, view)" then the view can be retrieved 
     * using get_subview.  If called as "add_subview(view)", it will be added without a name.  Either way, when
     * "remove()" is called on the parent, "remove()" is called on each subview in turn.
     * 
     * @param {string|object|function} name or view
     * @param {object|function} view
     * @returns void
     */
    add_subview : function(name,view) {
      if (typeof(name) === "string") {
        this.subviews[name] = view;
      } else if ( (typeof(name) === "function") || (typeof(name) === "object") ) {
        this.subviews[Mossaic.utils.UUID] = view;
      }
    },
    
    /**
     * Return the specified subview, or undefined if it doesn't exist.
     * @param {string} name
     * @returns {Backbone.View}
     */        
    get_subview : function(name) {
      return this.subviews[name];
    },
    

    /**
     * Render the view into this.el.  DOES NOT modify the global DOM.  The parent is responsible for calling 
     * render() to get the output, and then inserting that into the DOM.
     * 
     * This avoids modifying any DOM elements outside of the view
     * @instance
     */
    render: function() {
      return this.el;
    },
            
    /**
     * Adds the supplied subview to the subview object list, and calls render() on the subview to insert 
     * it's content into the DOM element specified by "selector".
     * 
     * @param {type} selector A jQuery selector to insert the view into.  It is scoped within this view.
     * @param {type} subview The view to add
     * @returns void;
     */
    render_subview : function(selector, subview) {
      this.add_subview(subview);
      this.$el.find(selector).html(subview.render());
    },

    /**
     * Removes the named subview.
     * 
     * @param {type} name
     * @returns void
     */
    remove_subview: function(name) {
      if (typeof(this.subviews[name])==="object") {
        this.subviews[name].remove();
      }
    },
            
    /** 
     * Remove only subviews from the DOM.  Stops any model listeners to avoid zombie events.
     * @returns void.
     */
    remove_subviews: function() {
      var that = this;
      _.each(this.subviews, function(subview, name) {
        subview.remove();
        delete that.subviews[name];
      });
    },
    
    /** 
     * Remove the view and any subviews from the DOM.  Stops any listeners.  
     * Because "el" has been removed from the DOM, the view won't be able to modify 
     * the DOM any more.  Generally to be called by a views parent rather then the view itself.
     * 
     * @returns void.
     */    
    remove: function() {
      this.remove_subviews();
      this.stopListening();
      this.$el.remove();
    },
    
    /** 
     * Removes subviews, stops listening to events, and empties the view,  Does *not* remove the view
     * element from the DOM, so the view can still modify the DOM.  Generally to be called by the view on itself
     * rather than being called by parent views.  (They should call "remove()" because they can reinsert the view if
     * needed)
     * 
     * @returns void
     */
    empty: function() {
      this.remove_subviews();
      this.stopListening();
      this.$el.html("");
    },
      
  });
 });