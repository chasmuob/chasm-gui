define([
  "workflows/slope/_basic.rj",
  "forms/workflows/slope/boundary_conditions.rj"
], function(
  BasicSlopeWorkflow,
  BoundaryConditionsForm
) {
  return BasicSlopeWorkflow.extend({
    template: "workflows._slope.boundary_conditions",
            
    initialize: function(options) {
      BasicSlopeWorkflow.prototype.initialize.apply(this,[options]);
      this.model = options.model;   //BoundaryConditions model
      this.events = _.extend({},BasicSlopeWorkflow.prototype.events,this.events);
      this.$el.mustache(this.template, {},{method:"html"});

      this.form = new BoundaryConditionsForm({
        model: this.model,          
      });
    },

    render: function() {
      this.$el.find("#boundary_conditions_form").html(this.form.render(),{}, {method:"html"});
      return this.el;
    }
    
  });

});