define(["views/_basic.rj"],function(BasicView) {
   
  /**
   * Base workflow tab that others inherit from.  It manages the navigation - Next/Prev buttons - as follows:
   *  1) Event handlers are attached here to each button.  
   *  2) The next() event handler calls "finish()".  If needed this should be over riden by each tab, this way the 
   *     tab can be notified when it is time to move on.  The tab can then validate it's data, update the model, and if 
   *     all OK then return true.  If the tab refuses to move on (e.g. validation fails), it should return false.
   *     If a tab has subviews/forms, it should do a similar thing on them, that way the return value cascades up the 
   *     views.
   *  3) The "prev()" event handler calls "cancel()".  Similar to above, subviews can override it with code to clean
   *     up before they are removed by the parent.  They must return a boolean, and really should only ever return true
   *     otherwise our navigation can get stuck.
   */
    return BasicView.extend({
      
    events: {
      "click .btn.next" : "next",
      "click .btn.prev" : "prev"
    },

    initialize : function(options) {
      BasicView.prototype.initialize.apply(this, [options]);
      this.model = options.model;
      this.slope_display = options.slope_display;
      this.tabs_widget = options.tabs_widget;
    },

    finish : function() {
      // Subclasses should override this to carry out actions when "next" is clicked but before we 
      // change tabs.  
      // Return true if the tab is OK to move on (data validates etc) otherwise return false to stay on the tab
      return true;
    },
            
    cancel: function() {
      return true;
    },
    
    next: function() {
      if (this.finish())  this.trigger("next");
    },
            
    prev: function() {
      if (this.cancel()) this.trigger("prev");
    },
      
    });
});