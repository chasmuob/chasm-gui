define([
  "workflows/slope/_basic.rj",
  "forms/workflows/slope/stability.rj"
], function(
  BasicSlopeWorkflow,
  StabilityForm
) {
  return BasicSlopeWorkflow.extend({
    template: "workflows._slope.stability",

    initialize: function(options) {
      BasicSlopeWorkflow.prototype.initialize.apply(this,[options]);
      this.events = _.extend({},BasicSlopeWorkflow.prototype.events,this.events);

      this.model = options.model;   // Stability model
      // this.model.auto_set_values();
      this.stability_display = options.stability_display;

      this.$el.mustache(this.template, {},{method:"html"});

      this.form = new StabilityForm({
        model: this.model,
        stability_display: this.stability_display
      });
    },

    render: function() {
      this.$el.find("#stability_form").html(this.form.render());
      return this.el;
    }

  });

});
