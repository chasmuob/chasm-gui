define([
  "models/task.rj",
  "workflows/slope/_basic.rj"
], function(
  TaskModel,
  BasicSlopeWorkflow
) {
  return BasicSlopeWorkflow.extend({
    template: "workflows._slope.confirmation",

    events: {
      "click #submit": "submit_task",
      "change input": "update_model",
      "blur input": "update_model"
    },


    initialize: function(options) {
      BasicSlopeWorkflow.prototype.initialize.apply(this, [options]);
      this.events = _.extend({}, BasicSlopeWorkflow.prototype.events, this.events);
      this.slope_display.hide();

      this.$el.mustache(this.template, {}, {
        method: "html"
      });

      this.models = options.models;

      this.task = new TaskModel({});
      this.task.models = this.models; // Don't include this when we create the task, because although the task
      // needs access to the models, we don't want them in the task.attributes.


      this.default_length = 0;
      this.min_length = 0;

      var storms = this.models.boundary_conditions.get('storm') || [];
      if (storms.length) {
        this.min_length = _.max(_.map(storms, function(storm) {
          return storm.start.value + (storm.duration || 24);
        }));
        this.default_length = this.min_length + 168;
      }
      if(!this.task.has('simulation_parameters.length')){
        this.task.set('simulation_parameters.length',this.default_length);
      }

      this.params = {
        meta: this.models.meta.get("name"),
        cross_section: this.models.cross_section.get("name"),
        geometry: this.models.geometry.get("name"),
        soil_stochastic: this.models.soils.is_stochastic() ? true : false,
        soil_deterministic: this.models.soils.is_stochastic() ? false : true,
        boundary_conditions_stochastic: this.models.boundary_conditions.is_stochastic() ? true : false,
        boundary_conditions_deterministic: this.models.boundary_conditions.is_stochastic() ? false : true,
        storm_count: this.models.boundary_conditions.get_storm_count(),
        default_length: this.default_length,
        min_length: this.min_length,
        executables: []
      };
      this.update_executable_list();
    },

    prev: function() {
      this.slope_display.show();
      BasicSlopeWorkflow.prototype.prev.apply(this);
    },

    render: function() {
      this.$el.mustache(this.template, this.params, {
        method: "html"
      });
      this.check_status();
      return this.el;
    },

    update_executable_list: function() {
      var that = this;
      var dbname = Mossaic.db.get_db_name({
        type: "release"
      });
      var db = $.couch.db(dbname);
      db.view("release/releases", {
        success: function(results) {
          for (var i in results.rows) {
            var row = results.rows[i];
            var executable = row.key.split("_");
            if (executable[0] === that.models.geometry.mode) {
              that.params.executables.push({
                id: row.id,
                application: executable[0],
                version: executable[1]
              });
            }
          }

          if (!that.task.has("executable") && that.params.executables.length > 0) {
            that.task.set({
              executable: {
                application: that.params.executables[0].application,
                version: that.params.executables[0].version
              }
            });
          }
          that.render(); //TODO(1) - Only redraw the select list here, not the entire form.
        },
        error: function(response) {
          alert("Could not fetch application list");
        }
      });
    },
    save_models: function() {
      var that = this;
      _.each(this.models, function(model) {
        Mossaic.utils.quietly_save_model(model);
      });
    },

    submit_task: function(event) {
      event.preventDefault();
      event.stopImmediatePropagation();
      this.task.submit_task();
    },

    update_model: function(ev) {
      var target = $(ev.currentTarget);
      var name = target.attr("name");
      var val = target.val();
      this.task.set(name, val);
      this.check_status();
    },

    check_status: function() {
      var attrs = this.task.attributes;

      if (attrs.name &&
        attrs.simulation_parameters &&
        attrs.simulation_parameters.length &&
        +attrs.simulation_parameters.length > this.min_length &&
        attrs.time_step.value) {
        $('#submit').removeAttr('disabled');
      } else {
        $('#submit').attr('disabled', 'disabled');
      }
    }
  });

});
