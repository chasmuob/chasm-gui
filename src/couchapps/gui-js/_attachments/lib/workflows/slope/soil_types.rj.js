define([
  "workflows/slope/_basic.rj",
  "forms/workflows/slope/soil_types.rj"
], function(
  BasicSlopeWorkflow,
  SoilTypesForm
) {
  return BasicSlopeWorkflow.extend({
    template: "workflows._slope.soil_types",

    initialize: function(options) {
      BasicSlopeWorkflow.prototype.initialize.apply(this,[options]);
      this.soils = options.soils;
      this.model = options.model;   // This is the GEOMETRY (not soil) model

      this.events = _.extend({},BasicSlopeWorkflow.prototype.events,this.events);
      this.$el.mustache(this.template, {},{method:"html"});

      this.form = new SoilTypesForm({
        layers: this.model.layers,  // Historic baggage?  I don't think the form uses this in a meaningful way.
        model: this.model,          // Geometry model, contains soils strata (i.e. measurements) but not types
        stochastic: false,
        fixed_layers: true,
        mode: this.model.mode,
        soils: this.soils           // Soils (types) model.  Will be empty here, the form will populate it.
      });
    },

    cancel: function() {
      // var geometry = _.clone(this.model.get("geometry"));
      // delete(geometry.mesh_data);
      // this.model.set("geometry", geometry);
      return true;
    },

    render: function() {
      this.$el.find("#soil_types_form").html(this.form.render());
      return this.el;
    }

  });

});
