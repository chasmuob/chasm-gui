define([
  "workflows/slope/_basic.rj",
  "forms/workflows/slope/water_table.rj"
], function(
  BasicSlopeWorkflow,
  WaterTableForm
  ) {
  return BasicSlopeWorkflow.extend({
    template: "workflows._slope.soil_strata",
    events : {
      "click #end_boreholes_btn" : function() {
        this.slope_display.end_mode("draw_water_table_boreholes");
        this.slope_display.set_mode("draw_water_table");
        this.$el.find(".next").enable();
      }
    },

    initialize: function(options) {
      BasicSlopeWorkflow.prototype.initialize.apply(this, [options]);
      this.events = _.extend({},BasicSlopeWorkflow.prototype.events,this.events);
      this.$el.mustache(this.template, {},{method:"html"});
    },


    choose: function() {
      this.$el.find("#soil_strata_form").mustache("forms._workflow._slope._soil_strata.choose",
        {name:"\"Water Table\""},
        {method:"html"}
      );
      this.$el.find("#draw_strata_btn").click(function() {
        Mossaic.utils.change_page("draw");
      });
      this.$el.find("#type_strata_btn").click(function() {
        Mossaic.utils.change_page("edit");
      });
      this.slope_display.end_mode("show_onscreen_coords");
      return this.render();
    },

    draw: function() {
      var that = this;

      // We disable the "next" button until they have completed the strata
      this.$el.find(".next").prop("disabled","disabled");

      this.slope_display.set_mode("draw_water_table_boreholes");

      var end_boreholes_btn = $("<button class='btn' id='end_boreholes_btn' >End Adding Boreholes</button>");
      end_boreholes_btn.click(function() {
        end_boreholes_btn.remove();
        that.slope_display.end_mode("draw_water_table_boreholes");
        that.slope_display.set_mode("draw_water_table", {
          on_strata_ended: function() {
            that.slope_display.save_water_table_to_model();
            that.slope_display.end_mode("draw_water_table");
            that.model.set_complete("water_table");
            Mossaic.utils.change_page("edit");
          }
        });
      });

      this.$el.find("#extra_buttons").html(end_boreholes_btn);
      return this.render();
    },

    edit: function() {
      var water_table_form = new WaterTableForm({
        model: this.model,
        buttons: {
          next: this.$el.find(".next"),
          prev: this.$el.find(".prev")
        }
      });
      this.slope_display.end_mode("show_onscreen_coords");
      this.add_subview("subview", water_table_form);
      this.$el.find("#soil_strata_form").html(water_table_form.render());
      return this.render();
    },

    display: function() {
      if (!this.model.get_complete("water_table")) {
         Mossaic.utils.change_page("choose");
      } else {
        return this.edit();
      }
    },



    finish: function() {
      var geometry = this.model.get('geometry');
      geometry.mesh_data = this.model.generate_chasm_mesh();
      this.model.set("geometry", geometry);
      this.slope_display.end_mode("show_onscreen_coords");
      this.model.set_complete("water_table");
      return true;
    },

    render: function() {
      return this.el;
    }
  });
});
