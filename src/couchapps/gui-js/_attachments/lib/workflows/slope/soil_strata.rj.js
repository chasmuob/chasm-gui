define([
  "workflows/slope/_basic.rj",
  "forms/workflows/slope/soil_strata.rj"
], function(
  BasicSlopeWorkflow,
  SoilStrataForm
  ) {
  return BasicSlopeWorkflow.extend({
    template: "workflows._slope.soil_strata",
    events : {
      "click #end_boreholes_btn" : function() {
        this.slope_display.end_mode("draw_soil_boreholes");
        this.slope_display.set_mode("draw_soils");
        this.$el.find(".next").enable();
      }
    },
    
    initialize: function(options) {
      BasicSlopeWorkflow.prototype.initialize.apply(this, [options]);
      this.events = _.extend({},BasicSlopeWorkflow.prototype.events,this.events);
      this.$el.mustache(this.template, {},{method:"html"});
      this.mode = null;   // Mode (screen) we are in.  Set by fns below.
    },
    
                  
    choose: function() {
      this.mode="choose";
      this.$el.find("#soil_strata_form").mustache("forms._workflow._slope._soil_strata.choose", 
        {name:"\"Soil Strata\""},
        {method:"html"}
      );
      
      this.$el.find("#draw_strata_btn").click(function() {
        Mossaic.utils.change_page("draw");
      });
      this.$el.find("#type_strata_btn").click(function() {
        Mossaic.utils.change_page("edit");
      });
      return this.render();
    },
            
    draw: function() {
      var that = this;
      this.mode = "draw";

      // We disable the "next" button until they have completed the strata
      this.$el.find(".next").prop("disabled","disabled");
      
      this.slope_display.set_mode("draw_soil_boreholes");
      this.slope_display.set_mode("show_onscreen_coords");

      // TODO - replace this with a button widget
      var $end_boreholes_btn = $("<button class='btn' id='end_boreholes_btn' >End Adding Boreholes</button>");
      $end_boreholes_btn.click(function() {
        that.slope_display.end_mode("draw_soil_boreholes");
        that.slope_display.set_mode("draw_soils");
        that.$el.find("#extra_buttons").html($end_strata_btn); // Change the button to "End Strata"
      });

      var $end_strata_btn = $("<button class='btn' id='end_strata_btn' >End Drawing Soil Strata</button>");
      $end_strata_btn.click(function() {
          that.slope_display.save_soils_to_model();
          that.slope_display.end_mode("draw_soils");
          that.model.set_complete("soil_strata");
          Mossaic.utils.change_page("edit");
      });
      
      this.$el.find("#extra_buttons").html($end_boreholes_btn);
      return this.render();
    },
    
    edit: function() {
      this.mode = "edit";
    
      var soil_strata_form = new SoilStrataForm({
        model: this.model,
        buttons: {
          next: this.$el.find(".next"),
          prev: this.$el.find(".prev")
        }
      });
      this.slope_display.end_mode("show_onscreen_coords");
      this.add_subview("subview", soil_strata_form); 
      this.$el.find("#soil_strata_form").html(soil_strata_form.render());
      return this.render();
    },
         
    display: function() {
      if (!this.model.get_complete("soil_strata")) {
         Mossaic.utils.change_page("choose");
      } else {
        return this.edit();
      }
    },
    
    finish: function() {
      this.model.set_complete("soil_strata");
      return true;
    },
            
    render: function() {
      return this.el;
    }
  });
});
