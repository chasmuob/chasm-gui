define([
  "forms/workflows/slope/define/choose.rj",
  "forms/workflows/slope/define/edit.rj",
  "workflows/slope/_basic.rj",
], function(
  ChooseDefineSlopeWorkflowForm,
  EditDefineSlopeWorkflowForm,
  BasicSlopeWorkflow
) {
  /**
   * Tab for users to define their slope.  Users can choose to draw their slope freehand, or type numerical data in.
   * URL routing is used to set which action below is called.  When an action is complete, it saves any data, and
   * does a URL redirect to the next action.  This way the browser back buttons continue to work.
   *
   * Actions either set modes on the slope graphic (the drawing ones), or create forms as subviews.
   *
   */

  return BasicSlopeWorkflow.extend({
    template: "workflows._slope.define", // The subsections have their own templates.

    initialize: function(options) {
      BasicSlopeWorkflow.prototype.initialize.apply(this,[options]);
      this.events = _.extend({},BasicSlopeWorkflow.prototype.events,this.events);
      this.$el.mustache(this.template, {},{method:"html"});
      this.$el.find("button.prev").remove();
      this.$el.find("button.next").enable(false);
    },


    choose_action: function() {
      this.remove_subview("subview");
      var subview = new ChooseDefineSlopeWorkflowForm({
        slope_display: this.slope_display,
        model: this.model,
        on_end: function(method) {
          Mossaic.utils.change_page(method);
        }
      });
      this.slope_display.end_mode("show_onscreen_coords");
      this.add_subview("subview", subview);
      this.$el.find("#slope_definition_form").html(subview.render());
      return this.render();
    },

    draw_action: function() {
      this.remove_subview("subview");
      this.slope_display.redraw();
      this.slope_display.set_mode("show_onscreen_coords");
      this.slope_display.set_mode("draw_surface", {
        callback: function() {
          Mossaic.utils.change_page("set_datum");
        }
      });
      this.slope_display.show();
      return this.render();
    },

    edit_action: function() {
      this.remove_subview("subview");
      this.slope_display.hide();
      var subview = new EditDefineSlopeWorkflowForm({
        slope_display: this.slope_display,
        model: this.model,
      });
      var $set_datum_btn = $("<button class='btn' id='set_datum_btn' >Set Datum</button>");
      $set_datum_btn.click(function() {
        set_datum_btn.remove();
        Mossaic.utils.change_page("set_datum");
      });
      this.$el.find("#extra_buttons").html($set_datum_btn);
      this.add_subview("subview", subview);
      this.$el.find("#slope_definition_form").html(subview.render());
      return this.render();
    },


   set_datum_action: function() {
      var that = this;
      this.remove_subview("subview");
      this.slope_display.redraw();
      this.slope_display.set_mode("show_onscreen_coords");
      this.slope_display.set_mode("draw_datum", {
        callback: function() {
          that.model.set_complete("surface");
          Mossaic.utils.change_page("display");
        }
      });
      this.slope_display.show();
      return this.render();

    },

    display_action: function() {
      if (!this.model.get_complete("surface")) {
         Mossaic.utils.change_page("choose");
      } else {
        this.remove_subview("subview");
        this.slope_display.show();
        this.slope_display.end_all_modes();
        this.slope_display.set_mode("show_onscreen_coords");
        this.slope_display.set_mode("edit_surface");
        var subview = new EditDefineSlopeWorkflowForm({
          slope_display: this.slope_display,
          model: this.model,
        });
        this.add_subview("subview", subview);
        this.$el.find("button.next").enable();
        this.$el.find("#slope_definition_form").html(subview.render());
        return this.render();
      }
    },

    render: function() {
      return this.el;
    }
  });

});
