define([
  "widgets/tabs.rj",
  "workflows/_basic.rj",
  "workflows/slope/define.rj",
  "workflows/slope/soil_strata.rj",
  "workflows/slope/water_table.rj",
  "workflows/slope/soil_types.rj",
  "workflows/slope/boundary_conditions.rj",
  "workflows/slope/stability.rj",
  "workflows/slope/confirmation.rj",
  "vis/slope.rj",
  "vis/stability.rj"
], function(
  TabsWidget,
  BasicWorkflow,
  DefineSlopeWorkflow,
  SoilStrataSlopeWorkflow,
  WaterTableSlopeWorkflow,
  SoilTypesSlopeWorkflow,
  BoundaryConditionsSlopeWorkflow,
  StabilitySlopeWorkflow,
  ConfirmationSlopeWorkflow,
  SlopeVis,
  StabilityVis
) {

  /**
   * Slope workflow for editing slope paramaters (shape, soil strata, water table), and simulation paramaters (soil
   * types, boundary conditions, stability search).
   *
   * The base template contains:
   *  (a) Tabs menu
   *  (b) Slope graphic
   *  (c) Tab content
   *
   * (a) are (b) are set when initialized, although "show_<tab>" methods can modify them.
   * (c) is set by each "show_<tab>" method, and is called by routing from the URL.
   *
   * Each workflow tab is a seperate JS Backbone view.  When created, the subview is added to the subviews list
   * (and named "workflow_form").
   *
   * Each "show_<tab>" method removes the existing subview (if any), creates a new subview of it's own, and renders()
   * it into "#workflow_form", which is in this views template.
   *
   * We don't need to "return this.el" in each "show" method, becase the parent view calls the default "render()" method
   * at the beginning of the workflow, which puts this.el into the DOM.  Each "show" method can then just modify this.el
   * and it will appear in the DOM.
   *
   * The workflow navigation - Next / Prev buttons - are rendered by the subviews, but the "show_<tab>" methods below
   * need to know when to swap tabs.  So each "show_<tab>" method below listensTo() "next" or "prev" events
   * on it's subview.  The subview triggers these events on itself when it is ready to move on.  The "show_<tab>"
   * methods below can then swap the tabs.
   *
   *
   * MODEL INFORMATION
   * In order to avoid changing any backend (python) scripts, this workflow is maintaining the same output models
   * as the old one.  This means the output is somewhat non-intuitive.
   * The "Define Slope", "Soil Strata" and "Water Table" tabs all modify/save their information into the geometry model.
   * The others all have their own separate models.
   * When the workflow is submitted, a document is created in "map_task" DB that references the IDs of these models.
   */
  return BasicWorkflow.extend({
    template: "workflows.slope",


    initialize: function(options) {
      var that = this;
      BasicWorkflow.prototype.initialize.apply(this, [options]);
      // The tabs populate each model with data entered by the user.  At the end, all the models are saved and the
      // task is submitted.

      this.models = options.models;

      // There are still listeners (from the LS widget) hanging around.  A better way would be to rewrite the
      // LS widget to use Backbone listenTo(), but this is quicker (if dirtier, possible mem leak still?)
      this.models.meta.unbind();
      this.models.cross_section.unbind();
      this.models.geometry.unbind();
      this.models.stability.unbind();

      this.$el.mustache(this.template, {}, {
        method: "html"
      });

      // TODO(2) - automate this so we don't need to hardcode it.
      // this.base_url = "#task-composer/edit-params/";
      this.base_url = '#task-composer/edit-params/id/' + this.models.geometry.get('id') + '/';

      // Slope graphic
      this.slope_display = new SlopeVis({
        model: this.models.geometry,
        max_width: 600, // It would be nice to provide this dynamically based on the width of the parent element, but at
        // this point we aren't rendered into the DOM, so don't know the width, and the vis needs the width
        // up front, so need to hardcode it.  So there.
        max_height: 400 // The maximum height
      });

      this.render_subview("#workflow_slope_display", this.slope_display);

      this.stability_display = new StabilityVis({
        model: this.models.stability,
        models: this.models,
        slope_display: this.slope_display,
        geometry: this.models.geometry,
        quantize: false
      });

      // Tabs
      this.tabs = [{
        name: "Define Slope",
        link: "define",
        enabled: true
      }, {
        name: "Soil Strata",
        link: "soil-strata",
        enabled: false
      }, {
        name: "Water Table",
        link: "water-table",
        enabled: false
      }, {
        name: "Soil Types",
        link: "soil-types",
        enabled: false
      }, {
        name: "Boundary Conditions",
        link: "boundary-conditions",
        enabled: false
      }, {
        name: "Stability",
        link: "stability-search",
        enabled: false
      }, {
        name: "Confirmation",
        link: "confirmation",
        enabled: false
      }];
      this.tabs_widget = new TabsWidget({
        base_url: this.base_url,
        tabs: this.tabs
      });
      this.tabs_widget.set_active_tab("define");
      this.tabs_widget.on('transitioning', function() {
        if(this.activeCb){
          this.activeCb();
        }

        if (this.activeModel) {
          this.save_model(this.activeModel);
        }
      }, this);
      this.render_subview("#workflow_tabs", this.tabs_widget);
    },

    save_model: function(model) {
      return new Promise(function(resolve, reject) {
        model.save(null, {
          success: resolve,
          error: reject
        });
      });
    },

    save_models: function() {
      return Promise.all(_.map(this.models, function(model) {
        return this.save_model(model);
      }, this));
    },

    set_title: function(title) {
      this.$el.find("#workflow_title").html(title);
    },

    saveAndTransition: function(form, evt, path, model, cb) {
      var that = this;
      var dest = that.base_url + path;
      this.activeModel = model;
      this.activeCb = cb;
      this.listenTo(form, evt, function() {
        if(cb){
          cb();
        }
        if (model) {
          that.save_model(model).then(function() {
            window.location = dest;
          });
        } else {
          window.location = dest;
        }
      });
    },

    configureTabs: function() {
      this.tabs_widget.enable_all_tabs();
      if (this.task_mode) {
        this.tabs_widget.disable_tab('define');
        this.tabs_widget.disable_tab('soil-strata');
        this.tabs_widget.disable_tab('water-table');
      }
    },

    navigate: function(action, params) {
      window.location = this.base_url + action + (params.length ? '?' + params.join('&') : '');
    },

    show_define: function(params) {
      if (params.length === 0) {
        window.location = window.location.href + "/display";
        return;
      }
      var action = params[0] + "_action";
      var that = this;
      this.slope_display.disable_cells();
      // this.tabs_widget.disable_all_tabs();
      this.tabs_widget.set_active_tab("define");
      this.remove_subview("workflow_form");
      this.set_title("Define Slope");

      if (!this.models.geometry.has('id')) {
        this.tabs_widget.disable_all_tabs();
        this.tabs_widget.enable_tab('define');
      }
      var workflow_form = new DefineSlopeWorkflow({
        slope_display: this.slope_display,
        model: this.models.geometry,
        tabs_widget: this.tabs_widget // The tabs widget is passed in so the form can [dis/en]able certain tabs
      });

      this.listenTo(workflow_form, "next", function() {
        this.save_model(this.models.geometry).then(function(response) {
          if (!that.models.geometry.has('id')) {
            that.models.set('id', response.id);
            that.models.set('_id', response.id);
          }
          that.base_url = '#/task-composer/edit-params/id/' + response.id + '/';
          that.tabs_widget.base_url = that.base_url;
          that.tabs_widget.enable_all_tabs();
          window.location = that.base_url + "soil-strata";
        });
      });

      this.add_subview("workflow_form", workflow_form);
      this.$el.find("#workflow_form").html(workflow_form[action]());
    },

    show_soil_strata: function(params) {
      // We want to explicitily force a default action onto the URL otherwise redirects from within the action
      // can break because the URL level varies: i.e. /soil-strata != /soil-strata/
      // TODO: Unfortunately this breaks the back button....
      if (params.length === 0) {
        window.location = window.location.href + "/display";
        return;
      }
      var action = params[0];

      this.slope_display.disable_cells();
      this.slope_display.show();
      // this.tabs_widget.disable_all_tabs();
      this.tabs_widget.set_active_tab("soil-strata");
      this.remove_subview("workflow_form");
      this.set_title("Soil Strata");

      // We don't need to maintain any state in the Subworkflow/view if the URL changes, so can just create
      // it new each time.
      var workflow_form = new SoilStrataSlopeWorkflow({
        slope_display: this.slope_display,
        model: this.models.geometry,
        tabs_widget: this.tabs_widget
      });
      this.slope_display.disable_cells();

      this.saveAndTransition(workflow_form, "prev", "define", this.models.geometry);
      this.saveAndTransition(workflow_form, "next", "water-table", this.models.geometry);

      this.add_subview("workflow_form", workflow_form);
      this.$el.find("#workflow_form").html(workflow_form[action]());
    },

    show_water_table: function(params) {
      // We want to explicitily force a default action onto the URL otherwise redirects from within the action
      // can break because the URL level varies: i.e. /soil-strata != /soil-strata/
      if (params.length === 0) {
        window.location = window.location.href + "/display";
        return;
      }
      var action = params[0];
      this.slope_display.disable_cells();
      this.slope_display.show();
      // this.tabs_widget.disable_all_tabs();
      this.tabs_widget.set_active_tab("water-table");
      this.remove_subview("workflow_form");
      this.set_title("Water Table");

      var workflow_form = new WaterTableSlopeWorkflow({
        slope_display: this.slope_display,
        model: this.models.geometry,
        tabs_widget: this.tabs_widget
      });
      this.slope_display.disable_cells();


      this.saveAndTransition(workflow_form, "prev", "soil-strata", this.models.geometry);
      this.saveAndTransition(workflow_form, "next", "soil-types", this.models.geometry);

      this.add_subview("workflow_form", workflow_form);
      this.$el.find("#workflow_form").html(workflow_form[action]());
    },

    show_soil_types: function(params) {
      this.slope_display.enable_cells();
      // this.tabs_widget.disable_all_tabs();
      this.slope_display.show();
      this.tabs_widget.set_active_tab("soil-types");
      this.remove_subview("workflow_form");
      this.set_title("Soil Types");

      var workflow_form = new SoilTypesSlopeWorkflow({
        slope_display: this.slope_display,
        soils: this.models.soils,
        model: this.models.geometry,
        tabs_widget: this.tabs_widget
      });

      this.saveAndTransition(workflow_form, "prev", "water-table", this.models.soils);
      this.saveAndTransition(workflow_form, "next", "boundary-conditions", this.models.soils);

      this.add_subview("workflow_form", workflow_form);
      this.$el.find("#workflow_form").html(workflow_form.render());
    },

    show_boundary_conditions: function(params) {
      // this.tabs_widget.disable_all_tabs();
      this.tabs_widget.set_active_tab("boundary-conditions");
      this.slope_display.show();
      this.remove_subview("workflow_form");
      this.set_title("Boundary Conditions");

      var workflow_form = new BoundaryConditionsSlopeWorkflow({
        slope_display: this.slope_display,
        model: this.models.boundary_conditions,
        tabs_widget: this.tabs_widget
      });
      this.saveAndTransition(workflow_form, "prev", "soil-types", this.models.boundary_conditions);
      this.saveAndTransition(workflow_form, "next", "stability-search", this.models.boundary_conditions);

      this.add_subview("workflow_form", workflow_form);
      this.$el.find("#workflow_form").html(workflow_form.render());
    },

    show_stability_search: function(params) {
      // this.tabs_widget.disable_all_tabs();
      this.tabs_widget.set_active_tab("stability-search");
      this.remove_subview("workflow_form");
      this.slope_display.show();
      this.slope_display.enable_cells();
      this.set_title("Stability Search");

      var workflow_form = new StabilitySlopeWorkflow({
        slope_display: this.slope_display,
        stability_display: this.stability_display,
        model: this.models.stability,
        tabs_widget: this.tabs_widget
      });

      var stab = this.stability_display;
      function hide() {
        stab.hide();
      }

      this.saveAndTransition(workflow_form, "prev", "boundary-conditions", this.models.stability, hide);
      this.saveAndTransition(workflow_form, "next", "confirmation", this.models.stability, hide);

      this.add_subview("workflow_form", workflow_form);
      this.$el.find("#workflow_form").html(workflow_form.render());
    },

    show_confirmation: function(params) {
      // this.tabs_widget.disable_all_tabs();
      this.tabs_widget.set_active_tab("confirmation");
      this.remove_subview("workflow_form");
      this.set_title("Confirmation");

      this.task_mode = this.task_mode || params.indexOf('task_mode=true') !== -1;
      this.configureTabs();

      var workflow_form = new ConfirmationSlopeWorkflow({
        slope_display: this.slope_display,
        stability_display: this.stability_display,
        models: this.models,
        tabs_widget: this.tabs_widget
      });

      this.listenTo(workflow_form, "prev", function() {
        window.location = this.base_url + "stability-search";
      });
      this.listenTo(workflow_form, "next", function() {
        window.location = this.base_url + "confirmation";
      });

      this.add_subview("workflow_form", workflow_form);
      this.$el.find("#workflow_form").html(workflow_form.render());
    }


  });

});
