/**
 * @file Defines a widget for listing CouchDB documents from a CouchDB view
 */
(function() {
  /**
   * Views that can be used interchangeably by ls widgets
   */
  var ls_views = {
    /**
     * Autosearch based view for ls_widget - user can enter text to search
     * data or can hit a button for a complete list.
     *
     * Based on Simon's fakesearch widget.
     *
     * @constructor
     * @inner
     * @private
     */
    AutoBox: Backbone.View.extend({
      /**
       * Create the view
       *
       * The view can be instantiated in two modes - normal and "input only".
       * In input_only mode the view will not browse data and just allow
       * text to be entered. This allows the view to be used to get a name
       * for a dataset, which is useful if this needs to be done in a way
       * that takes parent datasets into account.
       *
       * @memberof @AutoBox
       * @param {Backbone.Collection} collection Collection used to populate
       *                                         the widget.
       * @param {object} options Hash of options
       * @param {string} options.label Html to be displayed next to the widget
       * @param {boolean} options.focus Set to true if the widget should grab
       *                                focus when rendered
       * @param {object} options.listeners  Array of listener functions to be
       *                                    called when the selection changes
       * @param {string} options.divname  Id selector of the div to which this
       *                                  widget should be appended
       * @param {string} options.element_id Id selector to be used for the input
       *                                    element of this widget
       * @param {boolean} options.input_only  Set to true if the widget only
       *                                      allows data to be entered and
       *                                      does not perform searches
       */
      initialize: function(collection, options) {
        _.bindAll(this);
        _.extend(this, Backbone.Events);
        /**
         * The currently selected model
         * @memberof AutoBox
         * @instance
         * @type Backbone.Model
         */
        this.selected = {};
        /**
         * The name of the model that should be shown as selected
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.text = "";
        /**
         * The label to be displayed next to this widget
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.label = options.label;
        /**
         * True if the widget should grab focus when rendered
         * @memberof AutoBox
         * @instance
         * @type boolean
         */
        this.focus = options.focus;
        /**
         * The collection that determines what is matched in the autocomplete
         * or shown in the drop down list.
         * @memberof AutoBox
         * @instance
         * @type Backbone.Collection
         */
        this.collection = collection;
        this.collection.bind('change', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);
        this.collection.bind('reset', this.render);
        /**
         * Array of listener functions to be called whenever the selected
         * widget changes
         * @memberof AutoBox
         * @instance
         * @type array
         */
        this.listeners = options.listeners;
        /**
         * CSS selector of the div to which this widget should be appended
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.divname = options.divname || "#ls_widget";
        /**
         * The id of the input element of this widget
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.element_id = options.element_id ||
            this.divname.split("#")[1] + "_element";
        /**
         * The id of the button element (that brings the drop down list) of
         * this widget
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.button_id = options.element_id + "_button";
        /**
         * The template used to render the widget
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.template = 'widgets._ls.autobox';
        /**
         * Flag to set whether the widget should only be used for getting
         * the input in the search box
         * @memberof AutoBox
         * @instance
         * @type boolean
         */
        this.input_only = options.input_only;
        /**
         * Flag which indicates whether the widget should indicate that the
         * currently associated model is dirty
         * @memberof AutoBox
         * @instance
         * @type boolean
         */
        this.is_dirty = false;
      },
      /**
       * Get the model where model.name matches the supplied name. This is
       * a reverse lookup which will return the first match if there is more
       * than one model with a matching name.
       * @memberof AutoBox
       * @instance
       * @param {string} name Name of the model to be returned
       * @return {Backbone.Model} The first model in the collection that matches
       *                          the supplied name
       */
      name_to_model: function(name) {
        // Reverse lookup - assumes name is unique which it will be if the
        // dataset was created using this gui
        var selected_models = this.collection.filter(function(model) {
          return model.get("name") == name;
        });
        if (selected_models.length > 1) {
          console.log("More than one item matches the name " +
              name + ", using the first match.");
        } else if (selected_models.length == 0) {
          throw "Model with matching name does not exist";
        }
        return selected_models[0].toJSON();
      },
      /**
       * Indicate that there are unpersisted changes
       * @memberof AutoBox
       * @instance
       * @param {boolean} is_dirty True if the widget should indicate there are
       *                           unpersisted changes, false otherwise.
       */
      show_dirty_indicator: function(is_dirty) {
        this.is_dirty = is_dirty;
        var element = $("#" + this.element_id);
        if (this.is_dirty) {
          element.val("New dataset");
          element.addClass("dirty");
        } else {
          element.removeClass("dirty");
        }
      },
      /**
       * Show a placeholder. This is usually called when the collection is
       * fetched, so that the widget displays something to the user to let them
       * know something is happening.
       * @memberof AutoBox
       * @instance
       */
      show_placeholder: function() {
        var img = $(this.divname + " img");
        if (img.size() === 0) {
          $(this.divname + " .ls").append(
              "<img \
                style=\"float: left;position: relative; top: -30px; left: 5px;\"\
                src=\"image/ajax-loader.gif\" />");
        }
      },
      /**
       * Render the widget
       * @memberof AutoBox
       * @instance
       */
      render: function() {
        if (this.hidden) {
          $(this.divname).html("");
          return;
        }
        var that = this;
        var names = this.collection.map(function(model) {
          return model.get("name");
        });
        $(this.divname).mustache(this.template, {
          element_id: this.element_id,
          button_id: this.button_id,
          label: this.label
        },{ method: 'html' });
        var element = $("#" + this.element_id);
        if (this.input_only) {
          $("#" + this.button_id).remove();
          element.change(function() {
            that.text = element.val();
            that.selected = {name: element.val()};
          });
        } else {
          element.blur(function(event) {
            //event.preventDefault();
          });
          element.autocomplete({
            minLength: 0,
            source: names,
            change: function() {
              that.text = element.val();
            },
            select: function(event, selected) {
              that.selected = that.name_to_model(selected.item.value);
              _.each(that.listeners, function(listener) {
                listener({update_model: true});
              });
            }
          });
        }

        // Previously selected item should still be selected even though
        // items have changed
        if (this.text && !this.is_dirty) {
          element.val(this.text);
          try {
            var current_selection = this.name_to_model(this.text);
            this.selected = current_selection;
          } catch(e) {
            console.log("Current text does not match any current option");
          }
          this.text = undefined;
        } else {
          if (this.selected && !this.is_dirty) {
            element.val(this.selected.name);
          } else {
            element.val("New dataset");
            element.addClass("dirty");
          }
        }

        if (this.focus) {
          element.focus();
        }

        if (!this.input_only) {
          // Bring up all the options
          $("#" + this.button_id).click(function() {
            if (element.autocomplete("widget").is(":visible")) {
              element.autocomplete("close");
            } else {
              element.autocomplete("search", "");
              element.focus();
            }
          });
        }
      },
      /**
       * Get the currently selected model
       * @memberof AutoBox
       * @instance
       */
      get_selected: function() {
        return this.selected;
      },
      /**
       * Set the text to be shown for the default selection
       * @memberof AutoBox
       * @instance
       * @param {string} value Default selection text
       */
      set_default_selection: function(value) {
        this.text = value;
      }
    }),
    /**
     * Scrolling list view for ls_widget. Displays a list of all collection
     * models which can be filtered by typing in an associated input box.
     *
     * @constructor
     * @inner
     * @private
     */
    FilterList: Backbone.View.extend({
      /**
       * Create the view
       *
       * The view can be instantiated in two modes - normal and "input only".
       * In input_only mode the view will not browse data and just allow
       * text to be entered. This allows the view to be used to get a name
       * for a dataset, which is useful if this needs to be done in a way
       * that takes parent datasets into account.
       *
       * @memberof @FilterList
       * @param {Backbone.Collection} collection Collection used to populate
       *                                         the widget.
       * @param {object} options Hash of options
       * @param {string} options.label Html to be displayed next to the widget
       * @param {boolean} options.focus Set to true if the widget should grab
       *                                focus when rendered
       * @param {object} options.listeners  Array of listener functions to be
       *                                    called when the selection changes
       * @param {string} options.divname  Id selector of the div to which this
       *                                  widget should be appended
       * @param {string} options.element_id Id selector to be used for the input
       *                                    element of this widget
       * @param {boolean} options.input_only  Set to true if the widget only
       *                                      allows data to be entered and
       *                                      does not perform searches
       * @param {string} options.height CSS string specifying the desired height
       *                                of the scrolling list box
       */
      initialize: function(collection, options) {
        _.bindAll(this);
        /**
         * The currently selected model
         * @memberof FilterList
         * @instance
         * @type Backbone.Model
         */
        this.selected = {};
        /**
         * The name of the model that should be shown as selected
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.text = "";
        /**
         * The label to be displayed next to this widget
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.label = options.label;
        /**
         * True if the widget should grab focus when rendered
         * @memberof FilterList
         * @instance
         * @type boolean
         */
        this.focus = options.focus;
        /**
         * The collection that determines what is matched in the autocomplete
         * or shown in the drop down list.
         * @memberof FilterList
         * @instance
         * @type Backbone.Collection
         */
        this.collection = collection;
        this.collection.bind('change', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);
        this.collection.bind('reset', this.render);
        /**
         * Array of listener functions to be called whenever the selected
         * widget changes
         * @memberof FilterList
         * @instance
         * @type array
         */
        this.listeners = options.listeners;
        /**
         * CSS selector of the div to which this widget should be appended
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.divname = options.divname || "#ls_widget";
        /**
         * The id of the input element of this widget
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.element_id = options.element_id ||
        this.divname.split("#")[1] + "_element";
        /**
         * Flag to set whether the widget should only be used for getting
         * the input in the search box
         *
         * Currently not honoured by this widget
         *
         * @memberof FilterList
         * @instance
         * @type boolean
         */
        this.input_only = options.input_only;
        /**
         * Flag which indicates whether the widget should indicate that the
         * currently associated model is dirty
         * @memberof FilterList
         * @instance
         * @type boolean
         */
        this.is_dirty = false;
        /**
         * String to be used to filter the displayed items by model name
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.filter_val = "";
        /**
         * CSS string to specify the height of the widget
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.height = options.height;
      },
      /**
       * The template used to render the widget
       * @memberof FilterList
       * @instance
       * @type string
       */
      template: "widgets._ls.filterlist",
      /**
       * Show a placeholder. This is usually called when the collection is
       * fetched, so that the widget displays something to the user to let them
       * know something is happening.
       * @memberof FilterList
       * @instance
       */
      show_placeholder: function() {
        var img = $(this.divname + " img");
        if (img.size() === 0) {
          $(this.divname + " .ls").append(
              "<img \
                style=\"float: left;position: relative; top: -30px; left: 5px;\"\
                src=\"image/ajax-loader.gif\" />");
        }
      },
      /**
       * Indicate that there are unpersisted changes
       * @memberof FilterList
       * @instance
       * @param {boolean} is_dirty True if the widget should indicate there are
       *                           unpersisted changes, false otherwise.
       */
      show_dirty_indicator: function(is_dirty) {
        this.is_dirty = is_dirty;
        var element = $("#" + this.element_id);
        if (this.is_dirty) {
          element.val("New dataset");
          element.addClass("dirty");
        } else {
          element.removeClass("dirty");
        }
      },
      /**
       * Set the currently selected model to the model matching the id of the
       * DOM element that triggered the event
       * @memberof FilterList
       * @instance
       * @param {jQuery.Event} event The event that triggered the function
       */
      select: function(event) {
        var that = this;
        this.default_selection = undefined;
        if (typeof(this.selected) !== "undefined" &&
            typeof(this.selected.get) !== "undefined") {
          var indexOfSelected;
          this.collection.find(function(model, i) {
            indexOfSelected = i;
            return model.get("id") === that.selected.get("id");
          });
          $("#" + this.element_id + "_" + indexOfSelected).removeClass("selected");
        }
        var target_id_tokens = event.target.id.split('_');
        var index = target_id_tokens[target_id_tokens.length - 1];
        var new_selection = this.collection.at(index);
        if (this.selected !== new_selection) {
          this.selected = this.collection.at(index);
          $("#" + event.target.id).addClass("selected");
        } else {
          this.selected = {};
        }
        _.each(this.listeners, function(listener) {
          listener({update_model: true, clear_model: true});
        });
      },
      /**
       * Filter the displayed models to only those that match the string entered
       * in the filter text box
       * @memberof FilterList
       * @instance
       */
      filter: function() {
        var that = this;
        this.filter_val = $("#" + this.element_id + "_filter").val();
        this.collection.forEach(function(row, i) {
          if (row.get("name").indexOf(that.filter_val) >= 0) {
            $("#" + that.element_id + "_" + i).css("display", "block");
          } else {
            $("#" + that.element_id + "_" + i).css("display", "none");
          }
        });
      },
      /**
       * Render the widget
       * @memberof FilterList
       * @instance
       */
      render: function(options) {
        var options = options || {};
        if (this.hidden) {
          $(this.divname).html("");
          return;
        }
        var that = this;
        var rows = _.map(this.collection.toJSON(), function(row, i) {
          row.row_id = that.element_id + "_" + i;
          row.click_class = that.element_id + "_clickable";
          row.display = "block";
          if (typeof(that.filter_val) !== "undefined" &&
              that.filter_val.length > 0) {
            if (row.name.indexOf(that.filter_val) < 0) {
              row.display = "none";
            }
          }
          return row;
        });
        $(this.divname).mustache(this.template, {
          element_id: this.element_id,
          button_id: this.button_id,
          label: this.label,
          rows: rows,
          filter_id: this.element_id + "_filter",
          filter_val: this.filter_val
        },{ method: 'html' });

        if (typeof(this.default_selection) !== "undefined" &&
            this.default_selection !== "") {
          this.selected = this.collection.find(function(model) {
            return model.get("name") === that.default_selection;
          });
          _.each(this.listeners, function(listener) {
            listener({update_model: true, clear_model: true});
          });
        }
        this.collection.forEach(function(item, i) {
          if (typeof(item.get) === "function" &&
              typeof(that.selected.get) === "function" &&
              item.get("id") === that.selected.get("id")) {
            $("#" + that.element_id + "_" + i).addClass("selected");
          }
        });
        if (typeof(options.height) !== "undefined") {
          this.height = options.height;
        }
        if (this.height !== "undefined") {
          $(this.divname + " .filterlist").css("height", this.height);
        }
        $("." + that.element_id + "_clickable").click(this.select);
        $("#" + that.element_id + "_filter").keyup(this.filter);
      },
      /**
       * Get the currently selected model
       * @memberof FilterList
       * @instance
       */
      get_selected: function() {
        return this.selected || {};
      },
      /**
       * Select a model whose name field that matches the supplied name. If
       * multiple matches exist only the first is used.
       * @memberof FilterList
       * @instance
       * @param {string} name Name of the model to be selected
       */
      select_by_name: function(name) {
        var that = this;
        var indexOfSelected;
        if (typeof(that.selected) !== "undefined" &&
            (typeof(that.selected.get) !== "undefined")) {
          this.collection.find(function(model, i) {
            indexOfSelected = i;
            return model.get("id") === that.selected.get("id");
          });
          $("#" + this.element_id + "_" + indexOfSelected).removeClass("selected");
        }
        // Find item where name matches value
        var indexToSelect;
        var to_select = this.collection.find(function(model, i) {
          indexOfSelected = i;
          return model.name === name;
        });
        $("#" + this.element_id + "_" + indexToSelect).addClass("selected");
        this.selected = to_select;
      },
      /**
       * Set the text to be shown for the default selection and select the
       * model whose name matches that text
       * @memberof FilterList
       * @instance
       * @param {string} value Default selection text
       */
      set_default_selection: function(value) {
        var that = this;
        this.default_selection = value;
        this.select_by_name(this.default_selection);
      },
      /**
       *
       */
      set_selected: function(model){
        this.selected = model;
      },
      /**
       * Unselect whatever is currently selected
       * @memberof FilterList
       * @instance
       * @param {object} options Hash of options
       * @param {boolean} options.clear_model True if the selected model should
       *                                      actually be cleared, false if it
       *                                      should just be unselected.
       */
      unselect: function(options) {
        var options = options || {};
        this.selected = {};
        this.default_selection = undefined;
        this.render();
        _.each(this.listeners, function(listener) {
          listener({update_model: true, clear_model: options.clear_model});
        });
      }
    })
  };

  /**
   * Create a single ls widget for listing documents according to certain
   * options
   * @constructor
   * @private
   * @param {object} options Hash of options
   * @param {string} options.url                Name of the CouchDB view that
   *                                            provides the documents
   * @param {string} options.key                String to be used as the
   *                                            key parameter in the CouchDB
   *                                            view request
   * @param {boolean} options.reduce            True if the reduce paramter
   *                                            should be set in the CouchDB
   *                                            view request
   * @param {function} options.success          Function to call when the
   *                                            collection is successfully
   *                                            fetched
   * @param {string} options.startkey           String to be used as the
   *                                            startkey in the CouchDB view
   *                                            request
   * @param {string} options.endkey             String to be used as the
   *                                            endkey in the CouchDB view
   *                                            request
   * @param {function} options.docChangeHandler Function to handle changes
   *                                            received via the CouchDB
   *                                            _changes feed
   * @param {string} options.urlRoot            Name of the database that
   *                                            should be queried
   * @param {string} options.ddocName           Name of the CouchDB design
   *                                            document which defines the
   *                                            view defined in options.url
   */
  var ls = function(options) {
    var r = Math.random();
    var listeners = [];
    var prefetch_listeners = [];
    var collection = new Backbone.Collection([], {
      comparator: function(model) {
        var name = model.get("name");
        if (typeof(name) !== "undefined" && name !== null) {
          return name.toLowerCase();
        } else {
          return name;
        }
      }
    });
    collection.model = Mossaic.models.map[options.type];
    collection.view = options.url;
    collection.key = options.key;
    collection.doreduce = options.reduce;
    collection.success = options.success;
    collection.startkey = options.startkey;
    collection.endkey = options.endkey;
    collection.docChangeHandler = options.docChangeHandler;
    collection.urlRoot = options.urlRoot;
    collection.ddocName = options.ddocName;

    var divname = options.divname || "#ls_widget";
    var element_id = divname.split("#")[1] + "_element";
    var View = ls_views[options.view] || ls_views.AutoBox;
    var view = new View(collection, {
      divname: divname,
      element_id: element_id,
      listeners: listeners,
      label: options.label,
      focus: options.focus,
      input_only: options.input_only,
      height: options.height
    });

    // Add to the watchList if we're using backbone-couch
    if (typeof(Backbone.couch) !== "undefined" &&
        typeof(options.type) !== "undefined" &&
        !options.ignore_changes &&
        !options.input_only) {
      Backbone.couch._watchList[options.type] = collection;
    }

    if (typeof(options.silent) === "undefined") {
      collection.fetch();
    }

    return {
      /**
       * Register a listener function to be triggered whenever the widget
       * selection changes.
       *
       * @memberof Mossaic.widgets.ls
       * @instance
       * @param {function} listener Listener function to be triggered
       */
      register_listener: function(listener) {
        // Bind to the view, so listener updates when selection changes
        listeners.push(listener);
        // Bind to the collection so listener updates when options change
        collection.bind("reset", listener);
        collection.bind("add", listener);
        collection.bind("remove", listener);
      },
      /**
       * Register a listener function to be triggered before the widget is
       * reloaded.
       *
       * @memberof Mossaic.widgets.ls
       * @instance
       * @param {function} listener Listener function to be triggered
       */
      register_prefetch_listener: function(listener) {
        // Pre-fetch listeners are called *before* reset fetches the
        // collection
        prefetch_listeners.push(listener);
      },
      /**
       * Reset the widget, triggering any registered prefetch listeners
       * beforehand.
       *
       * @memberof Mossaic.widgets.ls
       * @instance
       * @param {object} options Hash of options
       * @param {boolean} options.preserve_selection Ensure the currently
       *                                             selected model is still
       *                                             selected after reset.
       * @param {boolean} options.silent Suppress the change event triggered
       *                                 by the underlying collection
       */
      reset: function(options) {
        var options = options || {};
        view.show_placeholder();
        _.each(prefetch_listeners, function(listener) {
          listener();
        });
        if (!options.preserve_selection) {
          view.set_default_selection("");
          view.selected = {};
        }
        if (options.silent) {
          options.success = function() { view.render() };
        }
        collection.fetch(options);
      },
      /**
       * Get the currently selected model
       * @memberof Mossaic.widgets.ls
       * @instance
       * @return {Backbone.model.Model} Currently selected model
       */
      get_selected: function() {
        return view.get_selected();
      },
      /**
       * Set the supplied options on the underlying collection
       * @memberof Mossaic.widgets.ls
       * @instance
       * @param {object} options Hash of options
       */
      set_collection_options: function(options) {
        _.each(_.keys(options), function(key) {
          collection[key] = options[key];
        });
      },
      /**
       * Get the name of the currently selected model
       * @memberof Mossaic.widgets.ls
       * @instance
       * @return {string} Currently selected model
       */
      get_name: function() {
        return view.get_selected().name;
      },
      /**
       * Set the default selection of the widget display to a particular string
       * @memberof Mossaic.widgets.ls
       * @instance
       * @param {string} value Default vaule to be displayed as a selection in
       *                       the view
       */
      set_default_selection: function(value) {
        view.set_default_selection(value);
      },
      set_selected: function(model){
        return view.set_selected(model);
      },
      /**
       * Show the widget
       * @memberof Mossaic.widgets.ls
       * @instance
       * @param {object} options Hash of options to be passed to render function
       *                         of the view
       */
      show: function(options) {
        view.hidden = false;
        return view.render(options);
      },
      /**
       * Hide the widget
       * @memberof Mossaic.widgets.ls
       * @instance
       */
      hide: function() {
        view.hidden = true;
        return view.render();
      },
      /**
       * Return true if this widget is only used for entering input
       * @memberof Mossaic.widgets.ls
       * @instance
       * @return {boolean} True if the widget is only used for entering input
       */
      is_input_only: function() {
        return view.input_only;
      },
      /**
       * Set this to true if the widget should only be used for entering
       * input, rather than browsing data
       *
       * If we are setting input only mode, we want to remove from backbone
       * watch list, as we do not need or want changes
       *
       * @memberof Mossaic.widgets.ls
       * @instance
       * @param {boolean} input_only true if the widget is to be made input-only
       *                             false otherwise
       */
      set_input_only: function(input_only) {
        view.input_only = input_only;
        if (typeof(Backbone.couch) !== "undefined" &&
            typeof(options.type) !== "undefined" &&
            !options.ignore_changes) {
          if (input_only) {
            delete Backbone.couch._watchList[options.type];
          } else {
            Backbone.couch._watchList[options.type] = collection;
          }
        }
      },
      /**
       * Force an update of registered listener functions
       * @memberof Mossaic.widgets.ls
       * @instance
       */
      update_listeners: function() {
        _.each(listeners, function(listener) {
          listener({update_model: true});
        });
      },
      /**
       * Set a visual indicator on the view to correspond to model dirtyiness
       * @memberof Mossaic.widgets.ls
       * @instance
       * @param {boolean} is_dirty true if view should indicate model has
       *                           unpersisted changes
       */
      update_dirty_indicator: function(is_dirty) {
        view.show_dirty_indicator(is_dirty);
      },
      /**
       * Show a placeholder in place of any actual data
       * @memberof Mossaic.widgets.ls
       * @instance
       */
      show_placeholder: function() {
        view.show_placeholder();
      },
      /**
       * Unselect the current selection
       * @memberof Mossaic.widgets.ls
       * @instance
       * @param {object} options Hash of options to be passed to view
       */
      unselect: function(options) {
        view.unselect(options);
      }
    };
  };

  /**
   * Create a ls widget - a widget that can be used to list the contents of a
   * Backbone.Collection and filter the items that are displayed.
   *
   * The widget can be displayed using one of two different Backbone.View
   * classes. AutoBox is an autocomplete style search box which which also comes
   * with a button that will list all available options.
   *
   * FilterList is a view that lists all the items but allows the list to be
   * filtered via the contents of a seperate text box.
   *
   * If a parent is defined it uses the docs_by_parent view to retrieve all
   * document ids and names for the document currently selected in the parent
   * widget. If a parent is not defined, it uses the docs_by_type view to get
   * all document ids and names for the given type.
   *
   * When a selection is made, it retrieves the full model, and calls the externally
   * provided "set_model()" function, if it has been defined.
   *
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {Backbone.Model} options.init_model Model to be selected on creation
   * @param {boolean} options.indicate_if_dirty Provide a visual indication if
   *                                            currently selected model is
   *                                            dirty (i.e. has unsaved changes)
   * @param {function} options.set_model        Function to be called when
   *                                            models are selected
   * @param {object} options.parent             ls_widget that selects the
   *                                            parent used to define the
   *                                            available options for this
   *                                            widget
   * @param {string} options.type               The widget will only retrieve
   *                                            documents where doc.type is
   *                                            equal to this field
   * @param {boolean} options.clear_model_on_parent_change
   *                                            Clear the selected model when
   *                                            the parent selection changes
   * @param {string} options.url                Name of the CouchDB view that
   *                                            provides the documents
   * @param {string} options.load_model_metadata_only
   *                                            Only load the name/id of a
   *                                            selected model. Useful if the
   *                                            documents are large and the
   *                                            actual content is not required.
   * @param {function} options.get_keys         Externally provided function
   *                                            to get the startkey and endkey
   *                                            from the selected parent model
   * @param {string} options.view               The Backbone.View to be used
   *                                            to display the widget. Can be
   *                                            undefined, "AutoBox" or
   *                                            "FilterList"
   * @param {function} options.success          Externally provided function to
   *                                            be called on successful fetching
   *                                            of collection
   */
  Mossaic.widgets.ls = function(options) {
    var selected_model = options.init_model;  // Ok if this is undefined
    var ls_widget;
    var options = options;
    var indicate_if_dirty = options.indicate_if_dirty;
    var set_model = options.set_model;
    var parent = options.parent;
    var parent_id;
    var type = options.type;
    var on_model_load = [];
    var clear_model_on_parent_change = options.clear_model_on_parent_change;
    var custom_data_source = options.url && true;
    var get_keys;
    var load_model_metadata_only = options.load_model_metadata_only;
    if (typeof(parent) === "undefined") {
      options.url = options.url || "docs_by_type";
      if (options.url === "docs_by_type") {
        options.key = type;
      }
    } else {
      parent_id = parent.get_selected().id;  // Probably doesn't exist yet
      if (options.get_keys) {
        get_keys = options.get_keys;
      } else if (custom_data_source) {
        get_keys = function(type, parent) {
          // Pass in the parent and get the id within the function, as externally
          // supplied get_keys functions may want to use something other than the
          // id
          var parent_id = parent.get_selected().id;
          return {
            startkey: [parent_id],
            endkey: [parent_id, {}]
          };
        };
      } else {
        options.url = "docs_by_parent";
        get_keys = function(type, parent) {
          var parent_id = parent.get_selected().id;
          return {
            startkey: [type, parent_id],
            endkey: [type, parent_id, {}]
          };
        };
      }
      var keys = get_keys(type, parent);
      options.startkey = keys.startkey;
      options.endkey = keys.endkey;
      options.silent = true;
      /**
       * Custom change handler ensures only documents with the matching parent
       * are added
       */
      options.docChangeHandler = function(collection, doc, id) {
        var model = collection.get(id);
        if (model) {
          if (model && doc._rev != model.get("_rev")) {
            model.set(doc);
          }
        } else {
          if (doc.parent_id == parent.get_selected().id) {
            if (!doc.id) {
              doc.id = id;
            }
            collection.add(doc);
          }
        }
      };
    }
    options.reduce = false;
    options.view = options.view || "AutoBox";
    options.success = options.success || function(result) {
      return _.map(result.rows, function(row) {
        return {name: row.value, id: row.id};
      });
    };
    ls_widget = ls(options);
    if (typeof(parent) !== "undefined") {
      // The placeholder will be shown as soon as the parent widget
      // starts to fetch its collection
      parent.register_prefetch_listener(ls_widget.show_placeholder);
      // When the selection in the parent widget is updated, we get the updated
      // keys (based on the new parent selection) and re-fetch the collection
      // using the new keys
      parent.register_listener(function() {
        var new_parent_id = parent.get_selected().id;
        if (new_parent_id != parent_id ||
            typeof(parent_id) === "undefined") {
          parent_id = new_parent_id;
          var keys = get_keys(type, parent);
          ls_widget.set_collection_options({
            startkey: keys.startkey,
            endkey: keys.endkey
          });
          ls_widget.reset();
          if (typeof(selected_model) !== "undefined") {
            if (clear_model_on_parent_change) {
              selected_model.clear();
            }
            selected_model.is_dirty = true; // Selected model is now dirty
                                            // as its parent has changed
           selected_model.change();        // Fire a change request
                                            // to update GUI

          }
        }
      });
    }

    // When a selection is made on this widget, update selected model and
    // fetch the actual model content from the server
    ls_widget.register_listener(function(options) {
      var selected = ls_widget.get_selected();
      var id = selected.id;
      var Model = Mossaic.models.map[type];
      if (typeof(id) !== "undefined" && typeof(type) !== "undefined" &&
          options.update_model) {
        selected_model = new Model({id: ls_widget.get_selected().id}, {});
        if (indicate_if_dirty) {
          // If any parent model changes so it has unpersisted changes, then
          // the model selected by this widget is also dirty, as when saved
          // it would have a reference to a parent that might not exist.
          // We therefore update the dirty indicator on any change in the models
          // associated with any parent in the tree.
          ls_widget.bind_to_model_tree("change", function(model) {
            ls_widget.update_dirty_indicator(model.is_dirty);
          });
        }
        if (!(load_model_metadata_only)) {
          // Fetch the actual model from the server
          selected_model.fetch({
            silent: true,
            success: function() {
              if (typeof(set_model) === "function") {
                // The set_model function is set externally (in router) and will usually
                // update the model reference in a Backbone.View to this new
                // model
                set_model(selected_model);
                // Bind to model changes so that the widget selection is
                // cleared if the selected model becomes dirty (because it has
                // then changed and so is not the same model)
                selected_model.bind("change", function(model) {
                  if (!model.is_dirty &&
                      _.all(_.values(model.changedAttributes()), function(v) {
                    return typeof(v) === "undefined";
                  })) {
                    ls_widget.unselect({clear_model: true});
                  } else {
                    ls_widget.unselect({clear_model: false});
                  }
                });
              }
              // The on_model_load function is set by some external code and
              // will normally cause various functions to be bound to the
              // newly loaded model
              _.each(on_model_load, function(func) {
                if (typeof(func) == "function") {
                  func(selected_model);
                }
              })
            },
            error: function() {
              alert("Could not load data " + selected_model.id);
            }
          });
        }
      } else if (options.clear_model) {
        selected_model.clear();
      } else {
        // console.log("Nothing to load", selected, type);
      }
    });

    /**
     * Get the currently selected model
     * @name get_selected_model
     * @method
     * @memberof Mossaic.widgets.ls
     * @instance
     * @return {Backbone.model.Model} Currently selected model
     */
    ls_widget.get_selected_model = function() {
      return selected_model;
    };

    /**
     * Get the id of the parent model that defines the current list of models
     * @name get_parent_id
     * @method
     * @memberof Mossaic.widgets.ls
     * @instance
     * @return {string} Id of parent model from which the startkey and endkey
     *                  of the collection that generates the list in this
     *                  widget are derived
     */
    ls_widget.get_parent_id = function() {
      if (typeof(parent) !== "undefined") {
        var parent_id = parent.get_parent_id(); // Calling up the parent chain
                                                // means any unsaved models will
                                                // throw an exception
        var selected = parent.get_selected_model();
        var has_id = typeof(selected) !== "undefined" &&
            typeof(selected.id) !== "undefined";
        var is_saved = !(selected.isNew() || selected.is_dirty);
        if (has_id && is_saved) {
          return selected.id;
        } else {
          throw "No valid parent selected";
        }
      }
    };

    /**
     * Get the parent model for this widget
     * @name get_parent
     * @method
     * @memberof Mossaic.widgets.ls
     * @instance
     * @return {Backbone.model.Model} Parent model for this widget
     */
    ls_widget.get_parent = function() {
      return parent;
    }

    /**
     * Register a function to be called whenever the widget loads a model
     * @name on_model_load
     * @method
     * @memberof Mossaic.widgets.ls
     * @instance
     * @param {function} func Function to be called if model is successfully
     *                        loaded
     */
    ls_widget.on_model_load = function(func) {
      on_model_load = on_model_load.concat([func]);
    };

    /**
     * Get the model type that this widget is listing
     * @name get_type
     * @method
     * @memberof Mossaic.widgets.ls
     * @instance
     * @return {string} Type of model listed by this widget
     */
    ls_widget.get_type = function() {
      return type;
    };

    /**
     * Bind a function to every parent model in the tree
     * @name bind_to_model_tree
     * @method
     * @memberof Mossaic.widgets.ls
     * @instance
     * @param {string} event_type The type of event to which the function should
     *                            be bound
     * @param {function} func The function to be bound
     */
    ls_widget.bind_to_model_tree = function(event_type, func) {
      if (typeof(selected_model) !== "undefined") {
        selected_model.bind(event_type, func);
      }
      if (typeof(parent) !== "undefined") {
        parent.bind_to_model_tree(event_type, func);
      }
    }

    /**
     * Get new startkey and endkey and reload the CouchDB view by re-fetching
     * the Backbone.Collection
     * @name reload_view
     * @method
     * @memberof Mossaic.widgets.ls
     * @instance
     */
    ls_widget.reload_view = function() {
      var keys = get_keys(type, parent);
      ls_widget.set_collection_options({
        startkey: keys.startkey,
        endkey: keys.endkey
      });
      ls_widget.reset({
        preserve_selection: true,
        success: function() {
          ls_widget.show();
        }
      });
    }

    if (indicate_if_dirty) {
      ls_widget.bind_to_model_tree("change", function(model) {
        ls_widget.update_dirty_indicator(model.is_dirty);
      });
    }

    return ls_widget;
  }
})();
