/**
 * @file Defines a widget for listing CouchDB documents
 */
(function() {
  /**
   * Create a ls_create widget. Basically just a wrapper around ls & create widgets and
   * which allows them to be specified in one shot and bound to a single div.
   *
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options                Hash of options
   * 
   * @param {string} options.type           The widget will only handle documents where
   *                                        doc.type is equal to this field
   * @param {string} options.divname        The div to attach the widget to
   * 
   * @param {string} options.model          The model attached to the widget
   * 
   * @param {object} options.ls             Options to be passed to the ls widget (see
   *                                        Mossaic.widgets.ls)
   *                                        
   * @param {object} options.save           Options to be passed to the save wdiget (see
   *                                        Mossaic.widgets.save)
   *                                        
   * @param {object} options.create_model   Options to be passed to the create_model
   *                                        widget (see Mossaic.widgets.create_model)
   *                                        
   * @return {Mossaic.widgets.ls}           ls widget, with show and hide methods that
   *                                        have been extended to show/hide the associated
   *                                        save and create_model widgets
   */
  Mossaic.widgets.ls_create = function(options) {
    var options = options || {};
    var divname = options.divname;
    var ls_options = options.ls || {};
    var save_options = options.save || {};
    var create_options = options.create || {};    
   
    ls_options.type = options.type;
    ls_options.divname = divname + "_ls";

    create_options.divname = divname + "_create";

    var show = function() {
        // Render the widget, it's essentially just div hooks for the ls and create widgets
      $(divname).mustache("widgets.ls_create", {
          ls_id: ls_options.divname.split("#")[1],
          create_id: create_options.divname.split("#")[1]
      },{ method: 'html' });  
    };
    
    var hide = function() {
      $(divname).html("");
    };
    
    show();
        
    var ls = Mossaic.widgets.ls(ls_options);
    
    create_options.ls_widget = ls;
    
    // If we have been passed a name_source, then concatenate it into an array
    // with the one we created above
    if (typeof(save_options.name_source) !== "undefined") {
      save_options.name_source = save_options.name_source.concat([ls]);
    } else {
      // Otherwise if we haven't been passed a name_source, just set the one we created above.
      save_options.name_source = ls;
    }

    var create = Mossaic.widgets.create({
      divname : divname + "_create",
      create: create_options,
      save: save_options
    });
    
    
    // Bit of a hack - copy the ls_widget hide/show functions, then replace
    // them with functions that call the other widgets in this composite
    // We return the ls widget
    var ls_hide = ls.hide;
    var ls_show = ls.show;
    ls.hide = function() {
      hide();
      create.hide();
      ls_hide();
    };
    ls.show = function(options) {
      show();
      create = Mossaic.widgets.create({
        divname : divname + "_create",
        create: create_options,
        save: save_options
      });
      ls_show(options);
    };
    return ls;
  };
})();