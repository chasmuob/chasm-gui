define([], function() {

  return function(options) {

    var models = options.models;

    var db_name = Mossaic.db.get_db_name({type: "task"});

    var default_chasm_output_files = {
      factor_of_safety: true,
      pressure_head: false,
      soil_moisture_content: false,
      total_soil_moisture: false,
      vegetation_interception: false
    };


    var task_model = options.model || new Mossaic.models.Task({});
    task_model.set({
      output_files: default_chasm_output_files
    }, {silent: true});
    task_model.urlRoot = db_name;

    // validate_task => moved into task model

    /**
     * Populate the application selector - backbone seems overkill for this
     * operation so we just use jquery.couch.js
     */
    var populate_executables = function(task_view) {
      var dbname = Mossaic.db.get_db_name({type: "release"});
      var db = $.couch.db(dbname);
      db.view("release/releases", {
        success: function(results) {
          var executables = [];
          for (var i in results.rows) {
            var row = results.rows[i];
            var executable_attributes = get_executable_attributes(row.key);
            executables.push({
              id: row.id,
              application: executable_attributes.application,
              version: executable_attributes.version
            });
          }
          task_view.all_executables = executables;
          if (!task_view.model.has("executable")) {
            task_view.model.set({
              executable: {
                application: executables[0].application,
                version: executables[0].version
              }
            });
          }
          task_view.model.change();
        },
        error: function(response) {
          alert("Could not fetch application list");
        }
      });
    };

    var get_executable_attributes = function(executable_id) {
      var tokens = executable_id.split("_");
      return {
        application: tokens[0],
        version: tokens[1]
      };
    };

    var get_duration = function() {
      var boundary_conditions = this.model.boundary_conditions;
      return {
        unit: "h",
        value: boundary_conditions.get_duration()
      };
    };

    

    var TaskFinalise = Backbone.View.extend({
      events: {
        "change #executable": "update_executable",
        "change #time_step": "update_time_step",
        "click #submit_task": "submit",
        "change #name": "update_name",
        "change #notes": "update_notes",
        "change #number_of_jobs": "update_number_of_jobs"
      },
      template: "widgets.task_finalise",
      all_executables: [],
      initialize: function(options) {
        _.bindAll(this);
        this.model.bind("change", this.render);
        this.model.view = this;
        this.models = options.models;


        populate_executables(this);  // Beware - fires an async request which updates the model on success
        this.render();
      },
      render: function() {
        var that = this;
        var relevant_executables;

        try {
          var geometry_type = this.models.geometry.mode;
          relevant_executables = _.filter(
                  this.all_executables,
                  function(executable) {
                    return (executable.application === geometry_type);
                  }
          );
        } catch (e) {
          relevant_executables = this.all_executables;
        }

        this.$el.mustache(that.template, {
          name: this.model.get("name"),
          notes: this.model.get("notes"),
          executables: relevant_executables,
          time_step: this.model.get("time_step"),
          number_of_jobs: this.model.get("simulation_parameters").number_of_jobs
        }, {method: "html"});
        if ("executable" in this.model.attributes) {
          var executable_id = (this.model.get("executable").application + "_" +
                  this.model.get("executable").version).replace(/\./g, "\\.");
          this.$el.find("#" + executable_id).attr("selected", "selected");
        }

        return this.el;
      },
      update_name: function(event) {
        this.model.set({name: $(event.target).val()});
      },
      update_executable: function(event) {
        var executable_attributes = get_executable_attributes(
                this.$el.find("#executable option:selected").attr("id"));
        if (executable_attributes.application == "questa") {
          if ("output_files" in this.model.attributes) {
            this.model.unset("output_files");
          }
        } else if (executable_attributes.application == "chasm") {
          this.model.set({"output_files": default_chasm_output_files});
        }
        this.model.set({executable: executable_attributes});
      },
      update_time_step: function(event) {
        var timeStep = this.model.get("time_step");
        timeStep.value = Number($(event.target).attr("value"));
        this.model.change();
      },
      update_notes: function(event) {
        this.model.set({notes: $(event.target).val()});
      },
      update_number_of_jobs: function(event) {
        var number_of_jobs = parseInt($(event.target).val());
        var simulation_parameters = _.clone(
                this.model.get("simulation_parameters"));
        simulation_parameters.number_of_jobs = number_of_jobs;
        this.model.changed_by = this;
        this.model.set({simulation_parameters: simulation_parameters});
      },
      /**
       * Save all unsaved inputs using names given in boxes on form
       * If parents exist, they are saved before their children
       * @private
       */
      submit: function() {
        // First, make sure the executable in the task model is what is shown
        // on the form
        this.update_executable();

        // Get the ID of the logged-in user
        $.couch.session({
          success: function(response) {
            // Courtesy check only - actual security must happen in the
            // validate_doc_update function on the server
            if (response.userCtx.name) {
              if ($.inArray("requestor", response.userCtx.roles) != -1) {
                // TODO(1) - Check dirty status of models, and save if needed

//                Geometry didn't need to be saved so get current _id
                var task_parent_id = this.models.geometry.get("_id");
                if (typeof(task_parent_id) === "undefined") {
                  alert("Cannot save task - parent id is undefined");
                  return;
                }
                submit_task_request(this.model, response.userCtx.name, task_parent_id);
              } else {
                alert("Sorry you do not have task requestor permissions - " +
                        "please contact the appropriate administrator");
              }
            } else {
              alert("Please log in before attempting to submit new task " +
                      "requests");
            }
          },
          error: function(response) {
            alert("Could not determine session status:" + response.toSource());
          }
        });
      }
    });

    var task_view = new TaskFinalise({
      model: task_model,
      models: models
    });

    return task_view;
  };
});