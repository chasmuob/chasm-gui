/**
 * @file Defines the clear_model widget
 */
(function() {
  /**
   * Simple widget that displays the form for creating/editing a new model.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @param {object} options              Hash of options
   * @param {string} options.divname      CSS selector of the div to which this
   *                                      widget will be appended
   * @param {string} options.form         The Form object to display to edit the model
   * @constructor
   */
  Mossaic.widgets.create = function(options) {
    var divname = options.divname;
    var id = divname.split("#")[1];
    var dialog_id = Mossaic.utils.UUID();
    var dialog_body_id = Mossaic.utils.UUID();
    var save_id = dialog_id + "_save";

    // Render basic create widget, essentially it's the dialog box, and bunch of DIVs to hook things to
    $(divname).mustache("widgets.create", {
        id: id,
        dialog_id: dialog_id,
        dialog_body_id: dialog_body_id,
        save_id: save_id
    },{ method: 'html' });
    
    // New / Edit click handlers
    $("#" + id + "_new").click(function() {
      options.create.ls_widget.unselect({clear_model:true});
       $("#" + dialog_id).modal("show");
    });
  
    $("#" + id +"_edit").click(function() {
      $("#" + dialog_id).modal("show");
    });

    // We have to pass the form in rather than create it here because it needs to be attached to the ls_widget when
    // that is created.  This is done with the "set_model" fn when it's created in choose_slope() in router.
    // However, we only know here what the divname for it will be.
    var form = options.create.form;  
    form.set_divname("#" + dialog_body_id);

    // We can now render the form, because the dialog box it sits inside now exists in the DOM
    form.render();

    options.save.divname = "#" + save_id;
    options.save.success = function() {
      $("#" + dialog_id).modal("hide");
    };
    var save = Mossaic.widgets.save(options.save);

  
    return {
      /**
       * Show the widget by appending it to the div
       * @memberof Mossaic.widgets.clear_model
       * @instance
       */
      show: function() {
//        $(divname).html($.mustache(template, {
//            id: id
//        }));
        alert("ERROR!! We don't want to call widgets.create.show");
      },
      /**
       * Hide the widget
       * @memberof Mossaic.widgets.clear_model
       * @instance
       */
      hide: function() {
        $(divname).html("");
      }
    };
  };
})();