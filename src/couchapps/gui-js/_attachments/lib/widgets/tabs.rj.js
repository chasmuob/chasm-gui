define(["widgets/_basic.rj"], function(BasicWidget) {
  return BasicWidget.extend({
    template : "widgets.tabs2",
    events:{
      "click a": "link_clicked",
    },
    initialize : function(options) {
      BasicWidget.prototype.initialize.apply(this,[options]);
      var that = this;
      this.tabs = options.tabs;
      this.base_url = options.base_url;
      this.$el.mustache(this.template, {base_url: this.base_url, tabs: this.tabs}, {method:"html"});
    },

    link_clicked: function(event){
      event.preventDefault();
      event.stopImmediatePropagation();

      var link = $(event.target).data('link');
      if(link === this.active){
        return;
      }

      this.trigger('transitioning');
      var href = this.base_url + link;
      if(window.location.hash != href){
        window.location = href;
      }
    },

    set_active_tab: function(tablink) {
      this.enable_tab(tablink);
      this.$el.find(".active").removeClass("active");
      this.$el.find("a[data-link='"+tablink+"']").parent().addClass("active");
      this.active = tablink;
    },

    enable_tab: function(tablink) {
      var $tab = this.$el.find("a[data-link='"+tablink+"']");
      $tab.parent().removeClass("disabled");
      $tab.unbind("click");  // unbind to restore default click behaviour
    },

    disable_tab: function(tablink) {
      var $tab = this.$el.find("a[data-link='"+tablink+"']");
      $tab.parent().addClass("disabled");
      $tab.click(function(e) {
         e.preventDefault();   // Disabled tabs still react to clicks, so prevent that
         e.stopImmediatePropagation();
       });
    },

    enable_all_tabs: function() {
      var that = this;
      _.each(this.tabs, function(tab) {
        that.enable_tab(tab.link);
      });
    },

    disable_all_tabs: function() {
      var that = this;
      _.each(this.tabs, function(tab) {
        that.disable_tab(tab.link);
      });
    },

    render : function() {
      return this.el;
    }

  });
});
