define(["widgets/_basic.rj"], function(BasicWidget) {
  return BasicWidget.extend({

    template : "widgets.button",

    initialize : function(options) {
      BasicWidget.prototype.initialize.apply(this,[options]);
      this.text = options.text;
      this.click = options.click;
      this.class = options.class;
    },

    enable: function() {
      this.$el.find("button").enable();
    },

    disable: function() {
      this.$el.find("button").enable(false);
    },

    render : function() {
      this.$el.mustache(this.template,{text: this.text, class: this.class}, {method : "html"});
      if (typeof(this.click) === "function") {
        this.$el.find("button").click(this.click);
      }
      return this.el;
    }

  });
});
