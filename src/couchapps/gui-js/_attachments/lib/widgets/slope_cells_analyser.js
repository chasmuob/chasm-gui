/**
 * @file Defines a widget for analysing slope cells data
 */
(function() {
  /**
   * Create a slope_cells_analyser widget. Uses the slope_cells view to
   * retrieve slope cells data for a given task and job.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {string} options.divname CSS selector of the div to append to
   * @param {Mossaic.widgets.ls} options.parent ls widget that sets the parent
   *                                            dataset (most likely a Task)
   * @param {Mossaic.widgets.ls} options.jobs_browser ls widget that sets the
   *                                                  job dataset
   * @param {boolean} options.active Set to false to make inactive (and hidden)
   */
  Mossaic.widgets.slope_cells_analyser = function(options) {
    var divname = options.divname;
    var parent = options.parent;
    var parent_id = parent.get_selected().id; // Probably doesn't exist yet
    var active = options.active;
    var variables = [{
      id: "moisture_content",
      name: "Moisture content",
      unit: "m3/m3",
      timeVariant: true,
      color: {
        min: "lightcyan",
        max: "midnightblue",
        water: "white"
      }
    }, {
      id: "pressure_head",
      name: "Pore pressure",
      unit: "m",
      timeVariant: true,
      color: {
        min: "lightblue",
        max: "firebrick",
        water: "white"
      }
    }, {
      id: "soil_type",
      name: "Soils",
      unit: "m",
      timeVariant: false,
      hideLegend: true,
      color: {
        min: "lightblue",
        max: "firebrick",
        water: "white"
      }
    }];
    var selected_variable = variables[0];
    var jobs_browser = options.jobs_browser;
    var job_id = jobs_browser.get_selected().id;
    var current_time = new Backbone.Model({
      value: 0
    });

    // This will be populated with all the task input files
    var inputs = {};

    var collection = new Backbone.Collection();
    collection.view = "slope_cells";
    collection.ddocName = "mining";
    collection.doreduce = false;
    collection.success = function(result) {
      return _.map(result.rows, function(row) {
        return {
          id: row.key,
          value: row.value
        };
      });
    };
    var collections;

    var fos_collection = new Backbone.Collection();
    var results_id;
    fos_collection.view = "fos";
    fos_collection.ddocName = "mining";
    fos_collection.doreduce = false;
    fos_collection.success = function(result) {
      return _.map(result.rows, function(row) {
        results_id = row.id;
        return {
          fos: row.value,
          time: row.key[2]
        };
      });
    };

    var slip_collection = new Backbone.Collection();
    slip_collection.view = "slip_circle";
    slip_collection.ddocName = "mining";
    slip_collection.doreduce = false;
    slip_collection.success = function(result) {
      return _.map(result.rows, function(row) {
        return row.value;
      });
    };
    slip_collection.startkey = [parent_id, job_id];
    slip_collection.endkey = [parent_id, job_id, {}];

    // Rainfall collection
    var rainfall;
    var rain_collection = new Backbone.Collection();
    rain_collection.view = "rainfall";
    rain_collection.ddocName = "mining";
    rain_collection.doreduce = false;
    rain_collection.success = function(result) {
      rainfall = _.map(result.rows[0].value, function(t) {
        return t / 0.00000027777786;
      });
    };

    // Results Collection
    var results_collection = new Backbone.Collection();
    results_collection.view = "results_by_job";
    results_collection.ddocName = "gui";
    results_collection.doreduce = false;
    var chasm_output = {};
    results_collection.success = function(result) {
      chasm_output = result.rows[0].value;
    };

    // Timestep View to become factor-of-safety-based timestep slider
    var TimestepView = Backbone.View.extend({
      initialize: function(collection, options) {
        _.bindAll(this);
        this.collection = fos_collection;
        // this.collection.bind("reset", this.render);
        // rain_collection.bind("reset",this.render);
        this.divname = options.divname;
      },
      redraw: function() {
        this.chart.selectAll("#current_time")
          .attr("x1", this.x(current_time.get("value")))
          .attr("x2", this.x(current_time.get("value")))
          .attr("y1", this.y(this.height))
          .attr("y2", this.y(0));
      },
      /**
       * TODO: Put me somewhere generic - I am duplicate code (see slope_graphic.js 279)
       * Get the coordinates of an event that occured in the screen coordinate
       * system in the svg coordinate system.
       *
       * @memberof Mossaic.widgets.slope_cells_analyser
       * @instance
       * @param {object} event D3 event object containing screen coordinates
       * @return Coordinates of event in svg space
       */
      screen_to_svg_transform: function(event) {
        // Get current transform matrix (why the [0][0] ?)
        var ctm = d3.select(event.target)[0][0].getScreenCTM();
        var point = this.svg[0][0].createSVGPoint();
        point.x = event.clientX;
        point.y = event.clientY;
        return transformed_point = point.matrixTransform(ctm.inverse());
      },
      show_placeholder: function() {
        this.collection.reset([]);
      },
      get_current_fos: function() {
        return this.data ? this.data[current_time.get('value')].fos : 0;
      },
      // Render the timestep view
      render: function() {
        var that = this;
        if (this.svg) {
          this.svg.remove();
        }
        if (!active) {
          console.log('Not active');
          return;
        }
        var div = d3.select(this.divname);
        this.width = $(this.divname).width();
        this.height = $(document).height() * 0.2;

        if (typeof(this.collection.at(0)) === "undefined") {
          console.log('No collection');
          return;
        }
        var margin = {
          x: 40,
          y: 40
        };

        this.svg = div.append("svg:svg")
          .attr("class", "chart time_step_viewer")
          .attr("width", this.width)
          .attr("height", this.height);
        this.chart = this.svg.append("svg:g");

        var data = this.data = this.collection.toJSON();
        var min_fos = _.min(data, function(d) {
          return d.fos;
        }).fos;
        var max_fos = _.max(data, function(d) {
          return d.fos;
        }).fos;
        // CouchDB view is sorted, so can just use first and last to get time
        var min_time = data[0].time;
        var max_time = _.last(data).time;

        this.min_y2 = _.min(rainfall);
        this.max_y2 = _.max(rainfall);

        this.x = d3.scale.linear()
          .domain([min_time, max_time])
          .range([0 + margin.x, this.width - margin.x]);
        this.y = d3.scale.linear()
          .domain([min_fos, max_fos])
          .range([this.height - margin.y, 0 + margin.y]);

        this.y2 = d3.scale.linear()
          .domain([this.min_y2, this.max_y2])
          .range([this.height - margin.y, 0 + margin.y]);

        this.chart.append("svg:line")
          .attr("x1", 0 + margin.x)
          .attr("x2", 0 + margin.x)
          .attr("y1", 0 + margin.y)
          .attr("y2", this.height - margin.y);
        this.chart.append("svg:line")
          .attr("x1", this.width - margin.x)
          .attr("x2", this.width - margin.x)
          .attr("y1", 0 + margin.y)
          .attr("y2", this.height - margin.y);
        this.chart.append("svg:line")
          .attr("x1", 0 + margin.x)
          .attr("x2", this.width - margin.x)
          .attr("y1", this.height - margin.y)
          .attr("y2", this.height - margin.y);

        // Add fos ticks
        var fos_ticks = this.y.ticks(5).map(this.y.tickFormat(5, ".3f"));
        this.chart.selectAll(".fos_tick")
          .data(fos_ticks)
          .enter().append("svg:line")
          .attr("class", "fos_tick noselect")
          .attr("x1", margin.x)
          .attr("x2", margin.x - 5)
          .attr("y1", function(d) {
            return that.y(d);
          })
          .attr("y2", function(d) {
            return that.y(d);
          });

        // Add labels for fos
        this.chart.selectAll(".fos_label")
          .data(fos_ticks)
          .enter().append("svg:text")
          .attr("class", "fos_label noselect")
          .text(String)
          .attr("x", margin.x - 15)
          .attr("y", function(d) {
            return that.y(d);
          })
          .attr("text-anchor", "middle")
          .attr("dy", 4);

        // Add rainfall ticks
        this.chart.selectAll(".rain_tick")
          .data(this.y2.ticks(3))
          .enter().append("svg:line")
          .attr("class", "fos_tick noselect")
          .attr("x1", this.width - margin.x)
          .attr("x2", this.width - margin.x + 5)
          .attr("y1", function(d) {
            return that.y2(d);
          })
          .attr("y2", function(d) {
            return that.y2(d);
          });

        // Add labels for rainfall
        this.chart.selectAll(".rain_label")
          .data(this.y2.ticks(3))
          .enter().append("svg:text")
          .attr("class", "fos_label noselect")
          .text(String)
          .attr("x", this.width - margin.x + 15)
          .attr("y", function(d) {
            return that.y2(d);
          })
          .attr("text-anchor", "middle")
          .attr("dy", 4);

        this.chart.selectAll(".bar")
          .data(rainfall)
          .enter().append("svg:rect")
          .attr("class", "bar")
          .style("fill", "steelblue")
          // .attr("x", function(d,i) { return that.x(i); })
          .attr("x", function(d, i) {
            return that.x(i);
          })
          .attr("width", (this.width / rainfall.length))
          .attr("y", function(d) {
            return that.y2(d);
          })
          .attr("height", function(d) {
            if (!d) {
              return 0;
            }
            return that.height - that.y2(d) - margin.y;
          });
        // Add time step ticks
        this.chart.selectAll(".time_tick")
          .data(this.x.ticks(5))
          .enter().append("svg:line")
          .attr("class", "time_tick noselect")
          .attr("x1", function(d) {
            return that.x(d);
          })
          .attr("x2", function(d) {
            return that.x(d);
          })
          .attr("y1", this.height - margin.y)
          .attr("y2", this.height - margin.y + 5);

        // Add time step labels
        this.chart.selectAll(".time_label")
          .data(this.x.ticks(5))
          .enter().append("svg:text")
          .attr("class", "time_label noselect")
          .text(String)
          .attr("x", function(d) {
            return that.x(d);
          })
          .attr("y", this.height - margin.y + 15)
          .attr("text-anchor", "middle")
          .attr("dy", 4);

        var fos_title = {
          x: 10,
          y: this.height / 2 + 30
        };

        var rainfall_title = {
          x: this.width - 10,
          y: this.height / 2 - 30
        };

        this.chart.append("svg:text")
          .attr("class", "fos_title noselect")
          .attr("x", fos_title.x)
          .attr("y", fos_title.y)
          .text("Factor of safety")
          .attr("transform", "rotate(270, " + fos_title.x + ", " +
            fos_title.y + ")");

        this.chart.append("svg:text")
          .attr("class", "fos_title noselect")
          .attr("x", rainfall_title.x)
          .attr("y", rainfall_title.y)
          .text("Rainfall Intensity (mm/h)")
          .attr("transform", "rotate(90, " + rainfall_title.x + ", " + rainfall_title.y + ")");

        this.chart.append("svg:text")
          .attr("class", "noselect")
          .attr("x", this.width / 2)
          .attr("y", this.height - (margin.y * 0.2))
          .text("Time step");

        this.chart.selectAll(".fos_point")
          .data(data)
          .enter()
          .append("svg:line")
          .attr("class", "fos_single")
          .attr("x1", function(d, i) {
            if (i > 0) {
              return that.x(data[i - 1].time);
            } else {
              return that.x(d.time);
            }
          })
          .attr("y1", function(d, i) {
            if (i > 0) {
              return that.y(data[i - 1].fos);
            } else {
              return that.y(d.fos);
            }
          })
          .attr("x2", function(d, i) {
            return that.x(d.time);
          })
          .attr("y2", function(d, i) {
            return that.y(d.fos);
          });


        this.chart.selectAll("#current_time")
          .data([current_time])
          .enter()
          .append("svg:line")
          .attr("id", "current_time")
          .attr("x1", this.x(current_time.get("value")))
          .attr("x2", this.x(current_time.get("value")))
          .attr("y1", this.height - margin.y)
          .attr("y2", 0 + margin.y);

        var mousedown = false;
        this.svg
          .on("mousedown", function(event) {
            var point = that.screen_to_svg_transform(d3.event);
            if (that.x.invert(point.x) < 0) {
              point.x = that.x(0);
            } else if (that.x.invert(point.x) > max_time) {
              point.x = that.x(max_time);
            }
            d3.select("#current_time")
              .attr("x1", point.x)
              .attr("x2", point.x);
            current_time.set({
              value: Math.round(that.x.invert(point.x))
            });
            mousedown = true;
          })
          .on("mouseup", function() {
            mousedown = false;
          })
          .on("mousemove", function(event) {
            if (mousedown) {
              var point = that.screen_to_svg_transform(d3.event);
              if (that.x.invert(point.x) < 0) {
                point.x = that.x(0);
              } else if (that.x.invert(point.x) > max_time) {
                point.x = that.x(max_time);
              }
              d3.select("#current_time")
                .attr("x1", point.x)
                .attr("x2", point.x);
              current_time.set({
                value: Math.round(that.x.invert(point.x))
              });
            }
          });
      }
    });

    var WaterTableView = Backbone.View.extend({
      initialize: function(parent) {
        this.cache = [];
        this.parent = parent;
      },
      calculate_water_table: function(map) {
        var result = [];
        for (var x = 0; x < map.length; x++) {
          for (var y = 0; y < map[x].length; y++) {
            var v = map[x][y] > 0;
            // Top - check cell above
            if (y + 1 < map[x].length) {
              var top = map[x][y + 1] > 0;
              if (top !== v) {
                result.push({
                  x1: x,
                  y1: y,
                  x2: x + 1,
                  y2: y
                });
              }

            }
            // Right - check cell to the right
            if (x + 1 < map.length && y < map[x + 1].length) {
              var right = map[x + 1][y] > 0;
              if (right !== v) {
                result.push({
                  x1: x + 1,
                  y1: y,
                  x2: x + 1,
                  y2: y - 1
                });
              }
            }
          }
        }
        return result;
      },
      render: function(t) {
        var chart = this.parent.chart;
        var that = this;
        if (!collections.pressure_head.length) {
          return;
        }

        if (typeof this.cache[t] === 'undefined') {
          var map = collections.pressure_head.at(t).attributes.value;
          var waterTable = this.calculate_water_table(map);
          if (waterTable.length) {
            this.cache[t] = waterTable;
          }
        }
        var B = this.cache[t];
        if (!B) {
          return;
        }

        chart.selectAll(".water-table-line").remove();

        var wtClass = "water-table-line water-table-line-" + selected_variable.color.water;
        chart.selectAll(".water-table-line")
          .data(B)
          .enter()
          .append("svg:line")
          .attr("class", wtClass)
          .attr("x1", function(d) {
            return that.parent.x(d.x1);
          })
          .attr("x2", function(d) {
            return that.parent.x(d.x2);
          })
          .attr("y1", function(d) {
            return that.parent.y(d.y1);
          })
          .attr("y2", function(d) {
            return that.parent.y(d.y2);
          })
          .attr('stroke-width', 2)
          .attr('fill', 'none');
      }

    });

    var View = Backbone.View.extend({
      initialize: function(collection, options) {
        _.bindAll(this);
        this.divname = options.divname;
        this.collection = collection;
        this.collection.bind("reset", this.render);
        this.waterTableView = new WaterTableView(this);
        this.water_table_cache = [];
        Backbone.View.prototype.initialize.apply(this, [collection, options]);
      },
      render_variable_select: function(div) {
        var that = this;
        if (div.select("select").empty()) {
          if (div.select(".time_step_viewer").empty()) {
            this.select = div.append("xhtml:select");
          } else {
            this.select = div.insert("xhtml:select", ".time_step_viewer");
          }
          this.select
            .attr("id", "variable_select");
          _.each(variables, function(variable) {
            var option = that.select.append("xhtml:option")
              .attr("id", variable.id)
              .html(variable.name + " (" + variable.unit + ")");
            if (variable === selected_variable) {
              option.attr("selected", "selected");
            }
          });
          this.select.on("change", function() {
            var selected = $("#variable_select option:selected");
            selected_variable = _.find(variables, function(variable) {
              return variable.id === selected.attr("id");
            });
            collection.startkey[1] = selected_variable.id;
            collection.endkey[1] = selected_variable.id;
            collection.fetch();
          });
        }
      },
      render_job_output_link: function(div) {
        if (div.select(".time_step_viewer").empty()) {
          this.job_output_link = div.append("xhtml:div");
        } else {
          this.job_output_link = div.insert("xhtml:div", ".time_step_viewer");
        }

        this.job_output_link
          .html('<a href="../../' + results_id + "/output_" +
            job_id + ".tar.gz" + '">Download input/output files (.tar.gz)</a>')
          .style("float", "right");
      },
      redraw: function() {
        var that = this;
        // No need to redraw soil types
        var t = current_time.get('value');
        if (!selected_variable.timeVariant) {
          this.waterTableView.render(t);
          return;
        }

        var data = this.flattened_values[t];
        this.chart.selectAll("rect")
          .data(data)
          .attr("fill", function(d) {
            return that.color(d.value);
          });
        this.waterTableView.render(t);
      },
      draw_legend: function(chart, options) {
        var legend_width = options.width;
        var legend_line_length = options.line_length;
        var legend_line_height = options.line_height;
        var legend_margin_top = options.margin_top;
        var legend_x = options.legend_x;
        var legend_y = 0 + legend_margin_top;
        var legend_y_spacing = options.item_spacing;
        var legend_padding_y = options.padding_y;
        var legend_padding_x = options.padding_x;
        var legend_box_x = legend_x - legend_padding_x;
        var legend_box_y = legend_y - legend_padding_y;
        var increments = options.increments;
        var color_domain = options.color_domain;
        var color = options.color;
        var min = color_domain[0];
        var max = _.last(color_domain);
        var inc = (max - min) / increments;
        var data = _.map(_.range(increments), function(index) {
          return min + inc * index;
        });

        chart.append("svg:rect")
          .attr("class", "legend_box")
          .attr("x", legend_box_x)
          .attr("y", legend_box_y)
          .attr("width", legend_width + legend_padding_x)
          .attr("height", legend_box_y +
            (legend_y_spacing * (increments - 1)) +
            legend_padding_y);

        chart.selectAll(".legend_rect")
          .data(data)
          .enter()
          .append("svg:rect")
          .attr("class", "legend_rect")
          .attr("x", legend_x)
          .attr("y", function(d, i) {
            return legend_y + legend_y_spacing * i - legend_line_height / 2;
          })
          .attr("width", legend_line_length)
          .attr("height", legend_line_height)
          .style("fill", function(d) {
            return color(d);
          });

        chart.selectAll(".legend_text")
          .data(data)
          .enter()
          .append("svg:text")
          .attr("class", "legend_text")
          .attr("x", legend_x + legend_line_length + legend_padding_x)
          .attr("y", function(d, i) {
            return legend_y + (legend_y_spacing * i);
          })
          .attr("dominant-baseline", "central")
          .text(function(d) {
            return Mossaic.utils.round(d, 4);
          });
      },
      show_placeholder: function() {
        this.collection.reset([]);
      },
      get_cell: function(x, y) {
        var t = selected_variable.timeVariant ? current_time.get('value') : 0;
        return _.find(this.flattened_values[t], function(c) {
          return c.x === x && c.y === y;
        });
      },
      // Render the slope viewer
      render: function() {
        var that = this;
        if (this.svg) {
          this.svg.remove();
          this.select.remove();
        }
        if (this.job_output_link) {
          this.job_output_link.remove();
        }

        if (!active) {
          return;
        }

        if (selected_variable.timeVariant && typeof(this.collection.at(current_time.get("value"))) === "undefined") {
          return;
        }

        var div = d3.select(this.divname);
        this.width = $(this.divname).width() * 0.8;
        this.height = $(document).height() * 0.45;

        var values = this.collection;
        var size = values.size();
        if (size === 0) {
          return;
        }

        // this.height = this.width;

        this.render_variable_select(div);
        this.render_job_output_link(div);

        var min_value, max_value, max_x, max_y;
        var max_time = size - 1;
        var min_time = 0;
        this.flattened_values = _.toArray(values.map(function(values_at) {
          return _.flatten(_.toArray(_.map(values_at.toJSON().value, function(values_y, i) {
            return _.map(values_y, function(value, j) {
              var x = parseInt(i);
              var y = parseInt(j);
              if (typeof(min_value) === "undefined" || (value < min_value)) {
                min_value = value;
              }
              if (typeof(max_value) === "undefined" || (value > max_value)) {
                max_value = value;
              }
              if (typeof(max_x) === "undefined" || (x > max_x)) {
                max_x = x;
              }
              if (typeof(max_y) === "undefined" || (y > max_y)) {
                max_y = y;
              }
              return {
                x: parseInt(i),
                y: parseInt(j),
                value: value
              };
            });
          })));
        }));
        var ratio = _.max([max_y, max_x]) / max_y;

        var color_domain = [min_value, max_value];
        this.color = d3.scale.linear()
          .domain(color_domain)
          .range([selected_variable.color.min, selected_variable.color.max]);

        var margin = 30;

        // Aspect ratio fixing
        var max_width = 600;
        var max_height = 400;

        this.width = _.max([this.width, this.height]);
        if (this.width > max_width) {
          this.width = max_width;
        }
        this.height = this.width;
        var aspect_max_x = _.max([max_y, max_x]);

        this.x = d3.scale.linear()
          .domain([0, aspect_max_x])
          .range([0 + margin, this.width - margin])
          // .range([0,this.width])
        ;
        this.y = d3.scale.linear()
          .domain([0, max_y])
          .range([(this.height - margin) / ratio, (0 + margin) / ratio])
          // .range([0,this.width/aspectFactor])
        ;

        if (div.select(".time_step_viewer").empty()) {
          this.svg = div.append("svg:svg");
        } else {
          this.svg = div.insert("svg:svg", ".time_step_viewer");
        }

        this.svg
          .attr("class", "chart slope-chart")
          .attr("width", this.width)
          .attr("height", (this.height + margin) / ratio);
        this.chart = this.svg.append("svg:g");

        // Add x labels
        this.chart.selectAll(".slope_cell_label_x")
          .data(this.x.ticks(8))
          .enter().append("svg:text")
          .attr("class", "slope_cell_label_x noselect")
          .text(String)
          .attr("x", function(d) {
            return that.x(d);
          })
          // .attr("y", that.height - margin + 20)
          .attr("y", that.y(0) + margin)
          .attr("text-anchor", "middle")
          .attr("dy", 4);

        // Add y labels
        this.chart.selectAll(".slope_cell_label_y")
          .data(this.y.ticks(8))
          .enter().append("svg:text")
          .attr("class", "slope_cell_label_y noselect")
          .text(String)
          .attr("x", margin - 15)
          .attr("y", function(d) {
            return that.y(d);
          })
          .attr("text-anchor", "start")
          .attr("dy", 15);

        var t = selected_variable.timeVariant ? current_time.get("value") : 0;
        this.chart.selectAll("rect")
          .data(this.flattened_values[t])
          .enter()
          .append("svg:rect")
          .attr("id", function(d) {
            return "cell_" + d.x + "_" + d.y;
          })
          .attr("x", function(d) {
            return that.x(d.x);
          })
          .attr("y", function(d) {
            return that.y(d.y);
          })
          .attr("width", this.x(1) - this.x(0))
          .attr("height", Math.abs(this.y(1) - this.y(0)))
          .attr("fill", function(d) {
            return that.color(d.value);
          })
          .on('click', function(rect) {
            that.trigger('cell:clicked', rect);
          });

        // Reset the active cell
        this.trigger('cell:reset');

        var legend_width = 80;
        if (!selected_variable.hideLegend) {
          this.draw_legend(this.chart, {
            width: legend_width,
            margin_top: this.height * 0.1,
            item_spacing: 15,
            legend_x: this.width - legend_width - 20,
            padding_y: 15,
            padding_x: 10,
            color_domain: color_domain,
            color: this.color,
            increments: 8,
            line_length: this.x(1) - this.x(0),
            line_height: Math.abs(this.y(1) - this.y(0))
          });
        }

        this.waterTableView.render(0);
        if (typeof(this.do_after_render) === "function") {
          this.do_after_render();
        }
      }
    });

    // Render the slip circle
    var SlipView = Backbone.View.extend({
      initialize: function(collection, options) {
        _.bindAll(this);
        this.divname = options.divname;
        this.collection = collection;
        // this.collection.bind("reset", this.render);
        this.get_chart = options.get_chart;
        this.get_x = options.get_x;
        this.get_y = options.get_y;
        Backbone.View.prototype.initialize.apply(this, [collection, options]);
      },
      redraw: function() {
        var t = current_time.get("value");
        var circle = this.collection.at(t);
        var chart = this.get_chart();
        chart.select("#slip_circle")
          .attr("cx", this.get_x()(circle.get("x")))
          .attr("cy", this.get_y()(circle.get("y")))
          .attr("rx", Math.abs(this.get_x()(circle.get("r")) - this.get_x()(0)))
          .attr("ry", Math.abs(this.get_y()(circle.get("r")) - this.get_y()(0)));
        chart.select("#slip_circle_center")
          .attr("cx", this.get_x()(circle.get("x")))
          .attr("cy", this.get_y()(circle.get("y")));
        this.trigger('circle:changed', circle.attributes);
      },
      render: function() {
        var circle = this.collection.at(current_time.get("value"));
        if (typeof(circle) === "undefined" || circle.length === 0) {
          return;
        }
        var chart = this.get_chart();
        chart.insert("svg:ellipse", ".legend_box")
          .attr("id", "slip_circle")
          .attr("cx", this.get_x()(circle.get("x")))
          .attr("cy", this.get_y()(circle.get("y")))
          .attr("rx", Math.abs(this.get_x()(circle.get("r")) - this.get_x()(0)))
          .attr("ry", Math.abs(this.get_y()(circle.get("r")) - this.get_y()(0)))
          .attr("fill", "none")
          .attr("stroke", "#000");
        chart.insert("svg:circle")
          .attr("id", "slip_circle_center")
          .attr("cx", this.get_x()(circle.get("x")))
          .attr("cy", this.get_y()(circle.get("y")))
          .attr("r", 1)
          .attr("fill", "#000")
          .attr("stroke", "#000");
        this.trigger('circle:changed', circle.attributes);
      }
    });

    var DataView = Backbone.View.extend({
      initialize: function(options) {
        _.bindAll(this);
        this.divname = options.divname;
        this.slip_view = options.slip_view;
        this.chart_view = options.chart_view;
        this.timestep_view = options.timestep_view;
        this.activeCell = undefined;
        this.circle = {};
        // Bind to the main chart click handlers on cells
        this.chart_view.bind('cell:clicked', function(cell) {
          this.activeCell = cell;
          this.render();
        }, this);

        this.chart_view.bind('cell:reset', function() {
          if (this.activeCell) {
            this.activeCell = this.chart_view.get_cell(this.activeCell.x, this.activeCell.y);
          }
          // this.activeCell = undefined;
          this.render();
        }, this);

        // Bind to the slip circle changing
        this.slip_view.bind('circle:changed', function(circle) {
          this.circle = circle;
          this.render();
        }, this);


      },
      render: function() {
        if (this.div) {
          this.div.remove();
        }
        if (!active) {
          return;
        }
        var parent = $(this.divname);
        var template = 'widgets.data_panel';

        var t = current_time.get('value') || 0;

        // Read out the required data fields
        var influx = chasm_output.infux && chasm_output.infux.value[t];
        if (influx) {
          influx = (influx / 0.00000027777786).toFixed(4);
        }
        var moisture = chasm_output.moisture && chasm_output.moisture.value[t];
        var hourlyRainfall = rainfall && rainfall[t].toFixed(4);
        var fos = chasm_output.factor_of_safety && chasm_output.factor_of_safety[t];
        var runout = chasm_output.runout && chasm_output.runout.value[t];
        var seismicity = chasm_output.seismicity && chasm_output.seismicity.value[t];
        var height = $(this.divname).height() * 0.6;
        var soil = null;
        if (selected_variable.id === 'soil_type' && this.activeCell) {
          soil = inputs.soils.soils[this.activeCell.value];
        }

        this.div = $('<div style="max-height: ' + height + 'px;">').mustache(template, {
          hour: t,
          circle: this.circle,
          fos: fos,
          influx: influx,
          hourlyRainfall: hourlyRainfall,
          moisture: moisture,
          cell: this.activeCell,
          runout: runout,
          seismicity: seismicity,
          soil: soil,
          title: selected_variable.name
        }, {
          method: "html"
        });
        parent.append(this.div);
      },
      redraw: function() {
        this.render();
      }
    });

    var view = new View(collection, {
      divname: divname,

    });



    var slip_view = new SlipView(slip_collection, {
      get_chart: function() {
        return view.chart;
      },
      get_x: function() {
        return view.x;
      },
      get_y: function() {
        return view.y;
      }
    });

    var timestep_view = new TimestepView(fos_collection, {
      divname: divname
    });

    var data_view = new DataView({
      divname: divname,
      slip_view: slip_view,
      chart_view: view,
      timestep_view: timestep_view
    });
    current_time.bind("change", data_view.redraw);
    view.do_after_render = function() {
      slip_view.render();
    };
    current_time.bind("change", view.redraw);
    current_time.bind("change", slip_view.redraw);

    return {
      /**
       * Reset the analyser by re-fetching the underlying collections
       * @memberof Mossaic.widgets.slope_cells_analyser
       * @instance
       */
      reset: function() {
        collection.fetch();
        fos_collection.fetch();
      },
      /**
       * Show the widget
       * @memberof Mossaic.widgets.slope_cells_analyser
       * @instance
       */
      show: function() {
        view.render();
        data_view.render();
        // timestep_view.render();
      },
      /**
       * Make the widget active/inactive
       * @memberof Mossaic.widgets.slope_cells_analyser
       * @instance
       * @param {boolean} state true if widget should become active, false
       *                        otherwise
       */
      set_active: function(state) {
        active = state;
        if (!active) {
          $(divname).empty();
          return;
        }

        parent_id = parent.get_selected().id;
        job_id = jobs_browser.get_selected().id;
        job = jobs_browser.get_selected();
        var task = parent.get_selected();

        console.log('Task ID', parent_id, 'Job ID', job_id, job, task);

        slip_collection.startkey = [parent_id, job_id];
        slip_collection.endkey = [parent_id, job_id, {}];

        collection.startkey = [parent_id, selected_variable.id, job_id];
        collection.endkey = [parent_id, selected_variable.id, job_id, {}];
        fos_collection.startkey = [parent_id, job_id];
        fos_collection.endkey = [parent_id, job_id, {}];
        results_collection.startkey = [job_id];
        results_collection.endkey = [job_id, {}];
        rain_collection.startkey = [parent_id];
        rain_collection.endkey = [parent_id, {}];

        var complete = _.invoke([
          collection,
          fos_collection,
          results_collection,
          slip_collection,
          rain_collection
        ], 'fetch');

        collections = _.reduce(variables, function(total, variable) {
          var c = new Backbone.Collection();

          c.view = "slope_cells";
          c.ddocName = "mining";
          c.doreduce = false;
          c.startkey = [parent_id, variable.id, job_id];
          c.endkey = [parent_id, variable.id, job_id, {}];

          c.success = function(result) {
            return _.map(result.rows, function(row) {
              return {
                id: row.key,
                value: row.value
              };
            });
          };
          complete.push(c.fetch());
          total[variable.id] = c;
          return total;
        }, {});

        var that = this;

        var input_files = task.get('input_files');
        var input_revisions = task.get('input_revisions');
        _.each(input_files, function(docId, key) {
          var rev = input_revisions[key];
          var deferred = $.Deferred();
          complete.push(deferred);
          $.couch.db('chasm').openDoc(docId, {
            rev: rev,
            success: function(data) {
              inputs[key] = data;
              deferred.resolve();
            }
          });
        });

        $.when.apply($, complete).done(function() {
          setTimeout(function() {
            view.render();
            data_view.render();
            timestep_view.render();
          }, 100);
        });
      }
    };
  };
})();
