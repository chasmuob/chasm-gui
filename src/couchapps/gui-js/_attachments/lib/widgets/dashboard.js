/**
 * @file Defines the dashboard widget which provides a simple overview of
 *       selected data
 */
(function() {
  if (typeof(Mossaic.widgets.deferred) === "undefined") {
    Mossaic.widgets.deferred = [];
  }
  Mossaic.widgets.deferred = Mossaic.widgets.deferred.concat([
    function() {
      var template = "widgets.dashboard";
      /**
       * Widget which provides a simple overview of selected data
       *
       * WARNING: This is implemented using the module pattern so should not be
       * called with the "new" keyword
       *
       * @constructor
       * @param {string} divname CSS selector of the div to which this widget
       *                         should be added
       * @param {object} options Hash of options;
       * @param {Mossaic.widgets.ls_widget} options.datum_browser
       *    ls_widget which holds the selected datum metadata
       *    Mossaic.models.Meta
       * @param {Mossaic.widgets.ls_widget} options.cross_section_browser
       *    ls_widget which holds the selected cross section model
       *    Mossaic.models.CrossSection
       * @param {Mossaic.widgets.ls_widget} options.profile_browser
       *    ls_widget which holds the selected geometry model
       *    Mossaic.models.Geometry
       */
      Mossaic.widgets.dashboard = function(options) {
        var div = options.divname;
        var datum_browser = options.datum_browser;
        var cross_section_browser = options.cross_section_browser;
        var profile_browser = options.profile_browser;

        var get_name_safely = function(browser) {
          var selected = browser.get_selected();
          if (typeof(selected) !== "undefined" &&
              typeof(selected.get) === "function") {
            return selected.get("name");
          } else {
            return "";
          }
        };

        var render = function() {
          var labels = [
            {
              name: "Datum",
              value: get_name_safely(datum_browser)
            },
            {
              name: "X-sec",
              value: get_name_safely(cross_section_browser)
            },
            {
              name: "Profile",
              value: get_name_safely(profile_browser)
            }
          ];
          $(div).mustache(template, {
            labels: labels
          },{method:"html"});
        };

        datum_browser.register_listener(render);
        cross_section_browser.register_listener(render);
        profile_browser.register_listener(render);

        return {
          /**
           * Show the widget
           * @memberof Mossaic.widgets.dashboard
           * @instance
           */
          show: function() {
            render();
          }
        };
      };
    }
  ]);
})();