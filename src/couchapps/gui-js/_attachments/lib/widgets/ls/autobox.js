define([], function() {
    return Backbone.View.extend({
      /**
       * Create the view
       *
       * The view can be instantiated in two modes - normal and "input only".
       * In input_only mode the view will not browse data and just allow
       * text to be entered. This allows the view to be used to get a name
       * for a dataset, which is useful if this needs to be done in a way
       * that takes parent datasets into account.
       *
       * @memberof @AutoBox
       * @param {Backbone.Collection} collection Collection used to populate
       *                                         the widget.
       * @param {object} options Hash of options
       * @param {string} options.label Html to be displayed next to the widget
       * @param {boolean} options.focus Set to true if the widget should grab
       *                                focus when rendered
       * @param {object} options.listeners  Array of listener functions to be
       *                                    called when the selection changes
       * @param {string} options.divname  Id selector of the div to which this
       *                                  widget should be appended
       * @param {string} options.element_id Id selector to be used for the input
       *                                    element of this widget
       * @param {boolean} options.input_only  Set to true if the widget only
       *                                      allows data to be entered and
       *                                      does not perform searches
       */
      initialize: function(collection, options) {
        _.bindAll(this);
        _.extend(this, Backbone.Events);
        /**
         * The currently selected model
         * @memberof AutoBox
         * @instance
         * @type Backbone.Model
         */
        this.selected = {};
        /**
         * The name of the model that should be shown as selected
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.text = "";
        /**
         * The label to be displayed next to this widget
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.label = options.label;
        /**
         * True if the widget should grab focus when rendered
         * @memberof AutoBox
         * @instance
         * @type boolean
         */
        this.focus = options.focus;
        /**
         * The collection that determines what is matched in the autocomplete
         * or shown in the drop down list.
         * @memberof AutoBox
         * @instance
         * @type Backbone.Collection
         */
        this.collection = collection;
        this.collection.bind('change', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);
        this.collection.bind('reset', this.render);
        /**
         * Array of listener functions to be called whenever the selected
         * widget changes
         * @memberof AutoBox
         * @instance
         * @type array
         */
        this.listeners = options.listeners;
        /**
         * CSS selector of the div to which this widget should be appended
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.divname = options.divname || "#ls_widget";
        /**
         * The id of the input element of this widget
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.element_id = options.element_id ||
            this.divname.split("#")[1] + "_element";
        /**
         * The id of the button element (that brings the drop down list) of
         * this widget
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.button_id = options.element_id + "_button";
        /**
         * The template used to render the widget
         * @memberof AutoBox
         * @instance
         * @type string
         */
        this.template = 'widgets._ls.autobox';
        /**
         * Flag to set whether the widget should only be used for getting
         * the input in the search box
         * @memberof AutoBox
         * @instance
         * @type boolean
         */
        this.input_only = options.input_only;
        /**
         * Flag which indicates whether the widget should indicate that the
         * currently associated model is dirty
         * @memberof AutoBox
         * @instance
         * @type boolean
         */
        this.is_dirty = false;
      },
      /**
       * Get the model where model.name matches the supplied name. This is
       * a reverse lookup which will return the first match if there is more
       * than one model with a matching name.
       * @memberof AutoBox
       * @instance
       * @param {string} name Name of the model to be returned
       * @return {Backbone.Model} The first model in the collection that matches
       *                          the supplied name
       */
      name_to_model: function(name) {
        // Reverse lookup - assumes name is unique which it will be if the
        // dataset was created using this gui
        var selected_models = this.collection.filter(function(model) {
          return model.get("name") == name;
        });
        if (selected_models.length > 1) {
          console.log("More than one item matches the name " +
              name + ", using the first match.");
        } else if (selected_models.length == 0) {
          throw "Model with matching name does not exist";
        }
        return selected_models[0].toJSON();
      },
      /**
       * Indicate that there are unpersisted changes
       * @memberof AutoBox
       * @instance
       * @param {boolean} is_dirty True if the widget should indicate there are
       *                           unpersisted changes, false otherwise.
       */
      show_dirty_indicator: function(is_dirty) {
        this.is_dirty = is_dirty;
        var element = this.$el.find("#" + this.element_id);
        if (this.is_dirty) {
          element.val("New dataset");
          element.addClass("dirty");
        } else {
          element.removeClass("dirty");
        }
      },
      /**
       * Show a placeholder. This is usually called when the collection is
       * fetched, so that the widget displays something to the user to let them
       * know something is happening.
       * @memberof AutoBox
       * @instance
       */
      show_placeholder: function() {
        var img = this.$el.find(this.divname + " img");
        if (img.size() === 0) {
          this.$el.find(this.divname + " .ls").append(
              "<img \
                style=\"float: left;position: relative; top: -30px; left: 5px;\"\
                src=\"image/ajax-loader.gif\" />");
        }
      },
      /**
       * Render the widget
       * @memberof AutoBox
       * @instance
       */
      render: function() {
        if (this.hidden) {
          this.$el.html("");
          return;
        }
        var that = this;
        var names = this.collection.map(function(model) {
          return model.get("name");
        });
        this.$el.mustache(this.template, {
          element_id: this.element_id,
          button_id: this.button_id,
          label: this.label
        },{ method: 'html' });
        var element = this.$el.find("#" + this.element_id);
        if (this.input_only) {
          this.$el.find("#" + this.button_id).remove();
          element.change(function() {
            that.text = element.val();
            that.selected = {name: element.val()};
          });
        } else {
          element.blur(function(event) {
            //event.preventDefault();
          });
          element.autocomplete({
            minLength: 0,
            source: names,
            change: function() {
              that.text = element.val();
            },
            select: function(event, selected) {
              that.selected = that.name_to_model(selected.item.value);
              _.each(that.listeners, function(listener) {
                listener({update_model: true});
              });
            }
          });
        }

        // Previously selected item should still be selected even though
        // items have changed
        if (this.text && !this.is_dirty) {
          element.val(this.text);
          try {
            var current_selection = this.name_to_model(this.text);
            this.selected = current_selection;
          } catch(e) {
            console.log("Current text does not match any current option");
          }
          this.text = undefined;
        } else {
          if (this.selected && !this.is_dirty) {
            element.val(this.selected.name);
          } else {
            element.val("New dataset");
            element.addClass("dirty");
          }
        }

        if (this.focus) {
          element.focus();
        }

        if (!this.input_only) {
          // Bring up all the options
          this.$el.find("#" + this.button_id).click(function() {
            if (element.autocomplete("widget").is(":visible")) {
              element.autocomplete("close");
            } else {
              element.autocomplete("search", "");
              element.focus();
            }
          });
        }
        return this.$el;
      },
      /**
       * Get the currently selected model
       * @memberof AutoBox
       * @instance
       */
      get_selected: function() {
        return this.selected;
      },
      /**
       * Set the text to be shown for the default selection
       * @memberof AutoBox
       * @instance
       * @param {string} value Default selection text
       */
      set_default_selection: function(value) {
        this.text = value;
      }
    })

});