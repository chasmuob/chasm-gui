define([], function() {
  return Backbone.View.extend({
      /**
       * Create the view
       *
       * The view can be instantiated in two modes - normal and "input only".
       * In input_only mode the view will not browse data and just allow
       * text to be entered. This allows the view to be used to get a name
       * for a dataset, which is useful if this needs to be done in a way
       * that takes parent datasets into account.
       *
       * @memberof @FilterList
       * @param {Backbone.Collection} collection Collection used to populate
       *                                         the widget.
       * @param {object} options Hash of options
       * @param {string} options.label Html to be displayed next to the widget
       * @param {boolean} options.focus Set to true if the widget should grab
       *                                focus when rendered
       * @param {object} options.listeners  Array of listener functions to be
       *                                    called when the selection changes
       * @param {string} options.divname  Id selector of the div to which this
       *                                  widget should be appended
       * @param {string} options.element_id Id selector to be used for the input
       *                                    element of this widget
       * @param {boolean} options.input_only  Set to true if the widget only
       *                                      allows data to be entered and
       *                                      does not perform searches
       * @param {string} options.height CSS string specifying the desired height
       *                                of the scrolling list box
       */
      initialize: function(collection, options) {
        _.bindAll(this);
        /**
         * The currently selected model
         * @memberof FilterList
         * @instance
         * @type Backbone.Model
         */
        this.selected = {};
        /**
         * The name of the model that should be shown as selected
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.text = "";
        /**
         * The label to be displayed next to this widget
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.label = options.label;
        /**
         * True if the widget should grab focus when rendered
         * @memberof FilterList
         * @instance
         * @type boolean
         */
        this.focus = options.focus;
        /**
         * The collection that determines what is matched in the autocomplete
         * or shown in the drop down list.
         * @memberof FilterList
         * @instance
         * @type Backbone.Collection
         */
        this.collection = collection;
        this.collection.bind('change', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);
        this.collection.bind('reset', this.render);
        /**
         * Array of listener functions to be called whenever the selected
         * widget changes
         * @memberof FilterList
         * @instance
         * @type array
         */
        this.listeners = options.listeners;
        /**
         * CSS selector of the div to which this widget should be appended
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.divname = options.divname || "#ls_widget";
        /**
         * The id of the input element of this widget
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.element_id = options.element_id ||
        this.divname.split("#")[1] + "_element";
        /**
         * Flag to set whether the widget should only be used for getting
         * the input in the search box
         *
         * Currently not honoured by this widget
         *
         * @memberof FilterList
         * @instance
         * @type boolean
         */
        this.input_only = options.input_only;
        /**
         * Flag which indicates whether the widget should indicate that the
         * currently associated model is dirty
         * @memberof FilterList
         * @instance
         * @type boolean
         */
        this.is_dirty = false;
        /**
         * String to be used to filter the displayed items by model name
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.filter_val = "";
        /**
         * CSS string to specify the height of the widget
         * @memberof FilterList
         * @instance
         * @type string
         */
        this.height = options.height;
      },
      /**
       * The template used to render the widget
       * @memberof FilterList
       * @instance
       * @type string
       */
      template: "widgets._ls.filterlist",
      /**
       * Show a placeholder. This is usually called when the collection is
       * fetched, so that the widget displays something to the user to let them
       * know something is happening.
       * @memberof FilterList
       * @instance
       */
      show_placeholder: function() {
        var img = $(this.divname + " img");
        if (img.size() === 0) {
          $(this.divname + " .ls").append(
              "<img \
                style=\"float: left;position: relative; top: -30px; left: 5px;\"\
                src=\"image/ajax-loader.gif\" />");
        }
      },
      /**
       * Indicate that there are unpersisted changes
       * @memberof FilterList
       * @instance
       * @param {boolean} is_dirty True if the widget should indicate there are
       *                           unpersisted changes, false otherwise.
       */
      show_dirty_indicator: function(is_dirty) {
        this.is_dirty = is_dirty;
        var element = this.$el.find("#" + this.element_id);
        if (this.is_dirty) {
          element.val("New dataset");
          element.addClass("dirty");
        } else {
          element.removeClass("dirty");
        }
      },
      /**
       * Set the currently selected model to the model matching the id of the
       * DOM element that triggered the event
       * @memberof FilterList
       * @instance
       * @param {jQuery.Event} event The event that triggered the function
       */
      select: function(event) {
        var that = this;
        this.default_selection = undefined;
        if (typeof(this.selected) !== "undefined" &&
            typeof(this.selected.get) !== "undefined") {
          var indexOfSelected;
          this.collection.find(function(model, i) {
            indexOfSelected = i;
            return model.get("id") === that.selected.get("id");
          });
          this.$el.find("#" + this.element_id + "_" + indexOfSelected).removeClass("selected");
        }
        var target_id_tokens = event.target.id.split('_');
        var index = target_id_tokens[target_id_tokens.length - 1];
        var new_selection = this.collection.at(index);
        if (this.selected !== new_selection) {
          this.selected = this.collection.at(index);
          this.$el.find("#" + event.target.id).addClass("selected");
        } else {
          this.selected = {};
        }
        _.each(this.listeners, function(listener) {
          listener({update_model: true, clear_model: true});
        });
      },
      /**
       * Filter the displayed models to only those that match the string entered
       * in the filter text box
       * @memberof FilterList
       * @instance
       */
      filter: function() {
        var that = this;
        this.filter_val = this.$el.find("#" + this.element_id + "_filter").val();
        this.collection.forEach(function(row, i) {
          if (row.get("name").indexOf(that.filter_val) >= 0) {
            this.$el.find("#" + that.element_id + "_" + i).css("display", "block");
          } else {
            this.$el.find("#" + that.element_id + "_" + i).css("display", "none");
          }
        });
      },
      /**
       * Render the widget
       * @memberof FilterList
       * @instance
       */
      render: function(options) {
        var options = options || {};
        if (this.hidden) {
          this.$el.html("");
          return;
        }
        var that = this;
        var rows = _.map(this.collection.toJSON(), function(row, i) {
          row.row_id = that.element_id + "_" + i;
          row.click_class = that.element_id + "_clickable";
          row.display = "block";
          if (typeof(that.filter_val) !== "undefined" &&
              that.filter_val.length > 0) {
            if (row.name.indexOf(that.filter_val) < 0) {
              row.display = "none";
            }
          }
          return row;
        });
        this.$el.mustache(this.template, {
          element_id: this.element_id,
          button_id: this.button_id,
          label: this.label,
          rows: rows,
          filter_id: this.element_id + "_filter",
          filter_val: this.filter_val
        },{ method: 'html' });
      
        if (typeof(this.default_selection) !== "undefined" &&
            this.default_selection !== "") {
          this.selected = this.collection.find(function(model) {
            return model.get("name") === that.default_selection;
          });
          _.each(this.listeners, function(listener) {
            listener({update_model: true, clear_model: true});
          });
        }
        this.collection.forEach(function(item, i) {
          if (typeof(item.get) === "function" &&
              typeof(that.selected.get) === "function" &&
              item.get("id") === that.selected.get("id")) {
            that.$el.find("#" + that.element_id + "_" + i).addClass("selected");
          }
        });
        if (typeof(options.height) !== "undefined") {
          this.height = options.height;
        }
        if (this.height !== "undefined") {
          this.$el.find(this.divname + " .filterlist").css("height", this.height);
        }
        this.$el.find("." + that.element_id + "_clickable").click(this.select);
        this.$el.find("#" + that.element_id + "_filter").keyup(this.filter);
        return this.$el;
      },
      /**
       * Get the currently selected model
       * @memberof FilterList
       * @instance
       */
      get_selected: function() {
        return this.selected || {};
      },
      /**
       * Select a model whose name field that matches the supplied name. If
       * multiple matches exist only the first is used.
       * @memberof FilterList
       * @instance
       * @param {string} name Name of the model to be selected
       */
      select_by_name: function(name) {
        var that = this;
        var indexOfSelected;
        if (typeof(that.selected) !== "undefined" &&
            (typeof(that.selected.get) !== "undefined")) {
          this.collection.find(function(model, i) {
            indexOfSelected = i;
            return model.get("id") === that.selected.get("id");
          });
          this.$el.find("#" + this.element_id + "_" + indexOfSelected).removeClass("selected");
        }
        // Find item where name matches value
        var indexToSelect;
        var to_select = this.collection.find(function(model, i) {
          indexOfSelected = i;
          return model.name === name;
        });
        this.$el.find("#" + this.element_id + "_" + indexToSelect).addClass("selected");
        this.selected = to_select;
      },
      /**
       * Set the text to be shown for the default selection and select the
       * model whose name matches that text
       * @memberof FilterList
       * @instance
       * @param {string} value Default selection text
       */
      set_default_selection: function(value) {
        var that = this;
        this.default_selection = value;
        this.select_by_name(this.default_selection);
      },
      /**
       * Unselect whatever is currently selected
       * @memberof FilterList
       * @instance
       * @param {object} options Hash of options
       * @param {boolean} options.clear_model True if the selected model should
       *                                      actually be cleared, false if it
       *                                      should just be unselected.
       */
      unselect: function(options) {
        var options = options || {};
        this.selected = {};
        this.default_selection = undefined;
        this.render();
        _.each(this.listeners, function(listener) {
          listener({update_model: true, clear_model: options.clear_model});
        });
      }
    })  
  
});