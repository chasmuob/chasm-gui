define(["widgets/_basic.rj"], function(BasicWidget) {
  /**
   * Widget that displays a Bootstrap modal dialog box to enable users to create
   * and edit models.
   * 
   * @param {object} options
   * @param {string} options.divname The div to insert the New and Edit buttons into (must start with "#")
   * @param {string} options.template String specifying the Mustache template for the form
   * @param {ls_widget} options.browser The "ls" widget (if any) this is attached to. 
   * @param (object} options.Model  The model "class" to create and pass to the dialog box if "new" is clicked
   * @returns {undefined}
   */
  return BasicWidget.extend({
    initialize: function(options) {
      var that = this;
      BasicWidget.prototype.initialize.apply(this,[options]);
      this.browser = options.browser;
      this.dialog = options.dialog;
      this.Model = options.Model;
      
      this.$el.mustache("widgets.edit_model", {id: this.id}), {method: "html"};

      var $new_btn = this.$el.find(".new");
      $new_btn.enable(false); // Initially set to disabled
      $new_btn.click(function() {
        that.browser.unselect({clear_model: true});
        var model = new that.Model();   // Create a new object of the model "class"
        // Now need to set the parent ID for the new model:
        var parent_model_id = that.browser.get_parent_id(); // Get the id of the model selected in the parent browser 
        if (typeof(parent_model_id) !== undefined) {
          model.set("parent_id", parent_model_id);
        }
        that.dialog.set_model(model);
        that.dialog.show();
      });

      var $edit_btn = this.$el.find(".edit");
      $edit_btn.click(function() {
        that.dialog.set_model(that.browser.get_selected_model());
        that.dialog.show();
      });
      $edit_btn.enable(false);
      
      // Register a listener on the LS browser that is called whenever a (de)selection is made.  
      // If we don't have an entry selected, then disable the edit btn.
      this.browser.register_listener(function() {
        if (typeof(that.browser.get_selected().id) === "undefined") {
          $edit_btn.enable(false);  // Strangely, jQ doesn't seem to have a disable() fn, we do enable(false) instead...
        } else {
           $edit_btn.enable(true);
        }
      });

      // Register a listener on the *parent* LS browser that is called whenever a (de)selection is made.  
      this.parent_browser = this.browser.get_parent();
      if (typeof(this.parent_browser) !== "undefined") { 
        // If we don't have an entry selected, then disable the new btn.
        this.parent_browser.register_listener(function() {
          if (typeof(that.parent_browser.get_selected().id) === "undefined") {
            $new_btn.enable(false);
          } else {
             $new_btn.enable(true);
          }
        });
      } else {
        // We don't have a parent, so just enable the button
        $new_btn.enable(true);
      }

      
    },

    render : function() {
      return this.el;
    }

  });

}); 