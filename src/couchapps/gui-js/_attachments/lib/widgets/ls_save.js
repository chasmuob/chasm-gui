/**
 * @file Defines a widget for listing CouchDB documents
 */
(function() {
  /**
   * Create a ls_save widget. Basically just a wrapper around ls, save and
   * clear_model widgets, which allows them to be specified in one shot and
   * bound to a single div.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {string} options.type The widget will only handle documents where
   *                              doc.type is equal to this field
   * @param {object} options.ls Options to be passed to the ls widget (see
   *                            Mossaic.widgets.ls)
   * @param {object} options.save Options to be passed to the save wdiget (see
   *                              Mossaic.widgets.save)
   * @param {object} options.clear_model Options to be passed to the clear_model
   *                                     widget (see
   *                                     Mossaic.widgets.clear_model)
   * @return {Mossaic.widgets.ls} ls widget, with show and hide methods that
   *                              have been extended to show/hide the associated
   *                              save and clear_model widgets
   */
  Mossaic.widgets.ls_save = function(options) {
    var options = options || {};
    var ls_options = options.ls || {};
    var save_options = options.save || {};
    var clear_model_options = options.clear_model || {};
    ls_options.type = options.type;
    var ls = Mossaic.widgets.ls(ls_options);
    
    // If we have been passed a name_source, then concatenate it into an array
    // with the one we created above
    if (typeof(save_options.name_source) !== "undefined") {
      save_options.name_source = save_options.name_source.concat([ls]);
    } else {
      // Otherwise if we haven't been passed a name_source, just set the one we created above.
      save_options.name_source = ls;
    }
    save_options.type = options.type;
    var save = Mossaic.widgets.save(save_options);
    
    clear_model_options.name_source = ls;
    var clear_model = Mossaic.widgets.clear_model(clear_model_options);
    // Bit of a hack - copy the ls_widget hide/show functions, then replace
    // them with functions that call the other widgets in this composite
    var ls_hide = ls.hide;
    var ls_show = ls.show;
    ls.hide = function() {
      save.hide();
      clear_model.hide();
      ls_hide();
    }
    ls.show = function(options) {
      save = Mossaic.widgets.save(save_options);
      clear_model = Mossaic.widgets.clear_model(clear_model_options);
      ls_show(options);
    }
    return ls;
  }
})();