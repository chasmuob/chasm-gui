/**
 * @file Defines a tabs widget which creates selection tabs
 */
(function() {
  /**
   * A widget which creates a row of tabs from supplied tab data. The widget
   * defines two tab groups: Slope Profile and Simulation Parameters. Tabs from
   * the second group cannot be selected unless the associated Geometry model
   * (as defined by options.model) is clean. If a tab from the second group is
   * open and a tab from the first group is selected, the user is asked to
   * confirm and if they do the Geometry model is set to dirty and must be
   * re-saved before Simulation Parameters tabs can be selected again.
   *
   * This behaviour supports a linear workflow, where simulation parameters
   * cannot be defined until a slope profile is set.
   *
   * The actual action taken on selecting a tab is defined externally and
   * passed in via options.on_select.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {string} options.divname CSS selector of the div to which this
   *                                 widget should be appended
   * @param {function} options.on_select Function to call when a tab is
   *                                    selected
   * @param {function} options.save_geometry Function to call when the geometry
   *                                         needs saving
   * @param {array} options.tab_data Array of tab data which should consist
   *                                 of objects which must have a key "name"
   *                                 where the value is a string to be displayed
   *                                 on the tab
   * @param {Mossaic.models.Geometry} options.model Model that must be clean in
   *                                                order to advance to the
   *                                                parameters tabs
   */
  Mossaic.widgets.tabs = function(options) {
    var MossaicTabs = Backbone.View.extend({
      initialize: function(options) {
        var options = options || {};
        this.divname = options.divname;
        this.on_select = options.on_select;
        this.save_geometry = options.save_geometry;
        this.update_tab_data(options.tab_data);
        this.model = options.model;
        /**
         * The index of the tab to jump to on init if the associated model is
         * clean
         * @instance
         * @constant
         * @type integer
         */
        this.first_tab_if_clean = 3;
      },
      template: "widgets.tabs",
      /**
       * Update the tab data with the supplied array
       * @memberof Mossaic.widgets.tabs
       * @instance
       * @param {object} options Hash of options
       * @param {array} options.tab_data Array of tab data which should consist
       *                                 of objects which must have a key "name"
       *                                 where the value is a string to be
       *                                 displayed on the tab
       */
      update_tab_data: function(tab_data) {
        this.tab_data = _.map(tab_data, function(view, i) {
          return {
            name: view.name,
            index: i,
            width: "" + (100 / tab_data.length) + "%"
          };
        });
      },
      /**
       * Set the geometry model used to determine which tabs are allowed
       * @memberof Mossaic.widgets.tabs
       * @instance
       * @param {Mossaic.models.Geometry} model Model used to determine allowed
       *                                        tabs
       */
      set_model: function(model) {
        this.model = model;
      },
      /**
       * Render the tabs
       * @memberof Mossaic.widgets.tabs
       * @instance
       * @private
       */
      render: function() {
        var that = this;
        var tab_groups = [
          {
            name: "Slope profile",
            width: (100 * (this.first_tab_if_clean / this.tab_data.length)) + "%"
          },
          {
            name: "Simulation parameters",
            width: (100 * ((this.tab_data.length - this.first_tab_if_clean) / this.tab_data.length)) + "%"
          }
        ];
        var tabs_to_render = _.map(this.tab_data, function(d, i) {
          var tab_data_to_render = _.clone(d);
          if (that.model.is_dirty) {
            if (i < that.first_tab_if_clean) {
              tab_data_to_render.tab_class = "tab_unselected";
            } else {
              tab_data_to_render.tab_class = "tab_disabled";
            }
          } else {
            if (i < that.first_tab_if_clean) {
              tab_data_to_render.tab_class = "tab_disabled";
            } else {
              tab_data_to_render.tab_class = "tab_unselected";
            }
          }
          return tab_data_to_render;
        });
        $(this.divname).mustache(this.template, {
          tabs: tabs_to_render,
          groups: tab_groups
        },{method:"html"});
        $(".tab_unselected").click(function(event) {
          var index = event.target.id.split("_")[1];
          that.on_select(index);
        });
        $(".tab_disabled").click(function(event) {
          var confirmed;
          var index = parseInt(event.target.id.split("_")[1]);
          if (that.model.is_dirty) {
            confirmed = confirm("You must save the geometry before you can edit simulation parameters, ok?");
            if (confirmed) {
              that.save_geometry({model_to_save: that.model, index: index});
            }
          } else {
            confirmed = confirm("This will change the geometry, ok?");
            if (confirmed) {
              that.model.is_dirty = true;
              that.model.changed_by = this;
              that.model.change();
              that.on_select(index);
            }
          }
        })
      },
      /**
       * Force an update of the tab view and select a specific tab
       * @memberof Mossaic.widgets.tabs
       * @instance
       * @param {integer} index Index of the tab to be selected
       */
      update: function(index) {
        var that = this;
        this.render();
        $(".tab_selected").removeClass("tab_selected");
        $("#tab_" + index).addClass("tab_selected");
      }
    });
    return new MossaicTabs(options);
  };
})();