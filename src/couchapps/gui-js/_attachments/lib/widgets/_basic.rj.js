define(["views/_basic.rj"], function(BasicView) {
  /**
   * Widgets are intended to be small, reusable graphical items that do a specific task.  This definition has been 
   * somewhat stretched in places but the idea is there.
   * 
   * As with almost everything in Backbone/Mossaic, they are more convention than anything else.
   * 
   */
  // Just do nothing for now, might do something smarter in future
  return BasicView;
  
});