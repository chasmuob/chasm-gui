/**
 * @file Defines the task_builder widget for creating tasks
 */
(function() {
  /**
   * Create a task_builder widget.
   *
   * Provides single view which allows users to review task inputs, enter
   * metadata, enter names for unsaved data, save any unsaved datasets and
   * save the task request.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {array} options.input_sources Array of ls widgets that provide the
   *                                      input datasets for this task
   * @param {Mossaic.widgets.ls} options.meta_source ls widget that provides the
   *                                                 slope datum dataset for
   *                                                 this task
   * @param {Mossaic.widgets.ls} options.cross_section_source
   *                                                  ls widget that provides
   *                                                  the cross-section dataset
   *                                                  for this task
   * @param {Mossaic.models.Task} options.model   Task model that represents the
   *                                              task being built
   * @param {string} options.divname              CSS selector of the div to
   *                                              which the widget will be
   *                                              appended
   */
  Mossaic.widgets.task_builder = function(options) {
    var input_sources = options.input_sources;
    var meta_source = options.meta_source;
    var cross_section_source = options.cross_section_source;
    var next_route = options.next_route;
    var db_name = Mossaic.db.get_db_name({type: "task"});

    var default_chasm_output_files = {
      factor_of_safety: true,
      pressure_head: false,
      soil_moisture_content: false,
      total_soil_moisture: false,
      vegetation_interception: false
    };
    var divname = options.divname;

    var task_model = options.model || new Mossaic.models.Task({});
    task_model.set({
      output_files: default_chasm_output_files
    }, {silent: true});
    task_model.urlRoot = db_name;

    /**
     * Validate the task
     *
     * Client-side task validation as follows:
     *  - for each required model
     *   - is it persisted
     *  - are all task fields completed?
     *  - of all available models, is this a valid combination?
     *
     * Note that *model* validation is to be handled at model save time,
     * in the browser and in Couch as a validate_doc_update function.
     */
    var validate_task = function () {
      var required_inputs = task_model.get_required_inputs();
      var all_inputs = _.clone(input_sources);
      all_inputs.meta = meta_source;
      all_inputs.cross_section = cross_section_source;
      var inputs_saved = _.all(_.keys(all_inputs), function(key) {
        var is_required_input = _.any(required_inputs, function(input) {
          return input == key;
        });
        if (!is_required_input) {
          return true;
        }
        var model = all_inputs[key].get_selected_model();
        if (typeof(model) === "undefined") {
          return false;
        } else {
          return !model.is_dirty;
        }
      });
      if (!inputs_saved) {
        return {
            is_valid: false,
            message: "Task has unsaved inputs"
        };
      }
      var required_fields = ["name", "time_step"];
      var all_required_fields = _.all(required_fields, function(field) {
        return typeof(task_model.get(field)) !== "undefined";
      });
      if (!all_required_fields) {
        return {
          is_valid: false,
          message: "Task description is incomplete"
        }
      }
      return {
        is_valid: true
      };
    };

    /**
     * Get the input files from the input selection widgets
     */
    var get_input_files = function() {
      var input_files = {};
      _.each(_.keys(input_sources), function(key) {
        var selected_input = input_sources[key].get_selected_model();
        if (typeof selected_input !== "undefined") {
          input_files[key] = selected_input.get("id");
        }
      });
      return input_files;
    };

    /**
     * Populate the application selector - backbone seems overkill for this
     * operation so we just use jquery.couch.js
     */
    var populate_executables = function(task_view) {
      var dbname = Mossaic.db.get_db_name({type: "release"});
      var db = $.couch.db(dbname);
      db.view("release/releases", {
        success: function(results) {
          var executables = [];
          for (var i in results.rows) {
            row = results.rows[i];
            executable_attributes = get_executable_attributes(row.key);
            executables.push({
              id: row.id,
              application: executable_attributes.application,
              version: executable_attributes.version
            });
          }
          task_view.all_executables = executables;
          if (!task_view.model.has("executable")) {
            task_view.model.set({
              executable: {
                application: executables[0].application,
                version: executables[0].version
              }
            });
          }
          task_view.model.change();
        },
        error: function(response) {
          alert("Could not fetch application list");
        }
      });
    };

    var get_executable_attributes = function(executable_id) {
      var tokens = executable_id.split("_");
      return {
        application: tokens[0],
        version: tokens[1]
      };
    };

    var get_duration = function() {
      var boundary_conditions = input_sources["boundary_conditions"]
                                                        .get_selected_model();
      return {
        unit: "h",
        value: boundary_conditions.get_duration()
      };
    };

    var submit_task_request = function(model, userId, parent_id) {
      var validity = validate_task();
      if (!validity.is_valid) {
        alert("Could not submit task request: " + validity.message);
        return;
      }
      model.set({requestor: userId});
      // If we have an id then the task request already exists. Remove id
      // and rev attributes so task request is treated as new.
      if ("id" in model.attributes) {
        model.unset("id");
        model.unset("_id");
        model.unset("_rev");
      }
      model.set({input_files: get_input_files()});
      model.set({parent_id: parent_id});
      model.set({_id: parent_id + "|" +
          model.get("type") + ":" + model.get("name")});
      model.set({timestamp: (new Date().getTime() / 1000)});
      model.set({duration: get_duration()});
      model.save(null, {
        success: function(response) {
          console.log(response);
          alert("Task " + model.get("name") +
              " submitted with id: " + response.id);
        },
        error: function(response) {
          var message;
          if (response === 409) {
            message = "Task with name " + model.get("name") +
                " already exists for this slope profile";
          } else {
            message = "Unknown error: " + response;
          }
          alert("Could not submit task request: " + message);
        }
      });
    };

    var TaskFinalise = Backbone.View.extend({
      events: {
        "change #executable": "update_executable",
        "change #time_step": "update_time_step",
        "click #submit_task": "submit",
        "change #name": "update_name",
        "change #notes": "update_notes",
        "change #number_of_jobs": "update_number_of_jobs"
      },
      template: "widgets.task_finalise",
      all_executables: [],
      initialize: function() {
        _.bindAll(this);
        this.model.bind("change", this.render);
        this.model.view = this;
        $(divname).html(this.el);
        populate_executables(this);  // Beware - fires an async request which
                                     // updates the model on success
        this.render();
      },
      render: function() {
        var that = this;
        var relevant_executables;
        /**
         * Return a list of all task inputs
         *
         * [
         *   {
         *     type: "Geometry",
         *     name: "",
         *     is_dirty: true
         *   }
         * ]
         */
        var get_input_states = function(all_inputs) {
          return _.map(all_inputs, function(input_source) {
            var model = input_source.get_selected_model();
            var type = input_source.get_type()
            if (typeof(model) === "undefined") {
              return {
                type: type,
                type_pretty: Mossaic.utils.type_to_pretty[type],
                input_name: "",
                is_dirty: true
              }
            } else {
              var has_dirty_parent = false;
              try {
                input_source.get_parent_id();
              } catch (exception) {
                has_dirty_parent = true;
              }
              var is_stochastic = typeof(model.is_stochastic) === "function" &&
                  model.is_stochastic();
              var stochastic_or_deterministic = "";
              if (is_stochastic) {
                stochastic_or_deterministic = "(stochastic)";
              }
              var input_state = {
                type: type,
                type_pretty: Mossaic.utils.type_to_pretty[type],
                is_dirty: model.is_dirty || has_dirty_parent,
                stochastic_or_deterministic: stochastic_or_deterministic
              };
              if (!input_state.is_dirty) {
                input_state.input_name = model.get("name");
              }
              return input_state;
            }
          });
        };

        var input_states = get_input_states(_.clone(input_sources));
        var meta_states = get_input_states([meta_source, cross_section_source]);
        try {
          var geometry_type = input_sources.geometry.get_selected_model().mode;
          relevant_executables = _.filter(this.all_executables,
            function(executable) {
              return (executable.application === geometry_type);
            }
          );
        } catch (e) {
          relevant_executables = this.all_executables;
        }

        $(this.el).mustache(that.template, {
          name: this.model.get("name"),
          notes: this.model.get("notes"),
          inputs: input_states,
          meta_inputs: meta_states,
          executables: relevant_executables,
          time_step: this.model.get("time_step"),
          number_of_jobs: this.model.get("simulation_parameters").number_of_jobs
        },{method:"html"});
        if ("executable" in this.model.attributes) {
          var executable_id = (this.model.get("executable").application + "_" +
              this.model.get("executable").version).replace(/\./g, "\\.");
          $(divname + " #" + executable_id).attr("selected", "selected");
        }
        _.each(input_states.concat(meta_states), function(input_state) {
          if (!input_state.is_dirty) {
            $("#" + input_state.type).attr("readonly", true);
          }
        });
      },
      update_name: function(event) {
        this.model.set({name: $(event.target).val()});
      },
      update_executable: function(event) {
        var executable_attributes = get_executable_attributes(
            $("#executable option:selected").attr("id"));
        if (executable_attributes.application == "questa") {
          if ("output_files" in this.model.attributes) {
            this.model.unset("output_files");
          }
        } else if (executable_attributes.application == "chasm") {
          this.model.set({"output_files": default_chasm_output_files});
        }
        this.model.set({executable: executable_attributes});
      },
      update_time_step: function(event) {
        var timeStep = this.model.get("time_step");
        timeStep.value = Number($(event.target).attr("value"));
        this.model.change();
      },
      update_notes: function(event) {
        this.model.set({notes: $(event.target).val()});
      },
      update_number_of_jobs: function(event) {
        var number_of_jobs = parseInt($(event.target).val());
        var simulation_parameters = _.clone(
            this.model.get("simulation_parameters"));
        simulation_parameters.number_of_jobs = number_of_jobs;
        this.model.changed_by = this;
        this.model.set({simulation_parameters: simulation_parameters});
      },
      /**
       * Save all unsaved inputs using names given in boxes on form
       * If parents exist, they are saved before their children
       * @private
       */
      submit: function() {
        // First, make sure the executable in the task model is what is shown
        // on the form
        this.update_executable();
        var task_parent_id; // Regrettably, this gets set as a side-effect in
                            // save. This is the only way of ensuring the id
                            // is the same as that of the saved geometry object.
        /**
         * Actually save the model held by a widget
         * If parent is defined, we recursively call ourselves, saving the
         * original model on success of parent save
         */
        var save = function(input_widget, options) {
          if (typeof(input_widget.get_parent()) !== "undefined") {
            var is_parent_dirty = false;
            try {
              input_widget.get_parent_id();
            } catch (exception) {
              is_parent_dirty = true;
            }
            // If we have a dirty parent, save it and then save current widget
            // on success
            if (is_parent_dirty) {
              return save(input_widget.get_parent(), {
                success: function() {
                  return save(input_widget, options);
                }
              });
            }
          }
          // If we get here, we either have a parent, but it is saved,
          // or we don't have a parent
          // Exception thrown here if parent dirty
          var parent_id = input_widget.get_parent_id();
          var model = input_widget.get_selected_model();
          var _id;
          var name = $("#" + input_widget.get_type()).val();
          // We save, regardless of whether a model appears dirty or not
          // Checks for dirtiness are done in the calling code
          if (typeof(parent_id) !== "undefined") {
            model.set({parent_id: parent_id}, {silent: true});
            _id = parent_id + "|" + model.get("type") + ":" + name;
          } else {
            _id = model.get("type") + ":" + name;
          }
          if (model.get("type") === "geometry") {
            task_parent_id = _id;
          }
          model.unset("id", {silent: true});  // Model may have an existing id so
          model.unset("_id", {silent: true}); // kill it
          model.unset("_rev", {silent: true});// No seriously, kill it
          delete model.id;                    // Really kill it
          delete model.cid;                   // DIE!
          model.save({_id: _id, name: name}, {
            error: function(response) {
              var message;
              if (response === 409) {
                message = "" + Mossaic.utils.type_to_pretty[model.get("type")] +
                    " with name " + model.get("name") + " already exists";
              } else {
                message = "Unknown error: " + response;
              }
              alert("Could not save " +
                Mossaic.utils.type_to_pretty[model.get("type")].toLowerCase() +
                " data: " + message);
            },
            success: function() {
              input_widget.set_default_selection(name);
              input_widget.update_dirty_indicator(false);
              if (typeof(options.success) === "function") {
                options.success();
              } else {
                alert("Save successful for model with id: " + model.get("id") +
                  ", type: " + model.get("type") + ", name: " +
                  model.get("name"));
              }
            },
            silent: true
          });
        };
        /**
         * Wrapper for the save function which accepts a list and saves each
         * successive item when the previous item is successfully saved.
         * Because non-dirty models aren't saved and just call the success
         * function, we can just call this on all input widgets and have things
         * work correctly.
         *
         * We pass in the code to save the actual task in the on_finish arg,
         * which is called in the success function of the final item in the
         * list.
         */
        var save_wrapper = function(inputs, acc, on_finish) {
          if (inputs.length === 0) {
            return on_finish();
          } else {
            save(inputs[acc], {
              success: function() {
                if (acc < inputs.length - 1) {
                  save_wrapper(inputs, ++acc, on_finish);
                } else {
                  console.log("All inputs saved");
                  if (typeof(on_finish) === "function") {
                    return on_finish();
                  }
                }
              }
            });
          }
        };
        // The problem is that when a child is found to have a dirty parent,
        // the parent is then saved, then the child is saved
        // BUT:
        //  - parent may get saved before the child
        //    so child doesn't seem dirty
        //  - another child may be dependent, so parent is saved
        //    then we get to existing child and ohnoes
        // SO:
        //  - pass through all models and just build a list of dirty ones
        //  - this then ensures dirty children are properly identified
        //  - when we then save everything, parents always get saved before
        //    children, which means when children are saved they will correctly
        //    pick up the new parent _id
        var dirty_inputs = _.filter(input_sources, function(input_source) {
          var has_dirty_parent = false;
          try {
            input_source.get_parent_id();
          } catch (exception) {
            has_dirty_parent = true;
          }
          return (has_dirty_parent ||
            input_source.get_selected_model().is_dirty);
        });
        var validate_dirty_input = function(input_source) {
          var name = $("#" + input_source.get_type()).val();
          if (typeof(name) === "undefined" ||
              name == "") {
            return false;
          } else {
            return true;
          }
        };
        try {
          _.each(dirty_inputs, function(input_source) {
            if (!validate_dirty_input(input_source)) {
              throw "" + Mossaic.utils.type_to_pretty[input_source.get_type()] +
                " dataset must be given a valid name";
            }
          });
        } catch (exception) {
          alert(exception);
          return;
        }
        var model_to_save = this.model;
        // Get the ID of the logged-in user
        $.couch.session({
          success: function(response) {
            // Courtesy check only - actual security must happen in the
            // validate_doc_update function on the server
            if (response.userCtx.name) {
              if ($.inArray("requestor", response.userCtx.roles) != -1) {
                  save_wrapper(_.values(dirty_inputs),
                      0, function() {
                    // Save the task
                    if (typeof(task_parent_id) === "undefined") {
                      // Geometry didn't need to be saved so get current _id
                      var geometry = input_sources.geometry.get_selected_model();
                      if (!geometry.is_dirty) {
                        task_parent_id = geometry.get("_id");
                      }
                    }
                    if (typeof(task_parent_id) === "undefined") {
                      // If still undefined, we have problems
                      alert("Cannot save task - parent id is undefined");
                      return;
                    }
                    submit_task_request(model_to_save, response.userCtx.name,
                        task_parent_id);
                  });
              } else {
                alert("Sorry you do not have task requestor permissions - " +
                    "please contact the appropriate administrator");
              }
            } else {
              alert("Please log in before attempting to submit new task " +
                  "requests");
            }
          },
          error: function(response) {
            alert("Could not determine session status:" + response.toSource());
          }
        });
      }
    });

    var task_view = new TaskFinalise({
      model: task_model
    });

    return {};
  };
})();