/**
 * @file Defines the clear_model widget
 */
(function() {
  /**
   * Simple widget that clears the currently selected model for a given
   * Mossaic.widgets.ls_widget
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @param {object} options Hash of options
   * @param {string} options.divname CSS selector of the div to which this
   *                                     widget will be appended
   * @param {object} options.name_source Mossaic.widgets.ls_widget that provides
   *                                     the name that should be used for the
   *                                     model to be saved
   * @constructor
   */
  Mossaic.widgets.clear_model = function(options) {
    var divname = options.divname;
    var name_source = options.name_source || {};
    var id = divname.split("#")[1] + "_clear";
    
    $(divname).mustache("widgets.clear_model", {
        id: id,
        message: "New"
    },{ method: 'html' });
  
    $("#" + id).click(function() {
      name_source.unselect({clear_model: true});
    });
    return {
      /**
       * Show the widget by appending it to the div
       * @memberof Mossaic.widgets.clear_model
       * @instance
       */
      show: function() {
        $(divname).mustache("widgets.clear_model", {
            id: id,
            message: "New"
        },{ method: 'html' });
      },
      /**
       * Hide the widget
       * @memberof Mossaic.widgets.clear_model
       * @instance
       */
      hide: function() {
        $(divname).html("");
      }
    };
  };
})();