define([
  "widgets/_basic.rj", "widgets/ls.rj", "widgets/edit_model.rj", "widgets/button.rj",
  "dialogs/datum.rj", "dialogs/cross_section.rj",
  "forms/widgets/choose_slope/datum.rj", "forms/widgets/choose_slope/cross_section.rj"],
  function(
    BasicWidget, LsWidget, EditModelWidget, ButtonWidget,
    DatumDialog, CrossSectionDialog,
    DatumChooseSlopeWidgetForm, CrossSectionChooseSlopeWidgetForm
  ) {
  /**
   * Widget (albeit a complex one!) that is used to create and/or choose the basic models to run/analyze a simulation
   * on. The detailed simulation paramaeters are then specified later in the simulation workflow.
   *
   * It consists of three similar sections (columns in the GUI) that each contain sub widgets of:
   * widgets/ls - Retrieves and lists a Backbone.Collection for the document type specified.
   * widgets/dialog - Dialog box that contains a HTML form for editing a model
   * widgets/edit_model - The new/edit buttons.
   * forms/... - Form that is bound to the model to display information about it.  Displayed below the new/edit btns.
   *
   * If "options.models" is supplied by the creating object, a reference to it is held locally in "this.models",
   * otherwise an empty object is created.  This object holds the respective models that are selected in the Ls widget.
   * If "options.models" was supplied by the creating object, then it will have access to the models that were selected
   * in this widget.
   *
   * The edit_model widget registers a listener on the ls widget, so when a selection is made (or unmade) on the ls
   * widget, the new & edit buttons can enable or disable themselves accordingly.
   *
   * See inline docs for more info on how it all ties together.
   *
   * TODOs:
   *  - General error handling and validation around the whole parent/child chain to make sure only valid combinations
   *    get created.
   *  - Bug where a models comments stay visible after saving, but the model isn't selected in the ls widget.
   */
    return BasicWidget.extend({

      initialize: function(options) {
        BasicWidget.prototype.initialize.apply(this,[options]);

        /**
         * Object hash to hold the currently selected model for each respective ls widget.
         * If a widget doesn't have a selection, a blank model (of each type) is held.
         * Widgets can be passed a get_model() function to call when they need a reference to this model.
         */
        this.models = options.models || {};
        this.models.meta = new Mossaic.models.Meta();
        this.models.cross_section = new Mossaic.models.CrossSection();
        this.models.geometry = new Mossaic.models.Geometry();

        var that = this,
                title,
                message,
                next_route = options.next_route,
                dirty_message;
        title = "Choose a slope";
        message = "<p>Select an existing datum and cross section.</p>";
        dirty_message = "Choose a slope";

        // Render basic scaffolding
        this.$el.mustache('widgets.choose_slope', {
          title: title,
          message: message
        }, {method: "html"});

        /****************** Slope Datum ************************/

        // This widget makes a call to Couch for a list of documents of the type specified ("meta" here).
        // Couch returns a collection, which the widget lists on screen.  When a selection is made by the user,
        // the widget calls retrieves the full model details from Couch, and calls the supplied set_model() function.
        this.datum_browser = new LsWidget({
          type: "meta", // The ls widget will retrieve documents where type = "meta"
          label: "Slope datum",
          view: "FilterList", // Backbone view to display the widget (filterlist or autobox)
          set_model: function(model) {  // This fn is called from widgets.ls when a selection is made.
            that.models.meta = model;  // Update the selected model in the parent scope, so other widgets can get it.
            that.datum_form.set_model(model); // Update the details (e.g. comments) on screen
          },
          focus: true,
          init_model: this.models.meta, // Model to be selected on creation
          height: "300px"
        });
        this.add_subview("datum_browser", this.datum_browser);

        // Render the widgets output
        this.$el.find("#datum_browser").html(this.datum_browser.render());

        // Dialog box used to view/edit the datum details.
        this.datum_dialog = new DatumDialog();
        this.add_subview("datum_dialog", this.datum_dialog);

        // New/Edit buttons.
        this.datum_edit = new EditModelWidget({
          browser: this.datum_browser,    // Pass the browser in so we can change the selection if a new model is saved.
          dialog: this.datum_dialog,      // Dialog box to show when a button is clicked.
          Model: Mossaic.models.Meta      // Model "class".  The widget will instantiate a copy of this if the user
                                          // clicks "new", and pass it to the dialog box.
        });
        this.add_subview("datum_edit", this.datum_edit);
        this.$el.find("#datum_edit").html(this.datum_edit.render());

        // Area below the New/Edit buttons showing (read only) model information to aid a users selection
        this.datum_form = new DatumChooseSlopeWidgetForm({
          model: that.models.meta  // We pass that.models.meta in here, rather than a get_model() in the other
                                    // widgets because forms need a model bound at creation.  The form is updated by
                                    // the Ls widget above calling set_model() on this form when the selection changes.
        });
        this.add_subview("datum_form", this.datum_form);
        this.$el.find("#datum_form").html(this.datum_form.render());

        /****************** Cross Section ************************/
        this.cross_section_browser = LsWidget({
          type: "cross_section",
          label: "Cross section",
          parent: this.datum_browser,
          view: "FilterList",
          set_model: function(model) {
            that.models.cross_section = model;
            that.cross_section_form.set_model(model);
            that.models.geometry = new Mossaic.models.Geometry({parent_id: model.id});
          },
          focus: false,
          init_model: this.cross_section,
          clear_model_on_parent_change: true,
          height: "300px"
        });
        this.add_subview("cross_section_browser", this.cross_section_browser);
        this.$el.find("#cross_section_browser").html(this.cross_section_browser.render());

        this.cross_section_dialog = new CrossSectionDialog();
        this.add_subview("cross_section_dialog", this.cross_section_dialog);

        this.cross_section_edit = new EditModelWidget({
          browser: this.cross_section_browser,
          dialog: this.cross_section_dialog,
          Model: Mossaic.models.CrossSection
        });
        this.add_subview("cross_section_edit", this.cross_section_edit);
        this.$el.find("#cross_section_edit").html(this.cross_section_edit.render());

        this.cross_section_form = new CrossSectionChooseSlopeWidgetForm({model:this.models.cross_section});
        this.add_subview("cross_section_form", this.cross_section_form);
        this.$el.find("#cross_section_form").html(this.cross_section_form.render());

        /******************** Slope Geometry ************************************/
        this.profile_browser = LsWidget({
          label: "Slope Geometry",
          type: "geometry",
          parent: this.cross_section_browser,
          view: "FilterList",
          set_model: function(model) {
            if (typeof(that.slope_editor) !== "undefined") {
              that.slope_editor.set_geometry(model);
            }
            if (typeof(that.slope_editor_linear) !== "undefined") {
              that.slope_editor_linear.set_geometry(model);
            }
            if (typeof(that.geometry_metadata_form) !== "undefined") {
              that.geometry_metadata_form.set_geometry(model);
            }
            that.models.geometry = model;
          },
          init_model: this.geometry,
          clear_model_on_parent_change: true,
          height: "300px"
        });
        this.profile_browser.reset();
        this.$el.find("#profile_browser").html(this.profile_browser.render());

        this.new_button = new ButtonWidget({
          text:"New",
          click : function(){
            that.models.geometry = new Mossaic.models.Geometry({
              parent_id: that.models.cross_section.id
            });
            window.location.href = "#task-composer/edit-params/define";
          }
        });

        this.edit_button = new ButtonWidget({
          text:"Edit",
          click : function(){
            var item = that.profile_browser.get_selected();
            that.models.geometry = item;
            window.location.href = "#task-composer/edit-params/id/"+item.get('id') + "/confirmation";
          }
        });

        this.delete_button = new ButtonWidget({
          text:"Delete",
          class:"btn-danger",
          click: function(){
            var userConfirm = window.confirm('Are you sure you wish to delete this profile?');
            if(!userConfirm){
              return;
            }

            var item = that.profile_browser.get_selected();
            $.couch.db('chasm').openDoc(item.get('id'),{
              success: function(data){
                $.couch.db('chasm').removeDoc(data,{
                  success: function(){
                    console.log('Deleted!');
                    window.location.reload();
                  }
                });
              }
            });
          }
        });

        that.delete_button.disable();
        that.edit_button.disable();

         // Register a listener on the cross section browser that is called whenever a (de)selection is made.
        // If we don't have an entry selected, then disable the above btn.
        this.cross_section_browser.register_listener(function() {
          if (typeof(that.cross_section_browser.get_selected().id) === "undefined") {
            that.new_button.disable();
          } else {
            that.new_button.enable();
          }
        });

        this.profile_browser.register_listener(function(){
          if (typeof(that.profile_browser.get_selected().id) === "undefined") {
            that.delete_button.disable();
            that.edit_button.disable();
          } else {
            that.delete_button.enable();
            that.edit_button.enable();
          }
        });


        this.$el.find("#profile_edit").append(this.new_button.render());
        this.$el.find("#profile_edit").append(this.edit_button.render());
        this.$el.find("#profile_edit").append(this.delete_button.render());

        if (typeof(this.geometry_metadata_form) === "undefined") {
          this.geometry_metadata_form = new Mossaic.forms.GeometryMetadata(this.geometry, {
            divname: "#profile_form"
          });
        }
        this.geometry_metadata_form.show();
        this.profile_save = Mossaic.widgets.save({
          divname: "#profile_edit",
          type: "geometry",
          get_model: function() {
            return [that.models.meta, that.models.cross_section, that.models.geometry];
          },
          name_source: [this.datum_browser, this.cross_section_browser,
            this.profile_browser],
          success: function() {
            if (typeof(next_route) !== "undefined") {
              window.location.href = "#" + next_route;
            }
          },
          // This function will be called after parents are confirmed as clean.
          // The result is that once we know parent models are persisted we check
          // we have the required fields in the geometry metadata form then
          // navigate to the next route.
          instead_of_save: function() {
            if (typeof(that.profile_browser.get_selected().id) === "undefined") {
              that.geometry.clear();

              if (that.geometry_metadata_form.executable === "questa") {
                if (((typeof(that.geometry_metadata_form.upslope_sections) === "undefined" ||
                        typeof(that.geometry_metadata_form.downslope_sections) === "undefined") ||
                        (that.geometry_metadata_form.downslope_sections === 0 &&
                                that.geometry_metadata_form.upslope_sections === 0)) &&
                        that.models.geometry.is_dirty) {
                  alert("Enter number of upslope and downslope sections");
                  return;
                }
                var datum_has_road_link = typeof(that.models.meta) !== "undefined" &&
                        typeof(that.models.meta.get("road_link")) !== "undefined" &&
                        that.models.meta.get("road_link").length === 2;
                if (!datum_has_road_link) {
                  alert("Cannot use a Questa slope profile for a datum with no " +
                          "defined road link");
                  return;
                }
              }
            }
            if (typeof(next_route) !== "undefined") {
              window.location.href = "#" + next_route;
            }
          },
          allow_save: false,
          dirty_message: "Create new profile",
          clean_message: "Use this profile",
          clear_model_on_parent_change: true,
          css_class: "wide-button"
        });
      },

      render: function(options) {
        return this.el;
      }
    });
  }
);
