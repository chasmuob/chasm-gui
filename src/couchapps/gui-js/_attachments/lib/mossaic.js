/**
 * @file The top-level file for the application and the only script that needs
 *       to be included. It sets up the top-level namespace and loads the
 *       scripts that build the child namespaces.
 */
(function($) {
  var scripts_to_load = [
      "lib/utils.js",
      "lib/models.js",
//      "lib/workflow.js",
      "lib/db.js",
      "lib/widgets.js",
      "lib/forms.js",
      "lib/vis.js",
//      "lib/editors.js",
    ],
    load_scripts = function(scripts) {
      for (var i=0; i < scripts.length; i++) {
        document.write('<script src="'+scripts[i]+'"><\/script>');
      }
    };

  /**
   * The top level namespace for the MoSSaiC Application Platform GUI
   * @namespace
   */
  Mossaic = {};

  load_scripts(scripts_to_load);  
})(jQuery);