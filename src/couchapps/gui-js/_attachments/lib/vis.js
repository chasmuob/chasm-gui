/**
 * @file Define the visualisations for input data used in the app
 */
(function() {
  var scripts_to_load = [
      "lib/vis/slope.js",
      "lib/vis/stability.js"
  ];

  /**
   * Namespace for visualisations of CHASM/QUESTA input data
   * @namespace
   */
  Mossaic.vis = {};

  var load_scripts = function(scripts) {
    for (var i=0; i < scripts.length; i++) {
      document.write('<script src="'+scripts[i]+'"><\/script>');
    }
  };

  load_scripts(scripts_to_load);
})();