/**
 * Backbone Model for representing results data
 *
 * @constructor
 * @extends Mossaic.models.Basic
 */

  Mossaic.models.Results = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Results
     * @instance
     * @type object
     */
    defaults: {
      type: "results"
    }
  });
