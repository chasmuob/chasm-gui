/**
 * Backbone Model for representing task requests
 *
 * @constructor
 * @extends Mossaic.models.Basic
 */

  Mossaic.models.Task = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Task
     * @instance
     * @type object
     */
    defaults: {
      type: "task",
      requestor: "TODO - get logged-in from profile widget",
      input_files: {},
      output_variables: {
        "factor_of_safety": true,
        "pressure_head": false,
        "soil_moisture_content": false,
        "total_soil_moisture": false,
        "vegetation_interception": false
      },
      duration: {
        "unit": "h",
        "value": 360
      },
      time_step: {
        "unit": "s",
        "value": 60
      },
      report_filename: "output_summary",
      simulation_parameters: {
        "number_of_jobs": 1
      }
    },
    /**
     * Required inputs for each application type
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     */
    required_inputs: {
      chasm: ["boundary_conditions", "geometry", "meta", "soils",
          "stability"],
      questa: ["boundary_conditions", "geometry", "meta", "soils",
          "stability", "road_network", "engineering_cost"]
    },
    /**
     * Return the mandatory input types for the current application
     *
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     * @return {array} Array of required input types for the current application
     */
    get_required_inputs: function() {
      var executable = this.get("executable");
      var application;
      if (typeof(executable) !== "undefined") {
        application = executable.application;
      }
      if (typeof(application) !== "undefined") {
        if (application in this.required_inputs) {
          return this.required_inputs[application];
        } else {
          throw("Unsupported application: " + application);
        }
      } else {
        throw ("No application selected");
      }
    }
  });
