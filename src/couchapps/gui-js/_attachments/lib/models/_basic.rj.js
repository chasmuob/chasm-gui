define([], function() {
  /**
   * This file isn't currently used by anything.  Here as was going to investigate using DeepModel to avoid the whole
   * faffing that is needed to set nested attributes in default backbone models. i.e.
   *
   *  var obj = _.clone(model.get("obj"));
   *  obj.param = "something"
   *  model.set("obj",obj)
   *
   * Be nice to do instead:
   * model.set("obj.param", "something")
   *
   * DeepModel appears to work fine, but haven't had time to test it properly so have taken it back out again.
   */
  return Backbone.DeepModel.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Basic
     * @instance
     * @type object
     * @property {boolean} is_dirty True if newly initialised models should be
     *                              considered dirty (i.e. have unpersisted
     *                              changes).
     */
    defaults: {
      is_dirty: false
    },
    /**
     * Initialize the model
     *
     * @memberof Mossaic.models.Basic
     * @instance
     * @param {object} attributes Hash of attributes to be stored in the model
     * @param {object} options Hash of model options
     * @param {boolean} options.is_dirty Set to true if the initial model
     *                             should be considered dirty
     */
    initialize: function(attributes, options) {
      options = options || {};
      Backbone.DeepModel.prototype.initialize.apply(this, [attributes, options]);
      var that = this;
      this.is_dirty = false;
      this.bind("change", function() {
        var attributes_equal = _.isEqual(that.previousAttributes(),
            that.toJSON());
        var previous_attributes_persisted = "_id" in that.previousAttributes();
        if (!attributes_equal && previous_attributes_persisted) {
          that.is_dirty = true;
        }
      });
    },
    /**
     * Fetch the model from the DB
     *
     * @memberof Mossaic.models.Basic
     * @instance
     * @param {object} options Hash of options
     * @param {function} options.success Function to be called if fetch is
     *                                   successful
     */
    fetch: function(options) {
      var that = this;
      options || (options = {});
      var model = that;
      var success = options.success;
      options.success = function(resp, status, xhr) {
        that.is_dirty = false;
        if (success) success(model, resp);
      };
      Backbone.DeepModel.prototype.fetch.apply(this, [options]);
    },
    /**
     * Clear the model attributes and reset them to their default values.
     *
     * @memberof Mossaic.models.Basic
     * @instance
     */
    clear: function() {
       var that = this;
       var options = {};
       that.is_dirty = true;
       options.silent = true;
       var ret = Backbone.DeepModel.prototype.clear.apply(this, [options]);
       that.set(that.defaults, {silent: false});
       return ret;
    },
    /**
     * Save the model attribtues and update is_dirty on success.
     *
     * @memberof Mossaic.models.Basic
     * @instance
     * @param {object} attrs Hash of attributes to be updated before saving
     * @param {object} options Hash of model options
     * @param {function} options.success Function to be called if fetch is
     *                                   successful
     */
    save: function(attrs, options) {
      var that = this;
      options || (options = {});
      var model = that;
      var success = options.success;
      var error = options.error;
      options.success = function(model, resp, options) {
        that.is_dirty = false;
        if (success) success(resp);
      };
      options.error = function(resp, status, errorObject) {
        if (error) error(errorObject.errors);
      };
      Backbone.DeepModel.prototype.save.apply(this, [attrs, options]);
    }
  })
});

/*
save accepts success and error callbacks in the options hash, which are passed
(model, response, options) and (model, xhr, options) as arguments, respectively.
        If a server-side validation fails, return a non-200 HTTP response code,
along with an error response in text or JSON. */
