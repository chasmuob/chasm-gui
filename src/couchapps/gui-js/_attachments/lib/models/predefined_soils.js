/**
 * Backbone model for representing predefined soils
 * 
 * @constructor
 * @extends Mossaic.models.Basic
 */

  Mossaic.models.PredefinedSoils = Backbone.Model.extend({
    defaults: {
      type: "predefined_soil"
    }
  });
