/**
 * Backbone Model for representing road network data
 *
 * @constructor
 * @extends Mossaic.models.Basic
 */

  Mossaic.models.RoadNetwork = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.RoadNetwork
     * @instance
     * @type object
     */
    defaults: {
      type: "road_network"
    }
  });
