/**
 * Backbone Model for representing soils data
 *
 * @constructor
 * @extends Mossaic.models.Basic
 */

  Mossaic.models.Soils = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Soils
     * @instance
     * @type object
     * @property {string} type The type of the model, in this case "soils"
     */
    defaults: {
      type: "soils"
    },
    /**
     * Determine whether the model has any stochastic data
     *
     * @memberof Mossaic.models.Soils
     * @instance
     * @return true if any of the fields in the model are stochastic
     */
    is_stochastic: function() {
      var to_check = ["Ksat", "saturated_moisture_content",
          "saturated_bulk_density", "unsaturated_bulk_density",
          "effective_angle_of_internal_friction", "effective_cohesion"];
      var soils = this.get("soils");
      return _.any(soils, function(soil) {
        return _.any(to_check, function(field) {
          return ("mean" in soil[field] || "standard_deviation" in soil[field]);
        });
      });
    }
  });
