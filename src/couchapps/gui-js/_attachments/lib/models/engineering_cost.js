/**
 * Backbone Model for representing engineering cost data
 *
 * @constructor
 * @extends Mossaic.models.Basic
 */

  Mossaic.models.EngineeringCost = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     * @type object
     */
    defaults: {
      type: "engineering_cost",
      equipment: {
        shovel: {
          capacity: {
            unit: "m3",
            value: []
          },
          cost: {
            unit: "EC$h-1",
            value: []
          },
          quantity: []
        },
        backhoe: {
          capacity: {
            unit: "m3",
            value: []
          },
          cost: {
            unit: "EC$h-1",
            value: []
          },
          quantity: []
        },
        lorry: {
          capacity: {
            unit: "t",
            value: []
          },
          cost: {
            unit: "EC$h-1",
            value: []
          },
          quantity: []
        }
      },
      labour: {
        unskilled: {
          cost: {
            unit: "EC$h-1",
            value: 0
          },
          quantity: 0
        },
        skilled: {
          cost: {
            unit: "EC$h-1",
            value: 0
          },
          quantity: 0
        }
      }
    },
    /**
     * Get list of all available equipment
     *
     * @memberof Mossaic.models.EngineeringCost
     * @instance
     * @return {object} Hash of the types of equipment available in the model
     */
    get_available_equipment: function() {
      var equipment = this.get("equipment");
      return _.sortBy(_.keys(equipment), function(key) {
        return key;
      });
    },
    /**
     * Get equipment list in format suitable for rendering
     *
     * @memberof Mossaic.models.EngineeringCost
     * @instance
     * @return {array} Array of equipment in render-friendly format
     */
    get_equipment: function() {
      var equipment = this.get("equipment");
      var equipment_t = [];
      var keys = this.get_available_equipment();
      _.each(keys, function(key) {
        var item = equipment[key];
        var number_of_items = item.quantity.length;
        var lengths_equal = _.all([item.capacity.value.length,
            item.cost.value.length, item.quantity.length], function(length) {
          return length === number_of_items;
        });
        if (lengths_equal) {
          for (var i = 0; i < number_of_items; ++i) {
            // Mustache failing to handle nested objects inside lists properly,
            // so everything is at top level
            var new_item = {
              name: key,
              capacity: item.capacity.value[i],
              capacity_unit: item.capacity.unit,
              cost: item.cost.value[i],
              cost_unit: item.cost.unit,
              quantity: item.quantity[i],
              index: i
            };
            equipment_t.push(new_item);
          }
        } else {
          throw "Badly formed equipment specification";
        }
      });
      return equipment_t;
    },
    /**
     * Get labour list in format suitable for rendering
     *
     * @memberof Mossaic.models.EngineeringCost
     * @instance
     * @return {array} Array of labour in render-friendly format
     */
    get_labour: function() {
      var labour = this.get("labour");
      var labour_t = [];
      var keys = _.sortBy(_.keys(labour), function(key) {
        return key;
      });
      _.each(keys, function(key) {
        var item = labour[key];
        var number_of_items = item.quantity.length;
        var lengths_equal = _.all([item.cost.value.length,
            item.quantity.length], function(length) {
          return length === number_of_items;
        });
        if (lengths_equal) {
          // Mustache failing to handle nested objects inside lists properly,
          // so everything is at top level
          var new_item = {
            name: key,
            cost: item.cost.value,
            cost_unit: item.cost.unit,
            quantity: item.quantity
          };
          labour_t.push(new_item);
        } else {
          throw "Badly formed labour specification";
        }
      });
      return labour_t;
    }
  });
