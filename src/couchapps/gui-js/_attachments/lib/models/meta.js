/**
 * Backbone Model for representing slope datum metadata
 *
 * @constructor
 * @extends Mossaic.models.Basic
 */
  
  Mossaic.models.Meta = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Meta
     * @instance
     * @type object
     * @property {string} type The type of the model, in this case "meta"
     * @property {object} breadth The breadth of the slope
     * @property {array} road_link Indices of road network nodes either side
     *                             of the datum
     */
    defaults: {
      type: "meta",
      name: null,
      location: {
        lat: null,
        "long": null
      },
      breadth: {
        unit: "m",
        value: null
      },
      road_link: [],
      comments: null
    }
  });
  