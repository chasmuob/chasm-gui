/**
 * Backbone Model for representing task requests
 *
 * @constructor
 * @extends Mossaic.models.Basic
 */
define(["models/_basic.rj"], function(BasicModel) {
  return BasicModel.extend({


    initialize: function(options) {
      this.models = options.models;
      var db_name = Mossaic.db.get_db_name({type: "task"});
      this.urlRoot = db_name;
    },

    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Task
     * @instance
     * @type object
     */
    defaults: {
      type: "task",
      requestor: "TODO - get logged-in from profile widget",
      input_files: {},
      output_variables: {
        "factor_of_safety": true,
        "pressure_head": false,
        "soil_moisture_content": false,
        "total_soil_moisture": false,
        "vegetation_interception": false
      },
      output_files: {
        "factor_of_safety": true,
        "pressure_head": false,
        "soil_moisture_content": false,
        "total_soil_moisture": false,
        "vegetation_interception": false
      },
      duration: {
        "unit": "h",
        "value": 360
      },
      time_step: {
        "unit": "s",
        "value": 60
      },
      report_filename: "output_summary",
      simulation_parameters: {
        "number_of_jobs": 1
      }
    },

    /**
     * Required inputs for each application type
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     */
    required_inputs: {
      chasm: ["boundary_conditions", "geometry", "meta", "soils",
          "stability"],
      questa: ["boundary_conditions", "geometry", "meta", "soils",
          "stability", "road_network", "engineering_cost"]
    },
    /**
     * Return the mandatory input types for the current application
     *
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     * @return {array} Array of required input types for the current application
     */
    get_required_inputs: function() {
      var executable = this.get("executable");
      var application;
      if (typeof(executable) !== "undefined") {
        application = executable.application;
      }
      if (typeof(application) !== "undefined") {
        if (application in this.required_inputs) {
          return this.required_inputs[application];
        } else {
          throw("Unsupported application: " + application);
        }
      } else {
        throw ("No application selected");
      }
    },

     /**
     * Validate the task
     *
     * Client-side task validation as follows:
     *  - for each required model
     *   - is it persisted
     *  - are all task fields completed?
     *  - of all available models, is this a valid combination?
     *
     * Note that *model* validation is to be handled at model save time,
     * in the browser and in Couch as a validate_doc_update function.
     */
    validate_task : function() {
      var that = this;
      var required_inputs = this.get_required_inputs();
      var all_inputs = this.models;

      var inputs_saved = _.all(_.keys(all_inputs), function(key) {
        var is_required_input = _.any(required_inputs, function(input) {
          return input == key;
        });
        if (!is_required_input) {
          return true;
        }
        var model = all_inputs[key];
        if (typeof(model) === "undefined") {
          return false;
        } else {
          return !model.is_dirty;
        }
      });
      if (!inputs_saved) {
        return {
          is_valid: false,
          message: "Task has unsaved inputs"
        };
      }
      var required_fields = ["name", "time_step"];
      var all_required_fields = _.all(required_fields, function(field) {
        return typeof(that.get(field)) !== "undefined";
      });
      // Check that the length of each simulation >= earliest rainfall start time
      var storms = this.models.boundary_conditions.get('storm');
      var simulationParameters = this.get('simulation_parameters');

      if(!simulationParameters.length){
        return {
          is_valid: false,
          message: 'Length of each simulation must be completed'
        };
      }

      for(var i = 0; i < storms.length; i++) {
	// If the storm starts after the simulation has finished, error
        if(storms[i].start.value > simulationParameters.length){
	  return {
            is_valid: false,
	    message: "Simulation length must be greater than rainfall start"
          };
        }
      }

      if (!all_required_fields) {
        return {
          is_valid: false,
          message: "Task description is incomplete"
        }
      }
      return {
        is_valid: true
      };
    },

    get_input_files: function() {
      out = {};
      _.each(['meta','cross_section','geometry','stability','soils','boundary_conditions'],function(modelName){
        if(!this.models.hasOwnProperty(modelName)){
          throw "Job has missing input file" + fileName;
        }
        out[modelName] = this.models[modelName].get('_id');
      },this);
      return out;
    },

    get_input_revisions: function() {
      out = {};
      _.each(['meta','cross_section','geometry','stability','soils','boundary_conditions'],function(modelName){
        if(!this.models.hasOwnProperty(modelName)){
          throw "Job has missing input file" + fileName;
        }
        out[modelName] = this.models[modelName].get('_rev');
      },this);
      return out;
    },

    get_duration: function() {
      return {
        unit: "h",
        // value: this.models.boundary_conditions.get_duration()
        value: this.get('simulation_parameters').length
      };
    },

    task_id: function(){
      var parent_id = this.models.geometry.get('_id');
      return [ [parent_id], [this.get("type"),this.get("name"),new Date().valueOf()].join(':')].join('|');
    },

    submit_task: function(cb) {
      var that = this;
      var validity = this.validate_task();
      if (!validity.is_valid) {
        alert("Could not submit task request: " + validity.message);
        return;
      }
      this.set({requestor: Mossaic.session.userCtx.name});
      // If we have an id then the task request already exists. Remove id
      // and rev attributes so task request is treated as new.
      if ("id" in this.attributes) {
        this.unset("id");
        this.unset("_id");
        this.unset("_rev");
      }
      var parent_id = this.models.geometry.get("_id"); // Legacy? It's in the Couch models, so we copy it in...
      this.set({input_files: this.get_input_files()});
      this.set({input_revisions: this.get_input_revisions()});
      this.set({parent_id: parent_id});
      this.set({_id: this.task_id()});

      this.set({timestamp: (new Date().getTime() / 1000)});
      this.set({duration: this.get_duration()});

      this.save(null, {
        success: function(response) {
          console.log(response);
          alert("Task " + that.get("name") +
                  " submitted with id: " + response.id);
        },
        error: function(response) {
          var message;
          if (response === 409) {
            message = "Task with name " + that.get("name") +
                    " already exists for this slope profile";
          } else {
            message = "Unknown error: " + response;
          }
          alert("Could not submit task request: " + message);
        }
      });
    }




  });
});
