/**
 * Backbone Model for representing slope reinforcements
 *
 * @constructor
 * @extends Mossaic.models.Basic
 */
  
  Mossaic.models.Reinforcements = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Reinforcements
     * @instance
     * @type object
     */
    defaults: {
      type: "reinforcements"
    }
  });
  