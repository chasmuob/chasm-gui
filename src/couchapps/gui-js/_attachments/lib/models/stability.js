/**
 * Backbone Model for storing chasm/questa stability data.
 *
 * @constructor
 * @extends Mossaic.models.Basic
 */

  Mossaic.models.Stability = Mossaic.models.Basic.extend({
    /**
     * Initialize the model.
     * Called implicitly with "new Mossaic.models.Stability".
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} attributes Hash of attributes to be stored in the model
     * @param {object} options Hash of model options
     * @param {Mossaic.models.Geometry} options.geometry Reference to the
     *                                                   Mossaic.models.Geometry
     *                                                   which this stability
     *                                                   data is for
     */
    initialize: function(attributes, options) {
      options = options || {};
      console.log('New model!!');
      Mossaic.models.Basic.prototype.initialize.apply(this,
          [attributes, options]);
      _.bindAll(this);
      /**
       * The Mossaic.models.Geometry model that the stability model is for.
       * This reference is used when saving the model, so that the slope
       * coordinates can be written into the stability model (the slope
       * coordinates are required by CHASM).
       * @type Mossaic.models.Geometry
       */
      this.geometry = options.geometry;
      /**
       * Local copy of the attributes for auto-setting the stability
       * @type object
       */
      this.attributes_stash = attributes;
      if (typeof(this.geometry) !== "undefined" && !attributes.hasOwnProperty('_rev')) {
        this.auto_set_values(attributes);
      }
    },
    /**
     * True if the model is editable. Views check this flag before allowing
     * users to modify, however it is not enforced in the model.
     * @memberof Mossaic.models.Stability
     * @instance
     * @type boolean
     */
    editable: true,
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @type object
     * @property {string} type The type of the model, in this case "stability"
     * @property {string} stability_analysis_algorithm The algorithm that can
     *                                                 be used with this
     *                                                 stability model for
     *                                                 carrying out a slip
     *                                                 search
     * @property {object} grid_search_parameters An empty hash where the
     *                                           grid search parameters can be
     *                                           stored
     */
    defaults: {
      type: "stability",
      stability_analysis_algorithm: "Bishop",
      grid_search_parameters: {}
    },
    /**
     * Retrieve the current set of slope coordinates from the geometry model
     * and fall through to the standard save method.
     *
     * All stability models must contain the slope coordinates for their
     * corresponding geometry model.
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} attrs Hash of attributes to be updated before saving
     * @param {object} options Hash of model options
     */
    save: function(attributes, options) {
      var coords = this.geometry.compute_coords({quantize: false});
      this.set("slope_surface_coordinates", {
        x: _.map(coords, function(coord) { return coord.x; }),
        y: _.map(coords, function(coord) { return coord.y; })
      }, {silent: true});
      Mossaic.models.Basic.prototype.save.apply(this, [attributes, options]);
    },
    /**
     * Retrieve model fields, but silently retrieve certain keys from the
     * grid_search_parameters model attribute. This is a convenience method for
     * calling code.
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {string} key Key of the value to be retrieved
     * @return {object} Value corresponding to the supplied key
     */
    get: function(key) {
      if (key in {"origin": null, "spacing": null,
          "grid_size": null, "radius": null}) {
        return this.get_gsp(key);
      } else {
        return Backbone.Model.prototype.get.apply(this, [key]);
      }
    },
    /**
     * Set a model field.
     *
     * Silently set certain keys on the grid_search_parameters attribute, rather
     * than the top level of the model attributes.
     *
     * Default parameters are (key, value, options) but can also be called
     * with (attributes, options) where attributes is a hash with the key,value
     * to be set.
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {string} key Key of the value to be set
     * @param {object} value Value to be set
     * @param {object} options Hash of options
     */
    set: function(key, value, options) {
      var attrs;
      if (_.isObject(key)) {
        attrs = key;
        options = value;
      } else {
        attrs = {};
        attrs[key] = value;
      }
      for (var attr in attrs) {
        if (attr in
            {"origin": null, "spacing": null,
            "grid_size": null, "radius": null}) {
          this.set_gsp(attr, attrs[attr], options);
        } else {
          Backbone.Model.prototype.set.apply(this, [attr, attrs[attr],options]);
        }
      }
      return this;
    },
    /**
     * Set a field in the grid_search_parameters attribute
     *
     * Default parameters are (key, value, options) but can also be called
     * with (attributes, options) where attributes is a hash with the key,value
     * to be set.
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {string} key Key of the value to be set
     * @param {object} value Value to be set
     * @param {object} options Hash of options
     */
    set_gsp: function(key, value, options) {
       // Set a field in the grid search parameters
      var gsp = _.clone(this.get("grid_search_parameters"));
      gsp[key] = value;
      return Backbone.Model.prototype.set.apply(this,
          [{grid_search_parameters: gsp}, options]);
    },
    /**
     * Get a field in the grid_search_parameters attribute
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {string} key Key of the value to be retrieved
     * @return {object} Value corresponding to the supplied key
     */
    get_gsp: function(key) {
      var gsp = this.get("grid_search_parameters");
      if (typeof(gsp) === "undefined") {
        return;
      } else {
        return _.clone(this.get("grid_search_parameters")[key]);
      }
    },
    /**
     * Set the associated geometry model and auto set stability values
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {Mossaic.models.Geometry} geometry Geometry model from which slope
     *                                           surface coords should be
     *                                           obtained
     */
    set_geometry: function(geometry) {
      this.geometry = geometry;
      this.auto_set_values(this.attributes_stash);
    },
    /**
     * Auto-set stability grid according to questa rules
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} attributes Hash of attributes containing values that
     *                            should not be auto-set
     * @param {object} options Hash of options
     * @param {object} options.mesh Mesh dimensions for autosetting
     */
    auto_set_values: function(attributes, options) {
      var mesh = (options && options.mesh) || {x: 1, y: 1};
      this.changed_by = null;
      var dimensions = this.get_dimensions();
      var attributes = attributes || {};
      if (typeof(attributes.origin) === "undefined") {
        this.set_origin(dimensions, mesh);
      }
      if (typeof(attributes.spacing) === "undefined") {
        this.set_spacing(mesh);
      }
      if (typeof(attributes.grid_size) === "undefined") {
        this.set_grid_size(dimensions);
      }
      if (typeof(attributes.radius) === "undefined") {
        this.set_radius(dimensions);
      }
    },
    /**
     * Determine stability grid dimensions from maximum x and y coordinates of
     * the associated geometry
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @return {object} Height and length of stability grid
     */
    get_dimensions: function() {
      var coords = this.geometry.compute_coords();
      var x_max = _.max(coords, function(coord) {
        return coord.x;
      }).x;
      var y_max = _.max(coords, function(coord) {
        return coord.y;
      }).y;
      return {
        height: y_max,
        length: x_max
      };
    },
    /**
     * Set the grid spacing from the mesh spacing of the associated geometry
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} mesh x and y dimensions of a single mesh cell
     */
    set_spacing: function(mesh) {
      var spacing = _.clone(mesh);
      spacing.unit = "m";
      this.set({
        spacing: spacing
      });
    },
    /**
     * Set the origin according to questa logic
     * This is 2/3 the width, 2/3 the height, plus however many mesh increments
     * we need so that the grid origin is not within the slope profile.
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} dimensions height and length of the stability grid
     * @param {object} mesh x and y dimensions of a single mesh cell
     */
    set_origin: function(dimensions, mesh) {
      var that = this;
      var x_initial = dimensions.length * 2/3;
      var y_initial = dimensions.height * 2/3;
      var initial_point = {
        x: Math.round(x_initial / mesh.x) * mesh.x,
        y: Math.round(y_initial / mesh.y) * mesh.y
      };
      var set_point = function(point) {
        if (typeof(that.geometry) !== "undefined" &&
            that.geometry.is_point_in_slope(point)) {
          point.x = point.x + mesh.x;
          return set_point(point);
        } else {
          return point;
        }
      };
      var point = set_point(initial_point);
      point.unit = "m";
      console.log('Set origin to',point);
      this.set({origin: point});
    },
    /**
     * Set the size of the stability grid to default size
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} dimensions height and length of the stability grid
     */
    set_grid_size: function(dimensions) {
      this.set({
        grid_size: {
          unit: "m",
          x: 7,
          y: 7
        }
      });
    },
    /**
     * Set the stability grid radius to default radius
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} dimensions height and length of the stability grid
     */
    set_radius: function(dimensions) {
      this.set({
        radius: {
          unit: "m",
          initial: 5,
          increment: 0.5
        }
      })
    }
  });
