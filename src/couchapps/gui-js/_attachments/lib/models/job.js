/**
 * Backbone Model for representing job requests
 * @constructor
 * @extends Mossaic.models.Basic
 */

  Mossaic.models.Job = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Job
     * @instance
     * @type object
     */
    defaults: {
      type: "job"
    }
  });
