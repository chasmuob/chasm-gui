/**
 * Backbone Model for representing slope cross sections
 *
 * @constructor
 * @extends Mossaic.models.Basic
 */

  Mossaic.models.CrossSection = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.CrossSection
     * @instance
     * @type object
     */
    defaults: {
      type: "cross_section",
      start: {},
      end: {}
    }
  });
