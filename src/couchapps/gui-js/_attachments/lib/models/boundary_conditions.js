/**
 * Backbone Model for representing boundary conditions data
 *
 * @constructor
 * @extends Mossaic.models.Basic
 */

  Mossaic.models.BoundaryConditions = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     * @type object
     * @property {string} type The type of the model, in this case
     *                         "boundary_conditions"
     * @property {object} upslope_recharge Upslope recharge unit and value
     * @property {object} detention_capacity Detention capacity unit and value
     * @property {object} soil_evaporation Soil evaporation unit and value
     */
    defaults: {
      type: "boundary_conditions",
      upslope_recharge: {
        unit: "mh-1"
      },
      detention_capacity: {
        unit: "m"
      },
      soil_evaporation: {
        unit: "ms-1"
      }
    },
    /**
     * Determine whether the model has any stochastic data
     *
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     * @return true if any of the fields in the model are stochastic
     */
    is_stochastic: function() {
      var detention_capacity = this.get("detention_capacity");
      var soil_evaporation = this.get("soil_evaporation");
      return ("mean" in detention_capacity ||
          "standard_deviation" in detention_capacity ||
          "mean" in soil_evaporation ||
          "standard_deviation" in soil_evaporation);
    },

    get_storm_count: function() {
      return this.has("storm") ? this.get("storm").length : 0;
    },

    /**
     * Determine the duration (in days) of the design storm
     * (or design storm series)
     *
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     * @return Design storm duration
     */
    get_duration: function() {
      var compute_duration = function(storm) {
        var start = storm.start.value;
        var rainfall_hours = storm.rainfall_intensities.value.length;
        return start + rainfall_hours;
      }
      var storm = this.get("storm");
      if (typeof(storm) === "undefined" || storm.length == 0) {
        throw "Cannot return duration because no storms are defined";
      }
      // Get first storm, find length
      var duration = compute_duration(storm[0]);
      // Check all other storms, throw exception if any differ
      var all_storms_equal_duration = _.all(storm, function(instance) {
        return compute_duration(instance) == duration;
      });
      if (!all_storms_equal_duration) {
        throw "Multiple storms are defined with differing total durations";
      } else {
        return duration;
      }
    }
  })
