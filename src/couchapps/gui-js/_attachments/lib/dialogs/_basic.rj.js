define(["views/_basic.rj"], function(BasicView) {
  /**
   * Base module for a 1 or 2 button dialog boxes.  A more specific module should inherit from here, and 
   * generate the body of the dialog box, and functions to be excuted when the buttons are clicked.
   * 
   * This module provides default render(), show() and hide() functions.
   * 
   * This basic dialog box contains an empty body, and 1 or 2 buttons.  Subclasses should call update_body() to 
   * set the dialog body content
   * 
   * Unlike most other Mossaic views, the instantiating class is *not* repsonsible for inserting 
   * the module content into the DOM.  Instead, calling show() will append the dialog box to the DOM "body" element, and
   * hide() will remove it.
   * 
   */
          
  return BasicView.extend({
    /**
     * Configure sane defaults.
     * 
     * @constructor
     * 
     * @param {object} options
     * @param {string} [options.body] Content of the main body of the dialog box
     * @param {string} [options.title] Title of the dialog box
     * @param {string} [options.btn1_text] 1st button text
     * @param {string} [options.btn2_text] 2nd button text
     * @param {function} [options.btn1_action] Function to be executed when btn1 is clicked
     * @param {function} [options.bt2_action] Function to be executed when btn2 is clicked
     * @param {btn1_css} [options.btn1_css] CSS classes to be added to btn1.
     * @param {btn1_css} [options.btn1_css] CSS classes to be added to btn2.
     * 
     * @returns {undefined}
     */
    initialize : function(options) {
      BasicView.prototype.initialize.apply(this,[options]);
      this.body = "";
      this.title = "";
      this.btn1_text = "OK";
      this.btn2_text = "";
      this.btn1_action = null;
      this.btn2_action = null;
      this.btn1_css = "";
      this.btn2_css = "";
    },

    /**
     * Renders the basic scaffolding HTML into this.el and sets up the button event handlers.
     * Does not insert any elements into the global DOM. (See show() for that).
     * Subclasses should first call this function (with 'BasicDialog.prototype.render.apply(this);'), before adding
     * their own content (if needed).  
     * render() is called automatically when show() is called.
     */
    render : function() {
      var that = this;
      this.$el = $($.Mustache.render("dialogs.basic", {
        id: this.id,
        body: this.body,
        title: this.title,
        btn1_text: this.btn1_text,
        btn2_text: this.btn2_text,
        btn1_css: this.btn1_css,
        btn2_css: this.btn2_css
      }));  
      
      this.$el.find(".modal-body").html(this.body);

      if (typeof(this.btn1_action) === "function") {
        this.$el.find(".btn1").click(this.btn1_action);
      } else {
        this.$el.find(".btn1").click(function() {
          that.hide();
        });
      }

      if (typeof(this.btn2_action) === "function") {
        this.$el.find(".btn2").click(this.btn2_action);
      }
    }, 
    
   /**
    * Inserts the dialog box into the DOM, and shows it onscreen.
    */
    show: function() {
      // The subclass render fn() needs to call  'BasicDialog.prototype.render.apply(this);' in order to 
      // render() the scaffolding above, before adding anything else.
      this.render();
      $("body").append(this.$el);
      this.$el.modal("show"); // Backbone method for dialog boxes.
    },

    /**
     * Hides the dialog box and removes it from the DOM
     */
    hide: function() {
      this.$el.modal("hide");
      this.remove();
    },

    /*
     * Updates the dialog box body with supplied HTML content 
     * @param {string|element} new_body String, HTML element, or jQ element to replace the body with
     */
    update_body: function(new_body) {
      this.$el.find(".modal-body").html(new_body);
    },

    /**
     * Returns the body of the dialog box.
     * @returns {jQ element}
     */
    get_body: function() {
      return this.$el.find(".modal-body");
    }
  });
});