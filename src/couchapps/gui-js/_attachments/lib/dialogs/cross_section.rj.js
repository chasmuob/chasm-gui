define(["dialogs/_basic.rj"], function(BasicDialog) {

  /**
   * Dialog box to edit Cross Section models.  The creator should call set_model() with the model object to 
   * display in the dialog box.
   */
  
    return BasicDialog.extend({

    /**
     * @constructor
     * @param {object} options
     */
      initialize: function(options) {
        BasicDialog.prototype.initialize.apply(this,[options]);
        var that = this;
        var options = options || {};
        
        this.title = "Edit Cross Section";
        this.btn1_text = "Cancel";
        this.btn2_text = "Save";
        this.btn2_css = "btn-primary";

        this.btn2_action = function() {
          that.update_model_from_form();
          Mossaic.utils.quietly_save_model(that.model, {
              success: function() {
                that.hide();
              },
              fail: function(errors) {
                that.set_errors(errors);
              }
            });
        };
      },

    /**
      * Set the model to be represented by the form.
      * 
      * @param {Backbone.Model} model Model to be represented by this form
      */
     set_model: function(model) {
        this.model = model;
      },


      /**
       * Update the model from form data.  To be called before saving the model
       */
      update_model_from_form : function() {
        this.model.set("name", 
          this.$form.find("[name='name']").val()
        );
        this.model.set("start", {
            lat: this.$form.find("[name='start.lat']").val(),
            long: this.$form.find("[name='start.long']").val()
        });
        this.model.set("end", {
            lat: this.$form.find("[name='end.lat']").val(),
            long: this.$form.find("[name='end.long']").val()
        });
        this.model.set("comments", 
            this.$form.find("[name='comments']").val()
        );
      },
      
      /**
       * Display an error element, generally to be used when a model save() has failed.
       * 
       * @param {array} errors An array containing error objects: [{field:"fieldname", msg: "error message"}, ...]
       */  
      set_errors : function(errors) {
        // For some reason, combining the below into one line - $().mustache(..) - only shows the last error. Odd.
        var error_str = $.Mustache.render("chunks.flash-error",errors);
        this.$el.find(".form-errors").html(error_str);
      },

      /**
       * Renders the form (with model data) into this.el.  Will be called implicitly by show() so shouldn't need to 
       * call explicitly.
       */
      render: function() {
        BasicDialog.prototype.render.apply(this);
        
        // Render the form into HTML with mustache, and then jQuery-ify the HTML.
        // TODO - This would probably be better in a seperate form that extends forms/_basic.rj
        var $form = $($.Mustache.render("dialogs.cross_section"));  

        $form.find("#" + this.form_id + "-lat_start").val(this.model.get("start").lat || "");
        $form.find("#" + this.form_id + "-long_start").val(this.model.get("start")['long'] || "");
        $form.find("#" + this.form_id + "-lat_end").val(this.model.get("end").lat || "");
        $form.find("#" + this.form_id + "-long_end").val(this.model.get("end")['long'] || "");

        
        $form.find("[name='name']").val(this.model.get("name"));
        $form.find("[name='start.lat']").val(this.model.get("start").lat);
        $form.find("[name='start.long']").val(this.model.get("start").long);
        $form.find("[name='end.lat']").val(this.model.get("end").lat);
        $form.find("[name='end.long']").val(this.model.get("end").long);
        $form.find("[name='comments']").val(this.model.get("comments"));
        
        this.$form = $form;
        this.update_body($form);
      }
    });
  }
);