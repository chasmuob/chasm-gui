/**
 * @file Display and manage the dialog box for editing meta data
 */
define(["dialogs/_basic.rj"], function(BasicDialog) {

    return BasicDialog.extend({

      /**
     * @constructor
     * @param {object} options
     * @param {function} options.get_model Function to be called to retrieve in order to retrieve the model that is
     *                                     being edited.
     */
      initialize: function(options) {
        BasicDialog.prototype.initialize.apply(this,[options]);
        var that = this;
        var options = options || {};

        this.title = "Edit Datum";
        this.btn1_text = "Cancel";
        this.btn2_text = "Save";

        this.btn2_action = function() {
          that.update_model_from_form();
          Mossaic.utils.quietly_save_model(that.model, {
              success: function() {
                that.hide();
              },
              fail: function(errors) {
                that.set_errors(errors);
              }
            });
        };
        this.btn2_css = "btn-primary";
      },

   /**
       * Set the model to be represented by the form.
       * @param {Backbone.Model} model Model to be represented by this form
       */
      set_model: function(model) {
        this.model = model;
      },

      /**
       * Update the model from form data.  To be called before saving the model
       */
      update_model_from_form : function() {
        this.model.set("name",
          this.$form.find("[name='name']").val()
        );
        this.model.set("location", {
            lat: this.$form.find("[name='location.lat']").val(),
            long: this.$form.find("[name='location.long']").val()
        });
        this.model.set("breadth", {
          unit : "m",
          value : this.$form.find("[name='breadth']").val()
        });
        this.model.set("comments", 
          this.$form.find("[name='comments']").val()
        );
      },

      /**
       * Display an error element, generally to be used when a model save() has failed.
       *
       * @param {array} errors An array containing error objects: [{field:"fieldname", msg: "error message"}, ...]
       */
      set_errors : function(errors) {
        // For some reason, combining the below into one line - $().mustache(..) - only shows the last error. Odd.
        var error_str = $.Mustache.render("chunks.flash-error",errors);
        this.$form.find(".form-errors").html(error_str);
      },

      /**
       * Renders the form (with model data) into this.el.  Will be called implicitly by show() so shouldn't need to
       * call explicitly.
       */
      render: function() {
        BasicDialog.prototype.render.apply(this);


        // TODO - This would probably be better in a seperate form that extends forms/_basic.rj
        var $form = $($.Mustache.render("dialogs.datum"));

        $form.find("[name='name']").val(this.model.get("name") || "");
        $form.find("[name='location.lat']").val(this.model.get("location").lat || "");
        $form.find("[name='location.long']").val(this.model.get("location").long || "");
        $form.find("[name='breadth']").val(this.model.get("breadth").value || "");
        $form.find("[name='comments']").val(this.model.get("comments") || "");

        this.$form = $form;   // So other methods can access it later on

        this.update_body($form);
      }

    });
  }
);
