/**
 * @file Creates the Mossaic.forms namespace and loads the forms
 */
(function() {
  var scripts_to_load = [
      "lib/forms/basic.js",
      "lib/forms/cross_section.js",
      "lib/forms/datum.js",
      "lib/forms/geometry_metadata.js",
      "lib/forms/mesh.js",
      "lib/forms/cross_section.js",
      "lib/forms/engineering_cost.js",
  ];

  /**
   * @description Provides Backbone.View classes which render form
   * representations of models defined in Mossaic.models
   * @namespace
   */
  Mossaic.forms = {};

 

  var load_scripts = function(scripts) {
    for (var i=0; i < scripts.length; i++) {
      document.write('<script src="'+scripts[i]+'"><\/script>');
    }
  };

  load_scripts(scripts_to_load);

  /**
   * Build any forms that require templates from the design doc. This must
   * be run *after* the design document has loaded, otherwise the templates
   * won't be loaded.
   * This is triggered by the Mossaic.Router which, as top-level object, is
   * responsible for ensuring any deferred forms are built.
   * @function
   */
  Mossaic.forms.build_deferred = function() {
    _.each(Mossaic.forms.deferred, function(form) {
      form();
    });
  };
})();