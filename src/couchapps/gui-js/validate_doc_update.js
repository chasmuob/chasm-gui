function (newDoc, oldDoc, userCtx) {
  var errors = [];
  if (newDoc.type === "meta") {

    if (!newDoc.name) {
      errors.push({field:"name",msg:"Missing name"});
    }
    
    if (!newDoc.location.lat) {
      errors.push({field:"lat",msg:"Missing Northing"});
    }

    if (newDoc.location.lat) {
      if (!newDoc.location.lat.match(/^[\d]*[.]?[\d]*$/)) {
        errors.push({field:"lat",msg:"Invalid characters in Northing"});
      }
    }

    if (!newDoc.location["long"]) {
      errors.push({field:"long",msg:"Missing Easting"});
    }

    if (!newDoc.breadth.value) {
      errors.push({field:"breadth",msg:"Missing Slope Breadth"});
    }

    // Couch doesn't like returning an array for the errors, so wrap it in an object     
    if (errors.length > 0) throw({forbidden:{errors:errors}}); 
  }
}