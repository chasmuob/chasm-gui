(function() {
  'use strict';
  angular
    .module('chasm.core')
    .factory('Couch',Couch);
  function Couch($http){
    var api = {
      getByType: getByType,
      getByParent: getByParent,
      getTasksByGeometry: getTasksByGeometry,
      get: get
    };

    return api;

    function getByType(type){
      var url = '_view/docs_by_type?descending=false&skip=0&reduce=false&include_docs=false&inclusive_end=true&update_seq=false&key=\"' + type + '\"';
      return $http.get(url).then(function(response){
        return response.data.rows;
      });
    }

    function getByParent(type,id){
      var s = '["'+type+'","'+id+'"]';
      var e = '["'+type+'","'+id+'",{}]';
      var url = '_view/docs_by_parent?descending=false&skip=0&reduce=false&include_docs=false&inclusive_end=true&update_seq=false&startkey='+s+'&endkey='+e;
      return $http.get(url).then(function(response){
        return response.data.rows;
      });
    }

    function getTasksByGeometry(id){
      var url = '/chasm_task/_design/workflow/_view/tasks_by_slope?descending=false&skip=0&reduce=false&include_docs=false&inclusive_end=true&update_seq=false&startkey=%5B"'+id+'"%5D&endkey=%5B"'+id+'"%2C%7B%7D%5D';
      return $http.get(url).then(function(response){
        return response.data.rows;
      });
    }

    function get(id){
      return $http.get('/chasm/'+id).then(function(response){
        return response.data;
      });
    }
  }
}());
