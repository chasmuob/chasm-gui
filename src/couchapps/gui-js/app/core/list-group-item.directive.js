(function() {
  'use strict';
  angular
    .module('chasm.core')
    .directive('listGroupItem', function() {
      return {
        restrict: 'C',
        link: function(scope, element) {
          element.bind('click', function() {
            if (element.hasClass('active')) {
              element.removeClass('active');
            } else {
              element.addClass('active').siblings().removeClass('active');
            }
          });
        }
      };
    });
}());
