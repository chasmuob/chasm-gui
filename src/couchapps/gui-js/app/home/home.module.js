(function() {
  'use strict';
  angular
    .module('chasm.home',['chasm.core'])
    .config(function($stateProvider){
      $stateProvider.state('home',{
        url: '/',
        templateUrl: 'home/home.html',
        controller: 'HomeCtrl as ctrl'
      });
    });
}());
