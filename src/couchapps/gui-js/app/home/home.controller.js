(function() {
  'use strict';
  angular
    .module('chasm.home')
    .controller('HomeCtrl',HomeCtrl);

  function HomeCtrl($q,Couch){
    var vm = this;
    var models;
    vm.data = {};

    var promises = {};
    promises.datum = Couch.getByType('meta');
    promises.crossSection = Couch.getByType('cross_section');
    promises.geometry = Couch.getByType('geometry');

    $q.all(promises).then(function(results){
      // angular.extend(vm.data,results);
      models = results;
      vm.data.datum = results.datum;
      console.log(results);
    });

    vm.selectDatum = function(datum){
      console.log(datum);
      Couch.getByParent('cross_section',datum.id).then(function(values){
        vm.data.crossSection = values;
      });
    };

    vm.selectCrossSection = function(crossSection){
      Couch.getByParent('geometry',crossSection.id).then(function(values){
        vm.data.geometry = values;
      });
    };

    vm.selectGeometry = function(datum){

    };
  }
}());
