(function() {
  'use strict';
  angular
    .module('chasm.home')
    .directive('chasmBrowser', function($location, $q, $uibModal, Couch) {
      return {
        restrict: 'E',
        templateUrl: 'home/chasm-browser.directive.html',
        replace: true,
        link: function(scope, element, attributes) {
          scope.data = {};
          scope.active = {};
          scope.actives = {};

          scope.taskClassMap = {
            done: 'task-done',
            in_progress: 'task-in-progress',
          };

          scope.taskStatusMap = {
            done: 'Done',
            in_progress: 'Running'
          };

          // This shows us the heirarchy of data
          var dataHeirarchy = ['meta', 'cross_section', 'geometry', 'task'];

          Couch.getByType('meta').then(function(data) {
            scope.data.meta = data;
          });

          function clearData(model) {
            // Go backwards down the heirarchy until we get to our model
            for (var i = dataHeirarchy.length - 1; i >= 0; i--) {
              if (dataHeirarchy[i] === model.type) {
                break;
              }
              scope.data[dataHeirarchy[i]] = [];
            }
            scope.active = null;
            scope.actives[model.type] = null;
          }

          function isActive(model) {
            return model === scope.active || scope.actives[model.type] === model;
          }

          function setActive(model, type) {
            scope.actives[type] = model;
            model.type = type;
            scope.active = model;
          }

          function modal(options) {
            return $uibModal.open(angular.extend({}, {
              animate: true,
              scope: scope,
              resolve: {},
            }, options || {}));
          }

          scope.editDatum = function(model) {
            modal({
              templateUrl: 'home/forms/datum-edit.html',
              resolve: {
                data: function() {
                  return model ? Couch.get(model.id) : undefined;
                }
              },
              controller: function($scope, $modalInstance, data) {
                $scope.data = data || {
                  breadth: {},
                  location: {}
                };
                $scope.title = data ? 'Edit Datum' : 'New Datum';

                $scope.save = function() {
                  var instance = new Mossaic.models.Meta();
                  instance.set($scope.data);
                  instance.save(null, {
                    success: function(response) {
                      if (model) {
                        angular.extend(model, response);
                      } else {
                        model = response;
                        scope.data.meta.push(model);
                      }
                      model.value = $scope.data.name;
                      $scope.$apply(function() {
                        $modalInstance.close(model);
                      });
                    }
                  });
                };
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
                };
              }
            });
          };

          scope.deleteDatum = function(model) {
            if (confirm('Are you sure?')) {
              Couch.get(model.id).then(function(attributes) {
                var instance = new Mossaic.models.Meta();
                instance.set(attributes);
                instance.set('id', attributes._id);
                instance.destroy();
                clearData(model);
                scope.data.meta.splice(scope.data.meta.indexOf(model), 1);
              });
            }
          };

          scope.editCrossSection = function(model) {
            modal({
              templateUrl: 'home/forms/cross_section-edit.html',
              resolve: {
                data: function() {
                  return model ? Couch.get(model.id) : undefined;
                }
              },
              controller: function($scope, $modalInstance, data) {
                $scope.data = data || {
                  start: {},
                  end: {},
                  parent_id: scope.actives.meta.id
                };
                $scope.title = data ? 'Edit Cross Section' : 'New Cross Section';

                $scope.save = function() {
                  var instance = new Mossaic.models.CrossSection();
                  instance.set($scope.data);
                  instance.save(null, {
                    success: function(response) {
                      if (model) {
                        angular.extend(model, response);
                      } else {
                        model = response;
                        scope.data.cross_section.push(model);
                      }
                      model.value = $scope.data.name;
                      $scope.$apply(function() {
                        $modalInstance.close(model);
                      });
                    }
                  });
                };
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
                };
              }
            });
          };

          scope.deleteCrossSection = function(model) {
            if (confirm('Are you sure?')) {
              Couch.get(model.id).then(function(attributes) {
                var instance = new Mossaic.models.CrossSection();
                instance.set(attributes);
                instance.set('id', attributes._id);
                instance.destroy();
                clearData(model);
                scope.data.cross_section.splice(scope.data.cross_section.indexOf(model), 1);
              });
            }
          };

          scope.editGeometry = function(model, taskMode) {
            var url;
            if (model) {
              url = 'index.html#task-composer/edit-params/id/' + model.id + '/confirmation';
              if (taskMode && taskMode === true) {
                url = url + '?task_mode=true';
              } else {
                return modal({
                  templateUrl: 'home/forms/edit-geometry-confirm.html',
                  controller: function($scope, $modalInstance) {
                    $scope.edit = function() {
                      $modalInstance.dismiss('cancel');
                      window.location = url;
                    };
                    $scope.cancel = function() {
                      $modalInstance.dismiss('cancel');
                    };
                  }
                });
              }
            } else {
              window.router.state.geometry.clear();
              window.router.state.geometry.set('parent_id', scope.actives.cross_section.id);
              url = 'index.html#task-composer/edit-params/define/choose';
            }
            window.location = url;
          };

          scope.copyGeometry = function(model) {
            modal({
              templateUrl: 'home/forms/copy-geometry.html',
              controller: function($scope, $modalInstance) {
                $scope.data = {};
                $scope.createCopy = function() {
                  $scope.data.doingCopy = true;
                  doCopyGeometry(model, $scope.data.name, function() {
                    $modalInstance.dismiss('cancel');
                  });
                };
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
                };
              }
            });
          };
          /**
           * Copy an entire geometry!
           */
          function doCopyGeometry(model, name, cb) {
            // Instantiate our base model
            var id, base, copy;
            var promises = [];
            base = new Mossaic.models.Geometry();

            base.set(model);
            id = model.id;

            base.fetch({
              success: function() {


                // Create a copy of it
                copy = base.clone();
                copy.unset('id');
                copy.unset('_id');
                copy.unset('_rev');
                copy.unset('key');
                copy.set('parent_id', scope.actives.cross_section.id);
                copy.set('name', name);
                copy.set('value', name);
                copy.set('isNew', true);
                copy.set_complete('surface');
                copy.set_complete('soil_strata');
                copy.set_complete('water_table');

                // Get children of the base
                $.couch.db('chasm').view('gui/docs_by_parent', {
                  reduce: false,
                  descending: false,
                  include_docs: true,
                  skip: 0,
                  inclusive_end: true,
                  update_seq: false,
                  success: function(data) {

                    var soils = _.find(data.rows, function(item) {
                      return item.doc.type === 'soils' && item.doc.parent_id === id;
                    });
                    var boundary_conditions = _.find(data.rows, function(item) {
                      return item.doc.type === 'boundary_conditions' && item.doc.parent_id === id;
                    });
                    var stability = _.find(data.rows, function(item) {
                      return item.doc.type === 'stability' && item.doc.parent_id === id;
                    });

                    soils = new Mossaic.models.Soils(soils && soils.doc || {
                      parent_id: id
                    });
                    boundary_conditions = new Mossaic.models.BoundaryConditions(boundary_conditions && boundary_conditions.doc || {
                      parent_id: id
                    });
                    stability = new Mossaic.models.Stability(stability && stability.doc || {
                      parent_id: id
                    }, {
                      geometry: base
                    });
                    soils.unset('id');
                    soils.unset('_id');
                    soils.unset('_rev');
                    boundary_conditions.unset('id');
                    boundary_conditions.unset('_id');
                    boundary_conditions.unset('_rev');
                    stability.unset('id');
                    stability.unset('_id');
                    stability.unset('_rev');
                    // Save the copy
                    copy.save(null, {
                      success: function() {
                        var copyId = copy.get('id');
                        console.log('New ID', copyId);
                        soils.set('parent_id', copyId);
                        boundary_conditions.set('parent_id', copyId);
                        stability.set('parent_id', copyId);
                        $.when.apply($, _.invoke([soils, boundary_conditions, stability], 'save')).done(function() {
                          scope.$apply(function() {
                            scope.data.geometry.push(copy.attributes);
                            cb();
                          });
                        });
                      }
                    });
                  }
                });
              }
            });
          }

          scope.deleteGeometry = function(model) {
            if (confirm('Are you sure?')) {
              Couch.get(model.id).then(function(attributes) {
                var instance = new Mossaic.models.Geometry();
                instance.set(attributes);
                instance.set('id', attributes._id);
                instance.destroy();
                clearData(model);
                scope.data.geometry.splice(scope.data.geometry.indexOf(model), 1);
              });
            }
          };

          scope.newTask = function() {
            scope.editGeometry(scope.actives.geometry);
          };

          scope.selectDatum = function(datum) {
            datum.type = 'meta';
            clearData(datum);
            window.router.state.meta.set(datum);
            Couch.getByParent('cross_section', datum.id).then(function(values) {
              scope.data.cross_section = values;
            });
            setActive(datum, 'meta');
          };

          scope.selectCrossSection = function(crossSection) {
            crossSection.type = 'cross_section';
            clearData(crossSection);
            window.router.state.cross_section.set(crossSection);
            Couch.getByParent('geometry', crossSection.id).then(function(values) {
              scope.data.geometry = values;
            });
            setActive(crossSection, 'cross_section');
          };

          scope.selectGeometry = function(geometry) {
            geometry.type = 'geometry';
            clearData(geometry);
            window.router.state.geometry.set(geometry);
            Couch.getTasksByGeometry(geometry.id).then(function(values) {
              scope.data.task = values;
            });
            setActive(geometry, 'geometry');
          };

          scope.selectTask = function(task) {
            task.type = 'task';
            window.router.state.task.set(task);
            setActive(task, 'task');
          };

          scope.analyzeTask = function(task) {
            var encoded = encodeURI('#/task_analyser/analyse_task/id/' + task.id);
            console.log(encoded);
            window.location = encoded;
          };
        }
      };
    });
}());
