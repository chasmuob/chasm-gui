function(doc) {
  if (doc.type == "results" && "chasm_output" in doc) {
      emit([doc.task_id, doc.job_id],doc.chasm_output.precipitation.value);
    }
}
