function(doc) {
  if (doc.type == "results" && "job_id" in doc) {
    for (i in doc.chasm_output.radius.value) {
      emit([doc.task_id, doc.job_id, parseInt(i)],
        {
          x: doc.chasm_output.centre.x[i],
          y: doc.chasm_output.centre.y[i],
          r: doc.chasm_output.radius.value[i]
        }
      );
    }
  }
}