function(doc) {
  var base64 = require('views/lib/base64');
  var jszip = require('views/lib/tinyinflate');
  var JSON = require('views/lib/json2').init();

  if (doc.type === "results" && "chasm_output" in doc) {
    var decompress = function(data) {
      var decoded = base64.decode(data);
      var inflated = jszip.inflate(decoded);
      return JSON.parse(inflated.data);
    };
    var emit_cell_data = function(data, key) {
      if (key === "soil_type" || key === "mass") {
        emit([doc.task_id, key, doc.job_id, 0], data);
      } else {
        if (typeof(data) === "string") {
          var decompressed_data = decompress(data);
          for (var i = 0; i < decompressed_data.length; ++i) {
            emit([doc.task_id, key, doc.job_id, i], decompressed_data[i]);
          }
        }
      }
    };
    var keys_to_emit = {
      moisture_content: null,
      soil_type: null,
      pressure_head: null,
      mass: null
    };
    for (var key in keys_to_emit) {
      emit_cell_data(doc.chasm_output[key].value, key);
    }
  }
}
