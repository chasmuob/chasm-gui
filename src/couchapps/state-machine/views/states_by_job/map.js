function(doc) {
    if ("job_id" in doc && "task_id" in doc) {
        emit(doc.job_id, null);
    }
}