function(doc) {
  // States are: splitting, ready_to_submit, submitted, done
  // splitting->ready_to_submit = TaskSentinel
  // ready_to_submit->submitted = JobSentinel
  // submitted->done = JobTracker
  emit(doc.state, 1);
}