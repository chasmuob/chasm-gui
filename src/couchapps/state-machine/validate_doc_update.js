var arrayContains = function(array, term) {
  for (var i = 0; i < array.length; i++) {
    if (term == array[i]) {
      return true;
    }
  }
  return false;
}

function (newDoc, oldDoc, userCtx) {
  if (!userCtx.name) {
    throw({unauthorized: "Please log in before attempting to modify database content"});
  }
  if (newDoc.state == "done" || newDoc.state == "failed") {
    if (!arrayContains(userCtx.roles, "job_manager") && !arrayContains(userCtx.roles, "job_wrapper") && !arrayContains(userCtx.roles, "_admin")) {
      throw({forbidden: "You do not have permission to modify state documents"});
    }
  } else if (!arrayContains(userCtx.roles, "job_manager") && !arrayContains(userCtx.roles, "_admin")) {
    throw({forbidden: "You do not have permission to modify state documents"});
  }
}