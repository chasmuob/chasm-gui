/**
 * A library of functions that return common error objects.
 *
 */

exports.init = function() {
    var errors = {};

    /**
     * Returns error object for cases where no document has been requested.
     *
     * @return {object} HTTP response with appropriate error code.
     */
    errors.null_doc = function() {
        return {
            "headers": {
              "Content-Type": "text/plain"
             },
            "code": 400,
            "body": "No document requested."
        };
    };

    /**
     * Returns error object for cases where the requested document cannot be found.
     *
     * @return {object} HTTP response with appropriate error code.
     */
    errors.doc_not_found = function(id) {
        return {
            "headers": {
                "Content-Type": "text/plain"
            },
            "code": 404,
            "body": ['Could not find document with id "', id, '".'].join('')
        };
    };

    /**
     * Returns error object for cases where the requested document is of the wrong type.
     *
     * @return {object} HTTP response with appropriate error code.
     */
    errors.invalid_request = function(doc_type) {
        return {
            "headers": {
              "Content-Type": "text/plain"
             },
            "code": 400,
            "body": ["Requested document is not a valid", doc_type, "file."].join(" ")
        };
    };

    /**
     * Returns error object for cases where an unexpected problem occured.
     *
     * @return {object} HTTP response with appropriate error code.
     */
    errors.internal_error = function(id, message) {
        return {
            "headers": {
                "Content-Type": "text/plain"
            },
            "code": 500,
            "body": ["Internal server error occurred when processing document with id ", id, ".", "\n", "Error details: ", message].join("")
        };
    };

    return errors;
};