/**
 * Utilities for working with mossaic data
 */

var JSON = require('lib/json2').init();

 exports.init = function() {
     var utils = {};

     /**
      * Get the value of a data element. Handles deterministic and stochastic element definitions.
      *
      * @param {object} element The element from which the value is to be retrieved.
      * @param {number} index Optional. If present the element value is an array of values and the method will return value[index].
      * @return {string} Either a single value (if deterministic) or two values separated by a space (mean and standard deviation).
      */
     utils.getValue = function(element, index) {
         if ("mean" in element && "standard_deviation" in element) {
              if (index !== undefined) {
                  return [element.mean[index], element.standard_deviation[index]].join(" ");
              } else {
                  return [element.mean, element.standard_deviation].join(" ");
              }
          } else if ("value" in element) {
              if (index !== undefined) {
                  return element.value[index];
              } else {
                  return element.value;
              }
          } else {
              throw("Element has neither deterministic nor stochastic values");
          }
     };

     /**
      * Convert a boolean value to and integer, where false = 0 and true = 1.
      *
      * @param {boolean} bool Boolean value to be converted.
      * @return 1 if bool is true, 0 if bool is false.
      */
     utils.boolToInt = function(bool) {
         if (bool) {
             return 1;
         } else {
             return 0;
         }
     };
     
     utils.doSubstitutions = function(substitutions, doc) {
         substitutions = JSON.parse(substitutions);
         for (var pointer in substitutions) {
             var value = substitutions[pointer];
             var setPointer = function(json, pointerArray, value) {
                 if (pointerArray.length == 1) {
                     delete json.mean;
                     delete json.standard_deviation;
                     json[pointerArray[0]] = value;
                 } else {
                     setPointer(json[pointerArray[0]], pointerArray.slice(1), value);
                 }
             };
             setPointer(doc, pointer.split('/').slice(1), value);
         }
         return doc;
     };

     return utils;
 };