/** Show for stochastic parameters documents
*
* Validate requested document.
* If invalid: return HTTP error code.
* If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
*/

/**
 * Entry point function, called by couchdb.
 *
 * @param {object} doc The document to be rendered by this function.
 * @param {object} req The HTTP request to which this function is responding.
 * @return {object} HTTP response object which is returned by couchdb to the caller.
 */
function(doc, req) {
    var errors = require('lib/errors').init();
    var sha1 = require('lib/sha1').init().sha1;

    /**
     * Create the text content of a stochastic parameters file.
     *
     * @param {object} doc The document containing the stochastic parameters data.
     * @return {string} The text content of a stochastic parameters file.
     */
    var getStochasticFile = function(doc) {
        var stochastic = [];
        if (doc.number_of_simulations === undefined || doc.factor_of_safety_output_hour === undefined) {
            throw "Document is not a valid stochastic parameters file";
        }
        stochastic.push([doc.number_of_simulations, doc.factor_of_safety_output_hour].join(" "));
        return stochastic.join("\n");
    };

    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "stochastic_parameters") {
        return errors.invalid_request("stochastic parameters");
    }
    try {
        var stochasticFile = getStochasticFile(doc);
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = stochasticFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
         "body": body
    };
};