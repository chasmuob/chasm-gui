/** Show for engineering cost documents
*
* Validate requested document.
* If invalid: return HTTP error code.
* If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
*/

/**
 * Entry point function, called by couchdb.
 *
 * @param {object} doc The document to be rendered by this function.
 * @param {object} req The HTTP request to which this function is responding.
 * @return {object} HTTP response object which is returned by couchdb to the caller.
 */
function(doc, req) {
    var errors = require('lib/errors').init();
    var sha1 = require('lib/sha1').init().sha1;

    // Equipment and labour types currently supported by QUESTA
    var SUPPORTED_EQUIPMENT = ["shovel", "backhoe", "lorry"];
    var SUPPORTED_LABOUR = ["unskilled", "skilled"];

    /**
     * Get a string representation of the supplied equipment item type.
     *
     * @param {object} equipment_item Array of equipment item data.
     * @param {string} type Equipment type (shovel/backhoe, etc).
     * @return {string} String representation of the supplied equipment item.
     */
    var getEquipmentForType = function(equipment_item, type) {
        var itemData = [];
        for (var i = 0; i < equipment_item.quantity.length; ++i) {
            itemData.push([type, equipment_item.capacity.value[i], equipment_item.cost.value[i], equipment_item.quantity[i]].join("\t"));
        }
        return itemData.join("\n");
    };

    /**
     * Get a string representation of the available equipment.
     *
     * @param {object} equipment Equipment resources data.
     * @return {string} String representation of the supplied equipment data.
     */
    var getEquipment = function(equipment) {
        var equipmentData = [];
        for (var i = 0; i < SUPPORTED_EQUIPMENT.length; i += 1) {
            equipmentData.push(getEquipmentForType(equipment[SUPPORTED_EQUIPMENT[i]], SUPPORTED_EQUIPMENT[i]));
        }
        return equipmentData.join("\n");
    };

    /**
     * Get a string representation of the supplied type of labour.
     *
     * @param {object} labour Labour resources data.
     * @param {string} type Labour type (skilled/unskilled).
     * @return {string} String representation of the supplied type of labour.
     */
    var getLabourForType = function(labour, type) {
        return [type, "", labour.cost.value, labour.quantity].join("\t");
    };

    /**
     * Get a string representation of the available labour.
     *
     * @param {object} labour Labour resources data.
     * @return {string} String representation of labour resources.
     */
    var getLabour = function(labour) {
        var labourData = [];
        for (var i = 0; i < SUPPORTED_LABOUR.length; i += 1) {
            labourData.push(getLabourForType(labour[SUPPORTED_LABOUR[i]], SUPPORTED_LABOUR[i]));
        }
        return labourData.join("\n");
    };

    /**
     * Create the text content of an engineering cost file.
     *
     * @param {object} doc The document containing the engineering cost data.
     * @return {string} The text content of an engineering cost file.
     */
    var getEngineeringCostFile = function(doc) {
        var engineeringCost = [];
        engineeringCost.push(getEquipment(doc.equipment));
        engineeringCost.push(getLabour(doc.labour));
        return engineeringCost.join("\n");
    };

    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "engineering_cost") {
        return errors.invalid_request('engineering cost');
    }
    try {
        var engineeringCostFile = getEngineeringCostFile(doc);
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = engineeringCostFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
         "body": body
    };
};