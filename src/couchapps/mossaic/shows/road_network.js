/** Show for road network documents
*
* Validate requested document.
* If invalid: return HTTP error code.
* If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
*/

/**
 * Entry point function, called by couchdb.
 *
 * @param {object} doc The document to be rendered by this function.
 * @param {object} req The HTTP request to which this function is responding.
 * @return {object} HTTP response object which is returned by couchdb to the caller.
 */
function(doc, req) {
    var errors = require('lib/errors').init();
    var sha1 = require('lib/sha1').init().sha1;

    var INFINITY = 9999;

    /**
     * Get a sorted Array of the keys of an object.
     *
     * @param {object} The object whose keys are to be returned.
     * @return {Array} A sorted Array containing the keys of the supplied object.
     */
    var getKeys = function(obj) {
        var keys = [];
        for (key in obj) {
            keys.push(key);
        }
        return keys.sort();
    };

    /**
     * Get the number of keys in a given object.
     *
     * @param {object} obj Object whose keys are to be counted.
     * @return {number} Number of keys in supplied object.
     */
    var getNumberOfKeys = function(obj) {
        return getKeys(obj).length;
    };

    /**
     * Get the metadata for this road network.
     *
     * @param {object} doc Document containing road network metadata.
     * @return {string} String representation of the road network metadata.
     */
    var getRoadNetworkMetadata = function(doc) {
        var metadata = [];
        metadata.push(["nodes", doc.number_of_nodes].join("\t"));
        metadata.push(["origins", doc.centroids.origin.length].join("\t"));
        metadata.push(["destinations", doc.centroids.origin.length].join("\t"));
        metadata.push(["split pairs", doc.centroids.split_pairs.length].join("\t"));
        metadata.push(["diameter", doc.diameter].join("\t"));
        metadata.push(["transport modes", getNumberOfKeys(doc.transport_modes)].join("\t"));
        return metadata.join("\n");
    };

    /**
     * Convert a sparse matrix representation (dictionary of origin/destination nodes) into an origin/destination matrix.
     *
     * @param {object} dict Dictionary containing the origin/destination nodes that have values.
     * @param {number} number_of_nodes Number of nodes in the road network graph.
     * @param {number} notFoundValue Number to be used if value for row/col does not exist.
     * @return {string} String representation of the supplied origin/destination node dictionary.
     */
    var dictToMatrix = function(dict, number_of_nodes, not_found_value) {
        var matrix = [];
        for (var row = 0; row < number_of_nodes; row += 1) {
            var matrixRow = [];
            var originNode = String(row);
            for (var col = 0; col < number_of_nodes; col += 1) {
                var destinationNode = String(col);
                if (originNode in dict && destinationNode in dict[originNode]) {
                    matrixRow.push(dict[originNode][destinationNode]);
                } else {
                    matrixRow.push(not_found_value);
                }
            }
            matrix.push(matrixRow.join("\t"));
        }
        return matrix.join("\n");
    };

    /**
     * Get the link distance matrix as a string.
     *
     * @param {object} link_time Graph link time data.
     * @param {number} number_of_nodes Number of nodes in the road network graph.
     * @return {string} String representation of the link distance matrix.
     */
    var getLinkDistance = function(link_distance, number_of_nodes) {
        var linkDistance = [];
        linkDistance.push("link_distance");
        linkDistance.push(dictToMatrix(link_distance, number_of_nodes, INFINITY));
        return linkDistance.join("\n");
    };

    /**
     * Get the link time matrix as a string.
     *
     * @param {object} link_time Graph link time data.
     * @param {number} number_of_nodes Number of nodes in the road network graph.
     * @return {string} String representation of the link time matrix.
     */
    var getLinkTime = function(link_time, number_of_nodes) {
        var linkTime = [];
        linkTime.push("link_time");
        linkTime.push(dictToMatrix(link_time, number_of_nodes, INFINITY));
        return linkTime.join("\n");
    };

    /**
     * Get the matrices of transport counts for the supplied transport mode data.
     *
     * @param {object} transport_modes The transport mode cost and count data.
     * @param {number} number_of_nodes Number of nodes in the road network graph.
     * @param {Array} transportModeNames Alphabetically sorted list of transport mode names.
     * @return {string} String representation of the transport count matrices.
     */
    var getTransportCounts = function(transport_modes, number_of_nodes, transportModeNames) {
        var transportCounts = [];
        for (var mode = 0; mode < transportModeNames.length; mode += 1) {
            var counts = [];
            counts.push([transportModeNames[mode], "count"].join("_"));
            counts.push(dictToMatrix(transport_modes[transportModeNames[mode]].count, number_of_nodes, 0));
            transportCounts.push(counts.join("\n"));
        }
        return transportCounts.join("\n\n");
    };

    /**
     * Get a headed list of the supplied node array.
     *
     * @param {Array} nodes Array of node names.
     * @param {string} listName Name to be printed as the header of this list.
     * @return {string} String representation of this node name array.
     */
    var getNodeList = function(nodes, listName) {
        var nodeList = [];
        nodeList.push(listName);
        nodeList.push(nodes.join("\n"));
        return nodeList.join("\n");
    };

    /**
     * Get the split pair nodes for the road network graph.
     *
     * @param {object} split_pairs Split pairs of nodes.
     * @return {string} String representation of the supplied split pair nodes.
     */
    var getSplitPairs = function(split_pairs) {
        var splitPairs = [];
        splitPairs.push("split_pairs");
        for (var row = 0; row < split_pairs.length; row += 1) {
            splitPairs.push(split_pairs[row].join("\t"));
        }
        return splitPairs.join("\n");
    };

    /**
     * Get the travel costs for each mode of transport. Transport mode names are passed in as a separate list to preserve order.
     *
     * @param {object} transport_modes Transport mode cost and count data.
     * @param {Array} transportModeNames Alphabetically sorted list of transport mode names.
     * @return {string} String representation of travel costs for each transport mode.
     */
    var getTravelCosts = function(transport_modes, transportModeNames) {
        var travelCosts = [];
        travelCosts.push("travel_costs");
        for (var mode = 0; mode < transportModeNames.length; mode += 1) {
            var costs = [];
            costs.push(transport_modes[transportModeNames[mode]].running_cost.maintenance.value);
            costs.push(transport_modes[transportModeNames[mode]].travel_time_value.value);
            costs.push(transport_modes[transportModeNames[mode]].running_cost.fuel.value);
            travelCosts.push(costs.join("\t"));
        }
        return travelCosts.join("\n");
    };

    /**
     * Create the text content of a road network file.
     *
     * @param {object} doc The document containing the road network data.
     * @return {string} The text content of a road network file.
     */
    var getRoadNetworkFile = function(doc) {
        var roadNetwork = [];
        roadNetwork.push(getRoadNetworkMetadata(doc));
        roadNetwork.push("");
        roadNetwork.push(getLinkDistance(doc.link_distance, doc.number_of_nodes));
        roadNetwork.push("");
        roadNetwork.push(getLinkTime(doc.link_time, doc.number_of_nodes));
        roadNetwork.push("");
        var transportModeNames = getKeys(doc.transport_modes);    // Need it as a list so order is preserved
        roadNetwork.push(getTransportCounts(doc.transport_modes, doc.number_of_nodes, transportModeNames));
        roadNetwork.push("");
        roadNetwork.push(getNodeList(doc.centroids.origin, "origin_nodes"));
        roadNetwork.push("");
        roadNetwork.push(getNodeList(doc.centroids.destination, "destination_nodes"));
        roadNetwork.push("");
        roadNetwork.push(getSplitPairs(doc.centroids.split_pairs, "split_pairs"));
        roadNetwork.push("");
        roadNetwork.push(getTravelCosts(doc.transport_modes, transportModeNames)); //TODO!
        return roadNetwork.join("\n");
    };

    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "road_network") {
        return errors.invalid_request("road network");
    }
    try {
        var roadNetworkFile = getRoadNetworkFile(doc);
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = roadNetworkFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
         "body": body
    };
};