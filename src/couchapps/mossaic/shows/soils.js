/** Show for soils documents
*
* Validate requested document.
* If invalid: return HTTP error code.
* If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
*/

/**
 * Entry point function, called by couchdb.
 *
 * @param {object} doc The document to be rendered by this function.
 * @param {object} req The HTTP request to which this function is responding.
 * @return {object} HTTP response object which is returned by couchdb to the caller.
 */
function(doc, req) {
    var errors = require('lib/errors').init();
    var utils = require('lib/mossaicutils').init();
    var sha1 = require('lib/sha1').init().sha1;

    /**
     * Get the parameters of the supplied soil object as a string.
     *
     * @param {object} soil Soil data.
     * @return {string} String representation of the supplied soil data.
     */
    var getSoilParameters = function(soil) {
        var parameters = [];
        parameters.push(utils.getValue(soil.Ksat));
        parameters.push(utils.getValue(soil.saturated_moisture_content));
        parameters.push(utils.getValue(soil.saturated_bulk_density));
        parameters.push(utils.getValue(soil.unsaturated_bulk_density));
        parameters.push(utils.getValue(soil.effective_cohesion));
        parameters.push(utils.getValue(soil.effective_angle_of_internal_friction));
        if ("grade" in soil) {
            parameters.push(utils.getValue(soil.grade));
        }
        return parameters.join(" ");
    }

    /**
     * Determine whether suppled suction moisture curve is a Millington-Quirk model.
     *
     * @param {object} suction_moisture_curve Suction moisture curve data.
     * @return {boolean} True if supplied suction moisture curve is a Millington-Quirk model.
     */
    var isMillingtonQuirk = function(suction_moisture_curve) {
        try {
            return "moisture_content" in suction_moisture_curve && "suction" in suction_moisture_curve;
        } catch (error) {
            return false;
        }
    }

    /**
     * Get a string representation of the supplied suction moisture curve.
     *
     * @param {object} suction_moisture_curve Suction moisture curve data.
     * @return {string} String representation of the suction moisture curve.
     */
    var getSuctionMoistureCurve = function(suction_moisture_curve) {
        var suctionMoistureCurve = [];
        if (isMillingtonQuirk(suction_moisture_curve)) {
            suctionMoistureCurve.push(0);
            suctionMoistureCurve.push(suction_moisture_curve.moisture_content.length);
            suctionMoistureCurve.push(suction_moisture_curve.moisture_content.join(" "));
            suctionMoistureCurve.push(suction_moisture_curve.suction.join(" "));
        } else {
            suctionMoistureCurve.push(1);
            suctionMoistureCurve.push(suction_moisture_curve);
        }
        return suctionMoistureCurve.join("\n");
    }

    /**
     * Get a string representation of the supplied soil data.
     *
     * @param {object} soil Soil data.
     * @return {string} String representation of the soil data.
     */
    var getSoil = function(soil) {
        var extractedSoil = [];
        extractedSoil.push(getSoilParameters(soil));
        extractedSoil.push(getSuctionMoistureCurve(soil.suction_moisture_curve));
        return extractedSoil.join("\n");
    };

    /**
     * Create the text content of a soils file.
     *
     * @param {object} doc The document containing the soils data.
     * @return {string} The text content of a soils file.
     */
    var getSoilsFile = function(doc) {
        var soilsFile = [];
        soilsFile.push(doc.soils.length);
        soilsFile.push("");
        soils = [];
        for (var i = 0; i < doc.soils.length; i += 1) {
            soils.push(getSoil(doc.soils[i]));
        }
        soilsFile.push(soils.join("\n\n"));
        return soilsFile.join("\n");
    };

    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "soils") {
        return errors.invalid_request("soils");
    }
    if ('substitutions' in req.query) {
        doc = utils.doSubstitutions(req.query.substitutions, doc);
    }
    try {
        var soilsFile = getSoilsFile(doc);
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = soilsFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
         "body": body
    };
};