/** Show for slope loading documents
*
* Validate requested document.
* If invalid: return HTTP error code.
* If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
*/

/**
 * Entry point function, called by couchdb.
 *
 * @param {object} doc The document to be rendered by this function.
 * @param {object} req The HTTP request to which this function is responding.
 * @return {object} HTTP response object which is returned by couchdb to the caller.
 */
function(doc, req) {
    var errors = require('lib/errors').init();
    var sha1 = require('lib/sha1').init().sha1;

    /**
     * Convert the slope loading into rows of text
     *
     * @param {object} doc The document containing the slope loading data
     * @return {string} The text content of the actual slope loading defs
     */
    var getSlopeLoading = function(doc) {
        var slopeLoading = [];
        var newLoad;
        var nextLoad;
        for (var i = 0; i < doc.slope_loading.length; ++i) {
            newLoad = [];
            nextLoad = doc.slope_loading[i];
            if (typeof(nextLoad.coords.start) !== "undefined" &&
                    typeof(nextLoad.coords.end) !== "undefined") {
                newLoad.push("coords:\t\t" + nextLoad.coords.start.x.value + " " + nextLoad.coords.start.y.value +
                        " " + nextLoad.coords.end.x.value + " " + nextLoad.coords.end.y.value);
            } else {
                newLoad.push("coords:\t\t" + nextLoad.coords.x.value + " " + nextLoad.coords.y.value);           
            }
            newLoad.push("depth:\t\t" + nextLoad.depth.value);
            newLoad.push("total load:\t\t" + nextLoad.total_load.value);
            slopeLoading.push(newLoad.join("\n"));
        }
        return slopeLoading.join("\n\n");
    };

    /**
     * Create the text content of a slope loading file.
     *
     * @param {object} doc The document containing the slope loading data.
     * @return {string} The text content of a slope loading file.
     */
    var getSlopeLoadingFile = function(doc) {
      var rows = [];
      rows.push("number:\t\t" + doc.slope_loading.length);
      rows.push(getSlopeLoading(doc));
      return rows.join("\n\n");
    };

    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "slope_loading") {
        return errors.invalid_request('slope loading');
    }
    try {
        var slopeLoadingFile = getSlopeLoadingFile(doc);
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = slopeLoadingFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
         "body": body
    };
};