/** Show for vegetation documents
*
* Validate requested document.
* If invalid: return HTTP error code.
* If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
*/

/**
 * Entry point function, called by couchdb.
 *
 * @param {object} doc The document to be rendered by this function.
 * @param {object} req The HTTP request to which this function is responding.
 * @return {object} HTTP response object which is returned by couchdb to the caller.
 */
function(doc, req) {
    var errors = require('lib/errors').init();
    var utils = require('lib/mossaicutils').init();
    var sha1 = require('lib/sha1').init().sha1;

    /**
     * Get a string representation of supplied vegetation data.
     *
     * @param {object} vegetation Vegetation data.
     * @return {string} String containing rows of text representing vegetation data.
     */
    var getVegetation = function(vegetation) {
        var vegetationData = [];
        for (var i = 0; i < vegetation.vegetation_type.length; i += 1) {
            var row = [];
            row.push(utils.getValue(vegetation.leaf_area_index, i));
            row.push(vegetation.canopy_cover[i]);
            row.push(vegetation.rooting_depth[i]);
            row.push(utils.getValue(vegetation.max_transpiration, i));
            row.push(utils.getValue(vegetation.root_tensile_strength, i));
            row.push(utils.getValue(vegetation.root_area_ration, i));
            row.push(vegetation.vegetation_type[i]);
            vegetationData.push(row.join(" "));
        }
        return vegetationData.join("\n");
    };

    /**
     * Get the type of interception defined in the suppled vegetation document.
     *
     * @param {object} doc Vegetation document.
     * @return {number} 1 if interception is a canopy model, 2 if Thatch model
     */
    var getInterceptionType = function(doc) {
        if ("interception" in doc) {
            return 1;
        } else {
            return 2; 
        }
    };

    /**
     * Get a string representation of the supplied interception data.
     *
     * @param {object} interception Interception data.
     * @return {string} String representation of interception data.
     */
    var getInterception = function(interception) {
        var interceptionData = [];
        interceptionData.push(["Max_depth(mm):", interception.max_leaf_storage.value].join(" "));
        interceptionData.push(["Can_evap(m/s):", interception.wet_canopy_evaporation.value].join(" "));
        interceptionData.push(["Through_rate(0-1):", interception.leaf_drip_rate].join(" "));
        interceptionData.push(["Stem_portion(0-1):", interception.stem_portion].join(" "));
        interceptionData.push(["Trunk_capacity(mm):", interception.max_trunk_storage.value].join(" "));
        return interceptionData.join("\n");
    }

    /**
     * Determine whether the supplied vegetation document contains evapotranspiration parameters.
     *
     * @param {object} doc Vegetation document.
     * @return {boolean} True if vegetation document contains evapotranspiration parameters, false otherwise.
     */
    var isEvapotranspiration = function(doc) {
        if ("evapotranspiration" in doc) {
            return true;
        } else {
            return false;
        }
    };

    /**
     * Get a string representation of the supplied evapotranspiration data.
     *
     * @param {object} evapotrans Evapotranspiration data.
     * @return {string} String representation of the supplied evapotranspiration data.
     */
    var getEvapotranspiration = function(evapotrans) {
        var evapotransData = [];
        evapotransData.push(["Net_mean_sd(Wm2):", evapotrans.net_radiation.value].join(" "));
        evapotransData.push(["Tem_mean_sd(C):", evapotrans.average_daily_air_temperature.value].join(" "));
        evapotransData.push(["Rh_mean_sd(0-1):", evapotrans.average_daily_relative_humidity].join(" "));
        evapotransData.push(["Rc_mean_sd(sm-1):", evapotrans.canopy_resistance.value].join(" "));
        evapotransData.push(["SRa_mean_sd(sm-1):", evapotrans.soil_aerodynamic_resistance.value].join(" "));
        evapotransData.push(["VRa_mean_sd(sm-1):", evapotrans.vegetation_aerodynamic_resistance.value].join(" "));
        return evapotransData.join("\n");
    }

    /**
     * Determine whether the supplied vegetation document is for a stochastic simulation.
     *
     * @param {object} doc Vegetation document.
     * @return {boolean} True if vegetation document contains stochastic parameters, false otherwise.
     */
    var isStochastic = function(doc) {
        // TODO: As values must be either all deterministic or all stochastic, we only need to check one value assuming a valid document
        // As we will be validating input documents when they are created (with CouchDB validation functions) this is a valid assumption.
        try {
            return "mean" in doc.surcharge && "standard_deviation" in doc.surcharge;
        } catch (error) {
            return false;
        }
    };

    /**
     * Create the text content of a vegetation file.
     *
     * @param {object} doc The document containing the vegetation data.
     * @return {string} The text content of a vegetation file.
     */
    var getVegetationFile = function(doc) {
        var vegetationFile = [];
        vegetationFile.push("Model type: VEGETATION");
        vegetationFile.push(["Stoch:", utils.boolToInt(isStochastic(doc))].join(" "));
        vegetationFile.push(["Evapot:", utils.boolToInt(isEvapotranspiration(doc))].join(" "));
        vegetationFile.push(["Intercept:", getInterceptionType(doc)].join(" "));
        vegetationFile.push(["Root_dist(-):", utils.boolToInt(doc.root_dist)].join(" "));
        vegetationFile.push(["Root_decay(-):", doc.root_decay].join(" "));
        if (getInterceptionType(doc) == 1) {
            vegetationFile.push(getInterception(doc.interception));
        }
        if (isStochastic(doc)) {
            vegetationFile.push(["Mean_surcharge(kNm-2):", doc.surcharge.mean].join(" "));
            vegetationFile.push(["Sd_surcharge:", doc.surcharge.standard_deviation].join(" "));
        } else {
            vegetationFile.push(["Surcharge(kNm-2):", doc.surcharge.value].join(" "));
        }
        if (isEvapotranspiration(doc)) {
            vegetationFile.push(getEvapotranspiration(doc.evapotranspiration));
        }
        vegetationFile.push(["LH1(m):", doc.sink_terms.lh1.value].join(" "));
        vegetationFile.push(["LH2(m):", doc.sink_terms.lh2.value].join(" "));
        vegetationFile.push(["LH3(m):", doc.sink_terms.lh3.value].join(" "));
        vegetationFile.push(["LH4(m):", doc.sink_terms.lh4.value].join(" "));
        if (isStochastic(doc)) {
            vegetationFile.push("MLAI^SdLAI^Canopy^Rdepth^MMaxT^SdMaxT^MStr^SdStr^MRAR^SdRAR^Tree_grass");
        } else {
            vegetationFile.push("LAI^Canopy^Rdepth^MaxT^Str^RAR^Tree_grass");
        }
        vegetationFile.push(getVegetation(doc.vegetation));
        return vegetationFile.join("\n");
    };

    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "vegetation") {
        return errors.invalid_request("vegetation");
    }
    try {
        var vegetationFile = getVegetationFile(doc);
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = vegetationFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
         "body": body
    };
};