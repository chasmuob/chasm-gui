/** Show for geometry documents
*
* Validate requested document.
* If invalid: return HTTP error code.
* If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
*/

/**
 * Entry point function, called by couchdb.
 *
 * @param {object} doc The document to be rendered by this function.
 * @param {object} req The HTTP request to which this function is responding.
 * @return {object} HTTP response object which is returned by couchdb to the caller.
 */
function(doc, req) {
    var errors = require('lib/errors').init();
    var utils = require('lib/mossaicutils').init();
    var sha1 = require('lib/sha1').init().sha1;

    /**
     * Returns a string representation of the column metadata for a geometry column.
     *
     * $param {object} geometry Geometry column from which metadata is to be extracted.
     * $return {string} String representation of column metadata.
     */
    var extractColumnMetadata = function(geometry) {
        var columnMetadata = [];
        columnMetadata.push(geometry.cells.cell_depth.value.length);
        columnMetadata.push(geometry.number_of_saturated_cells);
        columnMetadata.push(geometry.column_width.value);
        columnMetadata.push(geometry.column_breadth.value);
        columnMetadata.push(geometry.initial_surface_suction.value);
        return columnMetadata.join(" ");
    };

    /**
     * Gets a string representation of a column of geometry cell objects.
     *
     * $param {Array} cells Array of geometry cells.
     * $return {string} String representation of geometry cells.
     */
    var extractColumnData = function(cells) {
        var columnData = [];
        for (var i = 0; i < cells.cell_depth.value.length; i += 1) {
            columnData.push(cells.cell_depth.value[i]);
            columnData.push(cells.soil_type[i]);
        }
        return columnData.join(" ");
    };

    /**
     * Gets a string representation of the supplied end boundary condition data.
     *
     * $param {object} end_boundary_condition End boundary condition data.
     * $return {string} String representation of end boundary condition data.
     */
    var extractEndBoundaryCondition = function(end_boundary_condition) {
        var endBoundaryCondition = [];
        endBoundaryCondition.push(end_boundary_condition.soil_type);
        endBoundaryCondition.push(end_boundary_condition.number_of_saturated_cells);
        return endBoundaryCondition.join(" ");
    };

    /**
     * Create the text content of a CHASM geometry file.
     *
     * @param {object} doc The document containing the CHASM geometry data.
     * @return {string} The text content of a CHASM geometry file.
     */
    var getGeometryFileChasm = function(doc) {
        var geometryFile = [];
        geometryFile.push(doc.geometry.mesh_data.length);
        for (var i = 0; i < doc.geometry.mesh_data.length; i += 1) {
            geometryFile.push(extractColumnMetadata(doc.geometry.mesh_data[i]));
            geometryFile.push(extractColumnData(doc.geometry.mesh_data[i].cells));
        }
        var end_boundary_condition;
        if (typeof(doc.end_boundary_condition) !== "undefined") {
          end_boundary_condition = doc.end_boundary_condition;
        } else {
          end_boundary_condition = {
            "soil_type": 0,
            "number_of_saturated_cells": 0
          };
        }
        geometryFile.push(extractEndBoundaryCondition(end_boundary_condition));
        return geometryFile.join("\n");
    };

    /**
     * Gets a string representation of the road prism.
     *
     * $param {object} road Object containing road data.
     * $return {string} String representation of road prism.
     */
    var getRoadPrism = function(road, slope_sections) {
        var roadPrism = [];
        var length = slope_sections[road.slope_section].surface_length.value;
        var pavementWidth = length - road.upslope_shoulder.value - road.downslope_shoulder.value;
        roadPrism.push(pavementWidth);
        roadPrism.push(length);
        roadPrism.push(road.downslope_shoulder.value);
        return roadPrism.join(" ");
    };

    /**
     * Gets a string representation of the supplied road link data.
     *
     * $param {object} road_link Road link data.
     * $return {string} String representation of the supplied road link data.
     */
    var getRoadLink = function(road_link) {
        return road_link.join(" ");
    };

    /**
     * Gets a string representation of the supplied slope breadth data.
     *
     * $param {object} slope_breadth Slope breadth data.
     * $return {string} String representation of the supplied slope breadth data.
     */
    var getSlopeBreadth = function(slope_breadth) {
        return slope_breadth.value;
    };

    /**
     * Gets a string representation of the supplied slope sections.
     *
     * @param {object} partial_slope_sections Data for all slope sections either upslope or downslope from road.
     * @return {string} String representation of the supplied slope sections.
     */
    var getSlopeSectionsData = function(partial_slope_sections) {
        var slopeSectionsData = [];
        for (var i = 0; i < partial_slope_sections.length; i += 1) {
            var section = [];
            if (partial_slope_sections[i].surface_length) {
                section.push(0);
                section.push(partial_slope_sections[i].surface_length.value);
            } else {
                section.push(partial_slope_sections[i].surface_height.value);
                section.push(0);
            }
            section.push(partial_slope_sections[i].angle.value);
            slopeSectionsData.push(section.join(" "));
        }
        return slopeSectionsData.join("\n");
    };

    /**
     * Gets the cut slope increment angle for a defined investment scenario.
     *
     * @param {object} investment Object containing data for geometry investment scenarios.
     * @return {number} Value of cut slope angle increments.
     */
    var getCutSlopeInvestment = function(investment) {
        return investment.cut_slope_increment.value;
    };

    /**
     * Gets a series of slope sections as a string.
     *
     * @param {object} slope_sections Slope section geometry data.
     * @param {object} road Road prism data.
     * @param {object} investment Investment data.
     * @return {string} String representation of slope section geometry.
     */
    var getSlopeSections = function(slope_sections, road, investment) {
        var slopeSections = [];
        var upslopeSections = slope_sections.slice(0, road.slope_section);
        var downslopeSections = slope_sections.slice(road.slope_section + 1);
        slopeSections.push([upslopeSections.length, downslopeSections.length].join(" "));
        slopeSections.push(getSlopeSectionsData(upslopeSections));
        slopeSections[slopeSections.length - 1] += [" ", getCutSlopeInvestment(investment)].join("");  // Last element is cutslope so append investment scenario`
        slopeSections.push(getSlopeSectionsData(slope_sections.slice(road.slope_section)));
        return slopeSections.join("\n");
    };

    /**
     * Gets a series of depth measurements in a section of slope as a string.
     *
     * We always return surface_distance_from_cut_toe as abs value because
     * QUESTA wants it positive (it knows whether it is upslope or downslope).
     *
     * @param {object} section Section of depth measurements.
     * @return {string} String representation of depth measurements.
     */
    var getSlopeSectionsDepth = function(sections) {
        var slopeSectionsData = [];
        for (var i = 0; i < sections.length; i += 1) {
              var section = [];
              section.push(Math.abs(sections[i].surface_distance_from_cut_toe.value));
              if (typeof(sections[i].depth.value) === "number") {
                  section.push(sections[i].depth.value);
              } else {
                  section.push(sections[i].depth.value.join(" "));
              }
              slopeSectionsData.push(section.join(" "));
        }
        return slopeSectionsData.join("\n");
    };

    /**
     * Helper function. Return the sections that pass supplied test.
     *
     * @param {object} sections Array of sections
     * @param {function} test Test function to apply to each section
     */
    var getSections = function(sections, test) {
        var filteredSections = [];
        for (var i = 0; i < sections.length; ++i) {
          if (test(sections[i])) {
            filteredSections.push(sections[i]);
          }
        }
        return filteredSections;
    };

    /**
     * Helper method for getGeometryFileQuesta. Gets the water table data as a string.
     *
     * @param {object} water_table Water table data.
     * @param {string} String representation of the supplied water table data.
     */
    var getWaterTable = function(water_table, road_section) {
        var waterTable = [];
        var upslopeSections = getSections(water_table, function(section) {
          return (section.surface_distance_from_cut_toe.value <= 0);
        });
        var downslopeSections = getSections(water_table, function(section) {
          return (section.surface_distance_from_cut_toe.value > 0);
        });
        waterTable.push([upslopeSections.length, downslopeSections.length].join(" "));
        waterTable.push(getSlopeSectionsDepth(water_table));
        return waterTable.join("\n");
    };

    /**
     * Gets the number of soils in a soil strata data object.
     *
     * @param {object} soil_strata Object that represents the soil strata data.
     * @return {number} The number of soils in the soil strata.
     */
    var getNumberOfSoils = function(soil_strata) {
        return soil_strata.interfaces.angle.value.length + 1;
    };

    /**
     * Helper method for getSoilStrataApproximate. Gets the soil strata data as a string for an approximated soil definition.
     *
     * @param {object} soil_strata Object that represents the soil strata data.
     * @param {number} numberOfSoils Number of soils in the soil strata.
     * @return {string} String representation of approximated soil definition.
     */
    var getSoilsApproximate = function(soil_strata, numberOfSoils) {
        var soils = [];
        soils.push(soil_strata.surface_distance_from_cut_toe.value);
        for (var i = 0; i < numberOfSoils - 1; i += 1) {
            soils.push(soil_strata.interfaces.depth.value[i]);
            soils.push(soil_strata.interfaces.angle.value[i]);
        }
        return soils.join(" ");
    };

    /**
     * Determine whether the supplied soil strata data is known or approximated.
     *
     * @param {object} soil_strata Object that represents the soil strata data.
     * @return {boolean} True if supplied soil strata data is approximated, false otherwise.
     */
    var isApproximateSoilStrata = function(soil_strata) {
        return ("interfaces" in soil_strata);
    };

    /**
     * Get the soil strata as a string for an approximate soil definition.
     *
     * @param {object} soil_strata Object that represents the soil strata data.
     * @return {string} String representation of the soil strata.
     */
    var getSoilStrataApproximate = function(soil_strata) {
        var soilStrata = [];
        numberOfSoils = getNumberOfSoils(soil_strata);
        soilStrata.push([numberOfSoils, 1, 99].join(" ")); // TODO: These magic numbers should disappear
        soilStrata.push(getSoilsApproximate(soil_strata, numberOfSoils));
        return soilStrata.join("\n");
    };

    /**
     * Get the soil strata as a string for a known soil definition.
     *
     * @param {object} soil_strata Object that represents the slope section data.
     * @return {string} String representation of the soil strata.
     */
    var getSoilStrataKnown = function(soil_strata) {
        var soilStrata = [];
        var numberOfSoils = soil_strata[0].depth.value.length + 1;
        var upslopeSections = getSections(soil_strata, function(section) {
          return (section.surface_distance_from_cut_toe.value <= 0);
        });
        var downslopeSections = getSections(soil_strata, function(section) {
          return (section.surface_distance_from_cut_toe.value > 0);
        });
        soilStrata.push([numberOfSoils, upslopeSections.length, downslopeSections.length].join(" "));
        soilStrata.push(getSlopeSectionsDepth(upslopeSections, "soil_strata"));
        soilStrata.push(getSlopeSectionsDepth(downslopeSections, "soil_strata"));
        return soilStrata.join("\n");
    };

    /**
     * Determine whether the supplied soil strata data is known or approximated.
     *
     * @param {object} soil_strata Object that represents the soil strata data.
     * @return {boolean} True if supplied soil strata data is approximated, false otherwise.
     */
    var isApproximateSoilStrata = function(soil_strata) {
        return ("interfaces" in soil_strata);
    };

    /**
     * Helper method for getGeometryFileQuesta. Gets the soil strata data as a string.
     *
     * @param {object} soil_strata Object that represents the soil strata data.
     * @return {string} String representation of the soil strata.
     */
    var getSoilStrata = function(soil_strata) {
        if (isApproximateSoilStrata(soil_strata)) {
            return getSoilStrataApproximate(soil_strata);
        } else {
            return getSoilStrataKnown(soil_strata);
        }
    };

    /**
     * Helper method for getGeometryFileQuesta. Gets the geometry mesh dimensions as a string.
     *
     * @param {object} mesh Object that represents the geometry mesh dimensions.
     * @return {string} String representation of geometry mesh.
     */
    var getMesh = function(mesh) {
        return [mesh.column_width.value, mesh.cell_depth.value].join(" ");
    };

    /**
     * Create the text content of a QUESTA geometry file.
     *
     * @param {object} doc The document containing the QUESTA geometry data.
     * @return {string} The text content of a QUESTA geometry file.
     */
    var getGeometryFileQuesta = function(doc) {
        var geometryFile = [];
        geometryFile.push(["Platform", doc.platform].join(" "));
        geometryFile.push("");
        geometryFile.push(getRoadPrism(doc.geometry.road, doc.geometry.slope_sections));
        geometryFile.push(getRoadLink(doc.geometry.road_link));
        geometryFile.push(getSlopeBreadth(doc.geometry.breadth));
        geometryFile.push("");
        geometryFile.push(getSlopeSections(doc.geometry.slope_sections, doc.geometry.road, doc.investment));
        geometryFile.push("");
        geometryFile.push(getWaterTable(doc.geometry.water_table));
        geometryFile.push("");
        geometryFile.push(getSoilStrata(doc.geometry.soil_strata));
        geometryFile.push("");
        geometryFile.push(getMesh(doc.geometry.mesh));
        return geometryFile.join("\n");
    };

    /**
     * Determines whether supplied document is a CHASM geometry file.
     *
     * @param {object} doc Document representing a geometry file.
     * @return {boolean} True if document is a CHASM geometry file.
     */
    var isGeometryFileChasm = function(doc) {
        return ("mesh_data" in doc.geometry);
    }

    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "geometry") {
        return errors.invalid_request("geometry");
    }
    if ('substitutions' in req.query) {
        doc = utils.doSubstitutions(req.query.substitutions, doc);
    }
    try {
        if (isGeometryFileChasm(doc)) {
            var geometryFile = getGeometryFileChasm(doc);
        } else {
            var geometryFile = getGeometryFileQuesta(doc);
        }
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = geometryFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
         "body": body
    };
};
