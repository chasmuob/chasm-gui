/** Show for point water sources documents
*
* Validate requested document.
* If invalid: return HTTP error code.
* If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
*/

/**
 * Entry point function, called by couchdb.
 *
 * @param {object} doc The document to be rendered by this function.
 * @param {object} req The HTTP request to which this function is responding.
 * @return {object} HTTP response object which is returned by couchdb to the caller.
 */
function(doc, req) {
    var errors = require('lib/errors').init();
    var sha1 = require('lib/sha1').init().sha1;

    /**
     * Convert the point water sources into rows of text
     *
     * @param {object} doc The document containing the point water sources data
     * @return {string} The text content of the actual point water source defs
     */
    var getPointWaterSources = function(doc) {
        var pointWaterSources = [];
        var newSource;
        var nextSource;
        for (var i = 0; i < doc.point_water_sources.length; ++i) {
            newSource = [];
            nextSource = doc.point_water_sources[i];
            newSource.push("coords:\t\t" + nextSource.coords.x.value + " " + nextSource.coords.y.value);
            newSource.push("rate:\t\t" + nextSource.rate.value);
            pointWaterSources.push(newSource.join("\n"));
        }
        return pointWaterSources.join("\n\n");
    };

    /**
     * Create the text content of a point water sources file.
     *
     * @param {object} doc The document containing the point water sources data.
     * @return {string} The text content of a point water sources file.
     */
    var getPointWaterSourcesFile = function(doc) {
        var rows = [];
        rows.push("number:\t\t" + doc.point_water_sources.length);
        rows.push(getPointWaterSources(doc));
        return rows.join("\n\n");
    };

    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "point_water_sources") {
        return errors.invalid_request('point water sources');
    }
    try {
        var pointWaterSourcesFile = getPointWaterSourcesFile(doc);
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = pointWaterSourcesFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
         "body": body
    };
};