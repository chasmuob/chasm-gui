/** Show for stability documents
*
* Validate requested document.
* If invalid: return HTTP error code.
* If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
*/

/**
 * Entry point function, called by couchdb.
 *
 * @param {object} doc The document to be rendered by this function.
 * @param {object} req The HTTP request to which this function is responding.
 * @return {object} HTTP response object which is returned by couchdb to the caller.
 */
function(doc, req) {
    var errors = require('lib/errors').init();
    var sha1 = require('lib/sha1').init().sha1;

    /**
     * Get a string representation of the supplied grid search parameters.
     *
     * @param {object} grid_search_parameters Grid search parameter data.
     * @return {string} String representation of supplied grid search parameters.
     */
    var getGridSearchParameters = function(grid_search_parameters) {
        var gridSearchParameters = [];
        gridSearchParameters.push(grid_search_parameters.origin.x);
        gridSearchParameters.push(grid_search_parameters.origin.y);
        gridSearchParameters.push(grid_search_parameters.spacing.x);
        gridSearchParameters.push(grid_search_parameters.spacing.y);
        gridSearchParameters.push(grid_search_parameters.grid_size.x);
        gridSearchParameters.push(grid_search_parameters.grid_size.y);
        gridSearchParameters.push(grid_search_parameters.radius.initial);
        gridSearchParameters.push(grid_search_parameters.radius.increment);
        return gridSearchParameters.join(" ");
    };

    /**
     * Determine whether the suppled stability document is for a Janbu automated stability analysis.
     *
     * @param {object} doc The stability document to be checked.
     * @return {boolean} True if stability document is for a Janbu automated stability analysis.
     */
    var isJanbuAutomated = function(doc) {
        return ("max" in doc.slip_surface_coordinates.y);
    };

    /**
     * Convert two arrays to a string of the following format:
     *   array1[0] array2[0]\narray1[1] array2[1]\n ... etc
     *
     * @param array1 {Array} array1 Array whose elements are to be first in the output rows.
     * @param array2 {Array} array2 Array whose elements are to be second in the output rows.
     * @return {string} String containing rows of paired array elements.
     */
    var pairArrayElements = function(array1, array2) {
        if (array1.length != array2.length) {
            throw("Arrays must be of equal length for elements to be paired");
        }
        var paired = [];
        for (var i = 0; i < array1.length; i += 1) {
            paired.push([array1[i], array2[i]].join(" "));
        }
        return paired.join("\n");
    };

    /**
     * Create the text content of a stability file.
     *
     * @param {object} doc The document containing the stability data.
     * @return {string} The text content of a stability file.
     */
    var getStabilityFile = function(doc) {
        var stabilityFile = [];
        stabilityFile.push(doc.stability_analysis_algorithm);
        if (doc.stability_analysis_algorithm.toLowerCase() == 'bishop') {
            stabilityFile.push(getGridSearchParameters(doc.grid_search_parameters));
            stabilityFile.push(doc.slope_surface_coordinates.x.length)
            stabilityFile.push(pairArrayElements(doc.slope_surface_coordinates.x, doc.slope_surface_coordinates.y));
        } else if (doc.stability_analysis_algorithm.toLowerCase() == 'janbu') {
            stabilityFile.push(doc.slope_surface_coordinates.x.length);
            stabilityFile.push(pairArrayElements(doc.slope_surface_coordinates.x, doc.slope_surface_coordinates.y));
            if (isJanbuAutomated(doc)) {
                stabilityFile.push(1);
                stabilityFile.push([doc.slip_surface_coordinates.y.max, doc.slip_surface_coordinates.y.min].join(" "));
                stabilityFile.push(pairArrayElements(doc.slip_surface_coordinates.y.bounds.lower, doc.slip_surface_coordinates.y.bounds.upper));
                stabilityFile.push(doc.slip_surface_coordinates.x.join("\n"));
            } else {
                stabilityFile.push(0);
                stabilityFile.push(doc.slip_surface_coordinates.x.length);
                stabilityFile.push(pairArrayElements(doc.slip_surface_coordinates.x, doc.slip_surface_coordinates.y));
            }
        } else {
            throw("Unknown stability algorithm");
        }
        return stabilityFile.join("\n");
    };

    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "stability") {
        return errors.invalid_request("stability");
    }
    try {
        var stabilityFile = getStabilityFile(doc);
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = stabilityFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
         "body": body
    };
};