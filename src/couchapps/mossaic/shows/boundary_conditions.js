/** Show for boundary condition documents
 *
 * Validate requested document.
 * If invalid: return HTTP error code.
 * If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
 */

/**
* Entry point function, called by couchdb.
*
* @param {object} doc The document to be rendered by this function.
* @param {object} req The HTTP request to which this function is responding.
* @return {object} HTTP response object which is returned by couchdb to the caller.
*/
function(doc, req) {
    var errors = require('lib/errors').init();
    var utils = require('lib/mossaicutils').init();
    var sha1 = require('lib/sha1').init().sha1;

    var RAINFALL_HOURS = 24;

    /**
     * Helper method which converts an array to a row of text where each row contains a set number of columns.
     *
     * @param {Array} values Array of values to be converted.
     * @param {number} columns Number of columns per row.
     * @return {string} Array values in required representation.
     */
    var arrayToRows = function(values, columns) {
        rows = [];
        for (var i = 0; i < values.length; i += columns) {
            rows.push(values.slice(i, i + columns).join(" "));
        }
        return rows.join("\n");
    };

    /**
     * Get the metadata (start time, end time and probability) for the supplied storm data.
     *
     * @param {object} storm Storm data.
     * @return {string} String representation of the storm metadata.
     */
    var getStormMetadata = function(storm) {
        var metadata = [];
        var end;
        for(var i = storm.start.value; i < storm.rainfall_intensities.value.length; i++){
          if(storm.rainfall_intensities.value[i] === 0){
            end = i;
            break;
          }
        }
        storm.end = end -1;
        metadata.push(storm.start.value);
        // metadata.push(storm.start.value + (storm.rainfall_intensities.value.length - 1));
        metadata.push(storm.end);
        if ("probability" in storm) {
            metadata.push(storm.probability);
        }
        return metadata.join(" ");
    };

    /**
     * Get the rainfall intensities for a given storm as a string representation where rainfall values for each day are separated by linebreaks.
     *
     * @param {object} storm Storm data containing rainfall intensities.
     * @return {string} String representation of the rainfall intensities.
     */
    var getRainfallIntensities = function(storm) {
        var rainfall = [];
        // for (var rainIndex = 0; rainIndex < storm.rainfall_intensities.value.length; rainIndex += RAINFALL_HOURS) {
        //     rainfall.push(arrayToRows(storm.rainfall_intensities.value.slice(rainIndex, rainIndex + RAINFALL_HOURS), 5));
        // }
        for ( var rainIndex = storm.start.value; rainIndex <= storm.end; rainIndex += RAINFALL_HOURS) {
          rainfall.push(arrayToRows(storm.rainfall_intensities.value.slice(rainIndex, rainIndex + RAINFALL_HOURS), 5));
        }
        return rainfall.join("\n\n");
    };

    /**
     * Get a string representation of the supplied array of design storm objects.
     *
     * @param {object} storm Array of design storm data.
     * @return {string} String representation of storm array.
     */
    var getDesignStorms = function(storm) {
        var storms = [];
        for (var i = 0; i < storm.length; i += 1) {
            newStorm = [];
            newStorm.push(getStormMetadata(storm[i]));
            newStorm.push("");
            newStorm.push(getRainfallIntensities(storm[i]));
            storms.push(newStorm.join("\n"));
        }
        return storms.join("\n\n");
    };

    /**
     * Determine whether the supplied document has design storms or not.
     *
     * @param {object} doc Document to be examined.
     * @return {boolean} True is document has one or more design storms, false otherwise.
     */
    var hasDesignStorm = function(doc) {
        return ("storm" in doc);
    };

    /**
     * Get string representation of one day of realistic ranfall.
     *
     * @param {object} day Object containing one day of realistic rainfall.
     * @return {string} String representation of supplied rainfall day.
     */
    var getRainfallDay = function(day) {
        var rainfallDay = [];
        rainfallDay.push(day.probability);
        rainfallDay.push(day.intensities.join(" "));
        return rainfallDay.join("\n");
    };

    /**
     * Get string representation of realistic rainfall data.
     *
     * @param {object} rainfall Realistic rainfall data.
     * @return {string} String representation of realistic rainfall data.
     */
    var getRealisticRainfall = function(rainfall) {
        rainfallDays = [];
        rainfallDays.push(rainfall.days.length);
        rainfallDays.push("");
        for (var i = 0; i < rainfall.days.length; i += 1) {
            rainfallDays.push(getRainfallDay(rainfall.days[i]));
        }
        return rainfallDays.join("\n");
    };

    /**
     * Create the text content of a boundary conditions file.
     *
     * @param {object} doc The document containing the boundary conditions data.
     * @return {string} The text content of a boundary conditions file.
     */
    var getBoundaryFile = function(doc) {
        var boundaryFile = [];
        boundaryFile.push(doc.upslope_recharge.value);
        boundaryFile.push([utils.getValue(doc.detention_capacity), utils.getValue(doc.soil_evaporation)].join(" "));
        boundaryFile.push("");
        if (hasDesignStorm(doc)) {
            boundaryFile.push(getDesignStorms(doc.storm));
        } else {
            boundaryFile.push(getRealisticRainfall(doc.rainfall));
        }
        return boundaryFile.join("\n");
    };
    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "boundary_conditions") {
        return errors.invalid_request("boundary conditions");
    }
    try {
        var boundaryFile = getBoundaryFile(doc);
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = boundaryFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
        "body": body
    };
};
