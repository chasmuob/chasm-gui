/** Show for reinforcements documents
*
* Validate requested document.
* If invalid: return HTTP error code.
* If valid: Convert document to text and return, returning HTTP error if anything goes wrong.
*/

/**
 * Entry point function, called by couchdb.
 *
 * @param {object} doc The document to be rendered by this function.
 * @param {object} req The HTTP request to which this function is responding.
 * @return {object} HTTP response object which is returned by couchdb to the caller.
 */
function(doc, req) {
    var errors = require('lib/errors').init();
    var sha1 = require('lib/sha1').init().sha1;

    /**
     * Determine whether supplied reinforcement data has geotextile reinforcements.
     *
     * @param {object} doc Reinforcement document.
     * @return {boolean} True if document has geotextile definitions.
     */
    var hasGeotextile = function(doc) {
        return ("geotextile" in doc);
    };

    /**
     * Determine whether supplied reinforcement data has earthnail reinforcements.
     *
     * @param {object} doc Reinforcement document.
     * @return {boolean} True if document has earthnail definitions.
     */
    var hasEarthnail = function(doc) {
        return ("earthnail" in doc);
    }

    /**
     * Get a string representation of the supplied reinforcement data.
     *
     * @param {object} reinforcement Reinforcement data.
     * @param {string} reinforcementName Name of this reinforcement type.
     * @return {string} String representation of the reinforcement data.
     */
    var getReinforcement = function(reinforcement, reinforcementName) {
        var reinforcementData = [];
        reinforcementData.push(reinforcementName);
        reinforcementData.push(["number:", reinforcement.length].join(" "));
        for (var i = 0; i < reinforcement.length; i += 1) {
            var row = [];
            row.push([i + 1, ":"].join(""));
            row.push(reinforcement[i].inclination.value);
            row.push(reinforcement[i].length.value);
            row.push(reinforcement[i].start.x.value);
            row.push(reinforcement[i].start.y.value);
            if ("interaction" in reinforcement[i]) {
                row.push(reinforcement[i].interaction);
            }
            if ("friction" in reinforcement[i]) {
                row.push(reinforcement[i].friction.value);
            }
            row.push(reinforcement[i].tensile_strength.value);
            row.push(reinforcement[i].factor_of_safety);
            reinforcementData.push(row.join(" "));
        }
        return reinforcementData.join("\n");
    };

    /**
     * Create the text content of a reinforcements file.
     *
     * @param {object} doc The document containing the reinforcements data.
     * @return {string} The text content of a reinforcements file.
     */
    var getReinforcementsFile = function(doc) {
        var reinforcementsFile = [];
        if (hasGeotextile(doc)) {
            reinforcementsFile.push(getReinforcement(doc.geotextile, "GEOTEXTILE"));
        }
        if (hasEarthnail(doc)) {
            if (reinforcementsFile.length > 0) {
                reinforcementsFile.push("");    // Add a linebreak if there is anything above
            }
            reinforcementsFile.push(getReinforcement(doc.earthnail, "EARTHNAIL"));
        }
        if (reinforcementsFile.length == 0) {
            throw "Document is not a valid reinforcements file";
        }
        return reinforcementsFile.join("\n");
    };

    var contentType = "text/plain";
    if (doc == null) {
        if (req.id == null) {
            return errors.null_doc();
        } else {
            return errors.doc_not_found(req.id);
        }
    } else if (doc.type != "reinforcements") {
      return errors.invalid_request("reinforcements");
    }
    try {
        var reinforcementsFile = getReinforcementsFile(doc);
    } catch (error) {
        log(error);
        return errors.internal_error(doc._id, error);
    }
    var body = reinforcementsFile;
    if ("sha1" in req.query && req.query.sha1 === "true") {
        body = sha1(body);
    }
    return {
        "headers": {
          "Content-Type": contentType
         },
         "body": body
    };
};