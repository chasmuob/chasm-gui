function(doc) {
  if ("destination_type" in doc && !("failed" in doc) &&
      !("converted" in doc) && !doc["converted"]) {
    emit(doc.destination_type, null);
  }
}