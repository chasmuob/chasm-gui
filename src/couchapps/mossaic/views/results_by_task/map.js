/**

Return the results for a particular task, keyed by task id, minimum factor of safety and job id.

This lets us do the following useful queries:
 - All jobs with results for task TASK_ID:
  _design/workflow/_view/results_by_task?reduce=false&startkey=["TASK_ID"]&endkey=["TASK_ID",{}]
 - All jobs where minimimum factor of safety is below 1.2:
  _design/workflow/_view/results_by_task?reduce=false&startkey=["TASK_ID",0]&endkey=["TASK_ID",1.2]
 - sum, min, max, sumsqr of net expected present value for task TASK_ID
  _design/workflow/_view/results_by_task?group_level=1&startkey=["t0"]&endkey=["t0",{}]
 - As above only for results where factor of safety is below 1.1:
  _design/workflow/_view/results_by_task?group_level=1&startkey=["t0",0]&endkey=["t0",1.5]

 */
function(doc) {
  if (doc.type == "results") {
    var min_fos = NaN;
    if ("chasm_output" in doc && typeof(doc.chasm_output) == typeof({}) && "factor_of_safety" in doc.chasm_output) {
      min_fos = Math.min.apply(Math.min, doc.chasm_output.factor_of_safety);
    }
    var nepv = 0;
    if ("questa_output" in doc && typeof(doc.questa_output) == typeof({}) && doc.questa_output.net_expected_present_value) {
      nepv = doc.questa_output.net_expected_present_value;
    }
    emit([doc.task_id, min_fos, doc.job_id], nepv);
  }
}