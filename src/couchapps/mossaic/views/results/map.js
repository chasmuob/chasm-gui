/** View which indexes results by job_id field
 */

/**
 * Entry point for view function, called by view server.
 *
 * @param {object} doc Document to be indexed.
 */
function(doc) {
    if (doc.type == "results" && "job_id" in doc) {
        emit(doc.job_id, null);
    }
}