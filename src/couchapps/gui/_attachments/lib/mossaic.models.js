/**
 * @file Defines various subclasses of Backbone.Model used in the app
 */
(function() {
  /**
   * Backbone Models used in the application.
   *
   * @namespace
   */
  Mossaic.models = {};

  /**
   * Modified Backbone.Model from which all Mossaic models inherit.
   *
   * Adds support for is_dirty which tells us whether a model has changes
   * that have not yet been persisted and overrides fetch, save and clear
   * to correctly handle is_dirty.
   *
   * @constructor
   * @extends Backbone.Model
   */
  Mossaic.models.Basic = Backbone.Model.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Basic
     * @instance
     * @type object
     * @property {boolean} is_dirty True if newly initialised models should be
     *                              considered dirty (i.e. have unpersisted
     *                              changes).
     */
    defaults: {
      is_dirty: false
    },
    /**
     * Initialize the model
     *
     * @memberof Mossaic.models.Basic
     * @instance
     * @param {object} attributes Hash of attributes to be stored in the model
     * @param {object} options Hash of model options
     * @param {boolean} options.is_dirty Set to true if the initial model
     *                             should be considered dirty
     */
    initialize: function(attributes, options) {
      var options = options || {};
      Backbone.Model.prototype.initialize.apply(this, [attributes, options]);
      var that = this;
      this.is_dirty = false;
      this.bind("change", function() {
        var attributes_equal = _.isEqual(that.previousAttributes(),
            that.toJSON());
        var previous_attributes_persisted = "_id" in that.previousAttributes();
        if (!attributes_equal && previous_attributes_persisted) {
          that.is_dirty = true;
        }
      });
    },
    /**
     * Fetch the model from the DB
     *
     * @memberof Mossaic.models.Basic
     * @instance
     * @param {object} options Hash of options
     * @param {function} options.success Function to be called if fetch is
     *                                   successful
     */
    fetch: function(options) {
      var that = this;
      options || (options = {});
      var model = that;
      var success = options.success;
      options.success = function(resp, status, xhr) {
        that.is_dirty = false;
        if (success) success(model, resp);
      };
      Backbone.Model.prototype.fetch.apply(this, [options]);
    },
    /**
     * Clear the model attributes and reset them to their default values.
     *
     * @memberof Mossaic.models.Basic
     * @instance
     */
    clear: function() {
       var that = this;
       var options = {};
       that.is_dirty = true;
       options.silent = true;
       var ret = Backbone.Model.prototype.clear.apply(this, [options]);
       that.set(that.defaults, {silent: false});
       return ret;
    },
    /**
     * Save the model attribtues and update is_dirty on success.
     *
     * @memberof Mossaic.models.Basic
     * @instance
     * @param {object} attrs Hash of attributes to be updated before saving
     * @param {object} options Hash of model options
     * @param {function} options.success Function to be called if fetch is
     *                                   successful
     */
    save: function(attrs, options) {
      var that = this;
      options || (options = {});
      var model = that;
      var success = options.success;
      options.success = function(resp, status, xhr) {
        that.is_dirty = false;
        if (success) success(model, resp, xhr);
      };
      Backbone.Model.prototype.save.apply(this, [attrs, options]);
    }
  });

  /**
   * Backbone Model for storing questa geometry data.
   *
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.Geometry = Mossaic.models.Basic.extend({
    /**
     * Initialize the model.
     * Called implicitly with "new Mossaic.models.Geometry".
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {object} attributes Hash of attributes to be stored in the model
     * @param {object} options Hash of model options
     * @param {Mossaic.models.Meta} options.datum Reference to the
     *                                            Mossaic.models.Meta which
     *                                            contains the slope datum
     *                                            metadata
     * @param {Backbone.Collection} options.layers Backbone.Collection
     *                                representing the soil strata interfaces.
     * @param {string} options.mode Specify whether the geometry should be
     *                              for a "chasm" or "questa" simulation.
     */
    initialize: function(attributes, options) {
      var that = this;
      var options = options || {};
      Mossaic.models.Basic.prototype.initialize.apply(this,
          [attributes, options]);
      _.bindAll(this);
      /**
       * A reference to the Mossaic.models.Meta which contains the slope
       * datum metadata. Required because we must copy in the breadth and
       * road_link fields on saving if we are to be able to create a valid
       * questa input file.
       * @type Mossaic.models.Meta
       */
      this.datum = options.datum || null;
      /**
       * A Backbone.Collection which is bound to
       * the soil strata in the model. Using a bound collection means widgets
       * can add/remove models from the collection and not worry about how
       * the internals of the Geometry model need to be updated.
       * @type Backbone.Collection
       */
      this.layers = options && options.layers || new Backbone.Collection(
          new Backbone.Model({})
      );
      this.layers.bind("add", this.add_soil_layer);
      this.layers.bind("remove", this.remove_soil_layer);
      this.set(attributes);
      if (!this.has("geometry")) {
        this.set({geometry: {
          slope_sections: []
        }});
      }
      /**
       * The geometry model can work in either "chasm" or "questa" mode. Questa
       * mode causes extra "wrapping" slope sections to be generated and uses a
       * slope section as a datum. Chasm mode uses a point as the datum and also
       * generates the chasm geometry mesh upon saving.
       * @type string
       */
      this.mode = options.mode || "questa";
      /**
       * Whether the model will be stochastic or deterministic by default.
       * @type boolean
       */
      this.default_stochastic = false;
      /**
       * Fetches the model using Mossaic.models.Basic but also ensures that
       * the mode is set correctly when the model is loaded, and that the
       * layers collection is updated to match the number of soil strata.
       * @param {object} options Hash of options
       * @param {function} options.success Success function to be executed
       *                                   when the model is fetched
       */
      this.fetch = function(options) {
        options || (options = {});
        var model = that;
        var success = options.success;
        options.success = function(resp, status, xhr) {
         var geometry = that.get("geometry");
          if (typeof(geometry) !== "undefined") {
            if (typeof(geometry.mesh_data) !== "undefined") {
              that.mode = "chasm";
            } else {
              that.mode = "questa";
            }
          }
          var layers_to_add = that.get_number_of_soil_interfaces();
          that.layers.reset([new Backbone.Model({})], {silent: true});
          _.each(_.range(layers_to_add), function() {
            that.layers.add(new Backbone.Model({}), {silent: true});
          });
          if (success) success(model, resp);
        };
        that.default_stochastic = false;
        Mossaic.models.Basic.prototype.fetch.apply(this, [options]);
      };
      /**
       * Clears the model using the method defined in Mossaic.models.Basic but
       * also ensures the associated layers are properly reset
       *
       * @memberof Mossaic.models.Basic
       * @instance
       */
      this.clear = function() {
        that.layers.reset([new Backbone.Model({})], {silent: true});
        that.default_stochastic = false;
        Mossaic.models.Basic.prototype.clear.apply(this);
      };
    },
    /**
     * A hash of booleans which defines which aspects of the model can be
     * edited. This is used by views to determine whether they should make
     * changes to the model. It is not enforced in the model.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @type object
     * @property {boolean} surface Surface geometry is editable
     * @property {boolean} water_table Water table depths are editable
     * @property {boolean} soil_strata Soil strata interface depths are editable
     * @property {boolean} datum Slope datum location is editable
     */
    editable: {
      "surface": false,
      "water_table": false,
      "soil_strata": false,
      "datum": false
    },
    /**
     * A hash of default values for a new model.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @type object
     */
    defaults: {
      type: "geometry",
      investment: {
        cut_slope_increment: {
          unit: "deg",
          value: 0
        }
      },
      geometry: {
        datum: {
          unit: "m",
          value: 0
        },
        mesh: {
          column_width: {
            unit: "m",
            value: 1
          },
          cell_depth: {
            unit: "m",
            value: 1
          }
        },
        road: {
          upslope_shoulder: {
            unit: "m"
          },
          downslope_shoulder: {
            unit: "m"
          }
        },
        slope_sections: [],
        water_table: []
      }
    },
    /**
     * Performs Geometry-specific actions before saving. If the geometry mode
     * is chasm then the chasm mesh data is generated. If the geometry mode is
     * questa, then the breadth and road_link fields of the Mossaic.models.Meta
     * are copied in.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     */
    save: function(attributes, options) {
      var geometry = _.clone(this.get("geometry"));
      if (this.datum) {
        geometry.breadth = this.datum.get("breadth");
        geometry.road_link = this.datum.get("road_link");
        geometry.location = this.datum.get("location");
      }
      if (this.mode === "questa" &&
          (typeof(geometry.road_link) === "undefined" ||
          geometry.road_link.length != 2)) {
        throw("Cannot create a Questa slope profile for a datum with no " +
            "defined road link");
      }
      if (this.mode === "chasm") {
        geometry.mesh_data = this.generate_chasm_mesh();
      }
      this.set({geometry: geometry}, {silent: true});
      Mossaic.models.Basic.prototype.save.apply(this, [attributes, options]);
    },
    /**
     * Get the number of soil interfaces present in the model.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @return {integer} Number of soil interfaces in the model
     */
    get_number_of_soil_interfaces: function() {
      var geometry = this.get("geometry");
      var number_of_interfaces = 0;
      if (typeof(geometry.soil_strata) !== "undefined" &&
          typeof(geometry.soil_strata.length) !== "undefined" &&
          typeof(geometry.soil_strata[0] !== "undefined")) {
        number_of_interfaces = geometry.soil_strata[0].depth.value.length;
      } else if (typeof(geometry.soil_strata) !== "undefined" &&
          typeof(geometry.soil_strata.interfaces) !== "undefined" &&
          typeof(geometry.soil_strata.interfaces.depth) !== "undefined") {
        if (typeof(geometry.soil_strata.interfaces.depth.value) !== "undefined") {
          number_of_interfaces = geometry.soil_strata.interfaces.depth.value.length;
        } else if (typeof(geometry.soil_strata.interfaces.depth.mean) !== "undefined") {
          number_of_interfaces = geometry.soil_strata.interfaces.depth.mean.length;
        }
      }
      return number_of_interfaces;
    },
    /**
     * Compute the water table coordinates from a supplied set of slope
     * coordinates and the water table depth measurements stored in the model.
     * The water table depth measurements are specified by the distance from
     * the datum along the surface and the depth at that point. This method
     * computes the actual coordinates of each depth in model space.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @private
     * @param {array} coords Array of slope coordinates
     * @return {array} Array of water table depth measurements
     */
    compute_water_table_coords: function(coords) {
      var that = this;
      var depths = _.map(this.get_water_depths(), function(depth) {
        return that.compute_depth_x_y(depth, coords, {
          quantize: false
        });
      });
      var lines = [];
      for (var i = 1; i < depths.length; ++i) {
        lines = lines.concat([this.compute_line_coords(
          depths[i - 1], depths[i], i, coords, depths)
        ]);
      }
      var water_table = [];
      for (var i = 0; i < lines.length; ++i) {
        if (i == 0) {
          water_table = water_table.concat([lines[i][0]]);
        }
        water_table = water_table.concat([lines[i][1]]);
      }
      return water_table;
    },
    /**
     * Compute the soil strata coordinates from a supplied set of slope
     * coordinates and the soil strata depth measurements stored in the model.
     * The soil strata depth measurements are specified by the distance from
     * the datum along the surface and the depth at that point. This method
     * computes the actual coordinates of each depth in model space.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @private
     * @param {array} coords Array of slope coordinates
     * @return {array} Array of arrays of depth measurements for each soil
     *                 strata
     */
    compute_soil_strata_coords: function(coords) {
      var that = this;
      var strata;
      if (this.is_soil_known()) {
        var soil_depths_by_strata = _.map(this.get_soil_depths(), function(strata) {
          return _.map(strata, function(depth) {
            return that.compute_depth_x_y(depth, coords, {
              quantize: false
            });
          });
        });
        var lines_by_strata = _.map(soil_depths_by_strata, function(strata) {
          var lines = [];
          for (var i = 1; i < strata.length; ++i) {
            lines = lines.concat([that.compute_line_coords(
              strata[i - 1], strata[i], i, coords, strata)
            ]);
          }
          return lines;
        });
        strata = _.map(lines_by_strata, function(strata) {
          var depth_coords = [];
          for (var i = 0; i < strata.length; ++i) {
            if (i == 0) {
              depth_coords = depth_coords.concat([strata[i][0]]);
            }
            depth_coords = depth_coords.concat([strata[i][1]]);
          }
          return depth_coords;
        });
      } else {
        var soil_depths = this.get_soil_estimates();
        strata = _.map(soil_depths, function(depth) {
          var origin = that.compute_depth_x_y(depth, coords, {
            quantize: false
          });
          var estimated_coords = that.compute_estimate_coords(depth, origin, coords);
          return [estimated_coords.start, estimated_coords.end];
        });
      }
      return strata;
    },
    /**
     * Generate a geometry mesh for chasm from the current geometry.
     *
     * The generated mesh is a representation of the current geometry
     * specification in the format required in order to be able to generate
     * chasm geometry files.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @private
     * @return {object} Mesh cell data for a chasm geometry file
     */
    generate_chasm_mesh: function() {
      var that = this;
      var mesh = this.get_mesh();
      var coords = this.compute_coords({quantize: false});
      var coords_q = this.compute_coords();
      // Use the non-quantized coords to compute the depths
      // because the surface distance specified is the surface distance along
      // the real slope, not the quantized slope
      // Then quantize the depths after they have been computed from the
      // un-quantized coordinates, because the actual depths refer to a specific
      // cell in the mesh
      var water_table_coords = this.compute_water_table_coords(coords);
      var soil_strata_coords = this.compute_soil_strata_coords(coords);
      var max_x = this.quantize_coord({x: coords_q[coords_q.length - 1].x, y: 0}).x;
      var number_of_cols = max_x / mesh.x;
      /**
       * Get profile height (in cells) at column
       */
      var get_height = function(col, coords) {
        var col_x = col * mesh.x + mesh.x / 2;
        var coord_to_left = 0;
        _.find(coords, function(coord, i) {
          if (coord.x >= col_x) {
            return true;
          } else {
            coord_to_left = i;
          }
        });
        if (coord_to_left < 0) {
          console.log("Column " + col + " out of range");
          return;
        }
        var x = coords[coord_to_left + 1].x - coords[coord_to_left].x;
        var y = coords[coord_to_left].y - coords[coord_to_left + 1].y;
        var tan_theta = y / x;
        var dx = col_x - coords[coord_to_left].x;
        // This is the height in units, so we now need to convert it to cells
        // If the height is greater than the cell midpoint, round up
        // if less, round down
        var actual_height = coords[coord_to_left + 1].y + (x - dx) * tan_theta;
        var height = that.quantize_coord({
            x: 0,
            y: actual_height
          }).y;
        return height / mesh.y;
      };
      /**
       * Get number of saturated cells for a given column
       */
      var get_sat = function(col, water_table_coords, slope_coords) {
        if (typeof(water_table_coords) === "undefined" ||
            water_table_coords.length === 0) {
          return 0;
        }
        var water_table_height = get_height(col, water_table_coords);
        var slope_height = get_height(col, slope_coords);
        if (water_table_height > slope_height) {
          return slope_height;
        } else if (water_table_height < 0) {
          return 0;
        } else {
          return water_table_height;
        }
      };
      /**
       * Get soil type of each cell for col
       */
      var get_cells = function(col, soil_strata_coords, slope_coords) {
        var slope_height = get_height(col, slope_coords);
        var cells = _.map(_.range(slope_height).reverse(), function(height) {
          var soil_type = 0;
          var found_strata = _.find(soil_strata_coords, function(strata, i) {
            var strata_height = get_height(col, strata);
            soil_type = i;
            return height >= strata_height;
          });
          if (found_strata) {
            return soil_type;
          } else {
            return ++soil_type;
          }
        });
        return cells;
      };
      var mesh_data = _.map(_.range(number_of_cols), function(col) {
        var height = get_height(col, coords);
        var col_data = {};
        col_data.number_of_saturated_cells = get_sat(
            col, water_table_coords, coords);
        col_data.column_breadth = {
          unit: "m",
          value: 1
        };
        col_data.column_width = {
          unit: "m",
          value: mesh.x
        };
        col_data.initial_surface_suction = {
          unit: "m",
          value: -0.5
        };
        col_data.cells = {
          cell_depth: {
            unit: "m",
            value: _.map(_.range(height), function() {
              return mesh.y;
            })
          },
          soil_type: get_cells(col, soil_strata_coords, coords)
        };
        return col_data;
      });
      var index_of_first_empty_col = 0;
      _.find(mesh_data, function(col, i) {
        index_of_first_empty_col = i;
        return col.cells.soil_type.length === 0 ||
            col.cells.cell_depth.value === 0;
      });
      return _.first(mesh_data, index_of_first_empty_col);
    },
    /**
     * Override the default set behaviour.
     * If slope sections have been changed then we get the "missing"
     * field (one of surface_length, height or angle) then we pass through to
     * the original set function.
     *
     * Default parameters are (key, value, options) but can also be called
     * with (attributes, options) where attributes is a hash with the key,value
     * to be set.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {string} key Model field to be set/updated
     * @param {object} value Value to be assigned to model.key
     * @param {object} options Hash of options as supported by
     *                         Backbone.Model.set
     */
    set: function(key, value, options) {
      // Copy hack from Backbone.js source
      var attrs;
      if (_.isObject(key)) {
        attrs = key;
        options = value;
      } else {
        attrs = {};
        attrs[key] = value;
      }
      options = options || {};
      var have_slope_sections = (typeof(attrs.geometry) !== "undefined" &&
          attrs.geometry !== null &&
          typeof(attrs.geometry.slope_sections) !== "undefined");
      if (have_slope_sections) {
        _.each(attrs.geometry.slope_sections, function(section) {
          if (!section.hasOwnProperty("surface_length")) {
            section.surface_length = {unit: "m", value: undefined};
          }
          if (!section.hasOwnProperty("surface_height")) {
            section.surface_height = {unit: "m", value: undefined};
          }
          if (!section.hasOwnProperty("angle")) {
            section.angle = {unit: "deg", value: undefined};
          }
        });
        this.generate_missing_section_values(attrs.geometry.slope_sections);
      }
      return Backbone.Model.prototype.set.apply(this, [attrs, options]);
    },
    /**
     * Convert section index to coordinate index.
     *
     * There may be more coordinate pairs than defined slope sections as some
     * coordinates may be auto-generated. This function will convert a section
     * index into the index of the first coordinates of the coordinates pair
     * describing that section.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {int} index Section index
     * @return Index of first coordinate pair describing given section
     */
    section_index_to_coords_index: function(index) {
      return index + this.get_number_of_extra_sections().number_of_top_sections;
    },
    /**
     * Convert coordinate index to section index
     *
     * Does the inverse of section_index_to_coords_index.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {int} index Coordinates index
     * @return Index of section for which coordinates pair are describing the
     *         left-most point
     */
    coords_index_to_section_index: function(index) {
      return index - this.get_number_of_extra_sections().number_of_top_sections;
    },
    /**
     * Get a hash of the number extra sections to wrap the user defined
     * sections.
     * For mode == "chasm", these will be zero. For questa they will be
     * nonzero.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @return {object} Provides two fields: number_of_top_sections and
     *                  number_of_bottom_sections
     */
    get_number_of_extra_sections: function() {
      if (this.mode === "questa") {
        return {
          number_of_top_sections: 1,
          number_of_bottom_sections: 2
        };
      } else {
        return {
          number_of_top_sections: 0,
          number_of_bottom_sections: 0
        }
      }
    },
    /**
     * Create a new section of slope surface by splitting the section containing
     * the point at a given distance from x = 0
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {float} distance Distance of split from x = 0 (i.e. the left-most
     *                         point of the slope surface)
     * @return {integer} The index of the new section
     */
    split_section: function(distance) {
      // Find section where total surface length > distance
      var geometry = _.clone(this.get("geometry"));
      var slope_sections = _.clone(geometry.slope_sections);
      var point_left_of_distance = 0;
      var total_surface_length = 0;
      var offset;
      var section_to_split = _.find(slope_sections, function(section, i) {
        total_surface_length += section.surface_length.value;
        point_left_of_distance = i;
        return total_surface_length > distance;
      });
      if (typeof(section_to_split) === "undefined" || distance < 0) {
        throw "Cannot split out-of-range section";
      }
      offset = distance - (total_surface_length - section_to_split.surface_length.value);
      var new_sections = [
        {
          angle: section_to_split.angle,
          surface_length: {
            unit: "m",
            value: offset
          }
        },
        {
          angle: section_to_split.angle,
          surface_length: {
            unit: "m",
            value: total_surface_length - distance
          }
        }
      ]
      var new_slope_sections = slope_sections.splice(0, point_left_of_distance);
      geometry.slope_sections = new_slope_sections.concat(new_sections).concat(slope_sections.splice(1));
      this.changed_by = this;
      this.set({geometry: geometry});
      return point_left_of_distance + 1;
    },
    /**
     * Set the road section
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {integer} value Index of slope surface section that is to be the
     *                        road
     * @param {object} options Hash of options
     * @param {object} changed_by The object that set the road section
     */
    set_road_section: function(value, options) {
      var options = options || {};
      var changed_by = options.changed_by;
      var geometry = _.clone(this.get("geometry"));
      var road = _.clone(geometry.road);
      road.slope_section = value;
      geometry.road = road;
      this.changed_by = changed_by;
      this.set({geometry: geometry});
    },
    /**
     * Generate the "missing" value for a section of slope profile using
     * trigonometry.
     *
     * Slope sections can be defined by any two of surface length, height and
     * angle. This function automatically calculates whichever value is
     * unspecified for each slope surface section.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @private
     * @param {array} slope_sections Array of slope sections
     */
    generate_missing_section_values: function(slope_sections) {
      // TODO replace the stuff that operates on add section data with this too
      var slope_sections = slope_sections;
      _.each(slope_sections, function(section, i) {
        if (typeof(section.surface_length.value) === "undefined") {
          if (section.angle.value === 0) {
            console.log("Cannot create section with height > 0 and angle == 0");
          } else {
            section.surface_length.value = section.surface_height.value / Math.sin(Mossaic.degs_to_rads * section.angle.value);
          }
        }
        if (typeof(section.surface_height.value) === "undefined") {
          section.surface_height.value = section.surface_length.value * Math.sin(Mossaic.degs_to_rads * section.angle.value);
        }
        if (typeof(section.angle.value) === "undefined") {
          if (section.surface_height.value > section.surface_length.value) {
            console.log("Cannot create section where height is greater than surface length");
          } else {
            if (section.surface_length.value === 0) {
              section.angle.value = 90;
            } else {
              section.angle.value = Math.asin(section.surface_height.value / section.surface_length.value) / Mossaic.degs_to_rads;
            }
          }
        }
      });
    },
    /**
     * Convenience function to get a blank soil strata estimate
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @private
     * @param {boolean} stochastic True if a stochastic default soil estimate is
     *                             required
     * @return {object} Soil strata estimate
     */
    get_blank_soil_estimate: function(stochastic) {
      var soil = {
        surface_distance_from_cut_toe: {
          unit: "m",
          value: 0
        },
        interfaces: {
          depth: {
            unit: "m"
          },
          angle: {
            unit: "deg"
          }
        }
      };
      if (stochastic) {
        soil.interfaces.depth.mean = [];
        soil.interfaces.depth.standard_deviation = [];
        soil.interfaces.angle.mean = [];
        soil.interfaces.angle.standard_deviation = [];
      } else {
        soil.interfaces.depth.value = [];
        soil.interfaces.angle.value = [];
      }
      return soil;
    },
    /**
     * Add a layer to the soil strata measurements. This is bound to this.layers
     * and is triggered by changes on that collection.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {Backbone.Model} model The model that triggered the change
     * @param {Backbone.Collection} collection The collection that holds the
     *                                         model that triggered the change
     */
    add_soil_layer: function(model, collection) {
      var that = this;
      var is_stochastic = this.is_stochastic("soil_strata");
      var changed_by = collection.changed_by;
      var geometry = _.clone(this.get("geometry"));
      var soil_strata = _.clone(geometry.soil_strata);
      if (typeof(soil_strata) === "undefined") {
        // Create empty list
        if (this.is_soil_known()) {
          soil_strata = [];
        } else {
          soil_strata = this.get_blank_soil_estimate(is_stochastic);
        }
      } else if (!this.is_soil_known() && typeof(soil_strata.interfaces) === "undefined") {
        var surface_distance_from_cut_toe = soil_strata.surface_distance_from_cut_toe;
        if (typeof(surface_distance_from_cut_toe) === "undefined") {
          surface_distance_from_cut_toe = 0;
        }
        soil_strata = this.get_blank_soil_estimate(is_stochastic);
        soil_strata.surface_distance_from_cut_toe = surface_distance_from_cut_toe;
      } else {
        if (this.layers.length > 1) {
          if (this.is_soil_known()) {
            soil_strata = _.map(soil_strata, function(section, i) {
              var modified_section = _.clone(section);
              modified_section.depth = _.clone(modified_section.depth);
              var previous_layer = that.layers.length - 3;
              var depth = 2;
              if (is_stochastic) {
                var soil_depths = _.clone(modified_section.depth.mean);
              } else {
                var soil_depths = _.clone(modified_section.depth.value);
              }
              if (previous_layer >= 0) {
                var depth = soil_depths[previous_layer] + 2;
              }
              soil_depths.push(depth);
              if (is_stochastic) {
                modified_section.depth.mean = soil_depths;
                modified_section.depth.standard_deviation =
                    _.clone(modified_section.depth.standard_deviation);
                modified_section.depth.standard_deviation.push(0);
              } else {
                modified_section.depth.value = soil_depths;
              }
              return modified_section;
            });
          } else {
            var depth = (soil_strata.depth && _.clone(soil_strata.depth)) ||
              {unit: "m", value: []};
            var depths = _.clone(depth.value);
            var angle = (soil_strata.angle && _.clone(soil_strata.angle)) ||
              {unit: "m", value: []};
            var angles = _.clone(angle.value);
            if (depths.length > 0) {
              depths.push(_.last(depths) + 2);
            } else {
              depths.push(undefined);
            }
            if (angles.length > 0) {
              angles.push(_.last(angles));
            } else {
              angles.push(undefined);
            }
            depth.value = depths;
            angle.value = angles;
            soil_strata.depth = depth;
            soil_strata.angle = angle;
          }
        }
      }
      geometry.soil_strata = soil_strata;
      this.changed_by = changed_by;
      this.set({geometry: geometry});
      if (soil_strata.length == 0) {
        // Manually fire change event if we don't yet have any depths
        this.change();
      }
    },
    /**
     * Remove a layer from the soil strata measurements. This is bound to
     * this.layers and is triggered by changes on that collection.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {Backbone.Model} model The model that triggered the change
     * @param {Backbone.Collection} collection The collection that holds the
     *                                         model that triggered the change
     * @param {integer} index The index of the soil layer to be removed
     */
    remove_soil_layer: function(model, collection, index) {
      var that = this;
      var is_stochastic = this.is_stochastic("soil_strata");
      var changed_by = collection.changed_by;
      var geometry = _.clone(this.get("geometry"));
      var soil_strata = _.clone(geometry.soil_strata);
      if (typeof(soil_strata) === "undefined") {
        return;
      }
      // Always remove the upper soil, unless it's the first element
      var to_remove = index.index - 1;
      if (to_remove < 0) {
        to_remove = 0;
      }
      if (this.is_soil_known()) {
        soil_strata = _.map(soil_strata, function(section, i) {
          var modified_section = _.clone(section);
          modified_section.depth = _.clone(modified_section.depth);
          if (is_stochastic) {
            var soil_depths = _.clone(modified_section.depth.mean);
            var depth_sds = _.clone(modified_section.depth.standard_deviation);
            soil_depths.splice(to_remove, 1);
            depth_sds.splice(to_remove, 1);
            modified_section.depth.mean = soil_depths;
            modified_section.depth.standard_deviation = depth_sds;
          } else {
            var soil_depths = _.clone(modified_section.depth.value);
            soil_depths.splice(to_remove, 1);
            modified_section.depth.value = soil_depths;
          }
          return modified_section;
        });
      } else {
        var interfaces = _.clone(soil_strata.interfaces);
        var depth = _.clone(interfaces.depth);
        var angle = _.clone(interfaces.angle);
        if (is_stochastic) {
          var depths = _.clone(depth.mean);
          depths.splice(to_remove, 1);
          depth.mean = depths;
          var depths_sd = _.clone(depth.standard_deviation);
          depths_sd.splice(to_remove, 1);
          depth.standard_deviation = depths_sd;
          var angles = _.clone(angle.mean);
          angles.splice(to_remove, 1);
          angle.mean = angles;
          var angles_sd = _.clone(angle.standard_deviation);
          angles_sd.splice(to_remove, 1);
          angle.standard_deviation = angles_sd;
        } else {
          var depths = _.clone(depth.value);
          depths.splice(to_remove, 1);
          depth.value = depths;
          var angles = _.clone(angle.value);
          angles.splice(to_remove, 1);
          angle.value = angles;
        }
        interfaces.depth = depth;
        interfaces.angle = angle;
        soil_strata.interfaces = interfaces;
      }
      geometry.soil_strata = soil_strata;
      this.changed_by = changed_by;
      this.set({geometry: geometry}, {silent: true});
      // Manually fire change event
      this.change();
    },
    /**
     * Determine whether soil strata data is for known measurements
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @return {boolean} True if soil strata data comprises known measurements
     *                   and false if estimates have been made
     */
    is_soil_known: function() {
      var geometry = this.get("geometry");
      return (typeof(geometry.soil_strata) !== "undefined" &&
          typeof(geometry.soil_strata.length) !== "undefined");
    },
    /**
     * Return section index of inverted sections, where x > d_x
     *
     * Note: Inverted sections are not supported by questa but they may exist
     * temporarily whilst a geometry is tweaked. If so, we want to keep track
     * of them.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @private
     * @param {array} coords Slope profile coordinates
     * @param {int} index_offset Offset between sections and coordinates
     *                           may be non-zero if there are auto-generated
     *                           parts of the slope
     * @return {object} Hash of section indices that correspond to inverted
     *                  sections - can be searched using the in operator
     */
    get_inverted_sections: function(coords, index_offset) {
      var inverted_sections = {};
      var last_coord;
      _.each(coords, function(coord, i) {
        if (typeof(last_coord) !== "undefined") {
          if (coord.x - last_coord.x < 0) {
            inverted_sections[i - index_offset] = undefined;
          }
        }
        last_coord = coord;
      });
      return inverted_sections;
    },
    /**
     * Convenience function which returns the mesh column width and cell depth
     * in an easily manipulated format.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @return Hash representing mesh x and y size
     */
    get_mesh: function() {
      var geometry = this.get("geometry");
      var grid_x = geometry.mesh.column_width.value;
      var grid_y = geometry.mesh.cell_depth.value;
      return {
        x: grid_x,
        y: grid_y
      };
    },
    /**
     * Quantize pair of x,y coordinates to the mesh defined by this model
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {object} coord Coordinates hash representing x and y coords
     * @return Quantized x,y coordinates
     */
    quantize_coord: function(coord) {
      var mesh = this.get_mesh();
      var quantize_value = function(value, resolution) {
        return Math.round(value / resolution) * resolution;
      };
      return {
        x: quantize_value(coord.x, mesh.x),
        y: quantize_value(coord.y, mesh.y)
      };
    },
    /**
     * Quantize the supplied coordinates series to the mesh defined by this
     * model.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {array} coords Array of coordinates where coords.x and coords.y
     *                       are integers
     * @return {array} Series of quantized x,y coordinates
     */
    quantize: function(coords) {
      return _.map(coords, this.quantize_coord);
    },
    /**
     * Add questa wrapping sections to coords.
     *
     * The rules (as defined by the QUESTA source):
     * - total x = coords.x * 1.2 <-- added either side
     * - total y = coords.y * 1.35 <-- added to bottom
     * - add sloping upslope section
     *  - x = 0.1 * total x
     *  - y = calc from angle of 1st section
     * - add flat downslope section
     *  - x = 0.1 * total x
     *  - y = 0
     * - add vertical downslope section
     *  - x = 0
     *  - y = 0.35 * total y
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @private
     * @param {array} coords Array of coordinates where coords.x and coords.y
     *                       are integers
     * @param {array} slope_sections Array of slope surface section definitions
     * @return Array of coordinates including the newly generated sections
     */
    questify_coords: function(coords, slope_sections) {
      var mesh = this.get_mesh();
      var mesh_length = _.max(coords, function(coord) {
        return coord.x;
      }).x * 1.2;
      var number_of_columns = Math.floor(mesh_length / mesh.x) + 1;
      mesh_length = number_of_columns * mesh.x;

      var mesh_height = _.max(coords, function(coord) {
        return coord.y;
      }).y * 1.35;
      var number_of_cells = Math.floor(mesh_height / mesh.y) + 1;
      mesh_height = number_of_cells * mesh.y;
      var x_extension = Math.floor(mesh_length * 0.1);
      var y_extension = Math.floor(mesh_height * 0.35);
      var transformed_coords = _.map(coords, function (coord) {
        return {
          x: coord.x + x_extension,
          y: coord.y + y_extension
        };
      });
      var angle = slope_sections[0].angle.value;
      var top_coords = {
        x: 0,
        y: transformed_coords[0].y +
            x_extension * Math.tan(Mossaic.degs_to_rads * angle)
      };
      var bottom_coords = [{
          x: _.last(transformed_coords).x + x_extension,
          y: _.last(transformed_coords).y
        },
        {
          x: _.last(transformed_coords).x + x_extension,
          y: 0
        }
      ];
      var new_coords = [top_coords].concat(transformed_coords, bottom_coords);
      return new_coords;
    },
    /**
     * Convenience function to get water depths from the model as an array of
     * objects with depth and distance fields.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @return Array of water depths objects with depth and distance fields
     */
    get_water_depths: function() {
      var is_stochastic = this.is_stochastic("water_table");
      var water_table = this.get("geometry").water_table;
      return _.map(water_table, function(section) {
        if (is_stochastic) {
          return {
            depth: section.depth.mean,
            distance: section.surface_distance_from_cut_toe.value
          }
        } else {
          return {
            depth: section.depth.value,
            distance: section.surface_distance_from_cut_toe.value
          }
        }
      });
    },
    /**
     * Convenience function to get estimates for each soil strata as an array
     * of objects with fields depth, distance and angle.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @return Array of soil depth estimates
     */
    get_soil_estimates: function() {
      var soil_strata = this.get("geometry").soil_strata;
      var surface_distance = (soil_strata &&
        soil_strata.surface_distance_from_cut_toe &&
        soil_strata.surface_distance_from_cut_toe.value) || 0;
      var depths = (soil_strata && soil_strata.interfaces &&
        soil_strata.interfaces.depth &&
        (soil_strata.interfaces.depth.value ||
          soil_strata.interfaces.depth.mean)) || [];
      var angles = (soil_strata && soil_strata.interfaces &&
        soil_strata.interfaces.angle &&
        (soil_strata.interfaces.angle.value ||
          soil_strata.interfaces.angle.mean)) || [];
      return _.map(depths, function(depth, i) {
        return {
          depth: depth,
          angle: angles[i],
          distance: surface_distance
        };
      });
    },
    /**
     * Convenience function to get soil depths from the model as an array of
     * depth measurement arrays for each strata interface.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @return Array of arrays containing all soil measurements for each strata
     *         interface.
     */
    get_soil_depths: function() {
      var is_stochastic = this.is_stochastic("soil_strata");
      var soil_strata = this.get("geometry").soil_strata;
      var soil_depths_by_slope_section = _.map(soil_strata, function(section) {
        if (is_stochastic) {
          return {
            depth: section.depth.mean,
            distance: section.surface_distance_from_cut_toe.value
          };
        } else {
          return {
            depth: section.depth.value,
            distance: section.surface_distance_from_cut_toe.value
          };
        }
      });
      var total_strata = this.get_total_strata(soil_depths_by_slope_section);
      var soil_depths_by_strata = [];
      for (var strata = 0; strata < total_strata; ++strata) {
        soil_depths_by_strata.push(
          _.map(soil_depths_by_slope_section, function(depths) {
            if (typeof(depths) !== "undefined") {
              return {
                depth: depths.depth[strata],
                distance: depths.distance
              };
            } else {
              return 0;
            }
          })
        );
      }
      return soil_depths_by_strata;
    },
    /**
     * Convenience function to get soil depths from the model as an array of
     * depth measurement arrays for each slope section.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {object} options Hash of options
     * @param {array} soil_depths Array of arrays containing all soil
                                  measurements for each strata interface
     * @return Array of arrays containing all soil strata measurements for each
     *         slope section.
     */
    get_soil_depths_by_section: function(options) {
      var options = options || {};
      var soil_depths = options.soil_depths ||
          this.is_soil_known() && this.get_soil_depths() ||
          this.get_soil_estimates();
      if (typeof(soil_depths[0]) !== "undefined") {
        var total_sections = soil_depths[0].length;
        var soil_depths_by_section = [];
        for (var section = 0; section < total_sections; ++section) {
          soil_depths_by_section.push(_.map(soil_depths, function(strata) {
            if (typeof(strata) !== "undefined") {
              return strata[section];
            } else {
              return 0;
            }
          }));
        }
        return soil_depths_by_section;
      } else {
        return [];
      }
    },
    /**
     * Convenience function to get the total number of soil strata in an array
     * of depth measurements.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {array} depths Array of soil depth measurements
     * @return Number of soil strata interfaces in the supplied array
     */
    get_total_strata: function(depths) {
      var soil_depths = depths || this.get_soil_depths();
      return _.max(_.map(soil_depths, function(soil_depth) {
        if (typeof(soil_depth) !== "undefined" &&
            typeof(soil_depth.depth) !== "undefined") {
          return soil_depth.depth.length;
        }
      }));
    },
    /**
     * Find the point on the slope surface corresponding to a given x coordinate
     * and return the distance of that point along the surface from the left
     * most point on the surface.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {float} x The x coordinate of the surface point from which the
     *                  distance will be computed
     * @param {array} coords Array of surface coordinates
     * @return {float} Surface distance from left most point of surface point
     *                 corresponding to given x coordinate
     */
    compute_surface_distance_from_x: function(x, coords) {
      var that = this;
      var geometry = _.clone(this.get("geometry"));
      var get_section_containing_depth = function(x) {
        var slope_sections = geometry.slope_sections;
        var total_surface_distance = 0;
        var total_x = 0;
        for (var index = 0; index < slope_sections.length; ++index) {
          var new_x = Math.cos(Mossaic.degs_to_rads * slope_sections[index].angle.value) * slope_sections[index].surface_length.value;
          var new_total_x = total_x + new_x;
          var new_total_surface_distance = total_surface_distance + slope_sections[index].surface_length.value;
          if (new_total_x > x) {
            return {
              index: index,
              distance: total_surface_distance,
              distance_x: total_x
            };
          }
          total_surface_distance = new_total_surface_distance;
          total_x = new_total_x;
        }
        // The surface distance takes us past the last slope section so return
        // slope_sections.length
        return {
          index: slope_sections.length,
          distance: total_surface_distance,
          distance_x: total_x
        };
      }
      var section_of_interest = get_section_containing_depth(x);
      var difference = x - section_of_interest.distance_x;
      var x = 0;
      var y = 0;
      var i = section_of_interest.index;
      if (i >= geometry.slope_sections.length) {
        --i;
        difference = 0;
      }
      var i_c = this.section_index_to_coords_index(i);
      var angle = 90;
      if (i >= 0 &&
          i < geometry.slope_sections.length) {
        angle = 90 -
            geometry.slope_sections[i].angle.value;
        if (typeof(coords[i_c + 1]) !== "undefined"  && (coords[i_c + 1].x < coords[i_c].x)) {
          angle = -angle;
        }
      }
      var section_x = Math.sin(Mossaic.degs_to_rads * angle) * geometry.slope_sections[i].surface_length.value;
      var x_d = section_x - difference;
      var distance_d = x_d / Math.sin(Mossaic.degs_to_rads * angle);
      var total_distance = section_of_interest.distance + geometry.slope_sections[i].surface_length.value - distance_d;
      return total_distance;
    },
    /**
     * Compute start and end coordinates of a soil depth estimate.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {object} estimate Hash with an angle property, representing the
     *                          angle of the estimate line in degrees
     * @param {object} origin Coordinates of the depth estimate
     * @param {array} coords Coordinates of the slope surface
     * @return {object} Hash containing start and end coordinates
     */
    compute_estimate_coords: function(estimate, origin, coords) {
      var start = {x: coords[0].x};
      var end = {x: _.last(coords).x};
      var start_y_tmp = (origin.x - start.x);
      var end_y_tmp = end.x - origin.x;
      var tan_angle = Math.tan(Mossaic.degs_to_rads * estimate.angle);
      if (estimate.angle % 90 === 0 && estimate.angle % 180 !== 0) {
      start.y = 0;
      end.y = _.max(coords, function(coord) {
        return coord.y;
      }).y;
        start.x = origin.x;
        end.x = origin.x;
      } else {
        start.y = tan_angle * start_y_tmp + origin.y;
        end.y = origin.y - (tan_angle * end_y_tmp);
      }
      return {
        start: start,
        end: end
      }
    },
    /**
     * Compute the coordinates of the depth line, projecting to the beginning
     * or end of the slope if needs be.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {object} depth_x_y_prev Coordinates of the start of the line
     * @param {object} depth_x_y Coordinates of the end of the line
     * @param {integer} index Index of the depth measurement at the end of the
     *                        section
     * @return {array} Start and end coordinates of the line
     */
    compute_line_coords: function(depth_x_y_prev, depth_x_y, index, coords, depths) {
      /**
       * Project the depth line to the x co-ordinate specified by p_x.
       * The projection has the same gradient as the original line.
       */
      var project_line = function(x1, x2, y1, y2, p_x, end) {
        var adj_old = x2 - x1;
        if (end) {
          var adj_new = p_x - x1;
        } else {
          var adj_new = x2 - p_x;
        }
        var opp_old = y1 - y2;
        var angle = Math.atan(opp_old / adj_old);
        var opp_new = adj_new * Math.tan(angle);
        return {x: p_x, y: y2 + opp_new};
      };
      var x, d_x, y, d_y;
      if (index == 1 && depth_x_y_prev.x >= coords[0].x &&
          depth_x_y_prev.x < depth_x_y.x) {
        var projected = project_line(depth_x_y_prev.x, depth_x_y.x,
            depth_x_y_prev.y, depth_x_y.y, coords[0].x);
        x = projected.x;
        y = projected.y;
      } else {
        x = depth_x_y_prev.x;
        y = depth_x_y_prev.y;
      }
      if (index == depths.length - 1 &&
          depth_x_y.x <= coords[coords.length - 1].x &&
          depth_x_y_prev.x < depth_x_y.x) {
        var max_x = _.max(coords, function(coord) { return coord.x }).x;
        var projected = project_line(depth_x_y_prev.x, depth_x_y.x,
          depth_x_y.y, depth_x_y_prev.y, max_x, true);
        d_x = projected.x;
        d_y = projected.y;
      } else {
        d_x = depth_x_y.x;
        d_y = depth_x_y.y;
      }
      if (isNaN(y)) { y = 0; }
      if (isNaN(d_y)) { d_y = 0; }
      return [{x: x, y: y}, {x: d_x, y: d_y}];
    },
    /**
     * Compute the x and y coordinates of a given depth measurement. This
     * is generated by finding the x coordinate of the point on the surface that
     * the depth is specified (given by surface_distance_from_cut_toe) and
     * finding the y coordinate of the depth at that point.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {object} depth Depth measurement with depth and distance fields
     * @param {array} coords Array of slope coordinates in model space
     * @param {object} options Hash of options
     * @param {boolean} options.quantize Quantize coordinates to the mesh
     *                                   defined in the model
     * @param {object} options.origin The origin (datum) from which the depth
     *                                surface distances will be calculated
     *
     */
    compute_depth_x_y: function(depth, coords, options) {
      // Starting from datum, work back (or forwards if surface_distance_from_cut_toe is positive)
      // until total length > surface_distance_from_cut_toe
      // This gives us i, the coord to the left of the depth and d_i, the coord to the right
      // Calculate difference between total distance at d_i and surface_distance_from_cut_toe
      // Get the angle from the slope sections
      // Use that angle and the new length to find the x distance
      // Subtract that x distance from d_i.x
      var that = this;
      var options = options || {};
      var quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        quantize = options.quantize;
      }
      var geometry = _.clone(this.get("geometry"));
      var road = geometry.road;
      var road_section, datum;
      if (typeof(road) !== "undefined" &&
          typeof(road.slope_section) !== "undefined") {
        road_section = road.slope_section;
      } else if (typeof(geometry.slope_sections) !== "undefined") {
        if (this.mode === "questa") {
          road_section = 0; // Default datum is left most point of first section
          geometry.road = {slope_section: road_section};
          this.changed_by = this;
          this.set({geometry: geometry}); // Update the model
        } else if (this.mode === "chasm") {
          if (typeof(options.origin) !== "undefined") {
            datum = options.origin;
          } else if (typeof(geometry.datum.index) !== "undefined") {
            road_section = geometry.datum.index;
          } else {
            datum = geometry.datum.value;
          }
        }
      }
      /**
       * Returns the index and surface distance of the section that contains a
       * particular depth measurement. It is possible for a measurement to lie
       * outside the range of defined sections. In this case index will be
       * either -1, if the measurement is before section 0, or
       * slope_sections.length, if the measurement is after the last section.
       *
       * Can be called specifying either road_section (index of surface section
       * where left-most point is the datum) or datum (distance from left most
       * point of the slope surface of the datum). If both are specified, datum
       * takes precedence.
       *
       * Calling code should be aware of the potential for sections that don't
       * exist and handle accordingly.
       *
       * @inner
       * @private
       * @param {float} surface_distance Distance along the surface from the
       *                                 datum of the depth measurement
       * @param {object} options Hash of options
       * @param {float} options.datum Distance of datum from left most point on
       *                              slope surface
       * @param {integer} options.road_section Index of slope surface section
       *                                       to be used as the datum
       * @return {index, distance} Index of section and distance of section
       *                           start from the datum.
       */
      var get_section_containing_depth = function(surface_distance, options) {
        var options = options || {};
        var road_section = options.road_section;
        var datum = options.datum;
        var slope_sections = geometry.slope_sections;
        var total_surface_distance = 0;
        var datum_section;
        var offset = 0;
        if (typeof(datum) !== "undefined") {
          if (datum !== 0) {
            var section = get_section_containing_depth(options.datum, {
              datum: 0
            });
            datum_section = section.index;
            offset = datum - section.distance;
          } else {
            datum_section = 0;
            offset = 0;
          }
        } else {
          datum_section = road_section;
        }
        if (surface_distance < 0) {
          total_surface_distance = offset;
          for (var index = datum_section - 1; index >= 0; --index) {
            var new_total_surface_distance = total_surface_distance + slope_sections[index].surface_length.value;
            if (new_total_surface_distance > Math.abs(surface_distance)) {
              return {
                index: index,
                distance: total_surface_distance
              };
            }
            total_surface_distance = new_total_surface_distance;
          }
          // The surface distance takes us past the first slope section so
          // return index -1
          return {
            index: -1,
            distance: total_surface_distance
          };
        } else {
          total_surface_distance = -offset;
          for (var index = datum_section; index < slope_sections.length; ++index) {
            var new_total_surface_distance = total_surface_distance + slope_sections[index].surface_length.value;
            if (new_total_surface_distance > surface_distance) {
              return {
                index: index,
                distance: total_surface_distance
              };
            }
            total_surface_distance = new_total_surface_distance;
          }
          // The surface distance takes us past the last slope section so return
          // slope_sections.length
          return {
            index: slope_sections.length,
            distance: total_surface_distance
          };
        }
      }
      var section_of_interest = get_section_containing_depth(depth.distance, {
        road_section: road_section,
        datum: datum
      });
      var difference = Math.abs(depth.distance) - section_of_interest.distance;
      var x = 0;
      var y = 0;
      var i = section_of_interest.index;
      var i_c = this.section_index_to_coords_index(section_of_interest.index);
      if (depth.distance < 0) {
        var angle = 0;
        if (i >= 0 &&
            i < geometry.slope_sections.length) {
          angle = geometry.slope_sections[i].angle.value;
        } else if (i == -1) {
          angle = geometry.slope_sections[0].angle.value;
        }
        if (typeof(coords[i_c + 1]) !== "undefined" &&
            typeof(coords[i_c]) !== "undefined" &&
            (coords[i_c + 1].x < coords[i_c].x)) {
          angle = 180 - angle;
        }
        var x_offset = Math.cos(Mossaic.degs_to_rads * angle) * difference;
        var y_offset = Math.sin(Mossaic.degs_to_rads * angle) * difference;
        x = coords[i_c + 1].x - x_offset;
        y = coords[i_c + 1].y + y_offset - depth.depth;
      } else {
        var angle = 90;
        if (i >= 0 &&
            i < geometry.slope_sections.length) {
          angle = 90 -
              geometry.slope_sections[i].angle.value;
          if (typeof(coords[i_c + 1]) !== "undefined"  &&
              (coords[i_c + 1].x < coords[i_c].x)) {
            angle = -angle;
          }
        }
        var x_offset = Math.sin(Mossaic.degs_to_rads * angle) * difference;
        var y_offset = Math.cos(Mossaic.degs_to_rads * angle) * difference;
        x = coords[i_c].x + x_offset;
        y = coords[i_c].y - y_offset - depth.depth;
      }
      if (quantize) {
        return this.quantize_coord({x: x, y: y});
      } else {
        return {x: x, y: y};
      }
    },
    /**
     * Determine the coordinates in model space from the defined slope profile
     * sections.
     *
     * Note: The default behaviour is to quantize these coordinates to the mesh
     * that this model defines. Pass in options.quantize = false to get the
     * raw coordinates.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {object} options Hash of options
     * @param {array} options.old_coords Array of old coordinates, used to
     *                                   determine which sections were inverted
     * @param {boolean} options.quantize Quantize coordinates to the defined
     *                                   mesh
     * @return {array} Array of slope surface coordinates
     */
    compute_coords: function(options) {
      var old_coords = options && options.old_coords;
      var quantize = true;
      if (options && typeof(options.quantize) !== "undefined") {
        quantize = options.quantize;
      }
      var slope_sections = this.get("geometry").slope_sections;
      var index_offset = (this.mode === "questa") && 2 || 1;
      var inverted_sections =
          this.get_inverted_sections(old_coords, index_offset);
      var relative_coords = _.map(slope_sections, function(section, i) {
        if (typeof(section.surface_height) !== "undefined" && typeof(section.surface_height.value) !== "undefined" &&
            typeof(section.surface_length) !== "undefined" && typeof(section.surface_length.value) !== "undefined") {
          var dx = Math.sqrt(Math.pow(section.surface_length.value, 2) - Math.pow(section.surface_height.value, 2));
          var dy = section.surface_height.value;
          if (i in inverted_sections) {
            dx = -dx;
          }
          return {dx: dx, dy: dy};
        } else if (typeof(section.surface_height) !== "undefined" && typeof(section.surface_height.value) !== "undefined" &&
            typeof(section.angle) !== "undefined" && typeof(section.angle.value) !== "undefined") {
          var dx = section.surface_height.value /
                Math.tan(Mossaic.degs_to_rads * section.angle.value);
          var dy = section.surface_height.value;
          if (i in inverted_sections) {
            dx = -dx;
          }
          return {dx: dx, dy: dy};
        } else if (typeof(section.surface_length) !== "undefined" && typeof(section.surface_length.value) !== "undefined" &&
            typeof(section.angle) !== "undefined" && typeof(section.angle.value) !== "undefined") {
          var dx = section.surface_length.value *
                Math.cos(Mossaic.degs_to_rads * section.angle.value);
          var dy = section.surface_length.value *
                Math.sin(Mossaic.degs_to_rads * section.angle.value);
          if (i in inverted_sections) {
            dx = -dx;
          }
          return {dx: dx, dy: dy};
        } else {
          console.log(["Can't deal with section", section]);
        }
      });
      var absolute_coords = _.reduce(relative_coords, function(acc, coords) {
        if (acc.length == 0) {
          acc.push({x: coords.dx, y: coords.dy});
        } else {
          var last_coords = acc[acc.length - 1];
          acc.push({x: coords.dx + last_coords.x,
            y: last_coords.y - coords.dy});
        }
        return acc;
      }, [{x: 0, y: 0}]);
      var min_y = _.min(absolute_coords, function(coords) {
        return coords.y;
      }).y;
      var fixed_absolute_coords = _.map(absolute_coords, function(coords) {
        return {x: coords.x, y: coords.y - min_y};
      });
      if (this.mode === "questa" &&
          slope_sections && slope_sections.length > 0) {
        var final_coords = this.questify_coords(fixed_absolute_coords, slope_sections);
      } else {
        var final_coords = fixed_absolute_coords;
      }
      if (quantize) {
        return this.quantize(final_coords);
      } else {
        return final_coords;
      }
    },
    /**
     * Add a slope profile section to the model
     * If index is undefined then the section will be added to the end of
     * the slope sections.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {object} data Hash containing two of length, height, angle
     * @param {int} index Index at which the section is to be added
     */
    add_section: function(data, index) {
      var index = index;
      if (typeof(index) === "undefined") {
        var slope_sections = this.get("geometry").slope_sections;
        if (typeof(slope_sections) === "undefined" || slope_sections.length == 0){
          index = 0;
        } else {
          index = slope_sections.length;
        }
      }
      var geometry = _.clone(this.get("geometry"));
      var slope_sections = _.clone(geometry.slope_sections);
      if (typeof(data.length) === "undefined" || isNaN(data.length)) {
        if (data.angle === 0) {
          throw "Cannot create section with height > 0 and angle == 0";
        } else {
          data.length = data.height / Math.sin(Mossaic.degs_to_rads * data.angle);
        }
      }
      if (typeof(data.height) === "undefined" || isNaN(data.height)) {
        data.height = data.length * Math.sin(Mossaic.degs_to_rads * data.angle);
      }
      if (typeof(data.angle) === "undefined" || isNaN(data.angle)) {
        if (data.height > data.length) {
          throw "Cannot create section where height is greater than surface length";
        } else {
          if (data.length === 0) {
            data.angle = 90;
          } else {
            data.angle = Math.asin(data.height / data.length) / Mossaic.degs_to_rads;
          }
        }
      }
      var new_section = {
        surface_length: {
          unit: "m",
          value: data.length
        },
        angle: {
          unit: "deg",
          value: data.angle
        },
        surface_height: {
          unit: "m",
          value: data.height
        }
      };
      geometry.slope_sections = slope_sections.concat([new_section]);
      this.set({geometry: geometry});
    },
    /**
     * Remove slope profile section at index
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {int} index Index of section to be removed
     */
    remove_section: function(index) {
      var geometry = _.clone(this.get("geometry"));
      var slope_sections = _.clone(geometry.slope_sections);
      var road = _.clone(geometry.road);
      if (index <= road.slope_section && road.slope_section > 0) {
        road.slope_section -= 1;
        geometry.road = road;
      }
      slope_sections.splice(index, 1);
      geometry.slope_sections = slope_sections;
      this.set({geometry: geometry});
    },
    /**
     * Set a soil strata depth estimate. Can be used to set either the surface
     * distance or the angle or depth of the estimated depth.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {object} options Hash of options
     * @param {float} options.surface_distance_from_cut_toe Distance of depth
     *                                                      estimate from datum
     * @param {string} options.field The soil estimate field to be set, which
     *                               can be either
     *                               "surface_distance_from_cut_toe" or "depth"
     * @param {integer} options.index The index of the strata that should be set
     * @param {float} options.value The value to be set
     */
    set_soil_estimate: function(options) {
      var is_stochastic = this.is_stochastic("soil_strata");
      var geometry = _.clone(this.get("geometry"));
      var soil_strata = _.clone(geometry.soil_strata) || {};
      if (typeof(options.surface_distance_from_cut_toe) !== "undefined") {
        if ("surface_distance_from_cut_toe" in soil_strata) {
          var surface_distance = _.clone(
            soil_strata.surface_distance_from_cut_toe);
        } else {
          var surface_distance = {unit: "m"};
        }
        surface_distance.value = options.surface_distance_from_cut_toe;
        soil_strata.surface_distance_from_cut_toe = surface_distance;
      } else if (typeof(options.field) !== "undefined" &&
          typeof(options.value) !== "undefined" &&
          typeof(options.index) !== "undefined") {
        var interfaces = _.clone(soil_strata.interfaces);
        var field = _.clone(interfaces[options.field]);
        if (is_stochastic) {
          var mean = _.clone(field.mean);
          var standard_deviation = _.clone(field.standard_deviation);
          mean[options.index] = options.value.mean;
          standard_deviation[options.index] = options.value.standard_deviation;
          field.mean = mean;
          field.standard_deviation = standard_deviation;
        } else {
          var values = _.clone(field.value);
          values[options.index] = options.value.value;
          field.value = values;
        }
        interfaces[options.field] = field;
        soil_strata.interfaces = interfaces;
      }
      geometry.soil_strata = soil_strata;
      this.changed_by = options.changed_by;
      this.set({geometry: geometry});
    },
    /**
     * Set water table depth measurement
     *
     * Leaving index undefined will cause a new depth to be added to the end
     * of the array.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {float} depth Depth value
     * @param {object} options Hash of options may define
     *                         surface_distance_from_cut_toe, and index of
     *                         measurement
     */
    set_water_depth: function(_depth, options) {
      var surface_distance_from_cut_toe = options.surface_distance_from_cut_toe;
      var index = options.index;
      var depth, depth_sd;
      if (typeof(_depth) === "number" || typeof(_depth) === "undefined") {
        depth = _depth;
      } else if ("value" in _depth) {
        depth = _depth.value;
      } else if ("mean" in _depth) {
        depth = _depth.mean;
      }
      if (typeof(_depth) === "object") {
        depth_sd = _depth.standard_deviation;
      } else {
        depth_sd = 0;
      }
      var is_stochastic = this.is_stochastic("water_table");
      var changed_by = options.changed_by;
      var blank_depth = {unit: "m"};
      var blank_surface_distance = {unit: "m"};
      var geometry = _.clone(this.get("geometry"));
      var water_table = _.clone(geometry.water_table);
      if (typeof(water_table) === "undefined") {
        water_table = [];
      }
      if (typeof(index) !== "undefined") {
        if (typeof(water_table[index]) === "undefined") {
          water_table[index] = {
            surface_distance_from_cut_toe: blank_surface_distance,
            depth: blank_depth
          }
          water_table[index].surface_distance_from_cut_toe.value =
              surface_distance_from_cut_toe;
          if (is_stochastic) {
            water_table[index].depth.mean = depth;
            water_table[index].depth.standard_deviation = depth_sd;
          } else {
            water_table[index].depth.value = depth;
          }
        } else {
          if (typeof(depth) !== "undefined") {
            var modified_measurement = _.clone(water_table[index]);
            modified_measurement.depth = _.clone(modified_measurement.depth);
            if (is_stochastic) {
              modified_measurement.depth.mean = depth;
              modified_measurement.depth.standard_deviation = depth_sd;
            } else {
              modified_measurement.depth.value = depth;
            }
            water_table[index] = modified_measurement;
          }
          if (typeof(surface_distance_from_cut_toe) !== "undefined") {
            var modified_measurement = _.clone(water_table[index]);
            modified_measurement.surface_distance_from_cut_toe = _.clone(
                modified_measurement.surface_distance_from_cut_toe);
            modified_measurement.surface_distance_from_cut_toe.value =
                                                  surface_distance_from_cut_toe;
            water_table[index] = modified_measurement;
          }
        }
      } else {
        if (is_stochastic) {
          blank_depth.mean = depth;
          blank_depth.standard_deviation = depth_sd;
        } else {
          blank_depth.value = depth;
        }
        blank_surface_distance.value = surface_distance_from_cut_toe;
        water_table.push({
          surface_distance_from_cut_toe: blank_surface_distance,
          depth: blank_depth
        });
      }
      geometry.water_table = water_table;
      this.changed_by = changed_by;
      this.set({geometry: geometry});
    },
    /**
     * Set soil strata table depth measurement
     *
     * Leaving index undefined will cause a new depth to be added to the end
     * of the array.
     *
     * Depth can be an array, in which case it will be treated as an array of
     * measurements for all soil strata interfaces at that index.
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {float} depth Depth value
     * @param {int} soil_strata_index Index of strata interface
     * @param {object} options Hash of options may define
     *                         surface_distance_from_cut_toe, and index of
     *                         measurement
     */
    set_soil_depth: function(_depth, soil_strata_index, options) {
      var depth, depth_sd;
      if (typeof(_depth) === "number" ||
          (typeof(_depth) === "object" && _depth.hasOwnProperty("length")) ||
          (typeof(_depth) === "undefined")) {
        depth = _depth;
      } else if ("value" in _depth) {
        depth = _depth.value;
      } else if ("mean" in _depth) {
        depth = _depth.mean;
      }
      if (typeof(_depth) === "object") {
        depth_sd = _depth.standard_deviation;
      }
      var is_stochastic = this.is_stochastic("soil_strata");
      var soil_strata_index = soil_strata_index;
      var surface_distance_from_cut_toe = options.surface_distance_from_cut_toe;
      var index = options.index;
      var changed_by = options.changed_by;
      var geometry = _.clone(this.get("geometry"));
      var soil_strata = _.clone(geometry.soil_strata);
      if (typeof(soil_strata) === "undefined") {
        soil_strata = [];
      }
      if (typeof(index) !== "undefined") {
        if (typeof(soil_strata[index]) === "undefined") {
          soil_strata[index] = {
            surface_distance_from_cut_toe: {
              unit: "m",
              value: surface_distance_from_cut_toe
            },
            depth: {
              unit: "m"
            }
          }
          var new_depth;
          if (depth.hasOwnProperty("length")) {
            new_depth = depth;
          } else {
            new_depth = [depth];soil_strata[index].depth.value = [depth];
          }
          if (is_stochastic) {
            soil_strata[index].depth.mean = new_depth;
            if (typeof(depth_sd) !== "undefined") {
              soil_strata[index].depth.standard_deviation = depth_sd;
            }
          } else {
            soil_strata[index].depth.value = new_depth;
          }
        } else {
          if (typeof(depth) !== "undefined") {
            var modified_measurement = _.clone(soil_strata[index]);
            modified_measurement.depth = _.clone(modified_measurement.depth);
            if (depth.hasOwnProperty("length")) {
              if (is_stochastic) {
                modified_measurement.depth.mean = depth;
                if (typeof(depth_sd) !== "undefined") {
                  soil_strata[index].depth.standard_deviation = depth_sd;
                }
              } else {
                modified_measurement.depth.value = depth;
              }
              soil_strata[index] = modified_measurement;
            } else {
              if (typeof(soil_strata_index) === "undefined") {
                console.log("Cannot set depth with undefined strata index");
              } else {
                if (is_stochastic) {
                  modified_measurement.depth.mean = _.clone(
                      modified_measurement.depth.mean);
                  modified_measurement.depth.mean[soil_strata_index] = depth;
                  if (typeof(depth_sd) !== "undefined") {
                    modified_measurement.depth.standard_deviation = _.clone(
                        modified_measurement.depth.standard_deviation);
                    modified_measurement.
                        depth.standard_deviation[soil_strata_index] = depth_sd;
                  }
                } else {
                  modified_measurement.depth.value = _.clone(
                      modified_measurement.depth.value);
                  modified_measurement.depth.value[soil_strata_index] = depth;
                }
                soil_strata[index] = modified_measurement;
              }
            }
          }
          if (typeof(surface_distance_from_cut_toe) !== "undefined") {
            var modified_measurement = _.clone(soil_strata[index]);
            modified_measurement.surface_distance_from_cut_toe =
                _.clone(modified_measurement.surface_distance_from_cut_toe);
            modified_measurement.surface_distance_from_cut_toe.value =
                surface_distance_from_cut_toe;
            soil_strata[index] = modified_measurement;
          }
        }
      } else {
        var new_depth;
        if (typeof(depth) !== "undefined") {
          new_depth = {
            unit: "m"
          };
          if (is_stochastic) {
            new_depth.mean = [];
            new_depth.standard_deviation = [];
          } else {
            new_depth.value = [];
          }
        }
        if (depth.hasOwnProperty("length")) {
          if (is_stochastic) {
            new_depth.mean = depth;
            new_depth.standard_deviation = depth_sd;
          } else {
            new_depth.value = depth;
          }
        } else {
          if (is_stochastic) {
            new_depth.mean[soil_strata_index] = depth;
            new_depth.standard_deviation[soil_strata_index] = depth_sd;
          } else {
            new_depth.value[soil_strata_index] = depth;
          }
        }
        soil_strata.push({
          surface_distance_from_cut_toe: {
            unit: "m",
            value: surface_distance_from_cut_toe
          },
          depth: new_depth
        });
      }
      geometry.soil_strata = soil_strata;
      this.changed_by = changed_by;
      this.set({geometry: geometry});
    },
    /**
     * Return the number of surface slope sections in the model
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @return Number of slope profile sections in this model
     */
    get_number_of_sections: function() {
      return this.get("geometry").slope_sections.length;
    },
    /**
     * Determine whether a point is within the slope profile
     * Assumes a valid questa slope (no overhangs)
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {object} point Hash of point x and y values
     * @return true if supplied point is in slope profile
     */
    is_point_in_slope: function(point) {
      var coords = this.compute_coords();
      var section = _.filter(coords, function(coord, i) {
        var next_section = (i < coords.length - 1) && i + 1;
        var point_is_after = (next_section !== false) && (point.x > coord.x &&
            point.x <= coords[next_section].x);
        var previous_section = (i > 0) && i - 1;
        var point_is_before = (previous_section !== false) && (point.x <= coord.x &&
            point.x > coords[previous_section].x);
        return point_is_after || point_is_before;
      });
      if (section.length != 2) {
        console.log("Couldn't determine correct section for this point",
            coords, section, point);
        return false;
      }
      // Simple cases
      var max_y = _.max(section, function(coords) { return coords.y; });
      if (point.y > max_y) {
        return false;
      }
      var min_y = _.min(section, function(coords) { return coords.y; });
      if (point.y < min_y) {
        return true;
      }
      // Hard case
      var adj = section[1].x - section[0].x;
      var opp = section[0].y - section[1].y;  // Could be negative
      var angle = Math.atan(opp / adj);
      var new_adj = section[1].x - point.x;
      var new_opp = new_adj * Math.tan(angle);
      return point.y < section[1].y + new_opp;
    },
    /**
     * Determine whether the model has any stochastic data
     *
     * @memberof Mossaic.models.Geometry
     * @instance
     * @param {string} field Model field to be checked, either "soil_strata" or
     *                       "water table". If left undefined then both fields
     *                       will be checked.
     * @return true if any of the fields in the model are stochastic
     */
    is_stochastic: function(field) {
      var strata_to_check;
      var is_estimated_strata_stochastic = function(strata) {
        var to_check = [];
        if (typeof(strata) !== "undefined" &&
            typeof(strata.interfaces) !== "undefined") {
          to_check = [strata.surface_distance_from_cut_toe,
              strata.interfaces.depth, strata.interfaces.angle];
        }
        return _.any(to_check, function(item) {
          return ("mean" in item || "standard_deviation" in item);
        });
      };
      var is_strata_stochastic = function(strata) {
        if (typeof(strata.length) === "undefined") {
          return is_estimated_strata_stochastic(strata);
        } else {
          var to_check = ["depth"];
          return _.any(strata, function(point) {
            return _.any(to_check, function(field) {
              return (typeof(point[field]) === "object") &&
                  ("mean" in point[field] ||
                    "standard_deviation" in point[field]);
            });
          });
        }
      };
      var geometry = this.get("geometry");
      if (typeof(field) !== "undefined") {
        strata_to_check = [geometry[field]];
      } else {
        strata_to_check = [geometry.soil_strata, geometry.water_table];
      }
      // If all strata are empty, return the default
      if (_.all(strata_to_check, function(strata) {
            return (typeof(strata) === "undefined" ||
                ("length" in strata && strata.length === 0));
        }))
          {
        if (typeof(this.default_stochastic) !== "undefined") {
          return this.default_stochastic;
        }
      }
      return _.any(strata_to_check, function(strata) {
        if (typeof(strata) === "undefined") {
          return false;
        } else {
          return is_strata_stochastic(strata);
        }
      });
    }
  });

  /**
   * Backbone Model for storing chasm/questa stability data.
   *
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.Stability = Mossaic.models.Basic.extend({
    /**
     * Initialize the model.
     * Called implicitly with "new Mossaic.models.Stability".
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} attributes Hash of attributes to be stored in the model
     * @param {object} options Hash of model options
     * @param {Mossaic.models.Geometry} options.geometry Reference to the
     *                                                   Mossaic.models.Geometry
     *                                                   which this stability
     *                                                   data is for
     */
    initialize: function(attributes, options) {
      var options = options || {};
      Mossaic.models.Basic.prototype.initialize.apply(this,
          [attributes, options]);
      _.bindAll(this);
      /**
       * The Mossaic.models.Geometry model that the stability model is for.
       * This reference is used when saving the model, so that the slope
       * coordinates can be written into the stability model (the slope
       * coordinates are required by CHASM).
       * @type Mossaic.models.Geometry
       */
      this.geometry = options.geometry;
      /**
       * Local copy of the attributes for auto-setting the stability
       * @type object
       */
      this.attributes_stash = attributes;
      if (typeof(this.geometry) !== "undefined") {
        this.auto_set_values(attributes);
      }
    },
    /**
     * True if the model is editable. Views check this flag before allowing
     * users to modify, however it is not enforced in the model.
     * @memberof Mossaic.models.Stability
     * @instance
     * @type boolean
     */
    editable: true,
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @type object
     * @property {string} type The type of the model, in this case "stability"
     * @property {string} stability_analysis_algorithm The algorithm that can
     *                                                 be used with this
     *                                                 stability model for
     *                                                 carrying out a slip
     *                                                 search
     * @property {object} grid_search_parameters An empty hash where the
     *                                           grid search parameters can be
     *                                           stored
     */
    defaults: {
      type: "stability",
      stability_analysis_algorithm: "Bishop",
      grid_search_parameters: {}
    },
    /**
     * Retrieve the current set of slope coordinates from the geometry model
     * and fall through to the standard save method.
     *
     * All stability models must contain the slope coordinates for their
     * corresponding geometry model.
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} attrs Hash of attributes to be updated before saving
     * @param {object} options Hash of model options
     */
    save: function(attributes, options) {
      var coords = this.geometry.compute_coords({quantize: false});
      this.set("slope_surface_coordinates", {
        x: _.map(coords, function(coord) { return coord.x; }),
        y: _.map(coords, function(coord) { return coord.y; })
      }, {silent: true});
      Mossaic.models.Basic.prototype.save.apply(this, [attributes, options]);
    },
    /**
     * Retrieve model fields, but silently retrieve certain keys from the
     * grid_search_parameters model attribute. This is a convenience method for
     * calling code.
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {string} key Key of the value to be retrieved
     * @return {object} Value corresponding to the supplied key
     */
    get: function(key) {
      if (key in {"origin": null, "spacing": null,
          "grid_size": null, "radius": null}) {
        return this.get_gsp(key);
      } else {
        return Backbone.Model.prototype.get.apply(this, [key]);
      }
    },
    /**
     * Set a model field.
     *
     * Silently set certain keys on the grid_search_parameters attribute, rather
     * than the top level of the model attributes.
     *
     * Default parameters are (key, value, options) but can also be called
     * with (attributes, options) where attributes is a hash with the key,value
     * to be set.
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {string} key Key of the value to be set
     * @param {object} value Value to be set
     * @param {object} options Hash of options
     */
    set: function(key, value, options) {
      var attrs;
      if (_.isObject(key)) {
        attrs = key;
        options = value;
      } else {
        attrs = {};
        attrs[key] = value;
      }
      for (var attr in attrs) {
        if (attr in
            {"origin": null, "spacing": null,
            "grid_size": null, "radius": null}) {
          this.set_gsp(attr, attrs[attr], options);
        } else {
          Backbone.Model.prototype.set.apply(this, [attr, attrs[attr],options]);
        }
      }
      return this;
    },
    /**
     * Set a field in the grid_search_parameters attribute
     *
     * Default parameters are (key, value, options) but can also be called
     * with (attributes, options) where attributes is a hash with the key,value
     * to be set.
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {string} key Key of the value to be set
     * @param {object} value Value to be set
     * @param {object} options Hash of options
     */
    set_gsp: function(key, value, options) {
       // Set a field in the grid search parameters
      var gsp = _.clone(this.get("grid_search_parameters"));
      gsp[key] = value;
      return Backbone.Model.prototype.set.apply(this,
          [{grid_search_parameters: gsp}, options]);
    },
    /**
     * Get a field in the grid_search_parameters attribute
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {string} key Key of the value to be retrieved
     * @return {object} Value corresponding to the supplied key
     */
    get_gsp: function(key) {
      var gsp = this.get("grid_search_parameters");
      if (typeof(gsp) === "undefined") {
        return;
      } else {
        return _.clone(this.get("grid_search_parameters")[key]);
      }
    },
    /**
     * Set the associated geometry model and auto set stability values
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {Mossaic.models.Geometry} geometry Geometry model from which slope
     *                                           surface coords should be
     *                                           obtained
     */
    set_geometry: function(geometry) {
      this.geometry = geometry;
      this.auto_set_values(this.attributes_stash);
    },
    /**
     * Auto-set stability grid according to questa rules
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} attributes Hash of attributes containing values that
     *                            should not be auto-set
     * @param {object} options Hash of options
     * @param {object} options.mesh Mesh dimensions for autosetting
     */
    auto_set_values: function(attributes, options) {
      var mesh = (options && options.mesh) || {x: 1, y: 1};
      this.changed_by = null;
      var dimensions = this.get_dimensions();
      var attributes = attributes || {};
      if (typeof(attributes.origin) === "undefined") {
        this.set_origin(dimensions, mesh);
      }
      if (typeof(attributes.spacing) === "undefined") {
        this.set_spacing(mesh);
      }
      if (typeof(attributes.grid_size) === "undefined") {
        this.set_grid_size(dimensions);
      }
      if (typeof(attributes.radius) === "undefined") {
        this.set_radius(dimensions);
      }
    },
    /**
     * Determine stability grid dimensions from maximum x and y coordinates of
     * the associated geometry
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @return {object} Height and length of stability grid
     */
    get_dimensions: function() {
      var coords = this.geometry.compute_coords();
      var x_max = _.max(coords, function(coord) {
        return coord.x;
      }).x;
      var y_max = _.max(coords, function(coord) {
        return coord.y;
      }).y;
      return {
        height: y_max,
        length: x_max
      };
    },
    /**
     * Set the grid spacing from the mesh spacing of the associated geometry
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} mesh x and y dimensions of a single mesh cell
     */
    set_spacing: function(mesh) {
      var spacing = _.clone(mesh);
      spacing.unit = "m";
      this.set({
        spacing: spacing
      });
    },
    /**
     * Set the origin according to questa logic
     * This is 2/3 the width, 2/3 the height, plus however many mesh increments
     * we need so that the grid origin is not within the slope profile.
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} dimensions height and length of the stability grid
     * @param {object} mesh x and y dimensions of a single mesh cell
     */
    set_origin: function(dimensions, mesh) {
      var that = this;
      var x_initial = dimensions.length * 2/3;
      var y_initial = dimensions.height * 2/3;
      var initial_point = {
        x: Math.round(x_initial / mesh.x) * mesh.x,
        y: Math.round(y_initial / mesh.y) * mesh.y
      };
      var set_point = function(point) {
        if (typeof(that.geometry) !== "undefined" &&
            that.geometry.is_point_in_slope(point)) {
          point.x = point.x + mesh.x;
          return set_point(point);
        } else {
          return point;
        }
      };
      var point = set_point(initial_point);
      point.unit = "m";
      this.set({origin: point});
    },
    /**
     * Set the size of the stability grid to default size
     * 
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} dimensions height and length of the stability grid
     */
    set_grid_size: function(dimensions) {
      this.set({
        grid_size: {
          unit: "m",
          x: 7,
          y: 7
        }
      });
    },
    /**
     * Set the stability grid radius to default radius
     *
     * @memberof Mossaic.models.Stability
     * @instance
     * @param {object} dimensions height and length of the stability grid
     */
    set_radius: function(dimensions) {
      this.set({
        radius: {
          unit: "m",
          initial: 5,
          increment: 0.5
        }
      })
    }
  });

  /**
   * Backbone Model for representing slope datum metadata
   *
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.Meta = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Meta
     * @instance
     * @type object
     * @property {string} type The type of the model, in this case "meta"
     * @property {object} breadth The breadth of the slope
     * @property {array} road_link Indices of road network nodes either side
     *                             of the datum
     */
    defaults: {
      type: "meta",
      breadth: {
        unit: "m"
      },
      road_link: []
    }
  });

  /**
   * Backbone Model for representing soils data
   *
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.Soils = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Soils
     * @instance
     * @type object
     * @property {string} type The type of the model, in this case "soils"
     */
    defaults: {
      type: "soils"
    },
    /**
     * Determine whether the model has any stochastic data
     *
     * @memberof Mossaic.models.Soils
     * @instance
     * @return true if any of the fields in the model are stochastic
     */
    is_stochastic: function() {
      var to_check = ["Ksat", "saturated_moisture_content",
          "saturated_bulk_density", "unsaturated_bulk_density",
          "effective_angle_of_internal_friction", "effective_cohesion"];
      var soils = this.get("soils");
      return _.any(soils, function(soil) {
        return _.any(to_check, function(field) {
          return ("mean" in soil[field] || "standard_deviation" in soil[field]);
        });
      });
    }
  });

  /**
   * Backbone Model for representing boundary conditions data
   *
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.BoundaryConditions = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     * @type object
     * @property {string} type The type of the model, in this case
     *                         "boundary_conditions"
     * @property {object} upslope_recharge Upslope recharge unit and value
     * @property {object} detention_capacity Detention capacity unit and value
     * @property {object} soil_evaporation Soil evaporation unit and value
     */
    defaults: {
      type: "boundary_conditions",
      upslope_recharge: {
        unit: "mh-1"
      },
      detention_capacity: {
        unit: "m"
      },
      soil_evaporation: {
        unit: "ms-1"
      }
    },
    /**
     * Determine whether the model has any stochastic data
     *
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     * @return true if any of the fields in the model are stochastic
     */
    is_stochastic: function() {
      var detention_capacity = this.get("detention_capacity");
      var soil_evaporation = this.get("soil_evaporation");
      return ("mean" in detention_capacity ||
          "standard_deviation" in detention_capacity ||
          "mean" in soil_evaporation ||
          "standard_deviation" in soil_evaporation);
    },
    /**
     * Determine the duration (in days) of the design storm
     * (or design storm series)
     *
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     * @return Design storm duration
     */
    get_duration: function() {
      var compute_duration = function(storm) {
        var start = storm.start.value;
        var rainfall_hours = storm.rainfall_intensities.value.length;
        return start + rainfall_hours;
      }
      var storm = this.get("storm");
      if (typeof(storm) === "undefined" || storm.length == 0) {
        throw "Cannot return duration because no storms are defined";
      }
      // Get first storm, find length
      var duration = compute_duration(storm[0]);
      // Check all other storms, throw exception if any differ
      var all_storms_equal_duration = _.all(storm, function(instance) {
        return compute_duration(instance) == duration;
      });
      if (!all_storms_equal_duration) {
        throw "Multiple storms are defined with differing total durations";
      } else {
        return duration;
      }
    }
  });

  /**
   * Backbone Model for representing engineering cost data
   *
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.EngineeringCost = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     * @type object
     */
    defaults: {
      type: "engineering_cost",
      equipment: {
        shovel: {
          capacity: {
            unit: "m3",
            value: []
          },
          cost: {
            unit: "EC$h-1",
            value: []
          },
          quantity: []
        },
        backhoe: {
          capacity: {
            unit: "m3",
            value: []
          },
          cost: {
            unit: "EC$h-1",
            value: []
          },
          quantity: []
        },
        lorry: {
          capacity: {
            unit: "t",
            value: []
          },
          cost: {
            unit: "EC$h-1",
            value: []
          },
          quantity: []
        }
      },
      labour: {
        unskilled: {
          cost: {
            unit: "EC$h-1",
            value: 0
          },
          quantity: 0
        },
        skilled: {
          cost: {
            unit: "EC$h-1",
            value: 0
          },
          quantity: 0
        }
      }
    },
    /**
     * Get list of all available equipment
     *
     * @memberof Mossaic.models.EngineeringCost
     * @instance
     * @return {object} Hash of the types of equipment available in the model
     */
    get_available_equipment: function() {
      var equipment = this.get("equipment");
      return _.sortBy(_.keys(equipment), function(key) {
        return key;
      });
    },
    /**
     * Get equipment list in format suitable for rendering
     *
     * @memberof Mossaic.models.EngineeringCost
     * @instance
     * @return {array} Array of equipment in render-friendly format
     */
    get_equipment: function() {
      var equipment = this.get("equipment");
      var equipment_t = [];
      var keys = this.get_available_equipment();
      _.each(keys, function(key) {
        var item = equipment[key];
        var number_of_items = item.quantity.length;
        var lengths_equal = _.all([item.capacity.value.length,
            item.cost.value.length, item.quantity.length], function(length) {
          return length === number_of_items;
        });
        if (lengths_equal) {
          for (var i = 0; i < number_of_items; ++i) {
            // Mustache failing to handle nested objects inside lists properly,
            // so everything is at top level
            var new_item = {
              name: key,
              capacity: item.capacity.value[i],
              capacity_unit: item.capacity.unit,
              cost: item.cost.value[i],
              cost_unit: item.cost.unit,
              quantity: item.quantity[i],
              index: i
            };
            equipment_t.push(new_item);
          }
        } else {
          throw "Badly formed equipment specification";
        }
      });
      return equipment_t;
    },
    /**
     * Get labour list in format suitable for rendering
     *
     * @memberof Mossaic.models.EngineeringCost
     * @instance
     * @return {array} Array of labour in render-friendly format
     */
    get_labour: function() {
      var labour = this.get("labour");
      var labour_t = [];
      var keys = _.sortBy(_.keys(labour), function(key) {
        return key;
      });
      _.each(keys, function(key) {
        var item = labour[key];
        var number_of_items = item.quantity.length;
        var lengths_equal = _.all([item.cost.value.length,
            item.quantity.length], function(length) {
          return length === number_of_items;
        });
        if (lengths_equal) {
          // Mustache failing to handle nested objects inside lists properly,
          // so everything is at top level
          var new_item = {
            name: key,
            cost: item.cost.value,
            cost_unit: item.cost.unit,
            quantity: item.quantity
          };
          labour_t.push(new_item);
        } else {
          throw "Badly formed labour specification";
        }
      });
      return labour_t;
    }
  });

  /**
   * Backbone Model for representing slope reinforcements
   *
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.Reinforcements = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Reinforcements
     * @instance
     * @type object
     */
    defaults: {
      type: "reinforcements"
    }
  });

  /**
   * Backbone Model for representing road network data
   *
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.RoadNetwork = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.RoadNetwork
     * @instance
     * @type object
     */
    defaults: {
      type: "road_network"
    }
  });

  /**
   * Backbone Model for representing results data
   *
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.Results = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Results
     * @instance
     * @type object
     */
    defaults: {
      type: "results"
    }
  });

  /**
   * Backbone Model for representing job requests
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.Job = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Job
     * @instance
     * @type object
     */
    defaults: {
      type: "job"
    }
  });

  /**
   * Backbone Model for representing task requests
   *
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.Task = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.Task
     * @instance
     * @type object
     */
    defaults: {
      type: "task",
      requestor: "TODO - get logged-in from profile widget",
      input_files: {},
      output_variables: {
        "factor_of_safety": true,
        "pressure_head": false,
        "soil_moisture_content": false,
        "total_soil_moisture": false,
        "vegetation_interception": false
      },
      duration: {
        "unit": "h",
        "value": 360
      },
      time_step: {
        "unit": "s",
        "value": 60
      },
      report_filename: "output_summary",
      simulation_parameters: {
        "number_of_jobs": 1
      }
    },
    /**
     * Required inputs for each application type
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     */
    required_inputs: {
      chasm: ["boundary_conditions", "geometry", "meta", "soils",
          "stability"],
      questa: ["boundary_conditions", "geometry", "meta", "soils",
          "stability", "road_network", "engineering_cost"]
    },
    /**
     * Return the mandatory input types for the current application
     *
     * @memberof Mossaic.models.BoundaryConditions
     * @instance
     * @return {array} Array of required input types for the current application
     */
    get_required_inputs: function() {
      var executable = this.get("executable");
      var application;
      if (typeof(executable) !== "undefined") {
        application = executable.application;
      }
      if (typeof(application) !== "undefined") {
        if (application in this.required_inputs) {
          return this.required_inputs[application];
        } else {
          throw("Unsupported application: " + application);
        }
      } else {
        throw ("No application selected");
      }
    }
  });

  /**
   * Backbone Model for representing slope cross sections
   *
   * @constructor
   * @extends Mossaic.models.Basic
   */
  Mossaic.models.CrossSection = Mossaic.models.Basic.extend({
    /**
     * Default values for the model
     *
     * @memberof Mossaic.models.CrossSection
     * @instance
     * @type object
     */
    defaults: {
      type: "cross_section",
      start: {},
      end: {}
    }
  });

  /**
   * Convenience map for retrieving the relevant model type
   *
   * @type object
   */
  Mossaic.models.map = {
    basic: Mossaic.models.Basic,
    geometry: Mossaic.models.Geometry,
    stability: Mossaic.models.Stability,
    meta: Mossaic.models.Meta,
    soils: Mossaic.models.Soils,
    boundary_conditions: Mossaic.models.BoundaryConditions,
    engineering_cost: Mossaic.models.EngineeringCost,
    reinforcements: Mossaic.models.Reinforcements,
    road_network: Mossaic.models.RoadNetwork,
    results: Mossaic.models.Results,
    task: Mossaic.models.Task,
    job: Mossaic.models.Job,
    cross_section: Mossaic.models.CrossSection
  };
})();
