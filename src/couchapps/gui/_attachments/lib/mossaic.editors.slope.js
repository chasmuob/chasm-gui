/**
 * @file Defines the slope editor
 */
(function () {
  var INDEX_IF_GEOMETRY_CLEAN = 3;

  Mossaic.degs_to_rads = Math.PI / 180;

  /**
   * Helper method to create the divs for the slope editor
   */
  var create_divs = function(parent_div, divnames) {
    for (var i = 0; i < divnames.length; ++i) {
      if (parent_div.select(divnames[i])[0][0] === null) {
        parent_div.append("xhtml:div")
          .attr("id", divnames[i].slice(1));
      }
    }
  }

  var browsers = {};

  /**
   * Slope editor. Consists of a set of models, a graphical display and a 
   * Mossaic.workflow.slope which provides a linear series of activities
   * allowing the user to edit the models in a way that makes sense in their
   * domain.
   *
   * Mossaic.workflow.slope workflow sets various model flags which can change
   * the behaviour of the graphical display. For example, the datum setting
   * stage of the workflow will set a flag on the geometry model that causes
   * the graphical display to allow the datum to be set.
   *
   * Use the module pattern to build the slope editor and return the expose
   * parts of the editor via a closure.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {Mossaic.models.Meta} options.meta Slope datum metadata model
   * @param {Mossaic.models.Soils} options.soils_data Soils model
   * @param {string} options.divname Selector for the div to which the slope
   *                                 editor should be appended
   * @param {string} options.display_div Selector for the div to be used for the
   *                                     graphical display
   * @param {string} options.form_div Selector for the div to be used for the
   *                                  form display
   * @param {string} options.overspill_div
   *                                      CSS selector of the div to which the
   *                                      "overspill" form elements (elements
   *                                      that we don't want on the main form)
   *                                      can be added
   * @param {string} options.tab_div      CSS selector of the div to which the
   *                                      tabs should be added
   * @param {Mossaic.widgets.ls} options.geometry_browser
   *                                      The Mossaic.widgets.ls object that
   *                                      selects the Mossaic.models.Geometry
   *                                      model used in the workflow
   * @param {integer} options.number_of_sections
   *                                      For CHASM workflows, number of slope
   *                                      sections to be created
   * @param {integer} options.upslope_sections
   *                                      For QUESTA workflows, number of
   *                                      slope sections upslope of the road
   *                                      section
   * @param {integer} options.downslope_sections
   *                                      For QUESTA workflows, number of
   *                                      slope sections downslope of the
   *                                      road section
   * @param {string} options.next_route   The route that should be navigated
   *                                      to when the workflow is completed
   * @param {string} options.current_route
   *                                      The route that initiated the workflow
   */
  Mossaic.editors.slope = function(options) {
    var that = {};
    var application = options.application || "chasm";
    var tip_div = options.tip_div;
    var overspill_div = options.overspill_div;
    var tab_div = options.tab_div;
    var models = {};
    var next_route = options.next_route || "#";
    var current_route = options.current_route || "#";
    var geometry_browser = options.geometry_browser;
    var number_of_sections = options.number_of_sections;
    var upslope_sections = options.upslope_sections;
    var downslope_sections = options.downslope_sections;
    var name_source = options.name_source;

    models.meta = new Mossaic.models.Meta(options.meta) ||
        new Mossaic.models.Meta();
    models.soils = new Mossaic.models.Soils(options.soils_data) ||
        new Mossaic.models.Soils();
    var layers = new Backbone.Collection([new Backbone.Model({})]);
    models.boundary_conditions = new Mossaic.models.BoundaryConditions({});

    if (options.geometry) {
      models.geometry = options.geometry;
      var layers = models.geometry.layers;
    } else {
      var layers = new Backbone.Collection([new Backbone.Model({})]);
      models.geometry = new Mossaic.models.Geometry({}, {
        layers: layers, datum: models.meta});
      models.geometry.is_dirty = true;
    }

    models.stability = new Mossaic.models.Stability({}, {
      geometry: models.geometry
    });
    models.engineering_cost = new Mossaic.models.EngineeringCost();
    models.road_network = new Mossaic.models.RoadNetwork();

    if (!options.meta) {
      models.meta.is_dirty = true;  // Locally created model not yet saved
    }
    if (!options.soils_data) {
      models.soils.is_dirty = true;
    }
    models.boundary_conditions.is_dirty = true;
    models.stability.is_dirty = true;
    models.engineering_cost.is_dirty = true;
    models.road_network.is_dirty = true;

    var display_div = options.display_div || "#slope_display";
    var form_div = options.form_div || "#slope_form";
    var form_container_div = form_div + "_container";
    var tab_div = form_div + "_tabs";
    var div = d3.select(options.divname);

    create_divs(div, [tab_div, display_div, form_container_div]);
    create_divs(d3.select(form_container_div), [form_div]);

    var slope_display = new Mossaic.vis.Slope(models.geometry, {
      divname: display_div,
      parent_divname: options.divname,
      road: {},
      quantize: false
    });

    var stability_display = new Mossaic.vis.Stability(models.stability, {
      slope_display: slope_display,
      geometry: models.geometry, // TODO maybe remove as stability model has a ref to this anyway
      quantize: false
    });

    /**
     * Mossaic.widgets.ls_save widgets for simulation parameters
     *
     * @name browsers
     * @memberof Mossaic.editors.slope
     * @instance
     * @type object
     * @property {Mossaic.widgets.ls_save} soils
     *                                      Browser for soils data
     * @property {Mossaic.widgets.ls_save} boundary_conditions
     *                                      Browser for boundary conditions data
     * @property {Mossaic.widgets.ls_save} stability
     *                                      Browser for stability data
     * @property {Mossaic.widgets.ls_save} engineering_cost
     *                                      Browser for engineering cost data
     * @property {Mossaic.widgets.ls_save} road_network
     *                                      Browser for road network data
     */
    that.browsers = {};

    workflows = {
      slope: Mossaic.workflow.slope({
        model: models.geometry,
        tip_div: tip_div,
        overspill_div: overspill_div,
        tab_div: tab_div,
        divname: form_div,
        soils: models.soils,
        stability: models.stability,
        boundary_conditions: models.boundary_conditions,
        engineering_cost: models.engineering_cost,
        road_network: models.road_network,
        next_route: next_route,
        current_route: current_route,
        parent: that,
        browsers: that.browsers,
        number_of_sections: number_of_sections,
        upslope_sections: upslope_sections,
        downslope_sections: downslope_sections,
        name_source: geometry_browser
      })
    };

    /**
     * Set a new geometry model and update all things that depend on it.
     * @name set_geometry
     * @method
     * @memberof Mossaic.editors.slope
     * @instance
     * @param {Mossaic.models.Geometry} new_geometry Geometry model to set
     */
    that.set_geometry = function(new_geometry) {
      var layers_to_add;
      models.geometry = new_geometry;
      models.stability.geometry = models.geometry;
      slope_display.set_model(models.geometry);
      models.geometry.bind("change", stability_display.redraw);
      workflows.slope.set_geometry(models.geometry);
      models.geometry.datum = models.meta;
      models.geometry.change();
    }

    /**
     * Get active model of specified type.
     * @name get_model
     * @method
     * @memberof Mossaic.editors.slope
     * @instance
     * @param {String} model Type of model (see Mossaic.models.map)
     * @return Currently live model of type requested
     */
    that.get_model = function(model) {
      return models[model];
    };

    /**
     * Redraw everything
     * @name show
     * @method
     * @memberof Mossaic.editors.slope
     * @instance
     * @param {object} options_show Hash of options to pass to the workflows
     *                              show method (see
     *                              Mossaic.workflows.slope.show)
     */
    that.show = function(options_show) {
      // Intentional reference to options object in the above scope
      var div = d3.select(options.divname);
      create_divs(div, [tab_div, display_div, form_container_div]);
      create_divs(d3.select(form_container_div), [form_div]);
      if (d3.select(display_div)[0][0] === null ||
          d3.select(display_div).html() === "") {
        slope_display.show();
      }
      if (models.geometry.is_dirty) {
        // Force a fresh draw of the stability display as the svg:g it was drawn
        // to no longer exists on the screen
        stability_display.draw();
        workflows.slope.show(options_show);
      } else {
        // Take user to the "geometry complete" bit of the workflow
        options_show.start_index = INDEX_IF_GEOMETRY_CLEAN;
        workflows.slope.show(options_show);
      }
    };

    /**
     * Show or hide the stability display.
     * Other GUI widgets may want to do this so it is exposed.
     * @name set_stability_visible
     * @method
     * @memberof Mossaic.editors.slope
     * @instance
     * @param {boolean} visible True if the stability display should be made
     *                          visible
     */
    that.set_stability_visible = function(visible) {
      if (visible) {
        stability_display.show();
      } else {
        stability_display.hide();
      }
    };

    /**
     * Show or hide the stability radius.
     * Other GUI widgets may want to do this so it is exposed.
     * @name set_stability_radius_visible
     * @method
     * @memberof Mossaic.editors.slope
     * @instance
     * @param {boolean} visible True if the stability radius should be made
     *                          visible
     */
    that.set_stability_radius_visible = function(visible) {
      if (visible) {
        stability_display.show_radius();
      } else {
        stability_display.hide_radius();
      }
    }

    /**
     * Set the soils model
     * @name set_soils_data
     * @method
     * @memberof Mossaic.editors.slope
     * @instance
     * @param {Mossaic.models.Soils} soils_data Soils model
     */
    that.set_soils_data = function(soils_data) {
      models.soils = soils_data;
      workflows.slope.set_soils(models.soils);
      models.soils.change();
      return that;
    };

    /**
     * Set stability data
     * Stability has no child data so we just update
     * @name set_stability
     * @method
     * @memberof Mossaic.editors.slope
     * @instance
     * @param {Mossaic.models.Stablity} stability Stability model
     */
    that.set_stability = function(stability) {
      models.stability = stability;
      models.stability.geometry = models.geometry;
      workflows.slope.set_stability(models.stability);
      stability_display.set_model(models.stability);
      models.geometry.bind("change", stability_display.redraw);
      models.stability.change();
    };

    /**
     * Set the boundary conditions data
     * No children so just update
     * @name set_boundary_conditions_data
     * @method
     * @memberof Mossaic.editors.slope
     * @instance
     * @param {Mossaic.models.BoundaryConditions} boundary_conditions Boundary
     *                                                                Conditions
     */
    that.set_boundary_conditions = function(boundary_conditions) {
      models.boundary_conditions = boundary_conditions;
      workflows.slope.set_boundary_conditions(models.boundary_conditions);
      models.boundary_conditions.change();
    };

    /**
     * Set the engineering cost
     * No children so just update
     * @name set_engineering_cost_data
     * @method
     * @memberof Mossaic.editors.slope
     * @instance
     * @param {Mossaic.models.EngineeringCost} engineering_cost Engineering cost
     */
    that.set_engineering_cost = function(engineering_cost) {
      models.engineering_cost = engineering_cost;
      workflows.slope.set_engineering_cost(models.engineering_cost);
      models.engineering_cost.change();
    };

    /**
     * Set the road network
     * No children so just update
     * @name set_road_network
     * @method
     * @memberof Mossaic.editors.slope
     * @instance
     * @param {Mossaic.models.RoadNetwork} road_network Road network
     */
    that.set_road_network = function(road_network) {
      models.road_network = road_network;
      workflows.slope.set_road_network(models.road_network);
      models.road_network.change();
    };    

    that.browsers.soils = Mossaic.widgets.ls_save({
      type: "soils",
      ls: {
        divname: "#soils_browser",
        label: "Matching " + Mossaic.utils.type_to_pretty["soils"].toLowerCase() + " files",
        parent: geometry_browser,
        view: "FilterList",
        url: "soils_by_number",
        get_keys: function(type, parent) {
          var model = parent.get_selected_model();
          var minimum_number_of_soils = 0;
          if (typeof(model) !== "undefined") {
            minimum_number_of_soils = model.get_number_of_soil_interfaces() + 1;
          }
          if (model.mode === "questa") {
            ++minimum_number_of_soils;
          }
          return {
            startkey: minimum_number_of_soils,
            endkey: {}
          }
        },
        set_model: that.set_soils_data,
        init_model: that.get_model("soils"),
        indicate_if_dirty: true
      },
      save: {
        divname: "#soils_save",
        get_model: function() {
          return that.get_model("soils");
        },
        allow_save: true,
        dirty_message: "Save",
        clean_message: "Selected",
        class_ok: "btn-success",
        disable_on_success: true,
        clear_model_on_parent_change: true,
        css_class: "half-button"
      },
      clear_model: {
        divname: "#soils_clear_model"
      }
    });
    that.browsers.soils.hide();
    that.browsers.soils.reset();
    that.browsers.stability = Mossaic.widgets.ls_save({
      type: "stability",
      ls: {
        divname: "#stability_browser",
        label: "Matching " + Mossaic.utils.type_to_pretty["stability"].toLowerCase() + " files",
        parent: geometry_browser,
        view: "FilterList",
        set_model: that.set_stability,
        init_model: that.get_model("stability"),
        indicate_if_dirty: true
      },
      save: {
        divname: "#stability_save",
        get_model: function() {
          return that.get_model("stability");
        },
        allow_save: true,
        dirty_message: "Save",
        clean_message: "Selected",
        class_ok: "btn-success",
        disable_on_success: true,
        clear_model_on_parent_change: true,
        css_class: "half-button"
      },
      clear_model: {
        divname: "#stability_clear_model"
      }
    });
    that.browsers.stability.hide();
    that.browsers.stability.reset();
    that.browsers.boundary_conditions = Mossaic.widgets.ls_save({
      type: "boundary_conditions",
      ls: {
        divname: "#boundary_conditions_browser",
        label: "Matching " + Mossaic.utils.type_to_pretty["boundary_conditions"].toLowerCase() + " files",
        type: "boundary_conditions",
        view: "FilterList",
        set_model: that.set_boundary_conditions,
        init_model: that.get_model("boundary_conditions"),
        indicate_if_dirty: true
      },
      save: {
        divname: "#boundary_conditions_save",
        get_model: function() {
          return that.get_model("boundary_conditions");
        },
        allow_save: true,
        dirty_message: "Save",
        clean_message: "Selected",
        class_ok: "btn-success",
        disable_on_success: true,
        clear_model_on_parent_change: true,
        css_class: "half-button"
      },
      clear_model: {
        divname: "#boundary_conditions_clear_model"
      }
    });
    that.browsers.boundary_conditions.hide();
    that.browsers.engineering_cost = Mossaic.widgets.ls_save({
      type: "engineering_cost",
      ls: {
        divname: "#engineering_cost_browser",
        label: "Matching " + Mossaic.utils.type_to_pretty["engineering_cost"].toLowerCase() + " files",
        type: "engineering_cost",
        view: "FilterList",
        set_model: that.set_engineering_cost,
        init_model: that.get_model("engineering_cost"),
        indicate_if_dirty: true
      },
      save: {
        divname: "#engineering_cost_save",
        get_model: function() {
          return that.get_model("engineering_cost");
        },
        allow_save: true,
        dirty_message: "Save",
        clean_message: "Selected",
        class_ok: "btn-success",
        disable_on_success: true,
        clear_model_on_parent_change: true,
        css_class: "half-button"
      },
      clear_model: {
        divname: "#engineering_cost_clear_model"
      }
    });
    that.browsers.engineering_cost.hide();
    that.browsers.road_network = Mossaic.widgets.ls_save({
      type: "road_network",
      ls: {
        divname: "#road_network_browser",
        label: "Matching " + Mossaic.utils.type_to_pretty["road_network"].toLowerCase() + " files",
        type: "road_network",
        view: "FilterList",
        set_model: that.set_road_network,
        init_model: new Mossaic.models.RoadNetwork(),
        indicate_if_dirty: true
      },
      save: {
        divname: "#road_network_save",
        get_model: function() {
          return that.get_model("road_network");
        },
        allow_save: false,
        dirty_message: "Choose road network",
        clean_message: "Selected",
        class_ok: "btn-success",
        disable_on_success: true,
        clear_model_on_parent_change: true,
        css_class: "half-button"
      },
      clear_model: {
        divname: "#road_network_clear_model"
      }
    });
    that.browsers.road_network.hide();

    if (models.geometry.is_dirty) {
      workflows.slope.show({number_of_sections: number_of_sections});
    } else {
      // Take user to "geometry complete" bit...
      workflows.slope.show({
        start_index: INDEX_IF_GEOMETRY_CLEAN,
        number_of_sections: number_of_sections
      });
    }

    slope_display.show();

    return that;
  };
})();