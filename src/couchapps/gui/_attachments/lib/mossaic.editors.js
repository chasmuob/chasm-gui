/**
 * @file Creates the Mossaic.editors namespace and imports the scripts to
 *       create the editors
 */
(function() {
  var scripts_to_load = [
    "lib/mossaic.editors.slope.js"
    ],
    load_scripts;

  load_scripts = function(scripts) {
    for (var i=0; i < scripts.length; i++) {
      document.write('<script src="'+scripts[i]+'"><\/script>');
    }
  };

  if (typeof(Mossaic) === "undefined") {
    Mossaic = {};
  }
  /**
   * Editors used in the application.
   * An editor is a composition of workflows and graphical editors that allow
   * a user to create, edit and submit tasks.
   *
   * Editors use the module pattern to provide actual private members. They
   * are instantiated simply by calling the function *without* the new keyword.
   * A hash of functions is returned which form closures with variables
   * inside the function.
   *
   * Unfortunately JSDoc doesn't give us the right tools to properly document
   * the module pattern, so we cheat and use the constructor tag. This means
   * the editors are listed in the documentation as classes which should be
   * instantiated with the new keyword. This is untrue and should be ignored
   * for all editors.
   *
   * @namespace
   */
  Mossaic.editors = {};

  load_scripts(scripts_to_load);
})();