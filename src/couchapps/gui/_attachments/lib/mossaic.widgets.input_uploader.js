/**
 * @file Defines a widget for uploading raw input files to CouchDB
 */
(function() {
  /**
   * Create an input_uploader widget which allows raw input files to be
   * uploaded.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {string} options.parent CSS selector of parent div to append to
   */
  Mossaic.widgets.input_uploader = function(options) {
    var template = Mossaic.db.ddoc.templates.input_uploader;
    var parent = options.parent
    var db_name = Mossaic.db.get_db_name();
    var db = $.couch.db(db_name);
    var type_selector;
    var ls_widgets = {};
    var ls_widgets_to_show = [];

    var update_ls_widgets = function() {
      var selected_type = $(parent + " #input_type :selected").attr("id");
      _.each(_.values(ls_widgets), function(ls_widget) {
        ls_widget.hide();
      });
      _.each(_.range(ls_widgets_to_show.length), function() {
        ls_widgets_to_show.pop();
      });
      if (selected_type === "geometry") {
        ls_widgets_to_show.push("datum");
        ls_widgets_to_show.push("cross_section");
      } else if (selected_type === "stability") {
        ls_widgets_to_show.push("datum");
        ls_widgets_to_show.push("cross_section");
        ls_widgets_to_show.push("geometry");
      }
      _.each(ls_widgets_to_show, function(widget) {
        if (typeof(ls_widgets[widget]) === "undefined") {
          if (widget === "datum") {
            ls_widgets.datum = Mossaic.widgets.ls({
              divname: "#data_upload_ls_datum",
              label: Mossaic.utils.type_to_pretty["meta"],
              type: "meta",
              view: "AutoBox",
              focus: true
            });
            ls_widgets.datum.reset();
          } else if (widget === "cross_section") {
            ls_widgets.cross_section = Mossaic.widgets.ls({
              divname: "#data_upload_ls_cross_section",
              label: Mossaic.utils.type_to_pretty["cross_section"],
              type: "cross_section",
              parent: ls_widgets.datum,
              view: "AutoBox",
              focus: false,
              clear_model_on_parent_change: true,
              silent: true
            });
            ls_widgets.cross_section.reset();
          } else if (widget === "geometry") {
            ls_widgets.geometry = Mossaic.widgets.ls({
              divname: "#data_upload_ls_geometry",
              label: Mossaic.utils.type_to_pretty["geometry"],
              type: "geometry",
              parent: ls_widgets.cross_section,
              view: "AutoBox",
              focus: false,
              clear_model_on_parent_change: true,
              silent: true
            });
            ls_widgets.geometry.reset();
          }
        } else {
          ls_widgets[widget].show();
        }
      });
    };

    $(parent).html($.mustache(template, {}));
    update_ls_widgets();

    type_selector = $(parent + " #input_type");
    type_selector.change(update_ls_widgets);

    $(parent + " #input_upload_form").submit(function(event) {
      event.preventDefault();
      try {
        var data = {};
        var parent_id, parent_model;
        var id_array = [];
        if (ls_widgets_to_show.length > 0) {
          parent_model = ls_widgets[_.last(ls_widgets_to_show)].
            get_selected_model();
          if (typeof(parent_model) === "undefined") {
            throw "No valid parent selected";
          }
          parent_id = parent_model.get("_id");
          if (typeof(parent_id) === "undefined") {
            throw "No valid parent id could be found";
          }
          id_array = [parent_id];
          data.parent_id = parent_id;
        }
        data.name = $(parent + " #input_name").val();
        if (typeof(data.name) === "undefined" || data.name === "") {
          throw "No name provided for this data";
        }
        data.destination_type = $(parent + " #input_type :selected").attr("id");
        data._id = id_array.concat(["raw_input:" + data.name]).join("|");
        data.destination_id = id_array.concat(
            [data.destination_type + ":" + data.name]).join("|");
        var file = $("#input_file").val();
        if (typeof(file) === "undefined" || file === "") {
          throw "No file selected";
        }
        $(parent + " #upload_input").attr('disabled', 'disabled');
        db.saveDoc(data, {
          w: 3, // TODO: fix this magic number as part of larger WP to make GUI
                // quorum-aware
          success: function(response) {
            // Use jquery.forms.js as jquery.couch.js provides no attachment
            // upload facility
            $(parent + " #rev").val(response.rev);
            $(parent + " #input_upload_form").ajaxSubmit({
              url: "/" + db_name + "/" + $.couch.encodeDocId(response.id),
              success: function(response) {
                if (response.match("ok")) {
                  alert("Input data successfully uploaded");
                  $(parent + " #upload_input").removeAttr('disabled');
                } else {
                  alert("Something went wrong uploading the file: " + response);
                  $(parent + " #upload_input").removeAttr('disabled');
                }
              },
              error: function(status, error, reason) {
                alert("Something went wrong attaching the input file: " + status);
                $(parent + " #upload_input").removeAttr('disabled');
              }
            });
          },
          error: function(status, error, reason) {
            if (status === 409) {
              alert("Data with name " + data.name + " already exists");
            } else {
              alert("Something went wrong creating the input document: " +
                status);
            }
            $(parent + " #upload_input").removeAttr('disabled');
          }
        });
      } catch(exception) {
        alert("Cannot upload data: " + exception);
      }
      
    });
  };
})();