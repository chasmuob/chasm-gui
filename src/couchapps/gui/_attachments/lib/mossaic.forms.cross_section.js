/**
 * @file Define the form for displaying Mossaic.models.CrossSection data
 */
(function() {
  /**
   * Backbone View for slope metadata represented by Mossaic.models.CrossSection
   * which displays values on a form.
   * @constructor
   * @extends Mossaic.forms.Basic
   */
  Mossaic.forms.CrossSection = Mossaic.forms.Basic.extend({
    /**
     * Template used to create the form
     * @memberof Mossaic.forms.CrossSection
     * @instance
     * @type string
     */
    template: '<div id="{{form_id}}" class="mossaic-form"><table class="wide">\
      {{#start}}\
        <tr><td>Top</td></tr>\
        <tr>\
          <td><label>Northing</label></td>\
          <td><input id="lat_start" class="input-thin" value="{{lat}}"></input></td>\
        </tr>\
        <tr>\
          <td><label>Easting</label></td>\
          <td><input id="long_start" class="input-thin" value="{{long}}"></input></td>\
        </tr>\
        </tr>\
      {{/start}}\
      {{#end}}\
        <tr><td>Toe</td></tr>\
        <tr>\
          <td><label>Northing</label></td>\
          <td><input id="lat_end" class="input-thin" value="{{lat}}"></input></td>\
        </tr>\
        <tr>\
          <td><label>Easting</label></td>\
          <td><input id="long_end" class="input-thin" value="{{long}}"></input></td>\
        </tr>\
      </tr>{{/end}}\
      </table></div>',
    /**
     * Initialize the form.
     * Called implicitly with "new Mossaic.forms.CrossSection".
     * @memberof Mossaic.forms.CrossSection
     * @instance
     * @param {Mossaic.models.CrossSection} model Model to be represented
     *                                            by this form
     * @param {object} options Hash of options
     * @param {string} options.divname jQuery selector for div to which this
     *                                 form should be appended
     * @param {boolean} options.readonly Set to true if model data should not
     *                                   be editable on the form
     */
    initialize: function(model, options) {
      Mossaic.forms.Basic.prototype.initialize.apply(this, [model, options]);
      this.divname = options.divname || "#cross_section_form";
      this.form_id = this.divname + "_cross_section";
      this.readonly = options.readonly;
    },
    /**
     * Update the model with values in the form.
     * This method is bound to the DOM elements in the render function and so
     * does not need to be called directly.
     * @memberof Mossaic.forms.CrossSection
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM element
     */
    update_model: function(event) {
      var id = $(event.target).attr("id");
      var value = $(event.target).val();
      var id_tokens = id.split("_");
      var lat_or_long = id_tokens[0];
      var start_or_end = id_tokens[1];
      var to_update = _.clone(this.model.get(start_or_end));
      if (typeof(to_update) === "undefined") {
        to_update = {};
      }
      to_update[lat_or_long] = parseFloat(value);
      if (!Mossaic.utils.validate_number(to_update[lat_or_long],
          "Please enter a numeric value for latitude/longitude", event.target)){
        return;
      }
      var invalid_value = isNaN(to_update[lat_or_long]);
      if (invalid_value) {
        alert("Please enter a numeric value for latitude/longitude");
        $(event.target).focus().select();
        return;
      }
      this.model.changed_by = this;
      var to_set = {};
      to_set[start_or_end] = to_update;
      this.model.set(to_set);
    },
    /**
     * Handle change events triggered by the model. Only redraw if the
     * model was changed by a different view.
     * @memberof Mossaic.forms.CrossSection
     * @instance
     */
    handle_change: function() {
      // Change may have originated from this view or somewhere else. So check
      // called_by attribute. We only want to render if the model was changed
      // by a different view as this view will already be up-to-date.
      if (this.model.changed_by != this) {
        this.render();
      }
    },
    /**
     * Render the form.
     * Will be called implicitly by show or handle_change so should not need
     * to be called directly.
     * @memberof Mossaic.forms.CrossSection
     * @instance
     */
    render: function() {
      if (this.hidden) {
        return;
      }
      var that = this;
      var start = this.model.get("start") || {};
      var end = this.model.get("end") || {};
      $(this.form_id).remove();
      $(this.divname).append($.mustache(this.template, {
        form_id: this.form_id.split("#")[1],
        start: start,
        end: end
      }));
      if (this.readonly) {
        $(this.form_id + " input").attr("disabled", "disabled");
      } else {
        $("#lat_start").change(this.update_model);
        $("#long_start").change(this.update_model);
        $("#lat_end").change(this.update_model);
        $("#long_end").change(this.update_model);
      }
    }
  });
})();