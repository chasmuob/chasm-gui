/**
 * @file Defines a widget that provides a button that validates and saves one
 *       or more models.
 */
(function() {
  /**
   * Create a save widget - a button which will save one or more models. Will
   * provide a visual indication if the models it is responsible for are dirty.
   *
   * Can also be used with save ability disabled, if you just want to only
   * allow the success function to be called when all models are loaded (and
   * therefore clean).
   *
   * If being used with multiple models you will need to make sure get_model
   * returns an array of all models, and that name_source is an array of
   * ls_widgets, in the correct order for the array of models returned by
   * get_model. Not great, but this was a bit of a last minute kludge.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {string} options.divname     Id selector of the div to which this
   *                                     widget will be appended
   * @param {boolean} options.allow_save Set to false to disable saving, and
   *                                     make the widget a validator only. This
   *                                     will make the button call the success
   *                                     function if the associated models are
   *                                     clean, otherwise display an error.
   * @param {string} options.type        Type of document that will be saved
   * @param {function} options.get_model Return the Backbone.Model(s) to be
   *                                     saved
   * @param {object} options.name_source Mossaic.widgets.ls widget that provides
   *                                     the name that should be used for the
   *                                     model to be saved
   * @param {function} options.success   Success function to be called when
   *                                     save completes and all models are clean
   * @param {string} options.dirty_message Message to be displayed on the button
   *                                       when one or more models are dirty
   * @param {string} options.clean_message Message to be displayed on the button
   *                                       when all associated models are clean
   * @param {boolean} options.disable_on_success
   *                                     Disable the button once the model is
   *                                     successfully saved
   * @param {string} options.css_class   The CSS class that should be applied
   *                                     to the button
   * @param {string} options.class_ok    The CSS class that should be applied
   *                                     to the button if the associated models
   *                                     are clean
   * @param {function} options.instead_of_save
   *                                     Function to call instead of attempting
   *                                     to save the model. This will be called
   *                                     after the parents are confirmed as
   *                                     clean. If you haven't already guessed
   *                                     this is a bit of a hack to squeeze
   *                                     some extra functionality out of this
   *                                     widget.
   */
  Mossaic.widgets.save = function(options) {
    var divname = options.divname;
    var disable_on_clean = options.disable_on_success;
    var instead_of_save = options.instead_of_save;
    var css_class = options.css_class;
    var allow_save = options.allow_save;
    var class_ok = options.class_ok || "btn-primary";
    var type = options.type;
    var get_model = options.get_model;
    var name_source = options.name_source;
    if (!name_source.hasOwnProperty("length")) {
      name_source = [name_source];
    }
    var success = options.success;
    var dirty_message = options.dirty_message;
    var clean_message = options.clean_message || "Use this slope";
    var id = divname.split("#")[1] + "_save";
    var save_as_id = id + "_as";
    var template = '<button id="{{save_as_id}}"\
      class="btn {{css_class}}">{{message}}</button>';
    $(divname).html($.mustache(template, {
        save_as_id: save_as_id,
        message: dirty_message,
        css_class: options.css_class
    }));
    $("#" + save_as_id).click(function() {
      var model = get_model(type);
      if (model.hasOwnProperty("length")) {
        if (!name_source.hasOwnProperty("length") ||
            name_source.length !== model.length) {
          console.log("Trying to save multiple models but incorrect number " +
              "of name sources provided. Giving up.");
          return;
        }
      } else {
        model = [model];
      }
      var save_wrapper = function(inputs, acc, on_finish) {
        var save_success = function() {
          if (acc === inputs.length - 2 &&
              typeof(instead_of_save) === "function") {
            instead_of_save();
            return;
          } else if (acc < inputs.length - 1) {
            save_wrapper(inputs, ++acc, on_finish);
          } else {
            console.log("All inputs saved");
            if (typeof(on_finish) === "function") {
              return on_finish();
            }
          }
        };
        if (!inputs[acc].is_dirty) {
          save_success();
        } else if (!allow_save) {
          alert("Please select a " +
              Mossaic.utils.type_to_pretty[inputs[acc].get("type")] +
              " dataset");
          return;
        } else {
          if (inputs.length === 0) {
            return on_finish();
          } else {
            Mossaic.utils.save_model(inputs[acc], name_source[acc], {
              success: save_success
            });
          }
        }
      };
      save_wrapper(model, 0, function() {
        if (typeof(success) === "function") {
          return success();
        }
      });
    });
    var show_model_changed = function() {
      var local_model = get_model(type);
      if (!local_model.hasOwnProperty("length")) {
        local_model = [local_model];
      }
      var save_as = $("#" + save_as_id);
      if (_.any(local_model, function(model) { return model.is_dirty; })) {
        save_as.removeClass(class_ok);
        save_as.removeClass("disabled");
        save_as.addClass("btn-warning");
        save_as.html(dirty_message);
      } else {
        save_as.removeClass("btn-warning");
        save_as.addClass(class_ok);
        save_as.html(clean_message);
        if (disable_on_clean) {
          save_as.addClass("disabled");
        }
      }
    };
    show_model_changed();                // Initial status
    // Bind to initially loaded models
    var local_model = get_model(type);
    if (!local_model.hasOwnProperty("length")) {
      local_model = [local_model];
    }
    _.each(local_model, function(m, i) {
      m.bind("change", show_model_changed); // Bind for changes
      name_source[i].on_model_load(function(model) {     // Bind to future models
        show_model_changed(model);
        model.bind("change", show_model_changed);
      });
    });
    return {
      /**
       * Show the widget
       * @memberof Mossaic.widgets.save
       * @instance
       */
      show: function() {
        $(divname).html($.mustache(template, {
            save_as_id: save_as_id,
            message: dirty_message,
            css_class: options.css_class
        }));
      },
      /**
       * Hide the widget
       * @memberof Mossaic.widgets.save
       * @instance
       */
      hide: function() {
        $(divname).html("");
      },
      /**
       * Set the message to be displayed when one or more models are dirty
       * @memberof Mossaic.widgets.save
       * @instance
       */
      set_dirty_message: function(message) {
        dirty_message = message;
      },
      /**
       * Set whether the button will allow the model to be saved
       * @memberof Mossaic.widgets.save
       * @instance
       */
      set_allow_save: function(allow) {
        allow_save = allow;
      }
    };
  };
})();