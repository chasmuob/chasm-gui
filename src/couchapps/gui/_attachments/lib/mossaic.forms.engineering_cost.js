/**
 * @file Define the form for displaying Mossaic.models.EngineeringCost data
 */
(function() {
  /**
   * Backbone View for engineering cost data represented by
   * Mossaic.models.EngineeringCost which displays values on a form.
   * @constructor
   * @extends Mossaic.forms.Basic
   */
  Mossaic.forms.EngineeringCost = Mossaic.forms.Basic.extend({
    /**
     * Template used to create the form
     * @memberof Mossaic.forms.EngineeringCost
     * @instance
     * @type string
     */
    template: '<div id="{{form_id}}" class="mossaic-form">\
      <table class="wide">\
        <tr>\
          <th width="35%"><label>Labour</label></th>\
          <th><label>Cost</th>\
          <th><label>Quantity</th>\
        </tr>\
        {{#labour}}\
        <tr>\
          <td><label id="labour_name_{{name}}_{{index}}">{{name}}</label></td>\
          <td><input class="labour_input" style="width: 40%;float: left;" id="labour_cost_{{name}}_{{index}}" value="{{cost}}"></input><label style="width: 40%;float: left;"><small>{{cost_unit}}</small></td>\
          <td><input class="labour_input" id="labour_quantity_{{name}}_{{index}}" value="{{quantity}}"></input></td>\
        </tr>\
        {{/labour}}\
      </table>\
      <hr />\
      <div id="{{form_id}}_add_equipment" />\
      <table class="wide">\
        <tr>\
          <th width="35%"><label>Equipment</label></th>\
          <th><label>Capacity</th>\
          <th><label>Cost</th>\
          <th><label>Quantity</th>\
        </tr>\
        {{#equipment}}\
        <tr>\
          <td>\
            <label id="equipment_name_{{name}}_{{index}}">{{name}}</label>\
          </td>\
          <td><input class="equipment_input" style="width: 50%;float: left;" id="equipment_capacity_{{name}}_{{index}}" value="{{capacity}}"></input><label style="width: 35%;float: left;">{{capacity_unit}}</td>\
          <td><input class="equipment_input" style="width: 40%;float: left;" id="equipment_cost_{{name}}_{{index}}" value="{{cost}}"></input><label style="width: 40%;float: left;"><small>{{cost_unit}}</small></td>\
          <td><input class="equipment_input" id="equipment_quantity_{{name}}_{{index}}" value="{{quantity}}"></input></td>\
          <td><button class="delete" id="delete_equipment_{{name}}_{{index}}></button>"\
        </tr>\
        {{/equipment}}\
      </table>\
      </div>',
    /**
     * Initialize the form.
     * Called implicitly with "new Mossaic.forms.EngineeringCost".
     * @memberof Mossaic.forms.EngineeringCost
     * @instance
     * @param {Mossaic.models.EngineeringCost} model Model to be represented
     *                                               by this form
     * @param {object} options Hash of options
     * @param {string} options.divname jQuery selector for div to which this
     *                                 form should be appended
     */
    initialize: function(model, options) {
      Mossaic.forms.Basic.prototype.initialize.apply(this, [model, options]);
      this.divname = options.divname || "#engineering_cost_form";
      this.form_id = this.divname + "_form";
    },
    /**
     * Handle change events triggered by the model. Only redraw if the
     * model was changed by a different view.
     * Changes made by this view only trigger redraw if the equipment items
     * have changed (i.e. equipment has been added/deleted).
     * @memberof Mossaic.forms.EngineeringCost
     * @instance
     */
    handle_change: function() {
      var that = this;
      // Change may have originated from this view or somewhere else. So check
      // called_by attribute. We only want to render if the model was changed
      // by a different view as this view will already be up-to-date.
      if (this.model.changed_by != this) {
        this.render();
      } else {
        // If change originated here but equipment items changed,
        // we still want to update
        if ("equipment" in this.model.changedAttributes()) {
          var items_prev = _.keys(this.model.previous("equipment"));
          var items = _.keys(this.model.get("equipment"));
          var equipment_type_differs = _.any(items, function(item, i) {
            return item !== items_prev[i];
          });
          var equipment_range_differs = _.any(items, function(item) {
            var prev = that.model.previous("equipment")[item].quantity.length;
            var current = that.model.get("equipment")[item].quantity.length;
            return prev !== current;
          });
          if (equipment_type_differs || equipment_range_differs) {
            this.render();
          }
        }
      }
    },
    /**
     * Update the model with values in the form.
     * This method is bound to the DOM elements in the render function and so
     * does not need to be called directly.
     * @memberof Mossaic.forms.EngineeringCost
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM element
     */
    update_model: function(event) {
      var target = event.target;
      var id = target.id;
      var id_tokens = id.split("_");
      var resource_name = id_tokens[0];
      var attribute = id_tokens[1];
      var resource_type = id_tokens[2];
      var index = parseInt(id_tokens[3], 10);
      var resource = _.clone(this.model.get(resource_name));
      var item = _.clone(resource[resource_type]);
      var attr = _.clone(item[attribute]);
      if (typeof(attr) === "number") {
        attr = parseInt(target.value, 10);
      } else if ("value" in attr) {
        var value = _.clone(attr.value);
        if (typeof(index) === "undefined" || isNaN(index)) {
          value = parseFloat(target.value);
        } else {
          value[index] = parseFloat(target.value);          
        }
        attr.value = value;
      } else {
        var parseFun = parseFloat;
        if (attribute === "quantity") {
          parseFun = function(val) {
            return parseInt(val, 10);
          };
        }
        if (typeof(index) === "undefined") {
          attr = parseFun(target.value);
        } else {
          attr[index] = parseFun(target.value);
        }
      }
      item[attribute] = attr;
      resource[resource_type] = item;
      this.model.changed_by = this;
      var update_hash = {};
      update_hash[resource_name] = resource;
      this.model.set(update_hash);
    },
    /**
     * Remove the equipment from the model at the index corresponding to the row
     * of the DOM element that triggered the event.
     * @memberof Mossaic.models.EngineeringCost
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM element
     */
    remove_equipment: function() {
      var target = event.target;
      var id = target.id;
      var id_tokens = id.split("_");
      var name = id_tokens[2];
      var index = parseInt(id_tokens[3], 10);
      var equipment = _.clone(this.model.get("equipment"));
      var equipment_type = _.clone(equipment[name]);
      _.each(["capacity", "cost", "quantity"], function(item_name) {
        var item = _.clone(equipment_type[item_name]);
        if ("value" in item) {
          var value = _.clone(item.value);
          value.splice(index, 1);
          item.value = value;
        } else {
          item.splice(index, 1);
        }
        equipment_type[item_name] = item;
      });
      equipment[name] = equipment_type;
      this.model.changed_by = this;
      this.model.set({equipment: equipment});
    },
    /**
     * Add new equipment to the model. Uses an HTML option form element
     * to determine the type of equipment to add.
     * @memberof Mossaic.models.EngineeringCost
     * @instance
     */
    add_new_equipment: function() {
      // Usual mess of _.clone calls because otherwise we update the model
      // by reference, which screws up handling of change events
      var new_item = $("#equipment_select option:selected").attr("id");
      var equipment = _.clone(this.model.get("equipment"));
      var item = _.clone(equipment[new_item]);
      var capacity = _.clone(item.capacity);
      var capacity_values = _.clone(capacity.value);
      capacity_values.push(null);
      var cost = _.clone(item.cost);
      var cost_values = _.clone(cost.value);
      cost_values.push(null);
      var quantity = _.clone(item.quantity);
      quantity.push(null);
      capacity.value = capacity_values;
      cost.value = cost_values;
      item.capacity = capacity;
      item.cost = cost;
      item.quantity = quantity;
      equipment[new_item] = item;
      this.model.changed_by = this;
      this.model.set({equipment: equipment});
    },
    /**
     * Render the form.
     * Will be called implicitly by show or handle_change so should not need
     * to be called directly.
     * @memberof Mossaic.forms.EngineeringCost
     * @instance
     */
    render: function() {
      if (this.hidden) {
        return;
      }
      var that = this;
      var equipment = this.model.get_equipment();
      $(this.form_id).remove();
      $(this.divname).append($.mustache(this.template, {
        form_id: this.form_id.split("#")[1],
        equipment: equipment,
        labour: this.model.get_labour()
      }));
      var add_new = d3.select(this.form_id + "_add_equipment");
      add_new.append("xhtml:button")
        .html("Add new:")
        .attr("class", "add")
        .on("click", this.add_new_equipment);
      var equipment_select = add_new.append("xhtml:select")
        .attr("id", "equipment_select");
      var available_equipment = this.model.get_available_equipment();
      _.each(available_equipment, function(item) {
        equipment_select.append("xhtml:option")
          .attr("id", item)
          .html(item);
      });
      // Manually bind events because the event hash isn't working
      $(".equipment_input").change(this.update_model);
      $(".labour_input").change(this.update_model);
      $(".delete").click(this.remove_equipment);
    }
  });
})();