/**
 * @file Defines the slope workflow
 */
(function() {
  var UNSAVED_MESSAGE = "Please save your data or select an existing dataset";
  /**
   * Create a Slope workflow. This consists of an array of Backbone.Views
   * which can be sequenced through in order, and a set of Mossaic.widgets.tabs
   * that can be used to jump between stages of the workflow.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {Mossaic.models.Geometry} options.model
   *                                      Geometry model used by the workflow
   * @param {Mossaic.models.Soils} options.soils
   *                                      Soils data model used by the workflow
   * @param {Mossaic.models.BoundaryConditions} options.boundary_conditions
   *                                      Boundary conditions model used by the
   *                                      workflow
   * @param {Mossaic.models.EngineeringCost} options.engineering_cost
   *                                      Engineering cost model used by the
   *                                      workflow
   * @param {Mossaic.models.RoadNetwork} options.road_network
   *                                      Road network model used by the
   *                                      workflow
   * @param {integer} options.number_of_sections
   *                                      For CHASM workflows, number of slope
   *                                      sections to be created
   * @param {integer} options.upslope_sections
   *                                      For QUESTA workflows, number of
   *                                      slope sections upslope of the road
   *                                      section
   * @param {integer} options.downslope_sections
   *                                      For QUESTA workflows, number of
   *                                      slope sections downslope of the
   *                                      road section
   * @param {Mossaic.workflow.slope_editor} options.parent
   *                                      Reference to the
   *                                      Mossaic.workflow.slope_editor that
   *                                      holds this workflow and the graphical
   *                                      slope editor
   * @param {string} options.tip_div      CSS selector of the div to which
   *                                      "tips" for each stage of the workflow
   *                                      should be appended
   * @param {string} options.overspill_div
   *                                      CSS selector of the div to which the
   *                                      "overspill" form elements (elements
   *                                      that we don't want on the main form)
   *                                      can be added
   * @param {string} options.tab_div      CSS selector of the div to which the
   *                                      tabs should be added
   * @param {string} options.divname      CSS selector of the div to which the
   *                                      workflow forms should be added
   * @param {string} options.next_route   The route that should be navigated
   *                                      to when the workflow is completed
   * @param {string} options.current_route
   *                                      The route that initiated the workflow
   * @param {object} options.browsers     Hash of Mossaic.widgets.ls objects
   *                                      that are used to
   *                                      select Simulation Parameters (e.g.
   *                                      soil types, boundary conditions,
   *                                      stability data)
   * @param {Mossaic.widgets.ls} options.name_source
   *                                      The Mossaic.widgets.ls object that
   *                                      selects the Mossaic.models.Geometry
   *                                      model used in the workflow
   */
  Mossaic.workflow.slope = function(options) {
    var tabs;
    var model = options.model;
    var soils = options.soils;
    var stability = options.stability;
    var boundary_conditions = options.boundary_conditions;
    var engineering_cost = options.engineering_cost;
    var road_network = options.road_network;
    var number_of_sections = options.number_of_sections;
    var upslope_sections = options.upslope_sections;
    var downslope_sections = options.downslope_sections;
    var parent = options.parent;
    var tip_div = options.tip_div;
    var overspill_div = options.overspill_div;
    var tab_div = options.tab_div;
    var divname = options.divname;
    var next_route = options.next_route;
    var current_route = options.current_route;
    var browsers = options.browsers;
    var name_source = options.name_source;
    var views = [];
    /**
     * Workflow views that are used by CHASM (and therefore also QUESTA)
     */
    var chasm_views = [];
    /**
     * Workflow views that are only used by QUESTA
     */
    var questa_views = [];
    var index = -1;
    var initial_tabindex = 1;
    /**
     * Save the geometry model. Provided so that saving the geometry can be
     * triggered by the tabs, as part of switching from editing Slope Profile
     * and Simulation Parameters.
     *
     * @memberof Mossaic.workflow.slope
     * @instance
     * @private
     * @param {object} options Hash of options
     * @param {Mossaic.models.Geometry} options.model_to_save Model to be saved
     * @param {integer} options.index Index of tab to navigate to if the save
     *                                is successful
     */
    var save_geometry = function(options) {
      var options = options || {};
      var model_to_save = options.model_to_save;
      var index = options.index;
      Mossaic.utils.save_model(model_to_save, name_source, {
        success: function() {
          if (typeof(index) === "number") {
            route_to(index);
          } else {
            next();
          }
        }
      });
    };
    /**
     * Advance to the next stage in the workflow by setting window.location so
     * the navigation enters the browser history and passes through the
     * Backbone.Router, which will call Mossaic.workflow.slope.show in order
     * to display the required workflow stage.
     *
     * @memberof Mossaic.workflow.slope
     * @instance
     * @private
     */
    var next = function(options) {
      if (index + 1 < views.length) {
        window.location = current_route + "/:" + (index + 1);
      } else {
        views[index].view.hide();
        window.location = next_route;
      }
    };
    /**
     * Navigate to the previous stage in the workflow by setting window.location
     * so the navigation enters the browser history and passes through the
     * Backbone.Router, which will call Mossaic.workflow.slope.show in order
     * to display the required workflow stage.
     * @memberof Mossaic.workflow.slope
     * @instance
     * @private
     */
    var prev = function(options) {
      if (index > 0) {
        window.location = current_route + "/:" + (index - 1);
      }
    };
    /**
     * Route to a specific point in the workflow by setting window.location so
     * the navigation enters the browser history and passes through the
     * Backbone.Router, which will call Mossaic.workflow.slope.show in order
     * to display the required workflow stage.
     * @memberof Mossaic.workflow.slope
     * @instance
     * @private
     * @param {integer} index Index of the workflow to navigate to
     */
    var route_to = function(index) {
      if (index >= 0 && index < views.length) {
        window.location = current_route + "/:" + index;
      }
    };
    /**
     * Show the workflow
     * @memberof Mossaic.workflow.slope
     * @instance
     * @private
     * @param {object} options Hash of options
     * @param {integer} options.index Index of the workflow stage to be shown
     * @param {inteer} options.start_index Index of the first allowable
     *                                     workflow stage. If the required
     *                                     index is less than start_index,
     *                                     start_index will be used.
     */
    var show = function(options) {
      var new_index,
        start_index;
      var options = options || {};
      if (index >= 0 && index < views.length) {
        views[index].view.hide();
      }
      if (typeof(options.index) === "number") {
        new_index = options.index;
      }
      start_index = options.start_index || 0;
      if (start_index > new_index) {
        new_index = start_index;
      }
      if (new_index >= 0 && new_index < views.length) {
        views[new_index].view.show(options);
      }
      index = new_index;
      tabs.update(index);
    };
    /**
     * Start the workflow
     * @memberof Mossaic.workflow.slope
     * @instance
     * @private
     * @param {object} options Hash of options (see Mossaic.workflow.slope.show)
     */
    var start = function(options) {
      if (index > -1) {
        views[index].view.hide();
      }
      index = 0;
      views[index].view.show(options);
      tabs.update(index);
    };

    /**
     * Basic form for the slope workflow
     * @constructor
     * @extends Backbone.View
     */
    var WorkflowForm = Backbone.View.extend({
      /**
       * Initialise the form
       * @memberof WorkflowForm
       * @instance
       * @param {Mossaic.models.Model} model Model to be represented by the form
       * @param {object} options Hash of options
       * @param {string} options.divname CSS selector of the div to which the
       *                                 form should be appended
       * @param {function} options.next Function that takes the user to the
       *                                next stage in the workflow
       */
      initialize: function(model, options) {
        _.bindAll(this);
        /**
         * Whether the form should be hidden or not
         * @memberof WorkflowForm
         * @type boolean
         */
        this.hidden = true;
        /**
         * The model to be represented by this form
         * @memberof WorkflowForm
         * @type Mossaic.models.Basic
         */
        this.model = model;
        /**
         * CSS selector for the div to which this form should be appended
         * @memberof WorkflowForm
         * @type string
         */
        this.divname = options.divname;
        /**
         * Function to take the user to the next stage in the workflow
         * @memberof WorkflowForm
         * @type function
         */
        this.next = options.next || next;
      },
      /**
       * Template used to render the form
       * @memberof WorkflowForm
       * @instance
       * @type string
       */
      template: "",
      /**
       * Show the form
       * @memberof WorkflowForm
       * @instance
       */
      show: function() {},
      /**
       * Hide the form
       * @memberof WorkflowForm
       * @instance
       */
      hide: function() {},
      /**
       * Render the user tip - should be a helpful hint as to what they should
       * be doing at this stage of the workflow
       * @memberof WorkflowForm
       * @instance
       */
      render_tip: function() {},
      /**
       * Render the actual form
       * @memberof WorkflowForm
       * @instance
       */
      render_form: function() {},
      /**
       * Handle change events triggered by the model.
       * @memberof WorkflowForm
       * @instance
       */
      handle_change: function() {},
      /**
       * Render the view
       * @memberof WorkflowForm
       * @instance
       */
      render: function() {}
    });

    /**
     * View for specifying slope section values. Provides a form which allows
     * the surface length, height and angle to be defined for each slope
     * section. Requires *either* options.number_of_sections *or*
     * options.upslope_sections and options.downslope_sections for CHASM and
     * QUESTA geometry data respectively.
     *
     * @constructor
     * @extends WorkflowForm
     */
    var SpecifySections = WorkflowForm.extend({
      /**
       * Initialise the view
       * @memberof SpecifySections
       * @instance
       * @param {Mossaic.models.Geometry} model Geometry model to be represented
       *                                        by the view
       * @param {object} options                Hash of options
       * @param {function} options.next         Function to move to the next
       *                                        step in the workflow
       * @param {string} options.divname        CSS selector of the div to which
       *                                        this form will be added
       * @param {integer} options.upslope_sections
       *                                        For QUESTA geometry models,
       *                                        number of sections upslope of
       *                                        the road section
       * @parm {integer} options.downslope_sections
       *                                        For QUESTA geometry models,
       *                                        number of sections downslope of
       *                                        the road section
       * @param {integer} options.number_of_sections
       *                                        For CHASM geometry models,
       *                                        number of sections in the slope
       */
      initialize: function(model, options) {
        _.bindAll(this);
        this.hidden = true;
        this.model = model;
        this.model.bind("change", this.handle_change);
        this.next = options.next || next;
        this.divname = options.divname || "#slope_workflow";
        this.form_id = this.divname + "_specify_sections";
        this.upslope_sections = options.upslope_sections;
        this.downslope_sections = options.downslope_sections;
        if (typeof(options.number_of_sections) !== "undefined") {
          this.upslope_sections = options.number_of_sections;
        }
      },
      template: Mossaic.db.ddoc.templates.specify_slope_sections,
      /**
       * Set the geometry model associated with this form
       * @memberof SpecifySections
       * @instance
       * @param {Mossaic.models.Geometry} model
       */
      set_geometry: function(model) {
        this.model = model;
        this.model.bind("change", this.handle_change);
      },
      hide: function() {
        $(this.form_id).remove();
        this.hidden = true;
      },
      /**
       * Show the form
       * @memberof SpecifySections
       * @instance
       * @param {object} options Hash of options
       * @param {integer} options.upslope_sections
       *                                        For QUESTA geometry models,
       *                                        number of sections upslope of
       *                                        the road section
       * @parm {integer} options.downslope_sections
       *                                        For QUESTA geometry models,
       *                                        number of sections downslope of
       *                                        the road section
       * @param {integer} options.number_of_sections
       *                                        For CHASM geometry models,
       *                                        number of sections in the slope
       */
      show: function(options) {
        var options = options || {};
        if (typeof(options.upslope_sections) !== "undefined") {
          this.upslope_sections = options.upslope_sections;
        }
        if (typeof(options.downslope_sections) !== "undefined") {
          this.downslope_sections = options.downslope_sections;
        }
        if (typeof(options.number_of_sections) !== "undefined") {
          this.number_of_sections = options.number_of_sections;
        }
        this.hidden = false;
        this.render();
      },
      /**
       * Function to be called when this workflow stage is finished.
       * Specified sections are added to the Mossaic.models.Geometry and if
       * we are dealing with a QUESTA geometry model, set the road section
       * index.
       * @memberof SpecifySections
       * @instance
       */
      finish: function() {
        var that = this;
        that.model.changed_by = this;
        that.model.clear({silent: true});
        that.model.changed_by = this;
        that.model.set(Mossaic.models.Geometry.prototype.defaults, {silent: true});
        var total_sections = this.number_of_sections;
        if (this.model.mode === "questa" &&
            typeof(this.downslope_sections) !== "undefined") {
          total_sections = this.upslope_sections + this.downslope_sections +1;
        }
        try {
          var sections_to_add = [];
          _.each(_.range(0, total_sections), function(i) {
            var parameters = {
              angle: $("#angle_" + i).val(),
              length: $("#length_" + i).val(),
              height: $("#height_" + i).val()
            };
            var defined_parameters = _.filter(_.keys(parameters), function(key) {
              return parameters[key] !== undefined &&
                  parameters[key].length !== 0;
            });
            if (defined_parameters.length !== 2) {
              console.log("Unexpetected number of parameters",
                  defined_parameters);
              throw("Unexpected number of parameters defined!");
            }
            var section = {};
            _.each(defined_parameters, function(parameter) {
              var new_value = parseFloat(parameters[parameter]);
              var invalid_value = isNaN(new_value) ||
                  (parameter !== "angle" && new_value < 0);
              if (invalid_value) {
                throw("Invalid value for " + parameter + " in section " + i);
              }
              section[parameter] = parseFloat(parameters[parameter]);
            });
            sections_to_add.push(section);
          });
          that.model.changed_by = that;
          _.each(sections_to_add, function(section) {
            that.model.add_section(section, {silent: true});
          });
        } catch(e) {
          alert(e);
          return;
        }
        that.model.changed_by = this;
        if (that.model.mode === "questa" &&
            typeof(this.downslope_sections) !== "undefined") {
          that.model.set_road_section(this.upslope_sections);
        }
        this.next({model_to_save: this.model});
      },
      render_tip: function() {
        $(tip_div).html("Enter the slope section measurements");
      },
      /**
       * Helper for render_form which generates empty sections in the format
       * required by the template
       * @memberof SpecifySections
       * @instance
       * @private
       * @param {integer} number_of_sections Number of sections to generate
       * @param {integer} initial_number Index of first generated section
       */
      generate_sections: function(number_of_sections, initial_number) {
        if (typeof(number_of_sections) === "undefined") {
          return [];
        }
        return _.map(
          _.range(initial_number, initial_number + number_of_sections),
          function(i) {
            return {
              number: i,
              tabindex: initial_tabindex + i
            }
          }
        );
      },
      render_form: function(form) {
        var road_section, downslope_sections, upslope_sections;
        if (this.model.mode === "chasm") {
          upslope_sections = this.generate_sections(this.number_of_sections, 0);
        } else if (this.model.mode === "questa") {
          upslope_sections = this.generate_sections(this.upslope_sections, 0);
        }
        var all_sections = upslope_sections;
        var mustache_opts = {};
        if (this.model.mode === "questa" &&
            typeof(this.downslope_sections) !== "undefined") {
          road_section = this.generate_sections(1, this.upslope_sections);
          downslope_sections = this.generate_sections(
              this.downslope_sections, this.upslope_sections + 1);
          mustache_opts.upslope_sections = upslope_sections;
          mustache_opts.downslope_sections = downslope_sections;
          mustache_opts.road_section = road_section;
        } else {
          mustache_opts.upslope_sections = upslope_sections;
        }
        $(this.form_id).html($.mustache(this.template, mustache_opts));
      },
      handle_change: function() {
        if (this.model.changed_by !== this) {
          this.render();
        }
      },
      render: function() {
        if (this.hidden) {
          return;
        }
        $(this.form_id).remove();
        $(this.divname).html('<div id="' + this.form_id.slice(1) + '"></div>');

        this.render_tip();
        this.render_form();

        $(this.form_id + " .prev").click(prev);
        $(this.form_id + " .next").click(this.finish);
        $(this.divname + " input:enabled:first").focus();
      }
    });

    /**
     * View for setting the datum. Doesn't really do anything, just sets the
     * editable.datum flag on the model so that it can be edited via the
     * graphical editor.
     *
     * @constructor
     * @extends WorkflowForm
     */
    var SetDatum = WorkflowForm.extend({
      /**
       * Initialise the view
       * @memberof SetDatum
       * @instance
       */
      initialize: function(model, options) {
        WorkflowForm.prototype.initialize.apply(this, [model, options]);
        this.form_id = this.divname + "_set_datum";
        this.model.bind("change", this.handle_change);
      },
      template: Mossaic.db.ddoc.templates.set_datum,
      /**
       * Set the geometry model associated with this form
       * @memberof SetDatum
       * @instance
       * @param {Mossaic.models.Geometry} model
       */
      set_geometry: function(model) {
        this.model = model;
        this.model.bind("change", this.handle_change);
      },
      hide: function() {
        $(this.form_id).remove();
        this.model.editable.datum = false;
        this.hidden = true;
      },
      show: function() {
        if (this.model.mode === "questa") {
          return this.finish();  // No need to set datum for questa
        }
        this.model.editable.datum = true;
        this.hidden = false;
        this.render();
      },
      finish: function() {
        this.next({model_to_save: this.model});
      },
      handle_change: function() {
        if (this.model.changed_by !== this) {
          var changedAttributes = this.model.changedAttributes();
          if (typeof(changedAttributes.geometry) !== "undefined" &&
              typeof(changedAttributes.geometry.datum) !== "undefined") {
            if (changedAttributes.geometry.datum.index && !this.hidden) {
              this.finish();
            }
          }
          if (this.model.changedAttributes().geometry)
          this.render();
        }
      },
      render_tip: function() {
        $(tip_div).html("Click to set a datum");
      },
      render_form: function() {
        $(this.form_id).html($.mustache(this.template, {}));
      },
      render: function() {
        if (this.hidden) {
          return;
        }
        $(this.form_id).remove();
        $(this.divname).html('<div id="' + this.form_id.slice(1) + '"></div>');

        this.render_tip();
        this.render_form();

        $(this.form_id + " .prev").click(prev);
        $(this.form_id + " .next").click(this.finish);
        $(this.divname + " input:enabled:first").focus();
      }
    });

    /**
     * View for editing slope sections. Displays slope surface section values
     * and sets the editable.surface flag on the model so that a graphical
     * editor can edit the slope surface.
     *
     * @constructor
     * @extends WorkflowForm
     */
    var EditSections = WorkflowForm.extend({
      initialize: function(model, options) {
        WorkflowForm.prototype.initialize.apply(this, [model, options]);
        this.form_id = this.divname + "_edit_sections";
        this.model.bind('change', this.handle_change);
      },
      template: Mossaic.db.ddoc.templates.edit_slope_sections,
      set_geometry: function(model) {
        this.model = model;
        this.model.bind("change", this.handle_change);
      },
      hide: function() {
        $(this.form_id).remove();
        this.model.editable.surface = false;
        this.hidden = true;
      },
      show: function(options) {
        this.hidden = false;
        this.model.editable.surface = true;
        this.render();
      },
      finish: function() {
        this.next({model_to_save: this.model});
      },
      handle_change: function() {
        if (this.model.changed_by != this) {
          this.render();
        }
      },
      render_tip: function() {
        $(tip_div).html("Edit the slope geometry");
      },
      render_form: function(form) {
        // Return an array of objects with index and tabindex properties, as
        // well as whatever was in the array
        var mustacheify = function(section, number) {
          var section = section || {};
          section.number = number;
          section.tabindex = initial_tabindex + number;
          to_tempify = ["surface_height", "surface_length", "angle"];
          for (var i in to_tempify) {
            var attr = to_tempify[i];
            if (attr in section) {
              section[attr + "_value"] = section[attr].value;
            }
          }
          return section;
        };
        var geometry = this.model.get("geometry");
        try {
          var all_sections, upslope_sections;
          if (typeof(geometry.road.slope_section) !== "undefined") {
            var road_index = geometry.road.slope_section;
            upslope_sections = _.map(
              _.filter(geometry.slope_sections, function(section, i) {
                return i < road_index;
              }),
              function(section, i) {
                return mustacheify(section, i);
              }
            );
            var road_section = [mustacheify(geometry.slope_sections[road_index],
              upslope_sections.length)];
            var downslope_sections = _.map(
              _.filter(geometry.slope_sections, function(section, i) {
                return i > road_index;
              }),
              function(section, i) {
                return mustacheify(section, upslope_sections.length + 1 + i);
              }
            );
            all_sections = upslope_sections.concat(road_section).
                concat(downslope_sections);
          } else {
            all_sections = _.map(geometry.slope_sections, function(section, i) {
              return mustacheify(section, i);
            });
            upslope_sections = all_sections;
          }
          $(this.form_id).html($.mustache(this.template, {
            upslope_sections: upslope_sections,
            downslope_sections: downslope_sections,
            road_section: road_section,
            all_sections: all_sections,
            road: geometry.road.slope_section && geometry.road
          }));
        } catch(e) {
          console.log("Could not render geometry form: " + e);
          return;
        }
      },
      render: function() {
        if (this.hidden) {
          return;
        }
        var that = this;
        $(this.form_id).remove();
        $(this.divname).html('<div id="' + this.form_id.slice(1) + '"></div>');

        this.render_tip();
        this.render_form();

        $(".road_shoulder").change(function(event) {
          var id = event.target.id;
          var value = parseFloat(event.target.value);
          if (!Mossaic.utils.validate_number(value,
              "Please enter a non-negative number for road shoulder",
              event.target,
              [function(val) { return val >= 0; }])) {
            return;
          }
          var geometry = _.clone(that.model.get("geometry"));
          var road = _.clone(geometry.road);
          var attr_to_change = _.clone(road[id]);
          attr_to_change.value = value;
          road[id] = attr_to_change;
          geometry.road = road;
          that.model.changed_by = that;
          that.model.set({geometry: geometry});
        });

        $(this.form_id + " .prev").click(prev);
        $(this.form_id + " .next").click(this.finish);
        $(this.divname + " input:enabled:first").focus();
      }
    });

    /**
     * A composite view made up of the SpecifySections, SetDatum and
     * EditSections views
     * @constructor
     * @extends WorkflowForm
     */
    var EditProfile = WorkflowForm.extend({
      initialize: function(model, options) {
        WorkflowForm.prototype.initialize.apply(this, [model, options]);
        this.form_id = this.divname + "_subform";
        var that = this;
        var subform_options;
        this.index = 0;
        subform_options = {
          divname: this.form_id,
          next: function() {
            if (that.index + 1 < that.subforms.length) {
              that.subforms[that.index].hide();
              that.subforms[++that.index].show();
            } else {
              next();
            }
          }
        };
        this.subforms = [
          new SpecifySections(this.model, subform_options),
          new SetDatum(this.model, subform_options),
          new EditSections(this.model, subform_options)
        ];
        this.overspill_form = new Mossaic.forms.Mesh(this.model, {
          divname: overspill_div
        });
      },
      template: Mossaic.db.ddoc.templates.slope_profile,
      set_geometry: function(model) {
        this.model = model;
        _.each(this.subforms, function(form) {
          form.set_geometry(model);
        });
        this.overspill_form.set_model(model);
      },
      hide: function() {
        this.subforms[this.index].hide();
        this.overspill_form.hide();
        $(this.form_id).remove();
        this.hidden = true;
      },
      show: function(options) {
        var options = options || {};
        this.hidden = false;
        this.render();
        if (!this.model.is_dirty) {
          this.index = this.subforms.length - 1;
        } else if (typeof(options.number_of_sections) !== "undefined" ||
            typeof(options.upslope_sections) !== "undefined" ||
            typeof(options.downslope_sections) !== "undefined") {
          this.index = 0;    
        }
        this.subforms[this.index].show(options);
        this.overspill_form.show();
      },
      render: function() {
        $(this.divname).html($.mustache(this.template, {
          form_id: this.form_id.split("#")[1]
        }));
      }
    });

    /**
     * View for specifying the water table - basically just a wrapper around
     * Mossaic.forms.WaterTable
     * @constructor
     * @extends WorkflowForm
     */
    var DrawWaterTable = WorkflowForm.extend({
      initialize: function(model, options) {
        WorkflowForm.prototype.initialize.apply(this, [model, options]);
        this.form_id = this.divname + "_draw_water_table";
      },
      template: Mossaic.db.ddoc.templates.draw_water_table,
      set_geometry: function(model) {
        this.model = model;
        if (typeof(this.subform) !== "undefined") {
          this.subform.set_geometry(model);
        }
        if (typeof(this.overspill_form) !== "undefined") {
          this.overspill_form.set_model(model);
        }
      },
      hide: function() {
        $(this.form_id).remove();
        this.overspill_form.hide();
        this.model.editable.water_table = false;
        this.hidden = true;
      },
      show: function(options) {
        this.hidden = false;
        this.model.editable.water_table = true;
        this.render();
      },
      finish: function() {
        var that = this;
        this.next({model_to_save: this.model});
      },
      render_tip: function() {
        $(tip_div).html("Draw the water table");
      },
      render_form: function() {
        var subform_id = this.form_id.slice(1) + "_subform";
        $(this.form_id).html($.mustache(this.template, {
            subform_id: subform_id
        }));
        if (typeof(this.subform) === "undefined") {
          this.subform = new Mossaic.forms.WaterTable(this.model, {
              divname: "#" + subform_id
          });
        }
        this.subform.show();
        if (typeof(this.overspill_form) === "undefined") {
          this.overspill_form = new Mossaic.forms.Mesh(this.model, {
            divname: overspill_div
          });
        }
        this.overspill_form.show();
      },
      render: function() {
        if (this.hidden) {
          return;
        }
        $(this.form_id).remove();
        $(this.divname).html('<div id="' + this.form_id.slice(1) + '"></div>');

        this.render_tip();
        this.render_form();

        $(this.form_id + " .prev").click(prev);
        $(this.form_id + " .next").click(this.finish);
        $(this.divname + " input:enabled:first").focus();
      }
    });

    /**
     * View for specifying soil strata depths - a wrapper for
     * Mossaic.forms.SoilDepths
     * @constructor
     * @extends WorkflowForm
     */
    var DrawSoilStrata = WorkflowForm.extend({
      initialize: function(model, options) {
        WorkflowForm.prototype.initialize.apply(this, [model, options]);
        this.form_id = this.divname + "_draw_soil_strata_table";
      },
      set_geometry: function(geometry) {
        this.model = geometry;
        if (typeof(this.subform) !== "undefined") {
          this.subform.set_geometry(geometry);
          if (typeof(geometry.layers) !== "undefined") {
            this.subform.set_layers(geometry.layers);
          }
        }
        if (typeof(this.overspill_form) !== "undefined") {
          this.overspill_form.set_model(geometry);
        }
      },
      template: Mossaic.db.ddoc.templates.draw_soil_strata,
      hide: function() {
        $(this.form_id).remove();
        this.overspill_form.hide();
        this.model.editable.soil_strata = false;
        this.hidden = true;
      },
      show: function(options) {
        this.hidden = false;
        this.model.editable.soil_strata = true;
        this.render();
      },
      finish: function() {
        var that = this;
        this.next({model_to_save: this.model});
      },
      render_tip: function() {
        $(tip_div).html("Draw the soil strata");
      },
      render_form: function() {
        var subform_id = this.form_id.slice(1) + "_subform";
        $(this.form_id).html($.mustache(this.template, {
            subform_id: subform_id
        }));
        if (typeof(this.subform) === "undefined") {
          this.subform = new Mossaic.forms.SoilDepths(this.model, {
              divname: "#" + subform_id,
              geometry: this.model,
              layers: this.model.layers,
              stochastic: false
          });
        }
        this.subform.show();
        if (typeof(this.overspill_form) === "undefined") {
          this.overspill_form = new Mossaic.forms.Mesh(this.model, {
            divname: overspill_div
          });
        }
        this.overspill_form.show();
      },
      render: function() {
        var that = this;
        if (this.hidden) {
          return;
        }
        $(this.form_id).remove();
        $(this.divname).html('<div id="' + this.form_id.slice(1) + '"></div>');

        this.render_tip();
        this.render_form();

        $(this.form_id + " .add_new_layer").click(function() {
          // If no layers, we'll need to add an extra one
          if (that.model.layers.size() == 0) {
            that.model.layers.add(new Backbone.Model({}), {silent: true});
          }
          that.model.layers.changed_by = that;
          that.model.layers.add(new Backbone.Model({}));
        });
        $(this.form_id + " .prev").click(prev);
        $(this.form_id + " .next").click(this.finish);
        $(this.divname + " input:enabled:first").focus();
      }
    });

    /**
     * View for specifying soil types - a wrapper for Mossaic.forms.SoilsData
     * @constructor
     * @extends WorkflowForm
     */
    var SpecifySoilTypes = WorkflowForm.extend({
      initialize: function(model, options) {
        WorkflowForm.prototype.initialize.apply(this, [model, options]);
        this.form_id = this.divname + "_specify_soil_types_table";
        this.soils = options.soils;
      },
      template: Mossaic.db.ddoc.templates.specify_soil_types,
      set_soils: function(model) {
        this.soils = model;
        if (typeof(this.subform) !== "undefined") {
          this.subform.set_soils(model);
        }
      },
      set_geometry: function(geometry) {
        this.model = geometry;
        if (typeof(this.subform) !== "undefined" &&
            typeof(geometry.layers) !== "undefined") {
          this.subform.set_layers(geometry.layers);
        }
      },
      hide: function() {
        $(this.form_id).remove();
        this.model.editable.soil_strata = false;
        this.hidden = true;
        if (typeof(browsers.soils) !== "undefined") {
          browsers.soils.hide();
        }
      },
      show: function(options) {
        this.hidden = false;
        this.render();
        if (typeof(browsers.soils) !== "undefined") {
          browsers.soils.reload_view();
        }
      },
      finish: function() {
        var that = this;
        if (!this.soils.is_dirty) {
          this.next();
        } else {
          alert(UNSAVED_MESSAGE);
        }
      },
      render_tip: function() {
        $(tip_div).html("Specify soil types");
      },
      render_form: function() {
        var subform_id = this.form_id.slice(1) + "_subform";
        $(this.form_id).html($.mustache(this.template, {
            subform_id: subform_id
        }));
        if (typeof(this.subform) === "undefined") {
          this.subform = new Mossaic.forms.SoilsData(this.model.layers, {
              divname: "#" + subform_id,
              soils: this.soils,
              stochastic: false,
              fixed_layers: true,
              mode: this.model.mode
          });
        }
        this.subform.show({mode: this.model.mode});
      },
      render: function() {
        var that = this;
        if (this.hidden) {
          return;
        }
        $(this.form_id).remove();
        $(this.divname).html('<div id="' + this.form_id.slice(1) + '"></div>');

        this.render_tip();
        this.render_form();

        $(this.form_id + " .prev").click(prev);
        $(this.form_id + " .next").click(this.finish);
        $(this.divname + " input:enabled:first").focus();
      }
    });

    /**
     * View for specifying stability search grid - a wrapper for
     * Mossaic.forms.Stability
     * @constructor
     * @extends WorkflowForm
     */
    var SpecifyStability = WorkflowForm.extend({
      initialize: function(model, options) {
        WorkflowForm.prototype.initialize.apply(this, [model, options]);
        this.form_id = this.divname + "_specify_stability";
      },
      template: Mossaic.db.ddoc.templates.specify_soil_types,
      set_stability: function(model) {
        this.model = model;
        if (typeof(this.subform) !== "undefined") {
          this.subform.set_model(model);
        }
      },
      hide: function() {
        $(this.form_id).remove();
        this.model.editable = false;
        this.hidden = true;
        if (typeof(browsers.stability) !== "undefined") {
          browsers.stability.hide();
        }
      },
      show: function(options) {
        this.hidden = false;
        this.model.editable = true;
        this.render();
        if (typeof(browsers.stability) !== "undefined") {
          browsers.stability.show();
        }
      },
      finish: function() {
        var that = this;
        if (!this.model.is_dirty) {
          this.next();
        } else {
          alert(UNSAVED_MESSAGE);
        }
      },
      render_tip: function() {
        $(tip_div).html("Specify stability search");
      },
      render_form: function() {
        var subform_id = this.form_id.slice(1) + "_subform";
        $(this.form_id).html($.mustache(this.template, {
            subform_id: subform_id
        }));
        if (typeof(this.subform) === "undefined") {
          this.subform = new Mossaic.forms.Stability(this.model, {
              divname: "#" + subform_id,
              parent: parent
          });
        }
        this.subform.show();
      },
      render: function() {
        var that = this;
        if (this.hidden) {
          return;
        }
        $(this.form_id).remove();
        $(this.divname).html('<div id="' + this.form_id.slice(1) + '"></div>');

        this.render_tip();
        this.render_form();

        $(this.form_id + " .prev").click(prev);
        $(this.form_id + " .next").click(this.finish);
        $(this.divname + " input:enabled:first").focus();
      }
    });

    /**
     * View for specifying boundary conditions data - a wrapper for
     * Mossaic.forms.BoundaryConditions
     * @constructor
     * @extends WorkflowForm
     */
    var SpecifyBoundaryConditions = WorkflowForm.extend({
      initialize: function(model, options) {
        WorkflowForm.prototype.initialize.apply(this, [model, options]);
        this.form_id = this.divname + "_specify_boundary_conditions";
      },
      template: Mossaic.db.ddoc.templates.specify_boundary_conditions,
      set_boundary_conditions: function(model) {
        this.model = model;
        if (typeof(this.subform) !== "undefined") {
          this.subform.set_model(model);
        }
      },
      hide: function() {
        $(this.form_id).remove();
        this.model.editable = false;
        this.hidden = true;
        if (typeof(browsers.boundary_conditions) !== "undefined") {
          browsers.boundary_conditions.hide();
        }
      },
      show: function(options) {
        this.hidden = false;
        this.model.editable = true;
        this.render();
        if (typeof(browsers.boundary_conditions) !== "undefined") {
          browsers.boundary_conditions.show();
        }
      },
      finish: function() {
        var that = this;
        if (!this.model.is_dirty) {
          this.next();
        } else {
          alert(UNSAVED_MESSAGE);
        }
      },
      render_tip: function() {
        $(tip_div).html("Specify boundary conditions");
      },
      render_form: function() {
        var subform_id = this.form_id.slice(1) + "_subform";
        $(this.form_id).html($.mustache(this.template, {
            subform_id: subform_id
        }));
        if (typeof(this.subform) === "undefined") {
          this.subform = new Mossaic.forms.Rainfall(this.model, {
              divname: "#" + subform_id,
              parent: parent
          });
        }
        this.subform.show();
      },
      render: function() {
        var that = this;
        if (this.hidden) {
          return;
        }
        $(this.form_id).remove();
        $(this.divname).html('<div id="' + this.form_id.slice(1) + '"></div>');

        this.render_tip();
        this.render_form();

        $(this.form_id + " .prev").click(prev);
        $(this.form_id + " .next").click(this.finish);
        $(this.divname + " input:enabled:first").focus();
      }
    });

    /**
     * View for specifying engineering cost - a wrapper for
     * Mossaic.forms.EngeineeringCost
     * @constructor
     * @extends WorkflowForm
     */
    var SpecifyEngineeringCost = WorkflowForm.extend({
      initialize: function(model, options) {
        WorkflowForm.prototype.initialize.apply(this, [model, options]);
        this.form_id = this.divname + "_specify_engineering_cost";
      },
      template: Mossaic.db.ddoc.templates.specify_engineering_cost,
      set_engineering_cost: function(model) {
        this.model = model;
        if (typeof(this.subform) !== "undefined") {
          this.subform.set_model(model);
        }
      },
      hide: function() {
        $(this.form_id).remove();
        this.model.editable = false;
        this.hidden = true;
        if (typeof(browsers.engineering_cost) !== "undefined") {
          browsers.engineering_cost.hide();
        }
      },
      show: function(options) {
        this.hidden = false;
        this.model.editable = true;
        this.render();
        if (typeof(browsers.engineering_cost) !== "undefined") {
          browsers.engineering_cost.show();
        }
      },
      finish: function() {
        var that = this;
        if (!this.model.is_dirty) {
          this.next();
        } else {
          alert(UNSAVED_MESSAGE);
        }
      },
      render_tip: function() {
        $(tip_div).html("Specify engineering costs");
      },
      render_form: function() {
        var subform_id = this.form_id.slice(1) + "_subform";
        $(this.form_id).html($.mustache(this.template, {
            subform_id: subform_id
        }));
        if (typeof(this.subform) === "undefined") {
          this.subform = new Mossaic.forms.EngineeringCost(this.model, {
              divname: "#" + subform_id,
              parent: parent
          });
        }
        this.subform.show();
      },
      render: function() {
        var that = this;
        if (this.hidden) {
          return;
        }
        $(this.form_id).remove();
        $(this.divname).html('<div id="' + this.form_id.slice(1) + '"></div>');

        this.render_tip();
        this.render_form();

        $(this.form_id + " .prev").click(prev);
        $(this.form_id + " .next").click(this.finish);
        $(this.divname + " input:enabled:first").focus();
      }
    });

    /**
     * View for specifying road data
     * Not yet implemented
     * @constructor
     * @extends WorkflowForm
     */
    var SpecifyRoadNetwork = WorkflowForm.extend({
      initialize: function(model, options) {
        WorkflowForm.prototype.initialize.apply(this, [model, options]);
        this.form_id = this.divname + "_specify_road_network";
      },
      template: Mossaic.db.ddoc.templates.specify_road_network,
      set_road_network: function(model) {
        this.model = model;
      },
      hide: function() {
        $(this.form_id).remove();
        this.model.editable = false;
        this.hidden = true;
        if (typeof(browsers.road_network) !== "undefined") {
          browsers.road_network.hide();
        }
      },
      show: function(options) {
        this.hidden = false;
        this.model.editable = true;
        this.render();
        if (typeof(browsers.road_network) !== "undefined") {
          browsers.road_network.show();
        }
      },
      finish: function() {
        var that = this;
        if (!this.model.is_dirty) {
          this.next();
        } else {
          alert(UNSAVED_MESSAGE);
        }
      },
      render_tip: function() {
        $(tip_div).html("Select road network data");
      },
      render_form: function() {
        $(this.form_id).html($.mustache(this.template));
      },
      render: function() {
        var that = this;
        if (this.hidden) {
          return;
        }
        $(this.form_id).remove();
        $(this.divname).html('<div id="' + this.form_id.slice(1) + '"></div>');

        this.render_tip();
        this.render_form();

        $(this.form_id + " .prev").click(prev);
        $(this.form_id + " .next").click(this.finish);
        $(this.divname + " input:enabled:first").focus();
      }
    });

    views_by_type = {
      edit_profile: new EditProfile(model, {
        divname: divname
      }),
      draw_soil_strata: new DrawSoilStrata(model, {
        divname: divname
      }),
      specify_soil_types: new SpecifySoilTypes(model, {
        divname: divname,
        soils: soils
      }),
      draw_water_table: new DrawWaterTable(model, {
        divname: divname,
        next: save_geometry
      }),
      specify_stability: new SpecifyStability(stability, {
        divname: divname
      }),
      specify_boundary_conditions: new SpecifyBoundaryConditions(
        boundary_conditions, {
          divname: divname
        }
      ),
      specify_engineering_cost: new SpecifyEngineeringCost(engineering_cost, {
        divname: divname
      }),
      specify_road_network: new SpecifyRoadNetwork(road_network, {
        divname: divname
      })
    }

    chasm_views = [{
        view: views_by_type.edit_profile,
        name: "Slope profile"
      },
      {
        view: views_by_type.draw_soil_strata,
        name: "Soil strata"
      },
      {
        view: views_by_type.draw_water_table,
        name: "Water table"
      },
      {
        view: views_by_type.specify_soil_types,
        name: "Soil types"
      },
      {
        view: views_by_type.specify_boundary_conditions,
        name: "Boundary conditions"
      },
      {
        view: views_by_type.specify_stability,
        name: "Stability"
      }
    ];

    questa_views = [{
        view: views_by_type.specify_engineering_cost,
        name: "Eng costs"
      },
      {
        view: views_by_type.specify_road_network,
        name: "Road network"
      }
    ];
    if (model.mode === "questa") {
      views = chasm_views.concat(questa_views);
    } else {
      views = chasm_views;
    }

    tabs = Mossaic.widgets.tabs({
      divname: "#mossaic_tabs",
      tab_data: views,
      on_select: function(index) {
        route_to(index);
      },
      save_geometry: save_geometry,
      model: model
    });
    tabs.render();

    start({
      number_of_sections: number_of_sections,
      upslope_sections: upslope_sections,
      downslope_sections: downslope_sections
    });

    return {
      next: next,
      prev: prev,
      show: show,
      /**
       * Set the stability model for the workflow
       *
       * @memberof Mossaic.workflow.slope
       * @instance
       * @param {Mossaic.models.Stability} stability_new Stability model
       */
      set_stability: function(stability_new) {
        views_by_type.specify_stability.set_stability(stability_new);
      },
      /**
       * Set the boundary conditions model for the workflow
       *
       * @memberof Mossaic.workflow.slope
       * @instance
       * @param {Mossaic.models.BoundaryConditions} boundary_conditions_new
       *                                              Boundary conditions model
       */
      set_boundary_conditions: function(boundary_conditions_new) {
        views_by_type.specify_boundary_conditions.set_boundary_conditions(boundary_conditions_new);
      },
      /**
       * Set the soils model for the workflow
       *
       * @memberof Mossaic.workflow.slope
       * @instance
       * @param {Mossaic.models.Soils} soils_new Soils data model
       */
      set_soils: function(soils_new) {
        views_by_type.specify_soil_types.set_soils(soils_new);
      },
      /**
       * Set the engineering cost model for the workflow
       *
       * @memberof Mossaic.workflow.slope
       * @instance
       * @param {Mossaic.models.EngineeringCost} engineering_cost Engineering
       *                                                          cost model
       */
      set_engineering_cost: function(engineering_cost) {
        views_by_type.specify_engineering_cost.set_engineering_cost(engineering_cost);
      },
      /**
       * Set the road network model for the workflow
       *
       * @memberof Mossaic.workflow.slope
       * @instance
       * @param {Mossaic.models.RoadNetwork} road_network Road network model
       */
      set_road_network: function(road_network) {
        views_by_type.specify_road_network.set_road_network(road_network);
      },
      /**
       * Set the geometruy model for the workflow
       *
       * @memberof Mossaic.workflow.slope
       * @instance
       * @param {Mossaic.models.Geometry} geometry_new Geometry model
       */
      set_geometry: function(geometry_new) {
        views_by_type.edit_profile.set_geometry(geometry_new);
        views_by_type.draw_soil_strata.set_geometry(geometry_new),
        views_by_type.specify_soil_types.set_geometry(geometry_new)
        views_by_type.draw_water_table.set_geometry(geometry_new)
        if (geometry_new.mode === "chasm") {
          views = chasm_views;
        } else if (geometry_new.mode === "questa") {
          views = chasm_views.concat(questa_views);
        }
        tabs.update_tab_data(views);
        tabs.set_model(geometry_new);
      }
    };
  };
})();