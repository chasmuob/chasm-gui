/**
 * @file Defines a task_analyser widget, which is a composition of the
 *       fos_analyser and slope_cells_analyser widgets with selection tabs
 */
(function() {
  /**
   * Create a task_analyser - a composition of the fos_analyser and
   * slope_cells_analyser widgets with tabs to select between the two.
   *
   * WARNING: This is implemented using the module pattern so should not be
   * called with the "new" keyword
   *
   * @constructor
   * @param {object} options Hash of options
   * @param {string} options.divname CSS selector of the div to append to
   * @param {Mossaic.widgets.ls} options.parent ls widget that sets the parent
   *                                            dataset most likely a Task
   * @param {Mossaic.widgets.ls} options.jobs_browser ls widget that sets the
   *                                                  job dataset (only used by
   *                                                  the slope_cells_analyser)
   */
  Mossaic.widgets.task_analyser = function(options) {
    var analysers_to_use = [
      {
        name: "Factor of safety",
        id: "fos_analyser",
        widget: Mossaic.widgets.fos_analyser,
        tab_class: "tab_selected"
      },
      {
        name: "Slope cells",
        id: "slope_cells_analyser",
        widget: Mossaic.widgets.slope_cells_analyser,
        tab_class: "tab_unselected"
      }
    ];
    var divname = options.divname;
    var child_div = divname + "_child";
    var analyser_options = {
      parent: options.parent,
      divname: child_div,
      active: false,
      jobs_browser: options.jobs_browser
    };
    var analysers = {};
    var active_analyser;
    /**
     * Simple tab view for switching between fos_analyser and
     * slope_cells_analyser
     */
    var View = Backbone.View.extend({
      template: '<div id={{child_div}}></div>\
        <div class="tabs"><table class="tabs_table"><tr>\
        {{#analysers}}<td id="{{id}}" class="analyser_select tab {{tab_class}}">\
        {{name}}</td>{{/analysers}}\
        </tr></table></div>',
      initialize: function(collection, options) {
        _.bindAll(this);
      },
      render: function() {
        var that = this;
        $(this.el).html($.mustache(this.template, {
          analysers: analysers_to_use,
          child_div: child_div.split("#")[1]
        }));
        var selected_tab = d3.select(this.el).select(".tab_selected").attr("id");
        d3.selectAll(".analyser_select")
          .attr("width", 100 / analysers_to_use.length + "%")
          .on("click", function(event) {
            $("#" + selected_tab).removeClass("tab_selected");
            $("#" + selected_tab).addClass("tab_unselected");
            _.find(analysers_to_use, function(analyser) {
              return analyser.id === selected_tab;
            }).tab_class = "tab_unselected";
            selected_tab = d3.event.target.id;
            $("#" + selected_tab).removeClass("tab_unselected");
            $("#" + selected_tab).addClass("tab_selected");
            _.find(analysers_to_use, function(analyser) {
              return analyser.id === selected_tab;
            }).tab_class = "tab_selected";
            that.set_active_analyser(d3.event);
          });
      },
      set_active_analyser: function(event) {
        var analyser_to_activate = event.target.id;
        active_analyser.set_active(false);
        active_analyser.show();
        active_analyser = analysers[analyser_to_activate];
        active_analyser.set_active(true);
        active_analyser.show();
      }
    });
    var view = new View({el: divname});
    _.each(analysers_to_use, function(analyser, i) {
      var opts = _.clone(analyser_options);
      if (i == 0) {
        opts.active = true;
      }
      analysers[analyser.id] = analyser.widget(opts);
    });
    active_analyser = analysers[analysers_to_use[0].id];
    return {
      /**
       * Reset the widget
       * @memberof Mossaic.widgets.task_analyser
       * @instance
       */
      reset: function() {
        view.el = divname;
        view.render();
        active_analyser.reset();
      },
      /**
       * Show the widget
       * @memberof Mossaic.widgets.task_analyser
       * @instance
       */
      show: function() {
        view.el = divname;
        view.render();
        active_analyser.show();
      }
    };
  };
})();