/**
 * @file Creates the Mossaic.db namespace and initialises Backbone.couch
 *       connector
 */
(function() {
  /**
   * Provides convenience functions for interacting with CouchDB backend
   * @namespace
   */
  Mossaic.db = {};

  /**
   * Get the full database name for one of four databases that the app
   * interacts with.
   * @function
   * @param {object} options
   * @param {string} options.type Either "task", "release" or "state" for one of
   *                              three specific DBs, or undefined for the DB
   *                              that served the app.
   * @return {string} Full name of the specified CouchDB database
   */
  Mossaic.db.get_db_name = function(options) {
    var type = options && options.type || undefined;
    var base_db_name = unescape(document.location.href).split('/')[3];
    if (typeof(type) === "undefined") {
      return base_db_name;
    } else if (type === "task" || type === "release" || type === "state") {
      return [base_db_name, type].join("_");
    } else {
      console.log("Unrecognised DB type: " + type);
      return undefined;
    }
  };

  /**
   * Set the full name of the app database in Backbone.couch
   * @type string
   */
  Backbone.couch.databaseName = Mossaic.db.get_db_name();
  /**
   * Set the name of the CouchDB design document in Backbone.couch
   * @type string
   */
  Backbone.couch.ddocName = "gui";

  var db = Backbone.couch.db();
  var ddoc_uri = "_design/" + Backbone.couch.ddocName;
  db.getDbProperty(ddoc_uri, {
    success : function(doc) {
      /**
       * JS representation of the application design document
       * @type object
       */
      Mossaic.db.ddoc = doc;
    },
    error : function() {
      console.log("Could not fetch design document", ddoc_uri);
    }
  });
})();