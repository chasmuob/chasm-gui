/**
 * @file The top-level file for the application and the only script that needs
 *       to be included. It sets up the top-level namespace and loads the
 *       scripts that build the child namespaces.
 */
(function($) {
  var scripts_to_load = [
      "vendor/couchapp/jquery.couch.js",
      "vendor/couchapp/jquery.form.js",
      "vendor/couchapp/jquery.couchLogin.js",
      "vendor/d3/d3.min.js",
      "vendor/d3/d3.layout.min.js",
      "vendor/backbone/underscore.js",
      "vendor/backbone/backbone.js",
      "vendor/backbone/backbone.couch.js",
      "vendor/mustache/jquery.mustache.js",
      "lib/mossaic.utils.js",
      "lib/mossaic.models.js",
      "lib/mossaic.workflow.js",
      "lib/mossaic.db.js",
      "lib/mossaic.widgets.js",
      "lib/mossaic.forms.js",
      "lib/mossaic.vis.js",
      "lib/mossaic.editors.js",
      "lib/mossaic.router.js"
    ],
    load_scripts = function(scripts) {
      for (var i=0; i < scripts.length; i++) {
        document.write('<script src="'+scripts[i]+'"><\/script>');
      }
    };

  /**
   * The top level namespace for the MoSSaiC Application Platform GUI
   * @namespace
   */
  Mossaic = {};

  load_scripts(scripts_to_load);
})(jQuery);