/**
 * @file Define the form for displaying a Mossaic.models.Soils model.
 */
(function() {
  /**
   * Backbone View for Mossaic.models.Soils which displays values on a form.
   *
   * This View has a reference to layers, a Backbone Collection which
   * is usually bound to a Mossaic.models.Geometry. This is used to determine
   * how many soils to show on the form.
   *
   * This is a legacy view which builds the form using d3.js rather than the
   * more sensible jQuery/templating approach.
   * @constructor
   * @extends Mossaic.forms.Basic
   */
  Mossaic.forms.SoilsData = Mossaic.forms.Basic.extend({
    /**
     * Initialize the form.
     * Called implicitly with "new Mossaic.forms.SoilsData".
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {Backbone.Collection} collection Collection of objects
     *                                         representing soil strata layers
     * @param {object} options Hash of options
     * @param {string} options.divname jQuery selector for div to which this
     *                                 form should be appended
     * @param {Mossaic.models.Soils} options.soils Model to be represented
     *                                             by this form
     * @param {string} options.mode Soils can be rendered in either "chasm" or
     *                              "questa" modes. The questa mode causes an
     *                              extra soil type to be displayed, which will
     *                              be used by questa for the road cells.
     * @param {boolean} options.stochastic Display form elements for editing
     *                                     mean/standard deviation of certain
     *                                     parameters, as opposed to just value.
     * @param {boolean} options.fixed_layers Setting this to false will provide
     *                                       elements to add/remove soil strata
     *                                       interfaces in the collection
     */
    initialize: function(collection, options) {
      _.bindAll(this);
      this.collection = collection;
      this.collection.bind("add", this.handle_change);
      this.collection.bind("change", this.handle_change);
      this.collection.bind("remove", this.handle_change);
      this.collection.bind("reset", this.handle_change);
      this.divname = options.divname || "#soils_data_form";
      this.fixed_layers = options.fixed_layers;
      this.form_id = this.divname + "_soils_data";
      this.mode = options.mode;
      this.soils = options.soils;
      this.soils.bind("change", this.handle_change);
      if (typeof(options.stochastic) !== "undefined") {
        this.stochastic = options.stochastic;
      } else {
        this.stochastic = this.soils.is_stochastic();
      }
    },
    /**
     * Set the layers collection to be represented by the form.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {Backbone.Collection} layers Collection of soil strata interfaces
     */
    set_layers: function(layers) {
      this.collection.unbind("add");
      this.collection.unbind("change");
      this.collection.unbind("remove");
      this.collection.unbind("reset");
      this.collection = layers;
      this.collection.bind("add", this.handle_change);
      this.collection.bind("change", this.handle_change);
      this.collection.bind("remove", this.handle_change);
      this.collection.bind("reset", this.handle_change);
    },
    /**
     * Set the Mossaic.forms.SoilsData model to be represented by the form.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {Mossaic.models.Soils} soils Model to be represented
     *                                     by this form
     */
    set_soils: function(soils) {
      if (typeof(this.soils) !== "undefined" &&
          typeof(this.soils.unbind) === "function") {
        this.soils.unbind("change");
      }
      this.soils = soils;
      this.soils.bind("change", this.handle_change);
    },
    /**
     * Show the form, optionally setting the mode to either "chasm" or "questa"
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {object} options Hash of options
     * @param {string} options.mode Set to "chasm" to display one soil for each
     *                              soil layer. Set to "questa" to display an
     *                              additional soil, which is used by the questa
     *                              model for the road.
     */
    show: function(options) {
      var options = options || {};
      if (options.mode) {
        this.mode = options.mode;
      }
      Mossaic.forms.Basic.prototype.show.apply(this);
      $(this.form_id + " input:enabled:first").focus();
    },
    /**
     * If form is stochastic, make it deterministic. If it's deterministic,
     * make it stochastic.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     */
    flip_stochastic: function() {
      var that = this;
      this.stochastic = !this.stochastic;
      var to_convert = ["Ksat", "saturated_moisture_content",
          "saturated_bulk_density", "unsaturated_bulk_density",
          "effective_angle_of_internal_friction", "effective_cohesion"];
      var soils = _.clone(this.soils.get("soils"));
      _.each(soils, function(soil_ref, i) {
        var soil = _.clone(soil_ref);
        if (that.stochastic) {
          // For stochastic forms, value becomes mean and standard deviation is
          // 0
          _.each(to_convert, function(field) {
            var parameter = _.clone(soil[field]);
            parameter.mean = parameter.value;
            parameter.standard_deviation = 0;
            delete parameter.value;
            soil[field] = parameter;
          });
        } else {
          // For deterministic forms, mean becomes value and sd is thrown away
          _.each(to_convert, function(field) {
            var parameter = _.clone(soil[field]);
            parameter.value = parameter.mean;
            delete parameter.mean;
            delete parameter.standard_deviation;
            soil[field] = parameter;
          });
        }
        soils[i] = soil;
      });
      this.soils.set({
        soils: soils
      });
      this.render();
    },
    /**
     * Update the model data.
     * The actual field to be updated is defined by the id of the event target.
     * The id format is "FIELDNAME_INDEX-SUBFIELD".
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    update_data: function(event) {
      var target = $(d3.event.target);
      var id = target.attr("id").split("-")[0].split("_");
      var field = id.slice(0, id.length - 1).join("_");
      if (this.stochastic) {
        var subfield = target.attr("id").split("-")[1];
      } else {
        var subfield = "value";
      }
      var layer = parseInt(id[id.length - 1]);
      var soil_index = this.collection.at(layer).get("soil_index");
      if (typeof(soil_index) === "undefined") {
        soil_index = layer;
      }
      var value = parseFloat(target.attr("value"));
      var soils = _.clone(this.soils.get("soils"));
      var soil = _.clone(soils[soil_index]);
      soil[field] = _.clone(soil[field]);
      soil[field][subfield] = value;
      soils[soil_index] = soil;
      this.soils.changed_by = this;
      this.soils.set({soils: soils});
    },
    /**
     * Build a row of soil data
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @private
     * @param {object} options Hash of options:
     * @param {string} options.label Label to be displayed
     * @param {String} options.field Used to make the input element id - must
     *                               correspond to a valid field in the model
     * @param {object} options.data Soils data to be rendered in the row
     * @param {d3.selection} options.parent d3 selection to which the row is to
     *                                      be added
     */
    build_row: function(options) {
      var options = options || {};
      var that = this;
      var label = options.label;
      var field = options.field;
      var soils_data = options.data;
      var parent = options.parent;
      var cols = options.cols;
      var tr = parent.append("xhtml:tr");
      var sigfigs = options.sigfigs || 3;
      tr.append("th")
        .style("min-width", "120px")
        .style("width", "35%")
        .append("xhtml.label")
          .html(label);
      _.each(_.range(cols), function(i) {
        var soil_index = i;
        if (typeof(soil_index) === "undefined") {
          soil_index = i;
        }
        var td = tr.append("xhtml:td");
        td.style("min-width", "100px");
        if (that.stochastic) {
          var input = td.append("xhtml:input");
          input
            .attr("id", field + "_" + i + "-mean")
            .attr("class", "input-thin")
            .on("change", that.update_data);
          var input_sd = td.append("xhtml:input");
          input_sd
            .attr("id", field + "_" + i + "-standard_deviation")
            .attr("class", "input-thin")
            .on("change", that.update_data);
          if (typeof(soil_index) !== "undefined") {
            input.attr("value",
                parseFloat(Mossaic.utils.round(
                    soils_data[soil_index][field].mean, sigfigs)));
            input_sd.attr("value",
                parseFloat(Mossaic.utils.round(
                    soils_data[soil_index][field].standard_deviation,sigfigs)));
          }
        } else {
          var input = td.append("xhtml:input");
          input
            .attr("id", field + "_" + i)
            .attr("class", "input-thin")
            .on("change", that.update_data);
          if (typeof(soil_index) !== "undefined") {
            input.attr("value",
                parseFloat(Mossaic.utils.round(
                    soils_data[soil_index][field].value, sigfigs)));
          } else {
            console.log("Undefined soil index " + i);
          }
        }
      });
    },
    /**
     * Remove layer of soil measurements from the soil strata layers.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     */
    remove_layer: function(event) {
      var target = $(d3.event.target);
      var id = target.attr("id").split("_");
      var layer = id[id.length - 1];
      var to_remove = this.collection.at(layer);
      this.collection.remove(to_remove);
    },
    /**
     * Add a new soil strata to the soil strata layers.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {jQuery.Event} event jQuery event triggered by the DOM
     * @param {object} options Hash of options to be passed to
     *                         collection.add
     */
    add_new_layer: function(event, options) {
      this.collection.changed_by = this;
      this.collection.add(new Backbone.Model({}), options);
    },
    /**
     * Return the default set of soil attributes, with default suction moisture
     * curve.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @private
     * @return {object} Object representing a default soil definition
     */
    get_blank_soil: function() {
      var blank_soil = {
        Ksat: {
          value: 0,
          unit: "ms-1"
        },
        saturated_moisture_content: {
          unit: "m3m-3",
          value: 0.41
        },
        saturated_bulk_density: {
          unit: "kNm-3",
          value: 20
        },
        unsaturated_bulk_density: {
          unit: "kNm-3",
          value: 18
        },
        effective_angle_of_internal_friction: {
          value: 0,
          unit: "deg"
        },
        effective_cohesion: {
          value: 0,
          unit: "kNm-2"
        },
        suction_moisture_curve: {
          moisture_content: [
            0.209,
            0.218,
            0.227,
            0.251,
            0.267,
            0.295,
            0.308,
            0.324,
            0.348,
            0.376,
            0.4,
            0.42
          ],
          suction: [
            -10,
            -8,
            -6,
            -4,
            -3,
            -2,
            -1.6,
            -1.2,
            -0.8,
            -0.4,
            -0.2,
            -0.1
          ]
        }
      };
      if (this.stochastic) {
        var keys = ["effective_cohesion",
            "effective_angle_of_internal_friction", "Ksat"];
        _.each(keys, function(key) {
          delete blank_soil[key].value;
          blank_soil[key].mean = 0;
          blank_soil[key].standard_deviation = 0;
        });
      }
      return blank_soil;
    },
    /**
     * Build the stochastic mean/sd labels.
     * Convenience function for building form.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @private
     * @params {object} options Hash of options
     * @parmas {d3.selection} options.parent d3 selection to which the labels
     *                               should be appended
     */
    build_stochastic_labels: function(options) {
      var options = options || {};
      var parent = options.parent;
      var cols = options.cols;
      var tr = parent.append("xhtml:tr");
      tr.append("xhtml:th");
      _.each(_.range(cols), function() {
        var td = tr.append("td");
        td.append("xhtml:label")
          .style("width", "50px")
          .style("float", "left")
          .html("Mean");
        td.append("xhtml:label")
          .style("width", "50px")
          .style("float", "left")
          .html("Sd");
      });
    },
    /**
     * Build row of labels to display soil number.
     * Convenience function for building form.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @private
     * @params {object} options Hash of options
     * @parmas {d3.selection} options.parent d3 selection to which the labels
     *                                       should be appended
     * @params {integer} options.cols Number of columns to be built for this row
     */
    build_label_row: function(options) {
      var that = this;
      var options = options || {};
      var parent = options.parent;
      var cols = options.cols;
      tr = parent.append("xhtml:tr");
      tr.append("xhtml:th");
      _.each(_.range(cols), function(col) {
        tr.append("td").html("Soil " + col);
      });
    },
    /**
     * Add soils if we have more layers than soil types
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @private
     * @params {object} options Hash of options
     * @params {integer} options.cols Number of columns to be built for this row
     */
    maybe_add_soils: function(options) {
      var options = options || {};
      var cols = options.cols;
      var soils_data = this.soils.get("soils");
      if (typeof(soils_data) === "undefined") {
        soils_data = [];
      }
      var number_of_layers = cols;
      var layer_with_highest_index = this.collection.max(function(model) {
        return model.get("soil_index");
      });
      var highest_index = (typeof(layer_with_highest_index) !== "undefined" &&
          layer_with_highest_index.get("soil_index")) || 0;
      var soils_required = ((number_of_layers > highest_index + 1) && 
          number_of_layers) || highest_index + 1;
      var soils_to_add = soils_required - soils_data.length;
      if (soils_to_add > 0) {
        for (var i = soils_to_add; i > 0; --i) {
          soils_data.push(this.get_blank_soil()); 
        }
        this.soils.set({soils: soils_data}, {silent: true});
      }
    },
    /**
     * Handle change events triggered by the model. Only redraw if the
     * model was changed by a different view. Will also trigger redraw if the
     * changed object is a collection, in which case the number of layers has
     * changed, so a redraw is required.
     * @memberof Mossaic.forms.SoilsData
     * @instance
     * @param {object} model Backbone.Model or Backbone.Collection that has
     *                       triggered the change
     */
    handle_change: function(model) {
      this.stochastic = this.soils.is_stochastic();
      if (typeof(model) !== "undefined" &&
          (model.changed_by != this || "collection" in model)) {
        this.render();
      }
    },
    /**
     * Render the form.
     * Will be called implicitly by show or handle_change so should not need
     * to be called directly.
     * @memberof Mossaic.forms.SoilDepths
     * @instance
     */
    render: function() {
      if (this.hidden) {
        return;
      }
      var that = this;
      var number_of_cols = this.collection.size();
      if (this.mode === "questa") {
        ++number_of_cols;
      }

      this.maybe_add_soils({cols: number_of_cols});

      d3.select(this.form_id).remove();

      var div = d3.select(this.divname)
        .append("xhtml:div")
        .attr("id", this.form_id.slice(1));

      var form = d3.select(this.form_id);
      var that = this;

      form.append("xhtml:input")
        .attr("id", "soil_form_stochastic")
        .attr("type", "checkbox")
        .attr("checked", this.stochastic && "checked" || null)
        .on("change", this.flip_stochastic);
      form.append("xhtml:label")
        .attr("for", "soil_form_stochastic")
        .html("Enable stochastic soils");

      if (!this.fixed_layers) {
        form.append("xhtml:button")
          .html("Add new layer")
          .attr("class", "add")
          .on("click", this.add_new_layer);
      }
      var table = form.append("xhtml:table");
      table.attr("class", "wide");

      this.build_label_row({
        parent: table,
        cols: number_of_cols
      });
      if (this.stochastic) {
        this.build_stochastic_labels({
          parent: table,
          cols: number_of_cols
        });
      }
      this.build_row({
        label: "Cohesion (kNm-2)",
        field: "effective_cohesion",
        data: this.soils.get("soils"),
        parent: table,
        cols: number_of_cols
      });
      this.build_row({
        label: "Angle of internal friction (deg)",
        field: "effective_angle_of_internal_friction",
        data: this.soils.get("soils"),
        parent: table,
        cols: number_of_cols
      });
      this.build_row({
        label: "Ksat (ms-1)",
        field: "Ksat",
        data: this.soils.get("soils"),
        parent: table,
        cols: number_of_cols,
        sigfigs: 8
      });
    }
  });
})();