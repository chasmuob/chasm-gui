fun({Doc}) ->
    case couch_util:get_value(<<"type">>, Doc) of
        undefined ->
           ok;
        Type when Type == <<"soils">> ->
           case couch_util:get_value(<<"soils">>, Doc) of
              undefined ->
                 ok;
              Soils when is_list(Soils) ->
                 Name = couch_util:get_value(<<"name">>, Doc),
                 Emit(length(Soils), Name);
              _ ->
                 ok
           end;
        _ ->
           ok
    end
end.