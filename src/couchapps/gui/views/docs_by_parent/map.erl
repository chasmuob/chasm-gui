fun({Doc}) ->
   {DocType, ParentField} = case couch_util:get_value(<<"type">>, Doc) of
      undefined ->
         {undefined, undefined};
      Type when Type == <<"results">>; Type == <<"job">> ->
         {Type, <<"task_id">>};
      Type ->
         {Type, <<"parent_id">>};
      _ ->
         {undefined, undefined}
   end,
   WellFormed = (DocType /= undefined) and (ParentField /= undefined),
   case WellFormed of
      true ->
         case couch_util:get_value(ParentField, Doc) of
            undefined ->
               ok;
            Parent ->
               case couch_util:get_value(<<"name">>, Doc) of
                  undefined ->
                     Emit([DocType, Parent], null);
                  Name ->
                     Emit([DocType, Parent], Name);
                  _ ->
                     ok
               end;
            _ ->
               ok
         end;
      _ ->
         ok
   end
end.