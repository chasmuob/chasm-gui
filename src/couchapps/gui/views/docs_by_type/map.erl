fun({Doc}) ->
   case couch_util:get_value(<<"type">>, Doc) of
      undefined ->
         ok;
      Type ->
         case couch_util:get_value(<<"name">>, Doc) of
            undefined ->
               Emit(Type, null);
            Name ->
               Emit(Type, Name);
            _ ->
               ok
         end;
      _ ->
         ok
   end
end.