#!/bin/bash

# Install MoSSaiC Application Platform on a single node and run all tests

usage="Usage: install.sh [OPTIONS]
    -p  Installation prefix
    -c  Certificate containing user grid certificate and key
    -v  Virtual organisation to be used for access to grid resources
    -s  Name of DIRAC setup to be used
    -u  URL of DIRAC configuration server
    -a  Email address of MAP administrator
    -l  Install 'lite' version (no dependencies apart from DIRAC)"

function print_usage {
    echo "$usage"
}

set -e  # Exit on any nonzero exit status

while getopts ":p:c:v:s:u:a:l" opt; do
    case $opt in
        p)
            prefix=$OPTARG
            ;;
        c)
            cert_dir=$OPTARG
            ;;
        v)
            vo=$OPTARG
            ;;
        s)
            dirac_setup=$OPTARG
            ;;
        u)
            config_server=$OPTARG
            ;;
        a)
            admin_email=$OPTARG
            ;;
        l)
            lite=true
            ;;
        \?)
            echo "Invalid option: -$OPTARG"
            print_usage
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument."
            print_usage
            exit 1
    esac
done

shift $(($OPTIND - 1))

configure_cmd="./configure"

if [ $prefix ];then configure_cmd="$configure_cmd -p $prefix";fi
if [ $cert_dir ];then configure_cmd="$configure_cmd -c $cert_dir";fi
if [ $vo ];then configure_cmd="$configure_cmd -v $vo";fi
if [ $dirac_setup ];then configure_cmd="$configure_cmd -s $dirac_setup";fi
if [ $config_server ];then configure_cmd="$configure_cmd -u $config_server";fi
if [ $admin_email ];then configure_cmd="$configure_cmd -a $admin_email";fi

$configure_cmd

if [ $lite ];then
    make lite
else
    make
fi
make check