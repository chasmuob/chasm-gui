MoSSaiC Application Platform Installer
====================================================

This Makefile installs Python 2.7.1, BigCouch (plus dependencies) and other project dependencies into a user-owned area of the filesystem.

Note: There should normally be no need to run this Makefile manually - it will be called by the master install script.

To Install:

 - Ensure the Makefile has been generated by running ./configure in the root of the MoSSaiC Application Platform distribution
 - Either:
  - Obtain the appropriate dependencies and either copy them where the Makefile expects to find them (see deps\_bigcouch.txt and deps\_main.txt)
  - Obtain the BigCouch "internal" dependencies. These would normally be automatically obtained using Git as part of the BigCouch build however the dependence on Git has been removed from this install process. The required dependencies are listed in the top level rebar.config file in the BigCouch source directory and are currently:
   - chttpd
   - fabric
   - ibrowse
   - mem3
   - mochiweb
   - oath
   - rexi
  - These should be copied into a directory named bigcouch-deps
 - Or:
  - Run the fetch dependency script: `sh fetch_dependencies.sh`
  - Manually verify the integrity of the downloaded dependencies
 - The Makefile will patch the rebar.config file so that the above dependencies are built on the local filesystem without using `git fetch`
 - `make install-dependencies`

By default this process installs the DIRAC client. If you want to skip the installation of the DIRAC client then `export SKIP_DIRAC_INSTALL=true`.

Before running any of the installed software it will be necessary to load the required environment variables with:

 - `source PREFIX/mossaicenv`

This will set `PATH` and `LD_LIBRARY_PATH` so that the software and libraries installed in `PREFIX` are found by the OS.

The contents of `PREFIX/bin` should then be runnable by any user who is a member of the primary group of the installing user. Note that once BigCouch is run as a user the files will be written to `PREFIX/var` as that user and you won't be able to run BigCouch as a different user without first clearing out the relevant files from `PREFIX/var`.