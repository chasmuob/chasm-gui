#!/bin/bash

# This script reads comma separated url,destination_dir pairs that define the location and target destination of each dependency.
# If the target directory does not already exist then the dependency is downloaded and unpacked into the target destination.

CUSTOM_DEPSFILE=$1
CUSTOM_DEPSDIR=$2
DEPSDIR="."
DEPSDIR_BIGCOUCH=$DEPSDIR/"bigcouch-deps"
DEPSFILE="deps_main.txt"
DEPSFILE_BIGCOUCH="deps_bigcouch.txt"
WGET_OUTPUT="wget.out"

function fetch_deps {
    deps_file=$1
    deps_dir=$2

    deps_to_ignore=`for dir in \`cat $deps_file | cut -d , -f 2\`;do ls $deps_dir | grep $dir$;done`

    for dep in `cat $deps_file`
    do
        dep_name=`echo $dep | cut -d , -f 2`
        echo $deps_to_ignore | grep $dep_name 1> /dev/null
        if [ $? -ne 0  ]; then
            url=`grep $dep $deps_file | cut -d , -f 1`
            echo Fetching $url...
            wget --no-check-certificate $url 2> $WGET_OUTPUT || { echo Could not fetch $dep_name $url; exit 1; }    # Note: wget bug means we need --no-check-certificate to work with github
            file=`cat $WGET_OUTPUT | grep "Saving to" | cut -d '\`' -f 2 | cut -d \' -f 1`                                # This rather kludgy line finds the filename that wget has downloaded
            file_borked=`echo $file | grep "Saving to"`
            if [ -n "$file_borked" ]
            then
                file=`cat $WGET_OUTPUT | grep "Saving to" | cut -d '“' -f 2 | cut -d '”' -f 1`
            fi
            rm $WGET_OUTPUT
            echo $file
            echo Unpacking $url...
            extension=`echo ${file##*.}`
            if [ $extension == 'zip' ]; then
                newdir=${file:0:${#file}-4}     # Return the filename without the .zip extension
                unzip $file && { mv $newdir $deps_dir/$dep_name; }
                rm $file
            elif [ $extension == 'egg' ] || [ $extension == 'dirac-install' ]; then
                mkdir -p $deps_dir/$dep_name
                mv $file $deps_dir/$dep_name
            else # Assume it's a tarball
                newdir=`tar tf $file | head -n 1`
                tar xvf $file && { mv $newdir $deps_dir/$dep_name; }
                rm $file
            fi
        else
            echo $dep_name already seems to be downloaded
        fi
    done
}

if [ $CUSTOM_DEPSFILE -a $CUSTOM_DEPSDIR ]; then
    fetch_deps $CUSTOM_DEPSFILE $CUSTOM_DEPSDIR
else
    fetch_deps $DEPSFILE $DEPSDIR
    mkdir -p $DEPSDIR_BIGCOUCH
    fetch_deps $DEPSFILE_BIGCOUCH $DEPSDIR_BIGCOUCH
fi