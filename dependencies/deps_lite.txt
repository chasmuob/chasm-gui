https://github.com/couchapp/couchapp/tarball/0.7.3,couchapp
http://httplib2.googlecode.com/files/httplib2-0.6.0.tar.gz,httplib2
http://www.python.org/ftp/python/2.7.1/Python-2.7.1.tgz,python
http://pypi.python.org/packages/2.7/s/setuptools/setuptools-0.6c11-py2.7.egg,setuptools
http://mock.googlecode.com/files/mock-0.7.2.zip,mock
http://pypi.python.org/packages/source/G/GSI/GSI-0.5.0.tar.gz,GSI-0.5.0
http://cern.ch/lhcbproject/dist/Dirac_project/dirac-install,DIRAC