#!/bin/bash

# A wrapper around the DIRAC installer which does things specific to the MoSSaiC Application Platform
#
# Usage: sh dirac-install-wrapper.sh DIRAC_SRC_DIR DIRAC_DEST_DIR DIRAC_VERSION
#
# Expects the following environment variables to be set:
#

set -e  # Exit on any nonzero exit status

if test ! $PREFIX
then
    echo Exiting: PREFIX environment variable not present
    exit 1
fi

DIRACSRC=$1
DIRACDIR=$2
DIRACVERSION=$3
GLOBUSDIR=~/.globus

cert_dir=$DIRAC_USER_CERT_PATH
vo=$DIRAC_VO
dirac_setup=$DIRAC_SETUP_NAME
config_server=$DIRAC_CONFIG_SERVER

mkdir -p $DIRACDIR
cp $DIRACSRC/* $DIRACDIR/
cd $DIRACDIR

echo "Running dirac-install script..."
chmod 755 dirac-install
./dirac-install -r $DIRACVERSION

echo "Configuring DIRAC..."

if [ $cert_dir ]; then
    mkdir -p $GLOBUSDIR
    cp $cert_dir/userkey.pem $GLOBUSDIR/
    chmod 600 $GLOBUSDIR/userkey.pem
    cp $cert_dir/usercert.pem $GLOBUSDIR/
    chmod 640 $GLOBUSDIR/usercert.pem
else
    echo "No directory provided, assuming key and cert are already located in ~/.globus."
fi

source $DIRACDIR/bashrc

if [ -z $vo ]; then
    vo="vo.landslides.mossaic.org"
fi

if [ -z $dirac_setup ]; then
    dirac_setup="MoSSaiC-Development"
fi

if [ -z $config_server ]; then
    echo "No configuration server specified - cannot continue configuring DIRAC client"
    exit 1
fi

dirac-configure -V $vo -S $dirac_setup -C $config_server

echo "DIRAC client successfully configured."

# Final trick is to symlink the DIRAC python dir into the mossaic lib/python/site-packages dir so we can use it with the mossaic python distro
echo "Creating symlink to DIRAC python modules in MoSSaiC python directory..."
rm -rf $PREFIX/lib/python2.7/site-packages/DIRAC
ln -s $DIRACDIR/DIRAC $PREFIX/lib/python2.7/site-packages/DIRAC

echo "DIRAC install complete."