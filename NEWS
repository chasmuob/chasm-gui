MoSSaiC Application Platform NEWS
===========================================

Version 0.4.2
-------------

 * Fix problem with the parsing of standard deviation in soil parameters form
 * Task tracker triggers a view/index build of the results on task completion
 * Support for CHASM/QUESTA point water source inputs
 * Support for CHASM/QUESTA slope loading inputs
 * Immutability of input and output data is now enforced at the database level
 * Fix bug where compaction script couldn't be executed directly in the shell
 * More docs added for operations and python backend

Version 0.4.1
-------------

 * GUI allows road link to be set at the slope datum metadata level and users
   are not allowed to create QUESTA slope profiles if the road link is missing
 * GUI strips trailing empty columns from generated CHASM mesh
 * GUI sets application field correctly in task specifications
 * GUI now calculates number of soil interfaces for measured soils correctly
 * JobWrapper adds slope location to results document to facilitate geospatial
   queries
 * Show function for geometry (slope profile) gracefully handles missing
   `end_boundary_condition` field by using a sensible default value
 * Minor bug fixes for job resubmission script
 * Minor bug fixes for deployment script
 * Results documents now include location data
 * Example geospatial query provided
 * A script is provided for generating usage reports on a per-user basis

Version 0.4.0
-------------

 * Simulation throughput increased due to use of zlib deflate to compress
   results document sizes and so reduce write times (note: this data is inflated
   in the `mining/slope_cells` view, meaning views will require significantly
   more disk space than data)
 * GUI provides a linear workflow that guides the user through a sequence of
   editing stages
 * GUI support for CHASM geometry data and task requests
 * GUI shows slope geometry with fixed aspect ratio of 1:1
 * Release database contains metadata only so it can be replicated to users
   without leaking the software executables
 * A private release database is created which mirrors the metadata in the
   release database and stores the actual executables for CHASM and QUESTA
 * A script is provided for uploading simulation executable metadata to the
   public release database and the executables themselves to the private release
   database
 * Complete system documentation, including user, developer and operations docs,
   now built from the docs Makefile target

Version 0.3.1
-------------

 * No more replication required between Task and Data databases
 * Job wrapper validates job inputs (including executables) before execution
 * Job wrapper now uses exponential backoff with randomisation for all
   database operations
 * Job wrapper waits a random amount of time between 0 and 30 seconds before
   starting to minimise the number of concurrent database actions
 * Javascript database views restored and available as deployment option
 * Utils scripts added for common ops operations
 * State database setup by default with Q=1, N=1 for full consistency at
   the expense of availability

Version 0.3.0
-------------

 * All new GUI for data entry, task composition and analysis
 * Publicly accessible login/signup page
 * Access to all but login/signup and docs restricted to authenticated users
   with the "mossaic" role
 * Tasks with stochastic depth measurements (water and soil strata) now
   supported
 * Slope cells data now parsed and included in results file
 * Job manager init script now uses hyphens instead of underscores
 * Job wrapper no longer accepts passwords via command arguments
 * QUESTA geometry data model refactored

Version 0.2.2
-------------

 * Stability improvements to job manager daemons
 * Job manager hits views with stale=ok where appropriate
 * Various deployment improvements
 * Updated to use BigCouch 0.4.x.map branch

Version 0.2.1
-------------

 * Fix broken results view in the GUI
 * Fix results query when net_expected_present_value is unavailable
 * Various deployment improvements

Version 0.2.0
-------------

 * Support for task requests with stochastic soil parameters and user specified number of runs
 * CHASM and QUESTA jobs can be submitted to the DIRAC workload management system (and from there the WLCG)
 * Number of jobs per task and number of total active jobs can be limited in the job manager configuration
 * Various security improvements to limit the permissions of credentials that are shipped out to worker nodes
 * SSL with certificate authentication used to authenticate and encrypt communications between worker nodes and CouchDB
 * Various improvements to installation/deployment/management scripts

Version 0.1.0
-------------

 * Support for simple (1 task:1 job) task requests
 * CHASM and QUESTA jobs run locally on a single server
 * Input data can be uploaded in native format for stability, boundary conditions, soils, geometry, engineering costs data
 * Basic security - require authenticated user to modify any databases and require job\_manager or \_admin roles for task/job submission
 * Simple GUI for uploading input data, viewing input data, launching simulations and viewing results
 * Basic installation/deployment/management scripts