# CHASM-GUI

The CHASM-GUI development, funded by the World Bank, is located under 'src/couchapps/gui-js/'. 
This work is available under an Apache 2.0 licence (http://www.apache.org/licenses/LICENSE-2.0).

CHASM-GUI is a CouchDB application distributed via a Vagrant (https://www.vagrantup.com/) 
Virtual Machine (VM) which runs on VirtualBox (https://www.virtualbox.org/). Access to the VM, 
which includes the CHASM binary needed for analysing data submitted via the CHASM-GUI, 
is available through the CHASM website (http://www.chasm.info/).

(c) 2016 University of Bristol

## Licence


```
#!text

Copyright 2016 University of Bristol

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```