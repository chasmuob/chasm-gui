#!/bin/bash

# variables
db_prefix=chasm
url=127.0.0.1
port=4000
username=chasm
password=chasm
auth=$username:$password

# directories
couchapps_dir=../src/couchapps
tests_dir=../tests
chasm_binary=chasm/bin/chasm

# base curl url
base_url=http://$auth@$url:$port

# databases
data_db=$db_prefix
task_db="$db_prefix"_task
state_db="$db_prefix"_state
release_db="$db_prefix"_release
release_db_private="$release_db"_private
login_db="$db_prefix"_login
docs_db="$db_prefix"_docs

# add the chasm admin user
#curl -X PUT http://$url:$port/_users/org.couchdb.user:chasm -d "{\"_id\": \"org.couchdb.user:chasm\",\"name\": \"chasm\",\"roles\": [\"admin\", \"mossaicadmin\"],\"type\": \"user\",\"password\": \"chasm\"}" -H 'Content-type: application/json'

curl -X PUT $base_url/_users/org.couchdb.user:chasm -H 'Accept: application/json' -H 'Content-Type: application/json' -d '{"name":"chasm","roles": ["admin", "mossaicadmin"], "type":"user"}'


# create the databases
for x in $data_db $task_db $state_db $release_db $release_db_private $login_db $docs_db
do
	curl -X PUT $base_url/$x
done


# push the apps
couchapp push $couchapps_dir/mossaic $base_url/$data_db
couchapp push $couchapps_dir/workflow $base_url/$task_db
couchapp push $couchapps_dir/state-machine $base_url/$state_db
couchapp push $couchapps_dir/release $base_url/$release_db
couchapp push $couchapps_dir/release $base_url/$release_db_private
couchapp push $couchapps_dir/docs $base_url/$docs_db
couchapp push $couchapps_dir/login $base_url/$login_db
couchapp push --docid _design/gui $couchapps_dir/gui-js $base_url/$data_db
couchapp push --docid _design/auth $couchapps_dir/auth-js $base_url/$data_db
couchapp push --docid _design/mining $couchapps_dir/mining-js $base_url/$data_db

# release information (just CHASM)
curl -X POST $base_url/$release_db -d @$tests_dir/data/releases/chasm_4.12.5.json -H 'Content-type: application/json'
curl -X PUT $base_url/$release_db_private/chasm_4.12.5/executable --data-binary @$chasm_binary -H 'Content-type: application/octet-stream'


# security roles
for x in $data_db $task_db $state_db $release_db
do
	curl -X PUT $base_url/$x/_security -d "{\"admins\":{\"names\":[\"$username\"]}, \"readers\":{\"roles\":[\"mossaic\"]}}" -H 'Content-type: application/json'
done

# admin login
curl -X PUT $base_url/$login_db/_security -d "{\"admins\":{\"names\":[\"$username\"]}}" -H 'Content-type: application/json'

# job wrapper security - might not need this in the future
curl -X PUT $base_url/$release_db_private/_security -d "{\"admins\":{\"names\":[\"$username\"]}, \"readers\":{\"roles\":[\"job_wrapper\"]}}" -H 'Content-type: application/json'

# Data ...
curl -X POST $base_url/$data_db -d @$tests_dir/data/input_files/boundary_conditions_deterministic.json -H 'Content-type: application/json'
curl -X POST $base_url/$data_db -d @$tests_dir/data/input_files/engineering_cost.json -H 'Content-type: application/json'
curl -X POST $base_url/$data_db -d @$tests_dir/data/input_files/geometry.json -H 'Content-type: application/json'
curl -X POST $base_url/$data_db -d @$tests_dir/data/input_files/geometry_questa_soil_approximated.json -H 'Content-type: application/json'
curl -X POST $base_url/$data_db -d @$tests_dir/data/input_files/reinforcements_earthnail.json -H 'Content-type: application/json'
curl -X POST $base_url/$data_db -d @$tests_dir/data/input_files/road_network.json -H 'Content-type: application/json'
curl -X POST $base_url/$data_db -d @$tests_dir/data/input_files/soils_deterministic.json -H 'Content-type: application/json'
curl -X POST $base_url/$data_db -d @$tests_dir/data/input_files/stability_bishop.json -H 'Content-type: application/json'