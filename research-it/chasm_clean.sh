#!/bin/bash

# variables
db_prefix=chasm
url=127.0.0.1
port=4000
username=chasm
password=chasm
auth=$username:$password

# base curl url
base_url=http://$auth@$url:$port

# databases
data_db=$db_prefix
task_db="$db_prefix"_task
state_db="$db_prefix"_state
release_db="$db_prefix"_release
release_db_private="$release_db"_private
login_db="$db_prefix"_login
docs_db="$db_prefix"_docs


# delete the databases
for x in $data_db $task_db $state_db $release_db $release_db_private $login_db $docs_db
do
	curl -X DELETE $base_url/$x
done
